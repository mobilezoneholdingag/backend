#!/usr/bin/env bash
echo 'Check dependencies for update...'
if [[ $(php advanced/yii command/has-new-dependencies) == '1' ]]
then
    cd advanced/
    composer config github-oauth.github.com ada0f9426ca6f970583e552e5130e0cffe44f3ac
    composer self-update
    composer update
    composer install
    echo 'Update complete.'
else
    echo 'Check complete. No updates.'
fi