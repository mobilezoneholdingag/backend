Stardust Backend
================

HOW TO INSTALL
--------------

We have a installation guide available in confluence: [Entwicklungsumgebung](https://deinhandy.atlassian.net/wiki/display/STAR/Entwicklungsumgebung)

DIRECTORY STRUCTURE
-------------------

```
advanced
    common
        config/              contains shared configurations
        helper/              contains static helpers (calculations, conversions etc.)
        mail/                contains view files for e-mails
        messages/
        models/              contains model classes used in both backend and frontend
        rawData/             contains raw text and lists for special actions (temporary)
    components
        api/
        ExportService/
        values/
        widgets/
    console
        config/              contains console configurations
        controllers/         contains console controllers (commands)
        migrations/          contains database migrations
        models/              contains console-specific model classes
        runtime/             contains files generated during runtime
    environments
        dev/
        prod/        
    backend
        assets/              contains application assets such as JavaScript and CSS
        config/              contains backend configurations
        controllers/         contains Web controller classes
        models/              contains backend-specific model classes
        runtime/             contains files generated during runtime
        views/               contains view files for the Web application
        web/                 contains the entry script and Web resources
    frontend
        assets/              contains application assets such as JavaScript and CSS
        config/              contains frontend configurations
        controllers/         contains Web controller classes
        models/              contains frontend-specific model classes
        runtime/             contains files generated during runtime
        views/               contains view files for the Web application
        web/                 contains the entry script and Web resources
        widgets/             contains frontend widgets
    migrations/              contains the initial database migration (for first setup of new instance of application)         
    vendor/                  contains dependent 3rd-party packages
    environments/            contains environment-based overrides
    tools                    contains external tools used by the application (currently only bank validation)
    tests                    contains various tests for the advanced application
        codeception/         contains tests developed with Codeception PHP Testing Framework
```