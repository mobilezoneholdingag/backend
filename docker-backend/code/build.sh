#!/bin/sh
cd ../../
cp docker-backend/code/Dockerfile .
docker build -t deinhandy/${1}stardust-backend .
rm Dockerfile