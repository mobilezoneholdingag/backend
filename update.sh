#!/usr/bin/env bash

if [ ! -r config-update.sh ]
then
    exit 0;
fi

source config-update.sh
cd /usr/share/nginx/deinhandy.de
git reset --hard
git fetch --all
git checkout $branch
git pull
npm install
rm -rf .sass-cache/
grunt minify:dev

cd advanced
php init --env=$environment --overwrite=$overwrite
php yii migrate --interactive=0

rm -rf frontend/runtime/Twig
chmod +x frontend/web/images/article
chmod +x tools/cKonto
service php5-fpm restart
service nginx restart