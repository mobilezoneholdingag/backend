<?php
return [
	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=mysql;dbname=mobilezone',
			'username' => 'root',
			'password' => '123',
			'charset' => 'utf8',
		],
		'urlManager' => [
			'hostInfo' => 'http://www.stardust.dev',
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'viewPath' => '@common/mail',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'mailtrap.io',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
				'username' => '3637512a7b3eeb',
				'password' => 'e9bebb3ea4a284',
				'port' => '25',
				'encryption' => 'tls',
			],
		],
		'homerMailer' => [
			'class' => yii\swiftmailer\Mailer::class,
			'viewPath' => '@homer',
			'htmlLayout' => false,
			'textLayout' => false,
			'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'mailtrap.io',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
				'username' => '3637512a7b3eeb',
				'password' => 'e9bebb3ea4a284',
				'port' => '25',
				'encryption' => 'tls',
			],
			'view' => [
				'class' => 'yii\web\View',
				'renderers' => [
					'twig' => [
						'class' => yii\twig\ViewRenderer::class,
						'options' => [
							'auto_reload' => true,
						],
						'extensions' => YII_DEBUG ? [
							Twig_Extension_Debug::class,
							Twig_Extensions_Extension_I18n::class,
							common\lib\twig\CurrencyExtension::class,
							common\lib\twig\TranslationExtension::class,
						] : [
							Twig_Extensions_Extension_I18n::class,
							common\lib\twig\CurrencyExtension::class,
							common\lib\twig\TranslationExtension::class,
						],
						'globals' => [
							'OfferHelper' => common\helper\OfferHelper::class,
						],
						'functions' => ['t' => 'Yii::t'],
					],
				],
			],
		],
	],
];
