<?php
use components\Locale;

return [
	'host' => [
		'rackspace' => [
			'credentials' => [
				'username' => 'mobilezone.apiuser',
				'apiKey' => '28b8e409559e4c3cbcb96fb72f11eedc'
			],
		]
	],
	'languages' => [
		'ch' => Locale::SUISSE,
		'fr' => Locale::FRANCE,
		'it' => Locale::ITALY,
	],
	'localeMatchName' => 'stardust',
	'filesystems' => [
		'contracts' => [
			'rootDir' => 'dev/',
		],
		'feeds' => [
			'rootDir' => '/app/console/runtime/feeds/',
			'adapter' => 'fs.adapter.local',
		],
		'exports' => [
			'rootDir' => '/app/console/runtime/exports/',
			'adapter' => 'fs.adapter.local',
		],
	],
	'picasso' => [
		'http://picasso.vokky.net/storage',
		'http://picasso.vokky.net/api/v1/',
		'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFjYjE5NjMxN2UwZjM0MDBjMzVhNThjNTQxMTBmMmM2ZjMwMjg2MmRiZjA2NGI3MWZhYWFkYjZiNTA2ZjQ5OTgzNjg1NzMzNzRiMzcyYWQwIn0.eyJhdWQiOiIxIiwianRpIjoiMWNiMTk2MzE3ZTBmMzQwMGMzNWE1OGM1NDExMGYyYzZmMzAyODYyZGJmMDY0YjcxZmFhYWRiNmI1MDZmNDk5ODM2ODU3MzM3NGIzNzJhZDAiLCJpYXQiOjE0ODkxNTM5NzMsIm5iZiI6MTQ4OTE1Mzk3MywiZXhwIjo0NjQ0ODI3NTczLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.hqm4w0KKOIt7MpDI6OND0SHUYAxJyq6eJSk89QWToeJSOJujjLEdNto4dz_wFyJrCgrVIDu7VBcvTBQLZXK9G4r--5vymCcaIcL1Cc9-tRcmjEkLjh3QeubXyqeb0aLJJfgWFs1BkAJQze3SPJJzJsYdwW-f6PW_bugQlv8rqv-wHrqWEphHhxcfut5QV7niYFrGin_2oO2r9Z4n6KSHQPuwxNTr8g-sYsxxlue9-qZar1DHNoxKR-m7fxa6XrkgvtXDE80NZph91P5m-XrdqrG3dFupYcf7ebUI3TNemAVueJqPXTvLXLNOnzzQiRID49MlJlLPj2AwzkcgjKqKHgDugvWiv2Wc6nrfeKj4OFitlJppfAzwDCmy49PFaezRQ1MQQJs-otcJyU46Uqpq1SNqqR2xXKvp1wJKWoW0pXNGSZ9tKtSnLos-38MT4NHzvFPgyIdHu4uTu6qPT4oMH_v0iXNwwcWkmlDvTvs2l_CjombQJwCvGnHbiNrFAuBav8vm0mfNM5vGQiMW7prcY0ExlF89_tn-Sv-MmgAmpu3Yt9HIQK4LM1rI51CQRNj9_w2fhjEk6CPLkWD-5R9erudmm67c587LSVRYZJeF7BLhtpZxsWgF2UoSzAlUQhrPvIbrmxUaHF-ruHmAvqCHWwFt7JVrspeAKjUpW6PB7uY',
		'local.picasso.json',
		0,
	],
	'picasso_namespace' => 'deinhandy-dev',
	'contract_configurator_recipient' => 'tobias.kloeckner@deinhandy.de',
];
