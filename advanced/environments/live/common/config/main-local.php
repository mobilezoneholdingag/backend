<?php
return [

	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=304dac56905f430db262aaab62c27e89.publb.rackspaceclouddb.com;dbname=mobilezone',
			'username' => 'live-mz-be',
			'password' => '4JBJFEGhRZsaTDDHp5y5',
			'charset' => 'utf8',
		],
		'urlManager' => [
			'class' => 'yii\web\UrlManager',
			'hostInfo' => 'https://backend.mobilezone.ch',
		],
		'urlManagerFrontend' => [
			'hostInfo' => 'https://www.mobilezone.ch',
		],
		'redis' => [
			'database' => 2,
		],
		'redis-session' => [
			'database' => 3,
		],
		'ftps' => [
			'parentDirectory' => '', // Live works directly on root directory on FTP
		],
	],
	'bootstrap' => ['log'],
];
