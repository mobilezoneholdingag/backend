<?php

use components\Locale;

return [
	'host' => [
		'rackspace' => [
			'credentials' => [
				'username' => 'mobilezone.apiuser',
				'apiKey' => '28b8e409559e4c3cbcb96fb72f11eedc',
			],
			'directories' => [
				'default' => [
					'container' => [
						'name' => 'mobilezone-marketing-images',
						'address' => 'https://f390f95260ca9ee462f7-2cb64ab23bfb1ae42dfbb76e032b194c.ssl.cf3.rackcdn.com',
					],
					'dir' => null,
					'region' => 'LON'
				],
				'images' => [
					'dir' => 'images'
				],
				'articleImages' => [
					'dir' => 'images/article'
				],
			],
		],
	],
	'languages' => [
		'net' => Locale::SUISSE,
		'fr' => Locale::FRANCE,
		'it' => Locale::ITALY,
	],
	'localeMatchName' => 'vokky',
	'filesystems' => [
		'feeds' => [
			'container' => 'mobilezone-feeds',
		],
	],
	'language_domain_map' => [
		'de' => 'https://www.mobilezone.ch',
		'de-CH' => 'https://www.mobilezone.ch',
		'fr' => 'https://fr.mobilezone.ch',
		'fr-FR' => 'https://fr.mobilezone.ch',
		'it' => 'https://it.mobilezone.ch',
		'it-IT' => 'https://it.mobilezone.ch',
	],
	'contract_configurator_recipient' => 'onlineshop@mobilezone.ch',
];
