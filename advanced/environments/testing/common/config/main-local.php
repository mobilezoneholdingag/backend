<?php
return [

	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=9d7278110e192dc5311ee4c3ebad23215d4a6d4a.rackspaceclouddb.com;dbname=mobilezone-testing',
			'username' => 'testing-mz-be',
			'password' => 'yPMfekzLiY8PDxc62KYCbbz6s2RHUu',
			'charset' => 'utf8',
		],
		'urlManager' => [
			'class' => 'yii\web\UrlManager',
			'hostInfo' => 'https://backend.mztesting.ch',
		],
		'urlManagerFrontend' => [
			'hostInfo' => 'https://www.mztesting.ch',
		],
		'redis' => [
			'database' => 4,
		],
		'redis-session' => [
			'database' => 5,
		],
	],
	'bootstrap' => ['log'],
];
