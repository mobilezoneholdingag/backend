<?php

use components\Locale;

return [
	'host' => [
		'rackspace' => [
			'credentials' => [
				'username' => 'mobilezone.apiuser',
				'apiKey' => '28b8e409559e4c3cbcb96fb72f11eedc',
			],
			'directories' => [
				'default' => [
					'container' => [
						'name' => 'mobilezone-marketing-images',
						'address' => 'https://f390f95260ca9ee462f7-2cb64ab23bfb1ae42dfbb76e032b194c.ssl.cf3.rackcdn.com',
					],
					'dir' => null,
					'region' => 'LON'
				],
				'images' => [
					'dir' => 'images'
				],
				'articleImages' => [
					'dir' => 'images/article'
				],
			],
		],
	],
	'languages' => [
		'net' => Locale::SUISSE,
		'fr' => Locale::FRANCE,
		'it' => Locale::ITALY,
	],
	'localeMatchName' => 'vokky',
	'filesystems' => [
		'exports' => [
			'container' => 'mobilezone-exports-staging',
		],
	],
	'language_domain_map' => [
		'de' => 'https://www.vokky.de',
		'de-CH' => 'https://www.vokky.de',
		'fr' => 'https://fr.vokky.de',
		'fr-FR' => 'https://fr.vokky.de',
		'it' => 'https://it.vokky.de',
		'it-IT' => 'https://it.vokky.de',
	],
	'contract_configurator_recipient' => 'alessandro.divito@mobilezone.ch',
	'picasso_namespace' => 'mobilezone',
];
