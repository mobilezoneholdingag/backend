<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=9d7278110e192dc5311ee4c3ebad23215d4a6d4a.rackspaceclouddb.com;dbname=mobilezone-testing2',
            'username' => 'testing2-mz-be',
            'password' => 'AQxwCaN958JNpueH8kSOvJh0KCrejq',
            'charset' => 'utf8',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'hostInfo' => 'https://backend.mztesting2.ch',
        ],
        'urlManagerFrontend' => [
            'hostInfo' => 'https://www.mztesting2.ch',
        ],
        'redis' => [
            'database' => 6,
        ],
        'redis-session' => [
            'database' => 7,
        ],
    ],
    'bootstrap' => ['log'],
];