<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gf8748r40fnbf09vnfdfbhj434rlk3r2qfqgkbymfbvySD',
        ],
        'urlManager' => [
            'hostInfo' => 'https://backend.mztesting2.ch',
        ],
    ],
];
if (!YII_ENV_TEST && !YII_ENV_PROD) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}
return $config;