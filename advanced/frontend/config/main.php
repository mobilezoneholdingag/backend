<?php

$params = array_replace_recursive(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-frontend',
	'basePath' => dirname(__DIR__),
	'bootstrap' => [
		'log',
		[
			'class' => yii\filters\ContentNegotiator::class,
			'languages' => array_values($params['languages']),
		],
	],
	'controllerNamespace' => 'frontend\controllers',
	'on beforeAction' => function($event) {
		\components\Locale::setEnvironmentConfigurations(dirname(dirname(__DIR__)));
		$directory = dirname(dirname(__DIR__));
	},
	'components' => [
		'assetManager' => [
			'class' => 'yii\web\AssetManager',
			'appendTimestamp' => true,
			'hashCallback' => ['\components\AssetHelper', 'hashPath'],
		],
		'user' => [
			'identityClass' => 'common\models\Customer',
			'enableAutoLogin' => false,
			//			'enableSession' => true,
		],
		'log' => [
			'traceLevel' => 0,
			'targets' => [
				[
					'class' => 'yii\log\DbTarget',
					'levels' => ['error', 'warning'],
					'logTable' => '{{%log_frontend}}',
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error'],
					'categories' => ['warenkorb'],
					'logFile' => '@app/runtime/logs/warenkorb.log',
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				[
					'class' => 'yii\rest\UrlRule',
					'tokens' => [
						'{id}' => '<id:[\\dA-Za-z\\-_][\\d,A-Za-z\\-_]*>',
					],
					'controller' => [
						'product',
						'article',
						'article-group',
						'tariff',
						'article-tariff',
						'article-group',
						'coupon',
						'checkout',
						'customer',
						'customer-address',
						'customer-phone-number',
						'newsletter',
						'manufacturer',
						'marketing-images',
						'landing-page',
						'shop',
						'shop-hour',
						'stock',
						'job',
						'renewal-request',
						'renewal-request-offer',
						'provider',
						'product-category',
						'article-insurance',
						'order/order-info',
						'order/item',
					],
					'extraPatterns' => [
						'POST login' => 'login',
						'POST save-passport-information' => 'save-passport-information',
						'POST change-password' => 'change-password',
						'POST confirm-order' => 'confirm-order',
						'POST set-subscription' => 'set-subscription',
						'POST set-address' => 'set-address',
						'POST set-new-address' => 'set-new-address',
						'POST set-informations' => 'set-informations',
						'POST send-password-reset-mail' => 'send-password-reset-mail',
						'POST reset-password' => 'reset-password',
						'POST newsletter-subscribes' => 'newsletter-subscribes',
						'POST contact' => 'contact',
						'GET default-address' => 'default-address',
						'GET all-addresses' => 'all-addresses',
						'GET all-languages' => 'all-languages',
						'GET is-subscribed' => 'is-subscribed',
						'GET search' => 'search',
						'GET orderlist' => 'orderlist',
						'GET order-items' => 'order-items',
						'GET order/order' => 'order/order',
						'GET order/item' => 'order/item',
						'GET title' => 'title',
					],
					'pluralize' => false,
				],
				[
					'class' => 'yii\rest\UrlRule',
					'controller' => ['i18n' => 'language'],
					'extraPatterns' => [
						'POST file' => 'file',
					],
					'pluralize' => false,
				],
			],
		],
		'i18n' => [
			'translations' => [
				'app*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@common/messages',
					'fileMap' => [
						'app' => 'app.php',
						'app/error' => 'error.php',
					],
				],
				'seo*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@common/messages',
					'fileMap' => [
						'seo' => 'seo.php',
						'app/error' => 'error.php',
					],
				],
			],
		],
		'view' => [
			'defaultExtension' => 'twig',
			'renderers' => [
				'twig' => [
					'class' => 'yii\twig\ViewRenderer',
					'cachePath' => '@runtime/Twig/cache',
					'options' => YII_DEBUG ? [
						'debug' => true,
						'auto_reload' => true,
					] : [],
					'extensions' => YII_DEBUG ? [
						'\Twig_Extension_Debug',
						'\Twig_Extensions_Extension_I18n',
					] : [
						'\Twig_Extensions_Extension_I18n',
					],
					'globals' => [
						'html' => '\yii\helpers\Html',
						'Url' => '\yii\helpers\Url',
						'OfferHelper' => '\common\helper\OfferHelper',
						'Locale' => '\components\Locale',
					],
					'uses' => ['yii\bootstrap'],
				],
			],
		],
	],
	'params' => $params,
];
