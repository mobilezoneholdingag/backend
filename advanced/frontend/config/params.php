<?php
use components\Locale;

$params = [
	'adminEmail' => 'kolja.zuelsdorf@deinhandy.de',
	'cdn' => 'https://f4b3116d1a53e52a8120-7d0864a40903766aae11505e59659235.ssl.cf3.rackcdn.com',
	'url' => [
		'rules' => [
			'<controller:\w+>' => '<controller>',
			'<controller:\w+>/<action:[\w-]+>/?' => '<controller>/<action>',
			'<controller:\w+>/<action:[\w\-]+>/<id:[\w\-]+>' => '<controller>/<action>',
		] 
	],
	'google.tmc.id' => 'GTM-P5SFK8',
];

return $params;
