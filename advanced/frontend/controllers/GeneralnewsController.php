<?php

namespace frontend\controllers;

use common\models\GeneralNews;

use frontend\controllers\generalnews\ViewAction;
use yii\rest\ActiveController;

class GeneralnewsController extends ActiveController
{
    public $modelClass = GeneralNews::class;

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['update']);

        $actions['view']['class'] = ViewAction::class;

        return $actions;
    }
}
