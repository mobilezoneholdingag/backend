<?php

namespace frontend\controllers;

use common\models\CustomerAddress;
use Yii;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class CustomerAddressController extends ActiveController
{
	public $modelClass = '\common\models\CustomerAddress';

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['options']);

		return $actions;
	}

	public function actionDefaultAddress($id = null)
	{
		$data = CustomerAddress::find()->where(['is_default' => 1])->andWhere(['customer_id' => $id])->one();

		if ($data == null) {
			return ['statusCode' => 404];
		}

		return $data;
	}

	public function actionAllAddresses($id = null)
	{
		$data = CustomerAddress::find()->where(['customer_id' => $id])->all();

		if ($data == null) {
			return ['statusCode' => 404];
		}

		return $data;
	}

	public function actionSetAddress()
	{
		$params = Yii::$app->request->post();

		if ($params['is_default'] == 1) {
			/** @var CustomerAddress $defaultAddress */
			$defaultAddress = CustomerAddress::find()->where(['customer_id' => $params['customer_id']])->andWhere(
				['is_default' => 1]
			)->one();

			if ($defaultAddress != null) {
				$defaultAddress->is_default = 0;
				$defaultAddress->save();
			}
		}

		$data = CustomerAddress::find()->where(['id' => $params['id']])->one();
		$data->attributes = $params;
		$data->save();

		if ($data == null) {
			return ['statusCode' => 404];
		}

		return $data;
	}

	public function actionSetNewAddress()
	{
		$params = Yii::$app->request->post();

		$customerData = new CustomerAddress();
		$customerData->attributes = $params;

		$customerData->save();

		if ($customerData == null) {
			return ['statusCode' => 404];
		}

		return $customerData;
	}
}
