<?php

namespace frontend\controllers;

use common\modules\jobOffer\models\Job;
use frontend\controllers\job\ViewAction;
use yii\rest\ActiveController;

class JobController extends ActiveController
{
	public $modelClass = Job::class;

	public function actions()
	{
		$actions = parent::actions();

		unset($actions['delete'], $actions['update'], $actions['options']);

		$actions['view']['class'] = ViewAction::class;

		return $actions;
	}
}
