<?php

namespace frontend\controllers;

use common\models\ArticleTariffs;
use frontend\controllers\articletariff\IndexAction;
use frontend\controllers\articletariff\SearchAction;
use yii\rest\ActiveController;

/**
 * API endpoint for article tariffs.
 *
 * @package frontend\controllers
 */
class ArticleTariffController extends ActiveController
{
	public $modelClass = ArticleTariffs::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		// Custom index action
		$actions['index']['class'] = IndexAction::class;

		$actions['search'] = [
			'class' => SearchAction::class,
			'modelClass' => $this->modelClass,
			'checkAccess' => [$this, 'checkAccess'],
		];

		return $actions;
	}
}
