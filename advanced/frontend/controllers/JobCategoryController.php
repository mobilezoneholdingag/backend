<?php

namespace frontend\controllers;

use common\modules\jobOffer\models\JobCategory;
use yii\rest\ActiveController;

class JobCategoryController extends ActiveController
{
	public $modelClass = JobCategory::class;

	public function actions()
	{
		$actions = parent::actions();

		unset($actions['delete'], $actions['update'], $actions['view'], $actions['options']);

		return $actions;
	}
}
