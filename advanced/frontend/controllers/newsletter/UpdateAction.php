<?php

namespace frontend\controllers\newsletter;

use common\models\CustomerAddress;
use common\models\NewsletterSubscriber;
use Yii;

class UpdateAction extends \yii\rest\UpdateAction
{
	public function run($id)
	{
		$params = Yii::$app->getRequest()->post();
		$customerId = $params['customer_id'];

		/** @var CustomerAddress $customerAddress */
		$customerAddress = CustomerAddress::find()->where(['is_default' => 1, 'customer_id' => $customerId])->one();

		if ($customerAddress && isset($params['firstname']) && isset($params['lastname']) && isset($params['sex'])) {
			$customerAddress->firstname = $params['firstname'];
			$customerAddress->lastname = $params['lastname'];
			$customerAddress->sex = $params['sex'];
			$customerAddress->save();
		}

		return parent::run($id);
	}

	public function findModel($id)
	{
		return NewsletterSubscriber::find()->where([
			'id' => $id,
			'customer_id' => Yii::$app->getRequest()->post('customer_id')]
		)->one();
	}
}
