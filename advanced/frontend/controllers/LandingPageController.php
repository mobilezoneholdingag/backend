<?php

namespace frontend\controllers;

use common\models\LandingPages;
use frontend\controllers\landingpage\ViewAction;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class LandingPageController extends ActiveController
{
	public $modelClass = LandingPages::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		$actions['view']['class'] = ViewAction::class;
		
		return $actions;
	}
}