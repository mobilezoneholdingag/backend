<?php

namespace frontend\controllers\provider;

use yii\data\ActiveDataProvider;

class IndexAction extends \yii\rest\IndexAction
{
	/**
	 * @return ActiveDataProvider
	 */
	public function run()
	{
		$dataProvider = parent::run();

		$dataProvider->query->orderBy('sort');

		return $dataProvider;
	}
}
