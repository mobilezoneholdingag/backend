<?php

namespace frontend\controllers;

use common\models\Coupon;
use frontend\controllers\coupon\ViewAction;
use yii\rest\ActiveController;

/**
 * Class CouponController
 *
 * @package frontend\controllers
 */
class CouponController extends ActiveController
{
	public $modelClass = Coupon::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		// Custom view action
		$actions['view']['class'] = ViewAction::class;

		return $actions;
	}
}
