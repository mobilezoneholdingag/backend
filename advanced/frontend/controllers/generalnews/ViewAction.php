<?php

namespace frontend\controllers\generalnews;

use common\models\GeneralNews;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		return GeneralNews::findAll()->orderBy('date DESC');
	}
}
