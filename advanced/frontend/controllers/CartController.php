<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\ArticleTariffs;
use common\modules\pricing\models\ConsolidatedPrice;
use common\models\Coupon;
use common\modules\pricing\models\Price;
use common\models\Tariffs;
use common\modules\payment\models\PaymentMethodContext;
use frontend\controllers\rest\Controller;
use Yii;

/**
 * API endpoint for cart price calculations.
 *
 * @package frontend\controllers
 */
class CartController extends Controller
{
	public function actionTotals()
	{
		$request = Yii::$app->request;

		$prices = new ConsolidatedPrice();
		$priceOnce = $prices->getPriceOnce();
		$priceMonthly = $prices->getPriceMonthly();

		$products = $request->get('products', []);

		// TODO: This has to be extracted into price calculation services! Waiting for proper event system.
		foreach ($products as $product) {
			switch ($product['type']) {
				case 'article':
					/** @var Article $article */
					$article = Article::findOne($product['id']);
					$priceOnce->add($article->getPriceOnce());
					break;
				case 'tariff':
					/** @var Tariffs $tariff */
					$tariff = Tariffs::findOne($product['id']);
					$priceMonthly->add($tariff->getPriceMonthly());
					break;
				case 'article_tariff':
					/** @var ArticleTariffs $articleTariff */
					$articleTariff = ArticleTariffs::findOne($product['id']);
					$priceOnce->add($articleTariff->getPriceOnce());
					$priceMonthly->add($articleTariff->getPriceMonthly());
					if ($articleTariff->needsSimCardOrIsSwisscom()
						&& $simCard = $articleTariff->getSimCard()) {
						$priceOnce->add($simCard->getPriceOnce());
					}
			}
		}

		$coupon = $request->get('coupon');

		if (isset($coupon['code'])) {
			$couponModel = Coupon::findOne(['code' => $coupon['code']]);

			if (empty($couponModel) === false) {

			    if($couponModel->validateCode()) {
                    $priceOnce->subtract(new Price($couponModel->discount_device_price));
                    // We will not pay back negative amounts after coupon usage
                    if ($priceOnce->getValue() < 0) {
                        $priceOnce->setZero();
                    }
                }
			}
		}

		$totals = [
			'total' => $prices,
		];

		$paymentMethod = $request->get('payment_method');
		if ($paymentMethod) {
			$paymentMethodPrice = (new PaymentMethodContext($paymentMethod))->calculateFee($priceOnce->getValue());
			$commissionPrice = new Price($paymentMethodPrice);
			$priceOnce->add($commissionPrice);
			$totals['commission'] = $commissionPrice->getValue();
		}

		return $totals;
	}

	protected function verbs()
	{
		return [
			'totals' => ['GET'],
		];
	}
}
