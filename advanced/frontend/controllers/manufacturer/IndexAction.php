<?php

namespace frontend\controllers\manufacturer;

use yii\data\ActiveDataProvider;

class IndexAction extends \yii\rest\IndexAction
{
	/**
	 * @return ActiveDataProvider
	 */
	public function run()
	{
		$dataProvider = parent::run();
        $dataProvider->setPagination(false);
		$dataProvider->query->orderBy('sort');

		return $dataProvider;
	}
}
