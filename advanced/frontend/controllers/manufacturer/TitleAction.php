<?php

namespace frontend\controllers\manufacturer;

use common\models\Manufacturer;
use Yii;

class TitleAction extends \yii\rest\IndexAction
{
	public $modelClass = Manufacturer::class;

	public function run()
	{
		$id = Yii::$app->request->get('product_category_id');

		return Manufacturer::getManufacturerWithActiveArticles($id);
	}
}
