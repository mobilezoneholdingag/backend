<?php

namespace frontend\controllers;

use frontend\controllers\tariff\SearchAction;
use frontend\controllers\tariff\ViewAction;
use yii\rest\ActiveController;

/**
 * API endpoint for tariffs.
 *
 * @package frontend\controllers
 */
class TariffController extends ActiveController
{
	public $modelClass = '\common\models\Tariffs';

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		$actions['search'] = [
			'class' => SearchAction::class,
			'modelClass' => $this->modelClass,
			'checkAccess' => [$this, 'checkAccess'],
		];
		$actions['view']['class'] = ViewAction::class;

		return $actions;
	}
}
