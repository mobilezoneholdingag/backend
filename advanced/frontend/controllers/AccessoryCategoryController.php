<?php

namespace frontend\controllers;

use common\modules\set\models\AccessoryCategory;
use yii\rest\ActiveController;

/**
 * API Endpoint for AccessoryCategory
 *
 * @package frontend\controllers
 */
class AccessoryCategoryController extends ActiveController
{
	public $modelClass = AccessoryCategory::class;

	/**
	 * @return mixed
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		return $actions;
	}
}
