<?php

namespace frontend\controllers\rest;

use yii\rest\Controller as RestController;

/**
 * Abstract REST controller in order to have POST-requests properly parsed as single parameters. (Using filter).
 *
 * @package frontend\controllers\rest
 */
abstract class Controller extends RestController
{
	public function behaviors()
	{
		return array_merge(
			parent::behaviors(),
			[
				'class' => '\common\filters\rest\Post',
			]
		);
	}
}