<?php

namespace frontend\controllers\accessorycategoryset;

use Yii;

class IndexAction extends \yii\rest\IndexAction
{
	public function run()
	{
		$request = Yii::$app->request;
		$accessoryCategoryId = $request->get('accessory_category_id');
		$dataProvider = parent::run();
		$dataProvider->pagination->pageSize = 500;

		if ($accessoryCategoryId) {
			$dataProvider->query->where(['accessory_category_id' => $accessoryCategoryId]);
		}

		return $dataProvider;
	}
}
