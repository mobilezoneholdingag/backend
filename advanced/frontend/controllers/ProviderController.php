<?php

namespace frontend\controllers;

use common\models\Provider;
use frontend\controllers\provider\IndexAction;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class ProviderController extends ActiveController
{
	public $modelClass = Provider::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		// Custom index action
		$actions['index']['class'] = IndexAction::class;

		return $actions;
	}
}
