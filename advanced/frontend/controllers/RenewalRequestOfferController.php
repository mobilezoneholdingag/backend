<?php

namespace frontend\controllers;

use common\modules\renewalRequest\models\RenewalRequestOffers;
use frontend\controllers\renewalrequestoffer\ViewAction;
use yii\rest\ActiveController;

/**
 * ContractRequestController implements the CRUD actions for ContractRequest model.
 */
class RenewalRequestOfferController extends ActiveController
{
	public $modelClass = RenewalRequestOffers::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['update'], $actions['index'], $actions['options']);

		// Custom view action
		$actions['view']['class'] = ViewAction::class;

		return $actions;
	}
}
