<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\ProductCategory;
use common\models\Shop;
use frontend\models\SearchForm;
use Yii;
use yii\rest\ActiveController;
use common\models\Stock;

class SearchController extends ActiveController
{
	public $modelClass = SearchForm::class;
	public $limitQuery = 50;

	public function actionSearch()
	{
		if (empty(Yii::$app->request->get('q'))) {
			Yii::$app->response->statusCode = 404;

			return null;
		}

		$query = Yii::$app->request->get('q');

		$articles = $this->findArticles($query);
		$shops = $this->findShops($query);

		$result = [];

		if (!empty($articles) || !empty($shops)) {
			$result['articles'] = $articles;
			$result['shops'] = $shops;
		}

		return $result != null ? $result : null;
	}

	private function findArticles($query): array
	{
	    
	    /*
	     * Select only Articles with Group (Smartphones)
	     */
	    
	    $grouped = Article::find()
	    ->select(Article::tableName() . '.id')
	    ->where(['LIKE', 'product_name', $query])
	    ->joinWith(['stock s'])
	    ->orWhere(['=', 'mat_no', $query])
	    ->orWhere(['=', 'mpn', $query])
	    ->andWhere([Article::tableName() . '.is_active' => 1])
	    ->andWhere([Article::tableName() . '.product_category_id' => array_merge(
	        [ProductCategory::SMARTPHONE, ProductCategory::TABLET, ProductCategory::SMARTWATCH,],
	        ProductCategory::ACCESSORIES
	        )])
        /* ->andWhere(['>', 's.quantity', 0]) */
        ->andWhere(['!=', 'group_id', 0])
        ->orderBy(['price_sale' => SORT_DESC])
        ->groupBy(['group_id'])
        ->limit($this->limitQuery)
        ->column();
	    
        $ungrouped = Article::find()
        ->select(Article::tableName() . '.id')
        ->where(['LIKE', 'product_name', $query])
        ->joinWith(['stock s'])
        ->orWhere(['=', 'mat_no', $query])
        ->orWhere(['=', 'mpn', $query])
        ->andWhere([Article::tableName() . '.is_active' => 1])
        ->andWhere([Article::tableName() . '.product_category_id' => array_merge(
            [ProductCategory::SMARTPHONE, ProductCategory::TABLET, ProductCategory::SMARTWATCH,],
            ProductCategory::ACCESSORIES
            )])
        /* ->andWhere(['>', 's.quantity', 0]) */
        ->andWhere(['=', 'group_id', 0])
        ->orderBy(['price_sale' => SORT_DESC])
        ->limit($this->limitQuery)
        ->column();
	    
        return array_merge($grouped, $ungrouped);
        
	}

	private function findShops($query): array
	{
		return Shop::find()
			->select('id')
			->where(['LIKE', 'zip', $query])
			->orWhere(['LIKE', 'city', $query])
			->limit($this->limitQuery)
			->column();
	}
}
