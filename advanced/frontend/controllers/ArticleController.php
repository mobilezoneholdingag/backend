<?php

namespace frontend\controllers;

use common\models\Article;
use frontend\controllers\article\IndexAction;
use yii\rest\ActiveController;

/**
 * API endpoint for articles.
 *
 * @package frontend\controllers
 */
class ArticleController extends ActiveController
{
	public $modelClass = Article::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		// Custom index action
		$actions['index']['class'] = IndexAction::class;

		return $actions;
	}
}
