<?php

namespace frontend\controllers;

use common\models\Stock;
use yii\rest\ActiveController;

/**
 * API endpoint for stocks.
 *
 * @package frontend\controllers
 */
class StockController extends ActiveController
{
	public $modelClass = Stock::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		return $actions;
	}
}