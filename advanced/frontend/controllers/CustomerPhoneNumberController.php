<?php

namespace frontend\controllers;

use common\models\CustomerPhoneNumber;
use frontend\controllers\customerphonenumber\IndexAction;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class CustomerPhoneNumberController extends ActiveController
{
	public $modelClass = CustomerPhoneNumber::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete']);

		// Custom index action
		$actions['index']['class'] = IndexAction::class;

		return $actions;
	}
}
