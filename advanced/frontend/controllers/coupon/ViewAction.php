<?php

namespace frontend\controllers\coupon;

use common\models\Coupon;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		return Coupon::findOne(['code' => $id]);
	}
}
