<?php

namespace frontend\controllers\job;

use common\modules\jobOffer\models\Job;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		if (is_numeric($id)) {
			return parent::findModel($id);
		}

		return Job::findOne(['slug' => $id]);
	}
}
