<?php

namespace frontend\controllers\renewalrequest;

use common\modules\renewalRequest\models\RenewalRequest;
use yii\web\HttpException;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		$renewalRequest = RenewalRequest::findOne(['id' => $id]);

		if (!$renewalRequest) {
			throw new HttpException(404, "Renewal Request not found!");
		}

		if ($renewalRequest->status === RenewalRequest::STATUS_OFFERED) {
			$renewalRequest->status = RenewalRequest::STATUS_VIEWED;
			$renewalRequest->save();
		}

		return $renewalRequest;
	}
}
