<?php

namespace frontend\controllers\renewalrequest;

use common\models\Customer;
use common\modules\renewalRequest\mappers\CustomerMapper;
use Yii;

class UpdateAction extends \yii\rest\UpdateAction
{
	public function run($id)
	{
		$renewalRequest = $this->findModel($id);
		$request = Yii::$app->request;

		if ($customerId = $request->getBodyParam('customer_id')) {
			CustomerMapper::map(Customer::findOne($customerId), $renewalRequest);
			$renewalRequest->save(false);
		}

		return $renewalRequest;
	}
}
