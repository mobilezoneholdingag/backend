<?php

namespace frontend\controllers;

use common\modules\set\models\AccessoryCategorySet;
use frontend\controllers\accessorycategoryset\IndexAction;
use yii\rest\ActiveController;

/**
 * API Endpoint for AccessoryCategory
 *
 * @package frontend\controllers
 */
class AccessoryCategorySetController extends ActiveController
{
	public $modelClass = AccessoryCategorySet::class;

	/**
	 * @return mixed
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		$actions['index']['class'] = IndexAction::class;

		return $actions;
	}
}
