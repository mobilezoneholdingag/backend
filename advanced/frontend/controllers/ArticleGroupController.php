<?php

namespace frontend\controllers;

use common\models\ArticleGroups;
use frontend\controllers\articlegroup\IndexAction;
use frontend\controllers\articlegroup\ViewAction;
use yii\rest\ActiveController;

/**
 * Class ArticleGroups
 *
 * @package frontend\controllers
 */
class ArticleGroupController extends ActiveController
{
	public $modelClass = ArticleGroups::class;

	/**
	 * @return mixed
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		return $actions;
	}
}
