<?php

namespace frontend\controllers;

use common\models\ProductCategory;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class ProductCategoryController extends ActiveController
{
	public $modelClass = ProductCategory::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		return $actions;
	}
}
