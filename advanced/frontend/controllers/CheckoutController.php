<?php

namespace frontend\controllers;

use common\modules\order\events\OrderPlacedWithPayloadEvent;
use common\modules\order\factories\CustomerFactory;
use common\modules\order\factories\OrderFactory;
use common\modules\order\factories\OrderItemFactory;
use common\modules\order\models\Order;
use common\modules\order\models\Status;
use common\modules\payment\helper\Method;
use common\modules\renewalRequest\models\RenewalRequest;
use common\modules\renewalRequest\models\RenewalRequestOffers;
use common\modules\swissbilling\facades\InvoiceFacade;
use common\modules\swisscom\controllers\RetentionController;
use Exception;
use frontend\controllers\rest\Controller;
use LogicException;
use Yii;
use yii\base\Module;

/**
 * API endpoint for checkout confirmation and order save.
 *
 * @package frontend\controllers
 */
class CheckoutController extends Controller
{
	protected $orderItemFactory;
	protected $orderFactory;
	protected $customerFactory;

	public function __construct(
		$id,
		Module $module,
		CustomerFactory $customerFactory,
		OrderFactory $orderFactory,
		OrderItemFactory $orderItemFactory,
		array $config = []
	)
	{
		$this->customerFactory = $customerFactory;
		$this->orderFactory = $orderFactory;
		$this->orderItemFactory = $orderItemFactory;

		parent::__construct($id, $module, $config);
	}

	/**
	 * @return array
	 */
	public function actionConfirmOrder()
	{
		$postData = Yii::$app->request->post();
		if (!$postData) {
			return [];
		}

		$transaction = Yii::$app->db->beginTransaction();
		try {
			$customerAndAddressInfo = $this->customerFactory->create($postData);
			$order = $this->orderFactory->create(
				$postData['payment']['type'],
				$postData['cart']['items'],
				$customerAndAddressInfo,
				$postData['customerPhoneNumber'] ?? []
			);
			$this->orderItemFactory->create($order, $postData['cart']['items']);
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollBack();
			Yii::error($e, 'checkout');

			return [];
		}

		$paymentRedirectCode = $this->processPaymentAuthorization($postData, $order);

		$this->fireOrderPlaceEvent($order);

		$this->updateRenewalRequestStatus($order, $postData['renewal_request_id'] ?? null);

		return [
			'order_id' => $order->id,
			'order_status' => $order->status,
			'payment_redirect_html' => $paymentRedirectCode,
		];
	}

	private function updateRenewalRequestStatus(Order $order, int $renewalRequestId = null)
	{
		$hash = Yii::$app->request->post('article_hash');

		if (is_null($hash)) {
			return;
		}

		/** @var RenewalRequest $renewalRequest */
		$renewalRequest = $hash === RetentionController::HASH_KEY
			? RenewalRequest::findOne($renewalRequestId)
			: RenewalRequestOffers::findOne(['hash' => $hash])->renewalRequest;

		if ($renewalRequest->status === RenewalRequest::STATUS_VIEWED) {
			$renewalRequest->status = RenewalRequest::STATUS_CONFIRMED;
			$renewalRequest->order_id = $order->id;
			$renewalRequest->save(false);
		}
	}

	/**
	 * @param $order
	 */
	private function fireOrderPlaceEvent($order)
	{
		$orderPlacedWithPayloadEvent = new OrderPlacedWithPayloadEvent();
		$orderPlacedWithPayloadEvent->setOrder($order);
		$postData = Yii::$app->request->post();
		if (empty($postData) === false) {
			$orderPlacedWithPayloadEvent->setPayload($postData);
		}
		dispatch($orderPlacedWithPayloadEvent);
	}

	/**
	 * @param array $postData
	 * @param Order $order
	 *
	 * @return string
	 * @throws \yii\base\InvalidConfigException
	 */
	private function processPaymentAuthorization(array $postData, Order $order) : string
	{
		// skip payment authorization when there is nothing to pay
		if ($order->price_once < 0.01) {
			$order->status = Status::RECEIVED;
			$order->save();

			return '';
		}

		switch ($order->payment_method) {
			case Method::PREPAYMENT:
				$order->status = Status::AWAITING_PREPAYMENT;
				$order->save();

				return '';
			case Method::ACCOUNT_PURCHASE:
				$this->processAccountPurchasePayment($order);

				return '';
			case Method::CREDIT_CARD:
			case Method::POSTFINANCE_E_FINANCE:
			case Method::POSTFINANCE_CARD:
				return $this->processPostfinancePayment($order);
			case Method::NO_PAYMENT_REQUIRED:
				return '';
			default:
				throw new LogicException(
					'Payment method "' . $order->payment_method . '" autorization can not be processed.'
				);
		}
	}

	private function processAccountPurchasePayment(Order $order)
	{
		$facade = new InvoiceFacade(Yii::$app->request->getBodyParams(), ['order_id' => $order->id]);
		$result = $facade->actionDirect();

		if (($result['success'] ?? false) === true) {
			$order->status = Status::RECEIVED;
		} else {
			$order->status = Status::PAYMENT_INCOMPLETE;
		}
		$order->save();
	}

	private function processPostfinancePayment(Order $order): string
	{
		$request = Yii::$app->request;
		$tempQueryParams = $request->getQueryParams();
		$request->setQueryParams(['order_id' => $order->id]);
		$form = Yii::$app->runAction('postfinance/ecom/form');
		$request->setQueryParams($tempQueryParams);

		$redirectCode = '';
		if (isset($form['error']) === false) {
			$redirectCode = $form['form_html'];

			return $redirectCode;
		} else {
			Yii::error('Errors occured when trying to generate an PostFinance e-commerce redirect form: ' .var_export($form['errors'], true), 'checkout');

			return $redirectCode;
		}
	}
}
