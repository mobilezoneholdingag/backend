<?php

namespace frontend\controllers;

use common\modules\renewalRequest\models\RenewalRequest;
use frontend\controllers\renewalrequest\UpdateAction;
use frontend\controllers\renewalrequest\ViewAction;
use yii\rest\ActiveController;

/**
 * ContractRequestController implements the CRUD actions for ContractRequest model.
 */
class RenewalRequestController extends ActiveController
{
	public $modelClass = RenewalRequest::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		unset($actions['delete'], $actions['index'], $actions['options']);

		$actions['view']['class'] = ViewAction::class;
		$actions['update']['class'] = UpdateAction::class;

		return $actions;
	}
}
