<?php

namespace frontend\controllers;

use common\models\ShopHour;
use yii\rest\ActiveController;

class ShopHourController extends ActiveController
{
	public $modelClass = ShopHour::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		return $actions;
	}
}
