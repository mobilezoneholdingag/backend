<?php

namespace frontend\controllers\marketingimages;

use common\models\MarketingImages;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Query;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		$image = null;
		$query = static::getBaseQuery();
		$image = $query->andWhere(['tag' => $id, 'language' => Yii::$app->language])->one()
			?? $query->andWhere(['tag' => $id, 'language' => null])->one();

		if (!$image) {
			Yii::$app->response->statusCode = 404;
		}

		return $image;
	}

	public static function getBaseQuery(): ActiveQuery
	{
		$query = MarketingImages::find();

		$query->orderBy(['publish_at' => SORT_DESC]);
		self::applyPublishedAtCondition($query);

		return $query;
	}

	public static function applyPublishedAtCondition(Query $query)
	{
		$today = (new \DateTime('now'))->format('Y-m-d');
		$query->andWhere(['<=', 'publish_at', "$today"]);
	}
}
