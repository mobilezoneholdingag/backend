<?php

namespace frontend\controllers\landingpage;

use Yii;
use common\models\LandingPages;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		$landingPage = LandingPages::findOne(['url' => $id, 'language' => Yii::$app->language]);

		if (!$landingPage) {
			Yii::$app->response->statusCode = 404;
		}

		return $landingPage;
	}
}
