<?php

namespace frontend\controllers\articletariff;

class IndexAction extends SearchAction
{
	public function run()
	{
		$itemsAndFacets = parent::run();

		return $itemsAndFacets[0]['items'] ?? [];
	}
}
