<?php

namespace frontend\controllers\articletariff;

use common\models\Article;
use common\models\ArticleGroupsTariff;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use frontend\controllers\_generic\BaseTariffSearchAction;
use frontend\controllers\tariff\SearchAction as TariffSearchAction;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\HttpException;

class SearchAction extends BaseTariffSearchAction
{
	const SIMPLE_FILTER_PROPERTIES = [
		'provider_id' => 'provider',
		'contract_duration' => 'contract_duration',
	];

	const VALID_PARAMETERS = [
		'price_monthly',
		'data_volume',
		'recommended_for_youth',
		'deferred_payment',
		'provider_id_excluded',
		'article_id_excluded',
		'provider_id',
		'swisscom_id',
		'contract_duration',
		'offer_type',
		'article',
		'tariff',
		'article_tariff_id_included',
		'per-page',
		'group_by'
	];

	public function run()
	{
		$request = Yii::$app->request;

		if ($request->get('check-if-exists') === '1') {
			return [['exists' => $this->checkIfActiveItemExists()]];
		}

		parent::run();
		$articleId = $request->get('article');
		$addFacets = $request->get('facets');

		return [[
			'facets' => $addFacets ? $this->facetInformation->gatherFacetInfo() : [],
			'items' => $this->dataProvider->getModels(),
			'default_items' => $this->getDefaultArticleTariffs($articleId),
			'total' => $this->dataProvider->totalCount,
		]];
	}

	private static function removeSimOnlyFromResults(ActiveQuery $query)
	{
		$query->andWhere(['!=', ArticleTariffs::tableName() . '.article_id', Article::SIM_ONLY_ID]);
	}

	public static function joinWithTariffAndEnsureIsActive(ActiveQuery $query)
	{
		$query
			->joinWith('tariff')
			->andWhere([Tariffs::tableName() . '.is_active' => true])
			->andWhere([Tariffs::tableName() . '.is_active_erp' => true])
			->andWhere([Tariffs::tableName() . '.is_b2b_allowed' => false]);
	}

	public static function joinWithArticleAndEnsureIsActive(ActiveQuery $query)
	{
		$query
			->joinWith('article')
			->andWhere([Article::tableName() . '.is_active' => true])
			->andWhere([Article::tableName() . '.is_active_erp' => true]);
	}

	public static function applyOrderAndGrouping(ActiveQuery $query)
	{
		if ($orderBy = Yii::$app->request->get('order_by')) {
			$orderBy = array_map('intval', $orderBy);
			$query->addOrderBy($orderBy);
		} else {
			$query->addOrderBy(['price_article_once' => SORT_ASC, 'article_id' => SORT_DESC, 'tariff_id' => SORT_DESC]);
		}

		if ($groupBy = Yii::$app->request->get('group_by')) {
			$query->addGroupBy([$groupBy]);
		}
	}

	protected function validateParameters()
	{
		parent::validateParameters();

		$params = Yii::$app->request->get();

		if (isset($params['article']) && $params['article'] < 1) {
			throw new HttpException(422);
		}

		if (isset($params['offer-type']) && $params['offer-type'] < 1) {
			throw new HttpException(404);
		}
	}

	protected function applyBaseQueryConditions(ActiveQuery $query)
	{
		$request = Yii::$app->request;
		$articleId = $request->get('article');
		$tariffId = $request->get('tariff');
		$offerTypeId = $request->get('offer_type');
		$query->andWhere([ArticleTariffs::tableName() . '.is_active' => true]);

		if (empty($articleId) === false) {
			$query->andWhere(['article_id' => $articleId]);
		}

		if (empty($tariffId) === false) {
			$query->andWhere(['tariff_id' => $tariffId]);
		}

		if (empty($offerTypeId) === false) {
			$query->andWhere(['offer_type_id' => $offerTypeId]);
		}

		self::joinWithArticleAndEnsureIsActive($query);
		self::joinWithTariffAndEnsureIsActive($query);
		self::applyOrderAndGrouping($query);
		self::removeSimOnlyFromResults($query);
		TariffSearchAction::applyInSetCondition($query);

		$this->setBaseQuery($query);
	}

	private function cloneBaseQuery()
	{
		return clone $this->baseQuery;
	}

	protected function applyFilters(ActiveQuery $query)
	{
		parent::applyFilters($query);
		$this->applyDeferredPaymentFilter($query);
		$this->applyArticleTariffFilter($query);
		$this->applySwisscomFilter($query);
		//$this->applySetorder($query);
	}

    /*private function applySetorder(ActiveQuery $query)
    {
        $query->rightJoin("charts", 'product_id = article_tariffs.article_id AND charts.set_id = "5"');
        $query->orderBy(['charts.position' => SORT_ASC]);
    }*/

	private function applyArticleTariffFilter(ActiveQuery $query)
	{
		$includedArticleTariffs = Yii::$app->request->get('article_tariff_id_included');
		if (empty($includedArticleTariffs) === false) {
			$query->andWhere(['in', ArticleTariffs::tableName() . '.id', (array)$includedArticleTariffs]);
		}
	}

	private function applySwisscomFilter(ActiveQuery $query)
	{
		if (Yii::$app->request->get('swisscom_id')) {
			$query
				->andWhere(['is not', Article::tableName() . '.swisscom_id', null])
				->andWhere(['!=', Article::tableName() . '.swisscom_id', '']);
		}
	}

	private function applyDeferredPaymentFilter(ActiveQuery $query)
	{
		$deferredPaymentFilter = Yii::$app->request->get('deferred_payment', null);
		if (isset($deferredPaymentFilter)) {
			if($deferredPaymentFilter == 1) {
				$query->andWhere(['>', 'price_article_monthly', 0]);
			} else {
				$query->andWhere(['or', ['price_article_monthly' => null], ['price_article_monthly' => 0]]);
			}
		}
	}

	/**
	 * @param $articleId
	 *
	 * @return ArticleTariffs[]|ActiveRecord
	 */
	private function getDefaultArticleTariffs($articleId)
	{
		$articleGroupId = Article::find()
			->where(['id' => $articleId])
			->select('group_id')
			->scalar(Yii::$app->db);

		$defaultTariffIds = ArticleGroupsTariff::find()
			->where(['article_group_id' => $articleGroupId])
			->select('tariff_id')
			->column(Yii::$app->db);

		$defaultArticleTariffs = $this->cloneBaseQuery()
			->andWhere(['article_id' => $articleId])
			->andWhere(['in', 'tariff_id', $defaultTariffIds])
			->select(ArticleTariffs::tableName() . ".id")
			->column(Yii::$app->db);

		return $defaultArticleTariffs;
	}

	private function checkIfActiveItemExists(): bool
	{
		/** @var ActiveQuery $query */
		$query = ArticleTariffs::find();

		$this->applyBaseQueryConditions($query);
		$this->applyFilters($query);

		return $query->exists();
	}
}
