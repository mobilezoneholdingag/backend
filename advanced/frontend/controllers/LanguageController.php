<?php

namespace frontend\controllers;

use lajax\translatemanager\models\LanguageSource;
use lajax\translatemanager\models\LanguageTranslate;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ConflictHttpException;
use yii\web\ForbiddenHttpException;

/**
 * Class LanguageController
 *
 * @package frontend\controllers
 */
class LanguageController extends Controller
{
	const DEFAULT_LANGUAGE_CODE = 'de-CH';
	const SECURE_DELETE_KEY = 'YA_cEV5W#h*@KFgF';

	public function actionTranslations()
	{
		$languageCode = Yii::$app->request->get('language_code') ?? null;
		$category = Yii::$app->request->get('category') ?? null;

		if (!$category) {
			Yii::$app->response->statusCode = 400;

			return 'Category not provided';
		}

		if (!$languageCode) {
			Yii::$app->response->statusCode = 400;

			return 'Language Code not provided';
		}

		// return language content from db for country and cat
		$dbRows = $this->getTranslationsFromDb($languageCode, $category);

		if (empty($dbRows)) {
			Yii::$app->response->statusCode = 404;

			return '';
		}

		return $dbRows;
	}

	public function actionAddTranslateKey()
	{
		$translation = $this->getPostParameter('translate_value', false);

		$languageSource = $this->findLanguageSourceOrCreate();

		if ($translation !== false) {
			$this->createTranslation($languageSource, $translation);
		}

		return 'OK';
	}

	public function actionRemoveTranslateKey()
	{
		// sec_key is mandatory for security reasons
		$secure_key = $this->getPostParameter('sec_key');

		if ($secure_key !== self::SECURE_DELETE_KEY) {
			throw new ForbiddenHttpException("where ur dogs at?", 403);
		}

		$languageSource = $this->getLanguageSource();

		if (!$languageSource) {
			throw new ConflictHttpException('No language source found.', 409);
		}

		$languageSource->delete();

		return 'OK';
	}

	private function findLanguageSourceOrCreate(): LanguageSource
	{
		$languageSource = $this->getLanguageSource();
		$category = $this->getPostParameter('category');
		$key = $this->getPostParameter('translate_key');

		if ($languageSource && $this->getPostParameter('translate_value', false) === false) {
			throw new ConflictHttpException("Key '$key' already exists", 409);
		}

		if (!$languageSource) {
			$languageSource = $this->createLanguageSource($category, $key);
		}

		return $languageSource;
	}

	/**
	 * @return null|LanguageSource|ActiveRecord
	 */
	private function getLanguageSource()
	{
		$category = $this->getPostParameter('category');
		$key = $this->getPostParameter('translate_key');

		$languageSource = LanguageSource::find()
			->where(['message' => $key])
			->andWhere(['category' => $category])
			->one();

		return $languageSource;
	}

	private function getPostParameter(string $key, bool $isRequired = true)
	{
		$value = Yii::$app->request->post($key, false);

		// ONLY allow non-empty values, if required
		if ($value == false && $isRequired) {
			throw new BadRequestHttpException("Required Parameter '$key' not provided", 400);
		}

		return $value;
	}

	private function createLanguageSource(string $category, string $key): LanguageSource
	{
		$languageSource = new LanguageSource([
			'category' => $category,
			'message' => $key,
		]);
		$languageSource->save();

		return $languageSource;
	}

	private function createTranslation(LanguageSource $languageSource, string $translation)
	{
		$languageId = $languageSource->id;
		$languageTranslate = new LanguageTranslate([
			'id' => $languageId,
			'language' => self::DEFAULT_LANGUAGE_CODE,
		]);
		$languageTranslate->translation = $translation;
		$languageTranslate->save();
	}

	/**
	 * @param $languageCode
	 * @param $category
	 *
	 * @return array
	 */
	protected function getTranslationsFromDb($languageCode, $category)
	{
		$query = new Query;
		$query->select(
			[
				'language_translate.id',
				'language_source.message',
				'language_translate.translation',
			]
		)
			->from(LanguageTranslate::tableName())
			->join(
				'LEFT JOIN',
				LanguageSource::tableName(),
				'language_translate.id = language_source.id'
			)->where(
				[
					'language_translate.language' => $languageCode,
					'language_source.category' => $category,
				]
			);

		$command = $query->createCommand();
		$dbRows = $command->queryAll();

		return $dbRows;
	}
}
