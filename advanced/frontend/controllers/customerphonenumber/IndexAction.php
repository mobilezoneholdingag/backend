<?php

namespace frontend\controllers\customerphonenumber;

use Yii;
use yii\data\ActiveDataProvider;

class IndexAction extends \yii\rest\IndexAction
{
	/**
	 * @return ActiveDataProvider|null
	 */
	public function run()
	{
		$request = Yii::$app->request;
		$customerId = $request->get('customer_id');
		$dataProvider = parent::run();

		if ($customerId) {
			$dataProvider->query->where(['customer_id' => $customerId]);
		}

		if ($dataProvider->getCount() == 0) {
			Yii::$app->response->statusCode = 404;

			return null;
		}

		return $dataProvider;
	}
}
