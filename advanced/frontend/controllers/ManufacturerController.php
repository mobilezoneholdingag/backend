<?php

namespace frontend\controllers;

use common\models\Manufacturer;
use frontend\controllers\manufacturer\IndexAction;
use frontend\controllers\manufacturer\TitleAction;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class ManufacturerController extends ActiveController
{
	public $modelClass = Manufacturer::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		// Custom index action
		$actions['index']['class'] = IndexAction::class;
		$actions['title']['class'] = TitleAction::class;

		return $actions;
	}
}
