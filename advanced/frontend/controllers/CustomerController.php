<?php

namespace frontend\controllers;

use common\models\Customer;
use common\modules\homer\models\mails\PasswordResetTokenMail;
use frontend\controllers\customer\CreateAction;
use frontend\controllers\customer\IndexAction;
use Yii;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class CustomerController extends ActiveController
{
	public $modelClass = '\common\models\Customer';

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete']);

		// Custom index action
		$actions['index']['class'] = IndexAction::class;

		$actions['create']['class'] = CreateAction::class;

		return $actions;
	}

	public function actionLogin()
	{
		$email = Yii::$app->request->post('email');
		$password = Yii::$app->request->post('password');

		// email or password empty
		if(empty($email) || empty($password)) {
			Yii::$app->response->statusCode = 400;
			return ["statusCode" => 400];
		}

		$customer = Customer::findOne(['email' => $email]);

		// no customer not found
		if(empty($customer)) {
			Yii::$app->response->statusCode = 404;
			return ['statusCode' => 404];
		}

		// password invalid
		if(!$customer->validatePassword($password)) {
			Yii::$app->response->statusCode = 401;
			return ['statusCode' => 401];
		}

		// all checks passed
		Yii::$app->response->statusCode = 200;
		return ['statusCode' => 200, 'customerId' => $customer->id];
	}

	public function actionSendPasswordResetMail()
	{
		$data = Yii::$app->request->post();
		/* @var $customer Customer */
		$customer = Customer::findOne([
			'email' => $data['email'],
		]);

		if ($customer) {
			if (!Customer::isPasswordResetTokenValid($customer->password_reset_token)) {
				$customer->generatePasswordResetToken();
			}

			if ($customer->save()) {
				$mail = new PasswordResetTokenMail();
				$mail->setCustomer($customer);
				$mail->setTargetUrl($data['frontendUrl']);

				return $mail->send();
			}
		}

		return false;
	}

	public function actionResetPassword()
	{
		$data = Yii::$app->request->post();
		$token = $data['token'] ?? null;
		$password = $data['password'] ?? null;

		// mandatory params empty
		if (empty($token) || empty($password)) {
			Yii::$app->response->statusCode = 400;

			return ["statusCode" => 400, 'message' => YII_DEBUG ? 'params missing' : ''];
		}

		$customer = Customer::findOne(['password_reset_token' => $data['token']]);

		// no customer not found
		if (empty($customer)) {
			Yii::$app->response->statusCode = 404;

			return ["statusCode" => 404, 'message' => YII_DEBUG ? 'customer not found' : ''];
		}

		$customer->setPasswordHashByPassword($data['password']);
		$customer->password_reset_token = null;

		if (!$customer->save()) {
			Yii::$app->response->statusCode = 400;
			$message = ['customer not saved' => [
				'validation' => $customer->getErrors(),
				'customer' => $customer,
			]];
			return ["statusCode" => 400, 'message' => YII_DEBUG ? ($message) : ''];
		}

		Yii::$app->response->statusCode = 200;

		return ['statusCode' => 200];
	}

	public function actionSavePassportInformation()
	{
		$requestData = Yii::$app->request->post();
		$customerId = $requestData['customer_id'];

		if (!$customerId) {
			Yii::$app->response->statusCode = 404;

			return;
		}

		$customer = Customer::findOne(['id' => $customerId]);

		if (!$customer) {
			Yii::$app->response->statusCode = 404;

			return;
		}

		if (!array_key_exists('nationality', $requestData)
			or !array_key_exists('passport_id', $requestData)
			or !array_key_exists('passport_type', $requestData)
			or !array_key_exists('birthday', $requestData)
		) {
			Yii::$app->response->statusCode = 400;

			return;
		}

		$customer->nationality = $requestData['nationality'];
		$customer->passport_id = $requestData['passport_id'];
		$customer->passport_type = $requestData['passport_type'];
		$customer->birthday = $requestData['birthday'];

		if (!$customer->save()) {
			Yii::$app->response->statusCode = 500;
		}

		return;
	}

	public function actionAllLanguages()
	{
		return Customer::findAllLanguages();
	}
}
