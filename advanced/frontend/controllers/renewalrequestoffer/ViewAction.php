<?php

namespace frontend\controllers\renewalrequestoffer;

use common\modules\renewalRequest\models\RenewalRequestOffers;
use yii\web\HttpException;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		$renewalRequestOffer = RenewalRequestOffers::findOne(['hash' => $id]);

		if (!$renewalRequestOffer) {
			throw new HttpException(404, "Renewal request offer with hash '{$id}' not found!");
		}

		return $renewalRequestOffer;
	}
}
