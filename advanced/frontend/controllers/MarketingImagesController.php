<?php

namespace frontend\controllers;

use common\models\MarketingImages;
use frontend\controllers\marketingimages\ViewAction;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class MarketingImagesController extends ActiveController
{
	public $modelClass = MarketingImages::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		$actions['view']['class'] = ViewAction::class;
		$actions['changed'] = [$this, 'hasChanged'];

		return $actions;
	}
}
