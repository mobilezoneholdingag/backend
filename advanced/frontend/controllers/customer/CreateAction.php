<?php

namespace frontend\controllers\customer;

use common\models\Customer;
use Yii;

class CreateAction extends \yii\rest\CreateAction
{
	public function run()
	{
		/** @var Customer $model */
		$model = parent::run();

		/** @var Customer $existingCustomer */
		$existingCustomer = Customer::find()->where([
			'email' => $model->email,
			'via_newsletter' => 1,
			'password_hash' => null
		])->one();
		if ($existingCustomer) {
			$existingCustomer->load(Yii::$app->getRequest()->getBodyParams());
			$existingCustomer->save();

			return $existingCustomer;
		}

		return $model;
	}
}
