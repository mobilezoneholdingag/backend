<?php

namespace frontend\controllers\customer;

use Yii;
use yii\data\ActiveDataProvider;

class IndexAction extends \yii\rest\IndexAction
{
	/**
	 * @return ActiveDataProvider
	 */
	public function run()
	{
		$request = Yii::$app->request;
		$email = $request->get('email');
		$dataProvider = parent::run();

		if ($email) {
			$dataProvider->query->where(['email' => $email]);
		}

		return $dataProvider;
	}
}
