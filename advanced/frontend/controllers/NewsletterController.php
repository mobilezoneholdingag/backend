<?php

namespace frontend\controllers;

use common\models\NewsletterSubscriber;
use frontend\controllers\newsletter\UpdateAction;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class NewsletterController extends ActiveController
{
	public $modelClass = NewsletterSubscriber::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete']);

		$actions['update']['class'] = UpdateAction::class;

		return $actions;
	}
}
