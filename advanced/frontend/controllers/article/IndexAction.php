<?php

namespace frontend\controllers\article;

use common\models\Article;
use Yii;
use yii\db\ActiveQuery;

class IndexAction extends \yii\rest\IndexAction
{
	public function run()
	{
		$dataProvider = parent::run();

		/** @var ActiveQuery $query */
		$query = $dataProvider->query;
		$query->andWhere([Article::tableName() . '.is_active' => true]);

		$imageOverlayExistsFilter = Yii::$app->request->get('image_overlay_text_exists');
		if ($imageOverlayExistsFilter) {
			$query->andWhere(['not', ['image_overlay_text' => null]]);
			$query->andWhere(['!=', 'image_overlay_text', '']);
			$dataProvider->pagination->pageSize = 100;
		}

		return $dataProvider;
	}
}
