<?php

namespace frontend\controllers\_generic\filter;

use yii\db\ActiveQuery;

class VolumeFilter implements FilterInterface
{
	const FIELD = 'data_volume';

	/** @var string */
	private $selectedValue;
	/** @var bool */
	private $maxValueSelected;
	/** @var string */
	private $comparisonOperator;

	public function __construct(string $comparisonOperator, string $selectedValue = null, bool $maxValueSelected = false)
	{
		$this->selectedValue = $selectedValue;
		$this->maxValueSelected = $maxValueSelected;
		$this->comparisonOperator = $comparisonOperator;
	}

	public function applyTo(ActiveQuery &$query)
	{
		if (!$this->selectedValue) {
			return;
		}

		$filterCondition = [$this->comparisonOperator, self::FIELD, $this->selectedValue];

		if ($this->maxValueSelected) {
			$query->andWhere(['or',
				$filterCondition,
				['IS', self::FIELD, NULL],
			]);
		} else {
			$query->andWhere($filterCondition);
		}
	}
}
