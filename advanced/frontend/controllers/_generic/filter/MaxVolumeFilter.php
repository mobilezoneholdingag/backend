<?php

namespace frontend\controllers\_generic\filter;

class MaxVolumeFilter extends VolumeFilter
{
	public function __construct($selectedValue = null, $maxValueSelected = false)
	{
		parent::__construct('<=', $selectedValue, $maxValueSelected);
	}
}
