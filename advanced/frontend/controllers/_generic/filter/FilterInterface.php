<?php
namespace frontend\controllers\_generic\filter;

use yii\db\ActiveQuery;

interface FilterInterface
{
	public function applyTo(ActiveQuery &$query);
}
