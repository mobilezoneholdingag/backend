<?php

namespace frontend\controllers\_generic\filter;

use common\models\Provider;
use common\models\Tariffs;
use yii\db\ActiveQuery;
use yii\db\Expression;

class FacetInformation
{
	/** @var ActiveQuery */
	private $baseQuery;

	public function __construct(ActiveQuery $baseQuery)
	{
		$this->baseQuery = $baseQuery;
	}

	protected function cloneBaseQuery(): ActiveQuery
	{
		return clone $this->baseQuery;
	}

	public function gatherFacetInfo()
	{
		return [
			'provider' => $this->gatherProviderFacetInfos(),
			'contract_duration' => $this->gatherContractDurationFacetInfos(),
			'data_volume' => $this->gatherDataVolumeFacetInfos(),
			'recommended_for_youth_optional' => $this->gatherYoungPeopleFacetInfos(),
			'deferred_payment_optional' => $this->gatherDeferredPaymentFacetInfos(),
			'price_monthly' => $this->gatherPriceMonthlyFacetInfos(),
		];
	}

	private function gatherProviderFacetInfos()
	{
		$facetQuery = $this->cloneBaseQuery();
		$facetQuery->distinct();
		$facetQuery->select('provider_id');
		return $facetQuery->column();
	}

	private function gatherContractDurationFacetInfos()
	{
		$facetQuery = $this->cloneBaseQuery();
		$facetQuery->distinct();
		$facetQuery->select('contract_duration');
		return $facetQuery->column();
	}

	public function gatherDataVolumeFacetInfos()
	{
		$facetQuery = $this->cloneBaseQuery();
		$facetQuery->groupBy = null;
		$facetQuery->distinct();

		$facetQuery->select(
			new Expression(
				'MIN(data_volume) AS minvolume, 
				MAX(data_volume) AS maxvolume'
			)
		);
		$facetQuery->andWhere(new Expression('data_volume >= 0'));
		$result = $facetQuery->createCommand()->queryOne();

		return [
			'min' => $result['minvolume'],
			'max' => $result['maxvolume'],
		];
	}

	private function gatherYoungPeopleFacetInfos()
	{
		$withYouthQuery = $this->cloneBaseQuery();
		$withoutYouthQuery = $this->cloneBaseQuery();

		$withYouth = $withYouthQuery->andWhere(['young_people' => 1])->exists();
		$withoutYouth = $withoutYouthQuery->andWhere(['young_people' => 0])->exists();

		return ($withYouth && $withoutYouth);
	}

	private function gatherDeferredPaymentFacetInfos()
	{
		$withDeferredQuery = $this->cloneBaseQuery();
		$withoutDeferredQuery = $this->cloneBaseQuery();

		$withDeferred = $withDeferredQuery->andWhere(['>', Tariffs::tableName().'.rate_payment_count', 0])->exists();
		$withoutDeferred = $withoutDeferredQuery->andWhere(['or', ['rate_payment_count' => null], ['rate_payment_count' => 0]])->exists();

		return ($withDeferred && $withoutDeferred);
	}

	private function gatherPriceMonthlyFacetInfos()
	{
		$facetQuery = $this->cloneBaseQuery();
		$facetQuery->groupBy = null;
		$facetQuery->distinct();

		$facetQuery->select(
			new Expression(
				'MIN(' . Tariffs::tableName().'.price_monthly) AS minprice, 
				MAX(' . Tariffs::tableName().'.price_monthly) AS maxprice'
			)
		);
		$facetQuery->andWhere(new Expression(Tariffs::tableName().'.price_monthly > 0'));
		$result = $facetQuery->createCommand()->queryOne();

		return [
			'min' => $result['minprice'],
			'max' => $result['maxprice'],
		];
	}
}
