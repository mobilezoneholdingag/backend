<?php

namespace frontend\controllers\_generic\filter;

class MinVolumeFilter extends VolumeFilter
{
	public function __construct($selectedValue = null, $maxValueSelected = false)
	{
		parent::__construct('>=', $selectedValue, $maxValueSelected);
	}
}
