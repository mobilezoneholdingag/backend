<?php

namespace frontend\controllers\_generic\filter;

use yii\db\ActiveQuery;

class RangeFilter implements FilterInterface
{
	/** @var string */
	private $field;
	/** @var int|float|string */
	private $to;
	/** @var int|float|string */
	private $from;

	public function __construct(string $field, $from = null, $to = null)
	{
		$this->field = $field;
		$this->from = $from;
		$this->to = $to;
	}

	public function applyTo(ActiveQuery &$query)
	{
		if (!is_null($this->from)) {
			$query->andWhere(['>=', $this->field, $this->from]);
		}

		if (!is_null($this->to)) {
			$query->andWhere(['<=', $this->field, $this->to]);
		}
	}
}
