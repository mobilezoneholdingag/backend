<?php

namespace frontend\controllers\_generic;

use common\models\Provider;
use common\models\Tariffs;
use frontend\controllers\_generic\filter\FacetInformation;
use frontend\controllers\_generic\filter\MaxVolumeFilter;
use frontend\controllers\_generic\filter\MinVolumeFilter;
use frontend\controllers\_generic\filter\RangeFilter;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\rest\IndexAction;
use yii\web\HttpException;

abstract class BaseTariffSearchAction extends IndexAction
{
	const SIMPLE_FILTER_PROPERTIES = [];

	const VALID_PARAMETERS = [
		'price_monthly',
		'data_volume',
		'recommended_for_youth',
		'deferred_payment',
		'provider_id_excluded',
		'article_id_excluded',
		'provider',
		'contract_duration',
		'offer_type',
	];

	/** @var ActiveQuery */
	protected $baseQuery;
	/** @var FacetInformation */
	protected $facetInformation;
	/** @var ArrayDataProvider */
	protected $dataProvider;

	protected function setBaseQuery(ActiveQuery $query)
	{
		$this->baseQuery = clone $query;
		$this->facetInformation = new FacetInformation($this->baseQuery);
	}

	abstract protected function applyBaseQueryConditions(ActiveQuery $query);

	public function run()
	{
		if ($this->checkAccess) {
			call_user_func($this->checkAccess, $this->id);
		}

		$this->validateParameters();

		$this->dataProvider = $this->prepareDataProvider();

		/* @var $modelClass \yii\db\ActiveRecord */
		$modelClass = $this->modelClass;

		/** @var ActiveQuery $query */
		$query = $modelClass::find();

		$this->applyBaseQueryConditions($query);
		$this->applyFilters($query);
		$this->setLimit($query);

		$this->dataProvider->allModels = $query->all();
	}

	protected function validateParameters()
	{
		$parameters = Yii::$app->request->get();
		$this->validateHasValidParameter($parameters);

		if (isset($parameters['provider'])) {
			$providerExists = Provider::find()->where(['id' => $parameters['provider'], 'status' => 1])->exists();
			if (!$providerExists) {
				throw new HttpException(422);
			}
		}
	}

	protected function validateHasValidParameter(array $parameters = [])
	{
		$parameterKeys = array_keys($parameters);
		$hasAtLeastOneValidParameter = false;

		foreach ($parameterKeys as $parameter) {
			if (in_array($parameter, static::VALID_PARAMETERS)) {
				$hasAtLeastOneValidParameter = true;
				break;
			}
		}

		if (!$hasAtLeastOneValidParameter) {
			throw new InvalidParamException("Invalid search Parameter(s)!", 400);
		}
	}

	protected function applyFilters(ActiveQuery $query)
	{
		$this->applySimpleFilters($query);
		$this->applyPriceFilter($query);
		$this->applyVolumeFilter($query);
		$this->applyYoungPeopleFilter($query);
		$this->applyExcludedProviderFilter($query);
		$this->applyExcludedArticleFilter($query);
	}

	private function applySimpleFilters(ActiveQuery $query)
	{
		foreach (static::SIMPLE_FILTER_PROPERTIES as $filterableProperty => $parameterName) {
			$filterablePropertyValue = Yii::$app->request->get($parameterName);

			if (empty($filterablePropertyValue) === false) {
				$query->andWhere(['in', $filterableProperty, (array)$filterablePropertyValue]);
			}
		}
	}

	private function applyPriceFilter(ActiveQuery $query)
	{
		$price = Yii::$app->request->get('price_monthly');

		(new RangeFilter(Tariffs::tableName().'.price_monthly', $price['from'] ?? null, $price['to'] ?? null))->applyTo($query);
	}

	private function applyVolumeFilter(ActiveQuery $query)
	{
		$volumeFilter = Yii::$app->request->get('data_volume');

		if (!isset($volumeFilter)) {
			return;
		}

		$maxVolume = $this->facetInformation->gatherDataVolumeFacetInfos()['max'];
		$isMaxVolumeSelected = $volumeFilter['to'] == $maxVolume;

		(new MinVolumeFilter($volumeFilter['from'] ?? null, $isMaxVolumeSelected))->applyTo($query);
		(new MaxVolumeFilter($volumeFilter['to'] ?? null, $isMaxVolumeSelected))->applyTo($query);
	}

	private function applyYoungPeopleFilter(ActiveQuery $query)
	{
		$youngPeopleFilter = Yii::$app->request->get('recommended_for_youth');

		if (isset($youngPeopleFilter)) {
			if($youngPeopleFilter == 1) {
				$query->andWhere(['young_people' => 1]);
			} else {
				$query->andWhere(['young_people' => 0]);
			}
		}
	}

	private function applyExcludedProviderFilter(ActiveQuery $query)
	{
		$excludedProviders = Yii::$app->request->get('provider_id_excluded');

		if (empty($excludedProviders) === false) {
			$query->andWhere(['not in', Tariffs::tableName() . '.provider_id', (array)$excludedProviders]);
		}
	}

	private function applyExcludedArticleFilter(ActiveQuery $query)
	{
		$excludedArticles = Yii::$app->request->get('article_id_excluded');

		if (empty($excludedArticles) === false) {
			$query->andWhere(['not in', 'article_id', (array)$excludedArticles]);
		}
	}

	protected function prepareDataProvider()
	{
		if ($this->prepareDataProvider !== null) {
			return call_user_func($this->prepareDataProvider, $this);
		}

		return new ArrayDataProvider();
	}

	protected function setLimit(ActiveQuery $query)
	{
		$this->dataProvider->totalCount = $query->count();
		$this->dataProvider->pagination->page = Yii::$app->request->get('page', 1) - 1;
		$query->offset($this->dataProvider->pagination->offset)->limit($this->dataProvider->pagination->limit);
		$this->dataProvider->pagination->page = 0;
	}
}
