<?php

namespace frontend\controllers;

use common\models\Shop;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\rest\ActiveController;

class ShopController extends ActiveController
{
	public $modelClass = '\common\models\Shop';

	/**
	 * @return string
	 */
	public static function getTodayMysqlFormat(): string
	{
		return (new \DateTime('now'))->format('Y-m-d');
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['update'], $actions['create']);

		$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

		$actions['view']['findModel'] = [$this, 'findModel'];

		return $actions;
	}

	public function prepareDataProvider()
	{
		$limit = Yii::$app->request->get('limit', false);

		$query = static::getBaseQuery();

		$provider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => $limit,
			],
		]);

		return $provider;
	}

	public function findModel($id)
	{
		return static::getBaseQuery()->andWhere(['id' => $id])->one();
	}

	public static function getBaseQuery(): ActiveQuery
	{
		$query = Shop::find();

		self::applyClosureDateCondition($query);
        self::applyOpeningDateCondition($query);
		self::applyDistanceCondition($query);

		return $query;
	}

	public static function applyClosureDateCondition(Query $query)
	{
		$field = "`closure_date`";
		$today = self::getTodayMysqlFormat();
		$query->andWhere(['OR', "{$field} > '{$today}'", "{$field} IS NULL"]);
	}

	public static function applyOpeningDateCondition(Query $query)
	{
		$field = "`opening_date`";
		$today = self::getTodayMysqlFormat();
		$query->andWhere(['AND', "{$field} <= '{$today}'", "{$field} IS NOT NULL"]);
	}

	public static function applyDistanceCondition(Query $query)
	{
		$latitude = Yii::$app->request->get('latitude');
		$longitude = Yii::$app->request->get('longitude');

		if ($latitude !== null && $longitude !== null) {
			$query
				->addSelect(
					["*", "sqrt(pow(:latitude-[[latitude]],2)+pow(:longitude-[[longitude]],2)) AS distance"]
				)
				->orderBy(['distance' => SORT_ASC])
				->addParams([
					'latitude' => $latitude,
					'longitude' => $longitude,
				]);
		}
	}
}
