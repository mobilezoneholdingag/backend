<?php

namespace frontend\controllers;

use common\modules\insurances\models\ArticleInsurance;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class ArticleInsuranceController extends ActiveController
{
	public $modelClass = ArticleInsurance::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		return $actions;
	}
}
