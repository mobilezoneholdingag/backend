<?php

namespace frontend\controllers;

use common\modules\payment\exceptions\PreconditionsFailedException;
use common\modules\payment\helper\Method;
use common\modules\payment\models\PaymentMethodContext;
use Yii;
use yii\rest\Controller;
use yii\web\HttpException;

class PaymentController extends Controller
{
	/**
	 * This method returns all available payment methods to be shown in the frontend in a structure like shown below.
	 *
	 * <pre>
	 *  [
	 *      {
	 *          "name": "account-purchase",
	 * 	        "payload": {
	 * 	            "account-purchase-id": 18
	 * 	        }
	 *      },
	 *      {
	 *          "name": "credit-card"
	 *      },
	 *      {
	 *          "name": "pf-e-finance"
	 *      },
	 *      {
	 *          "name": "pf-card"
	 *      }
	 *  ]
	 * </pre>
	 *
	 * Please note that all payment methods are optional in theory, although only account-purchase is really dynamic
	 * right now.
	 *
	 * @return array
	 * @throws HttpException
	 */
	public function actionMethods()
	{
		$methods = [];

		$total = Yii::$app->request->post('total');

		if (!isset($total)) {
			throw new HttpException(400, "Missing argument 'total'!");
		}

		foreach (Method::METHODS as $name) {
			try {
				array_push($methods, $this->createMethodEntry($name, $total));
			} catch (PreconditionsFailedException $e) {
				// don't display payment method
			}
		}

		return $methods;
	}

	/**
	 * Helper method to create a single payment method entry in the response array.
	 *
	 * @param string $name
	 * @param float $totalPrice
	 * @param array|null $payload
	 *
	 * @return array
	 */
	private function createMethodEntry(string $name, float $totalPrice = 0.0, array $payload = []) : array
	{
		$arr = ['name' => $name];
		$context = new PaymentMethodContext($name);
		$context->checkPreconditions();
		$arr['payload'] = array_merge($payload, [
			'price' => $context->calculateFee($totalPrice)
		]);
		return $arr;
	}
}
