<?php

namespace frontend\controllers\tariff;

use common\models\Tariffs;
use common\modules\set\models\Chart;
use common\modules\set\models\Sets;
use frontend\controllers\_generic\BaseTariffSearchAction;
use Yii;
use yii\db\ActiveQuery;

class SearchAction extends BaseTariffSearchAction
{
	const SIMPLE_FILTER_PROPERTIES = [
		'provider_id' => 'provider',
		'contract_duration' => 'contract_duration',
		'portable_number' => 'porting',
	];

	public function run()
	{
		parent::run();
		$addFacets = Yii::$app->request->get('facets');

		return [[
			'facets' => $addFacets ? $this->facetInformation->gatherFacetInfo() : [],
			'items' => $this->dataProvider->getModels(),
			'total' => $this->dataProvider->totalCount,
		]];
	}

	protected function validateParameters()
	{
		return true;
	}

	protected function applyBaseQueryConditions(ActiveQuery $query)
	{
		self::applyBaseConditions($query);
		self::applyInSetCondition($query);
		$this->setBaseQuery($query);
	}

	public static function applyBaseConditions(ActiveQuery &$query)
	{
		self::applyOfferTypeConditions($query);
		self::applyArticleTariffsExistCondition($query);

		$query
			->andWhere([Tariffs::tableName() . '.is_active' => true])
			->andWhere([Tariffs::tableName() . '.is_active_erp' => true])
			->andWhere([Tariffs::tableName() . '.is_b2b_allowed' => false])
		;
		$query->orderBy(['price_monthly' => SORT_ASC]);
	}

	private static function applyOfferTypeConditions(ActiveQuery &$query)
	{
		$offerTypeId = Yii::$app->request->get('offer_type');
		if (empty($offerTypeId) === false) {
			$query->andWhere(['offer_type_id' => $offerTypeId]);
		}
	}

	private static function applyArticleTariffsExistCondition(ActiveQuery &$query)
	{
		$query->andWhere("EXISTS(
			SELECT tariff_id FROM article_tariffs 
			WHERE tariff_id = tariffs.id 
			AND article_tariffs.is_active_erp = TRUE
			AND article_tariffs.is_active = TRUE
		)");
	}

	public static function applyInSetCondition(ActiveQuery &$query)
	{
		$tariffIds = Sets::find()
			->joinWith('chart')
			->select(Chart::tableName().'.product_id')
			->where(['system_title' => 'tariffs'])
			->column()
		;
		$query->andWhere(['IN', Tariffs::tableName() . '.id', $tariffIds]);
	}
}
