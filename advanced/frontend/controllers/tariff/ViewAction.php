<?php

namespace frontend\controllers\tariff;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class ViewAction extends \yii\rest\ViewAction
{
	public function run($id)
	{
		/** @var Tariffs|ActiveRecord $tariff */
		$tariff = parent::run($id);

		if (!$tariff->isDetailPageViewable()) {
			Yii::$app->response->statusCode = 400;

			return 'Sorry, this tariff is no longer available!';
		}

		return $tariff->toArray();
	}
}
