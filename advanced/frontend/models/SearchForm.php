<?php

namespace frontend\models;

use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SearchForm extends Model
{
	public $search;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['search'], 'required'],
			[['search',], 'string',],
		];
	}

	public function attributeLabels()
	{
		return [
			'search' => 'Suchen',
		];
	}
}
