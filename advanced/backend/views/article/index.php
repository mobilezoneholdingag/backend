<?php

use common\models\Stock;
use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Article */

$this->title = Yii::t('app', 'Devices');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

	<?php if (Yii::$app->user->can('createArticles')) { ?>
	<p>
		<?= Html::a(Yii::t('app', 'Create Article'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php } ?>
	<div class="row">
		<div class="col-md-2">
			<div class="inactiveLegendInformation"></div>
			= inaktiv
		</div>
		<div class="col-md-2">
			<?= PageSize::widget([
				'pageSizeParam' => 'per-page',
			]); ?>
		</div>
	</div>

	<?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'rowOptions' => function ($model) {
			return ($model->is_active != true) ? ['class' => 'inactiveRow'] : [];
		},
		'columns' => [
			'id',
			'mat_no',
			[
				'attribute' => 'manufacturerTitle',
				'format' => 'html',
				'value' => function ($model) {
					if ($model->manufacturer) {
						return Html::a($model->manufacturerTitle, Url::to(['manufacturer/view', 'id' => $model->manufacturer->id]));
					}
					return null;
				}
			],
			'productCategoryLabel',
			[
				'attribute' => 'groupTitle',
				'format' => 'html',
				'value' => function ($model) {
					if ($model->group_id)
						return Html::a($model->groupTitle, Url::to(['articlegroups/view', 'id' => $model->group_id]));
				}
			],
			[
				'attribute' => 'product_name',
				'format' => 'html',
				'value' => function ($model) {
					return Html::a($model->product_name, Url::to(['article/view', 'id' => $model->id]));
				}
			],
			'price_sale:currency',
			'memory',
			'color_label',
			'stockQuantity',
            [
                'attribute' => 'description_1',
                'format' => 'html',
                'value' => function($model) {

                    if(!$model->isAccessory()) {
                        return '<span class="label label-warning">Kein Accessory</span>';
                    }

                    if(empty($model->description_1)) {
                        return '<span class="label label-danger">LEER!</span>';
                    }

                    return '<span class="label label-success">Vorhanden</span>';

                }
            ],
			[
				'label' => Yii::t('app', 'Availability'),
				'value' => function ($model) {
					if (isset($model->stock->status)) {
						return Yii::t('app', $model->stock->status);
					} else {
						return Yii::t('app', Stock::STATUS_NOT_IN_STOCK);
					}
				}
			],
			'is_active:boolean',
			'is_active_erp:boolean',
			'updated_at:datetime',
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => Yii::$app->user->can('createArticles') ? '{view} {update} {delete}' : '{view} {update}',
			],
		],
	]); ?>

</div>
