<?php

use common\models\ArticleDetails;
use common\models\ArticleDetailsGroup;
use common\models\ArticleGroups;
use components\i18n\widget\TabWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use backend\forms\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $form ActiveForm */

$this->registerJsFile('/js/articles/imageSort.js');
?>

<?= $this->render(Yii::$app->params['flashMessagesPath']); ?>

<div class="article-form">

	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<div class="form-group">
		<label><input type="checkbox" disabled="disabled" value="1" <?= $model->is_active_erp ? 'checked="checked"' : '' ?>> <?= Yii::t('app', 'Is Active Erp') ?></label>
	</div>

	<?= $form->field($model, 'is_active')->checkbox() ?>

	<?= $form->field($model, 'show_technical_information')->checkbox() ?>

	<?= $form->field($model, 'id')->textInput(['disabled' => 'disabled']) ?>

	<?= $form->field($model, 'mat_no')->textInput(['disabled' => 'disabled']) ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => ['product_name',],
			'editorOptions' => [
				'maxlength' => 80,
			],
			'inputType' => TabWidget::TYPE_TEXTFIELD,
		]
	); ?>


	<?php
	if ($model->isAccessory()) {
		echo TabWidget::widget(
			[
				'model' => $model,
				'form' => $form,
				'fieldsToRender' => [
					'description_1',
					'description_2',
				]
			]
		);
	}
	?>

	<?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(ArticleGroups::find()->orderBy('manufacturer_id')->all(), 'id', 'fullName'), ['prompt' => '- ' . Yii::t('app', 'please select') . ' -']) ?>

	<?= $form->field($model, 'memory')->textInput(['maxlength' => 80]) ?>

	<?= $form->field($model, 'color')->textInput(['maxlength' => 80]) ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => ['color_label',],
			'editorOptions' => [
				'maxlength' => 40,
			],
			'inputType' => TabWidget::TYPE_TEXTFIELD,
		]
	); ?>

	<?= $form->field($model, 'price_sale')->textInput(['maxlength' => 16, 'disabled' => 'disabled']) ?>

	<?= $form->field($model, 'price_feed')->textInput(['maxlength' => 9]) ?>

	<?= $form->field($model, 'price_discounted')->textInput(['maxlength' => 9]) ?>

	<?= $form->field($model, 'price_strike')->textInput(['maxlength' => 9]) ?>

    <?= $form->field($model, 'bonus_id')->checkbox([]) ?>

	<?= $form->field($model, 'stockQuantity')->textInput(['disabled' => 'disabled']) ?>

	<?= $form->field($model, 'override_positive_stock_to_reorder')->checkbox() ?>

    <?= $form->field($model, 'custom_stock')->dropDownList(\common\models\Stock::stockValues()) ?>

	<?= $form->field($model, 'nextDeliveryDate')->textInput(['disabled' => 'disabled']) ?>

	<?= $form->field($model, 'nextDeliveryAmount')->textInput(['disabled' => 'disabled']) ?>

	<?= $form->field($model, 'deliveryStatusTitle')->textInput(['disabled' => 'disabled']) ?>
    
	<?= $form->field($model, 'simcard_type')->dropDownList(Yii::$app->params['simCardTypes']) ?>

	<?= $form->field($model, 'manufacturer_article_id')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'swisscom_id')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'mpn')->textInput(['maxlength' => 64]) ?>

    <?php
    if($model->isSimcard()) {
        ?>
        <?= TabWidget::widget(
            [
                'model' => $model,
                'form' => $form,
                'fieldsToRender' => ['checkout_hint',],
                'editorOptions' => [
                    'rows' => 6,
                ],
                'inputType' => TabWidget::TYPE_TEXTAREA,
            ]
        ); ?>
        <?php
    }
    ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => ['delivery_includes',],
			'editorOptions' => [
				'rows' => 6,
			],
			'inputType' => TabWidget::TYPE_TEXTAREA,
		]
	); ?>

	<div class="form-group">
		<?= TabWidget::widget(
			[
				'model' => $model,
				'form' => $form,
				'fieldsToRender' => [
					'image_overlay_text',
				],
				'editorOptions' => [
					'maxlength' => 255,
				],
				'inputType' => TabWidget::TYPE_TEXTFIELD,
			]
		); ?>
	</div>

	<span class="save_flash" style="display: none"><?= Yii::t('app', 'saved') ?></span>

	<?=
		$form
			->field($model, 'images[]', ['selectors' => ['input' => 'input[class="dz-hidden-input"]']])
			->dropfileInput(['image' => true, 'multiple' => true, 'files' => 8, 'accept' => 'image/*', 'value' => '']);
	?>

	<h2 id="detailsHeadline" class="btn btn-success btn-lg">Produktdetails</h2>

	<div id="detailsField">
		<?php
		/** @var $detail common\models\ArticleDetails */
		foreach (ArticleDetailsGroup::getGroups() as $group):?>
			<fieldset>
				<legend><?= $group->title ?></legend>
				<?php foreach ($group->details as $detail): ?>
					<div class="col-sm-3">
						<?php if ($detail->isBool()): ?>
							<?= $form->field($model->articleDetailsValues, 'detail_' . $detail->detail_id)->checkbox([], false)->label($detail->detail_name) ?>
						<?php else: ?>
							<?= $form->field($model->articleDetailsValues, 'detail_' . $detail->detail_id)
								->textInput(['maxlength' => 64,
									'disabled' => (in_array($detail->detail_id, [
										ArticleDetails::DETAIL_INTERNAL_MEMORY, ArticleDetails::DETAIL_DISPLAY_PPI
									])) ? 'disabled' : false])
								->label($detail->detail_name) ?>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</fieldset>
			<?php
		endforeach; ?>

		<div class="form-group cautionArea">
			<?= Html::label(Yii::t('app', 'save details for all variations'), 'saveDetailsForAllVariations-checkbox'); ?>
			<?= Html::checkbox('saveDetailsForAllVariations', false, ['id' => 'saveDetailsForAllVariations-checkbox']); ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end() ?>

</div>
