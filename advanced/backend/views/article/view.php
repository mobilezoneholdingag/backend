<?php

use common\models\Stock;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = $model->product_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">


	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?php if (Yii::$app->user->can('createArticles')) { ?>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
		<?php } ?>
	</p>

	<h1><?= Html::encode($this->title) ?></h1>

	<?php
		foreach ($model->images->images ?? [] as $image) {
			echo Html::img($image, ['class' => 'product_image', 'height' => '100']);
		}
	?>

	<?php ?>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'is_active:boolean',
			'show_technical_information:boolean',
			'id',
			'mat_no',
			'product_name',
			['attribute' => 'description_1', 'format' => 'raw'],
			['attribute' => 'description_2', 'format' => 'raw'],
			'stockQuantity',
			'override_positive_stock_to_reorder:boolean',
			'articleGroup.title',
			'color',
			'color_label',
			'shortName',
			'price_sale:currency',
			'price_feed',
			'price_discounted:currency',
			'price_strike:currency',
            'bonus_id:boolean',
			'productCategory.label',
			[
				'label' => Yii::t('app', 'Availability'),
				'value' => isset($model->stock->status) ?
					Yii::t('app', $model->stock->status)
					: Yii::t('app', Stock::STATUS_NOT_IN_STOCK)
			],
			[
				'attribute' => 'simcard_type',
				'value' => Yii::$app->params['simCardTypes'][$model->simcard_type],
			],
			'swisscom_id',
			'mpn',
			[
				'label' => Yii::t('app', 'Operating System'),
				'value' => (($model->operatingSystem) ? $model->operatingSystem->title . ' ' . $model->operatingSystem->version : ''),
			],
			'memory',
			'manufacturer_article_id',
			'image_overlay_text',
			'updated_at:datetime',
            [
                'label' => Yii::t('app', 'Custom Stock'),
                'value' => Yii::t('app', $model->custom_stock)
            ]

		],
	]) ?>

</div>
