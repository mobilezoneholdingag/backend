<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DeliveryStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-status-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= TabWidget::widget([
		'model' => $model,
		'form' => $form,
		'fieldsToRender' => [
			'label',
		],
		'inputType' => TabWidget::TYPE_TEXTFIELD,
		'editorOptions' => ['maxlength' => true],
	]); ?>


	<?= $form->field($model, 'css_class')->dropDownList(['available' => 'available', 'pending' => 'pending']) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
