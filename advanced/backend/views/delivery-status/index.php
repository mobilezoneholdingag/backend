<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Delivery Status';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-status-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create Delivery Status'), ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a(Yii::t('app', 'Overwrite Delivery Status'), ['update-delivery-status'], ['class' => 'btn btn-danger']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'label',
			'css_class',
			'due',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
