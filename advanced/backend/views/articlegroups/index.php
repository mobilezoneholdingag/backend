<?php

use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ArticleGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Article Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-groups-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a(Yii::t('app', 'Create new device group', [
			'modelClass' => 'Article Groups',
		]), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<div class="row">
		<div class="col-md-2">
			<?= PageSize::widget([
				'pageSizeParam' => 'per-page',
			]); ?>
		</div>
	</div>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'columns' => [
			'id',
			'short_name',
			'title',
			[
				'attribute' => 'accessorySetTitle',
				'format' => 'html',
				'value' => function ($model) {
					return Html::a($model->accessorySetTitle, Url::to(['sets/view', 'id' => $model->accessory_set_id]));
				}
			],
			[
				'attribute' => 'manufacturerTitle',
				'format' => 'html',
				'value' => function ($model) {
					return Html::a($model->manufacturerTitle, Url::to(['manufacturer/view', 'id' => $model->manufacturer_id]));
				}
			],
            [
                'attribute' => 'description_1',
                'format' => 'html',
                'value' => function($model) {
                    if(empty($model->description_1)) {
                        return '<span class="label label-danger">LEER!</span>';
                    }

                    return '<span class="label label-success">Vorhanden</span>';

                }
            ],
			[
				'attribute' => Yii::t('app', 'Insurances'),
				'format' => 'html',
				'value' => function ($model) {
					$insurances = [];
					foreach ($model->insurances as $insurance) {
						$insurances[] = Html::a(
							$insurance->product_name,
							Url::to(['article/view', 'id' => $insurance->id])
						);
					}
					return Html::ul($insurances, ['encode' => false, 'style' => 'white-space: nowrap;']);
				}
			],
			[
				'attribute' => Yii::t('app', 'Variations'),
				'format' => 'html',
				'value' => function ($model) {
					$variations = [];
					foreach ($model->articles as $article) {
						$variations[] = Html::a(
							$article->color_label . ', ' . $article->memory . ' GB',
							Url::to(['article/view', 'id' => $article->id]),
							($model->isTopVariation($article)) ? ['style' => 'font-weight: bold;'] : []
						);
					}
					return Html::ul($variations, ['encode' => false, 'style' => 'white-space: nowrap;']);
				}
			],
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
