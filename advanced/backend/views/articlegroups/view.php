<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleGroups */

$this->title = Yii::t('app', 'Article Groups');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-groups-view">

	<h1><?= Html::encode($model->title) ?></h1>

	<?php
	if ($model->articles) {
		foreach ($model->articles as $article) {

			$seoImage6 = 'no_image';
			$seoImage7 = 'no_image';
			$seoImage8 = 'no_image';

			$articleImage6 = $article->images[6];
			if ($articleImage6) {
				$seoImage6 = (string) $articleImage6;
			}

			$articleImage7 = $article->images[7];
			if ($articleImage7) {
				$seoImage6 = (string) $articleImage7;
			}

			$articleImage8 = $article->images[8];
			if ($articleImage8) {
				$seoImage6 = (string) $articleImage8;
			}

			if (preg_match('/no_image/', $seoImage6)) {
				echo "6 SEO-Image fehlt Article-ID = " . $article->id . "\n";
			}

			if (preg_match('/no_image/', $seoImage7)) {
				echo "7 SEO-Image fehlt Article-ID = " .  $article->id . "\n";
			}

			if (preg_match('/no_image/', $seoImage8)) {
				echo "8 SEO-Image fehlt Article-ID = " .  $article->id . "\n";
			}
			echo "<br/>";
		}

		echo "<br/>";
		echo Html::img($seoImage6, ['class' => 'product_image','width'=>'200']);
		echo Html::img($seoImage7, ['class' => 'product_image','width'=>'200']);
		echo Html::img($seoImage8, ['class' => 'product_image','width'=>'200']);

	}
	?>

	<fieldset>
		<h3><?= Yii::t('app', 'devices of this group') ?></h3>

		<?php
		$variations = [];
		foreach ($model->articles as $article) {
			$variations[] = Html::a(
				$article->color_label . ', ' . $article->memory . ' GB',
				Url::to(['article/view', 'id' => $article->id]),
				(isset($model->topVariation->id) && $model->topVariation->id == $article->id) ? ['style' => 'font-weight: bold;'] : []
			);
		}
		echo Html::ul($variations, ['encode' => false, 'style' => 'white-space: nowrap;']);
		?>
	</fieldset>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'short_name',
			'title',
			[
				'attribute' => 'top_variation_id',
				'value' => ($model->topVariation) ? $model->topVariation->uniqueTitle : Yii::t('app', 'not selected'),
			],
			'footnote_text',
			'frontend_text:raw',
			'article_feed_description',
			'accessorySetTitle',
		],
	]) ?>

</div>
