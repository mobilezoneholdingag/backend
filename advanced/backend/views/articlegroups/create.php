<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ArticleGroups */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Article Groups',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-groups-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
		'dataProvider' => $dataProvider,
	]) ?>

</div>
