<?php

use common\models\Article;
use common\models\ArticleGroupsSim;
use common\models\ArticleGroupsTariff;
use common\models\Manufacturer;
use common\models\OperatingSystems;
use common\models\ProductCategory;
use common\modules\platform\models\Platform;
use common\modules\set\models\Sets;
use components\i18n\widget\TabWidget;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleGroups */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $articleGroupTariffProvider \yii\data\ArrayDataProvider */

?>

<?= $this->render(Yii::$app->params['flashMessagesPath']); ?>

<div class="article-groups-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'),
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?= $form->field($model, 'title')->textInput(['maxlength' => 64]) ?>

	<?= $form->field($model, 'short_name')->textInput(['maxlength' => 64]) ?>

	<?= $form->field($model, 'is_bundle')->checkbox() ?>

	<?= $form->field($model, 'product_category_id')->dropDownList(
		ArrayHelper::map(ProductCategory::find()->select(['id', 'type'])->asArray()->all(), 'id', 'type'),
		['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	) ?>

	<?= $form->field($model, 'os_id')->dropDownList(
		ArrayHelper::map(
			OperatingSystems::find()->select(['id', 'platform_id', 'title', 'version'])->asArray()->all(),
			'id',
			function($model) {
				$platformName = Platform::findOne($model['platform_id'])->name;

				return "{$platformName} {$model['title']} {$model['version']}";
			}
		),
		['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	) ?>

	<?php # $form->field($model, 'tariff_set_id')->dropDownList(ArrayHelper::map(Sets::find()->orderBy('title')->where(['type' => Sets::TYPE_TARIFF])->all(), 'id', 'title'), ['prompt' => '- ' . Yii::t('app', 'please select') . ' -']) ?>

	<?= $form->field($model, 'accessory_set_id')->dropDownList(
		ArrayHelper::map(
			Sets::find()->where(['type' => Sets::TYPE_ARTICLE])->select(['id', 'title'])->asArray()->all(),
			'id',
			'title'
		),
		['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	)->label(Yii::t('app', $model->getAttributeLabel('accessory_set'))) ?>

	<?= $form->field($model, 'manufacturer_id')->dropDownList(
		ArrayHelper::map(
			Manufacturer::find()->orderBy('title')->select(['id', 'title'])->asArray()->all(),
			'id',
			'title'
		),
		['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	) ?>

	<?= $form->field($model, 'top_variation_id')->dropDownList(
		ArrayHelper::map(
			Article::find()->where(['group_id' => $model->id])->all(),
			'id',
			'uniqueTitle'
		),
		['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	) ?>

	<?= $form->field($model, 'preorder')->checkbox() ?>

	<?= TabWidget::widget([
		'model' => $model,
		'form' => $form,
		'fieldsToRender' => [
			'footnote_text',
		],
		'inputType' => TabWidget::TYPE_TEXTFIELD
	]); ?>

	<?= TabWidget::widget([
		'model' => $model,
		'form' => $form,
		'fieldsToRender' => [
			'article_feed_description',
		],
		'inputType' => TabWidget::TYPE_TEXTFIELD
	]); ?>

	<div id="detailsFieldTariffSettings">
		<fieldset class="tariff_related_input_fields">
			<legend><?= Yii::t('app', 'tariff_settings_headline') ?></legend>
			<?php if (!$model->id): ?>
				<div class="well well-lg"><?= Yii::t('app', 'default_provider_save_hint') ?></div>
			<?php else: ?>
				<?= GridView::widget(
					[
						'dataProvider' => $articleGroupTariffProvider,
						'columns' => [
							'title',
							[
								'attribute' => 'tariffs',
								'value' => function($value) use ($model) {
									$providerId = $value['id'];
									return Html::dropDownList(
										"ArticleGroups[articleGroupTariffs][$providerId]",
										ArticleGroupsTariff::find()
											->where([
												'provider_id' => $providerId,
												'article_group_id' => $model->id,
											])
											->select(['tariff_id'])
											->scalar() ?? null,
										$value['tariffs'],
										['prompt' => '- '.Yii::t('app', 'default_provider_hint').' -']
									);
								},
								'format' => 'raw',
							]
						],
					]
				); ?>
			<?php endif; ?>
		</fieldset>
	</div>

	<!--h2 id="detailsHeadlineSimSettings" class="btn btn-success btn-lg">SIM card settings</h2-->
	<div id="detailsFieldSimSettings">
		<fieldset class="simcard_related_input_fields">
			<legend>SIM card settings - overwrite provider settings here</legend>
			<?php
				$simCards = ArrayHelper::map(
					Article::getSimCards()->select(['id', 'product_name'])->asArray()->all(),
					'id',
					function($array) {
						return "{$array['product_name']} [{$array['id']}]";
					}
				);

				echo GridView::widget(
					[
						'dataProvider' => $dataProvider,
						'columns' => [
							[
								'attribute' => Yii::t('app', 'Provider'),
								'value' => 'title',
							],
							[
								'attribute' => Yii::t('app', 'SIM card'),
								'value' => function($provider) use ($model, $simCards) {
									return Html::dropDownList(
										"ArticleGroupSim[$provider->id]",
										ArticleGroupsSim::find()->where([
											'provider_id' => $provider->id,
											'article_group_id' => $model->id,
										])
										->select(['simcard_id'])
										->scalar() ?? null,
										$simCards,
										['prompt' => '- ' . Yii::t('app', 'default_sim_provider_hint') . ' -']
									);
								},
								'format' => 'raw',
							],
						],
					]
				);
			?>
		</fieldset>
	</div>

	<!--h2 id="detailsHeadlineDesc" class="btn btn-success btn-lg">Beschreibung</h2-->
	<div id="detailsFieldDesc">
		<?= TabWidget::widget([
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => [
				'description_1',
				'description_2',
				'description_3',
			]
		]); ?>
	</div>

	<h2 id="detailsHeadlineFrontendText" class="btn btn-success btn-lg">Frontend Text</h2>
	<div id="detailsFieldFrontendText">
		<?= TabWidget::widget([
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => [
				'frontend_text',
			],
			'editorOptions' => [
				'hint' => 'Der Frontend Text wird unter der Artikeldetailbox angezeigt.'
			],
		]); ?>
	</div>

	<h2 id="detailsHeadlineSEO" class="btn btn-success btn-lg">SEO</h2>
	<div id="detailsFieldSEO">
		<fieldset class="seo_related_input_fields">
			<legend>SEO (optional)</legend>

			<div class="row">
				<div class="col-md-8">
					<?= TabWidget::widget([
						'model' => $model,
						'form' => $form,
						'fieldsToRender' => [
							'meta_title',
						],
						'editorOptions' => [
							'maxlength' => 49
						],
						'inputType' => TabWidget::TYPE_TEXTFIELD
					]); ?>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<p style="margin-top: 28px; font-size: 1.2em;"> | deinhandy.de </p>
					</div>
				</div>
			</div>

			<?= TabWidget::widget([
				'model' => $model,
				'form' => $form,
				'fieldsToRender' => [
					'meta_description',
				],
				'editorOptions' => [
					'maxlength' => 512
				],
				'inputType' => TabWidget::TYPE_TEXTFIELD
			]); ?>
		</fieldset>
		<fieldset class="seo_related_input_fields">
			<legend>SEO (Pflichtfelder)</legend>

			<?= TabWidget::widget([
				'model' => $model,
				'form' => $form,
				'fieldsToRender' => [
					'seo_text_1',
					'seo_text_2',
					'seo_text_3',
					'seo_text_4',
				],
			]); ?>
		</fieldset>
	</div>

    <fieldset class="articlegroup_marketing_image_input_field">
        <legend><?= Yii::t('app', 'Marketing Image') ?></legend>
		<?= $form->field($model, 'show_marketing_image')->checkbox() ?>
		<?= $form->field($model, 'marketing_image')->fileInput(['image' => true])->hint(Yii::t('app', 'only PNG file type allowed')); ?>
    </fieldset>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'),
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
