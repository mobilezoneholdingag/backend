<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleGroups */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $articleGroupTariffProvider \yii\data\ArrayDataProvider */


$this->registerJsFile('/js/articleGroupUpdate.js');

$this->title = Yii::t('app', 'Update {modelClass}: ', [
		'modelClass' => 'Article Groups',
	]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="article-groups-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
		'dataProvider' => $dataProvider,
		'articleGroupTariffProvider' => $articleGroupTariffProvider,
	]) ?>

</div>
