<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\ShopCertification */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel backend\models\CertificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */

$this->title = 'Update Shop Special: ' . $model->id;
$this->params['breadcrumbs'][] = 'Update';
?>
<p style="float:left;">
	<?=
	Html::a(
		Yii::t('app', 'Back to Shop List'),
		Url::toRoute(array_merge(['/shop/index'])),
		['class' => 'btn btn-success']
	);
	?>
</p>
<div style="clear:both;"></div>

<?php
echo GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'columns' => [
			'type',
			[
				'class' => 'yii\grid\ActionColumn',
				'header' => 'Actions',
				'template' => '{add}',
				'buttons' => [
					'add' => function($url, $product, $key) use ($existingIds, $model, $queryParams) {
						if (in_array($product->id, $existingIds)) {
							return Html::a(
								Html::button(Yii::t('app', 'Remove'), ['class' => 'btn-sm btn-danger']),
								Url::toRoute(
									array_merge(
										$queryParams,
										[
											'shop-certification/update',
											'id' => $model->id,
											'remove' => $product->id,
											'add' => '',
										]
									)
								)
							);
						}

						return Html::a(
							Html::button(Yii::t('app', 'Add'), ['class' => 'btn-sm btn-success']),
							Url::toRoute(
								array_merge(
									$queryParams,
									[
										'shop-certification/update',
										'id' => $model->id,
										'add' => $product->id,
										'remove' => '',
									]
								)
							)
						);
					},
				],
			],
		],
	]
);
?>
