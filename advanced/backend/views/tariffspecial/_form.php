<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TariffSpecial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tariff-special-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'type')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'filter_type')->dropDownList($model::getAvailableFilterTypes()) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'short_name')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'price_change')->textInput() ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => [
				'infotext',
				'infotext_2',
				'custom_infotext_tariff',
			],
			'editorOptions' => [
				'maxlength' => 255,
			],
			'inputType' => TabWidget::TYPE_TEXTFIELD,
		]
	); ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => [
				'custom_infotext_cart',
			],
			'editorOptions' => [
				'maxlength' => 4096,
			],
			'inputType' => TabWidget::TYPE_TEXTFIELD,
		]
	); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
