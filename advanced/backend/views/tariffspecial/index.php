<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tariff Specials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-special-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		We add these tariff specials manually via migrations when we get new ones (as the structure of these specials
		might change and catching this automatically is impossible).
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'filterTypeName',
			'type',
			'name',
			'short_name',
			'price_change',
			'infotext',
			'infotext_2',
			'custom_infotext_tariff',
			'custom_infotext_cart',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
