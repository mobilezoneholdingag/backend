<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TariffSpecial */

$this->title = Yii::t('app', 'Create Tariff Special');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tariff Specials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-special-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
