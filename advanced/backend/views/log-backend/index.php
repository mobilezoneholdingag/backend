<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\LogBackendSearch */

$this->title = 'Log Backends';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-backend-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('../log/overview', compact('dataProvider', 'searchModel')); ?>
</div>
