<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Newsletters');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<div style="margin-top: 10px; margin-bottom: 10px;">
		<?= Html::a('CSV Export', Url::to('/newsletter/export-all'), ['class' => 'btn btn-info']) ?>
	</div>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			//'id',
			'email:email',
			'bid',
			'created:datetime',
			'deleted:datetime',
			'confirmed:boolean',
			'submit_source',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
