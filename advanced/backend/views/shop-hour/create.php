<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ShopHour */

$this->title = 'Create Shop Hour';
$this->params['breadcrumbs'][] = ['label' => 'Shop Hours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-hour-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render(
		'_form',
		[
			'model' => $model,
		]
	) ?>

</div>
