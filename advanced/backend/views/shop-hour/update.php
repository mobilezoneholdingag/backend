<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ShopHour */

$this->title = 'Update Shop Hour: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Shop Hours', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shop-hour-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render(
		'_form',
		[
			'model' => $model,
		]
	) ?>

</div>
