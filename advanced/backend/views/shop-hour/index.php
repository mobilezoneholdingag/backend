<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ShopHourSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shop Hours';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-hour-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Create Shop Hour', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				'id',
				'shop_id',
				'day',
				'opened_at',
				'closed_at',

				['class' => 'yii\grid\ActionColumn'],
			],
		]
	); ?>
</div>
