<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TrackerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Trackers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracker-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a(Yii::t('app', 'Create Tracker'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'code',
			'title',
			'prio',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
