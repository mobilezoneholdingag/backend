<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Tracker */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Tracker',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trackers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tracker-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
