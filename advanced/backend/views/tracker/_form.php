<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Tracker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tracker-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'code')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'prio')->textInput() ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
