<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coupon-form">

	<?= Html::beginForm('/content-search', 'get'); ?>

	<div class="form-group">
		<?= Html::input('string', 'searchTerm', (isset($_GET['searchTerm']) ? $_GET['searchTerm'] : ''), ['class' => 'content-search-field']) ?>
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	</div>

	<?php Html::endForm(); ?>

</div>
