<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ExportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Content Search');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-search-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form') ?>

	<?= ($dataProvider->count) ? GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'section',
			'attribute',
			[
				'attribute' => 'url',
				'format' => 'raw',
				'value' => function ($dataProvider) {
					return Html::a('Link', $dataProvider['url']);
				}
			],
			'snippet',
		],
	]) : null; ?>

</div>
