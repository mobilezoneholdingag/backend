<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use components\i18n\widget\TabWidget;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::className(), [ ]); ?>
    <?= TabWidget::widget(
        [
            'model' => $model,
            'form' => $form,
            'fieldsToRender' => ['name',],
            'editorOptions' => [
                'maxlength' => 250,
            ],
            'inputType' => TabWidget::TYPE_TEXTFIELD,
        ]
    ); ?>
    <?= TabWidget::widget(
        [
            'model' => $model,
            'form' => $form,
            'fieldsToRender' => ['link',],
            'editorOptions' => [
                'maxlength' => 250,
            ],
            'inputType' => TabWidget::TYPE_TEXTFIELD,
        ]
    ); ?>

    <div class="form-group" style="margin-top: 20px;">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
