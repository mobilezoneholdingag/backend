<?php

use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LandingPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landingpages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?=
        Html::a(Yii::t('app', 'Create News Entry', [
            'modelClass' => 'GeneralNews',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="row">
        <div class="col-md-2">
            <?= PageSize::widget([
                'pageSizeParam' => 'per-page',
            ]); ?>
        </div>
    </div>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'date:date',
            'name',
            'link',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Yii::$app->user->can('createArticles') ? '{update} {delete}' : '{update}',
            ],

        ],
    ]); ?>

</div>
