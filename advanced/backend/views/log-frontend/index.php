<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\LogFrontendSearch */

$this->title = 'Log Frontends';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-frontend-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('../log/overview', compact('dataProvider', 'searchModel')); ?>
</div>
