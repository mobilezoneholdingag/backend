<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OfferType */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Offer Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offer-type-view">

	<?= DetailView::widget(
		[
			'model' => $model,
			'attributes' => [
				'id',
				'title',
				'is_active:boolean',
				'simcard_needed:boolean',
			],
		]
	) ?>

</div>
