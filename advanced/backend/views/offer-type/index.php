<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Offer Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offer-type-index">

	<?=
	GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'id',
				'title',
				'is_active:boolean',
			],
		]
	); ?>

</div>
