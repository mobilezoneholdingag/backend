<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AuditTrailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Audit Trails');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-trail-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'stamp:datetime',
			'userName',
			'model',
			//'old_value:ntext',
			//'new_value:ntext',
			'action',
			'field',
			'model_id',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
