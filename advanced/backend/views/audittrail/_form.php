<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sammaye\audittrail\AuditTrail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="audit-trail-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'old_value')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'new_value')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'action')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'model')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'field')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'stamp')->textInput() ?>

	<?= $form->field($model, 'user_id')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'model_id')->textInput(['maxlength' => 255]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
