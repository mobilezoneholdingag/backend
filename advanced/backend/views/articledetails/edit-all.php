<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ArticleDetailsGroup;

/* @var $this yii\web\View */
/* @var $models common\models\ArticleDetails[] */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'ArticleDetails',
]);
$result = [];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ArticleDetails'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->detail_id, 'url' => ['view', 'detail_id' => $model->detail_id, 'sort_id' => $model->sort_id, 'group_id' => $model->group_id, 'set_id' => $model->set_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit All');
?>
<div class="articledetails-edit-all">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php $form = ActiveForm::begin(); ?>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Update All'), ['class' => 'btn btn-primary']) ?>
	</div>
	<?php foreach ($models as $model): ?>
		<div class="articledetails-form">


			<?php //echo $form->field($model, 'detail_id')->textInput() ?>
<span>#<?= $model->detail_id;  ?>:</span>
			<?= Html::textInput('result['.$model->detail_id.'][name]' , $model->detail_name,['maxlength' => 40]) ?>

			<?php //echo $form->field($model, 'tooltip')->textInput(['maxlength' => 256]) ?>

			<?php //echo $form->field($model, 'sort_id')->textInput() ?>

			<?= Html::dropDownList('result['.$model->detail_id.'][gruppe]', $model->group_id, ArrayHelper::map(ArticleDetailsGroup::find()->orderBy('sort')->all(), 'group_id', 'title'), ['prompt' => '- ' . Yii::t('app', 'please select') . ' -']) ?>

			<?= Html::textInput('result['.$model->detail_id.'][unit]', $model->unit,['maxlength' => 32]) ?>

			<?php //echo $form->field($model, 'highlight')->textInput() ?>
			<hr>

		</div>
	<?php endforeach; ?>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Update All'), ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>
