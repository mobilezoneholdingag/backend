<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ArticleDetails */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'ArticleDetails',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ArticleDetails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articledetails-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
