<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ArticleDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Article Details');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articledetails-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?=
		Html::a(Yii::t('app', 'Edit All', [
			'modelClass' => 'ArticleDetails',
		]), ['edit-all'], ['class' => 'btn btn-success']) ?>
	</p>

	<?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		//'filterModel' => $searchModel,
		'columns' => [
			'detail_id',
			'detail_name',
			[
				'attribute' => 'group_id',
				'format' => 'raw',
				'value' => function ($model) {
					if (!empty($model->group))
						return $model->group->title;
				},
			],
			// 'set_id',
			'unit',
			// 'highlight',
			//'tooltip',
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
			],
		],
	]); ?>

</div>
