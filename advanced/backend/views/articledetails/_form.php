<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ArticleDetailsGroup;


/* @var $this yii\web\View */
/* @var $model common\models\ArticleDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articledetails-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'detail_name')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(ArticleDetailsGroup::find()->orderBy('sort')->all(), 'group_id', 'title')) ?>

	<?= $form->field($model, 'unit')->textInput(['maxlength' => 32]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
