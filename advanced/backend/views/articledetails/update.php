<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleDetails */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'ArticleDetails',
]) . ' ' . $model->detail_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ArticleDetails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->detail_id, 'url' => ['view', 'detail_id' => $model->detail_id, 'sort_id' => $model->sort_id, 'group_id' => $model->group_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="articledetails-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
