<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Ebay Template');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
	<div class="col-sm-4"><?php echo Html::a(Yii::t('app', 'View Template'), Url::toRoute(['ebay/view', 'article_id' => $article_id, 'tariff_id' => $tariff_id]), ['target' => '_blank', 'class' => 'btn btn-success pull-right']); ?></div>
	<div class="col-sm-4"><?php echo Html::button(Yii::t('app', 'Copy HTML'), ['class' => 'btn btn-success center-block js-textareacopybtn']); ?></div>
	<div class="col-sm-4"><?php echo Html::a(Yii::t('app', 'Back To Overview'), Url::toRoute('articletariffs/index'), ['class' => 'btn btn-warning pull-left']); ?></div>
</div>
<br/>

<div style="height: 500px">
<textarea readonly class="js-copytextarea" style="width: 100%;height: 100%;">
	<?php
	echo $html;
	?>
</textarea>
</div>


<script type="application/javascript">
	var copyTextareaBtn = document.querySelector('.js-textareacopybtn');
	copyTextareaBtn.addEventListener('click', function (event) {
		var copyTextarea = document.querySelector('.js-copytextarea');
		copyTextarea.select();
		try {
			var successful = document.execCommand('copy');
			var msg = successful ? 'successful' : 'unsuccessful';
			console.log('Copying text command was ' + msg);
		} catch (err) {
			console.log('Oops, unable to copy');
		}
	});
</script>



