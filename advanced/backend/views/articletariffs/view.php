<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleTariffs */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articletarife-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?php if (Yii::$app->user->can('createArticleTariffs')) { ?>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		]) ?>
		<?php } ?>
	</p>

    <h2><?= Yii::t('app', 'Article Tariff Availability'); ?></h2>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'is_active_erp:boolean',
            'valid_from',
            'valid_until',
		],
	]) ?>

    <h2><?= Yii::t('app', 'Article Tariff Info'); ?></h2>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'article_id',
			'productName',
			'tariff_id',
			'tariffName',
			'tariff.offerType.title',
            'providerTitle',
		],
	]) ?>

    <h2><?= Yii::t('app', 'Pricing'); ?></h2>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'price:currency',
			[
				'attribute' => 'price_article_once',
				'value' => $model->getArticlePriceOnce()
						? Yii::$app->formatter->asCurrency($model->getArticlePriceOnce()->getValue())
						: null
			],
			[
				'attribute' => 'price_article_first_month',
				'value' => $model->getArticlePriceFirstMonth()
						? Yii::$app->formatter->asCurrency($model->getArticlePriceFirstMonth()->getValue())
						: null
			],
			[
				'attribute' => 'price_article_monthly',
				'value' => $model->getArticlePriceMonthly()
						? Yii::$app->formatter->asCurrency($model->getArticlePriceMonthly()->getValue())
						: null
			],
			[
				'attribute' => 'price_article_total',
				'value' => $model->getArticlePriceTotal()
						? Yii::$app->formatter->asCurrency($model->getArticlePriceTotal()->getValue())
						: null
			],
			[
				'attribute' => 'price_tariff_monthly',
				'value' => $model->getTariffPriceMonthly()
						? Yii::$app->formatter->asCurrency($model->getTariffPriceMonthly()->getValue())
						: null
			],
			[
				'attribute' => 'price_tariff_once',
				'value' => $model->getTariffPriceOnce()
						? Yii::$app->formatter->asCurrency($model->getTariffPriceOnce()->getValue())
						: null
			],
			[
				'attribute' => 'simCard.product_name',
				'label' => Yii::t('app', 'Default SIM Card'),
			],
			[
				'attribute' => 'simCard.price_sale',
				'label' => Yii::t('app', 'Default SIM Card Price'),
			],
		],
	]) ?>

</div>
