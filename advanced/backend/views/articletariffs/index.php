<?php

use common\models\ArticleTariffs;
use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ArticleTariffsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Article Tariffs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articletariffs-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<div class="row">
		<div class="col-md-2">
			<?= PageSize::widget([
				'pageSizeParam' => 'per-page',
			]); ?>
		</div>
		<div class="col-md-2">
			<div class="inactiveLegendInformation"></div>
			inaktiv
		</div>
	</div>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterSelector' => 'select[name="per-page"]',
		'filterModel' => $searchModel,
		'columns' => [
			'id',
			'article_id',
			'productName',
			[
				'attribute' => 'price_article_once',
				'value' => function(Articletariffs $articleTariff) {
					return $articleTariff->getArticlePriceFirstMonth()
						? Yii::$app->formatter->asCurrency($articleTariff->getArticlePriceFirstMonth()->getValue())
						: null;
				}
			],
			[
				'attribute' => 'price_article_monthly',
				'value' => function(Articletariffs $articleTariff) {
					return $articleTariff->getArticlePriceFirstMonth()
						? Yii::$app->formatter->asCurrency($articleTariff->getArticlePriceMonthly()->getValue())
						: null;
				}
			],
			'providerTitle',
			'tariff_id',
			'tariffName',
			'offerTypeTitle',
			'valid_from',
			'valid_until',
			'is_active:boolean',
			'is_active_erp:boolean',
			[
				'class' => 'yii\grid\ActionColumn',
				'header' => 'Action',
				'template' => '{view}',
			],
		],
	]); ?>


</div>
