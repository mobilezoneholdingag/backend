<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ArticleTariffs */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Article Tariffs',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articletarife-create">
	<?php echo $this->render(Yii::$app->params['flashMessagesPath']); ?>

	<h1><?= Html::encode($this->title) ?></h1>

	<?=
	$this->render('_form', [
		'model' => $model,
	]) ?>

</div>