<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Provider;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $notMatchingTariffs yii\data\ActiveDataProvider */

$this->title = "EAM Tarife";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<!--p>
		<?= Html::a(Yii::t('app', 'Create Tariff', [
		'modelClass' => 'Tariffs',
	]), ['create'], ['class' => 'btn btn-success']) ?>
	</p-->

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],


			'tariff_id',
			[
				'attribute' => 'title',
				'label' => Yii::t('app', 'Title')
			],
			[
				'attribute' => 'net',
				'label' => Yii::t('app', 'Net')
			],
			[
				'attribute' => 'provider',
				'label' => Yii::t('app', 'Provider')
			],
			[
				'attribute' => 'provision',
				'label' => Yii::t('app', 'Provision'),
			],
			[
				'attribute' => 'type',
				'label' => Yii::t('app', 'Type')
			],
			[
				'attribute' => 'price_monthly',
				'label' => Yii::t('app', 'Price Monthly')
			],
			'status',
			[
				'attribute' => 'tariffLink',
				'format' => 'raw',
				'label' => Yii::t('app', 'Link')
			],


			['class' => 'yii\grid\ActionColumn',
				//'contentOptions' => ['style' => 'width:350px;'],
				'header' => 'Actions',
				'template' => '{update}',
				'buttons' => [
					'update' => function ($url, $dataProvider, $key) {
						if ($dataProvider['status'] == \backend\models\EAMTariffs::STATUS_NEW) {
							// return Html::a(Html::button(Yii::t('app', 'Create'), ['class' => 'btn-sm btn-info']), Url::to(['create', 'id' => $dataProvider['tariff_id']]));
						} else {
							return Html::a(Html::button(Yii::t('app', 'Update'), ['class' => 'btn-sm btn-info']), Url::to(['update', 'id' => $dataProvider['tariff_id']]));
						}
					},
				],
			],
		],
	]); ?>
	<br>

	<h1><?= Html::encode('Nicht (mehr) im Feed') ?></h1>
	<?= GridView::widget([
		'dataProvider' => $notMatchingTariffs,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'attribute' => 'title',
				'label' => Yii::t('app', 'Title')
			],
			[
				'attribute' => 'net',
				'label' => Yii::t('app', 'Net'),
				'value' => function ($notMatchingTariffs) {
					return Provider::findOne($notMatchingTariffs['provider_id'])->net->title;
				}
			],
			[
				'attribute' => 'provider_id',
				'label' => Yii::t('app', 'Provider'),
				'value' => function ($notMatchingTariffs) {
					return Provider::findOne($notMatchingTariffs['provider_id'])->title;
				}
			],
			[
				'attribute' => 'provision',
				'label' => Yii::t('app', 'Provision'),
			],
			[
				'attribute' => 'data_only',
				'label' => Yii::t('app', 'Data Only'),
				'format' => 'boolean',
			],
			[
				'attribute' => 'price_monthly',
				'label' => Yii::t('app', 'Price Monthly'),
				'format' => 'currency',
			],
			['class' => 'yii\grid\ActionColumn',
				//'contentOptions' => ['style' => 'width:350px;'],
				'header' => 'Actions',
				'template' => '{update} {delete}',
				'buttons' => [
					'update' => function ($url, $notMatchingTariffs, $key) {
						return Html::a(Html::button(Yii::t('app', 'Update'), ['class' => 'btn-sm btn-info']), Url::to(['update', 'id' => $notMatchingTariffs['id']]));
					},
					'delete' => function ($url, $notMatchingTariffs, $key) {
						return Html::a(Html::button(Yii::t('app', 'Delete'), ['class' => 'btn-sm btn-danger']), ['tariffs/delete', 'id' => $notMatchingTariffs['id']],
							['title' => Yii::t('app', 'Delete'), 'data-confirm' => "Wollen Sie diesen Tariff wirklich löschen?", 'data-method' => "post", 'data-pjax' => "0"]);
					},

				],
			],
		],
	]); ?>

</div>
