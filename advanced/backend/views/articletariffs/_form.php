<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleTariffs */
/* @var $form yii\widgets\ActiveForm */
?>

<?= DetailView::widget([
	'model' => $model,
	'attributes' => [
		'id',
		'productName',
		'article_id',
		'tariff_id',
		'tariffName',
		'tariff.offerType.title',
		'price:currency',
		'price_monthly:currency',
		'subsidy_payment:currency',
		'external_id',
		[
			'attribute' => 'simCard.product_name',
			'label' => Yii::t('app', 'Default SIM Card'),
		],
	],
]) ?>

<div class="articletarife-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<label><input type="checkbox" disabled="disabled" value="1" <?= $model->is_active_erp ? 'checked="checked"' : '' ?>> <?= Yii::t('app', 'Is Active Erp') ?></label>
	</div>

	<?= $form->field($model, 'is_active')->checkbox([['checked' => true]]) ?>

	<?= $form->field($model, 'price_monthly')->textInput(['maxlength' => 7]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
