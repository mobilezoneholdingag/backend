<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Reporting');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="reporting-index" style="margin-bottom: 50px;">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		Creates a list of rejected sales (from the chosen time span).<br/>
		Please enter the date in this format: YYYY-MM-DD
	</p>

	<?php $form = ActiveForm::begin(['action' => ['export-rejected-sales'], 'method' => 'get',]); ?>
		<?= Html::label('Von','timestamp_from') ?>
		<?= Html::input('date','timestamp_from') ?>
		<?= Html::label('Bis','timestamp_to') ?>
		<?= Html::input('date','timestamp_to') ?>
		<?= Html::submitButton(Yii::t('app', 'Get .CSV'), ['class' => 'btn btn-primary']) ?>
	<?php ActiveForm::end(); ?>

</div>