<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SpeakLanguage */

$this->title = 'Create Speak Language';
$this->params['breadcrumbs'][] = ['label' => 'Speak Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="speak-language-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render(
		'_form',
		[
			'model' => $model,
		]
	) ?>

</div>
