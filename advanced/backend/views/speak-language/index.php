<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SpeakLanguageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Speak Languages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="speak-language-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a('Create Speak Language', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'id',
				'name',
				['class' => 'yii\grid\ActionColumn'],
			],
		]
	); ?>
</div>
