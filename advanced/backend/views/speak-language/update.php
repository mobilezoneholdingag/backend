<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SpeakLanguage */

$this->title = 'Update Speak Language: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Speak Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="speak-language-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render(
		'_form',
		[
			'model' => $model,
		]
	) ?>

</div>
