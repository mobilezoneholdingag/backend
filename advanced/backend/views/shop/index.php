<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ShopSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shops';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create Shop', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'name',
				'shop_tree_id',
				'shop_tree_name',
				'street',
				[
					'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
					'template' => '{view} {update}' . (Yii::$app->user->can('starlord') ? ' {delete}' : ''),
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'header' => 'Config',
					'template' => '{hours} {specials} {certification} {language}',
					'buttons' => [
						'hours' => function($url, $model, $key) {
							return Html::a(
								'',
								Url::toRoute(['shop-hour/index', 'shop_id' => $model->id]),
								['class' => 'glyphicon glyphicon-time', 'title' => 'Öffnungszeiten']
							);
						},
						'specials' => function($url, $model, $key) {
							return Html::a(
								'',
								Url::toRoute(['shop-special/update', 'id' => $model->id]),
								['class' => 'glyphicon glyphicon-th-list', 'title' => 'Shop-Specials']
							);
						},
						'certification' => function($url, $model, $key) {
							return Html::a(
								'',
								Url::toRoute(['shop-certification/update', 'id' => $model->id]),
								['class' => 'glyphicon glyphicon-certificate', 'title' => 'Zertifizierungen']
							);
						},
						'language' => function($url, $model, $key) {
							return Html::a(
								'',
								Url::toRoute(['shop-language/update', 'id' => $model->id]),
								['class' => 'glyphicon glyphicon-bullhorn', 'title' => 'Sprachen']
							);
						},
					],
				],
			],
		]
	); ?>
</div>
