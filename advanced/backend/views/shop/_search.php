<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShopSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-search">

	<?php $form = ActiveForm::begin(
		[
			'action' => ['index'],
			'method' => 'get',
		]
	); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'name') ?>

	<?= $form->field($model, 'shop_tree_id') ?>

	<?= $form->field($model, 'shop_tree_name') ?>

	<?= $form->field($model, 'street') ?>

	<?php // echo $form->field($model, 'extra_line') ?>

	<?php // echo $form->field($model, 'zip') ?>

	<?php // echo $form->field($model, 'city') ?>

	<?php // echo $form->field($model, 'canton') ?>

	<?php // echo $form->field($model, 'latitude') ?>

	<?php // echo $form->field($model, 'longitude') ?>

	<?php // echo $form->field($model, 'phone') ?>

	<?php // echo $form->field($model, 'fax') ?>

	<?php // echo $form->field($model, 'email') ?>

	<?php // echo $form->field($model, 'sort') ?>

	<?php // echo $form->field($model, 'state') ?>

	<?php // echo $form->field($model, 'manager_name') ?>

	<?php // echo $form->field($model, 'manager_function') ?>

	<?php // echo $form->field($model, 'manager_image_id') ?>

	<?php // echo $form->field($model, 'images_title') ?>

	<?php // echo $form->field($model, 'shop_image_id_1') ?>

	<?php // echo $form->field($model, 'shop_image_id_2') ?>

	<?php // echo $form->field($model, 'shop_image_id_3') ?>

	<?php // echo $form->field($model, 'is_helpcenter') ?>

	<div class="form-group">
		<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
