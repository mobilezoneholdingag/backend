<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use backend\forms\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Shop */
/* @var $form ActiveForm */
?>

<div class="shop-form">

	<?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">

            <h3>Basis Informationen</h3>

            <?= TabWidget::widget(
                [
                    'model' => $model,
                    'form' => $form,
                    'fieldsToRender' => ['name',],
                    'editorOptions' => [
                        'maxlength' => true,
                    ],
                    'inputType' => TabWidget::TYPE_TEXTFIELD,
                ]
            ); ?>
        </div>

        <div class="col-md-4">

            <h3>Beschreibung</h3>

            <?= TabWidget::widget(
                [
                    'model' => $model,
                    'form' => $form,
                    'fieldsToRender' => ['description',],
                    'editorOptions' => [
                        'maxlength' => true,
                    ],
                    'inputType' => TabWidget::TYPE_TEXTAREA,
                ]
            ); ?>

        </div>

        <div class="col-md-4">

            <h3>Spezielle Öffnungszeiten</h3>

            <?= TabWidget::widget(
                [
                    'model' => $model,
                    'form' => $form,
                    'fieldsToRender' => ['special_opening',],
                    'editorOptions' => [
                        'maxlength' => true,
                    ],
                    'inputType' => TabWidget::TYPE_TEXTAREA,
                ]
            ); ?>

        </div>

    </div>

    <hr style="margin-top: 50px; margin-bottom: 50px;">


    <div class="row">
        <div class="col-md-4">
            <h3>Adresse</h3>
            <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'extra_line')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'zip')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'canton')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'latitude')->textInput() ?>

            <?= $form->field($model, 'longitude')->textInput() ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'manager_name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-4">
            <h3>Manager</h3>
            <?= TabWidget::widget(
                [
                    'model' => $model,
                    'form' => $form,
                    'fieldsToRender' => ['manager_function',],
                    'editorOptions' => [
                        'maxlength' => true,
                    ],
                    'inputType' => TabWidget::TYPE_TEXTFIELD,
                ]
            ); ?>

            <hr>

            <?= $form->field($model, 'manager_image')->fileInput(['image' => true])->hint('quadratisch, mindestens 300x300 px'); ?>

        </div>

        <div class="col-md-4">
            <h3>Shop Bilder</h3>
            <div style="margin-bottom:30px;">
                <?= $form->field($model, 'shop_image_1')->fileInput(['image' => true])->hint('rechteckig, mindestens 800x600px'); ?>
            </div>

            <div style="margin-bottom:30px;">
                <?= $form->field($model, 'shop_image_2')->fileInput(['image' => true])->hint('rechteckig, mindestens 800x600px'); ?>
            </div>

            <div style="margin-bottom:30px;">
                <?= $form->field($model, 'shop_image_3')->fileInput(['image' => true])->hint('rechteckig, mindestens 800x600px'); ?>
            </div>
        </div>

    </div>

	<hr style="margin-top: 50px; margin-bottom: 50px;">

	<?= $form->field($model, 'closure_date')->widget(\yii\jui\DatePicker::className()) ?>

	<?= $form->field($model, 'opening_date')->widget(\yii\jui\DatePicker::className()) ?>

    <div class="col-sm-3"><?= $form->field($model, 'state')->textInput(['disabled' => 'disabled']) ?></div>
    
    <div class="col-sm-3"><?= $form->field($model, 'shop_tree_id')->textInput(['disabled' => 'disabled']) ?></div>

    <div class="col-sm-3"><?= $form->field($model, 'shop_tree_name')->textInput(['disabled' => 'disabled']) ?></div>

    <div class="col-sm-3"><?= $form->field($model, 'sort')->textInput(['disabled' => 'disabled']) ?></div>

	<?= $form->field($model, 'is_helpcenter')->checkbox() ?>
    
    <div class="form-group">
	    <?= Html::submitButton(
		    $model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'),
		    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
	    ) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
