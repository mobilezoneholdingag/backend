<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Shop */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Shops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if (Yii::$app->user->can('starlord')): ?>
		<?= Html::a(
			'Delete',
			['delete', 'id' => $model->id],
			[
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Are you sure you want to delete this item?',
					'method' => 'post',
				],
			]
		) ?>
        <?php endif; ?>
	</p>

	<?= DetailView::widget(
		[
			'model' => $model,
			'attributes' => [
				'id',
				'name',
				'description',
				'shop_tree_id',
				'shop_tree_name',
				'street',
				'extra_line',
				'zip',
				'city',
				'canton',
				'latitude',
				'longitude',
				'phone',
				'fax',
				'email:email',
				'sort',
				'state',
				'manager_name',
				'manager_function',
				'is_helpcenter',
				'manager_image',
				'shop_image_1',
				'shop_image_2',
				'shop_image_3',
				'closure_date',
				'opening_date',
                'special_opening',
			],
		]
	) ?>

</div>
