<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LogConsoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Consoles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-console-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('../log/overview', compact('dataProvider', 'searchModel')); ?>
</div>
