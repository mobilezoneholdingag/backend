<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\ShopSpecial */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel backend\models\SpecialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */

$this->title = 'Update Shop Special: ' . $model->id;
$this->params['breadcrumbs'][] = 'Update';
?>
<p style="float:left;">
	<?=
	Html::a(
		Yii::t('app', 'Back to Shop List'),
		Url::toRoute(array_merge(['/shop/index'])),
		['class' => 'btn btn-success']
	);
	?>
</p>
<div style="clear:both;"></div>

<?php
echo GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'columns' => [
			'special_title',
			'special_description',
			[
				'class' => 'yii\grid\ActionColumn',
				'header' => 'Actions',
				'template' => '{add}',
				'buttons' => [
					'add' => function($url, $special, $key) use ($existingIds, $model, $queryParams) {
						if (in_array($special->id, $existingIds)) {
							return Html::a(
								Html::button(Yii::t('app', 'Remove'), ['class' => 'btn-sm btn-danger']),
								Url::toRoute(
									array_merge(
										$queryParams,
										[
											'shop-special/update',
											'id' => $model->id,
											'remove' => $special->id,
											'add' => '',
										]
									)
								)
							);
						}

						return Html::a(
							Html::button(Yii::t('app', 'Add'), ['class' => 'btn-sm btn-success']),
							Url::toRoute(
								array_merge(
									$queryParams,
									[
										'shop-special/update',
										'id' => $model->id,
										'add' => $special->id,
										'remove' => '',
									]
								)
							)
						);
					},
				],
			],
		],
	]
);
?>
