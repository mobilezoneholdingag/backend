<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Export */
/** @var $partners array */
/** @var $formatList array */

$this->title = Yii::t('app', 'Create Export');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Exports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="export-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
		'formatList' => $formatList,
		'partners' => $partners
	]) ?>

</div>
