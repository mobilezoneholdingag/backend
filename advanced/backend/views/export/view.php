<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Export */

$this->title = $model->partner;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Exports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="export-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		]) ?>
		<?= Html::a(Yii::t('app', 'Generate/Download'), ['download', 'key' => $model->key], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'partner',
			'preferred_format',
			'key',
			'filename',
			'last_run',
			'runs_today',
			'status',
			'delimiter',
			'bid',
			'extra_parameters',
			'link:url',
			'min_profit:currency',
		],
	]) ?>

</div>
