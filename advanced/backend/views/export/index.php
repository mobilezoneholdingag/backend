<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ExportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Exports');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="export-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a(Yii::t('app', 'Create Export'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'partner',
			'preferred_format',
			'key',
			'filename',
			'last_run',
			//'runs_today',
			'delimiter',
			'bid',
			'extra_parameters',
			'status:boolean',
			//'link:url',
			'min_profit:currency',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
