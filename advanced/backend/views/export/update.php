<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Export */
/** @var $formatList array */
/** @var $partners array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Export',
]) . ' ' . $model->partner;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Exports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->partner, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="export-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
		'formatList' => $formatList,
		'partners' => $partners
	]) ?>

</div>
