<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Export */
/* @var $form yii\widgets\ActiveForm */
/* @var $formatList array */
/** @var $partners array */
?>

<div class="export-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'partner')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'preferred_format')->dropDownList($formatList) ?>

	<?= $form->field($model, 'key')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'filename')->textInput()->hint('Type the file name without extension. The extension will be set by the application, depending on the format of the export.'
															.'<br /><p style="color:red">WARNING: If you use the same file name in another country, the files will overwrite each other.</p>') ?>

	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'last_run')->textInput(['readonly' => true]) ?>
		</div>
		<div class="col-md-6">
			<?= $form->field($model, 'runs_today')->textInput(['readonly' => true]) ?>
		</div>
	</div>

	<?= $form->field($model, 'status')->checkbox() ?>

	<?= $form->field($model, 'delimiter')->radioList(array_merge($model->getDefaultDelimitersList(), ['custom' => $model->delimiter]), ['item' => function($index, $label, $name, $checked, $value) use ($model) {
		if ($value == 'custom') {
			$checked = $model->isCustomDelimiter();
			return Html::radio($name, $checked, [
				'value' => 'custom',
				'label' => Html::textInput('custom', $checked ? $label : '')
			]);
		} else {
			return Html::radio($name, $checked, [
				'value' => $value,
				'label' => Html::encode($label),
			]);
		}
	},
	'separator' => '<br>']) ?>

	<?= $form->field($model, 'bid')->textInput() ?>

	<?= $form->field($model, 'extra_parameters')->textInput() ?>

	<?= $form->field($model, 'link')->textInput() ?>

	<?= $form->field($model, 'min_profit')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

<script type="application/json" id="partners"><?= json_encode($partners) ?></script>
<script type="application/javascript">
	$(document).ready(function() {
		$('#export-partner').on('blur', function (e) {
			var partners = JSON.parse($('#partners').text());
			$(e.target).parent().removeClass('has-warning').find('span.warning').remove();
			if ($.inArray($(e.target).val(), partners) < 0) {
				$(e.target).parent().removeClass('has-success').addClass('has-warning').append('<span class="warning">This partner has no module.</span>');
			}
		});
	});
</script>
