<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->dropDownList($dropdown); ?>

    <?= $form->field($model, 'message')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create') , ["class" => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
