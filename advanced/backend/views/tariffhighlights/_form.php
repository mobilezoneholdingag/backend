<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TariffHighlights */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifhighlights-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'tariff_id')->textInput() ?>

	<?= $form->field($model, 'sort')->textInput() ?>

	<?= $form->field($model, 'detail_id')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
