<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TariffHighlights */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'TariffHighlights',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TariffHighlights'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifhighlights-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
