<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'TariffHighlights');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifhighlights-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'TariffHighlights',
]), ['create'], ['class' => 'btn btn-success']) ?>

		<?= Html::a(Yii::t('app', 'Sort {modelClass}', [
			'modelClass' => 'TariffHighlights',
		]), ['sort'], ['class' => 'btn btn-warning']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'tariff_id',
			'sort',
			'detail_id',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
