<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Manufacturer */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manufacturers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacturer-view">
	<h1><?= Html::encode($this->title) ?></h1>
    <p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?php if (Yii::$app->user->can('starlord')): ?>
			<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
					'method' => 'post',
				],
			]) ?>
		<?php endif; ?>
    </p>
    <img src="<?= $model->image ?>" />
	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'is_active_erp:boolean',
			'is_active:boolean',
			'id',
			'erp_title',
			'title',
			[
				'attribute' => 'image',
				'format' => 'raw',
				'value' => Html::a(
					Html::img($model->image, ['width' => 50]),
					$model->image,
					['target'=>'_blank']
				),
			],
			[
				'attribute' => 'insurance_image',
				'format' => 'raw',
				'value' => Html::a(
					Html::img($model->insurance_image, ['width' => 50]),
					$model->insurance_image,
					['target'=>'_blank']
				),
			],
		],
	]) ?>
</div>
