<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ManufacturerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Manufacturers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacturer-index">
	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?php if (Yii::$app->user->can('createManufacturers')) { ?>
		<?=
		Html::a(Yii::t('app', 'Create Manufacturer', [
			'modelClass' => 'Manufacturer',
		]), ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>
		<?=
		Html::a(Yii::t('app', 'Sort Manufacturer', [
			'modelClass' => 'Manufacturer',
		]), ['sort'], ['class' => 'btn btn-warning']) ?>
	</p>
	<?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			'id',
			[
				'attribute' => 'title',
				'format' => 'html',
				'value' => function ($model) {
					return Html::a($model->title, \yii\helpers\Url::to(['manufacturer/view', 'id' => $model->id]));
				}
			],
			'erp_title',
			'is_active:boolean',
			'is_active_erp:boolean',
			[
				'attribute' => 'image',
				'format' => 'raw',
				'value' => function(\common\models\Manufacturer $model) {
					if ($model->image) {
						return Html::a(
							Html::img($model->image, ['width' => 50]),
							$model->image,
							['target'=>'_blank']
						);
					}

					return null;
				}
			],
			[
				'attribute' => 'insurance_image',
				'format' => 'raw',
				'value' => function(\common\models\Manufacturer $model) {
					if ($model->insurance_image) {
						return Html::a(
							Html::img($model->insurance_image, ['width' => 50]),
							$model->insurance_image,
							['target'=>'_blank']
						);
					}

					return null;
				}
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
			],
		],
	]); ?>
</div>
