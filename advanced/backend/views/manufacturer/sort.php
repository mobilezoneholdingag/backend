<?php

use yii\helpers\Html;
use common\models\Manufacturer;

/* @var $this yii\web\View */
/* @var $manufacturers Manufacturer[] */

$this->title = Yii::t('app', 'Sort Manufacturer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manufacturer'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Sort');
?>

<script>
	$(function () {
		$(".sortable").sortable({
			axis: 'y',
			update: function (event, ui) {
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/manufacturer/sort'
				});

				$(".save_flash").fadeIn(150).delay(600).fadeOut(300);
			}
		});
	});
</script>

<div class="manufacturer-sort">

	<h2><?= Html::encode($this->title) ?></h2><br>
	<span class="save_flash" style="display: none"><?= Yii::t('app', 'saved') ?></span>
	<?= Html::beginForm(); ?>
	<ul class="sortable">
		<?php foreach ($manufacturers as $manufacturer): ?>
			<li id="item-<?= $manufacturer->id ?>" class="ui-state-default">
				<img style="width: 50px" alt="<?= $manufacturer->title ?>" src="<?= (isset($manufacturer->image) ? $manufacturer->image : null) ?>"/>
				<span><?= $manufacturer->title ?></span>
			</li>
		<?php endforeach; ?>
	</ul>
	<?= Html::endForm(); ?>
</div>
