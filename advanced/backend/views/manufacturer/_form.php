<?php

use common\modules\set\models\Sets;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use backend\forms\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Manufacturer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manufacturer-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <label><input type="checkbox" disabled="disabled"
                      value="1" <?= $model->is_active_erp ? 'checked="checked"' : '' ?>> <?= Yii::t(
                'app',
                'Is Active Erp'
            ) ?></label>
    </div>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'visible_homepage')->checkbox() ?>

    <?= $form->field($model, 'nav_hide')->checkbox() ?>

    <?= $form->field($model, 'id')->textInput(['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'erp_title')->textInput(['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?php if (Yii::$app->user->can('starlord')): ?>
        <?= $form->field($model, 'accessory_set_id')->dropDownList(
            ArrayHelper::map(Sets::find()->where(['type' => Sets::TYPE_ARTICLE])->all(), 'id', 'title'),
            ['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
        ) ?>
    <?php endif; ?>

    <fieldset class="manufacturer_image_input_field">
        <legend><?= Yii::t('app', 'Manufacturer Image') ?></legend>
        <?= $form->field($model, 'image')->fileInput(['image' => true])->hint(Yii::t('app', 'only PNG file type allowed')); ?>
    </fieldset>

    <fieldset class="manufacturer_insurance_image_input_field">
        <legend><?= Yii::t('app', 'Insurance Image') ?></legend>
        <?= $form->field($model, 'insurance_image')->fileInput(['image' => true])->hint(Yii::t('app', 'only PNG file type allowed')); ?>
    </fieldset>

    <fieldset class="manufacturer_marketing_image_input_field">
        <legend><?= Yii::t('app', 'Marketing Image') ?></legend>
        <?= $form->field($model, 'show_on_category')->checkbox() ?>
        <?= $form->field($model, 'show_on_pdp')->checkbox() ?>
        <?= $form->field($model, 'marketing_image')->fileInput(['image' => true])->hint(Yii::t('app', 'only PNG file type allowed')); ?>
    </fieldset>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
