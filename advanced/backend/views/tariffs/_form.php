<?php

use common\models\Provider;
use common\modules\set\models\Sets;
use common\models\TariffHighlightDetails;
use common\models\TariffHighlights;
use common\models\Tariffs;
use components\i18n\widget\TabWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Tariffs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tariffs-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'),
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<fieldset>
		<legend>Stammdaten</legend>
		<?= $form->field($model, 'external_id')->textInput(['maxlength' => 32, 'readonly' => 'readonly']) ?>

		<?= $form->field($model, 'id')->textInput(['disabled' => 'disabled']) ?>

		<?= $form->field($model, 'erp_title')->textInput(['disabled' => 'disabled']) ?>

		<?= $form->field($model, 'title')->textInput(['maxlength' => 60]) ?>

		<div class="form-group">
			<label><input type="checkbox" disabled="disabled"
			              value="1" <?= $model->is_active_erp ? 'checked="checked"' : '' ?>> <?= Yii::t(
					'app',
					'Is Active Erp'
				) ?></label>
		</div>

		<?= $form->field($model, 'is_active')->checkbox() ?>

		<?= $form->field($model, 'is_b2b_allowed')->checkbox() ?>

		<?= $form->field($model, 'swisscom_id')->textInput(['maxlength' => 255]) ?>

		<?= $form->field($model, 'swisscom_id_2')->textInput(['maxlength' => 255]) ?>

		<?php
		if (!$model->hasSimOnlyArticleTariff) {
			echo $form->field($model, 'sim_only_tariff_id')->dropDownList(
				$model->getViableSimOnlyTariffs(),
				['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
			);
		}
		?>

        <?php
        if ($model->getAltProviderImg()) { ?>
	        <?= $form->field($model, 'show_alt_provider_img')->checkbox() ?>
        <?php } ?>
    </fieldset>

	<fieldset>
		<legend>Filter</legend>
		<?= $form->field($model, 'offerTypeTitle')->textInput(['disabled' => 'disabled']) ?>

		<?= $form->field($model, 'providerTitle')->textInput(['disabled' => 'disabled'])->label(Yii::t('app', 'Provider')) ?>

		<?= $form->field($model, 'contract_duration')->textInput(['max_length' => 2]) ?>

		<?= $form->field($model, 'ratePaymentCount')->textInput(['disabled' => 'disabled']) ?>

		<?= $form->field($model, 'data_volume')->textInput(['maxlength' => 32])->hint(
			Yii::t('app', 'in MB (empty = unlimited)')
		) ?>

		<?= $form->field($model, 'price_monthly')->textInput(['maxlength' => 6]) ?>

		<?= $form->field($model, 'young_people')->checkbox(['disabled' => !$model->hasYoungAgeLimit()]) ?>
	</fieldset>

	<fieldset>
		<legend>Highlights</legend>
		<div class="integratedCheckboxList">
			<?php
			foreach (TariffHighlightDetails::find()->orderBy('sort')->all() as $highlightDetail): ?>
				<div class="row">
					<?= Html::checkbox(
						'detailIds[]',
						boolval(
							TariffHighlights::findOne(['tariff_id' => $model->id, 'detail_id' => $highlightDetail->id])
						),
						['value' => $highlightDetail->id, 'id' => $highlightDetail->id]
					);
					echo Html::label($highlightDetail->title, $highlightDetail->id);
					echo Html::label($highlightDetail->subtext, $highlightDetail->id, ['class' => 'subtext']); ?>
				</div>
			<?php endforeach; ?>
		</div>
	</fieldset>

	<fieldset>
		<legend>Texte</legend>
		<?= TabWidget::widget(
			[
				'model' => $model,
				'form' => $form,
				'fieldsToRender' => [
					'tariff_feed_description',
				],
				'inputType' => TabWidget::TYPE_TEXTAREA,
			]
		); ?>

		<?= TabWidget::widget(
			[
				'model' => $model,
				'form' => $form,
				'fieldsToRender' => [
					'footnote_text',
				],
				'editorOptions' => [
					'rows' => 6,
				],
				'inputType' => TabWidget::TYPE_TEXTAREA,
			]
		); ?>

		<div class="col-sm-4">
			<?= TabWidget::widget(
				[
					'model' => $model,
					'form' => $form,
					'fieldsToRender' => [
						'text_call',
					],
					'inputType' => TabWidget::TYPE_TEXTFIELD,
				]
			); ?>
		</div>
		<div class="col-sm-4">
			<?= TabWidget::widget(
				[
					'model' => $model,
					'form' => $form,
					'fieldsToRender' => [
						'text_sms',
					],
					'inputType' => TabWidget::TYPE_TEXTFIELD,
				]
			); ?>
		</div>
		<div class="col-sm-4">
			<?= TabWidget::widget(
				[
					'model' => $model,
					'form' => $form,
					'fieldsToRender' => [
						'text_internet',
					],
					'inputType' => TabWidget::TYPE_TEXTFIELD,
				]
			); ?>
		</div>
		<?= TabWidget::widget(
			[
				'model' => $model,
				'form' => $form,
				'fieldsToRender' => [
					'specific_legal_text',
				],
				'inputType' => TabWidget::TYPE_TEXTAREA,
			]
		); ?>
	</fieldset>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'),
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
