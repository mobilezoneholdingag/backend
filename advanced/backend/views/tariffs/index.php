<?php

use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TariffsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tariffs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-index">

	<?php if (Yii::$app->user->can('createTariffs')) { ?>
	<p>
		<?= Html::a(Yii::t('app', 'Create Tariff', [
			'modelClass' => 'Tariffs',
		]), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php } ?>


	<div class="row">
		<div class="col-md-2">
			<div class="inactiveLegendInformation"></div>
			= inaktiv
		</div>
		<div class="col-md-2">
			<?= PageSize::widget([
				'pageSizeParam' => 'per-page',
			]); ?>
		</div>
	</div>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'rowOptions' => function ($model) {
			return ($model->is_active != true) ? ['class' => 'inactiveRow'] : [];
		},
		'columns' => [
			'id',
			'erp_title',
			[
				'attribute' => 'title',
				'format' => 'html',
				'value' => function ($model) {
					return Html::a($model->title, Url::to(['tariffs/update', 'id' => $model->id]));
				}
			],
			[
				'attribute' => 'providerTitle',
				'format' => 'html',
				'label' => Yii::t('app', 'Provider'),
				'value' => function ($model) {
					return (isset($model->provider)) ? Html::a(
						Html::img(
							$model->provider->alt_image && $model->show_alt_provider_img ?
                                $model->provider->alt_image : $model->provider->image,
							[
								'alt' => $model->provider->title,
								'width' => 100,
							]
						),
						Url::to(['provider/view', 'id' => $model->provider->id])
					) : null;
				}
			],
			'offerTypeTitle',
			'swisscom_id',
			'swisscom_id_2',
			'price_monthly:currency',
			'contract_duration',
			'is_b2b_allowed:boolean',
			'is_active:boolean',
			'is_active_erp:boolean',
			'updated_at:datetime',
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => Yii::$app->user->can('createTariffs') ? '{view} {update} {delete}' : '{view} {update}',
			],
		],
	]); ?>

</div>
