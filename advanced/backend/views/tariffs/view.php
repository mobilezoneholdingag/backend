<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tariffs */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'updated_at:datetime',
			'external_id',
			'id',
			'erp_title',
			'title',
			'is_active_erp:boolean',
			'is_active:boolean',
			'is_b2b_allowed:boolean',
			'swisscom_id',
			'swisscom_id_2',
			'simOnlyTariffTitle',
			'offerTypeTitle',
			'providerTitle',
			'contract_duration',
			'ratePaymentCount',
			'data_volume',
			'price_monthly:currency',
			'young_people:boolean',
			'tariff_feed_description',
			'footnote_text',
			'text_call',
			'text_sms',
			'text_internet',
		],
	]) ?>

</div>
