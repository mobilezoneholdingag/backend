<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tariffs */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Tariffs',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarives'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarife-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
