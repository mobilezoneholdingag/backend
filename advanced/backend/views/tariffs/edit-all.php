<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Provider;

/* @var $this yii\web\View */
/* @var $models common\models\Tariffs[] */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'ArticleDetails',
]);
$result = [];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tariffs'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->detail_id, 'url' => ['view', 'detail_id' => $model->detail_id, 'sort_id' => $model->sort_id, 'gruppen_id' => $model->gruppen_id, 'set_id' => $model->set_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit All');
?>
<div class="Tariffs-edit-all">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php $form = ActiveForm::begin(); ?>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Update All'), ['class' => 'btn btn-primary']) ?>
	</div>
	<?php foreach ($models as $model): ?>
		<div class="tarife-form">

			<?= Html::textInput('result['.$model->tariff_id.'][title]' , $model->title,['maxlength' => 64, 'width' => 100]) ?>

			<?= Html::dropDownList('result['.$model->tariff_id.'][provider_id]', $model->provider_id, ArrayHelper::map(Provider::find()->all(), 'id', 'title'), ['prompt' => '- ' . Yii::t('app', 'please select') . ' -']) ?>

			<hr>

		</div>
	<?php endforeach; ?>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Update All'), ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>
