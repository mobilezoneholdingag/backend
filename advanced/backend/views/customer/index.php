<?php

use common\models\Customer;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\CustomerSearch */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'id',
				'email:email',
				'source_details',
				'first_name',
				'last_name',
				[
					'attribute' => 'city',
					'format' => 'html',
					'value' => function($model) {
						foreach ($model->customerAddresses as $customerAddress) {
							if ($customerAddress->is_default) {
								return $customerAddress->city ? $customerAddress->city : '-';
							}
						}

						return null;
					},
				],
				[
					'attribute' => 'number',
					'format' => 'html',
					'value' => function($model) {
						$content = [];
						foreach ($model->customerPhoneNumbers as $numbers) {
							if ($numbers->number) {
								$content[] = $numbers->number;
							}
						}

						return Html::ul($content, ['encode' => false, 'style' => 'white-space: nowrap;']);
					},
				],
				'updated_at:datetime',
				'created_at:datetime',

				[
					'class' => 'yii\grid\ActionColumn',
					'header' => 'Action',
					'template' => '{view} {update} {sort} {address} {newsletter} ' . (Yii::$app->user->can('starlord') ? '{phone} {delete}' : ''),
					'buttons' => [
						'address' => function($url, Customer $model, $key) {
							return Html::a(
								Yii::t('app', 'Address'),
								Url::toRoute(['customer-address/index', 'CustomerAddressSearch[customer_id]' => $model->id]),
								['class' => 'glyphicon glyphicon-list-alt']
							);
						},
						'phone' => function($url, Customer $model, $key) {
							return Html::a(
								Yii::t('app', 'Phone Numbers'),
								Url::toRoute(['customer-phone-number/index', 'CustomerPhoneNumberSearch[customer_id]' => $model->id]),
								['class' => 'glyphicon glyphicon-list-alt']
							);
						},
						'newsletter' => function($url, Customer $model, $key) {
							return Html::a(
								Yii::t('app', 'Newsletter'),
								Url::toRoute([
									'newsletter-subscriber/view',
									'id' => $model->newsletterSubscriber ? $model->newsletterSubscriber->id : null
								]),
								['class' => 'glyphicon glyphicon-list-alt']
							);
						},
					],
				],
			],
		]
	); ?>
</div>
