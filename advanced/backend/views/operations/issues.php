<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2016 DEINHANDY.de, a Mister Mobile GmbH brand
 *
 * @var View $this
 * @var ArrayDataProvider $ticketList
 */
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Issues Next On Deployment';

$origList = $ticketList;
$ticketList = new ArrayDataProvider([
	'allModels' => $ticketList,
	'sort' => [
		'attributes' => ['key']
	]
]);

?>
<div class="operating-systems-issues">

	<h2><?= Html::encode($this->title) ?></h2>
	<?php if (count($origList) > 0): ?>
	<?= GridView::widget([
		'dataProvider' => $ticketList,
		'columns' => [
			'type',
			[
				'format' => 'raw',
				'label' => 'Issue',
				'value' => function($model, $key, $index, $column) {
					return Html::a($model['key'].': '.$model['title'], $model['link'], ['target' => '_blank']);

				}
			],
			'status',
			'assignee'
		]
	]) ?>
	<?php else: ?>
		<p class="disabled warning"> No JIRA issues registered for next deployment. </p>
		<br><br><br>
	<?php endif; ?>
</div>
