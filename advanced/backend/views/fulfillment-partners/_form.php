<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FulfillmentPartners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partners-form">

	<?php $form = ActiveForm::begin(); ?>



	<?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'debitor_id')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'api_exist')->checkbox(); ?>

	<?= $form->field($model, 'api_is_active')->checkbox()->hint('please disable if the API of this fulfillment partner is not working.'); ?>

	<div class="form-group" >
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
