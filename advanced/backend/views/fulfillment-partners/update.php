<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FulfillmentPartners */

$this->title = Yii::t('app', 'Update Fulfillment Partners: ', [
	'modelClass' => 'Fulfillment Partners',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'FulfillmentPartners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="partners-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
