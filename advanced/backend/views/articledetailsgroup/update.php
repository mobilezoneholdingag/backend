<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleDetailsGroup */

$this->title = 'Update '.Yii::t('app', 'ArticleDetails Group') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ArticleDetails Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<script>
	$(function () {
		$(".sortable").sortable({
			axis: 'y',
			update: function (event, ui) {
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/articledetailsgroup/sort?id=<?= $model->id ?>'
				});

				$(".save_flash").fadeIn(150).delay(600).fadeOut(300);
			}
		});
	});
</script>

<div class="articledetails-group-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>

<div class="sets-sort">

	<h2><?= Html::encode(str_replace('Update', Yii::t('app', 'Sort'), $this->title)) ?></h2><br>
	<span class="save_flash" style="display: none"><?= Yii::t('app', 'saved') ?></span>
	<?= Html::beginForm(); ?>
	<ul class="sortable">
		<?php foreach ($model->details as $key => $detail): ?>
			<li id="item-<?= $detail->detail_id ?>" class="ui-state-default">
				<span><?= $detail->detail_name ?></span>
			</li>
		<?php endforeach; ?>
	</ul>
	<?= Html::endForm(); ?>
</div>
