<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Detail Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articledetails-group-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Detail Group',
]), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			//'id',
			'title',
			'group_id',
			['class' => 'yii\grid\ActionColumn',
				'contentOptions' => ['style' => 'width:350px;'],
				'header' => 'Actions',
				'template' => '{view} {update}',
				'buttons' => [
					'view' => function ($url, $model, $key) {
						return Html::a(Html::button(Yii::t('app', 'View'), ['class' => 'btn-sm btn-info']), Url::to(['view', 'id' => $model->id]));
					},
					'update' => function ($url, $model, $key) {
						return Html::a(Html::button(Yii::t('app', 'Update').' / '.Yii::t('app', 'Sort'), ['class' => 'btn-sm btn-primary']), Url::to(['update', 'id' => $model->id]));
					},
				],
			],
		],
	]); ?>

</div>
