<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleDetailsGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articledetails-group-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php // $form->field($model, 'group_id')->textInput() ?>

	<?= TabWidget::widget([
		'model' => $model,
		'form' => $form,
		'fieldsToRender' => [
			'title',
		],
		'editorOptions' => [
			'maxlength' => 64
		],
		'inputType' => TabWidget::TYPE_TEXTFIELD
	]); ?>

	<?php // $form->field($model, 'sort')->textInput() ?>

	<?php ActiveForm::end(); ?>

</div>
