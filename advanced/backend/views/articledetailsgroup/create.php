<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ArticleDetailsGroup */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'ArticleDetails Group',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ArticleDetails Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articledetails-group-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
