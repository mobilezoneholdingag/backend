<?php
use backend\assets\AppAsset;
use common\models\ShopConfig;
use components\Locale;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$user = Yii::$app->user;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>

	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<nav class="navbar navbar-default navbar-static-top">
	<div class="navbar-container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
					aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="<?= Yii::$app->homeUrl ?>" class="navbar-brand"><b>mobile</b><span style="color: #f00">zone</span></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">

			<ul class="nav navbar-nav">
				<?php if (!$user->isGuest): ?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Mobile Phones') ?>
							<span
								class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?= Url::to(['/article/index']) ?>"><?= Yii::t('app', 'show all') ?></a></li>
							<li>
								<a href="<?= Url::to(['/articlegroups/index']) ?>"><?= Yii::t('app', 'Article Groups') ?></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/insurances/article-insurance/index']) ?>"><?= Yii::t('app', 'Article_Insurances') ?></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/manufacturer/index']) ?>"><?= Yii::t('app', 'Manufacturers') ?></a>
							</li>
							<li>
								<a href="<?= Url::to(['/productcategory/index']) ?>"><?= Yii::t('app', 'Product Categories') ?></a>
							</li>
							<li>
								<a href="<?= Url::to(['/platform/platform/index']) ?>"><?= Yii::t('app', 'Platforms') ?></a>
							</li>
							<li>
								<a href="<?= Url::to(['/operatingsystems/index']) ?>"><?= Yii::t('app', 'Operating Systems') ?></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/articledetailsgroup/index']) ?>"><?= Yii::t('app', 'Detail Groups') ?></a>
							</li>
							<li>
								<a href="<?= Url::to(['/articledetails/index']) ?>"><?= Yii::t('app', 'Article Details') ?></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/articletariffs/index']) ?>"><?= Yii::t('app', 'Article Tariffs') ?></a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Tariffs') ?> <span
								class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?= Url::to(['/tariffs/index']) ?>"><?= Yii::t('app', 'show all') ?></a></li>
							<li><a href="<?= Url::to(['/provider/index']) ?>"><?= Yii::t('app', 'Provider') ?></a></li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/tariffhighlightdetails/index']) ?>"><?= Yii::t('app', 'Highlights') ?></a>
							</li>
							<li>
								<a href="<?= Url::to(['/offer-type/index']) ?>"><?= Yii::t('app', 'Offer Types') ?></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/articletariffs/index']) ?>"><?= Yii::t('app', 'Article Tariffs') ?></a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Customers') ?>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?= Url::to(['/customer/index']) ?>"><?= Yii::t('app', 'Customer Overview') ?></a>
							</li>
							<li>
								<a href="<?= Url::to(['/customer-address/index']) ?>"><?= Yii::t('app', 'Customer Address') ?></a>
							</li>
                            <?php if (Yii::$app->user->can('starlord')): ?>
                                <li>
                                    <a href="<?= Url::to(['/customer-phone-number/index']) ?>"><?= Yii::t('app', 'Customer Phone') ?></a>
                                </li>
                            <?php endif; ?>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Requests') ?>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?= Url::to(['/renewalrequest/renewal-request/index']) ?>"><?= Yii::t('app', 'Renewal Request') ?></a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Orders') ?>
							<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?= Url::to(['/order/order/index']) ?>"><?= Yii::t('app', 'show all') ?></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/insurances/insurance-order/index']) ?>"><?= Yii::t('app', 'Insurance Order') ?></a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Shops') ?>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?= Url::to(['/shop/index']) ?>"><?= Yii::t('app', 'Shops') ?></a>
								<a href="<?= Url::to(['/special/index']) ?>"><?= Yii::t('app', 'Specials') ?></a>
								<a href="<?= Url::to(['/speak-language/index']) ?>"><?= Yii::t('app', 'Languages') ?></a>
								<a href="<?= Url::to(['/certification/index']) ?>"><?= Yii::t('app', 'Certifications') ?></a>
							</li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Marketing') ?>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?= Url::to(['/tracker/index']) ?>"><?= Yii::t('app', 'Tracker') ?></a></li>
							<li><a href="<?= Url::to(['/coupon/index']) ?>"><?= Yii::t('app', 'Coupons') ?></a></li>
							<li><a href="<?= Url::to(['/short-urls/index']) ?>"><?= Yii::t('app', 'URL Shortener') ?></a></li>
							<li><a href="<?= Url::to(['/newsletter-subscriber/index']) ?>"><?= Yii::t('app', 'Newsletter Subscriber') ?></a></li>
							<li><a href="<?= Url::to(['/marketing-partner/index']) ?>"><?= Yii::t('app', 'Marketing Partner') ?></a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Content') ?> <span
								class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?= Url::to(['/sets/index']) ?>">
									<?= Yii::t('app', 'Sets') ?>
								</a>
							</li>
							<li>
								<a href="<?= Url::to(['/set/accessory-category/index']) ?>">
									<?= Yii::t('app', 'Accessory Categories') ?>
								</a>
							</li>
							<li>
								<a href="<?= Url::to(['/seo/index']) ?>">
									<?= Yii::t('app', 'Page Settings') ?>
								</a>
							</li>
							<li>
								<a href="<?= Url::to(['/landingpages/index']) ?>">
									<?= Yii::t('app', 'Landing Pages') ?>
								</a>
							</li>
							<li>
								<a href="<?= Url::to(['/marketing-images']) ?>">
									<?= Yii::t('app', 'Marketing Images') ?>
								</a>
							</li>
							<li>
								<a href="<?= Url::to(['/joboffer/job']) ?>">
									<?= Yii::t('app', 'Jobs') ?>
								</a>
							</li>
							<li>
								<a href="<?= Url::to(['/joboffer/job-category']) ?>">
									<?= Yii::t('app', 'Job-Kategorien') ?>
								</a>
							</li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=  Yii::t('app', 'Translation') ?>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?= Url::to(['/translatemanager/language/list']) ?>"><?= Yii::t('app', 'List of languages') ?></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/translatemanager/language/import']) ?>"><?= Yii::t('app', 'Import') ?></a>
							</li>
							<li>
								<a href="<?= Url::to(['/translatemanager/language/export']) ?>"><?= Yii::t('app', 'Export') ?></a>
							</li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= Url::to(['/translation']) ?>"><?= Yii::t('app', 'Create a new string') ?></a>
                            </li>
						</ul>
					</li>

                    <?php if ($user->can('starlord')): ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::t('app', 'Configuration') ?>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="<?= Url::to(['/shopconfig/update', 'id' => ShopConfig::DEINHANDY]) ?>"><?= Yii::t('app', 'Shop Configuration') ?></a>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>

                <li>
                    <a href="<?= Url::to(['/generalnews']) ?>"><?= Yii::t('app', 'News') ?>

                    </a>
                </li>
			</ul>

			<?= Html::beginForm(Url::to(['/site/logout']), 'post', ['id' => 'logout_form']) ?>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle"
					   data-toggle="dropdown"><?= Yii::$app->user->identity->username
						. ' (' . Yii::$app->user->identity->getCurrentRoleName() . ')' ?>
						<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="<?= Url::to(['/user/view', 'id' => Yii::$app->user->id]) ?>"><?= Yii::t(
									'app',
									'Profil'
								) ?></a>
                        </li>

						<?php if (Yii::$app->user->can('handleUserAccounts')): ?>
                            <li>
                                <a href="<?= Url::to(['/user/index']) ?>"><?= Yii::t('app', 'User Management') ?></a>
                            </li>
						<?php endif; ?>

						<?php if (Yii::$app->user->can('accessModelLogger')): ?>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/audittrail/index']) ?>"><?= Yii::t('app', 'Model Logger') ?></a>
								<a href="<?= Url::to(['/log-backend/index']) ?>"><?= Yii::t('app', 'Log Backend') ?></a>
								<a href="<?= Url::to(['/log-console/index']) ?>"><?= Yii::t('app', 'Log Console') ?></a>
								<a href="<?= Url::to(['/log-frontend/index']) ?>"><?= Yii::t('app', 'Log Frontend') ?></a>
							</li>
						<?php endif; ?>
						<li class="divider"></li>
						<li>

							<a href="#"
							   onclick="document.getElementById('logout_form').submit(); return false;"><?= Yii::t('app', 'Logout') ?></a>

						</li>
					</ul>
				</li>
			</ul>
			<?= Html::endForm(); ?>
			<?php else: ?>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?= Url::to(['/site/login']) ?>">Login</a></li>
				</ul>
			<?php endif; ?>
		</div><!--/.nav-collapse -->
	</div>
</nav>

<div class="container">
	<?=
	Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
	<?php
		foreach (Yii::$app->session->getAllFlashes() as $key => $messages) {
			$messages = is_array($messages) ? $messages : [$messages];
			foreach($messages as $message) {
				if (is_string($message)) {
					echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
				}
			}
		}
	?>
	<?= $content ?>
</div>


<footer class="main-footer">
	<div class="container">
		<p class="pull-left"></p>
	</div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
