<?php
use backend\assets\AppAsset;
use components\Locale;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$user = Yii::$app->user;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>

	<?php $this->head() ?>
</head>
<body id="blank-backend-layout">
<?php $this->beginBody() ?>
<div class="container">
	<?= $content ?>
</div>

<footer class="main-footer">
	<div class="container">
		<p class="pull-left">&copy; deinhandy.de <?= date('Y') ?></p>
	</div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
