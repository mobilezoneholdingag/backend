<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsletterSubscriberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $fileUpdatedAt string */

$this->title = 'Newsletter Subscribers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-subscriber-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php if ($fileUpdatedAt): ?>
		<p>
			File updated at: <?= $fileUpdatedAt ?>
		</p>
	<?php endif; ?>
	<p>
		<?= Html::a('Export newsletter-subscriber.csv', ['export-csv'], ['class' => 'btn btn-primary']) ?>
		<?php if ($fileUpdatedAt): ?>
			<?= Html::a('Download newsletter-subscriber.csv', ['download-csv'], ['class' => 'btn btn-success']) ?>
		<?php endif; ?>
	</p>

	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'id',
				'customer_id',
				'statusLabel',
				'updated_at',
				'customerEmail',
				'customerGenderLabel',
				'customerFirstName',
				'customerLastName',
				'customerLanguage',
			],
		]
	); ?>
</div>
