<?php
use \yii\widgets\ActiveForm;
use yii\helpers\Html;


$this->title = 'Bulk Coupon';
$this->params['breadcrumbs'][] = ['label' => 'Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Bulk Upload';


?>

<div class="row">
    <div class="col-sm-6">

        <?php if($insert !== false): ?>
            <div class="alert alert-success"><?= $insert; ?> Datensätze eingefügt</div>
        <?php endif; ?>

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'file')->fileInput() ?>

        <?= Html::submitButton('Upload and Process', ['class' => 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="col-sm-6">

        <div class="well">

            <h3>Aufbau CSV File</h3>

            <div class="alert alert-warning">
                <strong>Achtung: </strong> Delimiter muss ein Semikolon sein
            </div>

            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>COUPONCODE</th>
                    <th>VALIDFROM</th>
                    <th>VALIDUNTIL</th>
                    <th>MULTIPLEUSE</th>
                    <th>AM</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Coupon Code</td>
                    <td>Valid von Datum</td>
                    <td>Valid bis Datum</td>
                    <td>Mehrfach nutzbar (N = NEIN, alles andere = JA)</td>
                    <td>Rabatt auf Artikel in CHF</td>
                </tr>
                <tr>
                    <td>TESTCOUPON</td>
                    <td>01.01.2017</td>
                    <td>01.01.2018</td>
                    <td>N</td>
                    <td>10</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>