<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coupons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coupon-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create Coupon', ['create'], ['class' => 'btn btn-success']) ?>
        <?php if (Yii::$app->user->can('starlord')): ?>
            <?= Html::a('Bulk Upload', ['bulk'], ['class' => 'btn btn-warning']) ?>
        <?php endif; ?>
	</p>

	<p>
		<b>Device Price Discount</b> ist der Betrag der vom Artikelpreis abgezogen wird.
	</p>
    <div class="row">
        <div class="col-sm-6">
            <h3>Gültige Codes</h3>

            <?= GridView::widget([
                'dataProvider' => $valid,
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'id',
                    'code',
                    'discount_device_price',
                    'minimum_price',
                    'valid_from:date',
                    'valid_until:date',
                    [
                        'attribute' => 'is_valid',
                        'value' => function($model) {
                            if($model->is_valid == 1) {
                                return 'Ja';
                            }

                            if($model->valid_unique == 1) {
                                return 'Nein (eingelöst)';
                            }

                            return 'Nein';

                        }
                    ],
                    [
                        'attribute' => 'valid_unique',
                        'value' => function($model) {
                            if($model->valid_unique == 1) {
                                return 'Ja (einfach)';
                            }

                            return 'Nein (mehrfach)';

                        }
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>

        <div class="col-sm-6">

            <h3>Ungültige / Benutzte Codes</h3>

            <?= GridView::widget([
                'dataProvider' => $not_valid,
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'id',
                    'code',
                    'discount_device_price',
                    'minimum_price',
                    'valid_from:date',
                    'valid_until:date',
                    [
                        'attribute' => 'is_valid',
                        'value' => function($model) {
                            if($model->is_valid == 1) {
                                return 'Ja';
                            }

                            if($model->valid_unique == 1) {
                                return 'Nein (eingelöst)';
                            }

                            return 'Nein';

                        }
                    ],
                    [
                        'attribute' => 'valid_unique',
                        'value' => function($model) {
                            if($model->valid_unique == 1) {
                                return 'Ja (einfach)';
                            }

                            return 'Nein (mehrfach)';

                        }
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>

    </div>

</div>
