<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Coupon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coupon-form">

	<?php $form = ActiveForm::begin(); ?>

    <h3>Code</h3>
    <div class="row">
        <div class="col-sm-12"><?= $form->field($model, 'code')->textInput(['maxlength' => 16]) ?></div>
    </div>

    <h3>Preis</h3>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'discount_device_price')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'minimum_price')->textInput()->hint('Minimaler Warenkorb Wert (inklusive Gutschein)') ?>
        </div>
    </div>

    <h3>Validierung</h3>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'valid_from')->widget(\yii\jui\DatePicker::className(), [

            ])->hint('Gültig ab diesem Tag (inkl. Tag selbst)'); ?>

        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'valid_until')->widget(\yii\jui\DatePicker::className(), [

            ])->hint('Gültig bis und mit diesem Tag'); ?>

        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'valid_unique')->dropDownList($model->getIsUniqueOptions()); ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'is_valid')->dropDownList($model->getIsValidOptions()); ?>
        </div>
    </div>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

    <style>
        label.control-label { display: block; }
    </style>

</div>
