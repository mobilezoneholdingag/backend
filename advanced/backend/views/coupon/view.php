<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Coupon */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coupon-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'code',
			'discount_device_price',
			'minimum_price',
            'valid_from:date',
            'valid_until:date',
            [
                'attribute' => 'is_valid',
                'value' => ($model->is_valid == 0) ? 'Nein' : 'Ja'
            ],
            [
                'attribute' => 'valid_unique',
                'value' => ($model->is_valid == 0) ? 'Nein (mehrfach)' : 'Ja'
            ],
		],
	]) ?>

</div>
