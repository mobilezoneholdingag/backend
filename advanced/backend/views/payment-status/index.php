<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Payment Statuses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-status-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create Payment Status'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

		   // 'id',
			'code',
			'title',
			'fulfillment_partner_id',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
