<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-status-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

	<?= TabWidget::widget([
		'model' => $model,
		'form' => $form,
		'fieldsToRender' => [
			'title',
		],
		'editorOptions' => [
			'maxlength' => true
		],
		'inputType' => TabWidget::TYPE_TEXTFIELD
	]); ?>

	<?= $form->field($model, 'fulfillment_partner_id')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
