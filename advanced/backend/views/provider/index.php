<?php

use common\models\Provider;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Providers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?php if (Yii::$app->user->can('createProviders')) { ?>
		<?= Html::a(Yii::t('app', 'Create Provider', [
			'modelClass' => 'Provider',
		]), ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>

		<?=
		Html::a(Yii::t('app', 'Sort Provider', [
			'modelClass' => 'Provider',
		]), ['sort'], ['class' => 'btn btn-warning']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'eam_id',
			'id',
			'title:ntext',
			'system_title',
			[
				'attribute' => 'simcard_id',
				'format' => 'raw',
				'label' => Yii::t('app', 'default SIM ID'),
				'value' => 'simcard_id'
			],
			[
				'attribute' => 'image',
				'format' => 'raw',
				'value' => function(Provider $model) {
					return $model->image ? Html::a(
						Html::img($model->image, ['width' => 100]),
						$model->image,
						['target' => '_blank']
					) : null;
				}
			],
			[
				'attribute' => 'link_tos',
				'format' => 'raw',
				'value' => function ($model) {

					return ($model->link_tos) ? Html::a('Link', $model->link_tos) : null;
				}
			],
			'status:boolean',
			[
			        'class' => 'yii\grid\ActionColumn',
				    'template' => '{update} {view}',
            ],
		],
	]); ?>

</div>
