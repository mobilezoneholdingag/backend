<?php

use yii\helpers\Html;
use common\models\Provider;

/* @var $this yii\web\View */
/* @var $providers Provider[] */

$this->title = Yii::t('app', 'Sort Provider');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Provider'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Sort');
?>

<script>
	$(function () {
		$(".sortable").sortable({
			axis: 'y',
			update: function (event, ui) {
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/provider/sort'
				});

				$(".save_flash").fadeIn(150).delay(600).fadeOut(300);
			}
		});
	});
</script>

<div class="manufacturer-sort">

	<h2><?= Html::encode($this->title) ?></h2><br>
	<span class="save_flash" style="display: none"><?= Yii::t('app', 'saved') ?></span>
	<?= Html::beginForm(); ?>
	<ul class="sortable">
		<?php foreach ($providers as $provider): ?>
			<li id="item-<?= $provider->id ?>" class="ui-state-default">
				<img alt="<?= $provider->title ?>" src="<?= $provider->image ?>"/>
				<span><?= $provider->title ?></span>
			</li>
		<?php endforeach; ?>
	</ul>
	<?= Html::endForm(); ?>
</div>
