<?php

use deinhandy\picasso\Client;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Provider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Providers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'eam_id',
			'id',
			'title:ntext',
            'status:boolean',
			'system_title:ntext',
			[
				'attribute' => 'simCard.product_name',
				'label' => Yii::t('app', 'Default SIM Card'),
			],
			[
				'attribute' => 'image',
				'format' => 'html',
				'value' => Html::a(
					Html::img($model->image, ['width' => 100]),
					$model->image,
					['target'=>'_blank']
				),
			],
			'link_tos:ntext',
		],
	]) ?>

</div>
