<?php

use backend\forms\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use components\i18n\widget\TabWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Provider */
/* @var $form ActiveForm */
?>

<div class="provider-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'visible_homepage')->checkbox() ?>

	<?= $form->field($model, 'status')->checkbox() ?>

	<?= $form->field($model, 'renewable')->checkbox() ?>

	<?= $form->field($model, 'simcard_id')->dropDownList(
		ArrayHelper::map(\common\models\Article::getSimCards()->all(), 'id', 'productNameAndId'),
		['prompt' => Yii::t('app', 'Please select default SIM card')]
	) ?>

	<?php echo $form->field($model, 'system_title')->textInput()->label(Yii::t('app', 'URL Text')); ?>
	<?php echo $form->field($model, 'young_age_limit')->textInput()->label(Yii::t('app', 'Young Age Limit')); ?>


	<fieldset class="provider_image_input_field">
		<legend><?= Yii::t('app', 'Provider Image') ?></legend>
		<?= $form
			->field($model, 'image')
			->fileInput(['image' => true])
			->hint(Yii::t('app', 'only PNG file type allowed')); ?>

        <legend><?= Yii::t('app', 'Alternative Provider Image') ?></legend>
		<?= $form
			->field($model, 'alt_image')
			->fileInput(['image' => true])
			->hint(Yii::t('app', 'only PNG file type allowed')); ?>
	</fieldset>

	<?= $form->field($model, 'link_tos')->textInput() ?>

    <div class="form-group">
    <?= TabWidget::widget(
        [
            'model' => $model,
            'form' => $form,
            'fieldsToRender' => ['link',],
            'editorOptions' => [
                'maxlength' => 500,
            ],
            'inputType' => TabWidget::TYPE_TEXTFIELD,
        ]
    ); ?>
    </div>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
