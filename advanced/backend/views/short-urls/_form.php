<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShortUrls */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="short-urls-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

	<?= $form->beginField($model, 'url'); ?>
	<label for="url_applying"><?= Yii::t('app', 'Target URL') ?></label>
	<div class="input-group">
		<span class="input-group-addon" id="sizing-addon1">
		   <?= $this->render('../general/_renderCountryUrlInclFlag') ?>
		</span>
		<input type="text" class="form-control" aria-describedby="sizing-addon1" name="ShortUrls[url]"
			   id="url_applying" value="<?= $model->url ?>">
	</div>
	<?= $form->endField() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
