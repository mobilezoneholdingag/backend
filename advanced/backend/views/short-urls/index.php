<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ShortUrlsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Short Urls');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="short-urls-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Yii::t('app', 'Create Short Url'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'code',
			[
				'attribute' => 'url',
				'label' => Yii::t('app', 'Target URL'),
				'format' => 'raw',
				'value' => function ($model) {
					return Html::a($model->url, Yii::$app->urlManagerFrontend->hostInfo . $model->url);
				}
			],
			[
				'label' => Yii::t('app', 'Short URL'),
				'format' => 'raw',
				'value' => function ($model) {
					return Html::a(Yii::$app->urlManagerFrontend->hostInfo.'/fb/'.$model->code, Yii::$app->urlManagerFrontend->hostInfo.'/fb/'.$model->code).'<br>'.
					Html::a(Yii::$app->urlManagerFrontend->hostInfo.'/tw/'.$model->code, Yii::$app->urlManagerFrontend->hostInfo.'/tw/'.$model->code).'<br>'.
					Html::a(Yii::$app->urlManagerFrontend->hostInfo.'/ins/'.$model->code, Yii::$app->urlManagerFrontend->hostInfo.'/ins/'.$model->code);
				}
			],
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
<?php Pjax::end(); ?></div>
