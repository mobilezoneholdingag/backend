<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Product Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="devicetype-index">

	<?php if (Yii::$app->user->can('manageProductCategory')) { ?>
	<p>
		<?= Html::a(Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Devicetype',
]), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php } ?>

	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'columns' => [
				'id',
				'type',
				'related_class',
			],
		]
	); ?>

</div>
