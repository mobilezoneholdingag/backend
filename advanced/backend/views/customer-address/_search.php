<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CustomerAddressSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-address-search">

	<?php $form = ActiveForm::begin(
		[
			'action' => ['index'],
			'method' => 'get',
		]
	); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'customer_id') ?>

	<?= $form->field($model, 'is_default') ?>

	<?= $form->field($model, 'sex') ?>

	<?= $form->field($model, 'firstname') ?>

	<?php // echo $form->field($model, 'lastname') ?>

	<?php // echo $form->field($model, 'street') ?>

	<?php // echo $form->field($model, 'house_number') ?>

	<?php // echo $form->field($model, 'zip') ?>

	<?php // echo $form->field($model, 'city') ?>

	<?php // echo $form->field($model, 'contact_phone_number') ?>

	<?php // echo $form->field($model, 'additional_info') ?>

	<?php // echo $form->field($model, 'created_at') ?>

	<?php // echo $form->field($model, 'updated_at') ?>

	<div class="form-group">
		<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
