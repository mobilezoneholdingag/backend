<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\CustomerAddressSearch */

$this->title = 'Customer Addresses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-address-index">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php if (Yii::$app->user->can('starlord')): ?>
        <p>
            <?= Html::a('Create Customer Address', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_id',
            'guid',
            'is_active',
            'is_default',
            // 'sex',
            // 'firstname',
            // 'lastname',
            // 'company',
            // 'street',
            // 'house_number',
            // 'zip',
            // 'city',
            // 'country',
            // 'contact_phone_number',
            // 'additional_info',
            // 'updated_at',
            // 'created_at',

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'template' => '{view} {update}',
	        ],
        ],
    ]); ?>
</div>
