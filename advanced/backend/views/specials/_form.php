<?php

use common\models\ArticleDetails;
use common\models\ArticleDetailsGroup;
use common\models\ArticleGroups;
use common\models\DeliveryStatus;
use components\i18n\widget\TabWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<?= $this->render(Yii::$app->params['flashMessagesPath']); ?>

<div class="article-form">

	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<div class="form-group">
		<label><input type="checkbox" disabled="disabled" value="1" <?= $model->is_active_erp ? 'checked="checked"' : '' ?>> <?= Yii::t('app', 'Is Active Erp') ?></label>
	</div>

	<div class="form-group">
		<label><input type="checkbox" disabled="disabled" value="1" <?= $model->is_active ? 'checked="checked"' : '' ?>> <?= Yii::t('app', 'Is Active') ?></label>
	</div>

	<?= $form->field($model, 'mat_no')->textInput(['disabled' => 'disabled']) ?>
	<?= $form->field($model, 'product_name')->textInput(['disabled' => 'disabled']) ?>
	<?= $form->field($model, 'manufacturerTitle')->textInput(['disabled' => 'disabled']) ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => [
				'image_overlay_text',
			],
			'editorOptions' => [
				'maxlength' => 255,
			],
			'inputType' => TabWidget::TYPE_TEXTFIELD,
		]
	); ?>

	<hr>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end() ?>

</div>
