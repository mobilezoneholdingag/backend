<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = 'Update: ' . ' ' . $model->uniqueTitle;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Special'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->shortName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="article-update">

	<h1><?= Html::encode($this->title) ?></h1>
	<br>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
