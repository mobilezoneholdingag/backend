<?php

use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Article */

$this->title = Yii::t('app', 'Devices');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

	<?php { ?>
	<p>
		<?= Html::a(Yii::t('app', 'Create new Special from Article'), ['/article/index'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php } ?>
	<div class="row">
		<div class="col-md-2">
			<div class="inactiveLegendInformation"></div>
			= inaktiv
		</div>
		<div class="col-md-2">
			<?= PageSize::widget([
				'pageSizeParam' => 'per-page',
			]); ?>
		</div>
	</div>

	<?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'rowOptions' => function ($model) {
			return ($model->is_active != true) ? ['class' => 'inactiveRow'] : [];
		},
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'mat_no',
			'image_overlay_text',
			[
				'attribute' => 'manufacturerTitle',
				'format' => 'html',
				'value' => function ($model) {
					if ($model->manufacturer) {
						return Html::a($model->manufacturerTitle, Url::to(['manufacturer/view', 'id' => $model->manufacturer->id]));
					}
					return null;
				}
			],
			'productCategoryLabel',
			[
				'attribute' => 'groupTitle',
				'format' => 'html',
				'value' => function ($model) {
					if ($model->group_id)
						return Html::a($model->groupTitle, Url::to(['articlegroups/view', 'id' => $model->group_id]));
				}
			],
			[
				'attribute' => 'product_name',
				'format' => 'html',
				'value' => function ($model) {
					return Html::a($model->product_name, Url::to(['article/view', 'id' => $model->id]));
				}
			],
			'memory',
			'color_label',
			'is_active:boolean',
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update} {delete}',
			],
		],
	]); ?>

</div>
