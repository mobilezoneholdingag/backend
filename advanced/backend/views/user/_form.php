<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')->textInput(); ?>

	<?= $form->field($model, 'username')->textInput(); ?>

	<?= $form->field($model, 'password')->passwordInput(['value' => '', 'placeholder' => 'Leave empty to keep your password.']); ?>

	<?= $form->field($model, 'email')->textInput(['type' => 'email']) ?>

    <?= $form->field($model, 'role')->dropDownList(User::getPossibleRoles()) ?>

	<?= $form->field($model, 'status')->dropDownList(User::getPosssibleStatuses()) ?>


	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
