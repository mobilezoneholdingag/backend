<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a(
			Yii::t(
				'app',
				'Create {modelClass}',
				[
					'modelClass' => 'User',
				]
			),
			['signup'],
			['class' => 'btn btn-success']
		) ?>
	</p>

	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'id',
				'username',
				'name',
				'email:email',
				[
					'attribute' => 'status',
					'label' => Yii::t('app', 'Status'),
					'value' => 'statusName',
				],
				[
					'attribute' => 'role',
					'label' => Yii::t('app', 'Role'),
					'value' => 'roleName',
				],
				['class' => 'yii\grid\ActionColumn'],
			],
		]
	); ?>

</div>
