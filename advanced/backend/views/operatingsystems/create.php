<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OperatingSystems */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Operating Systems',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operating Systems'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operating-systems-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
