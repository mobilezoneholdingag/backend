<?php

use common\modules\platform\models\Platform;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OperatingSystems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operating-systems-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'version')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'platform_id')->dropDownList(
			Platform::find()->select(['id', 'name'])->createCommand()->queryAll(PDO::FETCH_KEY_PAIR),
			['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
		);
	?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
