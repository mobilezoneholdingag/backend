<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\OperatingSystems */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Operating Systems');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operating-systems-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create new OS', [
			'modelClass' => 'Operating Systems',
		]), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'title',
			'version',
			[
				'attribute' => 'platform_id',
				'value' => function ($model) {
					return Yii::$app->params['os_platforms'][$model->platform_id];
				}
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
			],
	],
	]); ?>

</div>
