<?php

use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LandingPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Landing Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landingpages-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?=
		Html::a(Yii::t('app', 'Create Landing Pages', [
			'modelClass' => 'LandingPages',
		]), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<div class="row">
		<div class="col-md-2">
			<?= PageSize::widget([
				'pageSizeParam' => 'per-page',
			]); ?>
		</div>
	</div>


	<?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'title',
			'language',
			[
				'attribute' => 'url',
				'label' => Yii::t('app', 'Url Applying'),
				'format' => 'raw',
				'value' => function ($model) {
					return '/lp/'. $model->url;
				}
			],
			'meta_description',
			//'url_parameter',
			'redirect_url',
			'canonical_url',
			'active:boolean',
			'noindex:boolean',
			'updated_at:datetime',
			// 'html_code:ntext',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
