<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LandingPages */

$this->title = Yii::t('app', 'Create Landing Pages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'LandingPages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landingpages-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
