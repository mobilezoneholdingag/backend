<?php

use common\models\LandingPages;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/** @var $this yii\web\View */
/** @var $model common\models\LandingPages */
/** @var $form yii\widgets\ActiveForm */
/** @var array $offers */
/** @var array $saleables */
/** @var int $kind */
/** @var array $allKinds */
?>

<div class="landingpages-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?= $form->field($model, 'active')->checkbox()->hint('(inactive urls are resolving in home page redirects)') ?>
<br>
	<?= $form->field($model, 'noindex')->checkbox()->hint('("no index")') ?>

	<?= $form->field($model, 'language')->dropDownList(
		LandingPages::$languages,
		['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	) ?>


    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 256])->hint("Nutze einen Asterisk (*) als erstes Zeichen, um den h1 Titel auszublenden") ?>

            <?= $form->field($model, 'headline')->textInput(['maxlength' => 256]) ?>

            <?= $form->beginField($model, 'url'); ?>
            <label for="url"><?= Yii::t('app', 'Url Applying') ?></label>

            <div class="input-group">
                <span class="input-group-addon" id="sizing-addon1"><?= $this->render('../general/_renderCountryUrlInclFlag') ?>/</span>
                <input type="text" class="form-control" aria-describedby="sizing-addon1" name="LandingPages[url]" id="url"
                       value="<?= $model->url ?>">
            </div>
            <?= $form->endField() ?>

            <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 256]) ?>
        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'redirect_url')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'canonical_url')->textInput(['maxlength' => 255])->hint('Please use a full qualified URL ("https://www...")') ?>

        </div>
    </div>

	<?php # $form->field($model, 'url_parameter')->dropDownList(\common\models\LandingPages::URL_PARAMETERS, ['prompt' => '- ' . Yii::t('app', 'please select') . ' -']) ?>

	<?php # $form->field($model, 'sitemap_category_id')->dropDownList(\common\models\LandingPages::SITEMAP_CATEGORY_LABELS, ['prompt' => '- ' . Yii::t('app', 'please select') . ' -']) ?>


	<?php /* $form->field($model, 'html_code')->widget(CKEditor::className(), [
		'options' => ['rows' => 6],
		'preset' => 'full']);
        */
    ?>

    <?= $form->field($model, 'html_code')->widget(
        \conquer\codemirror\CodemirrorWidget::className(),
        [
            'preset' => 'php',
            'settings' => [

            ]
        ])->hint("
            Siehe Anleitung hier: <a target=\"_blank\" href=\"https://deinhandy.atlassian.net/wiki/spaces/MZ/pages/75628613/Landingpage\">Confluence/Landingpage</a>
        "); ?>

    <style>
        .CodeMirror {
            height: 80vh;
            border:1px solid #999;
        }
    </style>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
