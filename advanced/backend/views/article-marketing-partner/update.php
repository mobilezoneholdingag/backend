<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\MarketingPartner */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel backend\models\ArticleMarketingPartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */
?>
<p>
	<?=
	Html::a(
		Yii::t('app', 'Back to Marketing Partners'),
		Url::toRoute(array_merge(['/marketing-partner/index'])),
		['class' => 'btn btn-success']
	);
	?>
</p>
<p style="float:left;">
	<?=
	Html::a(
		Yii::t('app', 'Add all filtered articles'),
		Url::toRoute(array_merge(['/article-marketing-partner/add-all-filtered'], $queryParams)),
		['class' => 'btn btn-success']
	);
	?>

	<?= Html::a(
		Yii::t('app', 'Remove all filtered articles'),
		Url::toRoute(array_merge(['/article-marketing-partner/remove-all-filtered'], $queryParams)),
		['class' => 'btn btn-danger']
	);
	?>
</p>
<p style="float:right;">
	<?=
		Html::a(
			Yii::t('app', 'Show articles in feed'),
			Url::toRoute(array_merge(['/article-marketing-partner/update'], ArrayHelper::merge($queryParams, ['ArticleMarketingPartnerSearch' => ['added' => '1']]))),
			['class' => 'btn btn-success']
		);
	?>
	<?=
		Html::a(
			Yii::t('app', 'Show articles not in feed'),
			Url::toRoute(array_merge(['/article-marketing-partner/update'], ArrayHelper::merge($queryParams, ['ArticleMarketingPartnerSearch' => ['added' => '0']]))),
			['class' => 'btn btn-success']
		);
	?>
</p>
<div style="clear:both;"></div>
<?php
echo GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'rowOptions' => function($article) {
			return ($article->is_active != true) ? ['class' => 'inactiveRow'] : [];
		},
		'columns' => [
			'id',
			[
				'attribute' => 'manufacturerTitle',
				'format' => 'html',
				'value' => function($article) {
					if ($article->manufacturer) {
						return Html::a(
							$article->manufacturerTitle,
							Url::to(['manufacturer/view', 'id' => $article->manufacturer->id])
						);
					}

					return null;
				},
			],
			'productCategoryLabel',
			[
				'attribute' => 'groupTitle',
				'format' => 'html',
				'value' => function($article) {
					if ($article->group_id) {
						return Html::a(
							$article->groupTitle,
							Url::to(['articlegroups/view', 'id' => $article->group_id])
						);
					}

					return null;
				},
			],
			[
				'attribute' => 'product_name',
				'format' => 'html',
				'value' => function($article) {
					return Html::a($article->product_name, Url::to(['article/view', 'id' => $article->id]));
				},
			],
			'stockQuantity',
			'shortName',
			'memory',
			'color_label',
			[
				'attribute' => 'deliveryStatus',
				'value' => function($article) {
					if ($article->deliveryStatus) {
						return $article->deliveryStatus->label;
					} else {
						return null;
					}
				},
			],
			'updated_at:datetime',
			'is_active:boolean',
			'is_active_erp:boolean',
			[
				'class' => 'yii\grid\ActionColumn',
				'header' => 'Actions',
				'template' => '{add}',
				'buttons' => [
					'add' => function($url, $product, $key) use ($existingIds, $model, $queryParams) {
						if (in_array($product->id, $existingIds)) {
							return Html::a(
								Html::button(Yii::t('app', 'Remove'), ['class' => 'btn-sm btn-danger']),
								Url::toRoute(
									array_merge(
										$queryParams,
										[
											'article-marketing-partner/update',
											'id' => $model->id,
											'remove' => $product->id,
											'add' => '',
										]
									)
								)
							);
						}

						return Html::a(
							Html::button(Yii::t('app', 'Add'), ['class' => 'btn-sm btn-success']),
							Url::toRoute(
								array_merge(
									$queryParams,
									[
										'article-marketing-partner/update',
										'id' => $model->id,
										'add' => $product->id,
										'remove' => '',
									]
								)
							)
						);
					},
				],
			],
		],
	]
);

?>

