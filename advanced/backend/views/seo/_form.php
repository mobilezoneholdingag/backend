<?php

use components\Locale;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Seo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'visible')->checkbox() ?>

	<?= $form->field($model, 'page_title')->textInput(['maxlength' => 255]) ?>

	<?= $form->beginField($model, 'url_applying'); ?>
	<label for="url_applying"><?= Yii::t('app', 'Url Applying') ?></label>
	<div class="input-group">
		<span class="input-group-addon" id="sizing-addon1">
		   <?= $this->render('../general/_renderCountryUrlInclFlag') ?>
		</span>
		<input type="text" class="form-control" aria-describedby="sizing-addon1" name="Seo[url_applying]"
			   id="url_applying" value="<?= $model->url_applying ?>">
	</div>
	<?= $form->endField() ?>

	<?= $form->field($model, 'page_description')->textInput(['maxlength' => 2048]) ?>

	<?= $form->field($model, 'html_content')->widget(CKEditor::className(), [
		'options' => ['rows' => 6],
		'preset' => 'full']); ?>

	<?= $form->field($model, 'redirect_url')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'canonical_url')->textInput(['maxlength' => 255]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
