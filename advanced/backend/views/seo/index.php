<?php

use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\SetsSearch */

$this->title = Yii::t('app', 'Page Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create new setting'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<div class="row">
		<div class="col-md-2">
			<?= PageSize::widget([
				'pageSizeParam' => 'per-page',
			]); ?>
		</div>
	</div>


	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'page_title',
			[
				'attribute' => 'url_applying',
				'label' => Yii::t('app', 'Url Applying'),
				'format' => 'raw',
				'value' => function ($model) {
					return Html::a($model->url_applying, Yii::$app->urlManagerFrontend->hostInfo . $model->url_applying);
				}
			],

			'page_description',
			[
				'attribute' => 'visible',
				'value' => function ($model) {
					return ($model->visible) ? 'index' : 'noindex';
				}
			],
			// 'html_content',
			'redirect_url:url',
			'canonical_url:url',
			'updated_at:datetime',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
