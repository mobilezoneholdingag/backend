<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Special */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="special-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => ['special_title',],
			'editorOptions' => [
				'maxlength' => true,
			],
			'inputType' => TabWidget::TYPE_TEXTFIELD,
		]
	); ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => ['special_description',],
			'editorOptions' => [
				'maxlength' => true,
			],
			'inputType' => TabWidget::TYPE_TEXTAREA,
		]
	); ?>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? 'Create' : 'Update',
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
