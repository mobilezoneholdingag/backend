<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CertificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Certifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="certification-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a('Create Certification', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				'id',
				'type',

				['class' => 'yii\grid\ActionColumn'],
			],
		]
	); ?>
</div>
