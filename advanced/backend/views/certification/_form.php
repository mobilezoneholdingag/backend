<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Certification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="certification-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= TabWidget::widget(
		[
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => ['type'],
			'editorOptions' => [
				'maxlength' => true,
			],
			'inputType' => TabWidget::TYPE_TEXTFIELD,
		]
	); ?>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? 'Create' : 'Update',
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
