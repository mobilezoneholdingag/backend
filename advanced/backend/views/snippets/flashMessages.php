<?php if ($errors = Yii::$app->session->getFlash('error')): ?>
	<?php if (!is_array($errors))
		$errors = [ $errors ];
	?>
	<?php foreach($errors as $error): ?>
		<p class="flashMessage background-danger"><?= $error; ?></p>
	<?php endforeach; ?>
<?php endif; ?>
<?php if ($successes = Yii::$app->session->getFlash('success')): ?>
	<?php if (!is_array($successes))
		$successes = [ $successes ];
	?>
	<?php foreach($successes as $success): ?>
		<p class="flashMessage background-success"><?= $success; ?></p>
	<?php endforeach; ?>
<?php endif; ?>