<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $allModels common\models\Article[] */


$this->title = Yii::t('app', 'Browser Monitoring');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-browser">

	<iframe src="https://rpm.newrelic.com/public/charts/1OlNLFb2piU" width="99%" height="400" scrolling="no"
			frameborder="no"></iframe>
	<iframe src="https://rpm.newrelic.com/public/charts/QZlKnsf2NT" width="99%" height="400" scrolling="no"
			frameborder="no"></iframe>
	<iframe src="https://rpm.newrelic.com/public/charts/7YYBNEFGzkm" width="99%" height="400" scrolling="no"
			frameborder="no"></iframe>
	<iframe src="https://rpm.newrelic.com/public/charts/fmZ5p8FjFxU" width="99%" height="400" scrolling="no"
			frameborder="no"></iframe>


</div>
