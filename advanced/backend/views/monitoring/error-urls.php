<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $allModels common\models\Article[] */


$this->title = Yii::t('app', 'Error Urls');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-index">
	<h2>Monitoring requests with 404 & 500 HTTP status codes</h2>
	<div class="body-content">
		<div class="new_relic_container">

			<iframe width="99%" height="600"
					src="https://insights-embed.newrelic.com/embedded_widget/KkgcMPZIDz9FQ1XOZr1R4ku4r3g7HcJo"
					frameborder="0"></iframe>
			<p>"404" Counter</p>

			<iframe width="99%" height="600"
					src="https://insights-embed.newrelic.com/embedded_widget/0I8lF3Vfayx8qf5laqmQ8qQnfQKw9wwO"
					frameborder="0"></iframe>
			<p>"404" in the last hour</p>

			<iframe width="99%" height="600"
					src="https://insights-embed.newrelic.com/embedded_widget/3nmscx5fC9tceuQ64EGbjQG21qNEkE7P"
					frameborder="0"></iframe>
			<p>"500" in the last week</p>

			<iframe width="99%" height="300"
					src="https://insights-embed.newrelic.com/embedded_widget/h9ERpp_evsaFXPPMhDnySsW2dhgxggIp"
					frameborder="0"></iframe>
			<p>"404" in the last 24 hours</p>
		</div>
	</div>
</div>
