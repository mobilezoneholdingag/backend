<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $allModels common\models\Article[] */


$this->title = Yii::t('app', 'Server Monitoring');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-server">

	<iframe src="https://rpm.newrelic.com/public/charts/6eviCRfFf4o" width="99%" height="300" scrolling="no"
			frameborder="no"></iframe>
	<a href="http://www.codediesel.com/software/measuring-user-effectiveness-with-the-apdex-metric/"
	   target="_blank">What is the Apdex Score?</a>
	<iframe src="https://rpm.newrelic.com/public/charts/i0m5s3Rm4rk" width="99%" height="300" scrolling="no"
			frameborder="no"></iframe>

</div>
