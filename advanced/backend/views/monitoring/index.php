<?php
$this->title = Yii::t('app', 'Monitoring Overview');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="monitoring-overview row">
	<div class="col-md-4">
		<!-- Average page load time DE -->
		<iframe src="https://rpm.newrelic.com/public/charts/6lXEjzB6dhP" scrolling="no" frameborder="no"></iframe>
		<!-- Throughput DE -->
		<iframe src="https://rpm.newrelic.com/public/charts/dDW4IZchdY5" scrolling="no" frameborder="no"></iframe>
		<!-- Error Rate DE -->
		<iframe src="https://rpm.newrelic.com/public/charts/9sfIWhCYCuv" scrolling="no" frameborder="no"></iframe>
	</div>
	<div class="col-md-4">
		<!-- Apdex DE -->
		<iframe src="https://rpm.newrelic.com/public/charts/8Oi3Jc9oGi0" scrolling="no" frameborder="no"></iframe>
		<!-- Average response time DE -->
		<iframe src="https://rpm.newrelic.com/public/charts/kTEZmI64fyp" scrolling="no" frameborder="no"></iframe>
		<div>
			<a id="analytics_link_de" style="display: none;">Analytics Login</a>
			<div id="active_users_de" class="active-users"></div>
		</div>

	</div>
	<div class="col-md-4">
		<!-- Apdex CH -->
		<iframe src="https://rpm.newrelic.com/public/charts/iP4OFsIjdhr" scrolling="no" frameborder="no"></iframe>
		<!-- Average page load time CH -->
		<iframe src="https://rpm.newrelic.com/public/charts/7Sa4crIxBSk" scrolling="no" frameborder="no"></iframe>
		<!-- Error Rate CH -->
		<iframe src="https://rpm.newrelic.com/public/charts/jusXz9KtxOu" class="error-rate-ch" scrolling="no" frameborder="no"></iframe>
		<div class="active-users-ch">
			<a id="analytics_link_ch" style="display: none;">Analytics Login</a>
			<div id="active_users_ch" class="active-users"></div>
		</div>
	</div>
</div>

<script type="application/javascript">

	function activeUserNumber(countryCode, activeUsersId, authLinkId)
	{
		this.countryCode = 'countryCode=' + countryCode;
		this.activeUsersElement = $('#' + activeUsersId);
		this.authLinkElement = $('#' + authLinkId);
		var that = this;
		setInterval(function() {
			// try to get the active user count asynchronously
			$.get('<?= Yii::$app->getUrlManager()->getHostInfo() . '/monitoring/active-users' ?>', that.countryCode, function (data, status) {
				if (data > -1) {
					that.authLinkElement.hide();
					that.activeUsersElement.text(data);
					that.activeUsersElement.show();
				} else {
					// try to get the authentication url
					$.get('<?= Yii::$app->getUrlManager()->getHostInfo() . '/monitoring/authentication-url' ?>', function (data, status) {
						that.activeUsersElement.hide();
						that.authLinkElement.attr('href', data);
						that.authLinkElement.show();
					});
				}
			})
		}, 5000)
	}

	$(document).ready(function() {
		var activeUsersDE = new activeUserNumber('DE', 'active_users_de', 'analytics_link_de');
		var activeUsersCH = new activeUserNumber('CH', 'active_users_ch', 'analytics_link_ch');
	})

</script>