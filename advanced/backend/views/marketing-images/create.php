<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MarketingImages */

$this->title = Yii::t('app', 'Create Marketing Images');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Marketing Images'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marketing-images-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model')) ?>

</div>
