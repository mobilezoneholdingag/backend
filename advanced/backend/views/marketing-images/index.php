<?php

use common\models\MarketingImages;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MarketingImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Marketing Images');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marketing-images-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(
			Yii::t('app', 'Create') . " " . Yii::t('app', 'Marketing Image'),
			['create'],
			['class' => 'btn btn-success']
		) ?>
	</p>
	<?php if (Yii::$app->session->hasFlash('upload_success')) { ?>
		<div class="alert alert-success" role="alert"><?= Yii::$app->session->getFlash('upload_success') ?></div>
	<?php } ?>

	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				'id',
				'tag',
				'language',
				'target_url:url',
				'updated_at',
				'created_at',
				'publish_at',
				[
					'attribute' => 'image',
					'format' => 'raw',
					'value' => function(MarketingImages $model) {
						return $model->image ? Html::a(
							Html::img($model->image, ['width' => 100]),
							$model->image,
							['target' => '_blank']
						) : null;
					}
				],

				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{view} {update}',
				],
			],
		]
	); ?>
</div>
