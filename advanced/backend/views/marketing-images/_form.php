<?php

use yii\helpers\Html;
use backend\forms\ActiveForm;
use common\models\MarketingImages;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model MarketingImages */
/* @var $form ActiveForm */
?>

<div class="marketing-images-form">

	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

	<fieldset class="provider_image_input_field">
		<legend><?= $model->attributeLabels()['image'] ?></legend>
		<?= $form->field($model, 'image')->fileInput(['image' => true]); ?>
	</fieldset>

	<?= $form->field($model, 'target_url')->textInput(['maxlength' => true])->hint('Wenn leer, wird der Banner nicht angezeigt') ?>

	<?= $form->field($model, 'alt_text')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'language')->dropDownList(
		MarketingImages::LANGUAGES,
		['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	) ?>

	<?= $form->field($model, 'tag')->dropDownList(
		MarketingImages::BANNER_POSITIONS,
		['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	) ?>

    <?= $form->field($model, 'link_target')->dropDownList(
            $model->getLinkTargetOptions(),
            ['prompt' => Yii::t('app', 'please select')]
    ) ?>

	<?= $form->field($model, 'publish_at')->widget(DatePicker::className(), ['clientOptions' => [
			'minDate' => 0
	]]) ?>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
