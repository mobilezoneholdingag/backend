<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\set\models\Sets */
/* @var $products common\models\Article | common\models\Tariffs */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sets-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Sort'), ['sort', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'title:ntext',
			'system_title:ntext',
			'description:ntext',
			[
				'attribute' => 'productList',
				'value' => $model->id/*function ($model) {
					foreach ($model->productList as $product) {
						return var_dump($product);
					}

				}*/
			]
		],
	]) ?>

</div>

<div>
	<ul style="list-style: none">
		<?php foreach ($products as $key => $product):
			if ($product):
				?>
				<li>
					<img alt="<?= $product->uniqueTitle ?>"  width="auto" height="100px"
						 src="<?= ($product->hasImage() ? $product->getSmallImageUrl() : null) ?>"/>
					<span><?= $product->uniqueTitle ?></span>
				</li><br>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>
</div>
