<?php

use common\modules\set\models\Sets;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\set\models\Sets */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel backend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<style type="text/css">
	#w1 img {
		max-height: 30px;
	}
</style>

<div class="sets-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= empty($dataProvider) === false ? Html::a(Html::button(Yii::t('app', 'Sort'), ['class' => 'btn btn-warning']), Url::to(['sort', 'id' => $model->id])) : ''; ?>
	</div>

	<?= $form->field($model, 'title')->textInput() ?>

	<?= $form->field($model, 'system_title')->textInput() ?>

	<?= $form->field($model, 'description')->textarea(['row' => 3]) ?>

	<label class="control-label" for="sets-description"><?= Yii::t('app', 'In this set') ?></label>
	<?php if ($model->type === Sets::TYPE_ARTICLE): ?>
        <p><strong><?= Yii::t('app', 'Please add only one version of the article to the set.') ?></strong></p>
	<?php else: ?>
		<p><strong>Hier werden nur Tarife aufgelistet, die aktiv sind (sowohl im ERP als auch hier)
				und mindestens einen aktiven Artikeltarif besitzen.</strong></p>
	<?php endif; ?>
	<ul style="list-style: none">
		<?php

		if (count($model->chart ) == 0) {
			echo Yii::t('app', 'No entries');
		}

		$existingIds = [];

		foreach ($model->chart as $chart) {
			$product = $chart->getProductModel();
			$existingIds[] = $product->getId();
			?>
			<li>
				<img alt="<?= $product->getUniqueTitle() ?>" width="auto" height="100px"
					src="<?= ($product->hasImage() ? $product->getSmallImageUrl() : '#') ?>"/>
				<span><?= $product->getUniqueTitle() ?></span>
				<?=
				Html::a(
					Html::button(Yii::t('app', 'Remove'), ['class' => 'btn-sm btn-danger']),
					Url::current(['remove' => $product->id, 'add' => ''])
				);
				?>
			</li><br>
		<?php } ?>
	</ul>

	<?php ActiveForm::end(); ?>

	<?php

	if (empty($dataProvider) === false) {

		switch ($model->type) {
			case Sets::TYPE_ARTICLE:
				$specificColumns = [
					[
						'attribute' => 'manufacturerTitle',
						'format' => 'html',
						'value' => function ($model) {
							if ($model->manufacturer) {
								return Html::a($model->manufacturerTitle, Url::to(['manufacturer/view', 'id' => $model->manufacturer->id]));
							}
							return null;
						}
					],
					'productCategoryLabel',
					[
						'attribute' => 'groupTitle',
						'format' => 'html',
						'value' => function ($model) {
							if ($model->group_id) {
								return Html::a($model->groupTitle, Url::to(['articlegroups/view', 'id' => $model->group_id]));
							}
							return null;
						}
					],
					[
						'attribute' => 'product_name',
						'format' => 'html',
						'value' => function ($model) {
							return Html::a($model->product_name, Url::to(['article/view', 'id' => $model->id]));
						}
					],
					'stockQuantity',
					'shortName',
					'memory',
					'color_label',
					[
						'attribute' => 'deliveryStatus',
						'value' => function ($model) {
							if ($model->deliveryStatus):
								return $model->deliveryStatus->label;
							else:
								return null;
							endif;
						}
					],
				];
				break;
			case Sets::TYPE_TARIFF:
				$specificColumns = [
					[
						'attribute' => 'title',
						'format' => 'html',
						'value' => function ($model) {
							return Html::a($model->title, Url::to(['tariffs/update', 'id' => $model->id]));
						}
					],
					[
						'attribute' => 'providerTitle',
						'format' => 'html',
						'label' => Yii::t('app', 'Provider'),
						'value' => function ($model) {
							return (isset($model->provider)) ? Html::a(Html::img($model->provider->image, ['alt' => $model->provider->title]), Url::to(['provider/view', 'id' => $model->provider->id])) : null;
						}
					],
					'offerTypeTitle',
					'price_monthly:currency',
					'price_monthly_runtime',
				];
				break;
			default:
				throw new RuntimeException('Set type "' . $model->type . '" not able to be updated.');
		}
	?>
	<?php if ($model->type === Sets::TYPE_ARTICLE): ?>
        <p><strong><?= Yii::t('app', 'Please add only one version of the article to the set.') ?></strong></p>
	<?php else: ?>
		<p><strong>Hier werden nur Tarife aufgelistet, die aktiv sind (sowohl im ERP als auch hier)
				und mindestens einen aktiven Artikeltarif besitzen.</strong></p>
	<?php endif; ?>
    <?php

		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'filterSelector' => 'select[name="per-page"]',
			'rowOptions' => function ($model) {
				return ($model->is_active != true) ? ['class' => 'inactiveRow'] : [];
			},
			'columns' => array_merge(
				[
					['class' => 'yii\grid\SerialColumn'],
					'id',
				],
				$specificColumns,
				[
					'updated_at:datetime',
					'is_active:boolean',
					['class' => 'yii\grid\ActionColumn',
						'header' => 'Actions',
						'template' => '{add}',
						'buttons' => [
							'add' => function ($url, $product, $key) use ($existingIds) {
								if (in_array($product->id, $existingIds)) {
									return Html::a(
										Html::button(Yii::t('app', 'Remove'), ['class' => 'btn-sm btn-danger']),
										Url::current(['remove' => $product->id, 'add' => ''])
									);
								}

								return Html::a(
									Html::button(Yii::t('app', 'Add'), ['class' => 'btn-sm btn-success']),
									Url::current(['add' => $product->id, 'remove' => ''])
								);
							},
						],
					],
				]
			),
		]);

	?>

	<?php } ?>

</div>
