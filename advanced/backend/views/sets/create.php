<?php

use common\modules\set\models\Sets;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\set\models\Sets */
/* @var $productList array */

$title = (Yii::$app->request->getQueryParam('type') == Sets::TYPE_TARIFF) ? 'Tariff' : 'Device';
$this->title = Yii::t('app', 'Create') . " " . Yii::t('app', $title.' Set');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sets-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?=
	$this->render('_form', [
		'model' => $model,
	]) ?>

</div>
