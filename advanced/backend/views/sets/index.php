<?php

use common\modules\set\models\Sets;
use common\models\ProductCategory;
use nterms\pagesize\PageSize;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\SetsSearch */

$this->title = Yii::t('app', 'Sets');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sets-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create') . " " . Yii::t('app', 'Device Set'), ['create?type=' . Sets::TYPE_ARTICLE], ['class' => 'btn btn-primary']) ?>
        <?php if (Yii::$app->user->can('starlord')) { ?>
		<?= Html::a(Yii::t('app', 'Create') . " " . Yii::t('app', 'Tariff Set'), ['create?type=' . Sets::TYPE_TARIFF], ['class' => 'btn btn-primary']) ?>
        <?php } ?>
	</p>

	<div class="row">
		<div class="col-md-2">
			<?= PageSize::widget([
				'pageSizeParam' => 'per-page',
			]); ?>
		</div>
	</div>


	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
		'columns' => [
			'id',
			'title:ntext',
			'system_title:ntext',
			'typeTitle',
			['class' => 'yii\grid\ActionColumn',
				'contentOptions' => ['style' => 'width:350px;'],
				'header' => 'Actions',
				'template' => '{view} {update} {sort} {delete}',
				'buttons' => [
					'view' => function ($url, $model, $key) {
						return Html::a(Html::button(Yii::t('app', 'View'), ['class' => 'btn-sm btn-info']), Url::to(['view', 'id' => $model->id]));
					},
					'update' => function ($url, $model, $key) {
						return Html::a(Html::button(Yii::t('app', 'Update'), ['class' => 'btn-sm btn-primary']), Url::to(['update', 'id' => $model->id]));
					},
					'sort' => function ($url, $model, $key) {
						return Html::a(Html::button(Yii::t('app', 'Sort'), ['class' => 'btn-sm btn-warning']), Url::to(['sort', 'id' => $model->id]));
					},
					'delete' => function ($url, $model, $key) {
						return Html::a(Html::button(Yii::t('app', 'Delete'), ['class' => 'btn-sm btn-danger']), ['sets/delete', 'id' => $model->id],
							['title' => Yii::t('app', 'Delete'), 'data-confirm' => "Wollen Sie diesen Eintrag wirklich löschen?", 'data-method' => "post", 'data-pjax' => "0"]);
					},
				],
			],
		],
	]); ?>

</div>
