<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\set\models\Sets */
/* @var $searchModel backend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Sets',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<?php echo $this->render(Yii::$app->params['flashMessagesPath']); ?>
<div class="sets-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
		'searchModel' => $searchModel,
		'dataProvider' => $dataProvider,
	]) ?>

</div>
