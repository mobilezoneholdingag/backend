<?php

use yii\helpers\Html;
use common\models\Article;
use common\models\Tariffs;

/* @var $this yii\web\View */
/* @var $model common\modules\set\models\Sets */
/* @var $products Article | Tariffs */

$this->title = Yii::t('app', 'Sort Set') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Sort');
?>

<script>
	$(function () {
		$(".sortable").sortable({
			axis: 'y',
			update: function (event, ui) {
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/sets/sort?id=<?= $model->id ?>'
				});

				$(".save_flash").fadeIn(150).delay(600).fadeOut(300);
			}
		});
	});
</script>

<div class="sets-sort">

	<h2><?= Html::encode($this->title) ?></h2><br>
	<span class="save_flash" style="display: none"><?= Yii::t('app', 'saved') ?></span>
	<?= Html::beginForm(); ?>
	<ul class="sortable">
		<?php foreach ($products as $key => $product): ?>
			<li id="item-<?= $product->id ?>" class="ui-state-default">
                <img alt="<?= $product->getUniqueTitle() ?>" width="auto" height="100px"
                     src="<?= ($product->hasImage() ? $product->getSmallImageUrl() : '#') ?>"/>
				<span><?= $product->uniqueTitle ?></span>
			</li>
		<?php endforeach; ?>
	</ul>
	<?= Html::endForm(); ?>
</div>
