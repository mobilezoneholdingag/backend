<?php

use backend\models\MarketingPartner;
use common\modules\feed\models\FeedAbstract;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Marketing Partners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marketing-partner-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create Feed'), ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a(Yii::t('app', 'Delete Log'), ['delete-feed-log'], ['class' => 'btn btn-danger']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			'id',
			'title',
			[
				'attribute' => 'filename',
				'format' => 'url',
				'value' => function($marketingPartner) {
					return rtrim(Yii::$app->getModule('feed')->params['feed_base_url'], '/') . '/' . $marketingPartner->filename;
				},
			],
			[
				'attribute' => 'logicType',
				'format' => 'raw',
				'value' => function(MarketingPartner $marketingPartner) {
					$form = ActiveForm::begin([
						'action' => Url::toRoute(['/marketing-partner/update', 'id' => $marketingPartner->id, 'overview' => true]),
						'fieldConfig' => [
							'options' => ['tag' => false],
							'template' => '{input}'
						]
					]);
					$formHtml = Html::beginForm($form->action, $form->method, $form->options);
					$formHtml .= $form->field($marketingPartner, 'logic')->dropDownList(
						FeedAbstract::getLogicTypeLabels(),
						[
								'prompt' => '--- Kein Logik-Typ (keinen Feed generieren) ---',
								'onChange' => "$(this).closest('form').submit()",
						]
					)->label(false);
					ActiveForm::end();
					$formHtml .= Html::endForm();

					return $formHtml;
				}
			],
			'last_file_generated_at',
			[
				'class' => 'yii\grid\ActionColumn',
				'header' => 'Actions',
				'template' => '{view} {update} {add} {delete}',
				'buttons' => [
					'add' => function($url, $marketingPartner) {
						return Html::a(
								'<span class="glyphicon glyphicon-plus"></span>',
								Url::toRoute(['article-marketing-partner/update', 'id' => $marketingPartner->id]),
								[
									'title' => Yii::t('app', 'Add articles'),
								]
						);
					},
				],
			],
		],
	]);	?>

	<?php
	foreach ($feeds as $feed) {
		echo date('Y-m-d H:i',$feed->log_time) . ' = ' . str_replace(' [-][-][-][info][feed]', '', $feed->message) . '</br>';
	}
	?>

</div>
