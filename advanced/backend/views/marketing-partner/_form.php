<?php

use common\modules\feed\models\FeedAbstract;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MarketingPartner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marketing-partner-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'filename')->textInput() ?>
	<?= $form->field($model, 'warranty')->textInput(['type' => 'number']) ?>

	<?php
		if ($model->isNewRecord === false) {
			echo $form->field($model, 'logic')->dropDownList(
					FeedAbstract::getLogicTypeLabels(),
					['prompt' => '--- Kein Logik-Typ (keinen Feed generieren) ---']
			);
		}
	?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
