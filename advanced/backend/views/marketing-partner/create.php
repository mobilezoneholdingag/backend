<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MarketingPartner */

$this->title = Yii::t('app', 'Create Marketing Partner');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Marketing Partners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marketing-partner-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
