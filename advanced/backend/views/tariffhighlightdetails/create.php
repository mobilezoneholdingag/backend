<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TariffHighlightDetails */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Tarifhighlight Details',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifhighlight Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifhighlight-details-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
