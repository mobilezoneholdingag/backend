<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TariffHighlightDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifhighlight-details-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= TabWidget::widget([
		'model' => $model,
		'form' => $form,
		'fieldsToRender' => [
			'title',
			'subtext',
		],
		'inputType' => TabWidget::TYPE_TEXTFIELD
	]); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
