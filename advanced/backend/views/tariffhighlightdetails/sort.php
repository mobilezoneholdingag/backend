<?php

use yii\helpers\Html;
use common\models\Article;
use common\models\Tariffs;

/* @var $this yii\web\View */
/* @var $highlights Article | Tariffs */

$this->title = Yii::t('app', 'Sort Tariff Highlights');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tariff Highlights'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Sort Tariff Highlights', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Sort');
?>

<script>
	$(function () {
		$(".sortable").sortable({
			axis: 'y',
			update: function (event, ui) {
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/tariffhighlightdetails/sort'
				});

				$(".save_flash").fadeIn(150).delay(600).fadeOut(300);
			}
		});
	});
</script>

<div class="sets-sort">

	<h2><?= Html::encode($this->title) ?></h2><br>
	<span class="save_flash" style="display: none"><?= Yii::t('app', 'saved') ?></span>
	<?= Html::beginForm(); ?>
	<ul class="sortable">
		<?php foreach ($highlights as $key => $highlight): ?>
			<li id="item-<?= $highlight->id ?>" class="ui-state-default">
				<!--img alt="<?= $highlight->css_class ?>" src=""/-->
				<span><?= $highlight->title.' '.$highlight->subtext ?></span>
			</li>
		<?php endforeach; ?>
	</ul>
	<?= Html::endForm(); ?>
</div>
