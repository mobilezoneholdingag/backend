<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tariff highlights');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifhighlight-details-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create tariff highlights', [
		]), ['create'], ['class' => 'btn btn-success']) ?>

		<?= Html::a(Yii::t('app', 'Sort tariff highlights', [
		]), ['sort'], ['class' => 'btn btn-warning']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'title:ntext',
			'subtext:ntext',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
