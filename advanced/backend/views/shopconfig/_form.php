<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\modules\set\models\Sets;
use common\models\ProductCategory;

/* @var $this yii\web\View */
/* @var $model common\models\ShopConfig */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-config-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'top10smartphones_set_id')->dropDownList(ArrayHelper::map(Sets::find()->where(['type' => ProductCategory::SMARTPHONE])->all(), 'id', 'title'),['prompt' => '- '.Yii::t('app', 'please select').' -']) ?>

	<?php /* $form->field($model, 'id')->textInput() ?>

	<?= $form->field($model, 'kategoriebaum')->textInput() ?>

	<?= $form->field($model, 'design_id')->textInput() ?>

	<?= $form->field($model, 'absender_name')->textInput(['maxlength' => 80]) ?>

	<?= $form->field($model, 'absender_email')->textInput(['maxlength' => 80]) ?>

	<?= $form->field($model, 'kontakt_email')->textInput(['maxlength' => 80]) ?>

	<?= $form->field($model, 'fusszeile_email')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'fusszeile_1')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'fusszeile_2')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'fusszeile_3')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'meta_angaben')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'css_angaben')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'html_code_links')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'seitentitle')->textInput(['maxlength' => 256]) ?>

	<?= $form->field($model, 'info_einkauf')->textInput() ?>

	<?= $form->field($model, 'marge_handy')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'marge_mixer')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'versand_1')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'versand_2')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'versand_3')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'versand_4')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'versand_5')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'versand_handy')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'versand_grenze')->textInput(['maxlength' => 6]) ?>

	<?= $form->field($model, 'nachnahme')->textInput(['maxlength' => 3]) ?>

	<?= $form->field($model, 'bestelltext')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'bestelltext_vertrag')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'lieferzeit_1')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'lieferzeit_2')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'lieferzeit_3')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'lieferzeit_4')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'lieferzeit_5')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'lieferzeit_6')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'lieferzeit_7')->textInput(['maxlength' => 40]) ?>

	<?= $form->field($model, 'zahlung_vorkasse')->textInput() ?>

	<?= $form->field($model, 'zahlung_nachnahme')->textInput() ?>

	<?= $form->field($model, 'zahlung_lastschrift')->textInput() ?>

	<?= $form->field($model, 'zahlung_paypal')->textInput() ?>

	<?= $form->field($model, 'email_paypal')->textInput(['maxlength' => 80]) ?>

	<?= $form->field($model, 'text_vorkasse')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'text_nachnahme')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'versandart')->textInput() ?>

	<?= $form->field($model, 'schufatext')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'html_einkauf')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'html_festnetz')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'html_newsletter')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'html_ssl_einkauf')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'html_ssl_festnetz')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'html_ssl_newsletter')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'giveaway_article_id')->textInput() ?>

	<?= $form->field($model, 'giveaway')->textInput() */?>



	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
