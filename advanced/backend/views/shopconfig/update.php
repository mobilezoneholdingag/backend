<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ShopConfig */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Shop Config',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shop Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="shop-config-update">
	<?php echo $this->render(Yii::$app->params['flashMessagesPath']); ?>

	<h1><?= Html::encode($this->title) ?></h1>
<p style="text-align: center; padding: 20px">
	<?= Html::a(Html::button(Yii::t('app', 'RESET CACHE'), ['class' => 'btn btn-danger btn-lg']), '/shopconfig/update?id=1&cache-reset=1'); ?>
	<br><span style="font-size: x-small">(<?= $model->publish_token; ?>)</span>
</p>
	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
