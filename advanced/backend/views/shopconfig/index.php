<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Shop Configs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-config-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Shop Config',
]), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			//'id',
			//'kategoriebaum',
			//'design_id',
			//'absender_name',
			//'absender_email:email',
			// 'kontakt_email:email',
			// 'fusszeile_email:ntext',
			// 'fusszeile_1:ntext',
			// 'fusszeile_2:ntext',
			// 'fusszeile_3:ntext',
			// 'meta_angaben:ntext',
			// 'css_angaben:ntext',
			// 'html_code_links:ntext',
			// 'seitentitle',
			// 'info_einkauf',
			// 'marge_handy',
			// 'marge_mixer',
			// 'versand_1',
			// 'versand_2',
			// 'versand_3',
			// 'versand_4',
			// 'versand_5',
			// 'versand_handy',
			// 'versand_grenze',
			// 'nachnahme',
			// 'bestelltext:ntext',
			// 'bestelltext_vertrag:ntext',
			// 'lieferzeit_1',
			// 'lieferzeit_2',
			// 'lieferzeit_3',
			// 'lieferzeit_4',
			// 'lieferzeit_5',
			// 'lieferzeit_6',
			// 'lieferzeit_7',
			// 'zahlung_vorkasse',
			// 'zahlung_nachnahme',
			// 'zahlung_lastschrift',
			// 'zahlung_paypal',
			// 'email_paypal:email',
			// 'text_vorkasse:ntext',
			// 'text_nachnahme:ntext',
			// 'meta_description:ntext',
			// 'meta_keywords:ntext',
			// 'versandart',
			// 'schufatext:ntext',
			// 'html_einkauf:ntext',
			// 'html_festnetz:ntext',
			// 'html_newsletter:ntext',
			// 'html_ssl_einkauf:ntext',
			// 'html_ssl_festnetz:ntext',
			// 'html_ssl_newsletter:ntext',
			// 'giveaway_article_id',
			// 'giveaway',
			// 'null_prozent_aktion',
			 'top10smartphones_set_id',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
