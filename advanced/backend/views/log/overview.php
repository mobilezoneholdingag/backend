<?php use yii\grid\GridView;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\LogBackendSearch */

echo GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'level',
			[
				'attribute' => 'category',
				'filter' => $searchModel->getAllCategories(),
			],
			'log_time:datetime',
			'prefix:ntext',
			'message:ntext',

			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {delete}',
			],
		],
	]
);
