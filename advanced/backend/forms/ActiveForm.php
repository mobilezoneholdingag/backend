<?php

namespace backend\forms;

/**
 * @method ActiveField field($model, $attribute, $options = [])
 */
class ActiveForm extends \yii\widgets\ActiveForm
{
	public $fieldClass = ActiveField::class;
}
