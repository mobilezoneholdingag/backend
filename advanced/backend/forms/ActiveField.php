<?php

namespace backend\forms;

use deinhandy\yii2picasso\models\Image;
use deinhandy\yii2picasso\models\ImageCollection;
use yii\helpers\Html;

class ActiveField extends \yii\widgets\ActiveField
{
	const DEFAULT_FILE_COUNT = 8;

	public function fileInput($options = [])
	{
		parent::fileInput($options);

		if (isset($options['image'])) {
			$this->addImagePreviews();
		}

		return $this;
	}

	public function dropfileInput($options = [])
	{
		unset($options['multiple'], $options['accept']);
		$this->fileInput($options);
		unset($options['id']);

		$attribute = str_replace(['[', ']'], '', $this->attribute);
		$this->parts['{input}'] = Html::tag('div', $this->getImageCollectionFormInput($options, $attribute), [
			'class' => "{$attribute}-container dropzone-images",
			'data-id' => $this->model->id,
		]);
		$this->parts['{input}'] .= Html::tag('div', '', ['style' => 'clear: both;']);

		return $this;

	}

	private function getImageCollection()
	{
		$attribute = str_replace(['[', ']'], '', $this->attribute);
		$imageCollection = $this->model->{$attribute};

		if (!$imageCollection instanceof ImageCollection) {
			return new ImageCollection();
		}

		return $imageCollection;
	}

	private function addImagePreviews()
	{
		$imageCollection = $this->getImageCollection();

		$this->parts['{input}'] = Html::tag('p', $this->parts['{input}']);

		$this->addImageWrapper($imageCollection);
	}

	private function addImageWrapper($imageCollection)
	{
		$imagesHtml = '';
		foreach ($imageCollection->images as $image) {

			if (!($image instanceof Image)) {
				continue;
			}

			$imagesHtml .= Html::tag('div', $this->getImagePreview($image), ['class' => 'dropFile']);
		}
		$this->parts['{input}'] .= Html::tag('div', $imagesHtml, ['class' => 'dropFiles']);
	}

	private function getImagePreview(Image $image): string
	{
		return Html::img($image, ['width' => '600', 'class' => 'img-responsive']);
	}

	private function getImageCollectionFormInput(array $options = [], string $attribute = ''): string
	{
		$imageCollection = $this->getImageCollection();
		$fileCount = isset($options['files']) ? $options['files'] : self::DEFAULT_FILE_COUNT;
		$inputHtml = '';

		for ($i = 0; $i < $fileCount; $i++) {
			$inputHtml .= Html::tag('div', '', [
				'class' => 'form-group droppable',
				'id' => "dropzone-{$i}",
				'data-preview' => $imageCollection[$i],
				'data-name' => Html::getInputName($this->model, "{$attribute}[{$i}]"),
				'data-original-position' => 'images-' . $i,
			]);
		}

		return $inputHtml;
	}
}
