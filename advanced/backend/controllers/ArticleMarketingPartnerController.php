<?php
namespace backend\controllers;

use backend\models\ArticleMarketingPartnerSearch;
use backend\models\MarketingPartner;
use common\models\ArticleMarketingPartner;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Command;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ArticleMarketingPartnerController extends Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view', 'create', 'update', 'delete', 'sort', 'add-all-filtered', 'remove-all-filtered'],
						'allow' => true,
						'roles' => ['user'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function actionUpdate($id)
	{
		$marketingPartnerModel = MarketingPartner::findOne($id);
		$this->saveNewArticles($marketingPartnerModel->id);
		$this->removeArticles($marketingPartnerModel->id);
		$marketingPartnerArticleIds = $this->getArticleIdsForMarketingPartner($id);
		$searchModel = new ArticleMarketingPartnerSearch();
		$dataProvider = $this->getDataProvider($searchModel);
		$params = Yii::$app->request->queryParams;
		unset($params['add'], $params['remove']);

		return $this->render('update', [
			'model' => $marketingPartnerModel,
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'existingIds' => $marketingPartnerArticleIds,
			'queryParams' => $params,
		]);
	}

	protected function getDataProvider(ArticleMarketingPartnerSearch $searchModel)
	{
		return $searchModel->search(Yii::$app->request->queryParams);
	}

	protected function getArticleIdsForMarketingPartner($partnerId)
	{
		$marketingPartnerArticleIds = ArticleMarketingPartner::find()
			->where(['partner_id' => $partnerId])
			->select('article_id')
			->asArray(true)
			->column();
		return $marketingPartnerArticleIds;
	}

	public function actionAddAllFiltered($id)
	{
		return $this->handleFilteredArticles($id, 'saveNewArticles');
	}

	public function actionRemoveAllFiltered($id)
	{
		return $this->handleFilteredArticles($id, 'removeArticles');
	}

	protected function handleFilteredArticles($partnerId, $callback)
	{
		$params = Yii::$app->request->queryParams;
		$marketingPartnerModel = MarketingPartner::findOne($partnerId);
		if (isset($marketingPartnerModel) === false) {
			Yii::$app->session->addFlash('Marketing Partner', "The marketing partner with id $partnerId does not exist.");
			return $this->redirect('marketing-partner/index');
		}

		$searchModel = new ArticleMarketingPartnerSearch();
		$dataProvider = $this->getDataProvider($searchModel);
		/** @var $query ActiveQuery */
		$query = $dataProvider->query;
		$query->asArray(true);
		$query->select(
			[
				ArticleMarketingPartnerSearch::tableName() . '.id',
				ArticleMarketingPartnerSearch::tableName() . '.group_id',
				ArticleMarketingPartnerSearch::tableName() . '.delivery_status_id',
				ArticleMarketingPartnerSearch::tableName() . '.manufacturer_id',
				ArticleMarketingPartnerSearch::tableName() . '.product_category_id'
			]
		);

		foreach ($query->batch() as $articles) {
			$articleIds = [];
			foreach ($articles as $article) {
				$articleIds[] = $article['id'];
			}
			$result = call_user_func([$this, $callback], $partnerId, $articleIds);
			if ($result === false) {
				Yii::$app->session->addFlash(
					'warning',
					"Something went wrong on updating the dependencies"
					. " between marketing partner $partnerId and his articles."
				);
			}
		}

		return $this->redirect(array_merge(['update'], $params));
	}

	protected function saveNewArticles($partnerId, array $articleIdsToAdd = null) : bool
	{
		$articleIdsToAdd = $articleIdsToAdd ?? (array)Yii::$app->request->get('add');
		if (empty($articleIdsToAdd) || empty($articleIdsToAdd[0])) {
			return true;
		}

		$articleIdsToInsert = [];
		foreach ($this->getNotExistingArticleIds($partnerId, $articleIdsToAdd) as $articleIdToAdd) {
			$articleIdsToInsert[] = [$partnerId, $articleIdToAdd];
		}

		if (empty($articleIdsToInsert) === false) {
			$command = new Command(['db' => ArticleMarketingPartner::getDb()]);
			$command->batchInsert(
				ArticleMarketingPartner::tableName(),
				['partner_id', 'article_id'],
				$articleIdsToInsert
			)->execute();
		}

		return true;
	}

	private function getNotExistingArticleIds($partnerId, $articleIdsToCheck)
	{
		$existingArticleIds = $this->getExistingArticleIds($partnerId, $articleIdsToCheck);

		$resultIds = array_diff($articleIdsToCheck, $existingArticleIds);

		return $resultIds;
	}

	protected function removeArticles($partnerId, array $idsToRemove = null) : bool
	{
		$idsToRemove = $idsToRemove ?? (array)Yii::$app->request->get('remove');
		if (empty($idsToRemove) || empty($idsToRemove[0])) {
			return true;
		}

		$articleIdsToRemove = $this->getExistingArticleIds($partnerId, $idsToRemove);

		if (empty($articleIdsToRemove) === false) {
			ArticleMarketingPartner::deleteAll(['partner_id' => $partnerId, 'article_id' => $articleIdsToRemove]);
		}

		return true;
	}

	private function getExistingArticleIds($partnerId, $idsToCheck)
	{
		return ArticleMarketingPartner::find()
			->where(['article_id' => $idsToCheck, 'partner_id' => $partnerId])
			->asArray(true)
			->column();
	}
}
