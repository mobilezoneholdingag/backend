<?php

namespace backend\controllers;

use backend\assets\AppAsset;
use backend\models\ArticleSearch;
use common\models\Article;
use common\models\ArticleDetailsValues;
use deinhandy\yii2picasso\models\ImageCollection;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update', 'update-image-order'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'user'],
					],
					[
						'actions' => ['create', 'delete'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * @param string $id the ID of this controller.
	 * @param Module $module the module that this controller belongs to.
	 * @param array $config name-value pairs that will be used to initialize the object properties.
	 */
	public function __construct($id, $module, $config = [])
	{
		Yii::$app->view->registerJsFile('/js/multiUpload.js', ['depends' => AppAsset::className()]);

		parent::__construct($id, $module, $config);
	}

	/**
	 * Lists all Article models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ArticleSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Article model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Article model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Article();
		$model->setArticleDetailsValues(new ArticleDetailsValues());

		if ($model->load(Yii::$app->request->post()) && $model->articleDetailsValues->load(Yii::$app->request->post()) && $model->save()) {

			$model->articleDetailsValues->article_id = $model->id;

			if (!$model->save()) {
				Yii::$app->getSession()->addFlash('error', Yii::t('app', 'An error occurred by saving. See details below.'));
			} else if ($model->articleDetailsValues->load(Yii::$app->request->post()) && $model->articleDetailsValues->save()) {

				if (Yii::$app->request->post('saveDetailsForAllVariations', false)) {
					$model->saveDetailsForAllVariations();
				}
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Article model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		/** @var Article $model */
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {
			$session = Yii::$app->getSession();
			if (!$model->save()) {
				$session->addFlash('error', Yii::t('app', 'An error occurred by saving. See details below.'));
			} else {
				$detailsValues = $model->articleDetailsValues;
				if ($detailsValues->load(Yii::$app->request->post())) {
					if (!$detailsValues->save()) {
						$session->addFlash(
							'error',
							Yii::t('app', 'An error occurred by saving:' . var_export($detailsValues->getErrors(), true))
						);
					} else {
						if (Yii::$app->request->post('saveDetailsForAllVariations', false)) {
							$model->saveDetailsForAllVariations();
						}
						return $this->redirect(['view', 'id' => $model->id]);
					}
				}
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionUpdateImageOrder($id)
	{
		/** @var Article $model */
		$model = $this->findModel($id);
		$imagePositions = array_map('intval', Yii::$app->request->post('images', []));

		$oldCollection = $model->images;
		$newCollection = new ImageCollection();

		foreach ($imagePositions as $newPosition => $oldPosition) {
			$newCollection->images[$newPosition] = $oldCollection[$oldPosition];
		}
		$model->images = $newCollection;
		$model->save();

		return json_encode($imagePositions);
	}

	/**
	 * Deletes an existing Article model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Article model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Article the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		/** @var Article $model */
		if (($model = Article::findOne($id)) !== null) {
			$details = ArticleDetailsValues::findOne($id);
			if ($details) {
				$model->articleDetailsValues = $details;
			} else {
				$model->articleDetailsValues = new ArticleDetailsValues(['article_id' => $model->id]);
			}
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
