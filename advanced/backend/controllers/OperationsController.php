<?php

namespace backend\controllers;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use components\services\OperationsService;
use deinhandy\Jira\Issue;
use yii\helpers\Url;
use yii\web\Controller;

class OperationsController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'issues'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		return Url::to('/');
	}

	/**
	 * Issue overview on what is going to be deployed.
	 */
	public function actionIssues()
	{
		/** @var OperationsService $opService */
		$opService = \Yii::$app->get('operations');

		$tickets = $opService->getNewTickets();
		$list = [];

		/** @var Issue $ticket */
		foreach ($tickets as $ticket) {
			$listItem = [];
			$listItem['status'] = $ticket->getStatus() ? $ticket->getStatus()['name'] : '';
			$listItem['assignee'] = $ticket->getAssignee() ? $ticket->getAssignee()['displayName'] : '';
			$listItem['title'] = $ticket->getSummary() ?: '';
			$listItem['type'] = $ticket->getIssueType() ? $ticket->getIssueType()['name'] : '';
			$listItem['link'] = $opService->urlPrefix . '/' . $ticket->getKey();
			$listItem['key'] = $ticket->getKey();

			$list[] = $listItem;
		}

		return $this->render(
			'issues',
			[
				'ticketList' => $list,
			]
		);
	}
}