<?php

namespace backend\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\ArticleDetailsSearch;
use common\models\ArticleDetails;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * ArticleDetailsController implements the CRUD actions for ArticleDetails model.
 */
class ArticledetailsController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update', 'edit-all', 'delete'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'user'],
					],
					[
						'actions' => ['create', 'delete'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * Lists all ArticleDetails models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ArticleDetailsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = false;

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single ArticleDetails model.
	 * @param integer $detail_id
	 * @return mixed
	 */
	public function actionView($detail_id)
	{
		return $this->render('view', [
			'model' => $this->findModel($detail_id),
		]);
	}

	/**
	 * Creates a new ArticleDetails model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new ArticleDetails();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'detail_id' => $model->detail_id, 'sort_id' => $model->sort_id, 'gruppen_id' => $model->gruppen_id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing ArticleDetails model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $detail_id
	 * @return mixed
	 */
	public function actionUpdate($detail_id)
	{
		$model = $this->findModel($detail_id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates certain values of all models
	 * @return mixed
	 */
	public function actionEditAll()
	{
		$models = ArticleDetails::find()->all();

		if (Yii::$app->request->post()) {
			foreach (Yii::$app->request->post()['result'] as $id => $detail) {
				$model = $this->findModel($id);
				$model->detail_name = $detail['name'];
				$model->group_id = $detail['gruppe'];
				$model->unit = $detail['unit'];
				$model->save();
			}
			return $this->redirect(['edit-all']);
			/* if ($models->load(Yii::$app->request->post()) && $model->save()) {
				 return $this->redirect(['view', 'detail_id' => $model->detail_id, 'sort_id' => $model->sort_id, 'gruppen_id' => $model->gruppen_id]);
			 } */
		}
		return $this->render('edit-all', [
			'models' => $models,
		]);
	}

	/**
	 * Deletes an existing ArticleDetails model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $detail_id
	 * @return mixed
	 */
	public function actionDelete($detail_id)
	{
		$this->findModel($detail_id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the ArticleDetails model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $detail_id
	 * @return ArticleDetails the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($detail_id)
	{
		if (($model = ArticleDetails::findOne(['detail_id' => $detail_id])) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
