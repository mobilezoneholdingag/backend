<?php

namespace backend\controllers;

use backend\models\ArticleGroupsSearch;
use backend\models\ArticleGroupsSimSearch;
use common\models\ArticleGroups;
use common\models\ArticleGroupsSim;
use common\models\ArticleGroupsTariff;
use common\models\ArticleTariffs;
use common\models\OfferType;
use common\models\ProductCategory;
use frontend\controllers\articletariff\SearchAction;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ArticlegroupsController implements the CRUD actions for ArticleGroups model.
 */
class ArticlegroupsController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update', 'create', 'delete'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'user'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}
	/**
	 * Lists all ArticleGroups models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ArticleGroupsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single ArticleGroups model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new ArticleGroups model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$articleGroupSimSearchModel = new ArticleGroupsSimSearch();
		$dataProvider = $articleGroupSimSearchModel->search(null);
		$model = new ArticleGroups();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}

		return $this->render('create', [
			'model' => $model,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Updates an existing ArticleGroups model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return string|\yii\web\Response
	 */
	public function actionUpdate($id)
	{
		$articleGroupModel = $this->findModel($id);
		$articleGroupSimSearchModel = new ArticleGroupsSimSearch();
		$dataProvider = $articleGroupSimSearchModel->search($id);
		$articleGroupTariffProvider = $this->getArticleGroupTariffProvider($id);

		if ($articleGroupModel->load(Yii::$app->request->post()) && $articleGroupModel->validate()) {
			if ($articleGroupModel->save()) {
				$this->saveArticleGroupSims($id);
				$this->saveArticleGroupTariffs($id);
				return $this->redirect(['view', 'id' => $articleGroupModel->id]);
			}
		}

		if (isset($articleGroupModel->errors['articleGroupTariffs'])) {
			Yii::$app->getSession()->addFlash('danger', $articleGroupModel->errors['articleGroupTariffs'][0]);
		}

		return $this->render(
			'update',
			[
				'model' => $articleGroupModel,
				'dataProvider' => $dataProvider,
				'articleGroupTariffProvider' => $articleGroupTariffProvider,
			]
		);
	}

	/**
	 * Deletes an existing ArticleGroups model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the ArticleGroups model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return ArticleGroups the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = ArticleGroups::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	private function getArticleGroupTariffProvider($id)
	{

		$articleGroup = ArticleGroups::findOne($id);
		$variationIds = $articleGroup->getArticles()->select(['id'])->column();
		$categoryId = $articleGroup->product_category_id;

		if (empty($variationIds)) {
			return $this->buildResultDataProvider([]);
		}

		$articleTariffQuery = ArticleTariffs::find()
			->joinWith(['tariff t'], false)
			->joinWith(['tariff.provider p'], false)
			->select(['p.title provider_title', 'tariff_id', 't.provider_id', 't.erp_title'])
			->andWhere('article_id IN (' . (implode(',', $variationIds)) . ')');

		SearchAction::joinWithTariffAndEnsureIsActive($articleTariffQuery);
		SearchAction::applyOrderAndGrouping($articleTariffQuery);

		switch ($categoryId) {
			case ProductCategory::SMARTPHONE:
				$articleTariffQuery->andWhere(['t.offer_type_id' => OfferType::NEW_CONTRACT_TYPE]);
				break;
			case ProductCategory::TABLET:
				$articleTariffQuery->andWhere(['t.offer_type_id' => OfferType::NEW_DATA_CONTRACT_TYPE]);
				break;
		}

		$articleTariffs = $articleTariffQuery
			->asArray()
			->all();

		$result = [];

		foreach ($articleTariffs as $articleTariff) {
			$provider_id = $articleTariff['provider_id'];

			if (!isset($result[$provider_id])) {
				$result[$provider_id] = [
					'tariffs' => [],
					'title' => $articleTariff['provider_title'],
					'id' => $provider_id,
				];
			}

			$tariffTitle = $articleTariff['erp_title'] . ' ID: ' . $articleTariff['tariff_id'];
			$result[$provider_id]['tariffs'][$articleTariff['tariff_id']] = $tariffTitle;
		}

		return $this->buildResultDataProvider($result);
	}

	private function buildResultDataProvider($result) : ArrayDataProvider
	{
		return new ArrayDataProvider(
			[
				'allModels' => $result,
				'sort' => [
					'attributes' => ['title'],
				],
			]
		);
	}

	private function saveArticleGroupSims($id)
	{
		foreach (Yii::$app->request->post()['ArticleGroupSim'] as $providerId => $simId) {
			/** @var ArticleGroupsSim $articleGroupSimModel */
			$articleGroupSimModel = ArticleGroupsSim::find()->where(
				['provider_id' => $providerId, 'article_group_id' => $id]
			)->one();
			if (empty($simId)) {
				if (null !== $articleGroupSimModel) {
					$articleGroupSimModel->delete();
				}
			} else {
				if (null !== $articleGroupSimModel) {
					$articleGroupSimModel->simcard_id = $simId;
				} else {
					$articleGroupSimModel = new ArticleGroupsSim();
					$articleGroupSimModel->article_group_id = $id;
					$articleGroupSimModel->simcard_id = $simId;
					$articleGroupSimModel->provider_id = $providerId;
				}
				$articleGroupSimModel->save(false);
			}
		}
	}

	private function saveArticleGroupTariffs($id)
	{
		$articleGroupsData = Yii::$app->request->post('ArticleGroups');

		if (!isset($articleGroupsData['articleGroupTariffs'])) {
			return;
		}

		foreach ($articleGroupsData['articleGroupTariffs'] as $providerId => $tariffId) {
			/** @var ArticleGroupsTariff $articleGroupTariffModel */
			$articleGroupTariffModel = ArticleGroupsTariff::find()->where(
				['provider_id' => $providerId, 'article_group_id' => $id]
			)->one();
			if (empty($tariffId)) {
				if ($articleGroupTariffModel !== null) {
					$articleGroupTariffModel->delete();
				}
			} else {
				if ($articleGroupTariffModel !== null) {
					$articleGroupTariffModel->tariff_id = $tariffId;
				} else {
					$articleGroupTariffModel = new ArticleGroupsTariff();
					$articleGroupTariffModel->article_group_id = $id;
					$articleGroupTariffModel->tariff_id = $tariffId;
					$articleGroupTariffModel->provider_id = $providerId;
				}
				$articleGroupTariffModel->save(false);
			}
		}
	}
}
