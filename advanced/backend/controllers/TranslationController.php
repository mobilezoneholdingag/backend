<?php

namespace backend\controllers;


use lajax\translatemanager\models\LanguageSource;
use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;


class TranslationController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    public function actionIndex(){

        $categories = LanguageSource::find()->select('category')->distinct()->all();
        $dropdown = Array();

        foreach ($categories as $category){
            $dropdown[$category->category] = $category->category;
        }
        $request  = Yii::$app->request;

        if(Yii::$app->request->post("LanguageSource")["category"] && Yii::$app->request->post("LanguageSource")["message"]) {

            $source = new LanguageSource();
            $source->category = Yii::$app->request->post("LanguageSource")["category"];
            $source->message = Yii::$app->request->post("LanguageSource")["message"];
            $source->save();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your string has been added'));
        }

        return $this->render('create', [
            'model' => new LanguageSource(),
            'dropdown' => $dropdown
        ]);
    }


}
