<?php

namespace backend\controllers;

use backend\models\LogConsole;
use backend\models\LogConsoleSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LogConsoleController implements the CRUD actions for LogConsole model.
 */
class LogConsoleController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$searchModel = new LogConsoleSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render(
			'index',
			[
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]
		);
	}

	public function actionView($id)
	{
		return $this->render(
			'view',
			[
				'model' => $this->findModel($id),
			]
		);
	}

	protected function findModel($id)
	{
		if (($model = LogConsole::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}
}
