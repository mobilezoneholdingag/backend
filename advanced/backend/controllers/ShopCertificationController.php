<?php

namespace backend\controllers;

use backend\models\CertificationSearch;
use backend\models\ShopCertification;

/**
 * ShopCertificationController implements the Update actions for ShopCertification model.
 */
class ShopCertificationController extends AbstractShopUpdateController
{
	protected static $tableField = 'certification_id';

	protected static $searchModelClass = CertificationSearch::class;

	protected function getModelClass()
	{
		return ShopCertification::class;
	}
}
