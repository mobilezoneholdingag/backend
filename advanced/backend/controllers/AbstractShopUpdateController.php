<?php

namespace backend\controllers;

use backend\models\CertificationSearch;
use backend\models\SpeakLanguageSearch;
use backend\models\SpecialSearch;
use common\models\Shop;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Command;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

abstract class AbstractShopUpdateController extends Controller
{
	protected static $tableField;

	protected static $searchModelClass;

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'update',
						],
						'allow' => true,
						'roles' => ['user'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function actionUpdate($id)
	{
		$shopModel = Shop::findOne($id);
		$this->addItem($shopModel->id);
		$this->removeItem($shopModel->id);
		$idsForShop = $this->getItemIdsForShop($id);
		$searchModel = new static::$searchModelClass;
		$dataProvider = $this->getDataProvider($searchModel);
		$params = Yii::$app->request->queryParams;
		unset($params['add'], $params['remove']);

		return $this->render(
			'update',
			[
				'model' => $shopModel,
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'existingIds' => $idsForShop,
				'queryParams' => $params,
			]
		);
	}

	/**
	 * @return \yii\db\ActiveRecord
	 */
	abstract protected function getModelClass();

	protected function addItem($shopId, array $itemIdsToAdd = null): bool
	{
		$itemIdsToAdd = $itemIdsToAdd ?? (array)Yii::$app->request->get('add');
		if (empty($itemIdsToAdd) || empty($itemIdsToAdd[0])) {
			return true;
		}

		$itemIdsToInsert = [];
		foreach ($this->getNonExistingItemIds($shopId, $itemIdsToAdd) as $itemIdToAdd) {
			$itemIdsToInsert[] = [$shopId, $itemIdToAdd];
		}

		if (empty($itemIdsToInsert) === false) {
			$command = new Command(['db' => $this->getModelClass()::getDb()]);
			$command->batchInsert(
				$this->getModelClass()::tableName(),
				['shop_id', static::$tableField],
				$itemIdsToInsert
			)->execute();
		}

		return true;
	}

	private function getNonExistingItemIds($shopId, $certificationIdsToCheck)
	{
		$existingCertificationIds = $this->getExistingItemIds($shopId, $certificationIdsToCheck);

		$resultIds = array_diff($certificationIdsToCheck, $existingCertificationIds);

		return $resultIds;
	}

	private function getExistingItemIds($shopId, $idsToCheck)
	{

		return $this->getModelClass()::find()
			->where([static::$tableField => $idsToCheck, 'shop_id' => $shopId])
			->asArray(true)
			->column();
	}

	protected function removeItem($shopId, array $idsToRemove = null): bool
	{
		$idsToRemove = $idsToRemove ?? (array)Yii::$app->request->get('remove');
		if (empty($idsToRemove) || empty($idsToRemove[0])) {
			return true;
		}

		$this->getModelClass()::deleteAll(['shop_id' => $shopId, static::$tableField => $idsToRemove]);

		return true;
	}

	protected function getItemIdsForShop($shopId)
	{
		$shopCertificationIds = $this->getModelClass()::find()
			->select(static::$tableField)
			->where(['shop_id' => $shopId])
			->asArray(true)
			->column();

		return $shopCertificationIds;
	}

	/**
	 * @param SpeakLanguageSearch|CertificationSearch|SpecialSearch $searchModel
	 *
	 * @return ActiveDataProvider
	 */
	protected function getDataProvider($searchModel)
	{
		return $searchModel->search(Yii::$app->request->queryParams);
	}
}
