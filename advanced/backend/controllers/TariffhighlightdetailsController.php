<?php

namespace backend\controllers;

use common\models\TariffHighlightDetails;
use common\models\TariffHighlights;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TariffHighlightDetailsController implements the CRUD actions for TariffHighlightDetails model.
 */
class TariffhighlightdetailsController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update', 'create', 'delete', 'sort'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'user'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function beforeAction($action)
	{
		if ($action->id == 'sort' && Yii::$app->request->isAjax) {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}

	/**
	 * Lists all TariffHighlightDetails models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$dataProvider = new ActiveDataProvider([
			'query' => TariffHighlightDetails::find()->orderBy('sort'),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single TariffHighlightDetails model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new TariffHighlightDetails model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new TariffHighlightDetails();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing TariffHighlightDetails model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing TariffHighlightDetails model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		TariffHighlights::deleteAll(['detail_id' => $id]);

		return $this->redirect(['index']);
	}


	public function actionSort()
	{
		if (Yii::$app->request->post('item', false)) {
			$i = 1;
			foreach (Yii::$app->request->post('item') as $key => $item) {
				$highlight = TariffHighlightDetails::findOne($item);
				$highlight->sort = $i;
				$highlight->save();
				$i++;
			}

		}
		$models = $this->findAllModels();
		$highlights = [];
		foreach ($models as $highlight) {

			$highlights[$highlight->sort] = $highlight;

		}
		ksort($highlights);

		return $this->render('sort', [
			'highlights' => $highlights,
		]);
	}

	/**
	 * Finds the TariffHighlightDetails model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return TariffHighlightDetails the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = TariffHighlightDetails::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findAllModels()
	{
		if (($models = TariffHighlightDetails::find()->orderBy('sort')->all())) {
			return $models;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
