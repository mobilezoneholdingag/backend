<?php

namespace backend\controllers;

use backend\models\LogConsole;
use Symfony\Component\Process\ProcessBuilder;
use Yii;
use backend\models\MarketingPartner;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class MarketingPartnerController extends Controller
{

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['create', 'delete', 'update', 'export-feed', 'view', 'delete-feed-log'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'user', 'starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '300');
		$dataProvider = new ActiveDataProvider([
			'query' => MarketingPartner::find(),
		]);

		$feedLog = LogConsole::findAll(['category' => 'feed']);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'feeds' => $feedLog,
		]);
	}

	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	public function actionCreate()
	{
		$model = new MarketingPartner();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdate($id, $overview = false)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([$overview ? 'index' : 'view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	public function actionDeleteFeedLog()
	{
		/** @var LogConsole $feedLog */
		LogConsole::deleteAll(['category' => 'feed']);
		return $this->redirect(['index']);
	}

	protected function findModel($id)
	{
		if (($model = MarketingPartner::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionExportFeed($id)
	{
		$marketingPartner = $this->findModel($id);

		if (!$marketingPartner->getLogicType()) {
			Yii::$app->session->addFlash('danger', 'Sie müssen einen Logik-Typ festlegen, um den Feed exportieren zu können!');

			return $this->redirect('index');
		}

		$builder = new ProcessBuilder(['./yii', 'feed/feed/generate-feed-by-marketing-partner-id', $id]);
		$builder->setTimeout(1800);
		$builder->setWorkingDirectory(Yii::getAlias('@app/..'));
		$builder->getProcess()->start();


		Yii::$app->session->addFlash('success', 'Feed-Generierung erfolgreich angestoßen!');

		return $this->redirect('index');
	}
}
