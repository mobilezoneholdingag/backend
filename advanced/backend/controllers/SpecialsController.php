<?php

namespace backend\controllers;

use backend\models\ArticleSearch;
use common\models\Article;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * SpecialsController implements the CRUD actions for Article model.
 */
class SpecialsController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['user'],
					],
					[
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * Lists all Article models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ArticleSearch();
		$dataProvider = $searchModel->searchSpecials();

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Updates an existing Article model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException
	 */
	public function actionUpdate($id)
	{
		$error = null;
		/** @var Article $model */
		if (($model = Article::findOne($id)) === null) {
			throw new NotFoundHttpException('An error occurred - model not found. See details below.');
		}

		if ($model->load(Yii::$app->request->post())) {
			if (!$model->save()) {
				Yii::$app->getSession()->addFlash('error', Yii::t('app', 'An error occurred while saving. See details below.'));
			} else {
				return $this->redirect('index', 301);
			}
		}

		if(null !== $error) {

		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * @param $id
	 *
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 */
	public function actionDelete($id)
	{
		/** @var Article $model */
		if (($model = Article::findOne($id)) === null) {
			throw new NotFoundHttpException('An error occurred - model not found. See details below.');
		}

		foreach ($model->translationsArr['image_overlay_text'] as $lang => $trans) {
			$model->translationsArr['image_overlay_text'][$lang]['translation'] = null;
		}

		if (!$model->save()) {
			Yii::$app->getSession()->addFlash('error', Yii::t('app', 'An error occurred while deleting. See details below.'));
		}

		return $this->redirect(['index']);
	}
}
