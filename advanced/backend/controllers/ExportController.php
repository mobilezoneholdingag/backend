<?php

namespace backend\controllers;

use components\services\ExportService;
use yii\web\Controller;
use backend\models\ExportSearch;
use common\models\Export;
use Yii;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ExportController implements the CRUD actions for Export model.
 */
class ExportController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['create', 'delete', 'update', 'download'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}
	/**
	 * Lists all Export models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ExportSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Export model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Export model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Export();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
				'formatList' => $this->getFormatList(),
				'partners' => $this->getPartnersList()
			]);
		}
	}

	/**
	 * @return null|ExportService
	 * @throws \yii\base\InvalidConfigException
	 */
	protected function getService()
	{
		return Yii::$app->get('export');
	}

	/**
	 * @return array
	 * @throws \yii\base\InvalidConfigException
	 */
	protected function getFormatList()
	{
		/** @var ExportService $exportService */
		$exportService = $this->getService();

		if ($exportService) {
			$formatList = $exportService->formatFactory->getFormatList();
		} else {
			$formatList = ['' => '---'];
		}

		return $formatList;
	}

	protected function getPartnersList()
	{
		$exportService = $this->getService();
		return $exportService->getPartnersList();
	}

	/**
	 * Updates an existing Export model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
				'formatList' => $this->getFormatList(),
				'partners' => $this->getPartnersList()
			]);
		}
	}

	/**
	 * Deletes an existing Export model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Export model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Export the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Export::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * Provide a download of the feed, which will be determined by the given key.
	 *
	 * @param string $key
	 * @return array ExportService\partners\BasePartner
	 */
	public function actionDownload($key)
	{
		/** @var Export $export */
		$export = Export::find()->where(['key' => $key])->one();
		Yii::$app->response->setDownloadHeaders($export->getFilename(), 'text/csv');
		Yii::$app->response->format = Response::FORMAT_RAW;
		$f = fopen('php://output', 'w');

		/** @var ExportService $service */
		$service = $this->getExportService();
		Yii::$app->response->send();
		return $service->exportToStreams([$key => $f]);
	}

	/**
	 * Returns the export object.
	 *
	 * @return null|object
	 * @throws \yii\base\InvalidConfigException
	 */
	protected function getExportService()
	{
		return Yii::$app->get('export');
	}
}
