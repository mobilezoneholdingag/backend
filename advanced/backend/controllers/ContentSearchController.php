<?php

namespace backend\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;


class ContentSearchController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['user'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public $searchTerm;

	public function actionIndex($searchTerm = null)
	{
		$this->searchTerm = $searchTerm;
		$dataProvider = new ArrayDataProvider([
			'allModels' => $this->find(),
			'sort' => [
				'attributes' => ['section', 'attribute', 'url', 'snippet'],
			],
			'pagination' => [
				'pageSize' => 500,
			],
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	public function find()
	{
		$result = [];
		$sectionsAndAttributes = Yii::$app->params['contentSearchSectionsAndAttributes'];

		if ($this->searchTerm) {
			/**
			 * @var $section ActiveRecord
			 */
			foreach ($sectionsAndAttributes as $section => $attributes) {
				$subResult = [];
				foreach ($attributes as $attribute) {
					//$entry = $section::find()->where(['like', $attribute, $this->searchTerm])->select(['id', $attribute])->asArray()->all();
					$entry = $section::findBySql('SELECT id, '.$attribute.' FROM ' . $section::tableName() .
						' WHERE '.$attribute.' LIKE BINARY "%' . addslashes($this->searchTerm) . '%"')->asArray()->all();
					if (!empty($entry)) {
						$cleanedEntries = [];
						foreach ($entry as $item) {
							$cleanedEntries[$item['id']] = $item[$attribute];
						}
						$subResult[$attribute] = $cleanedEntries;
					}
				}
				$result["$section"] = $subResult;
			}
			$result = $this->prepareDataForResultPage($result);
		}

		return $result;
	}

	protected function prepareDataForResultPage($result)
	{
		$preparedData = [];
		foreach ($result as $sectionNameSpace => $section) {
			$model = new $sectionNameSpace;
			$labels = $model->attributeLabels();
			$nameSpaceArr = explode('\\', $sectionNameSpace);
			$sectionName = end($nameSpaceArr);
			foreach ($section as $attribute => $values) {
				foreach ($values as $id => $value) {
					$preparedData[] = [
						'section' => $sectionName,
						'attribute' => $labels[$attribute],
						'url' => \Yii::$app->request->hostInfo . Url::to([strtolower($sectionName) . '/update', 'id' => $id]),
						'snippet' => $this->getSnippet($value),
					];
				}
			}
		}

		return $preparedData;
	}

	protected function getSnippet($value)
	{
		$pos = strpos($value, $this->searchTerm);
		$pos = ($pos > 20) ? $pos - 20 : $pos;
		$snippet = substr($value, $pos, 60);
		return (($pos > 20) ? '... ' : '') . $snippet . ' ...';
	}
}
