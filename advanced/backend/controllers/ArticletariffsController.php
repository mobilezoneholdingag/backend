<?php

namespace backend\controllers;

use backend\models\ArticleTariffsSearch;
use common\models\ArticleTariffs;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ArticleTariffsController implements the CRUD actions for ArticleTariffs model.
 */
class ArticletariffsController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['create', 'delete', 'update'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * Lists all ArticleTariffs models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ArticleTariffsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single ArticleTariffs model.
	 *
	 * @param int $id ArticleTariff ID.
	 *
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new ArticleTariffs model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new ArticleTariffs();
		if ($model->load(Yii::$app->request->post())) {
			$exist = ArticleTariffs::findOne(['article_id' => $model->article_id, 'tariff_id' => $model->tariff_id, 'fulfillment_partner_id' => $model->fulfillment_partner_id]);
			if (!$exist) {
				if ($model->save()) {
					Yii::$app->session->setFlash('success', Yii::t('app', 'Entry saved successfully!'));
					return $this->redirect(['update', 'article_id' => $model->article_id, 'tariff_id' => $model->tariff_id, 'fulfillment_partner_id' => $model->fulfillment_partner_id]);
				}
			} else {
				Yii::$app->session->setFlash('error', Yii::t('app', 'Entry already exist!'));
			}
		}
		return $this->render('create', [
			'model' => $model,
		]);

	}

	/**
	 * Updates an existing ArticleTariffs model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param int $id Article Tariff ID.
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing ArticleTariffs model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $article_id
	 * @param integer $tariff_id
	 * @param integer $fulfillment_partner_id
	 * @return mixed
	 */
	public function actionDelete($article_id, $tariff_id, $fulfillment_partner_id)
	{
		$this->findModel($article_id, $tariff_id, $fulfillment_partner_id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the ArticleTariffs model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param int $articleTariffId
	 * @return ArticleTariffs the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($articleTariffId)
	{
		if (($model = ArticleTariffs::findOne($articleTariffId)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
