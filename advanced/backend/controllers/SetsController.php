<?php

namespace backend\controllers;

use backend\models\ArticleSearch;
use backend\models\SetsSearch;
use backend\models\TariffsSearch;
use common\models\Article;
use common\modules\set\models\Chart;
use common\modules\set\models\Sets;
use common\models\Tariffs;
use frontend\controllers\tariff\SearchAction;
use RuntimeException;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SetsController implements the CRUD actions for Sets model.
 */
class SetsController extends Controller
{

	public $class;

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['view', 'create', 'update', 'delete', 'sort'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'starlord'],
					],
					[
						'actions' => ['view', 'create', 'update', 'sort'],
						'allow' => true,
						'roles' => ['user'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function beforeAction($action)
	{
		if ($action->id == 'sort' && Yii::$app->request->isAjax) {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}


	/**
	 * Lists all Sets models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new SetsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Sets model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);
		$chart = $model->chart;
		$products = [];
		foreach ($chart as $product) {
		    if($product->productModel->is_active == 1){
                $products[$product->position] = $product->productModel;
            }
		}
		ksort($products);
		return $this->render('view', [
			'model' => $model,
			'products' => $products,
		]);
	}

	/**
	 * Creates a new Sets model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param int $type
	 *
	 * @return mixed
	 */
	public function actionCreate($type = null)
	{
		$model = new Sets(['type' => $type]);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['update', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Sets model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @var Sets $model
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$set = $this->findModel($id);
		$this->saveNewProduct($set->id);
		$this->removeProduct($set->id);

		if ($set->load(Yii::$app->request->post()) && $set->save()) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Set saved successfully!'));
			return $this->redirect(['update', 'id' => $set->id]);
		} else {

			switch ($set->type) {
				case Sets::TYPE_ARTICLE:
					$searchModel = new ArticleSearch();
					break;
				case Sets::TYPE_TARIFF:
					$baseQuery = Tariffs::find();
					SearchAction::applyBaseConditions($baseQuery);
					$searchModel = new TariffsSearch($baseQuery);
					break;
				default:
					throw new RuntimeException('Set type "' . $set->type . '" not able to be updated.');
			}

			$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

			return $this->render('update', [
				'model' => $set,
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]);
		}
	}

	/**
	 *
	 */
	public function actionSort($id)
	{
		if (Yii::$app->request->post('item', false)) {
			$i = 1;
			foreach (Yii::$app->request->post('item') as $key => $item) {
				$chart = Chart::findOne(['set_id' => $id, 'product_id' => $item]);
				$chart->position = $i;
				$chart->save();
				$i++;
			}
		}

		$model = $this->findModel($id);
		$chart = $model->chart;
		$products = [];
		foreach ($chart as $product) {
			if (!$product->productModel) {
				$this->redirect(['update', 'id' => $id]);
				Yii::$app->session->setFlash('error', Yii::t('app', 'You have to remove inactive products before you can sort sets'));
			} else {
				$products[$product->position] = $product->productModel;
			}
		}
		ksort($products);

		return $this->render('sort', [
			'model' => $model,
			'products' => $products,
		]);
	}

	/**
	 * Deletes an existing Sets model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		Chart::deleteAll(['set_id' => $id]);
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Sets model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Sets the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Sets::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function getProductClass($class_name, $withNamespace = true)
	{
		$class = ($class_name == Tariffs::className(false)) ? Tariffs::className() : Article::className();
		if ($withNamespace) {
			return $class;
		} else {
			$tmp = explode('\\', $class);
			return end($tmp);
		}
	}

	/**
	 * @param int $setId
	 *
	 * @return bool Success indicator.
	 */
	protected function saveNewProduct($setId)
	{
		$idToAdd = Yii::$app->request->get('add');
		if (empty($idToAdd)) {
			return true;
		}

		$this->removeProduct($setId, $idToAdd);

		$maxPos = Chart::find()->where(['set_id' => $setId])->max('position');

		$chart = new Chart();
		$chart->set_id = $setId;
		$chart->position = $maxPos + 1;
		$chart->product_id = $idToAdd;
		$chart->save();

		return true;
	}

	/**
	 * @param int $setId
	 * @param null|int $idToRemove
	 *
	 * @return bool Success indicator.
	 */
	protected function removeProduct($setId, $idToRemove = null)
	{
		$idToRemove = $idToRemove ?? Yii::$app->request->get('remove');
		if (empty($idToRemove)) {
			return true;
		}

		$existingWithId = Chart::findOne(['set_id' => $setId, 'product_id' => $idToRemove]);
		if ($existingWithId) {
			$existingWithId->delete();
			return true;
		}
		return false;
	}
}
