<?php

namespace backend\controllers;

class LanguageController extends \lajax\translatemanager\controllers\LanguageController
{
	public function behaviors()
	{
		$behaviors = parent::behaviors();

		$actions = &$behaviors['access']['rules'][0]['actions'];

		$deleteKey = array_search('delete', $actions);
		$updateKey = array_search('update', $actions);

		unset($actions[$deleteKey], $actions[$updateKey]);

		return $behaviors;
	}
}
