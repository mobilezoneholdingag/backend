<?php
namespace backend\controllers;

use common\models\UserAuth;
use Yii;
use common\models\LoginForm;
use common\models\User;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use common\models\Sales;
use yii\authclient\BaseClient;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['login', 'error', 'auth'],
						'roles' => ['?', '@'],
						'allow' => true,
					],
					[
						'actions' => ['logout', 'index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'auth' => [
				'class' => 'yii\authclient\AuthAction',
				'successCallback' => [$this, 'onAuthSuccess']
			]
		];
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionLogin()
	{
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

	public function actionRequestPasswordReset()
	{
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

				return $this->goHome();
			} else {
				Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
			}
		}

		return $this->render('requestPasswordResetToken', [
			'model' => $model,
		]);
	}

	public function actionResetPassword($token)
	{
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->getSession()->setFlash('success', 'New password was saved.');

			return $this->goHome();
		}

		return $this->render('resetPassword', [
			'model' => $model,
		]);
	}

	public function onAuthSuccess(BaseClient $client)
	{
		$attributes = $client->getUserAttributes();

		if (!empty($attributes['emails'])) {
			if (!isset($attributes['email']))
				$attributes['email'] = [];

			foreach ($attributes['emails'] as $emailObject) {
				if (isset($emailObject['value']))
					$attributes['email'][] = $emailObject['value'];
			}
		}

		$auth = UserAuth::find()->where([
			'source' => $client->getId(),
			'source_id' => $attributes['id']
		])->one();

		if (Yii::$app->user->isGuest) {
			if ($auth) { // User logged in on the foreign service, found linked to a local user
				$user = $auth->user;
				Yii::$app->user->login($user);
			} else {
				if (isset($attributes['email'])) {
					$userAccount = User::find()->where(['email' => $attributes['email']])->all();

					if (count($userAccount) > 1) {
						Yii::$app->getSession()->setFlash('error',
							Yii::t('app', 'Your email is ambigous. Please log in to link your account.')
						);
						return;
					}

					if (empty($userAccount)) {
						Yii::$app->getSession()->setFlash('error',
							Yii::t('app', 'There is no user account with this email address.')
						);
						return;
					}

					$userAccount = array_shift($userAccount);

					$auth = new UserAuth([
						'user_id' => $userAccount->id,
						'source' => $client->getId(),
						'source_id' => (string)$attributes['id'],
					]);

					if ($auth->save()) {
						Yii::$app->user->login($userAccount);
					} else {
						if (YII_DEBUG)
							print_r($auth->getErrors());

						Yii::$app->getSession()->setFlash('error', [
							Yii::t('app', 'Some error appeared by linking your account.')
						]);
					}
				}
			}
		} else {    // User already logged in on the local service
			if (!$auth) {   // Link local account to foreign service
				$auth = new UserAuth([
					'user_id' => Yii::$app->user->id,
					'source' => $client->getId(),
					'source_id' => $attributes['id']
				]);
				$auth->save();
			}
		}
	}

}
