<?php

namespace backend\controllers;

use backend\models\ShopLanguage;
use backend\models\SpeakLanguageSearch;

/**
 * ShopSpecialController implements the CRUD actions for ShopSpecial model.
 */
class ShopLanguageController extends AbstractShopUpdateController
{
	protected static $tableField = 'language_id';

	protected static $searchModelClass = SpeakLanguageSearch::class;

	protected function getModelClass()
	{
		return ShopLanguage::class;
	}
}
