<?php

namespace backend\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class EbayController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex($article_id, $tariff_id)
	{
		$ebayHtmlTemplateRaw = $this->renderPartial('ebay.twig', ['article_id' => $article_id, 'tariff_id' => $tariff_id,]);

		return $this->render('index', ['article_id' => $article_id, 'tariff_id' => $tariff_id, 'html' => $ebayHtmlTemplateRaw]);
	}

	public function actionView($article_id, $tariff_id)
	{
		return $this->renderPartial('ebay.twig', ['article_id' => $article_id, 'tariff_id' => $tariff_id]);
	}

}