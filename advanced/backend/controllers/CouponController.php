<?php

namespace backend\controllers;

use backend\models\CouponBulk;
use yii\db\Exception;
use yii\web\Controller;
use common\models\Coupon;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * CouponController implements the CRUD actions for Coupon model.
 */
class CouponController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['view', 'create', 'update', 'delete'],
						'allow' => true,
						'roles' => ['admin', 'editor'],
					],
					[
						'actions' => ['delete', 'bulk'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}
	/**
	 * Lists all Coupon models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$valid = new ActiveDataProvider([
			'query' => Coupon::find()->where(['is_valid' => 1]),
            'sort' => [
                'defaultOrder' => [
                    'valid_unique' => SORT_ASC,
                    'id' => SORT_DESC
                ],
                'sortParam' => 'sort-valid'
            ],
            'pagination' => [
                'pageSize' => 20,
                'pageParam' => 'page-valid'
            ]
		]);

        $not_valid = new ActiveDataProvider([
            'query' => Coupon::find()->where(['is_valid' => 0]),
            'sort' => [
                'defaultOrder' => [
                    'valid_unique' => SORT_ASC,
                    'id' => SORT_DESC
                ],
                'sortParam' => 'sort-not-valid'
            ],
            'pagination' => [
                'pageSize' => 1000,
                'pageParam' => 'page-not-valid',
            ]
        ]);

		return $this->render('index', [
			'valid' => $valid,
            'not_valid' => $not_valid
		]);
	}

	/**
	 * Displays a single Coupon model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Coupon model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Coupon();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Coupon model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Coupon model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	public function actionBulk()
    {

        $model = new CouponBulk();

        $insert = false;

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if( $model->process()) {

                $columns = ['code', 'valid_from', 'valid_until', 'is_valid', 'valid_unique', 'discount_device_price'];

                $rows = [];

                foreach($model->codes as $code) {

                    if(isset($code['COUPONCODE']) && isset($code['VALIDFROM']) && isset($code['VALIDUNTIL']) && isset($code['MULTIPLEUSE']) && isset($code['AM'])) {
                        $rows[] = [
                            'code' => $code['COUPONCODE'],
                            'valid_from' => (!empty($code['VALIDFROM'])) ? strtotime(substr($code['VALIDFROM'], 0, 10)) : '',
                            'valid_until' => (!empty($code['VALIDUNTIL'])) ? strtotime(substr($code['VALIDUNTIL'], 0, 10)) : '',
                            'is_valid' => 1,
                            'valid_unique' => ($code['MULTIPLEUSE'] == "N") ? 1 : 0,
                            'discount_device_price' => $code['AM']
                        ];
                    } else {
                        Yii::$app->session->setFlash('warning', '<strong>CSV ungültig!</strong> <br><br> <strong>Minimum Fields:</strong> COUPONCODE, VALIDFROM, VALIDUNTIL, MULTIPLEUSE, AM');
                        break;
                    }

                }

                if(!empty($rows)) {
                    try {
                        $insert = Yii::$app->db->createCommand()->batchInsert(Coupon::tableName(), $columns, $rows)->execute();
                    } catch (Exception $e) {
                        Yii::$app->session->setFlash('danger', '<strong>Beim einfügen ist ein Fehler passiert:</strong> <br><br> ' . substr($e->getMessage(), 0, 500));
                    }
                }

            }

        }

        return $this->render('bulk', ['model' => $model, 'insert' => $insert]);
    }

	/**
	 * Finds the Coupon model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Coupon the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Coupon::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
