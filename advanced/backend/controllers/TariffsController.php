<?php

namespace backend\controllers;

use backend\models\TariffsSearch;
use common\models\Provider;
use common\models\Tariffs;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TariffsController implements the CRUD actions for Tariffs model.
 */
class TariffsController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view',],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update', 'edit-all', 'get-for-provider', 'get-provider-list'],
						'allow' => true,
						'roles' => ['admin','editor', 'user'],
					],
					[
						'actions' => ['create', 'delete'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * @return \common\models\Provider[]
	 */
	public function actionGetProviderList()
	{
		/** @var Provider[] $providers */
		$providers = Provider::find()
			->select(['id', 'title'])
			->orderBy('title')
			->asArray()
			->all();

		Yii::$app->response->format = Response::FORMAT_JSON;
		return $providers;
	}

	/**
	 * @param $providerId
	 *
	 * @return \common\models\Tariffs[]
	 */
	public function actionGetForProvider($providerId)
	{
		/** @var Tariffs[] $tariffs */
		$tariffs = Tariffs::find()
			->select(['id', 'title'])
			->where(['provider_id' => $providerId])
			->orderBy('title')
			->asArray()
			->all();

		Yii::$app->response->format = Response::FORMAT_JSON;
		return $tariffs;
	}

	/**
	 * Lists all Tariffs models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new TariffsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Tariffs model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Tariffs model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Tariffs();

		if ($model->load(Yii::$app->request->post()) && $model->save() AND $model->saveHighlights(Yii::$app->request->post())) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Tariffs model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) AND $model->save() AND $model->saveHighlights(Yii::$app->request->post())) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates certain values of all entities
	 * @return mixed
	 */
	public function actionEditAll()
	{
		$models = Tariffs::find()->all();

		if (Yii::$app->request->post()) {
			foreach (Yii::$app->request->post()['result'] as $id => $tariff) {
				$model = $this->findModel($id);
				$model->provider_id = $tariff['provider_id'];
				$model->title = $tariff['title'];
				$model->save();
			}
			return $this->redirect(['edit-all']);
		}
		return $this->render('edit-all', [
			'models' => $models,
		]);
	}

	/**
	 * Deletes an existing Tariffs model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}


	/**
	 * Finds the Tariffs model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Tariffs the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Tariffs::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
