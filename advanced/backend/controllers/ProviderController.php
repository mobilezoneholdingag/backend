<?php

namespace backend\controllers;

use yii\web\Controller;
use common\models\Provider;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ProviderController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update', 'sort'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'user'],
					],
					[
						'actions' => ['create', 'delete'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}


	public function beforeAction($action)
	{
		if ($action->id == 'sort' && Yii::$app->request->isAjax) {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}

	public function actionIndex()
	{
		$dataProvider = new ActiveDataProvider([
			'query' => Provider::find()->orderBy('sort'),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	public function actionCreate()
	{
		$model = new Provider();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			if(!empty($model->image)) {
				Yii::$app->session->setFlash('upload_success', 'Your file were uploaded successfully.');
			}

			return $this->redirect(['index']);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	public function actionSort()
	{
		if (Yii::$app->request->post('item', false)) {
			$i = 1;
			foreach (Yii::$app->request->post('item') as $item) {
				$provider = $this->findModel($item);
				$provider->sort = $i;
				$provider->save();
				$i++;
			}
		}

		$providers = Provider::find()->orderBy('sort')->all();

		return $this->render('sort', [
			'providers' => $providers,
		]);
	}

	protected function findModel($id)
	{
		if (($model = Provider::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
