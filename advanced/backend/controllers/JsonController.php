<?php

namespace backend\controllers;


use common\helper\SaleableHelper;
use common\models\ProductCategory;
use components\validators\PassportValidator;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class JsonController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['get-preview-images-of-article', 'list-advertising-capable-products'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function __construct($id, $module, $config = [])
	{
		parent::__construct($id, $module, $config);

		\Yii::$app->response->format = Response::FORMAT_JSON;
	}

	public function beforeAction($action)
	{
		\Yii::$app->request->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	/**
	 * @param null $kind
	 *
	 * @return array|\common\models\Article[]|\common\models\Tariffs[]
	 */
	public function actionListAdvertisingCapableProducts($kind = null)
	{
		if (empty($kind) || !in_array($kind, [ProductCategory::SMARTPHONE, ProductCategory::TARIFF, ProductCategory::TABLET]))
			return [];

		return SaleableHelper::generateList($kind);
	}

	public function actionCheckId()
	{
		$validationResult = PassportValidator::validateRequest();

		return $validationResult;
	}
}
