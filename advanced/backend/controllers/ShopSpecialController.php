<?php

namespace backend\controllers;

use backend\models\ShopSpecial;
use backend\models\SpecialSearch;

/**
 * ShopSpecialController implements the CRUD actions for ShopSpecial model.
 */
class ShopSpecialController extends AbstractShopUpdateController
{
	protected static $tableField = 'special_id';

	protected static $searchModelClass = SpecialSearch::class;

	protected function getModelClass()
	{
		return ShopSpecial::class;
	}
}
