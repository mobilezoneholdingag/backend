<?php

namespace backend\controllers;

use common\models\OfferType;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OfferTypeController implements the view-only actions for OfferType model.
 */
class OfferTypeController extends Controller
{

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * Lists all OfferType models.
	 */
	public function actionIndex()
	{
		return $this->render('index', [
			'dataProvider' => new ActiveDataProvider(['query' => OfferType::find()])
		]);
	}

	/**
	 * Displays a single OfferType model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', ['model' => $this->findModel($id)]);
	}

	/**
	 * Updates an existing OfferType model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Finds the OfferType model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return OfferType the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if ($model = OfferType::findOne($id)) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
