<?php

namespace backend\controllers;

use backend\models\MarketingImagesSearch;
use common\models\MarketingImages;
use yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class MarketingImagesController extends Controller
{

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update', 'create', 'view', 'delete'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$searchModel = new MarketingImagesSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionView($id)
	{
		return $this->render('view', ['model' => $this->findModel($id)]);
	}

	public function actionCreate()
	{
		$model = new MarketingImages();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', ['model' => $model]);
		}
	}

	public function actionUpdate($id)
	{
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '300');
        ini_set('upload_max_filesize', '40M');
        ini_set('post_max_size', '40M');

		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->getSession()->setFlash('success', 'Änderungen erfolgreich gespeichert!');

			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			foreach ($model->getErrors() as $error) {
				Yii::$app->getSession()->setFlash('danger', $error[0]);
			}

			return $this->render('update', ['model' => $model]);
		}
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	protected function findModel($id)
	{
		if (($model = MarketingImages::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
