<?php

namespace backend\controllers;

use common\models\ArticleTariffs;
use common\models\DeliveryStatus;
use common\models\Tariffs;
use common\models\Article;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DeliveryStatusController implements the CRUD actions for DeliveryStatus model.
 */
class DeliveryStatusController extends Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all DeliveryStatus models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$dataProvider = new ActiveDataProvider([
			'query' => DeliveryStatus::find(),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single DeliveryStatus model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new DeliveryStatus model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new DeliveryStatus();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing DeliveryStatus model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing DeliveryStatus model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the DeliveryStatus model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return DeliveryStatus the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = DeliveryStatus::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * Overwrite delivery_status with another state because
	 */
	public function actionUpdateDeliveryStatus()
	{
		// SELECT * FROM ... as bla is necessary as MySQL does not allow updating and reading from the same table
		// at the same time, see http://stackoverflow.com/a/22861336 for details

		// Sets ALL article tariffs from EAM (partner 1) that are in stock to "1-3 days"
		ArticleTariffs::getDb()->createCommand('
			UPDATE article_tariffs AS outertariffs
			SET outertariffs.delivery_status_id = 8
			WHERE (
					`outertariffs`.article_id,
					`outertariffs`.tariff_id,
					`outertariffs`.fulfillment_partner_id
					) IN (
					SELECT *
					FROM (
						SELECT `article_tariffs`.article_id,
							`article_tariffs`.tariff_id,
							`article_tariffs`.fulfillment_partner_id
						FROM `article_tariffs`
						INNER JOIN `articles`
							ON article_tariffs.article_id = articles.id
						WHERE (`articles`.`delivery_status_id` = 1)
							AND (`fulfillment_partner_id` = 1)
						) AS bla
					)
		')->execute();

		// Sets ALL article tariffs from EAM (partner 1), BLAU (provider  13), Device (delivery_status_id 1) to ("2-4 days")
		ArticleTariffs::getDb()->createCommand('
			UPDATE article_tariffs AS outertariffs
			SET outertariffs.delivery_status_id = 7
			WHERE (
				   outertariffs.article_id,
				   outertariffs.tariff_id,
				   outertariffs.fulfillment_partner_id
				   ) IN (
				   SELECT *
				   FROM (
					   SELECT `article_tariffs`.article_id,
						   `article_tariffs`.tariff_id,
						   `article_tariffs`.fulfillment_partner_id
					   FROM `article_tariffs`
					   INNER JOIN `tariffs`
						   ON article_tariffs.tariff_id = tariffs.id
					   INNER JOIN `articles`
						   ON article_tariffs.article_id = articles.id
					   WHERE `provider_id` = 13
						   AND (`article_tariffs`.`fulfillment_partner_id` = 1)
						   AND (`articles`.`delivery_status_id` = 1)
					   ) AS bla
				   )
		')->execute();

		return $this->redirect(['index']);
	}
}
