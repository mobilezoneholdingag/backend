(function ( $ ) {
    $.fn.providerAndTariffDropdown = function(options) {
        return this.each(function() {

            // Initialization of outside data
            var $that = $(this);

            options = $.extend({
                loadingText: '-- loading ... --',
                promptText: '-- please select --',
                tariffDropdown: $that.data('tariff-dropdown')
            }, options);

            var $tariffDropdown = $(options.tariffDropdown);

            var selectedProviderId = $that.data('selected');
            var selectedTariffId = $tariffDropdown.data('selected');

            // Helper functions
            var getLoadingIndicator = function() {
                return $('<option />').text(options.loadingText);
            };

            var getSelectPrompt = function() {
                return $('<option />').val('').text(options.promptText);
            };

            var renderOptions = function($dropdown, entryList, selected) {
                $.each(entryList, function(key, entry) {
                    var option = $('<option />').val(entry.id).text(entry.title);
                    if (selected == entry.id) {
                        option.prop('selected', true);
                    }
                    $dropdown.append(option);
                });
            };

            // Bind user action
            $that.on('change.providerAndTariffDropdown', function() {
                var selected = $that.val();

                if (selected) {
                    $tariffDropdown.html(getLoadingIndicator());

                    $.getJSON({
                        url: '/tariffs/get-for-provider?providerId=' + selected,
                        success: function (response) {

                            $tariffDropdown.html(getSelectPrompt());
                            renderOptions($tariffDropdown, response, selectedTariffId);
                        }
                    });
                }
            });

            // Load providers
            $that.html(getLoadingIndicator());

            $.getJSON({
                url: '/tariffs/get-provider-list',
                success: function(response) {

                    $that.html(getSelectPrompt());
                    renderOptions($that, response, selectedProviderId);
                    if (selectedProviderId) {
                        $that.trigger('change');
                    }
                }
            });

            return this;
        });
    };

    $(function () {
        $('.provider-and-tariff-dropdown').providerAndTariffDropdown();
    });
}( jQuery ));