class Callbacks {
  static onOrderUpdate(event) {
    let $container = $(event.target);
    let id = $container.data('id');
    let data = $container.sortable('serialize', {
      attribute: 'data-original-position'
    });

    $.post(`/article/update-image-order?id=${id}`, data)
      .done((response) => {
        let newImagePositions = JSON.parse(response);
        $('.save_flash:first').fadeIn(150).delay(600).fadeOut(300);
        Helpers.updatePositions($container, newImagePositions);
      })
    ;
  }
}

class Helpers {
  static updatePositions($container, newImagePositions) {
    $container.find('div[data-original-position]').each((index, imageContainer) => {
      let $imageContainer = $(imageContainer);
      let oldPosition = parseInt($imageContainer.data('original-position').split('-')[1]);
      let newPosition = newImagePositions[oldPosition];
      let newPositionData = `images-${newPosition}`;
      $imageContainer.data('original-position', newPositionData).attr('data-original-position', newPositionData);
    });
  }
}

$(() => {
  $('.dropzone-images').sortable({
    update: Callbacks.onOrderUpdate,
  });
});
