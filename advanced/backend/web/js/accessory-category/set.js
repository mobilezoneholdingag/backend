$(document).ready(() => {
  $(document).on('click', '.add-set', (event) => {
    event.preventDefault();
    const $field = $('.set:last');
    const $formGroup = $field.parent();
    const newIndex = parseInt($field.data('index')) + 1;
    let $newField = $field.clone();
    let $newFormGroup = $('<div>').attr('class', 'form-group field-accessorycategory-sets-'+newIndex);
    $newField.attr('id', 'accessorycategory-sets-' + newIndex);
    $newField.attr('name', 'AccessoryCategory[sets]['+newIndex+']');
    $newField.attr('data-index', newIndex);
    $newFormGroup.append($newField);
    $newFormGroup.insertAfter($formGroup);
  });
});
