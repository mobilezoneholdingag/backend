class MultiUpload {
  constructor(containerSelector) {
    this.dropzones = [];
    this.$container = $(containerSelector);
    this.formData = new FormData();

    this.options = {
      autoProcessQueue: false,
      url: this.$container.attr('action'),

      thumbnailWidth: 250,
      thumbnailHeight: 250,

      addRemoveLinks: true,
      dictRemoveFile: '<i class="glyphicon glyphicon-remove"></i>',

      hiddenInputContainer: containerSelector,
      previewTemplate: '' +
        '<div class="dz-preview dz-file-preview">' +
          '<div class="dz-details">' +
            '<div class="dz-filename"><span data-dz-name></span></div>' +
            '<div class="dz-size" data-dz-size></div>' +
            '<img data-dz-thumbnail />' +
          '</div>' +
        '</div>',
    };

    this.requestOptions = {
      url: this.options.url,
      data: this.formData,
      dataType: 'json',
      processData: false,
      contentType: false,
    };
  }

  initDropzones() {
    let $dropzones = this.$container.find('.droppable');
    $dropzones.each((index, dropzone) => {
      let $dropzone = $(dropzone);
      let selector = $dropzone.attr('id');

      /** global: Dropzone */
      this.dropzones.push(new Dropzone(`#${selector}`, this.options));
      this.dropzones[index].on('addedfile', this.onAddedFile);
      this.dropzones[index].on('removedfile', this.onRemovedFile);
      this.dropzones[index].multiUpload = this;
      this.displayPreview(index, $dropzone);
    });

    this.$container.on('submit', (event) => { this.onSubmit(event); });
  }

  onSubmit(event) {
    event.preventDefault();

    const fieldName = $('#dropzone-0').data('name');

    this.formData.append(fieldName.substring(0, fieldName.lastIndexOf("[")) + "[-1]", "");
    this.$container.serializeArray().forEach((field) => {
      this.formData.append(field.name, field.value);
    });

    $.post(this.requestOptions);
  }

  onAddedFile(file) {
    $(this.element).find('.dz-preview:not(:last-child)').remove();
    this.multiUpload.formData.append(this.element.getAttribute('data-name'), file);
  }

  onRemovedFile() {
    this.multiUpload.formData.delete(this.element.getAttribute('data-name'));
  }

  displayPreview(index, $dropzone) {
    let preview = $dropzone.data('preview');
    if (preview) {
      let mockFile = {name: '', size: 0};
      this.dropzones[index].emit('addedfile', mockFile);
      this.dropzones[index].emit('thumbnail', mockFile, preview);
      $dropzone.find('.dz-filename').html('&nbsp;').next().html('&nbsp;');
    }
  }
}

$(() => {
  let multiUpload = new MultiUpload('form#w0');
  multiUpload.initDropzones();
});
