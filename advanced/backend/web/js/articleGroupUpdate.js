$(document).ready(function () {
    $('#articlegroups-product_category_id').on('change', function() {
        if (!$('#articlegroups-product_category_id-well').length) {
            $(this).before(
                '<div class="alert alert-danger" id="articlegroups-product_category_id-well">' +
                    'Nach der Änderung der Produktart müssen die Standardtarife ggf. neu gewählt werden.' +
                '</div>'
            );
        }
    });
});
