$(document).ready(function () {
    $(".datepicker").datepicker();

    allnet_flat = $('#allnet-flat');
    sms_flat = $('#sms-flat');
    mms_flat = $('#mms-flat');


    if (allnet_flat.prop('checked')) {
        $('#anytime-minutes').prop('disabled', true);
    }

    allnet_flat.click(function () {
        //If checkbox is checked then disable or enable input
        if ($(this).is(':checked')) {
            $('#anytime-minutes').prop('disabled', true);
        }
        //If checkbox is unchecked then disable or enable input
        else {
            $('#anytime-minutes').prop('disabled', false);
        }
    });

    if (sms_flat.prop('checked')) {
        $('#free-sms').prop('disabled', true);
        $('#price-sms').prop('disabled', true);
    }

    sms_flat.click(function () {
        //If checkbox is checked then disable or enable input
        if ($(this).is(':checked')) {
            $('#free-sms').prop('disabled', true);
            $('#price-sms').prop('disabled', true);
        }
        //If checkbox is unchecked then disable or enable input
        else {
            $('#free-sms').prop('disabled', false);
            $('#price-sms').prop('disabled', false);
        }
    });

    if (mms_flat.prop('checked')) {
        $('#price-mms').prop('disabled', true);
    }

    mms_flat.click(function () {
        //If checkbox is checked then disable or enable input
        if ($(this).is(':checked')) {
            $('#price-mms').prop('disabled', true);
        }
        //If checkbox is unchecked then disable or enable input
        else {
            $('#price-mms').prop('disabled', false);
        }
    });


    $('#detailsHeadlineImages, #detailsHeadline, #detailsHeadlineDesc, #detailsHeadlineFrontendText, #detailsHeadlineSEO, #detailsHeadlineSEOImages, #detailsHeadlineSimSettings').click(function (event) {
        field = "";

        if ($(event.target).is('#detailsHeadline')) {
            field = $('#detailsField');
        } else if ($(event.target).is('#detailsHeadlineImages')) {
            field = $('#detailsFieldImages');
        } else if ($(event.target).is('#detailsHeadlineDesc')) {
            field = $('#detailsFieldDesc');
        } else if ($(event.target).is('#detailsHeadlineFrontendText')) {
            field = $('#detailsFieldFrontendText');
        } else if ($(event.target).is('#detailsHeadlineSEO')) {
            field = $('#detailsFieldSEO');
        } else if ($(event.target).is('#detailsHeadlineSEOImages')) {
            field = $('#detailsFieldSEOImages');
        } else if ($(event.target).is('#detailsHeadlineSimSettings')) {
            field = $('#detailsFieldSimSettings');
        }

        collapseField(field);
    });

    function collapseField(field) {
        if (field.is(':visible')) {
            field.hide(100)
        } else {
            field.show(100)
        }
    }

    function isOverrideChecked(){
        if($("#article-override_positive_stock_to_reorder").is(":checked")){
            $(".field-article-custom_stock").show().css({position: "relative"});
        }else{
            $(".field-article-custom_stock").hide();
        }
    }

    $("#article-override_positive_stock_to_reorder").change(function() {
        isOverrideChecked();
    });

    isOverrideChecked();
});



