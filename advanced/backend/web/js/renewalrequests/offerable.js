$(document).ready(function() {
    let notUpdateable = $('#offerUpdate').data('notupdateable');

    $('.toggleSelect').on('change', function() {
        let selectfield =  $('#' + $(this).val());
        let $this = $(this);

        if($this.is(':checked')) {
            selectfield.removeAttr('disabled');
            $this.parent().parent().addClass('bg-success');
        } else {
            selectfield.attr('disabled', 'disabled');
            $this.parent().parent().removeClass('bg-success');
        }

        if(notUpdateable) {
            $this.attr('disabled', 'disabled');
        }
    }).trigger('change');


    $('.renewal-request-form > form :submit').click(function( event ) {
        if(!$('.toggleSelect[type="checkbox"]').is(':checked')) {
            alert('Sie müssen mindestens ein Angebot auswählen');

            return false;
        }
    });

});

