<?php

$params = array_replace_recursive(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-backend',
	'basePath' => dirname(__DIR__),
	'language' => 'de-CH',
	'controllerNamespace' => 'backend\controllers',
	'bootstrap' => ['log'],
	'modules' => [
		'gridview' => [
			'class' => '\kartik\grid\Module'
			// from http://demos.krajee.com/grid
			// enter optional module parameters below - only if you need to
			// use your own export download action or custom translation
			// message source
			// 'downloadAction' => 'gridview/export/download',
			// 'i18n' => []
		],
		'translatemanager' => [
			'class' => 'lajax\translatemanager\Module',
			'controllerMap' => [
				'language' => 'backend\controllers\LanguageController'
			],
			'root' => '@common/messages',               // The root directory of the project scan.
			'scanRootParentDirectory' => true, // Whether scan the defined `root` parent directory, or the folder itself.
			// IMPORTANT: for detailed instructions read the chapter about root configuration.
			'layout' => '@backend/views/layouts/main.php',         // Name of the used layout. If using own layout use 'null'.
			'allowedIPs' => ['*'],  // IP addresses from which the translation interface is accessible.
			'roles' => ['admin', 'editor'],               // For setting access levels to the translating interface.
			'tmpDir' => '@runtime',         // Writable directory for the client-side temporary language files.
			// IMPORTANT: must be identical for all applications (the AssetsManager serves the JavaScript files containing language elements from this directory).
			'phpTranslators' => ['::t'],    // list of the php function for translating messages.
			'jsTranslators' => ['lajax.t'], // list of the js function for translating messages.
			'patterns' => ['*.js', '*.php'],// list of file extensions that contain language elements.
			//            'ignoredCategories' => ['yii'], // these categories won't be included in the language database.
			'ignoredItems' => ['config'],   // these files will not be processed.
			'scanTimeLimit' => null,        // increase to prevent "Maximum execution time" errors, if null the default max_execution_time will be used
			'searchEmptyCommand' => '!',    // the search string to enter in the 'Translation' search field to find not yet translated items, set to null to disable this feature
			'defaultExportStatus' => 1,     // the default selection of languages to export, set to 0 to select all languages by default
			'defaultExportFormat' => 'json',// the default format for export, can be 'json' or 'xml'
			'tables' => [                   // Properties of individual tables
				[
					'connection' => 'db',   // connection identifier
					'table' => '{{%language}}',         // table name
					'columns' => ['name', 'name_ascii'],// names of multilingual fields
					'category' => 'database-table-name',// the category is the database table name
				],
			],
		],
	],
	'components' => [
		'session' => [
			'class' => yii\redis\Session::class,
			'redis' => 'redis-session',
		],
		'view' => [
			'renderers' => [
				'twig' => [
					'class' => 'yii\twig\ViewRenderer',
					'cachePath' => '@runtime/Twig/cache',
					'options' => [
						'auto_reload' => true,
					],
					'globals' => [
						'OfferHelper' => '\common\helper\OfferHelper',
					],
				],
			],
		],
		'assetManager' => [
			'class' => 'yii\web\AssetManager',
			'appendTimestamp' => true,
			'hashCallback' => ['\components\AssetHelper', 'hashPath'],
		],
		'user' => [
			'identityClass' => 'common\models\User',
			'enableAutoLogin' => true,
			'authTimeout' => 7200,
			// this clearly separates the backend user sessions from the frontend user sessions
			'identityCookie' => [
				'name' => '_backendUser',
				// correct path for backend app
				'path' => '/advanced/backend/web',
			],
		],
		'log' => [
			'traceLevel' => 0,
			'targets' => [
				[
					'class' => 'yii\log\DbTarget',
					'levels' => ['error', 'warning'],
					'logTable' => '{{%log_backend}}',
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'operations' => [
			'class' => 'components\services\OperationsService',
			'instances' => $params['operations']['instances'],
			'urlPrefix' => $params['operations']['urlPrefix'],
		],
		'urlManager' => [
			'class' => 'yii\web\UrlManager',
			'hostInfo' => 'http://backend.stardust.dev',
			'rules' => $params['url']['rules'],
		],
		'formatter' => [
			'class' => \components\i18n\Formatter::class,
			'dateFormat' => 'php:d.m.Y',
		]
	],
	'params' => $params,
];
