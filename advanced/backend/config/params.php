<?php
return [
	'adminEmail' => 'admin@example.com',
	'flashMessagesPath' => '../snippets/flashMessages',
	'staticSystemSets' => [
		'top_10_smartphones',
		'top_10_tariffs'
	],
	'contentSearchSectionsAndAttributes' => [
		\common\models\Seo::className() => [
			'page_title',
			'page_description',
			'html_content',
		],
		\common\models\LandingPages::className() => [
			'title',
			'url',
			'meta_description',
			'html_code',
			'redirect_url',
			'canonical_url',
			'headline',
		],
		\common\models\ArticleGroups::className() => [
			'description_1',
			'description_2',
			'description_3',
			'frontend_text',
			'seo_text_1',
			'seo_text_2',
			'seo_text_3',
			'seo_text_4',
			'article_feed_description',
		],
		\common\models\Article::className() => [
			'delivery_includes',
		],
		\common\models\Tariffs::className() => [
			'tariff_feed_description'
		],
	],
	'operations'    => [
		'instances' => [
			'local' => [
				'VM'  => 'deinhandy.dev'
			],
			'staging'   => [
				'Staging-01'    => '192.168.5.2',
				'Staging-02'    => '192.168.5.3',
				'Staging-01-Ch' => '192.168.5.7'
			],
			'live'      => [
				'Live-01'       => '192.168.5.1',
				'Live-02'       => '192.168.5.4',
				'Live-03'       => '192.168.5.5',
				'Live-01-Ch'    => '192.168.5.6'
			]
		],
		'urlPrefix' => 'https://deinhandy.atlassian.net/browse'
	],
	'url' => [
		'rules' => [
			'order/order/contractfile/<id:[\d]+>' => 'order/order/contractfile',
		]
	],
];
