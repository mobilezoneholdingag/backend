<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\View;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/main.css',
	];

	public $js = [
		'js/main.js',
		'js/jquery.providerAndTariffDropdown.js',
		'//code.jquery.com/ui/1.11.4/jquery-ui.js',
		'//cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js',
		'//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
	];

	public $jsOptions = [
		'position' => View::POS_HEAD
	];

	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
