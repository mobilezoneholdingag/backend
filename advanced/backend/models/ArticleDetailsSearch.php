<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ArticleDetails;

/**
 * ArticleDetailsSearch represents the model behind the search form about `common\models\ArticleDetails`.
 */
class ArticleDetailsSearch extends ArticleDetails
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['detail_id', 'sort_id', 'gruppen_id', 'set_id', 'highlight'], 'integer'],
			[['detail_name', 'tooltip', 'unit'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = ArticleDetails::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'detail_id' => $this->detail_id,
			'sort_id' => $this->sort_id,
			'gruppen_id' => $this->gruppen_id,
			'highlight' => $this->highlight,
		]);

		$query->andFilterWhere(['like', 'detail_name', $this->detail_name_de])
			->andFilterWhere(['like', 'tooltip', $this->tooltip_de])
			->andFilterWhere(['like', 'unit', $this->unit]);

		return $dataProvider;
	}
}
