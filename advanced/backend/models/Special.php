<?php

namespace backend\models;

use common\models\Shop;
use components\i18n\TranslatableActiveRecord;
use sammaye\audittrail\LoggableBehavior;
use Yii;

/**
 * This is the model class for table "special".
 *
 * @property integer $id
 * @property string $special_title
 * @property string $special_description
 * @property ShopSpecial[] $shopSpecials
 * @property Shop[] $shops
 */
class Special extends TranslatableActiveRecord
{
	protected $translateAttributes = [
		'special_title',
		'special_description',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'special';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['special_description'], 'string'],
			[['special_title'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'special_title' => 'Special Title',
			'special_description' => 'Special Description',
		];
	}

	public function fields()
	{
		return ['special_title', 'special_description'];
	}

	public function behaviors()
	{
		return array_merge(
			parent::behaviors(),
			Yii::$app->id == 'app-backend' ? [LoggableBehavior::className()] : []
		);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShopSpecials()
	{
		return $this->hasMany(ShopSpecial::className(), ['special_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShops()
	{
		return $this->hasMany(Shop::className(), ['id' => 'shop_id'])->viaTable('shop_special', ['special_id' => 'id']);
	}
}
