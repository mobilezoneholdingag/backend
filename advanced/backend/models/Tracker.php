<?php

namespace backend\models;

use Yii;
use common\models\Sales;

/**
 * This is the model class for table "tracker".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property integer $prio
 */
class Tracker extends \yii\db\ActiveRecord
{

	const DIRECT_SALE_TRACKER_TITLE = 'Direct';
	const DIRECT_SALE_TRACKER_CODE = 'DIRECT';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tracker';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['code', 'title', 'prio'], 'required'],
			[['code', 'title'], 'string', 'max' => 255],
			[['prio'], 'integer'],
			[['code'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'code' => Yii::t('app', 'Code'),
			'title' => Yii::t('app', 'Title'),
			'prio' => Yii::t('app', 'Priority'),
		];
	}


	/**
	 * Returns the tracker id for any bid
	 *
	 * @param string $bid
	 * @return mixed|null|integer
	 */
	public static function getTrackerIdByBid($bid = null)
	{
		$trackerId = null;
		$tracker = Tracker::find()->where(['title' => Tracker::DIRECT_SALE_TRACKER_TITLE])->one();
		$defaultTrackerId = ($tracker) ? $tracker->id : null;

		if (!empty($bid)) {

			// it's important to check for important tracker codes first, then for fallback trackers codes
			// search for DZM-XX, if nothing is found, then search for DZM, that's why we use priorities.
			$trackers = Tracker::find()->orderBy(['prio' => SORT_ASC])->all();

			foreach ($trackers as $tracker) {
				if (!$trackerId) {
					if (preg_match('/' . join('(.*?)', explode(' ', preg_quote($tracker->code))) . '/i', $bid)) {
						$trackerId = $tracker->id;
					}
				}
			}
		} else {
			$trackerId = $defaultTrackerId;
		}

		return $trackerId;
	}

	/**
	 * Returns the tracker id for any sale
	 *
	 * @param Sales $sale
	 * @return mixed|null|integer
	 */
	public static function getTrackerIdBySale($sale)
	{
		$trackerId = null;

		if (!empty($sale->bid)) {

			// it's important to check for important tracker codes first, then for fallback trackers codes
			// search for DZM-XX, if nothing is found, then search for DZM, that's why we use priorities.
			$trackers = Tracker::find()->orderBy(['prio' => SORT_ASC])->all();

			foreach ($trackers as $tracker) {
				if (!$trackerId) {
					if (preg_match('/' . join('(.*?)', explode(' ', preg_quote($tracker->code))) . '/i', $sale->bid)) {
						$trackerId = $tracker->id;
					}
				}
			}
		} else {
			$directTracker = Tracker::find()->where(['title' => self::DIRECT_SALE_TRACKER_TITLE])->one();
			$trackerId = ($directTracker) ? $directTracker->id : $trackerId;
		}

		return $trackerId;
	}

	/**
	 * Returns the tracker name for any sale
	 *
	 * @param Sales $sale
	 * @return mixed|null|string
	 */
	public static function getTrackerNameBySale($sale)
	{
		$trackerName = null;

		if (!empty($sale->bid)) {

			// it's important to check for important tracker codes first, then for fallback trackers codes
			// search for DZM-XX, if nothing is found, then search for DZM, that's why we use priorities.
			$trackers = Tracker::find()->orderBy(['prio' => SORT_ASC])->all();

			foreach ($trackers as $tracker) {
				if (!$trackerName) {
					if (preg_match('/' . join('(.*?)', explode(' ', preg_quote($tracker->code))) . '/i', $sale->bid)) {
						$trackerName = $tracker->title;
					}
				}
			}
			return $trackerName;
		} else {
			return 'DIRECT';
		}
	}
}
