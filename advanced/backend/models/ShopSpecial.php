<?php

namespace backend\models;

use common\models\Shop;

/**
 * This is the model class for table "shop_special".
 *
 * @property integer $shop_id
 * @property integer $special_id
 * @property Shop $shop
 * @property Special $special
 */
class ShopSpecial extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'shop_special';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['shop_id', 'special_id'], 'required'],
			[['shop_id', 'special_id'], 'integer'],
			[
				['shop_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Shop::className(),
				'targetAttribute' => ['shop_id' => 'id'],
			],
			[
				['special_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Special::className(),
				'targetAttribute' => ['special_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'shop_id' => 'Shop ID',
			'special_id' => 'Special ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShop()
	{
		return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSpecial()
	{
		return $this->hasOne(Special::className(), ['id' => 'special_id']);
	}
}
