<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TariffHighlights;

/**
 * TariffHighlightsSearch represents the model behind the search form about `common\models\TariffHighlights`.
 */
class TariffHighlightsSearch extends TariffHighlights
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'tariff_id', 'sort', 'detail_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = TariffHighlights::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'tariff_id' => $this->tariff_id,
			'sort' => $this->sort,
			'detail_id' => $this->detail_id,
		]);

		$query->innerJoin(TariffHighlightDetails::tableName(), ['id' => 'detail_id']);

		return $dataProvider;
	}
}
