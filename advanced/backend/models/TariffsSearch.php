<?php

namespace backend\models;

use common\models\FulfillmentPartners;
use common\models\Net;
use common\models\OfferType;
use common\models\Provider;
use common\modules\set\models\Sets;
use common\models\TariffHighlightDetails;
use common\models\TariffHighlights;
use common\models\Tariffs;
use common\models\TariffSpecial;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * TariffSearch represents the model behind the search form about `common\models\Tariffs`.
 */
class TariffsSearch extends Tariffs
{
	public $providerTitle;
	public $offerTypeTitle;
	public $setTitle;
	public $partnerTitle;

	/** @var Query */
	private $baseQuery;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'id',
					'is_active',
					'is_active_erp',
					'network',
					'provider_id',
					'is_prepaid',
					'is_b2b_allowed',
					'portable_number',
					'young_people',
					'voicecall_data',
					'set_id',
				],
				'integer',
			],
			[
				[
					'title',
					'erp_title',
					'description',
					'hint',
					'anytime_minutes',
					'free_sms',
					'flatrate_landline',
					'flatrate_mobile_networks',
					'sms_flatrate_mobile_networks',
					'internet_flat',
					'price_mms',
					'contract_duration',
					'bill_online',
					'data_volume',
					'lte',
					'speed_reduced',
					'speed_max',
					'footnote_id',
					'comments',
					'rating_value',
					'swisscom_id',
					'swisscom_id_2',
					'price_monthly_runtime',
				],
				'safe',
			],
			[['provision', 'profit', 'price_monthly'], 'number'],
			[
				[
					'providerTitle',
					'setTitle',
					'partnerTitle',
					'offerTypeTitle',
				],
				'safe',
			],
		];
	}

	public function __construct(Query $baseQuery = null, array $config = [])
	{
		parent::__construct($config);
		$this->baseQuery = $baseQuery ?? Tariffs::find();
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	protected function initializeDataProvider(ActiveDataProvider $dataProvider = null)
	{
		$query = $this->baseQuery;

		if ($dataProvider == null) {
			$dataProvider = new ActiveDataProvider(['pagination' => [
				'pageSizeLimit' => [1, 200]
			]]);
		}

		$dataProvider->query = $query;

		return $dataProvider;
	}

	/**
	 * @param string $searchTerm
	 * @return ActiveDataProvider
	 */
	public function searchByQuery($searchTerm)
	{
		$dataProvider = $this->initializeDataProvider();

		/**
		 * @var ActiveQuery $query
		 */
		$query = &$dataProvider->query;

		$query->andFilterWhere([self::tableName() . '.is_active' => 1]);
		$query->andFilterWhere(['like', self::tableName() . '.title', $searchTerm]);

		return $dataProvider;
	}

	public function searchByIds(array $ids = [])
	{
		$dataProvider = $this->initializeDataProvider();

		if (empty($ids))
			$ids = [-1];

		/** @var ActiveQuery $query */
		$query = &$dataProvider->query;

		$query->andFilterWhere([self::tableName() . '.id' => $ids]);
		$query->andFilterWhere([self::tableName() . '.is_active' => 1]);

		$this->joinWithProviderTitle($query);
		$this->joinWithSetTitle($query);

		return $dataProvider;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$dataProvider = $this->initializeDataProvider();

		/**
		 * @var ActiveQuery $query
		 */
		$query = &$dataProvider->query;

		$dataProvider->setSort([
			'defaultOrder' => ['id' => SORT_DESC],
			'attributes' => [
				'id',
				'erp_title',
				'title',
				'providerTitle' => [
					'asc' => ['provider.title' => SORT_ASC],
					'desc' => ['provider.title' => SORT_DESC],
					'label' => Yii::t('app', 'Provider'),
				],
				'offerTypeTitle' => [
					'asc' => ['offer_type.title' => SORT_ASC],
					'desc' => ['offer_type.title' => SORT_DESC],
				],
				'swisscom_id',
				'swisscom_id_2',
				'setTitle' => [
					'asc' => ['sets.title' => SORT_ASC],
					'desc' => ['sets.title' => SORT_DESC],
					'label' => Yii::t('app', 'Set'),
				],
				'price_monthly',
				'contract_duration',
				'is_b2b_allowed',
				'is_active', [
					'asc' => [self::tableName() . '.is_active' => SORT_ASC],
					'desc' => [self::tableName() . '.is_active' => SORT_DESC],
				],
				'is_active_erp',
				'price_monthly_runtime',
				'updated_at' => [
					'asc' => ['tariffs.updated_at' => SORT_ASC],
					'desc' => ['tariffs.updated_at' => SORT_DESC],
					'label' => 'Updated At'
				],
			]
		]);

		if (!($this->load($params) && $this->validate())) {
			$query->joinWith('provider');
			$query->joinWith('offerType');
			$query->joinWith('partner');
			// $query->joinWith('set');
			return $dataProvider;
		}

		$this->addWhereFilters($query);
		//$this->joinWithTariffHighlights($query);
		$this->joinWithProviderTitle($query);
		$this->joinWithOfferTypeTitle($query);
		$this->joinWithSetTitle($query);

		if (empty($this->partnerTitle) === false) {
			$query->joinWith(['partner' => function (ActiveQuery $q) {
				$q->where(FulfillmentPartners::tableName() . '.title LIKE "%' . $this->partnerTitle . '%"');
			}]);
		}

		return $dataProvider;
	}

	protected function addWhereFilters(ActiveQuery &$query)
	{
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			'footnote_id' => $this->footnote_id,
			self::tableName() . '.is_active' => $this->is_active,
			'is_active_erp' => $this->is_active_erp,
			'network' => $this->network,
			'provider_id' => $this->provider_id,
			'provision' => $this->provision,
			'profit' => $this->profit,
			'price_monthly' => $this->price_monthly,
			'is_prepaid' => $this->is_prepaid,
			'is_b2b_allowed' => $this->is_b2b_allowed,
			'portable_number' => $this->portable_number,
			'young_people' => $this->young_people,
			'voicecall_data' => $this->voicecall_data,
			'internet_flat' => $this->internet_flat,
			'flatrate_mobile_networks' => $this->flatrate_mobile_networks,
			'anytime_minutes' => $this->anytime_minutes,
			'sms_flatrate_mobile_networks' => $this->sms_flatrate_mobile_networks,
			'free_sms' => $this->free_sms,
		]);

		$query->andFilterWhere(['like', self::tableName() . '.title', $this->title])
			->andFilterWhere(['like', self::tableName() . '.erp_title', $this->erp_title])
			->andFilterWhere(['like', self::tableName() . '.description', $this->description])
			->andFilterWhere(['like', self::tableName() . '.hint', $this->hint])
			->andFilterWhere(['like', self::tableName() . '.flatrate_landline', $this->flatrate_landline])
			->andFilterWhere(['like', self::tableName() . '.price_mms', $this->price_mms])
			->andFilterWhere(['like', self::tableName() . '.contract_duration', $this->contract_duration])
			->andFilterWhere(['like', self::tableName() . '.bill_online', $this->bill_online])
			->andFilterWhere(['like', self::tableName() . '.data_volume', $this->data_volume])
			->andFilterWhere(['like', self::tableName() . '.lte', $this->lte])
			->andFilterWhere(['like', self::tableName() . '.speed_reduced', $this->speed_reduced])
			->andFilterWhere(['like', self::tableName() . '.speed_max', $this->speed_max])
			->andFilterWhere(['like', self::tableName() . '.comments', $this->comments])
			->andFilterWhere(['like', self::tableName() . '.rating_value', $this->rating_value]);
	}

	/**
	 * Decorate a query with a join of table "tariff_highlights" to load TariffHighlights sub-object.
	 *
	 * @param ActiveQuery $query
	 */
	protected function joinWithTariffHighlights(ActiveQuery &$query)
	{
		$query->joinWith(['highlights' => function (ActiveQuery $q) {
			$q->joinWith('detail');
		}]);
	}


	/**
	 *
	 * @param ActiveQuery $query
	 * @param string $title
	 */
	protected function joinWithProviderTitle(ActiveQuery &$query, $title = null)
	{
		$title = $title ?: $this->providerTitle;

		$query->join('LEFT JOIN', Provider::tableName(), Tariffs::tableName() . '.provider_id = ' . Provider::tableName() . '.id');

		if ($title)
			$query->andWhere(['like', Provider::tableName() . '.title', $title]);
	}

	/**
	 *
	 * @param ActiveQuery $query
	 * @param string $title
	 */
	protected function joinWithOfferTypeTitle(ActiveQuery &$query, $title = null)
	{
		$title = $title ?: $this->offerTypeTitle;

		$query->join('LEFT JOIN', OfferType::tableName(), Tariffs::tableName() . '.offer_type_id = ' . OfferType::tableName() . '.id');

		if ($title)
			$query->andWhere(['like', OfferType::tableName() . '.title', $title]);
	}

	/**
	 * Filter a query for providers
	 *
	 * @param ActiveQuery $query    Query object to alter.
	 * @param string|int  $provider Provider to filter for, either id or name.
	 *
	 * @return bool     False, if $provider is of invalid type.
	 */
	static public function forProvider(ActiveQuery &$query, $provider)
	{
		$query->joinWith('provider');

		if(is_array($provider)) {
			$orCondition = ['or'];
			foreach($provider as $providerCondition) {
				if (is_int($providerCondition) || is_numeric($providerCondition)) {
					$orCondition[] = [Provider::tableName().'.id' => (int) $providerCondition];
				} elseif (is_string($providerCondition)) {
					$orCondition[] = [Provider::tableName().'.title' => $providerCondition];
				}
			}

			$query->andFilterWhere($orCondition);
			return true;
		}

		return false;
	}

	/**
	 * Filter a query for nets.
	 *
	 * @param ActiveQuery $query
	 * @param int         $netId
	 */
	static public function forNet(ActiveQuery &$query, $netId)
	{
		$query->joinWith(['provider' => function (ActiveQuery $q) {
			$q->joinWith('net');
		}])->andWhere([Net::tableName().'.id' => $netId]);
	}

	/**
	 * Filter a query for data volumes.
	 *
	 * @param ActiveQuery $query
	 * @param array      $volumes   A parameter key.
	 */
	static public function forVolume(ActiveQuery &$query, $volumes)
	{
		if (!isset(Yii::$app->params['filter']['tariff']['datavolumes']))
			return;

		$dataVolumes = [];
		foreach($volumes as $volume) {
			$dataVolumes[] = Yii::$app->params['filter']['tariff']['datavolumes'][$volume];
		}

		self::findValuesForBetween($query, Tariffs::tableName().'.data_volume', $dataVolumes);
	}

	/**
	 * Constructs a between statement on given values.
	 *
	 * @param ActiveQuery   $query
	 * @param string        $field  The field in the query to search for.
	 * @param array     $values One value adds a minimum, two values add a range.
	 */
	static protected function findValuesForBetween(ActiveQuery &$query, $field, $values)
	{
		$conditions = ['or'];

		foreach($values as $value) {
			if (is_array($value)) {
				if (count($value) > 1) {
					$conditions[] = array_merge(['between', $field], $value);
				} elseif (count($value) == 1) {
					$conditions[] = array_merge(['>=', $field], $value);
				}
			}
		}

		$query->andWhere($conditions);
	}

	/**
	 * Adds extras' filtering to a query.
	 *
	 * @param ActiveQuery   $query
	 * @param array         $extras
	 */
	static public function forExtras(ActiveQuery &$query, array $extras)
	{
		if (in_array('youngsters', $extras)) {
			$query->andWhere([Tariffs::tableName().'.young_people' => true]);
		}

		if (in_array('musicflat', $extras)) {
			$query->joinWith(['highlights'])->andWhere([TariffHighlightDetails::tableName().'.css_class' => TariffHighlightDetails::MUSIC_FLAT]);
		}

		if (in_array('combo', $extras)) {
			$query->joinWith('tariffSpecial')->andWhere([TariffSpecial::tableName().'.filter_type' => 1]);
		}

		if (in_array('adac', $extras)) {
			$query->joinWith('tariffSpecial')->andWhere([TariffSpecial::tableName().'.filter_type' => 2]);
		}
	}

	/**
	 * Adds pricing range filters.
	 *
	 * @param ActiveQuery   $query
	 * @param array        $pricing
	 */
	static public function forPricing(ActiveQuery &$query, $pricing)
	{
		if (!isset(Yii::$app->params['filter']['tariff']['pricing']))
			return;

		$prices = [];
		foreach($pricing as $price) {
			$prices[] = Yii::$app->params['filter']['tariff']['pricing'][$price];
		}

		self::findValuesForBetween($query, Tariffs::tableName().'.price_monthly', $prices);
	}

	/**
	 *
	 * @param ActiveQuery $query
	 * @param string $title
	 */
	protected function joinWithSetTitle(ActiveQuery &$query, $title = null)
	{
		$title = $title ?: $this->setTitle;

		$query->join('LEFT JOIN', Sets::tableName(), Tariffs::tableName() . '.set_id = ' . Sets::tableName() . '.id');

		if ($title)
			$query->andWhere(['like', Sets::tableName() . '.title', $title]);
	}

	/**
	 * Sanitizes your array for integers only. (Helper method.)
	 * @param array $array
	 */
	private function sanitizeIntegers(&$array = [])
	{
		// Sort out non-integers
		foreach ($array as $k => $i) {
			if (is_int($i) == false)
				unset($array[$k]);
		}
	}

	/**
	 * Specific search for filter attributes.
	 *
	 * @param array $aspects
	 * @return null|ActiveDataProvider
	 */
	public function filterSearch($aspects = [])
	{
		$query = Tariffs::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (isset($aspects['highlights'])) {
			if (is_array($aspects['highlights'])) {
				$matchCounts = [];
				$highlightDetailIds = [];
				foreach ($aspects['highlights'] as $css_class) {
					$details = TariffHighlightDetails::find()->where(['css_class' => $css_class])->all();
					/** @var TariffHighlightDetails $detail */
					foreach ($details as $detail) {
						$highlightDetailIds[] = $detail->id;
					}
				}
				$highlightLinks = TariffHighlights::find()->where(['in', TariffHighlights::tableName() . '.detail_id', $highlightDetailIds])->all();
				/** @var TariffHighlights $link */
				foreach ($highlightLinks as $link) {
					if (isset($matchCounts[$link->tariff_id])) {
						$matchCounts[$link->tariff_id]++;
					} else {
						$matchCounts[$link->tariff_id] = 1;
					}
				}

				$tariffIds = [];
				foreach ($matchCounts as $id => $count) {
					if ($count >= count($aspects['highlights'])) {
						$tariffIds[] = $id;
					}
				}

				$query->andFilterWhere(['in', Tariffs::tableName() . '.id', $tariffIds]);
			}
		}

		if (isset($aspects['providers'])) {
			if (is_int($aspects['providers'])) {
				$aspects['providers'] = [$aspects['providers']];
			}

			if (is_array($aspects['providers'])) {
				$this->sanitizeIntegers($aspects['providers']);
				$query->andFilterWhere(['in', 'provider_id', $aspects['providers']]);
				// Doesn't work with foreign table's PK, for unknown reason
			}
		}

		if (isset($aspects['nets'])) {
			if (is_int($aspects['nets']))
				$aspects['nets'] = [$aspects['nets']];

			if (is_array($aspects['nets'])) {
				$this->sanitizeIntegers($aspects['nets']);
				$query->andFilterWhere(['in', 'net_id', $aspects['nets']]);
			}
		}

		if (isset($aspects['isYoungPeople']) && $aspects['isYoungPeople']) {
			$query->andWhere(['young_people' => 1]);
		}

		if (isset($aspects['internet_flat']) && $aspects['internet_flat']) {
			$query->andWhere(['internet_flat' => 1]);
		}

		if (isset($aspects['priceRange']) &&
			is_array($aspects['priceRange']) &&
			count($aspects['priceRange']) == 2
		) {
			$query->andWhere(['between', 'price_monthly', $aspects['priceRange'][0], $aspects['priceRange'][1]]);
		}

		$query->andFilterWhere([Tariffs::tableName() . '.is_active' => 1]);

		if (isset($aspects['data_only'])) {
			$dataOnly = $aspects['data_only'] ? 1 : 0; // ensure tidiness, no casting of bool
			$query->andFilterWhere(['data_only' => $dataOnly]);
		}

		return $dataProvider;
	}
}
