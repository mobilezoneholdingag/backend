<?php

namespace backend\models;

use common\models\Shop;
use components\i18n\TranslatableActiveRecord;
use sammaye\audittrail\LoggableBehavior;
use Yii;

/**
 * This is the model class for table "speak_language".
 *
 * @property integer $id
 * @property string $name
 * @property ShopLanguage[] $shopLanguages
 * @property Shop[] $shops
 */
class SpeakLanguage extends TranslatableActiveRecord
{
	protected $translateAttributes = [
		'name',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'speak_language';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
		];
	}

	public function fields()
	{
		return ['name'];
	}

	public function behaviors()
	{
		return array_merge(
			parent::behaviors(),
			Yii::$app->id == 'app-backend' ? [LoggableBehavior::className()] : []
		);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShopLanguages()
	{
		return $this->hasMany(ShopLanguage::className(), ['language_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShops()
	{
		return $this->hasMany(Shop::className(), ['id' => 'shop_id'])->viaTable('shop_language',['language_id' => 'id']);
	}
}
