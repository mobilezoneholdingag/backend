<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderStatus;

/**
 * OrderStatusSearch represents the model behind the search form about `common\models\OrderStatus`.
 */
class OrderStatusSearch extends OrderStatus
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'code', 'final', 'fulfillment_partner_id', 'successful_sale', 'rejected_order', 'order_information_step'], 'integer'],
			[['title', 'user_text', 'internal_title'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = OrderStatus::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'code' => $this->code,
			'final' => $this->final,
			'fulfillment_partner_id' => $this->fulfillment_partner_id,
			'successful_sale' => $this->successful_sale,
			'rejected_order' => $this->rejected_order,
			'order_information_step' => $this->order_information_step,
		]);

		$query->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'user_text', $this->user_text])
			->andFilterWhere(['like', 'internal_title', $this->internal_title]);

		return $dataProvider;
	}
}
