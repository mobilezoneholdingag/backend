<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2016 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace backend\models\interfaces;

/**
 * Interface ListUpInterface
 *
 * Classes implementing this interface can list themself up by one of their attributes. This interface is used to
 * provide dynamic lists, as the records are changed dynamically.
 *
 * @package backend\models\interfaces
 */
interface ListUpInterface
{
	/**
	 * Lists records of this class.
	 *
	 * @return array    Array of strings, keyed by a token.
	 */
	static public function getList();
}