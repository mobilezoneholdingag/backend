<?php

namespace backend\models;

use common\models\ArticleGroupsSim;
use common\models\Provider;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ArticleGroupsSearch represents the model behind the search form about `common\models\ArticleGroups`.
 */
class ArticleGroupsSimSearch extends ArticleGroupsSim
{
	public $provider;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['simcard_id', 'providerId', 'providerTitle'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * @param $id
	 *
	 * @return ActiveDataProvider
	 */
	public function search($id)
	{
		$query = Provider::find()->indexBy('id');

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
				'sort' => [
					'attributes' => [
						'providerId' => [
							'asc' => [Provider::tableName() . '.id' => SORT_ASC],
							'desc' => [Provider::tableName() . '.id' => SORT_DESC],
							'label' => Yii::t('app', 'Provider ID'),
						],
						'providerTitle' => [
							'asc' => [Provider::tableName() . '.title' => SORT_ASC],
							'desc' => [Provider::tableName() . '.title' => SORT_DESC],
							'label' => Yii::t('app', 'Provider Name'),
						],
						'simcard_id' => [
							'asc' => [ArticleGroupsSim::tableName() . '.simcard_id' => SORT_ASC],
							'desc' => [ArticleGroupsSim::tableName() . '.simcard_id' => SORT_DESC],
							'label' => Yii::t('app', 'SIM Card'),
						],
					],
				],
				'pagination' => false,
			]
		);

		$query->orderBy(Provider::tableName() . '.title');

		return $dataProvider;
	}

}
