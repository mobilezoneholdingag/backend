<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class Check24OrderCsv extends Model
{
	public $file;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['file'], 'required'],
			['file', 'file', 'extensions' => ['csv']],
		];
	}
}
