<?php

namespace backend\models;

use yii\base\Exception;
use yii\base\Model;

class CouponBulk extends Model
{
    public $file;
    public $codes = [];

    public function attributeLabels()
    {
        return [
            'file' => 'Datei',
            'codes' => 'Codes'
        ];
    }

    public function rules()
    {
        return [

        ];
    }

    public function process()
    {

        if(!isset($this->file->tempName)) {
            return false;
        }

        $codes = [];
        $key = [];

        foreach(file($this->file->tempName) as $k => $line) {
            if($k == 0) {
                $keys = str_getcsv($line, ';');
                continue;
            }

            $codes[] = @array_combine($keys, str_getcsv($line, ';'));

        }

        $this->codes = $codes;

        return true;

    }


}