<?php

namespace backend\models;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\FulfillmentPartners;
use common\models\OfferType;
use common\models\Provider;
use common\models\Tariffs;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ArticleTariffsSearch represents the model behind the search form about `common\models\ArticleTariffs`.
 */
class ArticleTariffsSearch extends ArticleTariffs
{

	public $productName;
	public $offerTypeTitle;
	public $tariffName;
	public $memory;
	public $providerTitle;
	public $color_label;
	public $partnerTitle;

	public function init()
	{
		// Intentionally not calling parent, as we do not want to have is_active as true in the search by default
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'article_id',
					'tariff_id',
					'is_active',
					'is_active_erp',
				],
				'integer',
			],
			[['price', 'subsidy_payment'], 'number'],
			[
				[
					'external_id',
					'productName',
					'offerTypeTitle',
					'tariffName',
					'memory',
					'color_label',
					'providerTitle',
					'partnerTitle',
					'valid_from',
					'valid_until',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = ArticleTariffs::find();

		$dataProvider = new ArticleTariffsSearchDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSizeLimit' => [1, 200]
			]
		]);

		$dataProvider->setSort([
			'attributes' => [
				'external_id',
				'productName' => [
					'asc' => ['articles.product_name' => SORT_ASC],
					'desc' => ['articles.product_name' => SORT_DESC],
					'label' => Yii::t('app', 'Article')
				],
				'memory',
				'color_label',
				'providerTitle' => [
					'asc' => ['provider.title' => SORT_ASC],
					'desc' => ['provider.title' => SORT_DESC],
					'label' => Yii::t('app', 'Provider')
				],
				'tariffName' => [
					'asc' => ['tariffs.title' => SORT_ASC],
					'desc' => ['tariffs.title' => SORT_DESC],
					'label' => Yii::t('app', 'Tariff')
				],
				'price',
				'subsidy_payment',
				'partnerTitle' => [
					'asc' => ['fulfillment_partners.title' => SORT_ASC],
					'desc' => ['fulfillment_partners.title' => SORT_DESC],
					'label' => Yii::t('app', 'FulfillmentPartners')
				],
				'is_active',
				'is_active_erp',
			]
		]);

		if (!($this->load($params) && $this->validate())) {
			$query->joinWith(['article']);
			$query->joinWith(['tariff']);
			$query->joinWith(['partner']);
			$query->joinWith(['tariff.provider']);
			$dataProvider->setListAll(true);
			return $dataProvider;
		}

		$query->andFilterWhere([
			'price' => $this->price,
			'subsidy_payment' => $this->subsidy_payment,
			'memory' => $this->memory,
			'article_id' => $this->article_id,
			'tariff_id' => $this->tariff_id,
			'color_label' => $this->color_label,
			self::tableName() . '.is_active' => $this->is_active,
			self::tableName() . '.is_active_erp' => $this->is_active_erp,
		]);

		if ($this->valid_from && $this->valid_until) {
			$query->andFilterWhere([
				'or', 'valid_from<="' . $this->valid_from . '"', 'valid_from IS NULL',
			]);
			$query->andFilterWhere([
				'or', 'valid_until>="' . $this->valid_until . '"', 'valid_until IS NULL',
			]);
		}

		if ($this->external_id) {
			$query->andWhere('article_tariffs.external_id LIKE "%' . $this->external_id . '%"');
		}

		if ($this->productName) {
			$query->joinWith(
				[
					'article' => function($q) {
						$q->where(Article::tableName() . '.product_name LIKE "%' . $this->productName . '%"');
					}
				]
			);
		}

		if ($this->offerTypeTitle) {
			$query->joinWith('tariff');
			$query->leftJoin(OfferType::tableName(), Tariffs::tableName() . '.offer_type_id = ' . OfferType::tableName() . '.id');
			$query->andWhere(OfferType::tableName() . '.title LIKE "%' . $this->offerTypeTitle . '%"');
		}

		if ($this->tariffName) {
			$query->joinWith(
				[
					'tariff' => function($q) {
						$q->where(Tariffs::tableName() . '.title LIKE "%' . $this->tariffName . '%"');
					}
				]
			);
		}

		if ($this->partnerTitle) {
			$query->joinWith(
				[
					'partner' => function($q) {
						$q->where(FulfillmentPartners::tableName() . '.title LIKE "%' . $this->partnerTitle . '%"');
					}
				]
			);
		}

		if ($this->providerTitle) {
			$query->joinWith(
				[
					'tariff.provider' => function($q) {
						$q->where(Provider::tableName() . '.title LIKE "%' . $this->providerTitle . '%"');
					}
				]
			);
		}

		return $dataProvider;
	}
}
