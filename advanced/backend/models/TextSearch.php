<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Text;

/**
 * TextSearch represents the model behind the search form about `common\models\Text`.
 */
class TextSearch extends Text
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['text_id'], 'integer'],
			[['text_1', 'kommentar'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Text::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'text_id' => $this->text_id,
		]);

		$query->andFilterWhere(['like', 'text_1', $this->text_1])
			->andFilterWhere(['like', 'kommentar', $this->kommentar]);

		return $dataProvider;
	}
}
