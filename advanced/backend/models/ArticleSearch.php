<?php

namespace backend\models;

use common\models\Article;
use common\models\ArticleGroups;
use common\models\DeliveryStatus;
use common\models\Manufacturer;
use common\models\ProductCategory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class ArticleSearch extends Article
{
	/**
	 * conducted by search
	 * @var string
	 */
	public $manufacturerTitle;
	public $groupTitle;
	public $deliveryStatus;
	public $productCategoryLabel;
	public $shortName;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'id',
					'giveaway_id',
					'bonus_id',
					'delivery_status_id',
					'simcard_type',
					'memory',
					'is_active',
					'is_active_erp',
				],
				'integer',
			],
			[['price_sale', 'price_buy', 'price_strike'], 'number'],
			[
				[
					'manufacturerTitle',
					'groupTitle',
					'deliveryStatus',
					'productCategoryLabel',
					'shortName',
					'mat_no',
					'product_name',
					'group_id',
					'color',
					'color_label',
					'manufacturer_article_id',
					'regional_code',
					'delivery_includes',
					'mpn',
					'testing_institute',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	protected function initializeDataProvider(ActiveDataProvider $dataProvider = null)
	{
		$query = Article::find();

		if ($dataProvider == null) {
			$dataProvider = new ActiveDataProvider([
				'pagination' => [
					'pageSizeLimit' => [1, 200]
				]
			]);
		}

		$dataProvider->query = $query;

		return $dataProvider;
	}

	/**
	 * A search for a specific term requires a much less restrictive query.
	 *
	 * @param string $searchTerm
	 *
	 * @return ActiveDataProvider
	 */
	public function searchByQuery($searchTerm)
	{
		$dataProvider = $this->initializeDataProvider();

		/**
		 * @var ActiveQuery $query
		 */
		$query = &$dataProvider->query;

		$query->leftJoin(ArticleGroups::tableName(), ArticleGroups::tableName() . '.top_variation_id' . '=' . Article::tableName() . '.id');
		$query->leftJoin(Manufacturer::tableName(), Manufacturer::tableName() . '.id' . '=' . ArticleGroups::tableName() . '.manufacturer_id');

		$searchTerm = str_replace(' ', '%', $searchTerm);

		$query->orWhere('memory LIKE "' . $searchTerm . '"')
			->orWhere('product_name LIKE "' . $searchTerm . '"')
			->orWhere('color LIKE "' . $searchTerm . '"')
			->orWhere('color_label LIKE "' . $searchTerm . '"')
			->orWhere('articleGroup.short_name LIKE "' . $searchTerm . '"')
			->orWhere(Manufacturer::tableName() . '.title LIKE "' . $searchTerm . '"')
			->andFilterWhere([Article::tableName() . '.is_active' => true]);

		return $dataProvider;
	}

	public function searchByIds(array $ids = [])
	{
		$dataProvider = $this->initializeDataProvider();

		if (empty($ids))
			$ids = [-1];

		/** @var ActiveQuery $query */
		$query = &$dataProvider->query;

		$query->andFilterWhere([self::tableName() . '.id' => $ids]);
		$query->andFilterWhere([self::tableName() . '.is_active' => true]);

		$this->joinWithArticleGroup($query);
		$this->joinWithDeliveryStatus($query);
		$this->joinWithManufacturer($query);

		return $dataProvider;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$dataProvider = $this->initializeDataProvider();

		/**
		 * @var ActiveQuery $query
		 */
		$query = &$dataProvider->query;

		$query->joinWith(['stock']);

		$dataProvider->setSort([
			'defaultOrder' => [
				'is_active' => SORT_DESC,
				'id' => SORT_DESC,
				'updated_at' => SORT_DESC,
			],
			'attributes' => [
				'id',
				'mat_no',
				'product_name',
				'productCategoryLabel' => [
					'asc' => ['article_groups.product_category.type' => SORT_ASC],
					'desc' => ['article_groups.product_category.type' => SORT_DESC],
				],
				'groupTitle' => [
					'asc' => ['article_groups.title' => SORT_ASC],
					'desc' => ['article_groups.title' => SORT_DESC],
					'label' => 'Article Group'
				],
				'manufacturerTitle' => [
					'asc' => ['manufacturer.title' => SORT_ASC],
					'desc' => ['manufacturer.title' => SORT_DESC],
					'label' => 'Manufacturer'
				],
				'shortName',
				'memory',
				'color_label',
				'price_sale',
				'deliveryStatus' => [
					'asc' => ['delivery_status.label' => SORT_ASC],
					'desc' => ['delivery_status.label' => SORT_DESC],
					'label' => 'Delivery Status'
				],
				'updated_at' => [
					'asc' => ['articles.updated_at' => SORT_ASC],
					'desc' => ['articles.updated_at' => SORT_DESC],
					'label' => 'Updated At'
				],
				'is_active' => [
					'asc' => ['articles.is_active' => SORT_ASC],
					'desc' => ['articles.is_active' => SORT_DESC],
					'label' => 'aktiv'
				],
				'is_active_erp'
			]
		]);

		$dataProvider->sort->attributes['stockQuantity'] = [
			'asc' => ['stock.quantity' => SORT_ASC],
			'desc' => ['stock.quantity' => SORT_DESC],
		];

		if (!($this->load($params) && $this->validate())) {
			$query->joinWith(['articleGroup.manufacturer']);
			$query->joinWith(['articleGroup']);
			$query->joinWith(['deliveryStatus']);
			$query->joinWith(['articleGroup.productCategory']);
			return $dataProvider;
		}

		$query->andFilterWhere([
			self::tableName() . '.is_active' => $this->is_active,
			self::tableName() . '.is_active_erp' => $this->is_active_erp,
			'articles.id' => $this->id,
			'giveaway_id' => $this->giveaway_id,
			'bonus_id' => $this->bonus_id,
			'group_id' => $this->group_id,
			'price_sale' => $this->price_sale,
			'price_buy' => $this->price_buy,
			'delivery_status_id' => $this->delivery_status_id,
			'simcard_type' => $this->simcard_type,
			'memory' => $this->memory,
			'price_strike' => $this->price_strike,
		]);

		$query->andFilterWhere(['like', 'product_name', $this->product_name])
			->andFilterWhere(['like', 'color', $this->color])
			->andFilterWhere(['like', 'color_label', $this->color_label])
			->andFilterWhere(['like', 'manufacturer_article_id', $this->manufacturer_article_id])
			->andFilterWhere(['like', 'regional_code', $this->regional_code])
			->andFilterWhere(['like', 'delivery_includes', $this->delivery_includes])
			->andFilterWhere(['like', 'mpn', $this->mpn])
			->andFilterWhere(['like', 'testing_institute', $this->testing_institute])
			->andFilterWhere(['like', 'mat_no', $this->mat_no]);

		$this->joinWithArticleGroup($query);
		$this->joinWithManufacturer($query);
		$this->joinWithDeliveryStatus($query);
		$this->joinWithProductCategory($query);

		return $dataProvider;
	}

	/**
	 * "Decorate" a query with a join of the table "manufacturer" to load a manufacturer sub-object.
	 *
	 * @param ActiveQuery &$query
	 */
	protected function joinWithManufacturer(ActiveQuery &$query)
	{
		$query->joinWith(['manufacturer' => function (ActiveQuery $q) {
			$q->where(['like', Manufacturer::tableName() . '.title', [$this->manufacturerTitle]]);
		}]);
	}

	/**
	 * "Decorate" a query with a join of the table "article_groups" to load a ArticleGroups sub-object.
	 *
	 * @param ActiveQuery &$query
	 */
	protected function joinWithArticleGroup(ActiveQuery &$query)
	{
		if ($this->groupTitle || $this->shortName) {
			$query->joinWith(['articleGroup' => function (ActiveQuery $q) {
				$q->where(['like', ArticleGroups::tableName() . '.title', [$this->groupTitle]])
					->andWhere(['like', ArticleGroups::tableName() . '.short_name', [$this->shortName]]);
			}]);
		}
	}

	/**
	 * "Decorate" a query with a join of the table "article_groups" to load a ArticleGroups sub-object.
	 *
	 * @param ActiveQuery &$query
	 */
	protected function joinWithDeliveryStatus(ActiveQuery &$query)
	{
		$query->joinWith(['deliveryStatus' => function (ActiveQuery $q) {
			$q->where(['like', DeliveryStatus::tableName() . '.label', [$this->deliveryStatus]]);
		}]);
	}

	protected function joinWithProductCategory(ActiveQuery &$query)
	{
		$query->joinWith(['productCategory' => function (ActiveQuery $q) {
			$q->where(['like', ProductCategory::tableName() . '.type', [$this->productCategoryLabel]]);
		}]);
	}

	public function searchSpecials()
	{
		$dataProvider = $this->initializeDataProvider();

		$dataProvider->setSort([
			'defaultOrder' => [
				'is_active' => SORT_DESC,
				'id' => SORT_DESC,
				'updated_at' => SORT_DESC,
			],
		]);

		$query = &$dataProvider->query;
		$query->where(['not', ['image_overlay_text' => null]]);

		return $dataProvider;
	}
}
