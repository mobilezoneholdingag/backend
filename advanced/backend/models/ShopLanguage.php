<?php

namespace backend\models;

use common\models\Shop;

/**
 * This is the model class for table "shop_language".
 *
 * @property integer $shop_id
 * @property integer $language_id
 * @property SpeakLanguage $language
 * @property Shop $shop
 */
class ShopLanguage extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'shop_language';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['shop_id', 'language_id'], 'required'],
			[['shop_id', 'language_id'], 'integer'],
			[
				['language_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => SpeakLanguage::className(),
				'targetAttribute' => ['language_id' => 'id'],
			],
			[
				['shop_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Shop::className(),
				'targetAttribute' => ['shop_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'shop_id' => 'Shop ID',
			'language_id' => 'Language ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLanguage()
	{
		return $this->hasOne(SpeakLanguage::className(), ['id' => 'language_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShop()
	{
		return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
	}
}
