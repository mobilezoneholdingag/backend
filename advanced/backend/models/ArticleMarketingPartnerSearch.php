<?php

namespace backend\models;

use common\models\Article;
use common\models\ArticleMarketingPartner;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class ArticleMarketingPartnerSearch extends ArticleSearch
{
	public $added;

	public function rules()
	{
		return ArrayHelper::merge([[['added'], 'safe']], parent::rules());
	}

	public function search($params)
	{
		$dataProvider = parent::search($params);

		if (!$this->load($params) || !$this->validate() || is_null($this->added)) {
			return $dataProvider;
		}

		/** @var ActiveQuery $query */
		$query = &$dataProvider->query;

		$addedIds = ArticleMarketingPartner::find()
			->select(['article_id'])
			->where(['partner_id' => $params['id']])
			->column();

		$query->andWhere([$this->added ? 'in' : 'not in', Article::tableName() . '.id', $addedIds]);

		return $dataProvider;
	}
}
