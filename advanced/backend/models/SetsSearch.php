<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\set\models\Sets;
use yii\db\ActiveQuery;
use common\models\ProductCategory;

/**
 * SetsSearch represents the model behind the search form about `common\models\Sets`.
 */
class SetsSearch extends Sets
{

	public $productCategoryTitle;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'type'], 'integer'],
			[['title', 'system_title', 'description', 'productCategoryTitle'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Sets::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSizeLimit' => [1, 200]
			]

		]);

		$dataProvider->setSort([
			'attributes' => [
				'title',
				'system_title',
				'productCategoryTitle' => [
					'asc' => ['product_category.type' => SORT_ASC],
					'desc' => ['product_category.type' => SORT_DESC],
				],
			]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			$query->joinWith(['productCategory']);
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'type' => $this->type,
		]);

		$query->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'system_title', $this->system_title])
			->andFilterWhere(['like', 'description', $this->description]);

		return $dataProvider;
	}
}
