<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MarketingImages;

/**
 * MarketingImagesSearch represents the model behind the search form about `common\models\MarketingImages`.
 */
class MarketingImagesSearch extends MarketingImages
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['target_url', 'tag', 'language', 'updated_at', 'created_at', 'publish_at'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = MarketingImages::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'=> ['defaultOrder' => ['publish_at' => SORT_DESC, 'id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
		]);

		$query->andFilterWhere(['like', 'target_url', $this->target_url])
			->andFilterWhere(['like', 'tag', $this->tag])
			->andFilterWhere(['like', 'language', $this->language])
			->andFilterWhere(['like', 'updated_at', $this->updated_at])
			->andFilterWhere(['like', 'created_at', $this->created_at])
			->andFilterWhere(['like', 'publish_at', $this->publish_at]);

		return $dataProvider;
	}
}
