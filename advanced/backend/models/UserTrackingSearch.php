<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserTracking;

/**
 * UserTrackingSearch represents the model behind the search form about `common\models\UserTracking`.
 */
class UserTrackingSearch extends UserTracking
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['visitor_id', 'clicks', 'revisits', 'basket', 'gekauft', 'shop_id'], 'integer'],
			[['banner_id', 'session_id', 'ip_address', 'hoster', 'browser', 'entry_time', 'first_entry', 'last_click', 'referer'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = UserTracking::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'visitor_id' => $this->visitor_id,
			'clicks' => $this->clicks,
			'entry_time' => $this->entry_time,
			'first_entry' => $this->first_entry,
			'last_click' => $this->last_click,
			'revisits' => $this->revisits,
			'basket' => $this->basket,
			'gekauft' => $this->gekauft,
			'shop_id' => $this->shop_id,
		]);

		$query->andFilterWhere(['like', 'banner_id', $this->banner_id])
			->andFilterWhere(['like', 'session_id', $this->session_id])
			->andFilterWhere(['like', 'ip_address', $this->ip_address])
			->andFilterWhere(['like', 'hoster', $this->hoster])
			->andFilterWhere(['like', 'browser', $this->browser])
			->andFilterWhere(['like', 'referer', $this->referer]);

		return $dataProvider;
	}
}
