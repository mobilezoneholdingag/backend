<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TariffPromotion;
use common\models\Tariffs;
use yii\db\ActiveQuery;

/**
 * TariffPromotionSearch represents the model behind the search form about `common\models\TariffPromotion`.
 */
class TariffPromotionSearch extends TariffPromotion
{

	public $tariffTitle;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'tariff_id'], 'integer'],
			[['code', 'tariffTitle'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = TariffPromotion::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->setSort([
			'attributes' => [
				'tariffTitle' => [
					'asc' => [Tariffs::tableName().'.title' => SORT_ASC],
					'desc' => [Tariffs::tableName().'.title' => SORT_DESC],
					'label' => Yii::t('app', 'Tariff')
				],
				'code'
			]
		]);

		if (!($this->load($params) && $this->validate())) {
			$query->joinWith(['tariff']);
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'tariff_id' => $this->tariff_id,
		]);

		$query->andFilterWhere(['like', 'code', $this->code]);

		$query->joinWith(['tariff' => function ($q) {
			$q->where(Tariffs::tableName().'.title LIKE "%' . $this->tariffTitle . '%"');
		}]);

		return $dataProvider;
	}
}
