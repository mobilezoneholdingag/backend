<?php

namespace backend\models\content\file;

use yii\helpers\ArrayHelper;

class PdfUpload extends DefaultUpload
{
	public function rules()
	{
		return ArrayHelper::merge(
			parent::rules(),
			[[
				'file',
				'file',
				'skipOnEmpty' => false,
				'wrongExtension' => 'Die Datei {file} hat den falschen Dateityp! Erlaubt ist ausschließlich der Typ {extensions}.',
				'extensions' => ['pdf'],
			]]
		);
	}
}
