<?php

namespace backend\models\content\file;

use components\filesystem\FilesystemProviderInterface;
use League\Flysystem\FilesystemInterface;
use yii\base\Model;
use yii\web\UploadedFile;

class DefaultUpload extends Model
{
	/** @var UploadedFile */
	public $file;
	public $remoteFileName;
	/** @var  FilesystemInterface */
	protected $fs;
	/** @var FilesystemProviderInterface */
	protected $fsProvider;
	protected $configName;


	public function __construct(FilesystemProviderInterface $fsProvider, string $configName = 'default')
	{
		$this->fsProvider = $fsProvider;
		$this->configName = $configName;

		parent::__construct();
	}

	public function getUrl($filename): string
	{
		return $this->fsProvider->url($filename, $this->configName);
	}

	public function download($filename): string
	{
		return $this->fsProvider->fs($this->configName)->get($filename)->read();
	}


	public function rules()
	{
		return [[
			'file',
			'file',
			'skipOnEmpty' => false,
			'wrongExtension' => 'The file {file} has the wrong extension: {extensions}',
		]];
	}

	public function upload(): bool
	{
		if ($this->validate()) {
			$this->remoteFileName = time() . '_' . $this->file->name;
			$this->fsProvider->fs($this->configName)->put(
				$this->remoteFileName,
				file_get_contents($this->file->tempName)
			);

			return true;
		} else {
			return false;
		}
	}
}
