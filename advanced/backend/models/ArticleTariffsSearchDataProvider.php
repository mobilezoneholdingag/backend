<?php

namespace backend\models;

use common\models\ArticleTariffs;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\QueryInterface;

/**
 *
 */
class ArticleTariffsSearchDataProvider extends ActiveDataProvider
{
	/**
	 * @var bool
	 */
	private $listAll;

	/**
	 * ActiveDataProvider implementation is kind of stupid here and re-uses the query that has all the JOINs still in it.
	 * To just count (as we are solely using LEFT-joins), a simple count on the article tariffs is greatly sufficing.
	 *
	 * @return int|string
	 * @throws InvalidConfigException
	 */
	protected function prepareTotalCount()
	{
		if (!$this->query instanceof QueryInterface) {
			throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
		}

		if ($this->listAll) {
			return ArticleTariffs::find()->count();
		}

		// We need the default implementation though if stuff is filtered, because the counts, and thus the
		// pagination, would be off otherwise
		return parent::prepareTotalCount();
	}

	/**
	 * @param bool $enabled
	 */
	public function setListAll($enabled)
	{
		$this->listAll = $enabled;
	}
}
