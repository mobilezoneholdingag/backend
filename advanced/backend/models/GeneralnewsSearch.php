<?php

namespace backend\models;

use common\models\GeneralNews;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * FootnotesSearch represents the model behind the search form about `common\models\Footnotes`.
 */
class GeneralnewsSearch extends GeneralNews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['name, link, date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeneralNews::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date'=>SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
        ]);


        return $dataProvider;
    }
}
