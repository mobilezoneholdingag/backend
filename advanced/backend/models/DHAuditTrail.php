<?php

namespace backend\models;

use sammaye\audittrail\AuditTrail;
use Yii;

/**
 * The followings are the available columns in table 'tbl_audit_trail':
 *
 * @var integer $id
 * @var string $new_value
 * @var string $old_value
 * @var string $action
 * @var string $model
 * @var string $field
 * @var string $stamp
 * @var integer $user_id
 * @var string $model_id
 * @var User $user
 */
class DHAuditTrail extends AuditTrail
{
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'old_value' => Yii::t('app', 'Old Value'),
			'new_value' => Yii::t('app', 'New Value'),
			'action' => Yii::t('app', 'Action'),
			'model' => Yii::t('app', 'Type'),
			'field' => Yii::t('app', 'Field'),
			'stamp' => Yii::t('app', 'Stamp'),
			'user_id' => Yii::t('app', 'User'),
			'model_id' => Yii::t('app', 'ID'),
		];
	}

	public function getUserName()
	{
		return $this->user->username;
	}
}