<?php

namespace backend\models;

use common\models\Article;
use common\models\ArticleMarketingPartner;
use common\modules\feed\models\FeedAbstract;
use DateTime;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "marketing_partners".
 *
 * @property integer $id
 * @property string $title
 * @property string $filename
 * @property integer $logic
 * @property integer $warranty
 * @property DateTime $last_file_generated_at
 * @property DateTime $last_accessed_at
 */
class MarketingPartner extends ActiveRecord
{
	const VERIVOX_ID = 1;
	const CHECK24_ID = 2;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'marketing_partners';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'filename'], 'string', 'max' => 255],
			[['logic', 'warranty'], 'integer'],
			[['warranty'], 'integer', 'min' => 0, 'max' => 255],
			[['last_file_generated_at', 'logic', 'warranty'], 'safe']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'filename' => Yii::t('app', 'Dateiname'),
			'last_file_generated_at' => Yii::t('app', 'Letzes Dateiupdate'),
			'warranty' => Yii::t('app', 'Garantie (in Monaten)'),
			'logic' => Yii::t('app', 'Logik-Typ'),
		];
	}

	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [LoggableBehavior::className()] : [];
	}

	public function getArticles()
	{
		return $this->hasMany(Article::class, ['id' => 'article_id'])
			->viaTable(ArticleMarketingPartner::tableName(), ['partner_id' => 'id']);
	}

	public function getLogicType()
	{
		return FeedAbstract::getLogicTypeLabels()[$this->logic] ?? null;
	}
}
