<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class ContentSearchResult extends Model
{
	public $section;
	public $attribute;
	public $url;
	public $snippet;


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['section', 'snippet', 'attribute'], 'string'],
			[['url'], 'url']
		];
	}

	public function attributeLabels()
	{
		return [
			'section' => Yii::t('app', 'Section'),
			'snippet' => Yii::t('app', 'Snippet'),
			'url' => Yii::t('app', 'URL'),
			'attribute' => Yii::t('app', 'Attribute'),
		];
	}




}
