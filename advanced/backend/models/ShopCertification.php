<?php

namespace backend\models;

use common\models\Shop;

/**
 * This is the model class for table "shop_certification".
 *
 * @property integer $shop_id
 * @property integer $certification_id
 * @property Certification $certification
 * @property Shop $shop
 */
class ShopCertification extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'shop_certification';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['shop_id', 'certification_id'], 'required'],
			[['shop_id', 'certification_id'], 'integer'],
			[
				['certification_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Certification::className(),
				'targetAttribute' => ['certification_id' => 'id'],
			],
			[
				['shop_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Shop::className(),
				'targetAttribute' => ['shop_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'shop_id' => 'Shop ID',
			'certification_id' => 'Certification ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCertification()
	{
		return $this->hasOne(Certification::className(), ['id' => 'certification_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShop()
	{
		return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
	}
}
