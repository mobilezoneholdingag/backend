<?php

namespace backend\models;

use common\models\Article;
use common\models\ArticleGroups;
use common\models\Manufacturer;
use common\models\Tariffs;
use common\modules\set\models\Sets;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * ArticleGroupsSearch represents the model behind the search form about `common\models\ArticleGroups`.
 */
class ArticleGroupsSearch extends ArticleGroups
{

	public $manufacturerTitle;
	public $accessorySetTitle;
	public $startTariffTitle;
	public $topVariationTitle;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'tariff_set_id',
					'accessory_set_id',
					'start_tariff_id',
					'top_variation_id',
				],
				'integer',
			],
			[['title', 'short_name'], 'string'],
			[['manufacturerTitle', 'startTariffTitle', 'topVariationTitle', 'accessorySetTitle'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = ArticleGroups::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSizeLimit' => [1, 200]
			]
		]);

		$dataProvider->setSort([
			'attributes' => [
				'short_name',
				'title',
				'manufacturerTitle' => [
					'asc' => ['manufacturer.title' => SORT_ASC],
					'desc' => ['manufacturer.title' => SORT_DESC],
					'label' => 'Manufacturer'
				],
				'accessorySetTitle' => [
					'asc' => ['sets.title' => SORT_ASC],
					'desc' => ['sets.title' => SORT_DESC],
					//'label' => Yii::t('app', 'Tariff Set')
				],
				'startTariffTitle' => [
					'asc' => ['tariffs.title' => SORT_ASC],
					'desc' => ['tariffs.title' => SORT_DESC],
					'label' => Yii::t('app', 'Start Tariff')
				],
				'topVariationTitle' => [
					'asc' => ['articles.product_name' => SORT_ASC],
					'desc' => ['articles.product_name' => SORT_DESC],
					'label' => Yii::t('app', 'Top Variation')
				],
			]
		]);

		if (!($this->load($params) && $this->validate())) {
			$query->joinWith(['manufacturer']);
			$query->joinWith(['accessorySet']);
			$query->joinWith(['startTariff']);
			$query->joinWith(['topVariation']);
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'accessory_set_id' => $this->tariff_set_id,
			'start_tariff_id' => $this->start_tariff_id,
			'manufacturer_id' => $this->manufacturer_id,
			'top_variation_id' => $this->top_variation_id
		]);

		if (empty($this->title) === false) {
			$query->andFilterWhere(['like', ArticleGroups::tableName() . '.title', $this->title]);
		}

		$query->joinWith(['manufacturer' => function (ActiveQuery $q) {
			if (empty($this->manufacturerTitle) === false) {
				$q->where(['like', Manufacturer::tableName() . '.title', [$this->manufacturerTitle]]);
			}
		}]);

		$query->joinWith(['accessorySet' => function (ActiveQuery $q) {
			if (empty($this->accessorySetTitle) === false) {
				$q->where(['like', Sets::tableName() . '.title', [$this->accessorySetTitle]]);
			}
		}]);

		$query->joinWith(['startTariff' => function (ActiveQuery $q) {
			if (empty($this->startTariffTitle) === false) {
				$q->where(['like', Tariffs::tableName() . '.title', [$this->startTariffTitle]]);
			}
		}]);

		$query->joinWith(['topVariation' => function (ActiveQuery $q) {
			if (empty($this->topVariationTitle) === false) {
				$q->where(['like', Article::tableName() . '.product_name', [$this->topVariationTitle]]);
			}
		}]);

		return $dataProvider;
	}


}
