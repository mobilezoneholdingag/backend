<?php

namespace backend\models;

use common\models\Shop;
use components\i18n\TranslatableActiveRecord;
use sammaye\audittrail\LoggableBehavior;
use Yii;

/**
 * This is the model class for table "certification".
 *
 * @property integer $id
 * @property string $type
 * @property ShopCertification[] $shopCertifications
 * @property Shop[] $shops
 */
class Certification extends TranslatableActiveRecord
{
	protected $translateAttributes = [
		'type',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'certification';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['type'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'type' => 'Type',
		];
	}

	public function fields()
	{
		return ['type'];
	}

	public function behaviors()
	{
		return array_merge(
			parent::behaviors(),
			Yii::$app->id == 'app-backend' ? [LoggableBehavior::className()] : []
		);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShopCertifications()
	{
		return $this->hasMany(ShopCertification::className(), ['certification_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShops()
	{
		return $this->hasMany(Shop::className(), ['id' => 'shop_id'])->viaTable(
			'shop_certification',
			['certification_id' => 'id']
		);
	}
}
