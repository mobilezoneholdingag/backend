<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\set\models\Chart;

/**
 * ChartSearch represents the model behind the search form about `common\modules\set\models\Chart`.
 */
class ChartSearch extends Chart
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['set_id', 'position', 'product_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Chart::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'set_id' => $this->set_id,
			'position' => $this->position,
			'product_id' => $this->product_id,
		]);

		return $dataProvider;
	}
}
