<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Footnotes;

/**
 * FootnotesSearch represents the model behind the search form about `common\models\Footnotes`.
 */
class FootnotesSearch extends Footnotes
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'number'], 'integer'],
			[['text'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Footnotes::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'number' => $this->number,
		]);

		$query->andFilterWhere(['like', 'text', $this->text]);

		return $dataProvider;
	}
}
