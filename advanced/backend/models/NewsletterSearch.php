<?php

namespace backend\models;

use frontend\models\Newsletter;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * NewsletterSearch represents the model behind the search form about `frontend\models\Newsletter`.
 */
class NewsletterSearch extends Newsletter
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'confirmed'], 'integer'],
			[['email', 'bid','submit_source'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Newsletter::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created' => SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			//'created' => $this->created,
			//'deleted' => $this->deleted,
			'confirmed' => $this->confirmed,
		]);

		$query->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', 'bid', $this->bid]);

		$query->andFilterWhere(['like', 'submit_source', $this->submit_source]);

		return $dataProvider;
	}
}
