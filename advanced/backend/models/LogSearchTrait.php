<?php

namespace backend\models;

use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * @property Model $this
 * @method string className()
 */
trait LogSearchTrait
{
	public function getAllCategories(): array
	{
		/** @var ActiveRecord $class */
		$class = static::className();

		$categories = $class::find()
			->select('category')
			->groupBy('category')
			->column();

		return array_combine($categories, $categories);
	}
}
