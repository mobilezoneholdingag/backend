<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LandingPages;

/**
 * LandingPagesSearch represents the model behind the search form about `common\models\LandingPages`.
 */
class LandingPagesSearch extends LandingPages
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['title', 'url', 'meta_description', 'canonical_url', 'redirect_url', 'html_code'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = LandingPages::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'updated_at' => SORT_DESC,
				]
			],
			'pagination' => [
				'pageSizeLimit' => [1,200]
			]
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
		]);

		$query->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'url', $this->url])
			->andFilterWhere(['like', 'meta_description', $this->meta_description])
			->andFilterWhere(['like', 'redirect_url', $this->redirect_url])
			->andFilterWhere(['like', 'canonical_url', $this->canonical_url])
			->andFilterWhere(['like', 'html_code', $this->html_code]);

		return $dataProvider;
	}
}
