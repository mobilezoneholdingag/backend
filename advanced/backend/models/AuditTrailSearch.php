<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use yii\db\ActiveQuery;

/**
 * AuditTrailSearch represents the model behind the search form about `sammaye\audittrail\AuditTrail`.
 */
class AuditTrailSearch extends DHAuditTrail
{

	public $userName;
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['old_value', 'new_value', 'action', 'model', 'field', 'stamp', 'user_id', 'model_id', 'userName'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = DHAuditTrail::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->setSort([
			'attributes' => [
				'stamp',
				'userName' => [
					'asc' => ['user.username' => SORT_ASC],
					'desc' => ['user.username' => SORT_DESC],
					],
					'model',
					'action',
					'field',
					'model_id'
			]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			$query->joinWith(['user']);
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'stamp' => $this->stamp,
		]);

		$query->andFilterWhere(['like', 'old_value', $this->old_value])
			->andFilterWhere(['like', 'new_value', $this->new_value])
			->andFilterWhere(['like', 'action', $this->action])
			->andFilterWhere(['like', 'model', $this->model])
			->andFilterWhere(['like', 'field', $this->field])
			->andFilterWhere(['like', 'user_id', $this->user_id])
			->andFilterWhere(['like', 'model_id', $this->model_id]);

		$query->orderBy('stamp DESC');

		$this->joinWithUser($query);

		return $dataProvider;
	}

	protected function joinWithUser(ActiveQuery &$query)
	{
		$query->joinWith(['user' => function (ActiveQuery $q) {
			$q->where(['like', User::tableName() . '.username', [$this->userName]]);
		}]);
	}
}
