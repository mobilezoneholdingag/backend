<?php
namespace common\behaviors;


use RuntimeException;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Class SerializeBehavior
 *
 * This behavior is to serialize an attribute before the active record will be saved
 * and to deserialize the attribute after the find call.
 *
 * @package common\behaviors
 */
class SerializeBehavior extends Behavior
{
	public $attributes = [];

	public $entity;

	/**
	 * @return array
	 */
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeSave',
			ActiveRecord::EVENT_AFTER_FIND => 'afterLoad',
		];
	}

	/**
	 * Makes a json_encode on all given attributes.
	 */
	public function beforeSave()
	{
		foreach ($this->attributes as $attributeName) {
			if (is_object($this->entity->$attributeName)) {
				throw new RuntimeException("The SerializeBehavior can not handle objects, but $attributeName in entity " . get_class($this->entity) . " is an object.");
			}
			$this->entity->$attributeName = serialize($this->entity->$attributeName);
		}
	}

	/**
	 * Makes a json_decode on all given attributes.
	 */
	public function afterLoad()
	{
		foreach ($this->attributes as $attributeName) {
			$this->entity->$attributeName = @unserialize($this->entity->$attributeName);
		}
	}
}
