<?php
namespace common\behaviors;


use yii\base\Behavior;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;

class DateBehavior extends Behavior
{
	const STORED_FORMAT = 'php:Y-m-d';

	public $attributes = [];

	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
			ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
		];
	}

	public function beforeSave()
	{
		$formatter = \Yii::$app->formatter;

		foreach ($this->attributes as $attribute) {
			if (!($value = $this->owner->{$attribute})) {
				continue;
			}

			try {
				$this->owner->{$attribute} = $formatter->asDate($value, self::STORED_FORMAT);
			} catch (InvalidParamException $e) {
				$this->owner->{$attribute} = null;
			}
		}
	}
}
