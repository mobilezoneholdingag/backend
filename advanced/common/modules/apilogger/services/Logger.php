<?php

namespace common\modules\apilogger\services;

use common\modules\apilogger\models\ApiLog;

class Logger
{
	/** @var float */
	private $startTime;
	/** @var ApiLog */
	private $log;
	/** @var string */
	private $apiLogClass;
	/** @var array */
	private $extraAttributes = [];

	public function __construct(string $apiLogClass)
	{
		$this->apiLogClass = $apiLogClass;
	}

	public function start(array $requestInformation, string $url, string $method = 'GET')
	{
		$this->startTime = microtime(true);
		$this->log = new $this->apiLogClass($this->extraAttributes);
		$this->log->request = json_encode($requestInformation);
		$this->log->url = $url;
		$this->log->method = $method;
		$this->log->save();
	}

	public function log(
		string $responseHeaders,
		string $response
	) {
		if ($this->startTime) {
			$this->log->execution_time = (microtime(true) - $this->startTime) * 1000;
		}
		$this->log->response_headers = $responseHeaders;
		$this->log->response = $response;
		$this->log->save();
	}

	public function timeout()
	{
		$this->log->response = 'timeout';
		$this->log->execution_time = 0;
		$this->log->save();
	}

	public function __set($name, $value)
	{
		$this->extraAttributes[$name] = $value;
	}

	public function __get($name)
	{
		return $this->log->{$name};
	}
}
