<?php

namespace common\modules\apilogger\models;

use common\models\behaviors\TimestampBehaviorSettings;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $created_at
 * @property string $url
 * @property string $method
 * @property string $request
 * @property integer $response_headers
 * @property integer $execution_time
 * @property string $response
 */
abstract class ApiLog extends ActiveRecord
{
	abstract public static function getTableName(): string;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return static::getTableName();
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAt(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['created_at'], 'safe'],
			[['url', 'method'], 'required'],
			[['url'], 'string', 'max' => 500],
			[['method'], 'string', 'max' => 50],
		];
	}
}
