<?php

namespace common\modules\apilogger;

use deinhandy\yii2container\Module;
use Yii;
use yii\console\Application;

class ApiLoggerModule extends Module
{
	public function init()
	{
		parent::init();
		if (Yii::$app instanceof Application) {
			$this->controllerNamespace = 'common\modules\apilogger\controllers';
		}
	}
}
