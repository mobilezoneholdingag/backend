<?php
namespace common\modules\feed;

use deinhandy\yii2container\Module;
use Yii;
use yii\console\Application as ConsoleApplication;

class FeedModule extends Module
{
	public function init()
	{
		parent::init();
		if (Yii::$app instanceof ConsoleApplication) {
			$this->controllerNamespace = 'common\modules\feed\controllers\console';
		}
	}
}
