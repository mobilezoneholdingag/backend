<?php
namespace common\modules\feed\models;


interface FeedInterface
{
	const TYPE_BACKLINKING = 1;
	const TYPE_SELLING_PLATTFORM = 2;

	public function generateFeed();
	public function getFilename();
	public function getOutput();
}
