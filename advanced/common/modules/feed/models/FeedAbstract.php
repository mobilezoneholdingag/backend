<?php
namespace common\modules\feed\models;

use backend\models\MarketingPartner;
use common\models\Article;
use common\modules\feed\FeedModule;
use Exception;
use SimpleXMLElement;
use Yii;
use yii\data\ActiveDataProvider;

abstract class FeedAbstract implements FeedInterface
{
	const TYPE = FeedInterface::TYPE_BACKLINKING;
	const REPLACE_MAP = [
		' ' => '-',
		'.' => '-',
		'ä' => 'ae',
		'ö' => 'oe',
		'ü' => 'ue',
		'ß' => 'ss',
	];

	protected $partner;
	protected $entitiesQueryCallback = 'getArticles';
	protected $output = '';
	protected $shopUrl;

	public function __construct(MarketingPartner $partner)
	{
		$this->partner = $partner;
		$this->shopUrl = Yii::$app->urlManagerFrontend->hostInfo;
	}

	protected function getConfig()
	{
		return Yii::$app->params['feeds'][$this->partner->title];
	}

	public static function getLogicTypes(): array
	{
		return FeedModule::getInstance()->params['logicTypes'] ?? [];
	}

	public static function getLogicTypeLabels(): array
	{
		return array_keys(self::getLogicTypes());
	}

	protected function getEntities()
	{
		$dataProvider = $this->getPreparedDataprovider();
		for ($i = 1; $i <= $dataProvider->getPagination()->getPageCount(); $i++) {
			$articles = $dataProvider->getModels();
			if (isset($articles)) {
				foreach ($articles as $article) {
					yield $article;
				}
			}
			$dataProvider->getPagination()->setPage($i);
			$dataProvider->prepare(true);
		}
	}

	protected function getPreparedDataprovider()
	{
		$dataProvider = new ActiveDataProvider();
		$dataProvider->query = call_user_func([$this->partner, $this->entitiesQueryCallback]);
		$dataProvider->prepare(true);
		return $dataProvider;
	}

	public function generateFeed()
	{
		$this->beforeGenerateFeed();
		$entities = $this->getEntities();
		foreach ($entities as $entity) {
			if ($entity->is_active && $entity->is_active_erp) {
				try {
				    if($this->partner->getLogicType() == "Epoq"){
                        $this->storeFeedLine($this->getFeedLine($entity,"de-CH"));
                        $this->storeFeedLine($this->getFeedLine($entity,"fr-FR"));
                        $this->storeFeedLine($this->getFeedLine($entity,"it-IT"));
                    }else {
                        $this->storeFeedLine($this->getFeedLine($entity));
                    }
				} catch (Exception $e) {
                    if($this->partner->getLogicType() != "Epoq") {
                        Yii::warning("Skipped in {$this->partner->title} with id = {$entity->id}. {$e->getMessage()}", 'feed');
                        Yii::warning($e->getMessage() . " The entity will be skipped in feed generation of feed {$this->partner->title} . {$entity->id}");
                    }
				}
			}else{
                if($this->partner->getLogicType() != "Epoq") {
                    Yii::warning("Skipped in {$this->partner->title} with id = {$entity->id}. is not active & not erp_active not in Feed", 'feed');
                }
			}
		}
		$this->afterGenerateFeed();
	}

	public function getOutput()
	{
		return $this->output;
	}

	protected function getArticlePrice(Article $article): float
	{
		switch(static::TYPE) {
			case FeedInterface::TYPE_BACKLINKING:
				return $article->price_discounted ?? $article->price_sale;
			case FeedInterface::TYPE_SELLING_PLATTFORM:
				return $article->price_feed ? $article->price_feed : $article->price_discounted ?? $article->price_sale;
			default:
				return 0.00;
		}
	}

	protected function addCDATAValue(SimpleXMLElement $item, string $name, string $value): SimpleXMLElement
	{
		$child = $item->addChild($name);

		$node = dom_import_simplexml($child);
		$node->appendChild($node->ownerDocument->createCDATASection($value));

		return $child;
	}

	protected function normalizeStringForUrl($string): string
	{
		if (!$string) {
			return "";
		}

		$adjustedString = str_replace(
			array_keys(self::REPLACE_MAP),
			array_values(self::REPLACE_MAP),
			mb_strtolower($string)
		);

		return preg_replace('/[^A-Za-z0-9\-]/', '', $adjustedString);
	}

	protected function beforeGenerateFeed() {}
	protected function afterGenerateFeed() {}
	abstract protected function getFeedLine($entity);
	abstract protected function storeFeedLine($line);
}
