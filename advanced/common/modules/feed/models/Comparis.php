<?php
namespace common\modules\feed\models;

use common\models\Article;
use Yii;

class Comparis extends Google
{
	/** @var resource */
	protected $csv;

	const COLUMNS = [
		'category',
		'description',
		'imageURL',
		'name',
		'ean',
		'price',
		'product id',
		'productURL',
		'shippingCost',
		'availability',
		'brand',
		'condition',
	];

	protected function beforeGenerateFeed()
	{
		$this->csv = fopen('php://output', 'w');
		ob_start();
		$this->addRow(self::COLUMNS);
	}

	protected function afterGenerateFeed()
	{
		$this->output = ob_get_clean();
		fclose($this->csv);
	}

	protected function getFeedLine($entity)
	{
		/** @var Article $entity */
		return [
			'category' => $entity->productCategory->getLabel(),
			'description' => $entity->getFeedDescription(),
			'imageURL' => (string) $entity->images[0] ?? '',
			'name' => $entity->product_name,
			'ean' => $entity->manufacturer_article_id,
			'price' => $entity->price_sale,
			'product id' => $entity->mat_no,
			'productURL' => $this->getProductUrl($entity, 'comparis', 'pc'),
			'shippingCost' => "Free",
			'availability' => Yii::t('view', $entity->stock->status),
			'brand' => $entity->manufacturerTitle,
			'condition' => "New",
		];
	}

	private function addRow(array $items)
	{
		return fputcsv($this->csv, $items);
	}

	protected function storeFeedLine($line)
	{
		$this->addRow((array)$line);
	}
}
