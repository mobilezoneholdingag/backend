<?php

namespace common\modules\feed\models;

use common\models\Article;
use common\models\ArticleDetails;
use common\models\ArticleDetailsValues;
use common\models\ProductCategory;
use SimpleXMLElement;
use Yii;
use yii\helpers\Inflector;
use yii\helpers\Url;

class Epoq extends Siroop
{
    protected $xml;
    protected $shopUrl = 'https://www.google.com/shopping';
    private $googleCategoryMapping = [
        1 => '267',
        2 => '2353',
        3 => '1745',
        4 => '7167',
        5 => '7167',
        6 => '505771',
        7 => '5572',
        8 => '2353',
        9 => '500035',
        10 => '1718',
        13 => '4745',
        15 => '7349',
        16 => '270',
        17 => '270',
        19 => '272',
        20 => '500106',
        21 => '272',
        28 => '500035',
        30 => '276',
        31 => '265',
        36 => '233',
        42 => '328',
        44 => '343',
        48 => '7349',
        49 => '2353',
        50 => '505771',
        51 => '5572',
        52 => '5566',
        53 => '3213',
        54 => '339',
        55 => '2353',
        56 => '404',
        58 => '3387',
        59 => '3712',
        60 => '249',
        61 => '5468',
        62 => '5308',
        63 => '264',
        64 => '7347',
        69 => '201',
        74 => '5123',
    ];

    protected function beforeGenerateFeed()
    {
        $this->hostUrl = Yii::$app->urlManagerFrontend->hostInfo;
        $xml = new SimpleXMLElement('<rss/>');
        $xml->addAttribute('version', '2.0');

        $xml->addAttribute('xmlns:xmlns:g', 'http://base.google.com/ns/1.0');
        $xml->addAttribute('xmlns:xmlns:s', 'http://www.google.ch/');

        $xml->addAttribute('xmlns:xmlns:e', 'http://base.google.com/cns/1.0');
        $xml->addAttribute('xmlns:xmlns:c', 'http://base.google.com/cns/1.0');

        $xml->addChild('channel');
        $xml->channel->addChild('title', 'Products for Google Shopping');
        $xml->channel->addChild('link', $this->shopUrl);
        $xml->channel->addChild(
            'description',
            'This is a mobilezone product feed for Google Shopping'
        );
        $this->xml = $xml;
    }

    protected function getFeedLine($entity, $lang = "de-CH" )
    {
        /** @var Article $entity */
        $item = new SimpleXMLElement('<item/>');
        $item->addChild('xmlns:g:id', $entity->id);

        Yii::$app->language = $lang;
        $this->addCDATAValue($item, 'xmlns:e:locakey', $lang );
        if(!empty($entity->articleGroup->short_name)){
            $this->addCDATAValue($item, 'xmlns:title', $entity->articleGroup->short_name);
        }else {
            $this->addCDATAValue($item, 'xmlns:title', Yii::t('database', 'common\models\Article_|_' . $entity->id . '_|_product_name'));
        }

        $this->addLongDescriptionToLine($entity, $item);

        $url = $this->getProductUrl($entity);

        if($lang == "fr-FR"){
            $url = str_replace("zubehoer","accessoires", $url);
            $url = str_replace("mobiltelefone","mobiles", $url);
            $url = str_replace("www","fr", $url);
        }
        if($lang == "it-IT"){
            $url = str_replace("zubehoer","accessori", $url);
            $url = str_replace("mobiltelefone","telefonicellulari", $url);
            $url = str_replace("www","it", $url);
        }
        $item->addChild('xmlns:link', htmlspecialchars($url));


        //If Image are Missing Entity not shown in Feed
        $this->addImagesToLine($entity, $item);

        $this->getAvailability($entity, $item);
        $item->addChild('xmlns:g:price', 'CHF ' . $this->getArticlePrice($entity));
        $item->addChild('xmlns:g:sale_price', 'CHF ' . $entity->price_sale);
        if($entity->price_strike != 0.00) {
            $item->addChild('xmlns:c:old_price', $entity->price_strike);
        }
        $item->addChild('xmlns:g:item_group_id', $entity->group_id);
        $this->addCDATAValue($item, 'xmlns:g:brand', $entity->getManufacturerTitle());
        $item->addChild('xmlns:g:quantity', $entity->getStockQuantity());
        $item->addChild('xmlns:g:gtin', $entity->manufacturer_article_id);

        $this->addAccessories($entity, $item);

        $this->addMpnToItem($entity, $item);
        $item->addChild('xmlns:g:condition', 'new');
        $item->addChild('xmlns:g:adult', 'no');

        $item->addChild('xmlns:g:color', $entity->color_label);
        $item->addChild('xmlns:c:frontend_color', $entity->color);

        $this->getAdditionalAttributes($entity, $item);

        $this->getShippingTag($item);
        $item->addChild('xmlns:g:google_product_category', $this->getGoogleCategoryId($entity));
        $item->addChild('xmlns:c:produktart', $this->normalizeStringForUrl($entity->getProductCategoryLabel()));

        return $item;
    }

    protected function getAdditionalAttributes(Article $entity, $item){

        $details = ArticleDetails::find()->all();
        $value = ArticleDetailsValues::findOne(["article_id"=>$entity->id]);
        foreach($details as $detail) {
            try {
                $detailname = preg_replace('/\d/', '', Inflector::slug($detail->detail_name, ''));
                if(empty($detailname)) {
                    //throw new \Exception('empty detail name');
                }
                if(!empty($value->{'detail_' . $detail->detail_id})) {
                    $item->addChild('xmlns:c:' . $detailname, $value->{'detail_' . $detail->detail_id} . " " . $detail->unit);
                }
            } catch (\Exception $e) {
                //Yii::warning($e,'feed-debug');
            }
        }

    }

    protected function addAccessories(Article $entity, SimpleXMLElement $item)
    {
        $accessories = $entity->getAccessories();
        foreach ($accessories as $accessory) {
            $accessories_array[] = $accessory->id;
        }
        if (isset($accessories_array)) {
            $item->addChild('xmlns:e:accessories', implode(",", $accessories_array));
        }
    }

    protected function getProductUrl(Article $entity, $utmSource = null, $utmMedium = null): string
    {
        if (in_array($entity->product_category_id, ProductCategory::ACCESSORIES)) {
            return Url::to(
                [
                    "/product/product/{$entity->getProductCategoryFrontendRouteName()}",
                    'id' => $entity->id,
                    'manufacturerTitle' => $this->normalizeStringForUrl($entity->manufacturerTitle),
                    'groupUrlText' => '',
                    'utm_source' => $utmSource,
                    'utm_medium' => $utmMedium,
                ],
                true
            );
        }

        return Url::to(
            [
                "/product/product/{$entity->getProductCategoryFrontendRouteName()}",
                'id' => $entity->id,
                'manufacturerTitle' => $this->normalizeStringForUrl($entity->manufacturerTitle),
                'groupUrlText' => $this->normalizeStringForUrl($entity->articleGroup->title),
                'utm_source' => $utmSource,
                'utm_medium' => $utmMedium,
            ],
            true
        );
    }

    protected function addImagesToLine(Article $entity, SimpleXMLElement $item)
    {
        $images = $entity->images;
        if (!$images || $images->count < 1) {
            throw new \RuntimeException("Article {$entity->id} has no images.");
        }
        $firstImage = (string)$images[0] ?? '';
        $item->addChild('xmlns:g:image_link', $firstImage);

        foreach ($images->images as $image) {
            $url = (string)$image;
            if ($url === $firstImage) {
                continue;
            }
            $item->addChild('xmlns:g:additional_image_link', $url);
        }
    }

    protected function addLongDescriptionToLine(Article $entity, SimpleXMLElement $item)
    {
        $description = "";

        $descStr1 = 'common\models\ArticleGroups_|_'.$entity->group_id.'_|_description_1';
        $descStr2 = 'common\models\ArticleGroups_|_'.$entity->group_id.'_|_description_2';
        $descStr3 = 'common\models\ArticleGroups_|_'.$entity->group_id.'_|_description_3';

        if(Yii::t('database', $descStr1) != $descStr1){
            $description .= Yii::t('database', $descStr1);
        }
        if(Yii::t('database', $descStr2) != $descStr2){
            $description .= Yii::t('database', $descStr2);
        }
        if(Yii::t('database', $descStr3) != $descStr3){
            $description .= Yii::t('database', $descStr3);
        }

        $descStr1 = 'common\models\Article_|_'.$entity->id.'_|_description_1';
        $descStr2 = 'common\models\Article_|_'.$entity->id.'_|_description_2';
        $descStr3 = 'common\models\Article_|_'.$entity->id.'_|_description_3';

        if(Yii::t('database', $descStr1) != $descStr1){
            $description .= Yii::t('database', $descStr1);
        }
        if(Yii::t('database', $descStr2) != $descStr2){
            $description .= Yii::t('database', $descStr2);
        }
        if(Yii::t('database', $descStr3) != $descStr3){
            $description .= Yii::t('database', $descStr3);
        }


        $this->addCDATAValue($item, 'xmlns:g:description', strip_tags($description));
    }

    protected function addMpnToItem(Article $entity, SimpleXMLElement $item)
    {
        if (empty($entity->mpn) === false) {
            $item->addChild('xmlns:g:mpn', $entity->mpn);
        }
    }

    public function getAvailability(Article $entity, SimpleXMLElement $item)
    {
        $status = $entity->stock->getStatus();
        $status = str_replace("stock_status_","", $status);
        $status = str_replace("_"," ", $status);
        if($status == "in preorder"){
            $status = str_replace("in ","", $status);
        }
        $item->addChild('xmlns:g:availability', $status);
    }

    private function getGoogleCategoryId(Article $entity)
    {
        return $this->googleCategoryMapping[$entity->product_category_id] ?? null;
    }

    private function getShippingTag(SimpleXMLElement $item)
    {
        $shipping = $item->addChild('xmlns:g:shipping');
        $shipping->addChild('xmlns:g:country', 'CH');
        $shipping->addChild('xmlns:g:service', 'free');
        $shipping->addChild('xmlns:g:price', 'CHF 0.00');
    }
}

