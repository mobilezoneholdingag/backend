<?php

namespace common\modules\feed\models;

use common\models\Article;
use common\models\ProductCategory;
use SimpleXMLElement;
use Yii;
use yii\helpers\Url;

class Google extends Siroop
{
    protected $xml;
    protected $shopUrl = 'https://www.google.com/shopping';
    private $googleCategoryMapping = [
        1 => '267',
        2 => '2353',
        3 => '1745',
        4 => '7167',
        5 => '7167',
        6 => '505771',
        7 => '5572',
        8 => '2353',
        9 => '500035',
        10 => '1718',
        13 => '4745',
        15 => '7349',
        16 => '270',
        17 => '270',
        19 => '272',
        20 => '500106',
        21 => '272',
        28 => '500035',
        30 => '276',
        31 => '265',
        36 => '233',
        42 => '328',
        44 => '343',
        48 => '7349',
        49 => '2353',
        50 => '505771',
        51 => '5572',
        52 => '5566',
        53 => '3213',
        54 => '339',
        55 => '2353',
        56 => '404',
        58 => '3387',
        59 => '3712',
        60 => '249',
        61 => '5468',
        62 => '5308',
        63 => '264',
        64 => '7347',
        69 => '201',
        74 => '5123',
    ];

    protected function beforeGenerateFeed()
    {
        $this->hostUrl = Yii::$app->urlManagerFrontend->hostInfo;
        $xml = new SimpleXMLElement('<rss/>');
        $xml->addAttribute('version', '2.0');

        $xml->addAttribute('xmlns:xmlns:g', 'http://base.google.com/ns/1.0');
        $xml->addAttribute('xmlns:xmlns:s', 'http://www.google.ch/');
        $xml->addChild('channel');
        $xml->channel->addChild('title', 'Products for Google Shopping');
        $xml->channel->addChild('link', $this->shopUrl);
        $xml->channel->addChild(
            'description',
            'This is a mobilezone product feed for Google Shopping'
        );
        $this->xml = $xml;
    }

    protected function getFeedLine($entity)
    {
        /** @var Article $entity */
        $item = new SimpleXMLElement('<item/>');
        $item->addChild('xmlns:g:id', $entity->id);
        $this->addCDATAValue($item, 'xmlns:title', $entity->product_name);

        $this->addLongDescriptionToLine($entity, $item);

        $url = $this->getProductUrl($entity);

        $item->addChild('xmlns:link', htmlspecialchars($url));

        //If Image are Missing Entity not shown in Feed
        $this->addImagesToLine($entity, $item);

        $this->getAvailability($entity, $item);
        $item->addChild('xmlns:g:price', 'CHF ' . $this->getArticlePrice($entity));
        $item->addChild('xmlns:g:sale_price', 'CHF ' . $entity->price_sale);
        $item->addChild('xmlns:g:item_group_id', $entity->group_id);
        $this->addCDATAValue($item, 'xmlns:g:brand', $entity->getManufacturerTitle());
        $item->addChild('xmlns:g:gtin', $entity->manufacturer_article_id);
        $this->addMpnToItem($entity, $item);
        $item->addChild('xmlns:g:condition', 'new');
        $item->addChild('xmlns:g:adult', 'no');
        $item->addChild('xmlns:g:color', $entity->color_label);
        $this->getShippingTag($item);
        $item->addChild('xmlns:g:google_product_category', $this->getGoogleCategoryId($entity));

        return $item;
    }

    protected function getProductUrl(Article $entity, $utmSource = null, $utmMedium = null): string
    {
        if (in_array($entity->product_category_id, ProductCategory::ACCESSORIES)) {
            return Url::to(
                [
                    "/product/product/{$entity->getProductCategoryFrontendRouteName()}",
                    'id' => $entity->id,
                    'manufacturerTitle' => $this->normalizeStringForUrl($entity->manufacturerTitle),
                    'groupUrlText' => '',
                    'utm_source' => $utmSource,
                    'utm_medium' => $utmMedium,
                ],
                true
            );
        }

        return Url::to(
            [
                "/product/product/{$entity->getProductCategoryFrontendRouteName()}",
                'id' => $entity->id,
                'manufacturerTitle' => $this->normalizeStringForUrl($entity->manufacturerTitle),
                'groupUrlText' => $this->normalizeStringForUrl($entity->articleGroup->title),
                'utm_source' => $utmSource,
                'utm_medium' => $utmMedium,
            ],
            true
        );
    }

    protected function addImagesToLine(Article $entity, SimpleXMLElement $item)
    {
        $images = $entity->images;
        if (!$images || $images->count < 1) {
            throw new \RuntimeException("Article {$entity->id} has no images.");
        }
        $firstImage = (string)$images[0] ?? '';
        $item->addChild('xmlns:g:image_link', $firstImage);

        foreach ($images->images as $image) {
            $url = (string)$image;
            if ($url === $firstImage) {
                continue;
            }
            $item->addChild('xmlns:g:additional_image_link', $url);
        }
    }

    protected function addLongDescriptionToLine(Article $entity, SimpleXMLElement $item)
    {
        $this->addCDATAValue($item, 'xmlns:g:description', $entity->getFeedDescription());
    }

    protected function addMpnToItem(Article $entity, SimpleXMLElement $item)
    {
        if (empty($entity->mpn) === false) {
            $item->addChild('xmlns:g:mpn', $entity->mpn);
        }
    }

    public function getAvailability(Article $entity, SimpleXMLElement $item)
    {
        if ($entity->getStockQuantity() > 0) {
            $item->addChild('xmlns:g:availability', 'in stock');
        } elseif ($entity->getStockQuantity() == 0) {
            $item->addChild('xmlns:g:availability', 'out of stock');
        } elseif (($entity->getStockQuantity() == 0 && $entity->getNextDeliveryDate() != 0) ||
            $entity->override_positive_stock_to_reorder == 1
        ) {
            $item->addChild('xmlns:g:availability', 'preorder');
        }
    }

    private function getGoogleCategoryId(Article $entity)
    {
        return $this->googleCategoryMapping[$entity->product_category_id] ?? null;
    }

    private function getShippingTag(SimpleXMLElement $item)
    {
        $shipping = $item->addChild('xmlns:g:shipping');
        $shipping->addChild('xmlns:g:country', 'CH');
        $shipping->addChild('xmlns:g:service', 'free');
        $shipping->addChild('xmlns:g:price', 'CHF 0.00');
    }
}

