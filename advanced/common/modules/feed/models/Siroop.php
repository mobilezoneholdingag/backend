<?php
namespace common\modules\feed\models;

use common\models\Article;
use common\models\ArticleGroups;
use common\models\ProductCategory;
use common\modules\set\models\Chart;
use SimpleXMLElement;

class Siroop extends FeedAbstract
{
	const TYPE = FeedInterface::TYPE_SELLING_PLATTFORM;

	protected $xml;
	private $siroopCategoryMapping = [
		1 => 's5819',
		2 => '2353',
		3 => '1745',
		4 => 's5823',
		5 => 's5823',
		6 => '505771',
		7 => '5572',
		8 => '2353',
		9 => 's5821',
		10 => '1718',
		13 => '4745',
		15 => 's6318',
		16 => '270',
		17 => 'S12244',
		19 => 'S12244',
		20 => 's5578',
		21 => '272',
		28 => 's5821',
		30 => 's6699',
		31 => '265',
		36 => '233',
		42 => '328',
		44 => '343',
		48 => 'S6318',
		49 => '2353',
		50 => '505771',
		51 => '5572',
		52 => '5566',
		53 => '3213',
		54 => 's6715',
		55 => '2353',
		56 => '404',
		58 => '3387',
		59 => '3712',
		60 => 's5479',
		61 => 's5822',
		62 => '5308',
		63 => 's5828',
		64 => '7347',
		69 => 's5824',
		74 => 's5826',
	];

	protected function beforeGenerateFeed()
	{
		$xml = new SimpleXMLElement('<rss/>');
		$xml->addAttribute('version', '2.0');

		$xml->addAttribute('xmlns:xmlns:g', 'http://base.google.com/ns/1.0');
		$xml->addAttribute('xmlns:xmlns:s', 'https://merchants.siroop.ch/');
		$xml->addChild('channel');
		$xml->channel->addChild('title', 'Products for Siroop Marketplace');
		$xml->channel->addChild('link', $this->shopUrl);
		$xml->channel->addChild(
			'description',
			'This is a mobilezone product feed for siroop.ch'
		);
		$this->xml = $xml;
	}

	protected function afterGenerateFeed()
	{
		if ($this->xml instanceof SimpleXMLElement) {
			$this->output = $this->xml->asXML() ? $this->xml->asXML() : '';
		}
	}

	protected function getFeedLine($entity)
	{
		/** @var Article $entity */
		$item = new SimpleXMLElement('<item/>');
		$item->addChild('xmlns:g:id', $entity->mat_no);
		$item->addChild('xmlns:g:item_group_id', $entity->group_id ? $entity->group_id : '');
		$this->addCDATAValue($item, 'xmlns:g:title', $entity->product_name);

		$this->addLongDescriptionToLine($entity, $item);

		$item->addChild('xmlns:s:siroop_category', $this->getSiroopCategoryId($entity));

		$this->addImagesToLine($entity, $item);

		$item->addChild('xmlns:s:quantity', $entity->getStockQuantity());
		$item->addChild('xmlns:g:price', $this->getArticlePrice($entity));
		$item->addChild('xmlns:s:productvat', 1);
		$item->addChild('xmlns:s:warranty', $this->partner->warranty ?? 6);

		$this->addManufacturerArticleIdToLine($entity, $item);
		$this->addMpnToItem($entity, $item);

		$this->addCDATAValue($item, 'xmlns:s:manufacturer_name', $entity->getManufacturerTitle());
		$this->addCDATAValue($item, 'xmlns:g:brand', $entity->getManufacturerTitle());

		$this->addCompatibleProductElement($item, $entity);

		$this->addDetailValues($item, $entity);

		return $item;
	}

	protected function addLongDescriptionToLine(Article $entity, SimpleXMLElement $item)
	{
		$this->addCDATAValue($item, 'xmlns:s:long_description', $entity->getFeedDescription());
	}

	protected function addImagesToLine(Article $entity, SimpleXMLElement $item)
	{
		$images = $entity->images;
		if (!$images || $images->count < 1) {
			throw new \RuntimeException("Article {$entity->id} has no images.");
		}
		$firstImage = (string) $images[0] ?? '';
		$item->addChild('xmlns:g:image_link', $firstImage);

		foreach ($images->images as $image) {
			$url = (string) $image;
			if ($url === $firstImage) {
				continue;
			}
			$item->addChild('xmlns:g:additional_image_link', $url);
		}
	}

	protected function addMpnToItem(Article $entity, SimpleXMLElement $item)
	{
		if (empty($entity->mpn) === false) {
			$this->addCDATAValue($item, 'xmlns:g:mpn', $entity->mpn);
		}
	}

	protected function addManufacturerArticleIdToLine(Article $entity, SimpleXMLElement $item)
	{
		if ($entity->manufacturer_article_id) {
			$item->addChild('xmlns:g:gtin', $entity->manufacturer_article_id);
		}
	}

	private function addCompatibleProductElement(SimpleXMLElement $item, $entity)
	{
		/** @var Article $entity */
		if (in_array($entity->product_category_id, ProductCategory::ACCESSORIES)) {
			$setIds = Chart::find()->select(['set_id'])->where(['product_id' => $entity->id])->column();
			$compatibleArticleNames = ArticleGroups::find()
				->select(['short_name'])
				->where(['in', 'accessory_set_id', $setIds])
				->column()
			;
			$this->addCDATAValue($item, 'xmlns:s:attribute', implode(',', $compatibleArticleNames))->addAttribute('name', 'Kompatibel zu');
		}
	}

	protected function addDetailValues(SimpleXMLElement $item, Article $entity)
	{
		if (!$entity->articleDetailsValues) {
			return;
		}

		foreach ($entity->getArticleDetails() as $articleDetail) {
			foreach ($articleDetail['items'] as $articleDetailItem) {
				if (!isset($articleDetailItem['detail_id'])) {
					continue;
				}

				$value = $articleDetailItem['value'];
				$unit = $articleDetailItem['unit'];
				$attribute = $articleDetailItem['title'];
				if ($unit && $value != 'k.A') {
					$value .= " {$unit}";
				}

				if (in_array($articleDetailItem['detail_id'], Article::BOOLEAN_ARTICLE_DETAILS)) {
					$value = $this->getBooleanValue($value);
				}

				$this->addAttributeItem($item, $value, $attribute);
			}
		}
	}

	private function getBooleanValue($value)
	{
		return $value ? 'Ja' : null;
	}

	private function addAttributeItem(SimpleXMLElement $item, $attributeValue, $attributeName)
	{
		if (!is_null($attributeValue) && !empty($attributeValue)) {
			$this->addCDATAValue($item, 'xmlns:s:attribute', $attributeValue)->addAttribute('name', $attributeName);
		}
	}

	private function addXmlElementToAnother(SimpleXMLElement $to, SimpleXMLElement $from)
	{
		$toDom = dom_import_simplexml($to);
		$fromDom = dom_import_simplexml($from);
		$toDom->appendChild($toDom->ownerDocument->importNode($fromDom, true));
	}

	protected function storeFeedLine($line)
	{
		$this->addXmlElementToAnother($this->xml->channel, $line);
	}

	private function getSiroopCategoryId(Article $entity)
	{
		return $this->siroopCategoryMapping[$entity->product_category_id] ?? null;
	}

	public function getFilename()
	{
		return $this->partner->filename;
	}
}
