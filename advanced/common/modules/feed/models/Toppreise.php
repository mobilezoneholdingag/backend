<?php

namespace common\modules\feed\models;

use common\models\Article;
use common\models\ProductCategory;
use SimpleXMLElement;
use Yii;
use yii\helpers\Url;

class Toppreise extends Siroop
{
	const TYPE = FeedInterface::TYPE_BACKLINKING;
	protected $xml;

	protected function beforeGenerateFeed()
	{
		$this->hostUrl = Yii::$app->urlManagerFrontend->hostInfo;
		$xml = new SimpleXMLElement('<rss/>');
		$xml->addAttribute('version', '2.0');

		$xml->addAttribute('xmlns:xmlns:g', 'http://base.google.com/ns/1.0');
		$xml->addAttribute('xmlns:xmlns:s', 'http://www.toppreise.ch/');
		$xml->addChild('channel');
		$xml->channel->addChild('title', 'Products for Toppreise.ch');
		$xml->channel->addChild('link', $this->shopUrl);
		$xml->channel->addChild(
			'description',
			'This is a mobilezone product feed for Toppreise.ch'
		);
		$this->xml = $xml;
	}

	protected function getFeedLine($entity): SimpleXMLElement
	{
		/** @var Article $entity */
		$item = new SimpleXMLElement('<item/>');
		$item->addChild('xmlns:g:id', $entity->id);
		$item->addChild('xmlns:g:item_group_id', $entity->group_id);
		$this->addCDATAValue($item, 'xmlns:g:title', $entity->product_name);

		$this->addLongDescriptionToLine($entity, $item);

		//If Image are Missing Entity not shown in Feed
		$this->addImagesToLine($entity, $item);

		$item->addChild('xmlns:s:quantity', $entity->getStockQuantity());
		$item->addChild('xmlns:g:price', $this->getArticlePrice($entity));
		$item->addChild('xmlns:s:productvat', 1);
		$item->addChild('xmlns:s:warranty', $this->partner->warranty ?? 6);

		$this->addManufacturerArticleIdToLine($entity, $item);
		$this->addMpnToItem($entity, $item);

		$this->addCDATAValue($item, 'xmlns:s:manufacturer_name', $entity->getManufacturerTitle());
		$this->addCDATAValue($item, 'xmlns:g:brand', $entity->getManufacturerTitle());

		$this->addDetailValues($item, $entity);

		if (in_array($entity->product_category_id, ProductCategory::ACCESSORIES)) {
			$url = Url::to(
				[
					"/product/product/{$entity->getProductCategoryFrontendRouteName()}",
					'id' => $entity->id,
					'manufacturerTitle' => strtolower($entity->manufacturerTitle),
					'utm_source' => 'pc',
					'utm_medium' => 'toppreise',
				],
				true
			);
		} else {
			$url = Url::to(
				[
					"/product/product/{$entity->getProductCategoryFrontendRouteName()}",
					'id' => $entity->id,
					'manufacturerTitle' => strtolower($entity->manufacturerTitle),
					'groupUrlText' => str_replace(" ", "-", strtolower(
						$entity->articleGroup
							? $entity->articleGroup->title
							: null
					)),
					'utm_source' => 'pc',
					'utm_medium' => 'toppreise',
				],
				true
			);
		}

        $this->addCDATAValue($item, 'xmlns:g:url', $url);
		//$this->addCDATAValue($item, 'xmlns:g:url', htmlspecialchars($url));

		return $item;
	}
}
