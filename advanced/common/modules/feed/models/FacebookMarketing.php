<?php
namespace common\modules\feed\models;

use common\models\Article;
use Yii;

class FacebookMarketing extends Google
{
    /** @var resource */
    protected $csv;
    /*
    const COLUMNS = [
        'category',
        'description',
        'imageURL',
        'name',
        'ean',
        'price',
        'product id',
        'productURL',
        'shippingCost',
        'availability',
        'brand',
        'condition',
    ];
    */

    private $googleCategoryMapping = [
        1 => '267',
        2 => '2353',
        3 => '1745',
        4 => '7167',
        5 => '7167',
        6 => '505771',
        7 => '5572',
        8 => '2353',
        9 => '500035',
        10 => '1718',
        13 => '4745',
        15 => '7349',
        16 => '270',
        17 => '270',
        19 => '272',
        20 => '500106',
        21 => '272',
        28 => '500035',
        30 => '276',
        31 => '265',
        36 => '233',
        42 => '328',
        44 => '343',
        48 => '7349',
        49 => '2353',
        50 => '505771',
        51 => '5572',
        52 => '5566',
        53 => '3213',
        54 => '339',
        55 => '2353',
        56 => '404',
        58 => '3387',
        59 => '3712',
        60 => '249',
        61 => '5468',
        62 => '5308',
        63 => '264',
        64 => '7347',
        69 => '201',
        74 => '5123',
    ];

    protected $stockmap = [
	    'stock_status_available' => 'in stock',
        'stock_status_in_reorder' => 'available for order',
        'stock_status_not_available' => 'out of stock',
        'stock_status_in_preorder' => 'preorder'
    ];

    const COLUMNS = [
        'id', // Unique ID for item. Can be a variant for a product. If there are multiple instances of the same ID, we ignore all instances.
        'availability', // If item in stock. Accepted values are: in stock - Item ships immediately. out of stock - No plan to restock. preorder- Available in future. available for order - Ships in 1-2 weeks. discontinued - Discontinued
        'condition', // new, refurbished, used,
        'description', // short text describing product
        'image_link',
        'link',
        'title',
        'price', // 10.00 CHF
        'brand', // Brand Name
        'google_product_category',

    ];

    protected function beforeGenerateFeed()
    {
        $this->csv = fopen('php://output', 'w');
        ob_start();
        $this->addRow(self::COLUMNS);
    }

    protected function afterGenerateFeed()
    {
        $this->output = ob_get_clean();
        fclose($this->csv);
    }

    protected function getFeedLine($entity)
    {
        /** @var Article $entity */

        return [
            'id' => $entity->mat_no,
            'availability' => $this->stockmap[$entity->stock->status],
            'condition' => 'new',
            'description' => html_entity_decode(strip_tags($entity->getFeedDescription())),
            'image_link' => (string) $entity->images[0] ?? '',
            'link' => $this->getProductUrl($entity),
            'title' => $entity->product_name,
            'price' => $entity->price_sale . ' CHF',
            'brand' => $entity->manufacturerTitle,
            'google_product_category' => $this->googleCategoryMapping[$entity->product_category_id]
        ];

        /*

        return [
            'category' => $entity->productCategory->getLabel(),
            'description' => $entity->getFeedDescription(),
            'imageURL' => (string) $entity->images[0] ?? '',
            'name' => $entity->product_name,
            'ean' => $entity->manufacturer_article_id,
            'price' => $entity->price_sale,
            'product id' => $entity->mat_no,
            'productURL' => $this->getProductUrl($entity),
            'shippingCost' => "Free",
            'availability' => Yii::t('view', $entity->stock->status),
            'brand' => $entity->manufacturerTitle,
            'condition' => "New",
        ];
        */
    }

    private function addRow(array $items)
    {
        return fputcsv($this->csv, $items);
    }

    protected function storeFeedLine($line)
    {
        $this->addRow((array)$line);
    }
}
