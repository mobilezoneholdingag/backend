<?php
namespace common\modules\feed\controllers\console;

use backend\models\MarketingPartner;
use common\modules\feed\services\Export;
use yii\console\Controller;

class FeedController extends Controller
{
	public function actionGenerateAllFeeds()
	{
		$this->getFeedExportService()->runAllFeeds();
	}

	public function actionGenerateFeedByMarketingPartnerId($id)
	{
		$partner = MarketingPartner::findOne($id);
		$this->getFeedExportService()->runFeed($partner);
	}

	public function actionGenerateFeedByMarketingPartnerTitle($title)
	{
		$partner = MarketingPartner::findOne(['title' => $title]);
		$this->getFeedExportService()->runFeed($partner);
	}

	private function getFeedExportService() : Export
	{
		return di('feed.export.service');
	}
}
