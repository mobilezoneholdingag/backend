<?php

use common\modules\feed\models\Comparis;
use common\modules\feed\models\Google;
use common\modules\feed\models\Siroop;
use common\modules\feed\models\Toppreise;
use common\modules\feed\models\FacebookMarketing;
use common\modules\feed\models\Epoq;

return [
	'params' => [
		'logicTypes' => [
			'Siroop' => [
				'class' => Siroop::class,
			],
			'Toppreise' => [
				'class' => Toppreise::class,
			],
			'Google' => [
				'class' => Google::class,
			],
			'Comparis' => [
				'class' => Comparis::class,
			],
            'FacebookMarketing' => [
                'class' => FacebookMarketing::class
            ],
            'Epoq' => [
                'class' => Epoq::class
            ],
		],
		'feed_base_url' => 'https://4192fe90ab0bf92daa09-036c61078f26b442203505df2aa3205d.ssl.cf3.rackcdn.com',
	]
];
