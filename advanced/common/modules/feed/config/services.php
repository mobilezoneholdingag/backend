<?php

use common\modules\feed\services\Export;
use components\filesystem\FilesystemProviderInterface;
use deinhandy\yii2container\Container;

$container = Yii::$container;

$container->set('feed.export.service', function(Container $container) {
	$fsProvider = di(FilesystemProviderInterface::class);

	return new Export($fsProvider, 'feeds');
});
