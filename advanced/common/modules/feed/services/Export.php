<?php
namespace common\modules\feed\services;

use backend\models\MarketingPartner;
use common\modules\feed\models\FeedAbstract;
use common\modules\feed\models\FeedInterface;
use components\filesystem\FilesystemProviderInterface;
use DateTime;
use Exception;
use League\Flysystem\FileNotFoundException;
use RuntimeException;
use Yii;
use Yii\base\ErrorException;

class Export
{
	protected $filesystemProvider;
	protected $filesystemConfig;

	public function __construct(FilesystemProviderInterface $filesystemProvider, $filesystemConfig)
	{
		$this->filesystemProvider = $filesystemProvider;
		$this->filesystemConfig = $filesystemConfig;
	}

	public function runFeed(MarketingPartner $marketingPartner)
	{
		try {
			$logicType = FeedAbstract::getLogicTypeLabels()[$marketingPartner->logic];
			$feedClass = FeedAbstract::getLogicTypes()[$logicType]['class'] ?? null;
			if (class_exists($feedClass)) {
				/** @var FeedInterface $feed */
				$feed = new $feedClass($marketingPartner);
				if ($feed instanceof FeedInterface) {
					$feed->generateFeed();
					if ($this->storeFeed($feed, $marketingPartner)) {
						$this->afterFeedStored($feed, $marketingPartner);
					} else {
						throw new RuntimeException('Feed was not saved!');
					}
				}
			}
		} catch (Exception $e) {
			Yii::error($e, 'feed-debug');
		}

	}

	public function runAllFeeds()
	{
		/** @var MarketingPartner[] $partners */
		$partners = MarketingPartner::find()->all();
		foreach ($partners as $partner) {
			$this->runFeed($partner);
		}
	}

	protected function afterFeedStored(FeedInterface $feed, MarketingPartner $marketingPartner)
	{
		$now = new DateTime();
		$marketingPartner->last_file_generated_at = $now->format('Y-m-d H:i:s');
		$marketingPartner->save();
	}

	protected function storeFeed(FeedInterface $feed, MarketingPartner $marketingPartner)
	{
		try {
			$this->filesystemProvider->fs($this->filesystemConfig)->delete($marketingPartner->filename);
		} catch (FileNotFoundException $e) {
			Yii::error($e,'feed-debug');
		} catch (ErrorException $e) {
			Yii::error($e,'feed-debug');
		}
		return $this->filesystemProvider->fs($this->filesystemConfig)->put($marketingPartner->filename, $feed->getOutput());
	}
}
