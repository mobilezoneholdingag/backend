<?php

namespace common\modules\set\models;

use yii\db\ActiveRecord;
use Yii;

class AccessoryCategorySet extends ActiveRecord
{
	public static function tableName()
	{
		return 'accessory_category_set';
	}

	public function rules()
	{
		return [
			[['set_id', 'accessory_category_id'], 'required'],
			[['set_id', 'accessory_category_id'], 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'set_id' => Yii::t('app', 'Set ID'),
			'accessory_category_id' => Yii::t('app', 'Accessory Category'),
		];
	}
}
