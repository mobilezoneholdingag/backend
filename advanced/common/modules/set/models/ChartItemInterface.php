<?php

namespace common\modules\set\models;

interface ChartItemInterface
{
	public function getId() : int;

	/**
	 * @return string Intentionally not set as type hint as null is returned in some valid cases.
	 */
	public function getUniqueTitle();

	public function hasImage() : bool;

	public function getSmallImageUrl() : string;

	/**
	 * Whether the entity does have an group (like article does have the article group) or not.
	 *
	 * @return bool
	 */
	public function hasGroup() : bool;

	/**
	 * @return array Intentionally not set as type hint, as this is a core-yii2-implementation.
	 */
	public function toArray();
}