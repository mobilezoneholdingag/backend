<?php

namespace common\modules\set\models;

use common\models\Article;
use common\models\Tariffs;
use RuntimeException;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "charts".
 *
 * @property integer $set_id
 * @property integer $position
 * @property integer $product_id
 *
 * @property Article $article
 * @property Sets $set
 */
class Chart extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'charts';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [
			LoggableBehavior::className(),
		] : [];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['set_id', 'position', 'product_id'], 'required'],
			[['set_id', 'position', 'product_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'set_id' => Yii::t('app', 'Set ID'),
			'position' => Yii::t('app', 'Position'),
			'product_id' => Yii::t('app', 'Product ID'),
			'productName' => Yii::t('app', 'Product Name'),
		];
	}

	/**
	 * @return string
	 */
	public function getProductName()
	{
		return $this->getProductModel()->getUniqueTitle();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSet()
	{
		return $this->hasOne(Sets::className(), ['id' => 'set_id']);
	}

	/**
	 * @param array $parameters
	 * @return ChartItemInterface
	 */
	public function getProductModel(array $parameters = [])
	{
		$parameters = array_merge($parameters, ['id' => $this->product_id]);

		/** @var Article|Tariffs|string $class */
		$class = '\\common\\models\\' . $this->set->typeTitle ?? '';
		if (class_exists($class) === false) {
			throw new RuntimeException('Set type class "' . ($this->set->typeTitle ?? '') . '" does not exist.');
		}

		/** @var ChartItemInterface $model */
		$model = $class::findOne($parameters);
		return $model;
	}
}
