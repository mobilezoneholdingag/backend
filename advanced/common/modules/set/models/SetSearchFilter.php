<?php
namespace common\modules\set\models;

use common\models\Article;
use common\models\ArticleDetails;
use common\models\ArticleDetailsValues;
use common\models\ArticleGroups;
use common\models\OperatingSystems;
use common\models\Stock;
use common\modules\platform\models\Platform;
use InvalidArgumentException;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class SetSearchFilter
{
	/** @var Query */
	private $query;

	public function __construct(ActiveDataProvider $dataProvider)
	{
		$this->query = $dataProvider->query;
		$this->joinChartsWithArticles();
		$this->joinWithSets();
	}

	public function applySetFilter(array $setIds)
	{
		$this->query->andWhere(['in', 'system_title', $setIds]);
	}

	public function joinWithSets()
	{
		if ($this->isTableJoined(Sets::tableName()) === false) {
			$this->query->innerJoin(Sets::tableName(), 'set_id = ' . Sets::tableName() . '.id');
		}
	}

	public function applyManufacturerFilter(array $manufacturerIds = [])
	{
		if (empty($manufacturerIds) === false) {
			$this->query->andWhere(['in', Article::tableName() . '.manufacturer_id', $manufacturerIds]);
		}
	}

	public function applyPriceFilter($fromPrice = null, $toPrice = null)
	{
		if ($fromPrice) {
			$this->query->andWhere(['>=', Article::tableName() . '.price_sale', $fromPrice]);
		}

		if ($toPrice) {
			$this->query->andWhere(['<=', Article::tableName() . '.price_sale', $toPrice]);
		}
	}

	public function applyColorFilter(array $color)
	{
		if (empty($color) === false) {
			$this->query->andWhere(['in', Article::tableName() . '.color', $color]);
		}
	}

	public function applyMemoryFilter(array $memory)
	{
		if (empty($memory) === false) {
			$this->query->andWhere(['in', Article::tableName() . '.memory', $memory]);
		}
	}

	public function applyCompatibleWithFilter(array $articleGroupIds)
	{
		if (empty($articleGroupIds) === false) {
			$compatibleProductIds = [];
			/** @var ArticleGroups $articleGroup */
			$articleGroup = ArticleGroups::find()->where(['in', ArticleGroups::tableName() . '.id', $articleGroupIds])->one();
			if (isset($articleGroup->accessorySet->productIds)) {
				$compatibleProductIds = $articleGroup->accessorySet->productIds;
			}
			$this->query->andWhere(['in', 'product_id', $compatibleProductIds]);
		}
	}

	public function getQuery() : Query
	{
		return clone $this->query;
	}

	public function joinChartsWithArticles()
	{
		if ($this->isTableJoined(Article::tableName()) === false) {
			$this->query->leftJoin(Chart::tableName(), 'product_id = ' . Article::tableName() . '.id');
		}
	}

	public function joinChartsWithArticleDetails()
	{
		if ($this->isTableJoined(ArticleDetailsValues::tableName()) === false) {
			$this->query->leftJoin(ArticleDetailsValues::tableName(), 'article_id = ' . Article::tableName() . '.id');
		}
	}

	public function joinChartsWithArticleGroups()
	{
		if ($this->isTableJoined(ArticleGroups::tableName()) === false) {
			$this->query->leftJoin(ArticleGroups::tableName(), ArticleGroups::tableName() . '.id = ' . Article::tableName() . '.group_id');
		}
	}

	public function joinChartsWithOperatingSystems()
	{
		if ($this->isTableJoined(OperatingSystems::tableName()) === false) {
			$this->joinChartsWithArticleGroups();
			$this->query->leftJoin(OperatingSystems::tableName(), OperatingSystems::tableName() . '.id = ' . ArticleGroups::tableName() . '.os_id');
		}
	}

	public function joinChartsWithPlatforms()
	{
		if ($this->isTableJoined(Platform::tableName()) === false) {
			$this->joinChartsWithOperatingSystems();
			$this->query->leftJoin(Platform::tableName(), Platform::tableName() . '.id = platform_id');
		}
	}

	public function applyDisplaySizeFilter(array $displaySizes)
	{
		if (empty($displaySizes) === false) {
			$this->joinChartsWithArticleDetails();
			$this->query->andWhere(['in', 'detail_' . ArticleDetails::DETAIL_DISPLAY_SIZE_INCH, $displaySizes]);
		}
	}

	public function applyOperatingSystemFilter(array $operatingSystems)
	{
		if (empty($operatingSystems) === false) {
			$this->joinChartsWithPlatforms();
			$this->query->andWhere(['in', Platform::tableName() . '.name', $operatingSystems]);
		}
	}

	public function applyOrderBy(string $sortByField)
	{
		if (in_array($sortByField, ['position', 'price_sale', 'stock_quantity']) === false) {
			throw new InvalidArgumentException('Sorting by "' . $sortByField . '" is not allowed.');
		}
		if (empty($sortByField) === false) {
			if ($sortByField == 'stock_quantity') {
				$this->joinChartsWithArticleStock();
				$this->orderByStockQuantity();
			} elseif ($sortByField == 'price_sale') {
				$this->query->orderBy(Article::tableName(). '.' .$sortByField);
			} else {
				$this->query->orderBy($sortByField);
			}
		}
	}

	protected function orderByStockQuantity()
	{
		$this->query->orderBy(['quantity' => SORT_DESC]);
	}

	public function joinChartsWithArticleStock()
	{
		if ($this->isTableJoined(Stock::tableName()) === false) {
			$this->query->innerJoin(
				Stock::tableName(),
				Stock::tableName() . '.id = ' . Article::tableName() . '.id'
			);
		}
	}

	public function isTableJoined(string $tableName)
	{
		return in_array($tableName, array_column($this->query->join ?? [], 1));
	}
}
