<?php

namespace common\modules\set\models;

use yii\data\ActiveDataProvider;

class AccessoryCategorySearch extends AccessoryCategory
{
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['title'], 'safe'],
		];
	}

	public function search($params)
	{
		$query = AccessoryCategory::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSizeLimit' => [1, 200]
			]
		]);

		$dataProvider->setSort([
			'attributes' => [
				'id',
				'title',
			]
		]);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
		]);

		$query->andFilterWhere(['like', 'title', $this->title]);

		return $dataProvider;
	}
}
