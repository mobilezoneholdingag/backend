<?php

namespace common\modules\set\models;

use common\models\Article;
use common\models\Tariffs;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "sets".
 *
 * @property integer $id
 * @property string $title
 * @property string $system_title
 * @property string $description
 * @property string $typeTitle
 * @property int $type
 *
 * @property Article[]|Tariffs[] $productList
 * @property array productIds
 * @property Chart[] $chart
 *
 */
class Sets extends ActiveRecord
{
	const TOP_ARTICLE_SMARTPHONES = 'top_article_smartphones';
	const TOP_ARTICLE_TABLETS = 'top_article_tablets';
	const TOP_TARIFFS = 'top_tariffs';
	const ALL_SMARTPHONES = 'all_smartphones';
	const ALL_TABLETS = 'all_tablets';
	const ALL_TARIFFS = 'all_tariffs';

	const TYPE_ARTICLE = 1;
	const TYPE_TARIFF = 2;

	const TYPE_ID_MAPPING = [
		self::TYPE_ARTICLE => 'Article',
		self::TYPE_TARIFF => 'Tariffs',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'sets';
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			$this->system_title = ($this->system_title == '') ? uniqid('system_title_') : $this->system_title;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [LoggableBehavior::className()] : [];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'required'],
			[['title', 'system_title', 'description'], 'string'],
			[['type'], 'integer'],
			[['title', 'system_title'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'system_title' => Yii::t('app', 'System Title'),
			'description' => Yii::t('app', 'Description'),
			'type' => Yii::t('app', 'Set Type'),
		];
	}

	public function getChart()
	{
		return $this->hasMany(Chart::className(), ['set_id' => 'id']);
	}

	public function getTypeTitle()
	{
		return self::TYPE_ID_MAPPING[$this->type] ?? '';
	}

	/**
	 * @param array $parameters
	 * @return array
	 */
	public function getProductList(array $parameters = [])
	{
		$productList = [];
		/** @var Chart $chartEntry */
		foreach ($this->chart as $chartEntry) {
			if (($model = $chartEntry->getProductModel($parameters)) != false)
				$productList[$chartEntry->position] = $model;
		}
		ksort($productList);
		return $productList;
	}

	/**
	 * @return array
	 */
	public function getProductIds()
	{
		$productIds = [];
		foreach ($this->chart as $chartEntry) {
			$productIds[] = $chartEntry->product_id;
		}
		return $productIds;
	}

	/**
	 * @param $system_title
	 * @param array $parameters
	 * @return Article[]|Tariffs[]|null
	 */
	public static function getItemsFor($system_title, array $parameters = [])
	{
		$products = null;
		if (($set = self::findOne(['system_title' => $system_title])) != false) {
			$products = $set->getProductList($parameters);
		}

		return $products;
	}
}
