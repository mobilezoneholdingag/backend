<?php

namespace common\modules\set\models;

use sammaye\audittrail\LoggableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property integer $id
 * @property string $title
 *
 * @property Sets[] $sets
 */
class AccessoryCategory extends ActiveRecord
{
	public $sets;

	public static function tableName()
	{
		return 'accessory_category';
	}

	public function rules()
	{
		return [
			[['title'], 'required'],
			[['title'], 'string'],
			[['title'], 'unique'],
			[['sets'], 'safe'],
		];
	}

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('app', 'Title (Key)'),
			'sets' => Yii::t('app', 'Sets'),
			'setTitles' => Yii::t('app', 'Sets'),
		];
	}

	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [LoggableBehavior::className()] : [];
	}

	public function getSets(): ActiveQuery
	{
		return $this->hasMany(Sets::class, ['id' => 'set_id'])
			->viaTable(AccessoryCategorySet::tableName(), ['accessory_category_id' => 'id']);
	}

	public function getSetTitles()
	{
		$titles = [];

		foreach ($this->getSets()->all() as $set) {
			$titles[] = $set->title;
		}

		return implode(', ', $titles);
	}

	public function beforeSave($insert)
	{
		$this->unlinkAll('sets', true);

		foreach ($this->sets ?? [] as $setId) {
			$set = Sets::findOne($setId);

			if ($set) {
				$this->link('sets', $set);
			}
		}

		return parent::beforeSave($insert);
	}
}
