<?php

namespace common\modules\set;

use Yii;
use yii\base\Module;
use yii\web\Application;

class SetModule extends Module
{
	public $controllerNamespace = 'common\modules\set\controllers';

	public function init()
	{
		if (!Yii::$app instanceof Application) {
			return;
		}

		$this->applyUrlRules();

		$this->controllerMap = [
			'set' => 'common\modules\set\controllers\SetController',
			'accessory' => 'common\modules\set\controllers\AccessoryController',
			'accessory-category' => 'common\modules\set\controllers\AccessoryCategoryController',
		];
	}

	private function applyUrlRules()
	{
		Yii::$app->getUrlManager()->addRules([
			'set/set/accessory' => 'set/accessory',
			'set/set/accessory/<id>' => 'set/accessory/view',
			'set/set/search' => 'set/set/search',
			'set/set/<id>' => 'set/set/view',
		]);
	}
}
