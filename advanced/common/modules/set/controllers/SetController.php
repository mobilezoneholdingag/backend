<?php

namespace common\modules\set\controllers;

use common\modules\set\models\Sets;
use common\modules\set\actions\set\SearchAction;
use common\modules\set\actions\set\ViewAction;
use yii\rest\ActiveController;

class SetController extends ActiveController
{
	public $modelClass = Sets::class;

	public function actions()
	{
		$actions = parent::actions();

		unset($actions['delete'], $actions['create'], $actions['update']);

		$actions['view']['class'] = ViewAction::class;

		$actions['search'] = [
			'class' => SearchAction::class,
			'modelClass' => $this->modelClass,
			'checkAccess' => [$this, 'checkAccess'],
		];

		return $actions;
	}
}
