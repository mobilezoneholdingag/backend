<?php

namespace common\modules\set\controllers;

use common\modules\set\actions\accessory\IndexAction;
use common\modules\set\models\Sets;
use common\modules\set\actions\accessory\ViewAction;
use yii\rest\ActiveController;

class AccessoryController extends ActiveController
{
	public $modelClass = Sets::class;

	public function actions()
	{
		$actions = parent::actions();

		unset($actions['delete'], $actions['create'], $actions['update']);

		$actions['index']['class'] = IndexAction::class;

		$actions['view']['class'] = ViewAction::class;

		return $actions;
	}
}
