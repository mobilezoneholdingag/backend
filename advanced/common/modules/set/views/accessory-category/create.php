<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\set\models\AccessoryCategory */

$this->title = 'Create Accessory Category';
$this->params['breadcrumbs'][] = ['label' => 'Accessory Category', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accessory-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
