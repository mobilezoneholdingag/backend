<?php

use common\modules\set\models\Sets;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\modules\set\models\AccessoryCategory */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('/js/accessory-category/set.js');
$sets = ArrayHelper::map(Sets::find()->select(['id', 'title'])->all(), 'id', 'title');
$index = 0;
$model->sets = $model->getSets()->select('id')->column();
?>

<br class="accessory-category-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput() ?>

	<?php if(!$model->isNewRecord): ?>
		<p>
			<label class="control-label" for="accessorycategory-sets-0">Sets</label>
		</p>

		<?php
			foreach ($model->sets as $index => $set) {
				echo $form->field($model, "sets[$index]")
					->dropDownList($sets, ['class' => 'form-control set', 'data-index' => $index, 'prompt' => ' -- Select Set --'])
					->label(false);
			}

			$index++;
		?>

		<?= $form->field($model, "sets[$index]")
			->dropDownList($sets, ['class' => 'form-control set', 'data-index' => $index, 'prompt' => ' -- Select Set --'])
			->label(false)
		?>

	    <p>
		    <a href="#" class="btn btn-success add-set">+ Set hinzufügen</a>
	    </p>
	<?php endif; ?>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? 'Create' : 'Update',
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>
