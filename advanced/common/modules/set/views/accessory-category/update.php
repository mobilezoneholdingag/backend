<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Update Accessory Category: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Accessory Category', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="accessory-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model')) ?>

</div>
