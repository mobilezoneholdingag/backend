<?php

use common\modules\set\models\AccessoryCategory;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\set\models\AccessoryCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Accessory Category';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="renewal-request-index">

	<?php if (Yii::$app->user->can('starlord')) { ?>
	<p>
		<?= Html::a(Yii::t('app', 'Create Accessory Category', [
			'modelClass' => AccessoryCategory::class,
		]), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php } ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            'setTitles',
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => Yii::$app->user->can('starlord') ? '{view} {update} {delete}' : '{view} {update}',
			],
        ],
    ]); ?>
</div>
