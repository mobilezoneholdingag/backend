<?php

namespace common\modules\set\actions\set;

use common\models\Article;
use common\models\ArticleDetails;
use common\models\ArticleGroups;
use common\models\Stock;
use common\modules\platform\models\Platform;
use common\models\Manufacturer;
use common\modules\set\models\Sets;
use common\modules\set\models\SetSearchFilter;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\rest\IndexAction;

class SearchAction extends IndexAction
{
	/** @var ActiveDataProvider */
	private $dataProvider;
	/** @var SetSearchFilter */
	private $setSearchFilter;

	public function init()
	{
		parent::init();
		$this->modelClass = Article::class;
		$this->dataProvider = $dataProvider = parent::run();
		$this->dataProvider->setPagination(['pageSizeLimit' => [1, 500]]);

		$this->setSearchFilter = new SetSearchFilter($this->dataProvider);
	}

	public function run()
	{
		// only apply filters which should affect the possible filter options ("facets")
		$this->applyMainFilters();
		$facets = $this->gatherFacetInfos();
		$this->applyFilters();

		$articleIds = $this->getActiveArticleIds();
		$this->setIdsAsQueryCondition($articleIds);

		return [[
			'items' => $this->dataProvider->models,
			'facets' => $facets,
			'total' => $this->dataProvider->totalCount,
		]];
	}

	private function getActiveArticleIds(): array
	{
		$articleQuery = $this->setSearchFilter->getQuery();
		$articleIds = [];
		/** @var Article $article */
		foreach ($articleQuery->each(10) as $article) {
		    if($article->is_active){
                if (
                    $article->is_active
                    && $article->is_active_erp
                    && $article->stock instanceof Stock
                    && $article->stock->isDispatchable()
                ) {
                    $articleIds[] = $article->id;
                    continue;
                }

                if (!$article->articleGroup) {
                    continue;
                }

                if (!$article->articleGroup->firstActiveArticle && $article->stock->isPreorderable()) {
                    $articleIds[] = $article->id;
                    continue;
                }

                if (!$article->articleGroup->firstActiveArticle){
                    continue;
                }

                $articleIds[] = $article->articleGroup->firstActiveArticle->id;
            }
		}

		return $articleIds;
	}

	private function setIdsAsQueryCondition(array $articleIds)
	{
		$articleIdColumn = Article::tableName() . '.id';
		$articleIdsString = implode(',', $articleIds) ?: 0;
		$orderBy = "FIELD ({$articleIdColumn},{$articleIdsString})";
		/** @var ActiveQuery $query */
		$query = $this->dataProvider->query;
		$query->join = null;
		$query
			->where(['IN', $articleIdColumn, $articleIds])
			->groupBy($articleIdColumn)
			->orderBy(new Expression($orderBy));
	}

	public function findModel($id)
	{
		return Sets::findOne(['system_title' => $id]);
	}

	private function applyMainFilters()
	{
		$this->setSearchFilter->applySetFilter((array)Yii::$app->request->get('id', []));
	}

	private function applyFilters()
	{
		$request = Yii::$app->request;
		$priceFilter = $request->get('price');
		$manufacturer = $request->get('manufacturer', false) ?: $request->get('manufacturer_id', []);
		$this->setSearchFilter->applyPriceFilter($priceFilter['from'] ?? null, $priceFilter['to'] ?? null);
		$this->setSearchFilter->applyManufacturerFilter((array)$manufacturer);
		$this->setSearchFilter->applyColorFilter((array)$request->get('color', []));
		$this->setSearchFilter->applyMemoryFilter((array)$request->get('memory', []));
		$this->setSearchFilter->applyCompatibleWithFilter($request->get('compatible_with', []));
		$this->setSearchFilter->applyDisplaySizeFilter((array)$request->get('display', []));
		$this->setSearchFilter->applyOperatingSystemFilter((array)$request->get('operating_system', []));
		$this->setSearchFilter->applyOrderBy($request->get('sort_by', 'position'));
	}

	protected function gatherFacetInfos() : array
	{
		if (is_null(Yii::$app->request->get('facets'))) {
			return [];
		}

		return [
			'manufacturer' => $this->gatherManufacturerFacetInfos(),
			'color' => $this->gatherColorFacetInfos(),
			'display' => $this->gatherDisplayFacetInfos(),
			'operating_system' => $this->gatherOperatingSystemFacetInfos(),
			'memory' => $this->gatherMemoryFacetInfos(),
			'price' => $this->gatherPriceFacetInfos(),
			'compatible_with' => $this->gatherCompatibleWithFacetInfos(),
		];
	}

	protected function gatherManufacturerFacetInfos() : array
	{
		$query = $this->setSearchFilter->getQuery();
		$query->select(Article::tableName() . '.manufacturer_id');
		$query->distinct();
		$manufacturerIds = array_values($query->createCommand()->queryColumn());

		$manufacturers = Manufacturer::findAll($manufacturerIds);

		usort(
			$manufacturers,
			function($a, $b) {
				/** @var Manufacturer $a */
				/** @var Manufacturer $b */
				return $a->sort <=> $b->sort;
			}
		);

		return $manufacturers;
	}

	private function gatherColorFacetInfos() : array
	{
		$query = $this->setSearchFilter->getQuery();
		$query->select(Article::tableName() . '.color');
		$query->distinct();
		$query->andWhere(new Expression(Article::tableName() . '.color IS NOT NULL'));
		$query->andWhere(new Expression(Article::tableName() . '.color != ""'));

		return $query->createCommand()->queryColumn();
	}

	private function gatherDisplayFacetInfos() : array
	{
		$this->setSearchFilter->joinChartsWithArticleDetails();
		$query = $this->setSearchFilter->getQuery();
		$query->select('detail_' . ArticleDetails::DETAIL_DISPLAY_SIZE_INCH);
		$query->distinct();

		return $query->createCommand()->queryColumn();
	}

	private function gatherOperatingSystemFacetInfos() : array
	{
		$this->setSearchFilter->joinChartsWithPlatforms();
		$query = $this->setSearchFilter->getQuery();
		$query->select(Platform::tableName() . '.name');
		$query->distinct();

		return array_filter($query->createCommand()->queryColumn());
	}

	private function gatherMemoryFacetInfos() : array
	{
		$query = $this->setSearchFilter->getQuery();
		$query->select(Article::tableName() . '.memory');
		$query->distinct();
		$query->andWhere(new Expression(Article::tableName() . '.memory > 0'));

		return $query->createCommand()->queryColumn();
	}

	protected function gatherPriceFacetInfos() : array
	{
		$query = $this->setSearchFilter->getQuery();
		$query->select(
			new Expression(
				'MIN(' . Article::tableName() . '.price_sale) AS minprice, MAX(' . Article::tableName(
				) . '.price_sale) AS maxprice'
			)
		);
		$query->andWhere(new Expression(Article::tableName() . '.price_sale > 0'));
		$result = $query->createCommand()->queryOne();

		return [
			'min' => $result['minprice'],
			'max' => $result['maxprice'],
		];
	}

	protected function gatherCompatibleWithFacetInfos() : array
	{
		return ArticleGroups::find()
			->select(['id', 'short_name'])
			->orderBy(['short_name' => SORT_ASC])
			->asArray()
			->all();
	}
}
