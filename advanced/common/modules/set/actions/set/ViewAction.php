<?php

namespace common\modules\set\actions\set;

use common\modules\set\models\Sets;
use Yii;

class ViewAction extends \yii\rest\ViewAction
{
	public function run($id)
	{
		$setExists = Sets::find()
			->where(['or', ['system_title' => $id], ['id' => $id]])
			->exists();

		if (!$setExists) {
			Yii::$app->response->statusCode = 404;
		}

		if (is_numeric($id)) {
			return Sets::findOne($id);
		}

		return ['items' => $this->getSetItems()];
	}

	private function getSetItems()
	{
		$searchAction = new SearchAction('search', $this->controller, ['modelClass' => Sets::class]);

		return $searchAction->run()[0]['items'];
	}
}
