<?php

namespace common\modules\set\actions\accessory;

use common\modules\set\models\AccessoryCategory;
use common\modules\set\models\Sets;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		$accessorySetCategory = AccessoryCategory::findOne(['title' => $id]);

		if ($accessorySetCategory) {
			return ['items' => $accessorySetCategory->getSets()->select('id')->column()];
		}

		$items = Sets::find()
			->where(['or', ['system_title' => $id], ['id' => $id]])
			->select('id')
			->column();

		return ['items' => $items];
	}
}
