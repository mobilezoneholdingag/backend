<?php

namespace common\modules\set\actions\accessory;

use common\modules\set\models\AccessoryCategorySet;

class IndexAction extends \yii\rest\IndexAction
{
	public function run()
	{
		return ['items' => AccessoryCategorySet::find()->select('set_id')->groupBy('set_id')->column()];
	}
}
