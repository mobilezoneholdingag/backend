<?php
/**
 * @var $model \common\modules\renewalRequest\models\RenewalRequest
 * @var $articles[]
 */
$offered = $model->renewalRequestOffers;

if (!is_array($offered)) {
	return;
}

foreach ($offered as $offer) {
	?>
	<tr>
		<td><input checked class="toggleSelect" type="checkbox"></td>
		<td><?= $offer->tariff->getProviderTitle() ?></td>
		<td><?= $offer->tariff->getOfferTypeTitle() ?></td>
		<td><?= $offer->tariff_id ?></td>
		<td><?= $offer->tariff->title ?></td>
		<td><?= $offer->tariff->contract_duration ?></td>
		<td><?= $offer->tariff->data_volume ?></td>
		<td><?= ($offer->article->product_name ?? null) . ($offer->article->id ?? null) ?></td>
		<td><?= $offer->getOfferLink($model->customer->language ?? null) ?></td>
	</tr>
	<?php
}
?>
