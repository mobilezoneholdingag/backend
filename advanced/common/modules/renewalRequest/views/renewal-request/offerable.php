<?php
/**
 * @var $offerable_tariffs[]
 * @var $form \yii\widgets\ActiveForm
 * @var $model \common\modules\renewalRequest\models\RenewalRequest
 */

use common\models\Tariffs;

foreach ($offerable_tariffs as $tariff) {
	?>
	<tr>
		<td><input value="<?= $tariff['id'] ?>" class="toggleSelect" type="checkbox"></td>
		<td><?= $tariff['provider_title'] ?></td>
		<td><?= $tariff['offertype_title'] ?></td>
		<td><?= $tariff['id'] ?></td>
		<td><?= $tariff['title'] ?></td>
		<td><?= $tariff['contract_duration'] ?></td>
		<td><?= $tariff['data_volume'] ?></td>
		<td>
			<?= $form->field($model, 'renewal_request_offers')->dropDownList(
				Tariffs::getAllHardwareArticlesForDropdown($tariff['id']),
				[
					'name' => 'RenewalRequest[renewal_request_offers][' . $tariff['id']  . ']',
					'id' => $tariff['id'],
				]
			)->label(false)?>
		</td>
	</tr>
	<?php
}
?>
