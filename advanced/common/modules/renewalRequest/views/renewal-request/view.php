<?php

use common\helper\DataHelperCountries;
use common\models\GenderInterface;
use common\models\PassportTypeInterface;
use common\modules\renewalRequest\models\RenewalRequest;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\renewalRequest\models\RenewalRequest */
/* @var $offered \common\modules\renewalRequest\models\RenewalRequestOffers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Contract Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-request-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?php if (Yii::$app->user->can('starlord')): ?>
			<?= Html::a('Delete', ['delete', 'id' => $model->id], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Are you sure you want to delete this item?',
					'method' => 'post',
				],
			]) ?>
		<?php endif; ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'comment',
			'id',
			[
				'attribute' => 'order',
				'value' => Html::a($model->order_id, Url::to(['/order/order/view', 'id' => $model->order_id])),
				'format' => 'raw'
			],
			'email:email',
			[
				'attribute' => 'gender',
				'value' => GenderInterface::ALL[$model->gender] ?? null,
			],
			'firstname',
			'lastname',
			'customer_id',
			'birthday',
			[
				'attribute' => 'passport_type',
				'value' => PassportTypeInterface::ALL[$model->gender] ?? null,
			],
			[
				'attribute' => 'nationality',
				'value' => $model->nationality ? Yii::t('countries', DataHelperCountries::getCountries()[strtoupper($model->nationality)]) : null,
			],
			'passport_number',
			'status',
			'phone_number',
			[
				'attribute' => 'provider_id',
				'value' => $model->provider->title
			],
			'current_tariff_id',
			'monthly_national_data_volume',
			'monthly_international_data_volume',
			'monthly_national_phone_minutes',
			'monthly_international_phone_minutes',
			'monthly_phone_minutes_from_abroad',
		],
	]) ?>

	<?php
	if(in_array($model->status, [RenewalRequest::STATUS_OFFERED, RenewalRequest::STATUS_VIEWED, RenewalRequest::STATUS_CONFIRMED])) {
		echo $this->render('offerable-tariffs', compact('model'));
	}
	?>

</div>
