<?php

use common\modules\renewalRequest\models\RenewalRequest;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\renewalRequest\models\RenewalRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $fileUpdatedAt string */

$this->title = 'Renewal Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="renewal-request-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php if (Yii::$app->user->can('editor')): ?>
		<?php if ($fileUpdatedAt): ?>
			<p>
				File updated at: <?= $fileUpdatedAt ?>
			</p>
		<?php endif; ?>
		<p>
			<?= Html::a(Yii::t('app', 'Export renewal-requests.xml'), ['export-renewal-request-list'], ['class' => 'btn btn-primary']) ?>
			<?php if ($fileUpdatedAt): ?>
				<?= Html::a(Yii::t('app', 'Download renewal-requests.xml'), ['download-renewal-request-list'], ['class' => 'btn btn-success']) ?>
			<?php endif; ?>
		</p>
	<?php endif; ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			'id',
			'created_at',
			'customer_id',
			'email',
			'lastname',
			['attribute' => 'providerTitle', 'value' => 'provider.title'],
			'phone_number',
			'status',
			'valid_until_date',
			'updated_at:DateTime',
			[
				'attribute' => 'order',
				'value' => function(RenewalRequest $renewalRequest) {
					return Html::a($renewalRequest->order_id, Url::to(['/order/order/view', 'id' => $renewalRequest->order_id]));
				},
				'format' => 'raw'
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
			],
		],
	]); ?>
</div>
