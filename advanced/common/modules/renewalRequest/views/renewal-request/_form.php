<?php

use common\helper\DataHelperCountries;
use common\models\GenderInterface;
use common\models\PassportTypeInterface;
use common\models\Provider;
use common\modules\renewalRequest\models\RenewalRequest;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\renewalRequest\models\RenewalRequest */
/* @var $offerable_tariffs \common\models\Tariffs[] */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="renewal-request-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
	</div>

	<?= $form->field($model, 'comment')->textarea() ?>

	<div class="row">
		<div class="col-sm-12">
			<fieldset style="background-color: #f5f5f5;">
				<div class="col-sm-6">
						<legend>Kunde</legend>
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'customer_id')->input('number', ['disabled' => 'disabled']) ?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'email')->input('email', ['maxlength' => true, 'disabled' => 'disabled']) ?>
								<?= $form->field($model, 'gender')->radioList(GenderInterface::ALL) ?>
								<?= $form->field($model, 'firstname')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
								<?= $form->field($model, 'lastname')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
							</div>
							<div class="col-sm-6">
								<?= $form->field($model, 'birthday')->input('date', ['disabled' => 'disabled']) ?>
								<?= $form->field($model, 'nationality')->textInput(
									[
										'maxlength' => true,
										'disabled' => 'disabled',
										'value' => $model->nationality ? DataHelperCountries::getCountries()[strtoupper($model->nationality)] : null,
									]
								) ?>
								<?= $form->field($model, 'passport_type')->dropDownList(
									PassportTypeInterface::ALL,
									['disabled' => 'disabled']
								) ?>

								<?= $form->field($model, 'passport_number')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
							</div>
						</div>
				</div>
				<div class="col-sm-6">
					<legend>Verlängerungsanfrage</legend>

					<div class="col-sm-6">
						<?= $form->field($model, 'status')->dropDownList(
							array_combine(RenewalRequest::STATUSES, RenewalRequest::STATUSES),
							['disabled' => 'disabled']
						) ?>

						<?= $form->field($model, 'phone_number')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

						<?= $form->field($model, 'provider_id')->dropDownList(
						ArrayHelper::map(Provider::find()->asArray()->all(), 'id', 'title'),
						['disabled' => 'disabled']
						) ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($model, 'current_tariff_id')->dropDownList(
						   $model->provider
								->getTariffs()
								->where(['is_active' => true])
								->select(['id', 'title'])
								->createCommand()
								->queryAll(PDO::FETCH_KEY_PAIR),
							['prompt' => 'aktuellen Tarif wählen']
						) ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($model, 'url')->textInput(['disabled' => 'disabled']) ?>

						<?= $form->field($model, 'order_id')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
					</div>

				</div>
			</fieldset>
		</div>
		<div class="col-sm-12">

			<fieldset style="background-color: #f9f9f9">
				<legend>Tarifpräferenzen</legend>
				<div class="col-sm-6">
					<?= $form->field(
						$model,
						'monthly_national_data_volume',
						[
							'template' => '{label}: {input} MB',
							'enableClientValidation' => false,
							'inputOptions' => [
								'value' => $model->applyRangeFormat(
									$model->monthly_national_data_volume,
									'monthly_national_data_volume'
								),
							],
						]
					)->textInput(['disabled' => 'disabled']); ?>
					<?= $form->field(
						$model,
						'monthly_international_data_volume',
						[
							'template' => '{label}: {input} MB',
							'enableClientValidation' => false,
							'inputOptions' => [
								'value' => $model->applyRangeFormat(
									$model->monthly_international_data_volume,
									'monthly_international_data_volume'
								),
							],
						]
					)->textInput(['disabled' => 'disabled']) ?>
				</div>
				<div class="col-sm-6">
					<?= $form->field(
						$model,
						'monthly_national_phone_minutes',
						[
							'template' => '{label}: {input} Minuten',
							'enableClientValidation' => false,
							'inputOptions' => [
								'value' => $model->applyRangeFormat(
									$model->monthly_national_phone_minutes,
									'monthly_national_phone_minutes'
								),
							],
						]
					)->textInput(['disabled' => 'disabled']) ?>
					<?= $form->field(
						$model,
						'monthly_international_phone_minutes',
						[
							'template' => '{label}: {input} Minuten',
							'enableClientValidation' => false,
							'inputOptions' => [
								'value' => $model->applyRangeFormat(
									$model->monthly_international_phone_minutes,
									'monthly_international_phone_minutes'
								),
							],
						]
					)->textInput(['disabled' => 'disabled']) ?>
					<?= $form->field(
						$model,
						'monthly_phone_minutes_from_abroad',
						[
							'template' => '{label}: {input} Minuten',
							'enableClientValidation' => false,
							'inputOptions' => [
								'value' => $model->applyRangeFormat(
									$model->monthly_phone_minutes_from_abroad,
									'monthly_phone_minutes_from_abroad'
								),
							],
						]
					)->textInput(['disabled' => 'disabled']) ?>
				</div>
			</fieldset>
		</div>

		<?= $model->status != RenewalRequest::STATUS_DENIED ? $this->render('offerable-tariffs', compact('offerable_tariffs', 'form', 'model')) : null ?>
		<?php
			if($model->isUpdateable()) {
		?>
				<div class="form-group">
					<?= Html::submitButton(
						$model->isNewRecord ? 'Create' : 'Angebot absenden (schickt E-Mail an den Kunden)',
						['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
					) ?>
				</div>
				<?php if(!$model->isNewRecord) { ?>
				<div class="form-group">
					<?= Html::a(
						$model->isNewRecord ? 'Create' : 'Ablehnen',
						['deny', 'id' => $model->id],
						[
							'class' => 'btn btn-danger',
							'data' => [
								'confirm' => 'Verlängerungsanfrage wirklich ablehnen?',
								'method' => 'post',
							],
						]
					) ?>
				</div>
				<?php } ?>
		<?php } ?>
	<?php ActiveForm::end(); ?>

</div>
