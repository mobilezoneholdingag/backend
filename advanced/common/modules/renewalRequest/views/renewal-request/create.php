<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\renewalRequest\models\RenewalRequest */

$this->title = 'Create Renewal Request';
$this->params['breadcrumbs'][] = ['label' => 'Renewal Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="renewal-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
