<?php
/* @var $form yii\widgets\ActiveForm */
/* @var $model \common\modules\renewalRequest\models\RenewalRequest */

use common\models\Article;
use yii\helpers\ArrayHelper;
use yii\web\View;

?>


<fieldset id="offerUpdate" data-notupdateable="<?= !$model->isUpdateable() ? 'true' : 'false'; ?>">
	<legend><?= $model->isUpdateable() ? 'Angebot machen' : 'Angeboten'  ?></legend>
	<?= $model->isUpdateable() ? $form->field($model, 'valid_until_date')->textInput(
		[
			'value' => (new DateTime())->modify('+3 days')->format('Y-m-d'),
		    'pattern' => '[0-9]{4}-[0-9]{2}-[0-9]{2}',
		    'required' => 'required',
		    'oninvalid' => 'setCustomValidity("Dies ist ein Pflichtfeld. Bitte verwenden Sie folgendes Datumsformat: YYYY-mm-dd");',
			'onchange' => 'try{setCustomValidity("")}catch(e){}',
		]
	) : '' ?>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
			<tr>
				<th>Auswahl</th>
				<th>Provider</th>
				<th>Abotyp</th>
				<th>Abo ID</th>
				<th>Aboname</th>
				<th>Mindestvertragslaufzeit</th>
				<th>Datenvolumen</th>
				<th>Artikel</th>
				<?= !$model->isUpdateable() ? "<th>Offerlink</th>" : ''?>
			</tr>
			</thead>
			<tbody>
				<?php
					if($model->isUpdateable()) {
						echo $this->render('offerable', compact('offerable_tariffs', 'form', 'model'));
					} else {
						echo $this->render('offered', compact('model'));
					}
				?>
			</tbody>
		</table>
		</div>
</fieldset>
<?php
	$this->registerJsFile('/js/renewalrequests/offerable.js');
?>
