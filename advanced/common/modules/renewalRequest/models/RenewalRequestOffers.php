<?php

namespace common\modules\renewalRequest\models;

use common\helper\UrlHelper;
use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $article_id
 * @property integer $tariff_id
 * @property integer $renewal_request_id
 * @property Tariffs $tariff
 * @property Article $article
 * @property ArticleTariffs $articleTariff
 * @property RenewalRequest $renewalRequest
 * @property string $offerLink
 * @property string $hash
 */
class RenewalRequestOffers extends ActiveRecord
{
	public static function tableName()
	{
		return 'renewal_request_offers';
	}

	/**
	 * @return ActiveQuery
	 */
	public function getArticle()
	{
		return $this->hasOne(Article::className(), ['id' => 'article_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getTariff()
	{
		return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getRenewalRequest()
	{
		return $this->hasOne(RenewalRequest::className(), ['id' => 'renewal_request_id']);
	}

	public function getOfferLink($language = null) {
		$links = [
			'ch' => 'aboverlaengern',
			'de-CH' => 'aboverlaengern',
			'de' => 'aboverlaengern',
			'fr-FR' => 'contrats-etendre',
			'fr' => 'contrats-etendre',
			'it-IT' => 'contratti-estendere',
			'it' => 'contratti-estendere',
		];
		// languages are extracted from Yii::$app->language on customer save to fix bugs i added
		// the languages without dash since they've been saved like that in database
		// New users however will be saved with dashes in the language props
		$language = $language ?? Yii::$app->language;

		$urlManager = UrlHelper::determineUrlManager($this->renewalRequest->customer->language ?? null);
		return $urlManager->hostInfo . '/' . $links[$language] . '/' . $this->hash;
	}

	public static function getRandomUniqueHash()
	{
		$hash = '';
		$tries = 0;
		while ($tries < 50) {
			$length = rand(6, 10);
			$hash = Yii::$app->security->generateRandomString($length);
			if (RenewalRequestOffers::find()->where(['hash' => $hash])->exists()) {
				$tries++;
				continue;
			}

			return $hash;
		}

		return $hash;
	}

	public function getArticleTariff()
	{
		return $this->hasOne(ArticleTariffs::className(), ['article_id' => 'article_id', 'tariff_id' => 'tariff_id']);
	}
}
