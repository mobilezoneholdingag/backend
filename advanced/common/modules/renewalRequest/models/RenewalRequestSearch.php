<?php

namespace common\modules\renewalRequest\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ContractRequestSearch represents the model behind the search form about `common\models\ContractRequest`.
 */
class RenewalRequestSearch extends RenewalRequest
{
	public $providerTitle;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'gender', 'passport_type', 'provider_id', 'customer_id'], 'integer'],
			[['phone_number', 'firstname', 'lastname', 'birthday', 'passport_number', 'email', 'status', 'providerTitle'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = RenewalRequest::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->setSort([
			'defaultOrder' => [
				'id' => SORT_DESC,
			],
			'attributes' => [
				'id',
				'created_at',
				'lastname',
				'email',
				'status',
				'customer_id',
			]
		]);

		$dataProvider->sort->attributes['providerTitle'] = [
			'asc' => ['provider.title' => SORT_ASC],
			'desc' => ['provider.title' => SORT_DESC],
		];

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->joinWith(['provider']);

		$query->andFilterWhere([
			'renewal_requests.id' => $this->id,
			'created_at' => $this->created_at,
			'customer_id' => $this->customer_id,
			'provider_id' => $this->provider_id,
		]);

		$query->andFilterWhere(['like', 'phone_number', $this->phone_number])
			->andFilterWhere(['like', 'lastname', $this->lastname])
			->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', RenewalRequest::tableName() . '.status', $this->status])
			->andFilterWhere(['like', 'provider.title', $this->providerTitle]);

		return $dataProvider;
	}
}
