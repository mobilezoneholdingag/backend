<?php

namespace common\modules\renewalRequest\models;

use common\models\ArticleTariffs;
use common\models\behaviors\TimestampBehaviorSettings;
use common\models\Customer;
use common\models\CustomerAddress;
use common\models\CustomerPhoneNumber;
use common\models\GenderInterface;
use common\models\OfferType;
use common\models\Provider;
use common\models\Tariffs;
use common\modules\order\models\Order;
use common\modules\renewalRequest\events\RenewalRequestStatusDeniedEvent;
use common\modules\renewalRequest\events\RenewalRequestStatusOfferedEvent;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contract_requests".
 *
 * @property integer $id
 * @property string $status
 * @property integer $gender
 * @property string $phone_number
 * @property string $firstname
 * @property string $lastname
 * @property string $birthday
 * @property integer $passport_type
 * @property string $passport_number
 * @property string $email
 * @property integer $provider_id
 * @property integer $customer_id
 * @property integer $current_tariff_id
 * @property integer $monthly_national_data_volume
 * @property integer $monthly_international_data_volume
 * @property integer $monthly_national_phone_minutes
 * @property integer $monthly_international_phone_minutes
 * @property integer $monthly_phone_minutes_from_abroad
 * @property integer $order_id
 * @property string $valid_until_date
 * @property string $url
 * @property string $comment
 *
 * @property ArticleTariffs $offer
 * @property Tariffs $currentTariff
 * @property Provider $provider
 * @property Customer $customer
 * @property RenewalRequestOffers[]|ActiveQuery $renewalRequestOffers
 * @property Tariffs[] $providerTariffs
 * @property ArticleTariffs[] $providerArticleTariffs
 * @property Order $order
 */
class RenewalRequest extends ActiveRecord
{
	public $renewal_request_offers;

	const EVENT_STATUS_CHANGED_TO_OFFERED = 'eventStatusChangedToOffered';
	const EVENT_STATUS_CHANGED_TO_DENIED = 'eventStatusChangedToDenied';

	const STATUS_OPEN = 'open';
	const STATUS_PORTED = 'ported';
	const STATUS_OFFERED = 'offered';
	const STATUS_VIEWED = 'viewed';
	const STATUS_CONFIRMED = 'confirmed';
	const STATUS_DENIED = 'denied';
	const STATUS_UNKNOWN_OFFERS = 'unknown_offers';

	const STATUSES = [
		self::STATUS_OPEN,
		self::STATUS_PORTED,
		self::STATUS_OFFERED,
		self::STATUS_VIEWED,
		self::STATUS_CONFIRMED,
		self::STATUS_DENIED,
		self::STATUS_UNKNOWN_OFFERS,
	];
	const PREFERENCES_RANGES = [
		'monthly_national_data_volume' => [0, 10000],
		'monthly_international_data_volume' => [0, 10000],
		'monthly_national_phone_minutes' => [0, 3000],
		'monthly_international_phone_minutes' => [0, 3000],
		'monthly_phone_minutes_from_abroad' => [0, 3000],
	];
	const PREFERENCE_MAX_EXEEDED_DESCRIPTION = 'unlimited';

	const LIST_XML_NAME = 'renewal-requests.xml';

	public function __construct(array $config = [])
	{
		parent::__construct($config);

		$this->on(
			RenewalRequest::EVENT_BEFORE_INSERT,
			function($event) {
				if (!($customer = Customer::findOne(['email' => $this->email]))) {
					$customer = new Customer(
						[
							'email' => $this->email,
							'first_name' => $this->firstname,
							'last_name' => $this->lastname,
							'gender' => $this->gender,
							'language' => Yii::$app->language,
							'birthday' => $this->birthday,
							'passport_type' => $this->passport_type,
							'passport_id' => $this->passport_number,
						]
					);
					$customer->save();

					(new CustomerPhoneNumber(
						[
							'number' => $this->phone_number,
							'customer_id' => $customer->id,
							'provider' => $this->provider_id,
						]
					))->save();

					(new CustomerAddress(
						[
							'sex' => $this->gender,
							'customer_id' => $customer->id,
							'firstname' => $this->firstname,
							'lastname' => $this->lastname,
						]
					))->save();
				}

				$this->customer_id = $customer->id;
			}
		);
	}

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		if (isset($changedAttributes['status']) && $changedAttributes['status'] != $this->status) {
			if ($this->status == self::STATUS_OFFERED) {
				$orderStatusReceivedEvent = new RenewalRequestStatusOfferedEvent();
				$orderStatusReceivedEvent->setRenewalRequest($this);
				dispatch($orderStatusReceivedEvent);
			} elseif ($this->status == self::STATUS_DENIED) {
				$orderStatusPaymentSuccessfulEvent = new RenewalRequestStatusDeniedEvent();
				$orderStatusPaymentSuccessfulEvent->setRenewalRequest($this);
				dispatch($orderStatusPaymentSuccessfulEvent);
			}
		}
	}

	public function save($runValidation = true, $attributeNames = null)
	{
		parent::save($runValidation, $attributeNames);

		if ($this->hasErrors()) {
			return false;
		}

		$offers = $this->renewal_request_offers;

		if ($offers) {
			foreach ($offers as $key => $article_id) {
				$hash = RenewalRequestOffers::getRandomUniqueHash();
				$renewal_request_offer = new RenewalRequestOffers(
					[
						'article_id' => (int)$article_id,
						'renewal_request_id' => $this->id,
						'tariff_id' => (int)$key,
						'hash' => $hash
					]
				);

				$renewal_request_offer->save();
			}
		}

		return true;
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'renewal_requests';
	}

	public function behaviors()
	{
		$behaviors = [
			'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
		];

		if (Yii::$app->id == 'app-backend') {
			$behaviors[] = LoggableBehavior::className();
		}

		return $behaviors;
	}

	/**
	 * @return ActiveQuery|ArticleTariffs
	 */
	public function getOffer()
	{
		return $this->hasOne(ArticleTariffs::class, ['id' => 'offer_id']);
	}

	/**
	 * @return ActiveQuery|Customer
	 */
	public function getCustomer()
	{
		return $this->hasOne(Customer::class, ['id' => 'customer_id']);
	}

	/**
	 * @return ActiveQuery|Provider
	 */
	public function getProvider()
	{
		return $this->hasOne(Provider::class, ['id' => 'provider_id']);
	}

	/**
	 * @return ActiveQuery|Tariffs
	 */
	public function getCurrentTariff()
	{
		return $this->hasOne(Tariffs::class, ['id' => 'current_tariff_id']);
	}

	/**
	 * @return \common\models\Tariffs[]|ActiveQuery
	 */
	public function getProviderTariffs()
	{
		return $this->provider->getTariffs();
	}

	/**
	 * @return ActiveQuery|Order
	 */
	public function getOrder()
	{
		return $this->hasOne(Order::class, ['id' => 'order_id']);
	}

	public function getProviderArticleTariffs()
	{
		$whereConditions = [
			'tariffs.offer_type_id' => OfferType::RENEWAL_TYPE,
			'tariffs.provider_id' => $this->provider_id,
		];

		if ($this->article_id) {
			$whereConditions['article_id'] = $this->article_id;
		}

		return ArticleTariffs::find()
			->select(['article_tariffs.*', 'tariffs.title'])
			->joinWith(['tariff'])
			->where($whereConditions)
			->asArray(true)
			->all();
	}

	public function getRenewalRequestOffers()
	{
		return $this->hasMany(RenewalRequestOffers::class, ['renewal_request_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'gender',
					'passport_type',
					'provider_id',
					'current_tariff_id',
					'monthly_national_data_volume',
					'monthly_international_data_volume',
					'monthly_national_phone_minutes',
					'monthly_international_phone_minutes',
					'monthly_phone_minutes_from_abroad',
				],
				'integer',
			],
			[['birthday', 'valid_until_date', 'url', 'comment'], 'safe'],
			[['email'], 'email'],
			[['provider_id', 'birthday'], 'required'],
			[['url'], 'string'],
			['renewal_request_offers', 'required', 'isEmpty' => [$this, 'validateRenewalRequestOffers'],
				'message' => 'Sie müssen zu jedem ausgewählten Tarif einen Artikel auswählen!'],
			[['phone_number', 'firstname', 'lastname', 'passport_number', 'email'], 'string', 'max' => 255],
			['status', 'in', 'range' => static::STATUSES],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'status' => Yii::t('app', 'Status'),
			'gender' => Yii::t('app', 'Geschlecht'),
			'phone_number' => Yii::t('app', 'Handynummer'),
			'firstname' => Yii::t('app', 'Vorname'),
			'lastname' => Yii::t('app', 'Nachname'),
			'birthday' => Yii::t('app', 'Day Of Birth'),
			'passport_type' => Yii::t('app', 'Passport Type'),
			'passport_number' => Yii::t('app', 'Passport ID'),
			'email' => Yii::t('app', 'Customer Email'),
			'provider_id' => Yii::t('app', 'Provider'),
			'article_id' => Yii::t('app', 'Hardware'),
			'offer_id' => Yii::t('app', 'Angebots-Tariff'),
			'current_tariff_id' => Yii::t('app', 'aktueller Tariff'),
			'monthly_national_data_volume' => Yii::t('app', 'monatl. Datenvolumen - national'),
			'monthly_international_data_volume' => Yii::t('app', 'monatl. Datenvolumen - international'),
			'monthly_national_phone_minutes' => Yii::t('app', 'monatl. Gesprächsdauer - national'),
			'monthly_international_phone_minutes' => Yii::t('app', 'monatl. Gesprächsdauer - international'),
			'monthly_phone_minutes_from_abroad' => Yii::t('app', 'monatl. Gesprächsdauer - aus dem Ausland'),
			'provider.title' => Yii::t('app', 'Providername'),
			'customer_id' => Yii::t('app', 'Kunden ID'),
			'created_at' => Yii::t('app', 'Created At'),
			'valid_until_date' => Yii::t('app', 'Valid Until Date'),
			'url' => Yii::t('app', 'Request URL'),
			'comment' => Yii::t('app', 'Comment'),
		];
	}

	public function applyRangeFormat($property, $propName)
	{
		if ($property !== null && !(
				$property >= self::PREFERENCES_RANGES[$propName][0] &&
				$property <= self::PREFERENCES_RANGES[$propName][1])
		) {
			$property = Yii::t('app', self::PREFERENCE_MAX_EXEEDED_DESCRIPTION);
		}

		return $property;
	}

	public function isUpdateable(): bool
	{
		return !in_array($this->status, [
			self::STATUS_DENIED,
			self::STATUS_OFFERED,
			self::STATUS_CONFIRMED,
			self::STATUS_VIEWED
		]);
	}

	public function getOfferableTariffs() {
		return Tariffs::find()
			->select(
				[
					'tariffs.id',
					'tariffs.title',
					'contract_duration',
					'data_volume',
					'provider.title as provider_title',
					'offer_type.title as offertype_title',
					'tariffs.offer_type_id as offer_type_id',
					'tariffs.provider_id as provider_id'
				]
			)
			->leftJoin(Provider::tableName(), Provider::tableName() . '.id = `provider_id`')
			->leftJoin(OfferType::tableName(), OfferType::tableName() . '.id = `offer_type_id`')
			->where(
				[
					'tariffs.is_active' => true,
					'tariffs.is_active_erp' => true,
					'tariffs.provider_id' => $this->provider_id,
					'tariffs.is_b2b_allowed' => false,
					'tariffs.offer_type_id' => OfferType::RENEWAL_TYPE,
				]
			)->asArray()->all();
	}

	public function getGenderLabel() : string
	{
		return Yii::t('checkout', GenderInterface::ALL[$this->gender]);
	}

	public static function validateRenewalRequestOffers($value = []): bool
	{
		$value = $value ?? [];
		$valueCount = count(
			array_filter(
				array_values($value),
				function($value) {
					return intval($value) > 0;
				}
			)
		);
		$keyCount = count(array_filter(array_keys($value)));

		return $keyCount !== $valueCount;
	}
}
