<?php

namespace common\modules\renewalRequest;

use common\modules\renewalRequest\events\EventListener;
use yii\base\Module;

/**
 * order module definition class
 */
class RenewalRequestModule extends Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'common\modules\renewalRequest\controllers';

	public function init()
	{
		parent::init();
	}
}
