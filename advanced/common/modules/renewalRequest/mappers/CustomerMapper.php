<?php

namespace common\modules\renewalRequest\mappers;

use common\models\Customer;
use common\modules\renewalRequest\models\RenewalRequest;

class CustomerMapper
{
	public static function map(Customer $customer, RenewalRequest &$renewalRequest = null) {
		if (is_null($renewalRequest)) {
			$renewalRequest = new RenewalRequest();
		}

		$renewalRequest->gender = $customer->gender;
		$renewalRequest->phone_number = $customer->getFirstCustomerPhoneNumber();
		$renewalRequest->firstname = $customer->first_name;
		$renewalRequest->lastname = $customer->last_name;
		$renewalRequest->birthday = $customer->birthday;
		$renewalRequest->passport_type = $customer->passport_type;
		$renewalRequest->passport_number = $customer->passport_id;
		$renewalRequest->email = $customer->email;
		$renewalRequest->customer_id = $customer->id;

		return $renewalRequest;
	}
}
