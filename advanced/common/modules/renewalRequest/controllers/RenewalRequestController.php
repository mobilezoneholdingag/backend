<?php

namespace common\modules\renewalRequest\controllers;

use components\filesystem\ExportService;
use Yii;
use common\modules\renewalRequest\models\RenewalRequest;
use common\modules\renewalRequest\models\RenewalRequestSearch;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContractRequestController implements the CRUD actions for ContractRequest model.
 */
class RenewalRequestController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update', 'deny', 'download-renewal-request-list', 'export-renewal-request-list'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'backoffice'],
					],
					[
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * Lists all ContractRequest models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new RenewalRequestSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'fileUpdatedAt' => di(ExportService::class)->getFileUpdatedAt(RenewalRequest::LIST_XML_NAME),
		]);
	}

	/**
	 * Displays a single ContractRequest model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);

		return $this->render('view', compact('model'));
	}

	public function actionDeny($id)
	{
		$model = $this->findModel($id);

		$model->status = RenewalRequest::STATUS_DENIED;
		$model->save();

		return $this->redirect(['view', 'id' => $model->id]);
	}

	/**
	 * Updates an existing ContractRequest model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if($model->isUpdateable()) {
			$offerable_tariffs = $model->getOfferableTariffs();
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->status = RenewalRequest::STATUS_OFFERED;
			if($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}

		return $this->render('update', compact('offerable_tariffs', 'model'));

	}

	/**
	 * Deletes an existing ContractRequest model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the ContractRequest model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return RenewalRequest the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = RenewalRequest::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	
	public function actionExportRenewalRequestList()
	{
		di(ExportService::class)->start('mobilezone-erp/export-renewal-requests-xml-to-filesystem');

		return $this->redirect('index');
	}
	
	public function actionDownloadRenewalRequestList()
	{
		return di(ExportService::class)->download(RenewalRequest::LIST_XML_NAME);
	}
}
