<?php
namespace common\modules\renewalRequest\events;

use common\modules\renewalRequest\models\RenewalRequest;

class RenewalRequestStatusOfferedEvent extends AbstractRenewalRequestEvent
{
	public $name = RenewalRequest::EVENT_STATUS_CHANGED_TO_OFFERED;
}
