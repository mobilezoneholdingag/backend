<?php
namespace common\modules\renewalRequest\events;

use common\modules\renewalRequest\models\RenewalRequest;
use deinhandy\yii2events\event\ClassLevelEvent;

class AbstractRenewalRequestEvent extends ClassLevelEvent
{
	public $sender = RenewalRequest::class;

	/**
	 * @var RenewalRequest
	 */
	protected $renewalRequest;

	public function getRenewalRequest() : RenewalRequest
	{
		return $this->renewalRequest;
	}

	public function setRenewalRequest(RenewalRequest $renewalRequest) : AbstractRenewalRequestEvent
	{
		$this->renewalRequest = $renewalRequest;

		return $this;
	}
}
