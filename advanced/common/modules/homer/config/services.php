<?php

/** @var \deinhandy\yii2container\Container $container */
$container = Yii::$container;

$container->addTags(\common\modules\homer\events\OrderEventListener::class, ['events.listener']);
$container->addTags(\common\modules\homer\events\RenewalRequestEventListener::class, ['events.listener']);
