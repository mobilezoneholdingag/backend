<?php

namespace common\modules\homer;

use common\modules\homer\events\OrderEventListener;
use deinhandy\yii2container\Module;
use Yii;
use yii\console\Application;

class HomerModule extends Module
{
	protected $views = '/mailTemplates';

	public function init()
	{
		parent::init();
		if (Yii::$app instanceof Application) {
			$this->controllerNamespace = 'common\modules\homer\controllers';
		}
		new OrderEventListener();
	}
}