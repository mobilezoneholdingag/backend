<?php
namespace common\modules\homer\controllers;

use common\modules\homer\models\Mail;
use common\modules\homer\models\mailHandler\YiiMailHandler;
use common\modules\homer\models\mails\ContactMail;
use common\modules\homer\models\mails\ContractConfiguratorMail;
use Symfony\Component\Process\Process;
use Yii;
use yii\console\Controller;

class MailController extends Controller
{
	const MICROS_PER_SECOND = 1000000;
	/**
	 * @param int $timeToLiveInSeconds
	 * @param int $waitTimeInSeconds
	 */
	public function actionStartHomerDaemon($timeToLiveInSeconds = 60, $waitTimeInSeconds = 1)
	{
		$startTimestamp = time();
		while ((time() - $startTimestamp) < $timeToLiveInSeconds) {
			$process = new Process('/usr/bin/php /app/yii homer/mail/send-homers');
			$process->run();

			if ($process->isSuccessful() === false) {
				Yii::error($process->getErrorOutput());
				echo $process->getErrorOutput();
				exit;
			}

			sleep($waitTimeInSeconds);
		}
	}

	/**
	 * Sends planned homers which, are ready to send.
	 */
	public function actionSendHomers()
	{
		/** @var Mail[] $homers */
		$mails = Mail::getReadyToSendMails();
		$mailHandler = new YiiMailHandler();
		foreach ($mails as $mail) {
			try {
				$mailHandler->sendMail($mail);
			} catch (\Exception $e) {
				Yii::error((string)$e, 'mails');
			}
		}

		return 0;
	}

	public function actionSendContractConfigurator()
	{
		if (!empty(Yii::$app->request->post())) {
			$mail = new ContractConfiguratorMail();
			$mail->additional_template_parameters = Yii::$app->request->post();

			if ($mail->send()) {
				return Yii::$app->response->statusCode = 200;
			} else {
				return Yii::$app->response->statusCode = 404;
			}
		} else {
			return Yii::$app->response->statusCode = 404;
		}
	}

	public function actionSendContact()
	{
		if (!empty(Yii::$app->request->post())) {
			$mail = new ContactMail();
			$mail->recipients = [Yii::$app->request->post()['recipient'], Yii::$app->request->post()['email']];
			$mail->additional_template_parameters = json_encode(Yii::$app->request->post());

			if ($mail->send()) {
				return Yii::$app->response->statusCode = 200;
			} else {
				return Yii::$app->response->statusCode = 404;
			}

		} else {
			return Yii::$app->response->statusCode = 404;
		}
	}
}
