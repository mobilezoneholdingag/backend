<?php

namespace common\modules\homer\events;

use common\modules\homer\models\mails\RenewalRequestDeniedMail;
use common\modules\homer\models\mails\RenewalRequestOfferMail;
use common\modules\renewalRequest\events\RenewalRequestStatusDeniedEvent;
use common\modules\renewalRequest\events\RenewalRequestStatusOfferedEvent;
use common\modules\renewalRequest\models\RenewalRequest;
use deinhandy\yii2events\listener\AbstractEventListener;

class RenewalRequestEventListener extends AbstractEventListener
{
	public function getClassEvents() : array
	{
		return [
			RenewalRequest::class => [
				RenewalRequest::EVENT_STATUS_CHANGED_TO_OFFERED => [$this, 'handleRenewalRequestStatusChangedToOffered'],
				RenewalRequest::EVENT_STATUS_CHANGED_TO_DENIED => [$this, 'handleRenewalRequestStatusChangedToDenied'],
			],
		];
	}

	public function handleRenewalRequestStatusChangedToOffered(RenewalRequestStatusOfferedEvent $event)
	{
		$renewalRequest = $event->getRenewalRequest();
		$mail = new RenewalRequestOfferMail();
		$mail->setEntity($renewalRequest);
		$mail->send();
	}

	public function handleRenewalRequestStatusChangedToDenied(RenewalRequestStatusDeniedEvent $event)
	{
		$renewalRequest = $event->getRenewalRequest();
		$mail = new RenewalRequestDeniedMail();
		$mail->setEntity($renewalRequest);
		$mail->send();
	}
}
