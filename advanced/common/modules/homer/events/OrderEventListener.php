<?php

namespace common\modules\homer\events;

use common\modules\homer\models\mails\OrderConfirmationMail;
use common\modules\homer\models\mails\OrderOfferMail;
use common\modules\homer\models\mails\ShipmentConfirmationMail;
use common\modules\order\events\OrderStatusOfferedEvent;
use common\modules\order\events\OrderStatusReceivedEvent;
use common\modules\order\events\OrderTrackingIdsChangedEvent;
use common\modules\order\models\Order;
use deinhandy\yii2events\listener\AbstractEventListener;

class OrderEventListener extends AbstractEventListener
{

	public function getClassEvents() : array
	{
		return [
			Order::class => [
				Order::EVENT_AFTER_ORDER_STATUS_RECEIVED => [$this, 'handleOrderStatusReceived'],
				Order::EVENT_AFTER_ORDER_TRACKING_IDS_CHANGED => [$this, 'handleOrderTrackingIdsChanged'],
				Order::EVENT_AFTER_ORDER_STATUS_OFFERED => [$this, 'handleOrderOffer'],
			],
		];
	}

	public function handleOrderStatusReceived(OrderStatusReceivedEvent $event)
	{
		/** @var $order Order */
		$order = $event->getOrder();
		$mail = new OrderConfirmationMail();
		$mail->setEntity($order);
		$mail->send();
	}

	public function handleOrderTrackingIdsChanged(OrderTrackingIdsChangedEvent $event)
	{
		$order = $event->getOrder();
		$mail = new ShipmentConfirmationMail();
		$mail->setEntity($order);
		$mail->send();
	}

	public function handleOrderOffer(OrderStatusOfferedEvent $event)
	{
		$order = $event->getOrder();
		$mail = new OrderOfferMail();
		$mail->setEntity($order);
		$mail->send();
	}
}
