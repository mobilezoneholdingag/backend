<?php
namespace common\modules\homer\models\mailHandler;

use common\modules\homer\models\MailHandlerInterface;
use common\modules\homer\models\MailInterface;
use RuntimeException;
use Yii;
use yii\mail\MessageInterface;

class YiiMailHandler implements MailHandlerInterface
{
	/**
	 * @param MailInterface $mail
	 *
	 * @return bool
	 */
	public function sendMail(MailInterface $mail)
	{
		if ($mail->isMailSent() === true) {
			return false;
		}
		Yii::$app->language = $mail->getLanguageCode();
		try {
			/** @var MessageInterface $message */
			$message = Yii::$app->homerMailer->compose(
				['html' => $mail->getHtmlTemplatePath(), 'text' => $mail->getTextTemplatePath()],
				$mail->getTemplateParameters()
			)
				->setFrom($mail->getSender())
				->setTo($mail->getRecipients())
				->setSubject($mail->getSubject())
				->setBcc($mail->getBcc())
				->setCc($mail->getCc());
			$result = $message->send();
		} catch (RuntimeException $e) {
			Yii::error((string)$e, 'mails');

			return false;
		}
		$mail->afterSend($result);

		return $result;



	}
}
