<?php
namespace common\modules\homer\models;

use common\behaviors\SerializeBehavior;
use components\Locale;
use DateTime;
use RuntimeException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Mail
 *
 * @package common\modules\homer\models
 * @property integer $id
 * @property string $mail_class
 * @property string $entity_class
 * @property integer $entity_id
 * @property array recipients
 * @property string $send_planned_at
 * @property string $sent_at
 * @property boolean $sent
 * @property boolean $error
 * @property string $language_code
 * @property string $additional_template_parameters
 */
abstract class Mail extends ActiveRecord implements MailInterface
{
    /** @var  string $htmlTemplatePath */
    protected $htmlTemplatePath;
    /** @var  string $textTemplatePath */
    protected $textTemplatePath;
    /** @var  ActiveRecord $entity */
    protected $entity;
    /** @var  string|null $subject */
    protected $subject;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->mail_class = get_class($this);
        if (empty($this->additional_template_parameters)) {
            $this->additional_template_parameters = [];
        }
        if (empty($this->recipients)) {
            $this->recipients = [];
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => SerializeBehavior::className(),
                'attributes' => ['additional_template_parameters', 'recipients'],
                'entity' => $this,
            ],
        ];
    }

    public function rules()
    {
        return [
            [['mail_class'], 'required'],
            [['entity_id'], 'integer'],
            [
                [
                    'mail_class',
                    'entity_class',
                    'send_planned_at',
                    'sent_at',
                    'recipients',
                    'additional_template_parameters',
                    'language_code',
                ],
                'string',
            ],
            [['sent','error'], 'boolean'],
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'homer';
    }

    /**
     * Load the entity.
     *
     * @throws RuntimeException
     */
    protected function loadEntity()
    {
        if (empty($this->entity_class) || empty($this->entity_id)) {
            throw new RuntimeException(
                'Can not load entity, because entity class name or id or both were not set before.'
            );
        }
        /** @var ActiveRecord $className */
        $className = $this->entity_class;
        $entity = $className::findOne(['id' => $this->entity_id]);
        if ($entity === null) {
            throw new RuntimeException(
                'The entity "' . $this->entity_class . '" with id ' . $this->entity_id . ' does not exist.'
            );
        }
        $this->entity = $entity;
    }

    /**
     * @return ActiveRecord
     */
    public function getEntity()
    {
        if ($this->entity === null) {
            $this->loadEntity();
        }

        return $this->entity;
    }

    /**
     * Sets the entity.
     *
     * @param ActiveRecord $entity
     */
    public function setEntity(ActiveRecord $entity)
    {
        $this->entity_class = get_class($entity);
        $this->entity_id = $entity->getPrimaryKey();
    }

    /**
     * @return array
     */
    public function getBcc()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getCc()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getLanguageCode() : string
    {
        if (empty($this->language_code)) {
            $this->language_code = $this->detectLanguageCode();
        }
        return $this->language_code;
    }

    /**
     * @return string
     */
    protected function detectLanguageCode()
    {
        return Locale::active();
    }

    /**
     * @return string
     */
    public function getHtmlTemplatePath()
    {
        return $this->htmlTemplatePath;
    }

    /**
     * @return string
     */
    public function getTextTemplatePath()
    {
        return $this->textTemplatePath;
    }

    /**
     * Sends the mail.
     *
     * @return bool
     */
    public function send()
    {
        if ($this->isApproved()) {
            $now = new DateTime();
            $this->send_planned_at = $now->format('Y-m-d H:i:s');
            $this->save();

            return true;
        }

        return false;
    }

    /**
     * Plans the mail the given time and date.
     *
     * @param DateTime $dateTime
     *
     * @return bool
     */
    public function queue(DateTime $dateTime)
    {
        if ($this->isApproved()) {
            $this->send_planned_at = $dateTime->format('Y-m-d H:i:s');
            $this->save();

            return true;
        }

        return false;
    }

    /**
     * @param bool $sentSuccessfully
     */
    public function afterSend($sentSuccessfully)
    {
        $this->sent = $sentSuccessfully;
        $this->sent_at = date('Y-m-d H:i:s');
        $this->save();
    }

    /**
     * @param $recipient
     */
    public function addRecipient($recipient)
    {
        // not possible to do $this->recipients[] = $recipient; due to Yii magic
        $recipients = $this->recipients;
        $recipients[] = $recipient;
        $this->recipients = $recipients;
    }

    /**
     * @param $key
     * @param $value
     */
    public function addAdditionalTemplateParameter($key, $value)
    {
        // not possible to do $this->additional_template_parameters[$key] = $value; due to Yii magic
        $parameter = $this->additional_template_parameters;
        $parameter[$key] = $value;
        $this->additional_template_parameters = $parameter;
    }

    /**
     * Returns true if a homer with the same mail_class, entity_class and entity_id exists and it has the sent flag set
     * to 1.
     *
     * @return bool
     */
    public function wasMailSentBefore()
    {
        return null !== $this->getActiveQueryForMail()->andWhere(['sent' => 1])->one();
    }

    /**
     * Returns true if a homer with the same mail_class, entity_class and entity_id exists and it has a send_planned_at
     * set and was not already sent.
     *
     * @return bool
     */
    public function isMailAlreadyPlanned()
    {
        $query = $this->getActiveQueryForMail()
            ->andWhere(['sent_at' => null]);

        return null !== $query->one();
    }

    /**
     * Returns true if the homer already exists.
     *
     * @return bool
     */
    public function mailAlreadyExists()
    {
        return null !== $this::getActiveQueryForMail()->one();
    }

    /**
     * @return ActiveQuery
     */
    private function getActiveQueryForMail()
    {
        return $this->find()->where(
            [
                'mail_class' => $this->mail_class,
                'entity_class' => $this->entity_class,
                'entity_id' => $this->entity_id,
            ]
        );
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert === true) {
            $this->language_code = $this->getLanguageCode();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function isMailSent() : bool
    {
        return (bool)$this->sent;
    }

    /**
     * @return Mail[]
     */
    public static function getReadyToSendMails()
    {
        $now = new DateTime();

        return self::find()
            ->where(['sent' => null, 'sent_at' => null, 'error' => null])
            ->andWhere("send_planned_at <= '" . $now->format('Y-m-d H:i:s') . "'")
            ->all();
    }

    /**
     * @param array $row
     *
     * @return MailInterface
     */
    public static function instantiate($row)
    {
        if (isset($row['mail_class'])) {
            return new $row['mail_class'];
        }
        throw new RuntimeException(
            'It is forbidden to use find() without catching the "mail_class" column, while searching for homers.'
        );
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getRecipients()
    {
        return $this->recipients;
    }
}
