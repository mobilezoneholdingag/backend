<?php
namespace common\modules\homer\models;

interface MailHandlerInterface
{
	/**
	 * @param MailInterface $mail
	 *
	 * @return bool
	 */
	public function sendMail(MailInterface $mail);
}