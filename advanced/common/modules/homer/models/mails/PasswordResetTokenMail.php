<?php
namespace common\modules\homer\models\mails;

use common\models\Customer;
use common\modules\homer\models\Mail;
use Yii;

class PasswordResetTokenMail extends Mail
{
	/** @var  string $htmlTemplatePath */
	protected $htmlTemplatePath = 'passwordResetToken/html.twig';
	/** @var  string $textTemplatePath */
	protected $textTemplatePath = 'passwordResetToken/text.twig';

	/**
	 * Check if the mail is allowed to be send.
	 *
	 * @return boolean
	 */
	public function isApproved()
	{
		return $this->isMailAlreadyPlanned() === false;
	}

	/**
	 * @return string
	 */
	public function getSender()
	{
		return Yii::$app->params['mailing.senderEmailSupport'];
	}

	/**
	 * @return Customer
	 */
	public function getCustomer()
	{
		return $this->getEntity();
	}

	/**
	 * @param Customer $customer
	 */
	public function setCustomer(Customer $customer)
	{
		$this->setEntity($customer);
		$this->addRecipient($customer->email);
	}

	/**
	 * @return array
	 */
	public function getTemplateParameters()
	{
		return array_merge(['customer' => $this->getCustomer()], $this->additional_template_parameters);
	}

	/**
	 * @param string $url
	 */
	public function setTargetUrl($url)
	{
		$this->addAdditionalTemplateParameter('url', $url);
	}

	/**
	 * @return string
	 */
	public function getTargetUrl()
	{
		return $this->additional_template_parameters['url'] ?? '';
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return Yii::t('mail_password_reset', 'subject');
	}
}
