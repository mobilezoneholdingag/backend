<?php
namespace common\modules\homer\models\mails;

use common\models\Customer;
use common\modules\homer\models\Mail;
use components\Locale;
use Yii;
use yii\db\ActiveRecord;

abstract class RenewalRequestAbstractMail extends Mail
{
	public function isApproved() : bool
	{
		return $this->isMailAlreadyPlanned() === false && $this->wasMailSentBefore() === false;
	}

	public function getSender() : array
	{
		return Yii::$app->params['mailing.senderEmailSupport'];
	}

	public function getTemplateParameters() : array
	{
		return ['renewal_request' => $this->getEntity()];
	}

	public function setEntity(ActiveRecord $entity)
	{
		$this->addRecipient($entity->email);
		parent::setEntity($entity);
	}

	public function getCustomer() : Customer
	{
		return $this->getEntity()->customer;
	}

	public function detectLanguageCode() : string
	{
		// @TODO after we have changed the language handling we should remove this and only use the customer language
		$languages = Yii::$app->params['languages'];
		$customerLanguagePart = $this->getCustomer()->language;
		foreach ($languages as $language) {
			if ($customerLanguagePart == $language) {
				return $language;
			}
			$languageParts = explode('-', $language);
			if (isset($languageParts[0]) && $customerLanguagePart == $languageParts[0]) {
				return $language;
			}
		}

		return Locale::SUISSE;
	}
}
