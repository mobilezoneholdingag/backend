<?php
namespace common\modules\homer\models\mails;

use common\modules\homer\models\Mail;
use Yii;

class ContractConfiguratorMail extends Mail
{
	protected $htmlTemplatePath = 'contractConfigurator/html.twig';
	protected $textTemplatePath = 'contractConfigurator/text.twig';

	public function getSender()
	{
		return Yii::$app->params['mailing.senderEmailSupport'];
	}

	public function getRecipients()
	{
		return Yii::$app->params['contract_configurator_recipient'];
	}

	public function getSubject()
	{
		return Yii::t('homer', 'Contract_Configurator_Subject');
	}

	public function getTemplateParameters()
	{
		return ['form' => $this->additional_template_parameters];
	}

	public function isApproved()
	{
		return true;
	}
}
