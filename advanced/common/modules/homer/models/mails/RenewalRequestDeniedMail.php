<?php
namespace common\modules\homer\models\mails;

use Yii;

class RenewalRequestDeniedMail extends RenewalRequestAbstractMail
{
	/** @var  string $htmlTemplatePath */
	protected $htmlTemplatePath = 'renewalRequestDenied/html.twig';
	/** @var  string $textTemplatePath */
	protected $textTemplatePath = 'renewalRequestDenied/text.twig';

	public function getSubject() : string
	{
		return Yii::t('mail', 'Subject_Contract_Renewal_Denied');
	}
}
