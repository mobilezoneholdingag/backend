<?php
namespace common\modules\homer\models\mails;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use common\modules\homer\models\Mail;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use components\Locale;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class OrderConfirmationMail extends Mail
{
	/** @var  string $htmlTemplatePath */
	protected $htmlTemplatePath = 'orderConfirmation/html.twig';
	/** @var  string $textTemplatePath */
	protected $textTemplatePath = 'orderConfirmation/text.twig';

	/**
	 * Check if the mail is allowed to be send.
	 *
	 * @return boolean
	 */
	public function isApproved()
	{
		return self::mailAlreadyExists() === false;
	}

	/**
	 * @return string
	 */
	public function getSender()
	{
		return Yii::$app->params['mailing.senderEmailSupport'];
	}

	public function getTemplateParameters()
	{
		/** @var Order $order */
		$order = $this->getEntity();

		$orderHasTariff = false;
		foreach ($order->items as $item) {
			if ($item->item_product_type_id == Item::TYPE_TARIFF) {
				$orderHasTariff = true;
			}
		}

		$gender = $order->billingAddress->sex ?? $order->deliveryAddress->sex;

		return [
			'order' => $order,
			'customer' => $order->customer,
			'billing_address' => $order->billingAddress,
			'delivery_address' => $order->deliveryAddress,
			'insurance' => $order->insuranceOrder,
			'order_has_tariff' => $orderHasTariff,
			'gender_title' => $gender === 1 ? 'Herr' : 'Frau',
		];
	}

	public function getBcc()
    {
        return ["bestellungen@mobilezone.ch"];
    }

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return Yii::t('mail_order_confirmation', 'subject') . ' ' . $this->getEntity()->id;
	}

	/**
	 * @param Order $order
	 */
	public function setEntity(ActiveRecord $order)
	{
		/** @var $order Order */
		parent::setEntity($order);
		$this->addRecipient($order->customer->email);
	}

	protected function detectLanguageCode()
	{
		/*$languages = Yii::$app->params['languages'];
		$customerLanguagePart = $this->getEntity()->customer->language;
		foreach ($languages as $language) {
			$languageParts = explode('-', $language);
			if (isset($languageParts[0]) && $customerLanguagePart == $languageParts[0]) {
				return $language;
			}
		}

		return Locale::SUISSE;*/

        return Locale::active();
	}
}
