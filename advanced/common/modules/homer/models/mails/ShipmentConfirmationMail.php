<?php
namespace common\modules\homer\models\mails;

use common\modules\homer\models\Mail;
use common\modules\order\models\Order;
use components\Locale;
use Yii;
use yii\db\ActiveRecord;

/**
 * @method Order getEntity()
 */
class ShipmentConfirmationMail extends Mail
{
	/** @var  string $htmlTemplatePath */
	protected $htmlTemplatePath = 'shipmentConfirmation/html.twig';
	/** @var  string $textTemplatePath */
	protected $textTemplatePath = 'shipmentConfirmation/text.twig';

	/**
	 * @return string
	 */
	public function getSender()
	{
		return Yii::$app->params['mailing.senderEmailSupport'];
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return Yii::t('homer', 'Shipment_Title');
	}

	/**
	 * @return array
	 */
	public function getTemplateParameters()
	{
		/** @var Order $order */
		$order = $this->getEntity();

		return [
			'tracking_ids' => $order->getShipmentTrackingIds(),
			'billingAddress' => $order->billingAddress,
			'tracking_url' => 'http://www.post.ch/track?formattedParcelCodes='
		];
	}

	public function isApproved()
	{
		return true;
	}

	public function setEntity(ActiveRecord $order)
	{
		/** @var $order Order */
		parent::setEntity($order);
		$this->addRecipient($order->customer->email);
	}

	protected function detectLanguageCode()
	{
		return $this->getEntity()->customer->language ?? Locale::SUISSE;
	}
}
