<?php
namespace common\modules\homer\models\mails;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use common\modules\homer\models\Mail;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use components\Locale;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class PrePaymentReminderMail extends OrderConfirmationMail
{
	/** @var string $htmlTemplatePath */
	protected $htmlTemplatePath = 'prepayment/reminder.twig';
	/** @var string $textTemplatePath */
	protected $textTemplatePath = 'prepayment/reminder.text.twig';

	public function getSubject(): string
	{
		return Yii::t('mail', 'Subject_Pre_Payment_Reminder');
	}
}
