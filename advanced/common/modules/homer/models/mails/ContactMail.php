<?php

namespace common\modules\homer\models\mails;

use common\modules\homer\models\Mail;
use Yii;

class ContactMail extends Mail
{
	protected $htmlTemplatePath = 'contact/html.twig';
	protected $textTemplatePath = 'contact/text.twig';

	public function getSender()
	{
		return Yii::$app->params['mailing.senderEmailSupport'];
	}

	public function getRecipients()
	{
		return $this->recipients;
	}

	public function getSubject()
	{
		return Yii::t('homer', 'Page_Contact_Contact');
	}

	public function getTemplateParameters()
	{
		if($this->additional_template_parameters == false){
			$this->error = true;
			return [];
		}

		return json_decode($this->additional_template_parameters,true);
	}

	public function isApproved()
	{
		return true;
	}
}
