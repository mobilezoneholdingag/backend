<?php
namespace common\modules\homer\models\mails;

use Yii;

class RenewalRequestOfferMail extends RenewalRequestAbstractMail
{
	/** @var  string $htmlTemplatePath */
	protected $htmlTemplatePath = 'renewalRequestOffer/html.twig';
	/** @var  string $textTemplatePath */
	protected $textTemplatePath = 'renewalRequestOffer/text.twig';

	public function getSubject() : string
	{
		return Yii::t('mail', 'Subject_Contract_Renewal_Offer');
	}
}
