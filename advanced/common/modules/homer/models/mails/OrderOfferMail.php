<?php
namespace common\modules\homer\models\mails;

use common\modules\homer\models\Mail;
use common\modules\order\models\Order;
use components\Locale;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * @method Order getEntity()
 */
class OrderOfferMail extends Mail
{
	/** @var  string $htmlTemplatePath */
	protected $htmlTemplatePath = 'orderOffer/html.twig';
	/** @var  string $textTemplatePath */
	protected $textTemplatePath = 'orderOffer/text.twig';

	/**
	 * @return string
	 */
	public function getSender()
	{
		return Yii::$app->params['mailing.senderEmailSupport'];
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return Yii::t('homer', 'Offered_Title');
	}

	/**
	 * @return array
	 */
	public function getTemplateParameters()
	{
		/** @var Order $order */
		$order = $this->getEntity();

		return [
			'offer_link' => Url::to(['/checkout/cart/accept-offer', 'id' => $order->id]),
		];
	}

	public function isApproved()
	{
		return true;
	}

	public function setEntity(ActiveRecord $order)
	{
		/** @var $order Order */
		parent::setEntity($order);
		$this->addRecipient($order->customer->email);
	}

	protected function detectLanguageCode()
	{
		return $this->getEntity()->customer->language ?? Locale::SUISSE;
	}
}
