<?php
namespace common\modules\homer\models;

interface MailInterface
{
	/**
	 * Check if the mail is allowed to be send.
	 *
	 * @return boolean
	 */
	public function isApproved();

	/**
	 * @return array
	 */
	public function getBcc();

	/**
	 * @return array
	 */
	public function getCc();

	/**
	 * @return array
	 */
	public function getRecipients();

	/**
	 * @return string
	 */
	public function getSender();

	/**
	 * @return string
	 */
	public function getHtmlTemplatePath();

	/**
	 * @return string
	 */
	public function getTextTemplatePath();

	/**
	 * @return string
	 */
	public function getSubject();

	/**
	 * @return array
	 */
	public function getTemplateParameters();

	/**
	 * @param bool $sentSuccessfully
	 */
	public function afterSend($sentSuccessfully);

	/**
	 * @return bool
	 */
	public function isMailSent() : bool;

	/**
	 * @return string
	 */
	public function getLanguageCode() : string;
}