<?php

namespace common\modules\postfinance\controllers;

use common\modules\postfinance\lib\DirectLinkMaintenanceRequest;
use common\modules\postfinance\models\DirectLinkMaintenanceRequest as RequestModel;
use common\modules\postfinance\models\DirectLinkMaintenanceResponse as ResponseModel;
use common\modules\postfinance\models\EcomCallback;
use common\modules\postfinance\PostFinanceModule;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\BadResponseException;
use LogicException;
use PostFinance\Passphrase;
use PostFinance\ShaComposer\AllParametersShaComposer;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use SimpleXMLElement;
use Yii;
use yii\console\Controller;

/**
 * Manage PostFinance asynchronous tasks.
 *
 * @package frontend\controllers
 */
class CommandController extends Controller
{
	private $httpClient;

	public function __construct($id, $module, HttpClient $httpClient, $config = [])
	{
		$this->httpClient = $httpClient;

		parent::__construct($id, $module, $config);
	}

	public function actionCapture($orderId)
	{
		$this->performDirectLinkOperation($orderId, DirectLinkMaintenanceRequest::OPERATION_CAPTURE_LAST_OR_FULL);

		$this->stdout('Captured payment for order #' . $orderId . ' successfully.' . PHP_EOL);

		return true;
	}

	public function actionCancel($orderId)
	{
		$this->performDirectLinkOperation($orderId, DirectLinkMaintenanceRequest::OPERATION_AUTHORISATION_DELETE);

		$this->stdout('Cancelled payment for order #' . $orderId . ' successfully.' . PHP_EOL);

		return true;
	}

	private function performDirectLinkOperation($orderId, $operation)
	{
		$callback = $this->getCallback($orderId);

		$directLinkRequest = $this->createDirectLinkRequest($callback->pay_id, $operation);

		$request = $this->saveRequest($callback->id, $callback->pay_id, $directLinkRequest);

		$httpResponse = $this->sendRequest($directLinkRequest);

		$this->saveResponse($httpResponse, $request->id);
	}

	private function getCallback($orderId)
	{
		$callback = EcomCallback::find()->where(['order_id' => $orderId])->one();

		if (empty($callback)) {
			throw new RuntimeException('Ecom Callback for Order #' . $orderId . ' not found.');
		}

		if ($callback instanceof EcomCallback && $callback->isPaymentAccepted() === false) {
			throw new LogicException(
				'Payments for non-accepted ecom callback for order #' . $orderId . ' can not be captured.'
			);
		}

		return $callback;
	}

	private function createDirectLinkRequest(string $callbackPayId, $operation): DirectLinkMaintenanceRequest
	{
		$module = PostFinanceModule::getInstance();

		$passPhrase = new Passphrase($module->params['shaInSecret']);
		$shaComposer = new AllParametersShaComposer($passPhrase);

		$directLinkRequest = new DirectLinkMaintenanceRequest($shaComposer);
		$directLinkRequest->setPostFinanceUri($this->getApiUri());
		$directLinkRequest->setPspid($module->getPspId());
		$directLinkRequest->setUserId($module->params['directLinkApi']['user']);
		$directLinkRequest->setPassword($module->params['directLinkApi']['pass']);
		$directLinkRequest->setPayId($callbackPayId);
		$directLinkRequest->setOperation($operation);
		$directLinkRequest->validate();

		return $directLinkRequest;
	}

	private function getApiUri()
	{
		return $this->getModule()->params['directLinkApi']['uri'];
	}

	private function getModule()
	{
		return PostFinanceModule::getInstance();
	}

	private function saveRequest(
		int $callbackId,
		string $callbackPayId,
		DirectLinkMaintenanceRequest $directLinkRequest
	): RequestModel {
		$module = PostFinanceModule::getInstance();
		$request = new RequestModel();
		$request->ecom_callback_id = $callbackId;
		$request->operation = $directLinkRequest->getOperation();
		$request->pay_id = $callbackPayId;
		$request->pspid = $module->params['pspId'];
		$request->userid = $module->params['directLinkApi']['user'];
		$request->pswd = $module->params['directLinkApi']['pass'];
		$request->sha_sign = $directLinkRequest->getShaSign();
		$request->created_at = date('Y-m-d H:i:s');
		$request->save();

		return $request;
	}

	private function sendRequest(DirectLinkMaintenanceRequest $directLinkRequest): ResponseInterface
	{
		try {
			$httpResponse = $this->httpClient->post(
				$directLinkRequest->getPostFinanceUri(),
				[
					'form_params' =>
						array_merge(
							$directLinkRequest->toArray(),
							['SHASIGN' => $directLinkRequest->getShaSign()]
						),
				]
			);
		} catch (BadResponseException $e) {
			if ($e->getResponse()->getStatusCode() === 400) {
				throw new LogicException('PostFinance API server replied with [400] Bad Request.', 400, $e);
			} else {
				$msg = 'Unknown error when communication to PostFinance API: ' . $e->getMessage();
				Yii::error($msg, 'postfinance');
				throw new RuntimeException($msg, 0, $e);
			}
		}

		return $httpResponse;
	}

	private function saveResponse(ResponseInterface $httpResponse, int $requestId)
	{
		$xml = new SimpleXMLElement((string)$httpResponse->getBody());
		$attr = $xml->attributes();

		$response = new ResponseModel();
		$response->maintenance_request_id = $requestId;
		$response->order_id = $this->xmlAttrOrNull($attr, 'orderID');
		$response->pay_id = $this->xmlAttrOrNull($attr, 'PAYID');
		$response->pay_id_sub = $this->xmlAttrOrNull($attr, 'PAYIDSUB');
		$response->nc_status = $this->xmlAttrOrNull($attr, 'NCSTATUS');
		$response->nc_error = $this->xmlAttrOrNull($attr, 'NCERROR');
		$response->nc_error_plus = $this->xmlAttrOrNull($attr, 'NCERRORPLUS');
		$response->acceptance = $this->xmlAttrOrNull($attr, 'ACCEPTANCE');
		$response->status = $this->xmlAttrOrNull($attr, 'STATUS');
		$response->amount = $this->xmlAttrOrNull($attr, 'amount');
		$response->currency = $this->xmlAttrOrNull($attr, 'currency');
		$response->created_at = date('Y-m-d H:i:s');
		$response->save();
	}

	private function xmlAttrOrNull($arr, $attr)
	{
		if (isset($arr[$attr])) {
			return (string)$arr[$attr];
		}

		return null;
	}

	public function stdout($string)
	{
		if (!defined('STDOUT')) {
			return 0;
		}

		return parent::stdout($string);
	}
}
