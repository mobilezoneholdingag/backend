<?php

namespace common\modules\postfinance\controllers;

use common\modules\order\models\Order;
use common\modules\order\models\Status;
use common\modules\payment\helper\Method;
use common\modules\postfinance\lib\EcommercePaymentRequest;
use common\modules\postfinance\models\EcomCallback;
use common\modules\postfinance\models\EcomForm;
use common\modules\postfinance\PostFinanceModule;
use frontend\controllers\rest\Controller;
use InvalidArgumentException;
use LogicException;
use PostFinance\Ecommerce\EcommercePaymentResponse;
use PostFinance\FormGenerator\SimpleFormGenerator;
use PostFinance\Passphrase;
use PostFinance\ShaComposer\AllParametersShaComposer;
use RuntimeException;
use Yii;

/**
 * Default controller for the `postfinance` module
 */
class EcomController extends Controller
{
	// Magic strings in this mapping taken from \PostFinance\AbstractPaymentRequest::$brandsmap
	const PAYMENT_METHOD_TO_POSTFINANCE_METHOD_MAP = [
		Method::CREDIT_CARD => 'CreditCard',
		Method::POSTFINANCE_E_FINANCE => 'yellownet',
		Method::POSTFINANCE_CARD => 'Debit Direct',
	];

	public function actionForm()
	{
		$orderId = Yii::$app->getRequest()->getQueryParam('order_id');
		if (empty($orderId)) {
			throw new InvalidArgumentException('Can not generate PostFinance Ecom Form without given order.');
		}

		$order = Order::findOne($orderId);

		if (empty($order)) {
			throw new RuntimeException(
				'Order #' . $orderId . ' could not be found when trying to create a postfinance ecom form.'
			);
		}

		$form = new EcomForm();
		$form->parseOrder($order);
		$form->psp_id = $this->getModule()->getPspId();

		$request = $this->createEcomRequest();

		$request->setPostFinanceUri($this->getApiUri());
		$request->setPspid($form->psp_id);
		$request->setOrderid($form->order_id);
		$request->setAmount($form->amount);
		$request->setCurrency($form->currency);
		$request->setLanguage($form->language);
		$request->setCn($form->cn);
		$request->setEmail($form->email);
		$request->setOwnerAddress($form->owner_address);
		$request->setOwnerZip($form->owner_zip);
		$request->setOwnerTown($form->owner_town);
		$request->setOwnerCountry($form->owner_cty);
		$request->setOwnertelno($form->owner_tel_no);

		$frontendFqdn = Yii::$app->getRequest()->getHeaders()['X-Stardust-Frontend-FQDN'];
		/**
		 * As an explanation why we attach the order-id to all return URLs here: Yes, we are aware that PostFinance
		 * *actually* automatically adds the order-id parameter again when returning the user to our site. However,
		 * the order-id parameter returnes from their side would be "orderID", which is not properly camel cased as
		 * in "orderId", which we use as a parameter in our checkout.
		 * And since we did not want to change our checkout controller to comply with only one payment provider,
		 * we decided to rather attach the parameter ourselves here. Side note: PostFinance recognizes the already
		 * existing parameter and only adds their own different parameters.
		 */
		$orderIdParam = '?orderId=' . $orderId;
		$request->setAccepturl($frontendFqdn . 'checkout/checkout/success' . $orderIdParam);
		$request->setDeclineurl($frontendFqdn . 'checkout/checkout/error' . $orderIdParam);
		$request->setExceptionurl($frontendFqdn . 'checkout/checkout/error' . $orderIdParam);
		$request->setCancelurl($frontendFqdn . 'checkout/checkout/error' . $orderIdParam);

		if (empty(self::PAYMENT_METHOD_TO_POSTFINANCE_METHOD_MAP[$order->payment_method])) {
			throw new LogicException('Unknown postfinance payment type: ' . $order->payment_method);
		}
		$request->setPm(self::PAYMENT_METHOD_TO_POSTFINANCE_METHOD_MAP[$order->payment_method]);

		$form->sha_sign = $request->getShaSign();
		$form->created_at = date('Y-m-d H:i:s');

		if ($form->validate() === false) {
			return [
				'error' => 'EcomForm for order #' . $orderId . ' is invalid.',
				'errors' => $form->getErrors(),
			];
		}
		$form->save();

		$formGenerator = new SimpleFormGenerator;
		$html = $formGenerator->render($request);

		return [
			'form_html' => $html,
		];
	}

	private function getModule()
	{
		return PostFinanceModule::getInstance();
	}

	private function createEcomRequest() : EcommercePaymentRequest
	{
		$module = PostFinanceModule::getInstance();

		$passphrase = new Passphrase($module->params['shaInSecret']);
		$shaComposer = new AllParametersShaComposer($passphrase);

		return new EcommercePaymentRequest($shaComposer);
	}

	private function getApiUri()
	{
		return $this->getModule()->params['ecomApiUri'];
	}

	public function actionAcceptCallback()
	{
		$callback = $this->receiveAndSaveCallbackRequest();

		if ($callback->validate() === false) {
			return [
				'error' => 'EcomCallback for order #' . $callback->order_id . ' is invalid.',
				'errors' => $callback->getErrors(),
			];
		}

		$success = false;
		$order = $callback->order;

		if (!$order) {
			Yii::error(
				"Order with ID '{$callback->order_id}' for Callback '{$callback->id}' was not found.",
				'postfinance'
			);
			return ['success' => false];
		}

		if ($callback->isPaymentAccepted()) {
			$success = true;
			$order->status = Status::RECEIVED;
		} else {
			Yii::error(
				'PostFinance callback for order #' . $callback->order_id . ' was not accepted - data is stored in DB.',
				'postfinance'
			);
			$order->status = Status::PAYMENT_INCOMPLETE;
		}
		$order->save();

		return [
			'success' => $success,
		];
	}

	private function receiveAndSaveCallbackRequest() : EcomCallback
	{
		$request = Yii::$app->getRequest();

		// Set all properties from request
		$callback = new EcomCallback();
		$callback->order_id = $request->getBodyParam('orderID');
		$callback->amount = $request->getBodyParam('amount');
		$callback->payment_method = $request->getBodyParam('PM');
		$callback->acceptance = $request->getBodyParam('ACCEPTANCE');
		$callback->status = $request->getBodyParam('STATUS');
		$callback->card_number = $request->getBodyParam('CARDNO');
		$callback->customer_name = $request->getBodyParam('CN');
		$callback->pay_id = $request->getBodyParam('PAYID');
		$callback->nc_error = $request->getBodyParam('NCERROR');
		$callback->brand = $request->getBodyParam('BRAND');
		$callback->creditdebit = $request->getBodyParam('CREDITDEBIT');
		$callback->sha_sign = $request->getBodyParam('SHASIGN');
		$callback->created_at = date('Y-m-d H:i:');

		// Validate and stuff
		$module = PostFinanceModule::getInstance();

		$passphrase = new Passphrase($module->params['shaOutSecret']);
		$shaComposer = new AllParametersShaComposer($passphrase);

		$response = new EcommercePaymentResponse($_REQUEST);

		$callback->is_sha_sign_valid = $response->isValid($shaComposer);
		$callback->is_successful = $response->isSuccessful();

		$callback->save(false);

		return $callback;
	}

	public function actionDeclineCallback()
	{
		$callback = $this->receiveAndSaveCallbackRequest();

		if ($callback->validate() === false) {
			return [
				'error' => 'EcomCallback for order #' . $callback->order_id . ' is invalid.',
				'errors' => $callback->getErrors(),
			];
		}

		$order = $callback->order;
		$order->status = Status::PAYMENT_INCOMPLETE;
		$order->save();

		return [
			'success' => true,
		];
	}
}
