<?php

namespace common\modules\postfinance\models;

/**
 * This is the model class for table "postfinance_directlink_maintenance_request".
 *
 * @property integer $id
 * @property integer $ecom_callback_id
 * @property string $operation
 * @property string $pay_id
 * @property string $pspid
 * @property string $userid
 * @property string $pswd
 * @property string $sha_sign
 * @property string $created_at
 *
 * @property EcomCallback $ecomCallback
 * @property DirectlinkMaintenanceResponse[] $maintenanceResponses
 */
class DirectLinkMaintenanceRequest extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'postfinance_directlink_maintenance_request';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['ecom_callback_id'], 'integer'],
			[['sha_sign', 'created_at'], 'required'],
			[['created_at'], 'safe'],
			[['operation', 'pay_id', 'pspid', 'userid', 'pswd', 'sha_sign'], 'string', 'max' => 255],
			[
				['ecom_callback_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => EcomCallback::className(),
				'targetAttribute' => ['ecom_callback_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'ecom_callback_id' => 'Ecom Callback ID',
			'operation' => 'Operation',
			'pay_id' => 'Pay ID',
			'pspid' => 'Pspid',
			'userid' => 'Userid',
			'pswd' => 'Pswd',
			'sha_sign' => 'Sha Sign',
			'created_at' => 'Created At',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEcomCallback()
	{
		return $this->hasOne(EcomCallback::className(), ['id' => 'ecom_callback_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMaintenanceResponses()
	{
		return $this->hasMany(
			DirectLinkMaintenanceResponse::class,
			['maintenance_request_id' => 'id']
		);
	}
}
