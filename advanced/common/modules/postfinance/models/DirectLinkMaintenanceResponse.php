<?php

namespace common\modules\postfinance\models;

/**
 * This is the model class for table "postfinance_directlink_maintenance_response".
 *
 * @property integer $id
 * @property integer $maintenance_request_id
 * @property string $order_id
 * @property string $pay_id
 * @property string $pay_id_sub
 * @property string $nc_status
 * @property string $nc_error
 * @property string $nc_error_plus
 * @property string $acceptance
 * @property string $status
 * @property string $amount
 * @property string $currency
 * @property string $created_at
 *
 * @property DirectLinkMaintenanceRequest $maintenanceRequest
 */
class DirectLinkMaintenanceResponse extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'postfinance_directlink_maintenance_response';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['maintenance_request_id'], 'integer'],
			[['created_at'], 'required'],
			[['created_at'], 'safe'],
			[
				[
					'order_id',
					'pay_id',
					'pay_id_sub',
					'nc_status',
					'nc_error',
					'nc_error_plus',
					'acceptance',
					'status',
					'amount',
					'currency',
				],
				'string',
				'max' => 255,
			],
			[
				['maintenance_request_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => DirectLinkMaintenanceRequest::className(),
				'targetAttribute' => ['maintenance_request_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'maintenance_request_id' => 'Maintenance Request ID',
			'order_id' => 'Order ID',
			'pay_id' => 'Pay ID',
			'pay_id_sub' => 'Pay Id Sub',
			'nc_status' => 'Nc Status',
			'nc_error' => 'Nc Error',
			'nc_error_plus' => 'Nc Error Plus',
			'acceptance' => 'Acceptance',
			'status' => 'Status',
			'amount' => 'Amount',
			'currency' => 'Currency',
			'created_at' => 'Created At',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMaintenanceRequest()
	{
		return $this->hasOne(DirectLinkMaintenanceRequest::className(), ['id' => 'maintenance_request_id']);
	}
}
