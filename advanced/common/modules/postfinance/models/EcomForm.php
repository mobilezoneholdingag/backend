<?php

namespace common\modules\postfinance\models;

use common\modules\order\models\Order;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "postfinance_ecom_form".
 *
 * @property integer $id
 * @property string $psp_id
 * @property integer $order_id
 * @property integer $amount
 * @property string $currency
 * @property string $language
 * @property string $cn Customer name
 * @property string $email
 * @property string $owner_address
 * @property string $owner_zip
 * @property string $owner_town
 * @property string $owner_cty Customer country.
 * @property string $owner_tel_no Customer phone number.
 * @property string $sha_sign
 * @property string $accept_url
 * @property string $decline_url
 * @property string $exception_url
 * @property string $cancel_url
 * @property string $created_at
 *
 * @property EcomCallback $ecomCallback
 * @property DirectLinkMaintenanceRequest $maintenanceRequest
 */
class EcomForm extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'postfinance_ecom_form';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['psp_id', 'order_id', 'amount', 'currency', 'language', 'created_at'], 'required'],
			[['order_id', 'amount'], 'integer'],
			[['created_at'], 'safe'],
			[
				[
					'psp_id',
					'cn',
					'email',
					'owner_address',
					'owner_zip',
					'owner_town',
					'owner_cty',
					'owner_tel_no',
					'sha_sign',
					'accept_url',
					'decline_url',
					'exception_url',
					'cancel_url',
				],
				'string',
				'max' => 255,
			],
			[['currency'], 'string', 'max' => 3],
			[['language'], 'string', 'max' => 5],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'psp_id' => 'Psp ID',
			'order_id' => 'Order ID',
			'amount' => 'Amount',
			'currency' => 'Currency',
			'language' => 'Language',
			'cn' => 'CN = Customer Name',
			'email' => 'Email',
			'owner_address' => 'Owner Address',
			'owner_zip' => 'Owner Zip',
			'owner_town' => 'Owner Town',
			'owner_cty' => 'country of the customer',
			'owner_tel_no' => 'phone number of the customer',
			'sha_sign' => 'Sha Sign',
			'accept_url' => 'Accept Url',
			'decline_url' => 'Decline Url',
			'exception_url' => 'Exception Url',
			'cancel_url' => 'Cancel Url',
			'created_at' => 'Created At',
		];
	}

	public function parseOrder(Order $order)
	{
		$address = $order->billingAddress;

		$this->order_id = $order->id;
		$this->amount = (int)($order->price_once * 100); // Always in "cents"
		$this->currency = 'CHF';
		$this->language = $this->translateLanguageForApi($order->customer->language ?? '');
		$this->cn = ($address->firstname ?? '') . ' ' . ($address->lastname ?? '');
		$this->email = ($order->customer->email ?? '');
		$this->owner_address = ($address->street ?? '') . ' ' . ($address->house_number ?? '');
		$this->owner_zip = ($address->zip ?? '');
		$this->owner_town = ($address->city ?? '');
		$this->owner_cty = 'CH';
	}

	private function translateLanguageForApi($customerLanguage)
	{
		switch ($customerLanguage) {
			case 'it':
				return 'it_IT';
			case 'fr':
				return 'fr_FR';
			case 'de':
			default:
				return 'de_DE';
		}
	}

	public function getEcomForm()
	{
		return $this;
	}

	public function getEcomCallback()
	{
		return $this->hasOne(EcomCallback::class, ['order_id' => 'order_id']);
	}

	public function getMaintenanceRequest()
	{
		return $this->ecomCallback->maintenanceRequest ?? null;
	}

	protected function getMaintenanceResponse()
	{
		return $this->maintenanceRequest ? $this->maintenanceRequest->getMaintenanceResponses()->one() : null;
	}
}
