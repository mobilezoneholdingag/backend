<?php

namespace common\modules\postfinance\models;

use common\modules\order\models\Order;

/**
 * This is the model class for table "postfinance_ecom_callback".
 *
 * @property integer $id
 * @property string $order_id
 * @property string $amount
 * @property string $payment_method
 * @property string $acceptance
 * @property string $status
 * @property string $card_number
 * @property string $customer_name
 * @property string $pay_id
 * @property string $nc_error
 * @property string $brand
 * @property string $creditdebit
 * @property string $sha_sign
 * @property boolean $is_sha_sign_valid
 * @property boolean $is_successful
 * @property string $created_at
 *
 * @property Order $order
 * @property DirectLinkMaintenanceRequest $maintenanceRequest
 * @property DirectLinkMaintenanceResponse[] $maintenanceResponses
 */
class EcomCallback extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'postfinance_ecom_callback';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['amount'], 'number'],
			[['created_at'], 'safe'],
			[
				[
					'order_id',
					'payment_method',
					'acceptance',
					'status',
					'card_number',
					'customer_name',
					'pay_id',
					'nc_error',
					'brand',
					'creditdebit',
					'sha_sign',
				],
				'string',
				'max' => 255,
			],
			[
				[
					'is_sha_sign_valid',
					'is_successful',
				],
				'boolean',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'order_id' => 'Order ID',
			'amount' => 'Amount',
			'payment_method' => 'Payment Method',
			'acceptance' => 'Acceptance',
			'status' => 'Status',
			'card_number' => 'Card Number',
			'customer_name' => 'Customer Name',
			'pay_id' => 'Pay ID',
			'nc_error' => 'Nc Error',
			'brand' => 'Brand',
			'creditdebit' => 'Creditdebit',
			'sha_sign' => 'Sha Sign',
			'is_sha_sign_valid' => 'Is Sha Sign Valid',
			'is_successful' => 'Is Successful',
			'created_at' => 'Created At',
		];
	}

	/**
	 * Determines whether we accept the payment as authorized or not.
	 *
	 * @return bool
	 */
	public function isPaymentAccepted()
	{
		return $this->is_sha_sign_valid && $this->is_successful;
	}

	public function getMaintenanceRequest()
	{
		return $this->hasOne(DirectLinkMaintenanceRequest::className(), ['ecom_callback_id' => 'id']);
	}

	public function getMaintenanceResponses()
	{
		return $this->maintenanceRequest->maintenanceResponses;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrder()
	{
		return $this->hasOne(Order::className(), ['id' => 'order_id']);
	}
}
