<?php

use common\modules\postfinance\PostFinanceModule;
use deinhandy\yii2container\Container;

/** @var Container $container */
$container = Yii::$container;

// SHA passphrase
$container->set(PostFinance\Passphrase::class, function(Container $container) {
	$module = PostFinanceModule::getInstance();
	return new PostFinance\Passphrase($module->params['shaInSecret']);
});

// SHA composer
$container->set(PostFinance\ShaComposer\ShaComposer::class, function(Container $container) {

	return new PostFinance\ShaComposer\AllParametersShaComposer($container->get(PostFinance\Passphrase::class));
});

// Ecommerce Request Form
$container->set(PostFinance\Ecommerce\EcommercePaymentRequest::class, function(Container $container) {
	return new PostFinance\Ecommerce\EcommercePaymentRequest($container->get(PostFinance\ShaComposer\ShaComposer::class));
});

$container->addTags(\common\modules\postfinance\events\OrderEventListener::class, ['events.listener']);
