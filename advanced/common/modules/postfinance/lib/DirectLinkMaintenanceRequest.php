<?php

namespace common\modules\postfinance\lib;

use PostFinance\DirectLink\DirectLinkMaintenanceRequest as VendorRequest;

class DirectLinkMaintenanceRequest extends VendorRequest
{
	const TEST = "https://e-payment.postfinance.ch/ncol/test/maintenancedirect.asp";
	const PRODUCTION = "https://e-payment.postfinance.ch/ncol/prod/maintenancedirect.asp";

	/**
	 * No late static binding in parent, have to overwrite this thus.
	 *
	 * @return array
	 */
	public function getValidPostFinanceUris()
	{
		return [
			self::TEST,
			self::PRODUCTION
		];
	}

	public function getOperation()
	{
		return $this->parameters['operation'] ?? null;
	}
}
