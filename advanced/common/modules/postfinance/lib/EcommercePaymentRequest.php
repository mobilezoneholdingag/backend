<?php

namespace common\modules\postfinance\lib;

use PostFinance\Ecommerce\EcommercePaymentRequest as VendorRequest;

class EcommercePaymentRequest extends VendorRequest
{
	const TEST = "https://e-payment.postfinance.ch/ncol/test/orderstandard_utf8.asp";
	const PRODUCTION = "https://e-payment.postfinance.ch/ncol/prod/orderstandard_utf8.asp";

	/**
	 * This is all but the directly commented copied from parent; as it is a bit crappy to overwrite since the parent
	 * implementation is just accessing the property instead of calling a method.
	 *
	 * @var array
	 */
	protected $brandsmap = array(
		'Acceptgiro' => 'Acceptgiro',
		'AIRPLUS' => 'CreditCard',
		'American Express' => 'CreditCard',
		'Aurora' => 'CreditCard',
		'Aurore' => 'CreditCard',
		'Bank transfer' => 'Bank transfer',
		'BCMC' => 'CreditCard',
		'Belfius Direct Net' => 'Belfius Direct Net',
		'Billy' => 'CreditCard',
		'cashU' => 'cashU',
		'CB' => 'CreditCard',
		'CBC Online' => 'CBC Online',
		'CENTEA Online' => 'CENTEA Online',
		'Cofinoga' => 'CreditCard',
		'Dankort' => 'CreditCard',
		'Dexia Direct Net' => 'Dexia Direct Net',
		'Diners Club' => 'CreditCard',
		'Direct Debits AT' => 'Direct Debits AT',
		'Direct Debits DE' => 'Direct Debits DE',
		'Direct Debits NL' => 'Direct Debits NL',
		'eDankort' => 'eDankort',
		'EPS' => 'EPS',
		'Fortis Pay Button' => 'Fortis Pay Button',
		'giropay' => 'giropay',
		'iDEAL' => 'iDEAL',
		'ING HomePay' => 'ING HomePay',
		'InterSolve' => 'InterSolve',
		'JCB' => 'CreditCard',
		'KBC Online' => 'KBC Online',
		'Maestro' => 'CreditCard',
		'MaestroUK' => 'CreditCard',
		'MasterCard' => 'CreditCard',
		'MiniTix' => 'MiniTix',
		'MPASS' => 'MPASS',
		'NetReserve' => 'CreditCard',
		'Payment on Delivery' => 'Payment on Delivery',
		'PAYPAL' => 'PAYPAL',
		'paysafecard' => 'paysafecard',
		'PingPing' => 'PingPing',
		'PostFinance + card' => 'PostFinance Card',
		'PostFinance e-finance' => 'PostFinance e-finance',
		'PRIVILEGE' => 'CreditCard',
		'Sofort Uberweisung' => 'DirectEbanking',
		'Solo' => 'CreditCard',
		'TUNZ' => 'TUNZ',
		'UATP' => 'CreditCard',
		'UNEUROCOM' => 'UNEUROCOM',
		'VISA' => 'CreditCard',
		'Wallie' => 'Wallie',

		// custom added
		// This is for "PostFinance Card" - as extracted from their payment site (not in documentation)
		'Debit Direct' => 'Debit Direct',

		// This is for "PostFinance e-finance" - as extracted from their payment site (not in documentation)
		'yellownet' => 'yellownet',
	);

	/**
	 * No late static binding in parent, have to overwrite this thus.
	 *
	 * @return array
	 */
	public function getValidPostFinanceUris()
	{
		return [
			self::TEST,
			self::PRODUCTION
		];
	}
}
