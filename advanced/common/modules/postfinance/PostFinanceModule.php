<?php

namespace common\modules\postfinance;

use common\modules\postfinance\events\OrderEventListener;
use deinhandy\yii2container\Module;
use Yii;
use yii\console\Application;

class PostFinanceModule extends Module
{
	public function init()
	{
		parent::init();
		if (Yii::$app instanceof Application) {
			$this->controllerNamespace = 'common\modules\postfinance\controllers';
		}
		new OrderEventListener();
	}

	public function getPspId()
	{
		return PostFinanceModule::getInstance()->params['pspId'];
	}
}
