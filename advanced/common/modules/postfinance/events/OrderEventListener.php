<?php

namespace common\modules\postfinance\events;

use common\modules\order\events\OrderStatusDeclinedEvent;
use common\modules\order\models\Order;
use common\modules\payment\helper\Method;
use deinhandy\yii2events\listener\AbstractEventListener;
use Exception;
use Yii;

class OrderEventListener extends AbstractEventListener
{
	public function getClassEvents() : array
	{
		return [
			Order::class => [
				Order::EVENT_AFTER_ORDER_STATUS_DECLINED => [$this, 'handleOrderStatusDeclined'],
			],
		];
	}

	public function handleOrderStatusDeclined(OrderStatusDeclinedEvent $event)
	{
		/** @var $order Order */
		$order = $event->getOrder();

		if (!in_array($order->payment_method, Method::POSTFINANCE_METHODS)) {
			return;
		}

		try {
			Yii::$app->runAction('postfinance/command/cancel', [$order->id]);
			Yii::$app->session->addFlash('success', "[PostFinance] Die Autorisierung wurde erfolgreich abgebrochen.");
		} catch (Exception $e) {
			Yii::$app->session->addFlash('danger', "[PostFinance] Das Abbrechen der Autorisierung ist fehlgeschlagen!" .
				"<br>Fehlermeldung: {$e->getMessage()}");
		}
	}
}
