<?php

namespace common\modules\testCleanup;

use deinhandy\yii2container\Module;
use Yii;
use yii\console\Application;

class TestCleanupModule extends Module
{
	public function init()
	{
		parent::init();
		if (Yii::$app instanceof Application) {
			$this->controllerNamespace = 'common\modules\testCleanup\controllers\console';
		}
	}
}
