<?php
namespace common\modules\testCleanup\controllers\console;

use common\models\Customer;
use common\modules\order\models\Address;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use common\modules\renewalRequest\models\RenewalRequest;
use common\modules\runscope\services\ApiClient;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Yii;
use yii\base\Module;
use yii\console\Controller;

/**
 * see https://www.runscope.com/radar/qyygjwjmj9l7/environments > "Live" > "Initial Script" for all variables
 */
class CleanupController extends Controller
{
	const TESTER_EMAIL = 'tester+%.%@deinhandy.de';
	const LOGGED_IN_TESTER_EMAIL = 'tester@deinhandy.de';

	/** @var array */
	private $scriptVariables;

	/** @var ConsoleOutput */
	private $output;

	public function __construct($id, Module $module, array $config = [])
	{
		/** @var ApiClient $runscopeClient */
		$runscopeClient = di(ApiClient::class);
		$this->scriptVariables = $runscopeClient->getScriptVariables();
		$this->output = new ConsoleOutput();
		parent::__construct($id, $module, $config);
	}

	public function actionCleanup()
	{
		$this->output->writeln(PHP_EOL);

		try {
			$this->cleanupAcceptanceTests();
		} catch (\Exception $e) {
			Yii::error($e);
		}

		try {
			$this->cleanRenewalRequests();
		} catch (\Exception $e) {
			Yii::error($e);
		}

		$this->output->writeln(PHP_EOL);
	}

	private function cleanupAcceptanceTests()
	{
		$this->cleanupOrders();
		$this->cleanupCustomer();
	}

	private function cleanupOrders()
	{
		$this->removeOrdersFrom(static::TESTER_EMAIL);
		$this->removeOrdersFrom(static::LOGGED_IN_TESTER_EMAIL);
	}

	private function removeOrdersFrom(string $email)
	{
		$this->output->writeln("Cleaning up orders which got created in acceptance tests for email '{$email}'...");

		$customerIds = Customer::find()
			->select(['id'])
			->where("email LIKE '{$email}'")
			->column();
		$orderIds = Order::find()
			->select([Order::tableName() . '.id'])
			->where(['IN', 'customer_id', $customerIds])
			->column();

		// remove child items
		$deletedOrderItemsCount = Item::deleteAll(
			['AND',
				['IN', 'order_id', $orderIds],
				['IS NOT', 'parent_item_id', null]
			]
		);
		// kill childless parents
		$deletedOrderItemsCount += Item::deleteAll(['IN', 'order_id', $orderIds]);
		$this->output->writeln("<info>Removed {$deletedOrderItemsCount} order items.</info>" . PHP_EOL);

		$deletedOrderAddressCount = Address::deleteAll(['IN', 'customer_id', $customerIds]);
		$this->output->writeln("<info>Removed {$deletedOrderAddressCount} order addresses.</info>" . PHP_EOL);

		$deletedOrdersCount = Order::deleteAll(['IN', 'id', $orderIds]);
		$this->output->writeln("<info>Removed {$deletedOrdersCount} orders.</info>" . PHP_EOL);
	}

	private function cleanupCustomer()
	{
		$this->output->writeln("Cleaning up customers which got created in acceptance tests...");

		$db = Yii::$app->db;
		$tableName = Customer::tableName();
		$email = static::TESTER_EMAIL;

		$rows = $db->createCommand("DELETE FROM {$tableName} WHERE email LIKE '{$email}'")->execute();

		$this->output->writeln("<info>Removed {$rows} customers.</info>" . PHP_EOL);
	}

	private function cleanRenewalRequests()
	{
		$this->output->writeln("Cleaning up renewal requests which got created in API tests...");

		$customerEmail = $this->scriptVariables['test-email'] ?? null;
		$renewalRequestQuery = RenewalRequest::find()->where(['email' => $customerEmail]);
		$renewalRequestCount = $renewalRequestQuery->count();
		$renewalRequests = $renewalRequestQuery->all();

		$this->output->writeln("<info>Removing {$renewalRequestCount} renewal requests...</info>");
		$progressBar = new ProgressBar(
			$this->output,
			$renewalRequestCount
		);

		foreach ($renewalRequests as $renewalRequest) {
			$renewalRequest->delete();
			$progressBar->advance();
		}

		$progressBar->finish();
	}
}
