<?php

namespace common\modules\order\models;

use common\helper\DataHelperCountries;
use common\models\Article;
use common\models\behaviors\TimestampBehaviorSettings;
use common\models\Coupon;
use common\models\CustomerPhoneNumber;
use common\models\OfferType;
use common\models\PassportTypeInterface;
use common\modules\order\events\AbstractOrderEvent;
use common\modules\order\events\OrderStatusDeclinedEvent;
use common\modules\order\events\OrderStatusOfferedEvent;
use common\modules\payment\helper\Method;
use common\modules\pricing\models\ConsolidatedPrice;
use common\models\Customer;
use common\models\ProductCategory;
use common\models\Tariffs;
use common\modules\insurances\models\InsuranceOrder;
use common\modules\order\events\OrderStatusPaymentSuccessfulEvent;
use common\modules\order\events\OrderStatusReceivedEvent;
use common\modules\order\events\OrderTrackingIdsChangedEvent;
use common\modules\swisscom\models\logger\RequestLog;
use components\validators\MsisdnValidator;
use LogicException;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\base\Component;
use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $delivery_address_id
 * @property integer $billing_address_id
 * @property string $price_once
 * @property string $price_monthly
 * @property string $serial_numbers
 * @property string $msisdn
 * @property string $simcard_number
 * @property string $contract_number
 * @property string $vat_rate
 * @property string $payment_method
 * @property integer $status
 * @property string $shipment_tracking_ids
 * @property string $created_at
 * @property string $updated_at
 * @property string $statusCode
 * @property string $statusCodeLabel
 * @property integer $couponValue
 * @property string $contract_file_name
 * @property string $customer_phone_number
 * @property string $mode
 * @property string $comment
 * @property string $xml_created_at
 * @property string $xml_content
 * @property string $sr_integration_id
 *
 * @property string $request_client_ip
 * @property string $request_client_agent
 * @property string $request_client_host
 *
 * @property Item[] $items
 * @property Item[] $allItems
 * @property Status $statusModel
 * @property Customer $customer
 * @property Address $billingAddress
 * @property Address $deliveryAddress
 * @property InsuranceOrder $insuranceOrder
 * @property Tariffs|null $firstTariff
 * @property Article|null $firstSmartphone
 * @property ConsolidatedPrice $prices
 * @property CustomerPhoneNumber $customerPhoneNumber
 * @property RequestLog[] $requestLogs
 */
class Order extends ActiveRecord
{
	/**
	 * Map of allowed order status changes in a 2-dimensional array. Dimension 1 being the old status and dimension 2
	 * being the status to be changed to. If the entry exists in this map, the status change is allowed, otherwise
	 * it is not.
	 *
	 * See <a href="https://deinhandy.atlassian.net/wiki/display/STAR/Order+Status">Order Status wiki page</a> for more
	 * details.
	 */
	const STATUS_CHANGE_MAP = [
		Status::AWAITING_PREPAYMENT => [
			Status::RECEIVED,
			Status::DECLINED,
			Status::PAYMENT_FAILED,
		],
		Status::NEW => [
			Status::RECEIVED,
			Status::PAYMENT_INCOMPLETE,
			Status::AWAITING_PREPAYMENT,
            Status::COMMISSIONED,
		],
		Status::RECEIVED => [
			Status::ON_HOLD,
			Status::DECLINED,
			Status::PENDING_PSP_CAPTURE,
			Status::PAYMENT_SUCCESSFUL,
		],
		Status::ON_HOLD => [
			Status::RECEIVED,
		],
		Status::PENDING_PSP_CAPTURE => [
			Status::PAYMENT_FAILED,
			Status::PAYMENT_SUCCESSFUL,
		],
		Status::PAYMENT_SUCCESSFUL => [
			Status::COMMISSIONED,
			Status::SHIPPED,
		],
		Status::COMMISSIONED => [
			Status::SHIPPED,
		],
		Status::PAYMENT_FAILED => [
			Status::OFFERED,
		],
		Status::DECLINED => [
			Status::OFFERED,
		],
	];

	const MODE_ACCESSORY = 'accessory';
	const MODE_HARDWARE = 'hardware';
	const MODE_PORTING = 'porting';
	const MODE_NEW_NUMBER = 'new-number';
	const MODE_RENEWAL = 'renewal';
	const MODE_UNKNOWN = 'unknown';

	const MODE_LABELS = [
		self::MODE_ACCESSORY => 'Zubehör',
		self::MODE_HARDWARE => 'Hardware only',
		self::MODE_PORTING => 'Portierung',
		self::MODE_NEW_NUMBER => 'Neuvertrag',
		self::MODE_RENEWAL => 'Verlängerung',
		self::MODE_UNKNOWN => 'Unbekannt',
	];

	const EVENT_AFTER_COMPLETE_ORDER = 'eventAfterCompleteOrder';
	const EVENT_AFTER_ORDER_STATUS_PAYMENT_SUCCESSFUL = 'eventOrderStatusPaymentSuccessful';
	const EVENT_AFTER_ORDER_STATUS_RECEIVED = 'eventOrderStatusReceived';
	const EVENT_AFTER_ORDER_STATUS_DECLINED = 'eventOrderStatusDeclined';
	const EVENT_AFTER_ORDER_STATUS_OFFERED = 'eventOrderStatusOffered';
	const EVENT_AFTER_ORDER_PLACED_WITH_PAYLOAD = 'eventOrderPlacedWithPayload';
	const EVENT_AFTER_ORDER_TRACKING_IDS_CHANGED = 'eventAfterOrderTrackingIdsChanged';

	const STATUS_EVENT_MAP = [
		Status::RECEIVED => OrderStatusReceivedEvent::class,
		Status::AWAITING_PREPAYMENT => OrderStatusReceivedEvent::class,
		Status::PAYMENT_SUCCESSFUL => OrderStatusPaymentSuccessfulEvent::class,
		Status::DECLINED => OrderStatusDeclinedEvent::class,
		Status::OFFERED => OrderStatusOfferedEvent::class,
	];

	const LIST_XML_NAME = 'orders.xml';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'order';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			//			[
			//				['customer_id', 'delivery_address_id', 'billing_address_id', 'status', 'created_at', 'updated_at'],
			//				'required',
			//			],
			[['customer_id', 'delivery_address_id', 'billing_address_id', 'status'], 'integer'],
			[['price_once', 'price_monthly', 'vat_rate'], 'number'],
			[
				[
					'shipment_tracking_ids',
					'serial_numbers',
					'msisdn',
					'statusCodeLabel',
					'contract_file_name',
					'simcard_number',
					'contract_number',
				],
				'string',
				'max' => 255,
			],
			[['created_at', 'updated_at', 'comment', 'sr_integration_id', 'xml_created_at', 'xml_content'], 'safe'],
			[
				'simcard_number',
				'required',
				'message' => 'Die Simkarten Nummer darf nicht leer sein. Bitte tragen sie diese erst ein und speichern die Bestellung, bevor sie die Bestellung akzeptieren.',
				'when' => function(Order $order) {
					return $order->hasSimcardItem() && $order->status === Status::PENDING_PSP_CAPTURE && $order->getOldAttribute('status') === Status::RECEIVED;
				},
				'whenClient' => 'function($attribute, $value) {
					return false;
				}',
			],
			[
				['contract_number', 'msisdn'],
				'required',
				'message' => 'Die {attribute} darf nicht leer sein. Bitte tragen sie diese erst ein und speichern die Bestellung, bevor sie die Bestellung akzeptieren.',
				'when' => [$this, 'hasTariffAndIsPending'],
				'whenClient' => 'function($attribute, $value) {
					return false;
				}',
			],
			['msisdn', MsisdnValidator::class],
		];
	}

	public function hasTariffAndIsPending(Order $order) {
		return $order->hasTariffItem()
			&& $order->status === Status::PENDING_PSP_CAPTURE
			&& $order->getOldAttribute('status') === Status::RECEIVED;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'customer_id' => Yii::t('app', 'Customer ID'),
			'delivery_address_id' => Yii::t('app', 'Delivery Address ID'),
			'billing_address_id' => Yii::t('app', 'Billing Address ID'),
			'price_once' => Yii::t('app', 'Price Once'),
			'price_monthly' => Yii::t('app', 'Price Monthly'),
			'vat_rate' => Yii::t('app', 'Vat Rate'),
			'status' => Yii::t('app', 'Status'),
			'statusCodeLabel' => Yii::t('app', 'Status'),
			'serial_numbers' => Yii::t('app', 'Serial Number'),
			'MSISDN' => Yii::t('app', 'MSISDN'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'payment_method' => Yii::t('app', 'payment method'),
			'comment' => Yii::t('app', 'Comment'),
		];
	}

	/**
	 * @param $item
	 */
	public function addItem($item)
	{
		$this->items[] = $item;
	}

	/**
	 * This is overriden so we can always hook into the magic $order->status setting calls, when called from outside.
	 *
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set($name, $value)
	{
		if ($name !== 'status' && $this->hasAttribute($name)) {
			$this->setAttribute($name, $value);
		} else {
			// Bypass parent implementation
			Component::__set($name, $value);
		}
	}

	public function fields()
	{
		return [
			'id',
			'customer_id',
			'created_at',
			'payment_method',
			'shipment_tracking_ids' => 'shipmentTrackingIds',
			'status_code_label' => 'statusCodeLabel',
			'items' => 'itemIds',
			'prices',
			'billing_address' => 'billingAddress',
			'delivery_address' => 'deliveryAddress',
			'status',
			'payment_method',
		];
	}

	/**
	 * @param ConsolidatedPrice $prices
	 *
	 * @return $this
	 */
	public function setPrices(ConsolidatedPrice $prices)
	{
		$this->price_once = $prices->getPriceOnce()->getValue();
		$this->price_monthly = $prices->getPriceMonthly()->getValue();

		return $this;
	}

	public function getPrices()
	{
		return new ConsolidatedPrice($this->price_once, $this->price_monthly);
	}

	public function setStatusCodeLabel($statusCodeLabel)
	{
		$this->statusCodeLabel = $statusCodeLabel;
	}

	/**
	 * @param int $newStatus One of Status:: status constants.
	 * @see Status
	 */
	public function setStatus(int $newStatus)
	{
		if ($this->isStatusChangeAllowed($newStatus)) {
			$this->setAttribute('status', $newStatus);
		} else {
			throw new LogicException(
				'Changing the order status from ' . $this->status . ' to ' . $newStatus . ' is not allowed.'
			);
		}
	}

	/**
	 * @param int $newStatus One of Status:: status constants.
	 * @see Status
	 *
	 * @return bool
	 */
	private function isStatusChangeAllowed(int $newStatus)
	{
		// Newly created orders should always receive the status "NEW"
		if (empty($this->status)) {
			return ($newStatus === Status::NEW || $newStatus === Status::AWAITING_PREPAYMENT);
		}

		// Keeping the same status is always allowed
		if ($this->status === $newStatus) {
			return true;
		}

		// Statuses that do not exist are never allowed
		$statusModel = Status::findOne($newStatus);
		if (empty($statusModel)) {
			return false;
		}

		// For final statuses (statuses that are not in the "from" dimension in the map), all changes are disallowed
		if (empty(self::STATUS_CHANGE_MAP[$this->status])) {
			return false;
		}

		// Status allowance mapping is when all general rules above have been processed
		return in_array($newStatus, self::STATUS_CHANGE_MAP[$this->status]);
	}

	public function canBePaused()
	{
		return $this->canChangeToDifferentStatus(Status::ON_HOLD);
	}

	public function canBeResumed()
	{
		return $this->status == Status::ON_HOLD;
	}

	private function canChangeToDifferentStatus(int $newStatus)
	{
		return $this->status != $newStatus && $this->isStatusChangeAllowed($newStatus);
	}

	public function canBeAccepted()
	{
		return $this->canChangeToDifferentStatus(Status::PENDING_PSP_CAPTURE);
	}

	public function canBeDeclined()
	{
		return $this->canChangeToDifferentStatus(Status::DECLINED);
	}

	public function canSerialNumberBeEntered()
	{
		return $this->canChangeToDifferentStatus(Status::SHIPPED);
	}

	public function canBeReceived()
	{
		return $this->payment_method === Method::PREPAYMENT &&
			$this->canChangeToDifferentStatus(Status::RECEIVED) &&
			$this->status != Status::ON_HOLD;
	}

	public function getStatusCode()
	{
		return $this->statusModel->code ?? null;
	}

	public function getStatusCodeLabel()
	{
		$code = $this->statusCode;
		if (empty($code)) {
			return null;
		}
		return Yii::t('app', 'Order Status ' . $code);
	}

	public function getPaymentLabel()
	{
		return Yii::t('app', $this->payment_method);
	}

	public function beforeSave($insert)
	{
		$parentReturn = parent::beforeSave($insert);

		if ($this->isSerialNumberSavedForTheFirstTime()) {
			$this->status = Status::COMMISSIONED;
		}

		if ($this->isShipmentTrackingIdSavedForFirstTime()) {
			$this->status = Status::SHIPPED;
		}


		foreach($this->getItems()->all() as $item) {

		    if($item->isCoupon()) {
		        /**
                 * Is this Coupon only valid one-time?
                 */

		        $coupon = Coupon::findOne($item->item_id);

		        if(!empty($coupon)) {
		            if($coupon->valid_unique == 1) {
		                $coupon->updateAttributes(['is_valid' => 0]);
                    }
                }

            }
        }

		return $parentReturn;
	}

	/**
	 * @param bool $insert
	 * @param array $changedAttributes
	 */
	public function afterSave($insert, $changedAttributes)
	{
		$this->fireCustomEvents($changedAttributes);
	}

	/**
	 * @param $changedAttributes
	 */
	protected function fireCustomEvents($changedAttributes)
	{
		if (isset($changedAttributes['status']) && $changedAttributes['status'] != $this->status) {
			if (in_array($this->status, array_keys(self::STATUS_EVENT_MAP))) {
				$eventClass = self::STATUS_EVENT_MAP[$this->status];
				/** @var AbstractOrderEvent $event */
				$event = new $eventClass;
				$event->setOrder($this);
				dispatch($event);
			}
		}

		if (isset($changedAttributes['shipment_tracking_ids']) && $changedAttributes['shipment_tracking_ids'] != $this->shipment_tracking_ids) {
			$orderTrackingIdsChangedEvent = new OrderTrackingIdsChangedEvent();
			$orderTrackingIdsChangedEvent->setOrder($this);
			dispatch($orderTrackingIdsChangedEvent);
		}
	}

	public function isSerialNumberSavedForTheFirstTime()
	{
		return $this->isAttributeSavedForFirstTime('serial_numbers');
	}

	public function isShipmentTrackingIdSavedForFirstTime()
	{
		return $this->isAttributeSavedForFirstTime('shipment_tracking_ids');
	}

	public function isAttributeSavedForFirstTime($attributeName)
	{
		$oldAttributeValue = $this->getOldAttribute($attributeName);
		return (empty($oldAttributeValue) && empty($this->$attributeName) === false);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getItems()
	{
		return $this->hasMany(Item::className(), ['order_id' => 'id'])
			->andWhere(['!=', 'item_product_type_id', 'article_tariff']);
	}

	public function getItemIds()
	{
		return $this->hasMany(Item::className(), ['order_id' => 'id'])
			->andWhere(['!=', 'item_product_type_id', 'article_tariff'])
			->select('id')->column();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAllItems()
	{
		return $this->hasMany(Item::className(), ['order_id' => 'id']);
	}

	/**
	 * @return Tariffs|null
	 */
	public function getFirstTariff()
	{
		/** @var Item $item */
		foreach ($this->getItems()->all() as $item) {
			$tariff = $item->getTariff();
			if ($tariff) {
				return $tariff;
			}
		}
		return null;
	}

	/**
	 * @return Article|null
	 */
	public function getFirstSmartphone()
	{
		/** @var Item $item */
		foreach ($this->getItems()->all() as $item) {
			$article = $item->getArticle();
			if ($article && $article->product_category_id == ProductCategory::SMARTPHONE) {
				return $article;
			}
		}
		return null;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDeliveryAddress()
	{
		return $this->hasOne(Address::className(), ['id' => 'delivery_address_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBillingAddress()
	{
		return $this->hasOne(Address::className(), ['id' => 'billing_address_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getStatusModel()
	{
		return $this->hasOne(Status::className(), ['id' => 'status']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getInsuranceOrder()
	{
		return $this->hasOne(InsuranceOrder::class, ['order_id' => 'id']);
	}

	public function getSerialNumbers() : array
	{
		return isset($this->serial_numbers) ? explode(',', (string)$this->serial_numbers) : [];
	}

	protected function setSerialNumbers(array $serialNumbers)
	{
		$this->serial_numbers = implode(',', $serialNumbers);
	}

	public function addSerialNumber(string $serialNumber) : bool
	{
		$serialNumbers = $this->getSerialNumbers();
		if (array_search($serialNumber, $serialNumbers) === false) {
			$serialNumbers[] = $serialNumber;
			$this->setSerialNumbers($serialNumbers);
			return true;
		}
		return false;
	}

	public function hasSerialNumber($serialNumber) : bool
	{
		$serialNumbers = $this->getSerialNumbers();
		if (in_array($serialNumber, $serialNumbers)) {
			return true;
		}
		return false;
	}

	public function getShipmentTrackingIds() : array
	{
		return isset($this->shipment_tracking_ids) ? explode(',', (string)$this->shipment_tracking_ids) : [];
	}

	protected function setShipmentTrackingIds(array $trackingIds)
	{
		$this->shipment_tracking_ids = implode(',', $trackingIds);
	}

	public function addShipmentTrackingId(string $trackingId) : bool
	{
		$trackingIds = $this->getShipmentTrackingIds();
		if (array_search($trackingId, $trackingIds) === false) {
			$trackingIds[] = $trackingId;
			$this->setShipmentTrackingIds($trackingIds);
			return true;
		}
		return false;
	}

	public function hasTrackingId($trackingId) : bool
	{
		$trackingIds = $this->getShipmentTrackingIds();
		if (in_array($trackingId, $trackingIds)) {
			return true;
		}
		return false;
	}

	public function hasSimcardItem() : bool
	{
		foreach ($this->items as $item) {
			if ($item->isSimCard()) {
				return true;
			}
		}

		return false;
	}

	public function hasTariffItem() : bool
	{
		foreach ($this->items as $item) {
			if ($item->isTariff()) {
				return true;
			}
		}

		return false;
	}

	public function hasContractFile(): bool
	{
		return !empty($this->contract_file_name);
	}

	public function setCustomerPhoneNumber(array $customerPhoneNumber)
	{
		$this->customer_phone_number = json_encode($customerPhoneNumber);
	}

	public function getCustomerPhoneNumber(): CustomerPhoneNumber
	{
		$attributes = json_decode($this->customer_phone_number, true);
		$customerPhoneNumber = new CustomerPhoneNumber();
		foreach (array_keys($attributes ?? []) as $attributeName) {
			try {
				$customerPhoneNumber->{$attributeName} = $attributes[$attributeName];
			} catch (UnknownPropertyException $e) {
				continue;
			}
		}
		return $customerPhoneNumber;
	}

	public function isAccessoryOnly() : bool
	{
		/** @var Item $item */
		foreach ($this->getItems()->all() as $item) {
			if ($item->isAccessory() === false && $item->item_id != Article::COMMISSION_ARTICLE_ID) {
				return false;
			}
		}

		return true;
	}

	public function isHardwareOnly() : bool
	{
		/** @var Item $item */
		foreach ($this->getItems()->all() as $item) {
			$hardwareCategoryIds = array_merge(
				[ProductCategory::SMARTPHONE, ProductCategory::TABLET, ProductCategory::INSURANCE],
				ProductCategory::ACCESSORIES
			);
			$isNotHardwareOrInsurance = in_array(
				$item->getArticle()->product_category_id ?? null,
				$hardwareCategoryIds
			) === false;
			$isNotCommissionArticle = $item->item_id != Article::COMMISSION_ARTICLE_ID;

			if ($isNotCommissionArticle && ($item->isArticle() === false || $isNotHardwareOrInsurance)) {
				return false;
			}
		}

		return true;
	}

	public function isNewContract() : bool
	{
		return $this->hasTariffWithOfferType(OfferType::NEW_CONTRACT_TYPE);
	}

	public function isPortingOrder() : bool
	{
		return $this->getCustomerPhoneNumber()->getPorting();
	}

	public function isContractRenewal() : bool
	{
		return $this->hasTariffWithOfferType(OfferType::RENEWAL_TYPE);
	}

	public function hasTariffWithOfferType(int $offertype) : bool
	{
		/** @var Item $item */
		foreach ($this->getItems()->all() as $item) {
			if ($item->isTariff() && $item->getTariff()->offer_type_id == $offertype) {
				return true;
			}
		}

		return false;
	}

	public function getMode()
	{
		if ($this->mode) {
			return $this->mode;
		}
		if ($this->isAccessoryOnly()) {
			return self::MODE_ACCESSORY;
		}
		if ($this->isHardwareOnly()) {
			return self::MODE_HARDWARE;
		}
		if ($this->isPortingOrder()) {
			return self::MODE_PORTING;
		}
		if ($this->isNewContract()) {
			return self::MODE_NEW_NUMBER;
		}
		if ($this->isContractRenewal()) {
			return self::MODE_RENEWAL;
		}

		return self::MODE_UNKNOWN;
	}

	public function getFirstname()
	{
		return $this->billingAddress->firstname ?? null;
	}

	public function getLastname()
	{
		return $this->billingAddress->lastname ?? null;
	}

	public function getEmail()
	{
		return $this->customer->email;
	}

	public function getNationalityLabel()
	{
		return DataHelperCountries::getCountries()[strtoupper($this->customer->nationality)] ?? null;
	}

	public function getPassportTypeLabel()
	{
		return PassportTypeInterface::ALL[$this->customer->passport_type] ?? null;
	}

	public function getRequestLogs()
	{
		return $this->hasMany(RequestLog::class, ['order_id' => 'id']);
	}

	public function hasRequestLogs()
	{
		return $this->getRequestLogs()->exists();
	}

	public function isOfferable()
	{
		return in_array($this->status, [
			Status::PAYMENT_FAILED,
			Status::DECLINED,
		]);
	}
}
