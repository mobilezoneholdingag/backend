<?php

namespace common\modules\order\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_status".
 *
 * @property integer $id
 * @property string $code
 * @property Order[] $orders
 */
class Status extends ActiveRecord
{
	const NEW = 1;
	const RECEIVED = 2;
	const ON_HOLD = 3;
	const PENDING_PSP_CAPTURE = 4;
	const PAYMENT_SUCCESSFUL = 5;
	const PAYMENT_INCOMPLETE = 6;
	const DECLINED = 7;
	const PAYMENT_FAILED = 8;
	const COMMISSIONED = 9;
	const SHIPPED = 10;
	const AWAITING_PREPAYMENT = 11;
	const OFFERED = 12;
	const SWISSBILLING_API_ERROR = 501;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'order_status';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['code'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'code' => Yii::t('app', 'Code'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrders()
	{
		return $this->hasMany(Order::className(), ['status' => 'id']);
	}

	public function getLabel()
	{
		return Yii::t('app', 'Order Status ' . $this->code);
	}
}

