<?php

namespace common\modules\order\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AddressSearch represents the model behind the search form about `common\modules\order\models\OrderAddress`.
 */
class AddressSearch extends Address
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'customer_id', 'guid', 'sex', 'contact_phone_number'], 'integer'],
			[
				[
					'firstname',
					'lastname',
					'company',
					'street',
					'house_number',
					'zip',
					'city',
					'country',
					'additional_info',
					'created_at',
					'updated_at',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Address::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
			]
		);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere(
			[
				'id' => $this->id,
				'customer_id' => $this->customer_id,
				'guid' => $this->guid,
				'sex' => $this->sex,
				'contact_phone_number' => $this->contact_phone_number,
				'created_at' => $this->created_at,
				'updated_at' => $this->updated_at,
			]
		);

		$query->andFilterWhere(['like', 'firstname', $this->firstname])
			->andFilterWhere(['like', 'lastname', $this->lastname])
			->andFilterWhere(['like', 'company', $this->company])
			->andFilterWhere(['like', 'street', $this->street])
			->andFilterWhere(['like', 'house_number', $this->house_number])
			->andFilterWhere(['like', 'zip', $this->zip])
			->andFilterWhere(['like', 'city', $this->city])
			->andFilterWhere(['like', 'country', $this->country])
			->andFilterWhere(['like', 'additional_info', $this->additional_info]);

		return $dataProvider;
	}
}
