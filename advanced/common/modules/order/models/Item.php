<?php

namespace common\modules\order\models;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\behaviors\TimestampBehaviorSettings;
use common\modules\pricing\models\ConsolidatedPrice;
use common\models\Coupon;
use common\models\ProductCategory;
use common\models\Tariffs;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_item".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $item_id
 * @property string $item_product_type_id
 * @property integer $parent_item_id
 * @property string $mat_no
 * @property string $title
 * @property string $erp_title
 * @property float $price_once
 * @property float $price_monthly
 * @property string $vat_rate
 * @property string $created_at
 * @property string $updated_at
 * @property Order $order
 * @property Article|Tariffs $item
 * @property Item $parentItem
 * @property Item[] $childItems
 * @property ConsolidatedPrice $prices
 */
class Item extends ActiveRecord
{
	const TYPE_ARTICLE = 'article';
	const TYPE_TARIFF = 'tariff';
	const TYPE_ARTICLE_TARIFF = 'article_tariff';
	const TYPE_SIM_CARD = 'sim_card';
	const TYPE_ACCESSORY = 'accessories';
	const TYPE_COUPON = 'coupon';
	const TYPE_ENTITY_MAPPING = [
		'article' => Article::class,
		'tariff' => Tariffs::class,
		'article_tariff' => ArticleTariffs::class,
		'accessories' => Article::class,
		'sim_card' => Article::class,
		'coupon' => Coupon::class,
	];

	public function fields()
	{
		return [
			'id',
			'item_id',
			'item_product_type_id',
			'mat_no',
			'title',
			'erp_title',
			'vat_rate',
			'prices',
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'order_item';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['order_id', 'item_id', 'parent_item_id'], 'integer'],
			[['item_id', 'item_product_type_id', 'title'], 'required'],
			[['price_once', 'price_monthly', 'vat_rate'], 'number'],
			[['created_at', 'updated_at', 'mat_no', 'erp_title'], 'safe'],
			[['item_product_type_id'], 'string', 'max' => 50],
			[['title', 'mat_no', 'erp_title'], 'string', 'max' => 255],
			[
				['order_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Order::className(),
				'targetAttribute' => ['order_id' => 'id'],
			],
			[
				['parent_item_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Item::className(),
				'targetAttribute' => ['parent_item_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'order_id' => Yii::t('app', 'Order ID'),
			'item_id' => Yii::t('app', 'Item ID'),
			'item_product_type_id' => Yii::t('app', 'Item Product Type ID'),
			'parent_item_id' => Yii::t('app', 'Parent Item ID'),
			'mat_no' => Yii::t('app', 'Mat_No'),
			'title' => Yii::t('app', 'Title'),
			'erp_title' => Yii::t('app', 'ERP Title'),
			'price_once' => Yii::t('app', 'Price Once'),
			'price_monthly' => Yii::t('app', 'Price Monthly'),
			'vat_rate' => Yii::t('app', 'Vat Rate'),
			'status_insurance' => Yii::t('app', 'Insurance Status'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
		];
	}

	/**
	 * @param ConsolidatedPrice $prices
	 *
	 * @return $this
	 */
	public function setPrices(ConsolidatedPrice $prices)
	{
		$this->price_once = $prices->getPriceOnce()->getValue();
		$this->price_monthly = $prices->getPriceMonthly()->getValue();

		return $this;
	}

	/**
	 * @return ConsolidatedPrice
	 */
	public function getPrices()
	{
		return new ConsolidatedPrice($this->price_once, $this->price_monthly);
	}

	/**
	 * @return bool
	 */
	public function isInsurance()
	{
		return $this->item instanceof Article && $this->item->productCategory->id === ProductCategory::INSURANCE;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 * @throws \Exception
	 */
	public function getItem()
	{
		$mapping = self::TYPE_ENTITY_MAPPING;

		if (!isset($mapping[$this->item_product_type_id])) {
			throw new \Exception('Invalid item type: `' . $this->item_product_type_id . '`.');
		}

		return $this->hasOne($mapping[$this->item_product_type_id], ['id' => 'item_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrder()
	{
		return $this->hasOne(Order::className(), ['id' => 'order_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getParentItem()
	{
		return $this->hasOne(Item::className(), ['id' => 'parent_item_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getChildItems()
	{
		return $this->hasMany(Item::className(), ['parent_item_id' => 'id']);
	}

	/**
	 * Tariff model if item contains an tariff; null otherwise.
	 *
	 * @return Tariffs|null
	 */
	public function getTariff()
	{
		if ($this->isTariff()) {
			return Tariffs::findOne($this->item_id);
		}
		return null;
	}

	/**
	 * Article model if item contains an tariff; null otherwise.
	 *
	 * @return Article|null
	 */
	public function getArticle()
	{
		if ($this->isArticle()) {
			return Article::findOne($this->item_id);
		}
		return null;
	}

	public function getSimCardArticle()
	{
		if ($this->isSimCard()) {
			return Article::findOne($this->item_id);
		}
		return null;
	}

	/**
	 * @return bool
	 */
	public function isArticleTariff()
	{
		return $this->item_product_type_id == self::TYPE_ARTICLE_TARIFF;
	}

	/**
	 * @return bool
	 */
	public function isArticle()
	{
		return $this->item_product_type_id == self::TYPE_ARTICLE || $this->isAccessory();
	}

	public function isAccessory()
	{
		return $this->item_product_type_id == self::TYPE_ACCESSORY;
	}

	/**
	 * @return bool
	 */
	public function isTariff()
	{
		return $this->item_product_type_id == self::TYPE_TARIFF;
	}

	public function isSimCard()
	{
		return $this->item_product_type_id == self::TYPE_SIM_CARD;
	}

	public function isCoupon()
	{
		return $this->item_product_type_id == self::TYPE_COUPON;
	}
}
