<?php

namespace common\modules\order\models;

use common\models\Customer;
use common\modules\payment\helper\Method;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrderSearch represents the model behind the search form about `common\modules\order\models\Order`.
 */
class OrderSearch extends Order
{
	public $firstname;
	public $lastname;
	public $email;
	public $paymentLabel;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'customer_id', 'delivery_address_id', 'billing_address_id', 'status'], 'integer'],
			[['price_once', 'price_monthly', 'vat_rate'], 'number'],
			[
				[
					'created_at',
					'updated_at',
					'firstname',
					'lastname',
					'email',
					'statusCodeLabel',
					'paymentLabel',
					'mode',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function getPaymentLabels()
	{
		$labels = [];

		foreach (Method::METHODS as $method) {
			$labels[$method] = \Yii::t('app', $method);
		}

		return $labels;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Order::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
			]
		);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->leftJoin(Address::tableName(), 'billing_address_id = ' . Address::tableName() . '.id');
		$query->leftJoin(Customer::tableName(), Order::tableName() . '.customer_id = ' . Customer::tableName() . '.id');
		$query->leftJoin(Status::tableName(), 'status = ' . Status::tableName() . '.id');

		// grid filtering conditions
		$query->andFilterWhere(
			[
				Order::tableName() . '.id' => $this->id,
				'customer_id' => $this->customer_id,
				'delivery_address_id' => $this->delivery_address_id,
				'billing_address_id' => $this->billing_address_id,
				'price_once' => $this->price_once,
				'price_monthly' => $this->price_monthly,
				'vat_rate' => $this->vat_rate,
				Order::tableName() . '.created_at' => $this->created_at,
				Order::tableName() . '.updated_at' => $this->updated_at,
				'payment_method' => $this->paymentLabel,
				'status' => $this->statusCodeLabel,
				'mode' => $this->mode,
			]
		);

		$query->andFilterWhere(['LIKE', Address::tableName() . '.firstname', $this->firstname]);
		$query->andFilterWhere(['LIKE', Address::tableName() . '.lastname', $this->lastname]);
		$query->andFilterWhere(['LIKE', Customer::tableName() . '.email', $this->email]);

		$query->orderBy(['id' => SORT_DESC]);

		return $dataProvider;
	}
}
