<?php

namespace common\modules\order\models;

use common\modules\insurances\models\ArticleGroupsInsurance;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * ItemSearch represents the model behind the search form about `common\modules\order\models\OrderItem`.
 */
class ItemSearch extends Item
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'order_id', 'item_id', 'parent_item_id'], 'integer'],
			[['item_product_type_id', 'title', 'created_at', 'updated_at'], 'safe'],
			[['price_once', 'price_monthly', 'vat_rate'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Item::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
			]
		);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere(
			[
				'id' => $this->id,
				'order_id' => $this->order_id,
				'item_id' => $this->item_id,
				'parent_item_id' => $this->parent_item_id,
				'price_once' => $this->price_once,
				'price_monthly' => $this->price_monthly,
				'vat_rate' => $this->vat_rate,
				'created_at' => $this->created_at,
				'updated_at' => $this->updated_at,
			]
		);

		$query->andFilterWhere(['like', 'item_product_type_id', $this->item_product_type_id])
			->andFilterWhere(['like', 'title', $this->title]);

		return $dataProvider;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function searchInsurances($params)
	{
		$query = Item::find()
			->leftJoin(ArticleGroupsInsurance::tableName(), 'insurance_id = item_id')
			->where(new Expression(ArticleGroupsInsurance::tableName() . '.insurance_id > 0'))
			->groupBy(Item::tableName() . '.id');

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere(
			[
				'id' => $this->id,
				'order_id' => $this->order_id,
				'item_id' => $this->item_id,
				'parent_item_id' => $this->parent_item_id,
				'price_once' => $this->price_once,
				'price_monthly' => $this->price_monthly,
				'vat_rate' => $this->vat_rate,
				'created_at' => $this->created_at,
				'updated_at' => $this->updated_at,
			]
		);

		$query->andFilterWhere(['like', 'item_product_type_id', $this->item_product_type_id])
			->andFilterWhere(['like', 'title', $this->title]);

		return $dataProvider;
	}
}
