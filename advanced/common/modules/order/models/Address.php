<?php

namespace common\modules\order\models;

use common\models\behaviors\TimestampBehaviorSettings;
use common\models\Customer;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_address".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $guid
 * @property integer $sex
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $street
 * @property string $house_number
 * @property string $zip
 * @property string $city
 * @property string $country
 * @property integer $contact_phone_number
 * @property string $additional_info
 * @property string $created_at
 * @property string $updated_at
 * @property Customer $customer
 */
class Address extends ActiveRecord
{
	const SEX_MALE = 1;
	const SEX_FEMALE = 2;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'order_address';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
			LoggableBehavior::className(),
		];
	}

	public function delete()
	{
		Yii::error('Order addresses should not be deleted!');

		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['customer_id'], 'required'],
			[['customer_id', 'guid', 'sex', 'contact_phone_number'], 'integer'],
			[['created_at', 'updated_at'], 'safe'],
			[
				[
					'firstname',
					'lastname',
					'company',
					'street',
					'house_number',
					'zip',
					'city',
					'country',
					'additional_info',
				],
				'string',
				'max' => 255,
			],
			[
				['customer_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Customer::className(),
				'targetAttribute' => ['customer_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'customer_id' => Yii::t('app', 'Customer ID'),
			'guid' => Yii::t('app', 'Guid'),
			'sex' => Yii::t('app', 'Gender'),
			'firstname' => Yii::t('app', 'Firstname'),
			'lastname' => Yii::t('app', 'Lastname'),
			'company' => Yii::t('app', 'Company'),
			'street' => Yii::t('app', 'Street'),
			'house_number' => Yii::t('app', 'House Number'),
			'zip' => Yii::t('app', 'Zip'),
			'city' => Yii::t('app', 'City'),
			'country' => Yii::t('app', 'Country'),
			'contact_phone_number' => Yii::t('app', 'Contact Phone Number'),
			'additional_info' => Yii::t('app', 'Additional Info'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'sexLabel' => Yii::t('app', 'Gender'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}

	public function getSexLabel()
	{
		switch ($this->sex) {
			case self::SEX_MALE:
				return Yii::t('app', "Male");
			case self::SEX_FEMALE:
				return Yii::t('app', "Female");
			default:
				return null;
		}
	}
}
