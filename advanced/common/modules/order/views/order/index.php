<?php

use common\modules\order\models\Order;
use common\modules\order\models\Status;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\order\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $fileUpdatedAt string */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">
	<?php if (Yii::$app->user->can('editor')): ?>
		<?php if ($fileUpdatedAt): ?>
			<p>
				File updated at: <?= $fileUpdatedAt ?>
			</p>
		<?php endif; ?>
		<p>
			<?= Html::a(Yii::t('app', 'Export orders.xml'), ['export-order-list'], ['class' => 'btn btn-primary']) ?>
			<?php if ($fileUpdatedAt): ?>
				<?= Html::a(Yii::t('app', 'Download orders.xml'), ['download-order-list'], ['class' => 'btn btn-success']) ?>
			<?php endif; ?>
		</p>
	<?php endif; ?>
	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				'id',
				'firstname',
				'lastname',
				'email',
				'statusCodeLabel' => [
					'attribute' => 'statusCodeLabel',
					'filter' => \yii\helpers\ArrayHelper::map(Status::find()->all(), 'id', 'label')
				],
				[
					'class' => 'yii\grid\DataColumn',
					'attribute' => 'paymentLabel',
					'filter' => $searchModel->getPaymentLabels(),
				],
				'mode' => [
					'attribute' => 'mode',
					'filter' => Order::MODE_LABELS,
					'value' => function(Order $order) {
						return Order::MODE_LABELS[$order->mode ?? Order::MODE_UNKNOWN];
					}
				],
				'created_at:datetime',
				'updated_at:datetime',
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{view} {update} {download}',
					'buttons' => [
						'download' => function($url, Order $order) {
							return Html::a(
									'<span class="glyphicon glyphicon-download-alt"></span>',
									Url::to(['download-order-xml', 'id' => $order->id]),
									['title' => Yii::t('app', 'Download XML'),]
							);
						},
					],
				],
			],
		]
	); ?>
</div>
