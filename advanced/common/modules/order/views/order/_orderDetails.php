<?php

/** @var $model Order */

use common\models\Customer;
use common\models\CustomerPhoneNumber;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

?>

<fieldset style="background-color: #fcfcfc;">
	<legend>Kunde</legend>
	<div class="row">
		<div class="col-md-6">
			<?= DetailView::widget(
				[
					'model' => $model->customer,
					'attributes' => [
						[
							'attribute' => 'id',
							'format' => 'raw',
							'value' => getCustomerLink($model->customer),
						],
						'email',
						'language',
						'source',
					],
				]
			) ?>
		</div>
		<div class="col-md-6">
			<?= DetailView::widget(
				[
					'model' => $model->customer,
					'attributes' => [
						[
							'attribute' => 'nationality',
							'value' => $model->getNationalityLabel(),
						],
						[
							'attribute' => 'passport_type',
							'value' => $model->getPassportTypeLabel(),
						],
						'passport_id',
						'birthday',
					],
				]
			) ?>
		</div>
	</div>
</fieldset>

<fieldset style="background-color: #fcfcfc;">
	<legend>Adressen</legend>

	<?php
	if ($model->billing_address_id == $model->delivery_address_id) {
		echo '<p>(Identische Liefer- und Rechnungsadresse.)</p>';
	} else {
		echo '<p>(Abweichende Liefer- und Rechnungsadresse!)</p>';
	}
	?>

	<div class="row">
		<?php foreach (
			[
				'Rechnungsadresse' => ['relation' => 'billingAddress', 'id' => 'billing_address_id'],
				'Lieferadresse' => ['relation' => 'deliveryAddress', 'id' => 'delivery_address_id'],
			] as $title => $properties
		) {
			if (!$model->{$properties['relation']}) {
				echo
					"<div class='alert alert-danger'>" .
						"{$title} mit der id #{$model->{$properties['id']}} existiert nicht!" .
					"</div>";

				continue;
			}
			?>
			<div class="col-md-6">
				<p><?= $title ?>:</p>
				<?= DetailView::widget(
					[
						'model' => $model->{$properties['relation']},
						'attributes' => [
							'id',
							'sexLabel',
							'firstname',
							'lastname',
							'street',
							'house_number',
							'zip',
							'city',
							'contact_phone_number',
							'created_at:datetime',
							'updated_at:datetime',
						],
					]
				) ?>
			</div>
		<?php } ?>
	</div>
</fieldset>

<fieldset style="background-color: #fcfcfc;">
	<legend>Bestellte Artikel</legend>
	<div class="row">
		<div class="col-md-12">
			<?= GridView::widget(
				[
					'dataProvider' => new ActiveDataProvider([
						'query' => $model->getAllItems(),
					]),
					'columns' => [
						'id',
						'item_product_type_id',
						'parent_item_id',
						'item_id',
						'mat_no',
						[
							'label' => 'Title',
							'value' => function(Item $item) {
								return $item->isTariff() ? $item->erp_title : $item->title;
							}
						],
						'price_once',
						'price_monthly',
						'vat_rate',
						'created_at:datetime',
						'updated_at:datetime',
						[
							'class' => 'yii\grid\ActionColumn',
							'template' => $model->isOfferable() ? '{delete}' : '',
							'buttons' => [
								'delete' => function ($url, $model, $key) {
									return Html::a(
										Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
										Url::to(['/order/order/delete-order-item', 'id' => $model->id]),
										[
											'title' => 'Löschen',
											'aria-label' => 'Löschen',
											'data-confirm' => 'Wollen Sie diesen Eintrag wirklich löschen?',
											'data-method' => 'post',
										]
									);
								},
							]
						]
					],
				]
			); ?>

			<?php if ($model->isOfferable()) : ?>
				<div class="form-inline">
					<div class="form-group">
						<label class="radio-inline">
							<?= Html::radio('order-item-type', true, ['value' => Item::TYPE_ARTICLE]) ?> Artikel
						</label>
						<label class="radio-inline">
							<?= Html::radio('order-item-type', false, ['value' => Item::TYPE_TARIFF]) ?> Tarif
						</label>
						<label class="radio-inline">
							<?= Html::radio('order-item-type', false, ['value' => Item::TYPE_ARTICLE_TARIFF]) ?>
							Artikeltarif
						</label>
					</div>
					<div class="form-group">
						<?= Html::input(
							'text',
							'order-item-id',
							null,
							['placeholder' => 'Artikel-/Tarif-ID', 'class' => 'form-control']
						) ?>
					</div>
					<div class="form-group">
						<?= Html::button(
							Html::tag('span', '', ['class' => 'glyphicon glyphicon-plus']),
							['type' => 'submit', 'style' => 'font-size:20px;']
						)
						?>
					</div>
					<a href="<?= Url::to(['/order/order/offer-order', 'id' => $model->id]) ?>" class="btn btn-primary pull-right">
						Angebot abschicken
					</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</fieldset>
<?php if ($model->isPortingOrder()) : ?>
	<fieldset style="background-color: #fcfcfc;">
		<legend>Rufnummerportierung</legend>
		<div class="row">
			<div class="col-md-12">
				<?php
					$dataProvider = new \yii\data\ArrayDataProvider([
						'allModels' => [$model->customerPhoneNumber],
					]);
					echo GridView::widget([
						'dataProvider' => $dataProvider,
						'columns' => [
							'number',
							'providerTitle',
							'contractType',
							[
								'attribute' => 'wait_until_contract_ends',
								'format' => 'text',
								'value' => function(CustomerPhoneNumber $phoneNumber){
									return is_null($phoneNumber->wait_until_contract_ends)
										? null : ($phoneNumber->wait_until_contract_ends ? 'Vertragsende' : 'Sofort');
								}
							],
						],
					]);
				?>
			</div>
		</div>
	</fieldset>
<?php endif; ?>

<?php
function getCustomerLink($customer)
{
	if (isset($customer->id)) {
		/* @var Customer $customer */
		$customerNameAndId = ' (' . $customer->first_name . ' ' . $customer->last_name . ')';

		return $customer->id . Html::a(
				$customerNameAndId,
				Url::to(['/customer/view', 'id' => $customer->id]),
				['target' => '_blank']
			);
	}

	return null;
}

?>
