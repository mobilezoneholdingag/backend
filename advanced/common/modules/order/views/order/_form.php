<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$_dis = ['disabled' => 'disabled'];

/* @var $this yii\web\View */
/* @var $model common\modules\order\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload backend\models\content\file\PdfUpload */

?>

<div class="order-form">

	<?= $this->render('_statusButtonsForm', ['model' => $model]) ?>

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<?= Html::submitButton(
			Yii::t('app', 'Save'),
			[
				'class' => 'btn btn-primary',
				'onclick' => 'return confirmSerialNumber();',
			]
		) ?>
	</div>

	<?= $form->field($model, 'comment')->textarea() ?>

	<input type="hidden" name="Order[id]" value="<?= $model->id ?>"/>

	<fieldset style="background-color: #fcfcfc;">
		<legend>Bestellung [ID: <?php echo $model->id ?>]</legend>
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'mode')->input('text', $_dis) ?>
				<?= $form->field($model, 'statusCodeLabel')->input('text', $_dis) ?>
				<?= $form->field($model, 'id')->textInput($_dis) ?>
				<?= $form->field($model, 'created_at')->textInput($_dis) ?>
				<?= $form->field($model, 'updated_at')->textInput($_dis) ?>
				<?= $form->field($model, 'shipment_tracking_ids')->textInput($_dis) ?>
			</div>
			<div class="col-md-6">
				<?= $form->field($model, 'paymentLabel')->input('text', $_dis) ?>
				<?= $form->field($model, 'price_once')->textInput($_dis) ?>
				<?= $form->field($model, 'price_monthly')->textInput($_dis) ?>
				<?= $form->field($model, 'vat_rate')->textInput($_dis) ?>
				<?= $form->field($model, 'sr_integration_id')->textInput($_dis) ?>
			</div>
		</div>
	</fieldset>

	<?= $this->render('_orderDetails', ['model' => $model]) ?>

	<?= $this->render('_apiLogs', ['model' => $model]) ?>

	<fieldset style="background-color: #fcfcfc;">
		<div class="row">
			<div class="col-md-6">
				<?php
				echo $form->field($model, 'serial_numbers')->textInput($_dis);

				if ($model->hasSimcardItem()) {
					echo $form->field($model, 'simcard_number')->textInput(empty($model->simcard_number) ? [] : $_dis);
				}
				if ($model->hasTariffItem()) {
					echo $form->field($model, 'contract_number')->textInput(empty($model->contract_number) ? [] : $_dis);
					echo $form->field($model, 'msisdn')->textInput(empty($model->msisdn) ? [] : $_dis);
				}
				?>
			</div>
		</div>
	</fieldset>

	<script>
		function confirmSerialNumber() {
			var $serialNumberInput = $('#order-serial_number');
			if ($serialNumberInput.length == 0) {
				return true;
			}
			if ($serialNumberInput.val() && $serialNumberInput.prop('disabled') == false) {
				return confirm(
					'Sind Sie sicher dass Sie die Seriennummer speichern möchten? Die Bestellung wird dadurch als ' +
					'VERSENDET markiert. Dies lässt sich NICHT rückgängig machen!'
				);
			}
			return true;
		}
	</script>

	<div class="form-group">
		<?= Html::submitButton(
			Yii::t('app', 'Save'),
			[
				'class' => 'btn btn-primary',
				'onclick' => 'return confirmSerialNumber();',
			]
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

	<?= $this->render('_uploadedFileForm', ['model' => $model, 'upload' => $upload]) ?>

	<?= $this->render('_statusButtonsForm', ['model' => $model]) ?>


</div>
