<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\order\models\Order */
/* @var $upload backend\models\content\file\PdfUpload */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	</p>

    <fieldset style="background-color: #fcfcfc;">
        <legend>Bestellung [ID: <?php echo $model->id ?>]</legend>
        <div class="row">
	        <div class="col-md-6">
				<?= DetailView::widget(
					[
						'model' => $model,
						'attributes' => [
							'comment',
						],
					]
				) ?>
	        </div>

            <div class="col-md-6">

                <?= DetailView::widget(
                    [
                        'model' => $model,
                        'attributes' => [
                            [
                                'label' => 'IP Adresse',
                                'attribute' => 'request_client_ip',
                                'format' => 'raw',
                                'value' => ($model->request_client_ip == "NOT_DEFINED") ? $model->request_client_ip : Html::a($model->request_client_ip, 'https://www.maxmind.com/de/geoip2-precision-demo?ip=' . $model->request_client_ip, ['target' => '_blank']),
                            ],
                            [
                                'label' => 'User Agent',
                                'attribute' => 'request_client_agent',
                            ],
                            [
                                'label' => 'Host',
                                'value' => call_user_func(function ($data) {
                                    try {
                                        return gethostbyaddr($data->request_client_ip);
                                    } catch(Exception $e) {
                                        return $data->request_client_ip;
                                    }
                                }, $model)
                            ]
                        ],
                    ]
                ) ?>
            </div>

            <div class="col-md-6">
				<?= DetailView::widget(
					[
						'model' => $model,
						'attributes' => [
							'mode',
							'statusCodeLabel',
							'id',
							'created_at:datetime',
							'updated_at:datetime',
							'shipment_tracking_ids',
						],
					]
				) ?>
            </div>
            <div class="col-md-6">
				<?= DetailView::widget(
					[
						'model' => $model,
						'attributes' => [
							[
								'attribute' => 'payment_method',
								'value' => Yii::t('app', $model->payment_method),
							],
							'price_once',
							'price_monthly',
							'vat_rate',
							'sr_integration_id',
						],
					]
				) ?>
            </div>
        </div>
    </fieldset>

	<?= $this->render('_orderDetails', ['model' => $model]) ?>

	<?= $this->render('_apiLogs', ['model' => $model]) ?>

	<?= $this->render('_uploadedFileForm', ['model' => $model, 'upload' => $upload]) ?>
</div>
