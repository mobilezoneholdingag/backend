<?php

use common\modules\order\models\Order;
use common\modules\payment\helper\Method;
use common\modules\postfinance\models\EcomForm;
use common\modules\swissbilling\models\SwissBillingApiLog;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var Order $model */

$swissbillingLogQuery = SwissBillingApiLog::find()->where(['order_id' => $model->id]);
$postFinanceLogQuery = EcomForm::find()->where(['order_id' => $model->id]);
?>

<?php if ($model->payment_method === Method::ACCOUNT_PURCHASE && $swissbillingLogQuery->exists()): ?>
	<fieldset style="background-color: #fcfcfc;">
		<legend>API-Calls (Swissbilling)</legend>
		<div class="row">
			<div class="col-md-12">
				<?= GridView::widget(
					[
						'dataProvider' => new ActiveDataProvider([
							'query' => SwissBillingApiLog::find()->where(['order_id' => $model->id]),
						]),
						'columns' => [
							'id',
							'created_at:datetime',
							'url',
							'method',
							'execution_time',
							'request:readable',
							'response:readable',
							'response_headers',
						],
					]
				); ?>
			</div>
		</div>
	</fieldset>
<?php elseif (in_array($model->payment_method, Method::POSTFINANCE_METHODS) && $postFinanceLogQuery->exists()): ?>
	<fieldset style="background-color: #fcfcfc;">
		<legend>API-Calls (PostFinance)</legend>
		<div class="row">
			<div class="col-md-12">
				<?= GridView::widget(
					[
						'dataProvider' => new ActiveDataProvider([
							'query' => EcomForm::find()->where(['order_id' => $model->id]),
						]),
						'columns' => [
							'id',
							'created_at:datetime',
							'ecomForm:readable',
							'ecomCallback:readable',
							'maintenanceRequest:readable',
							'maintenanceResponse:readable',
						],
					]
				); ?>
			</div>
		</div>
	</fieldset>
<?php endif; ?>
<?php if ($model->hasRequestLogs()): ?>
	<fieldset style="background-color: #fcfcfc;">
		<legend>API-Calls (Swisscom)</legend>
		<div class="row">
			<div class="col-md-12">
				<?= GridView::widget(
					[
						'dataProvider' => new ActiveDataProvider([
							'query' => $model->getRequestLogs(),
						]),
						'columns' => [
							'id',
							'created_at:datetime',
							'endpoint',
							'method',
							'request_options:readable',
							'responseLog.created_at:datetime',
							'responseLog.response:readable',
						],
					]
				); ?>
			</div>
		</div>
	</fieldset>
<?php endif; ?>
