<?php

/** @var $model Order */

use common\modules\order\models\Order;
use yii\helpers\Html;

?>

<?= Html::beginForm(['order/update-status']); ?>
<?= Html::hiddenInput('order_id', $model->id); ?>

<?php if ($model->canBePaused()) { ?>
	<?= Html::submitButton(
		Yii::t('app', 'Pause'),
		[
			'class' => 'btn btn-primary order-status-button',
			'name' => 'order_status',
			'value' => 'pause',
			'onclick' => 'return confirm("Sind Sie sicher, dass Sie die Bestellung PAUSIEREN möchten? ' .
				'Dies lässt sich NICHT rückgängig machen!");',
		]
	) ?>
<?php } ?>

<?php
	if ($model->canBeResumed()) {
		echo Html::submitButton(
			Yii::t('app', 'Resume'),
			[
				'class' => 'btn btn-primary order-status-button',
				'name' => 'order_status',
				'value' => 'resume',
			]
		);
	}
?>

<?php if ($model->canBeAccepted()) { ?>
	<?= Html::submitButton(
		Yii::t('app', 'Accept'),
		[
			'class' => 'btn btn-success order-status-button',
			'name' => 'order_status',
			'value' => 'accept',
			'onclick' => 'return confirm("Sind Sie sicher, dass Sie die Bestellung AKZEPTIEREN möchten? ' .
				'Dies lässt sich NICHT rückgängig machen!");',
		]
	) ?>
<?php } ?>

<?php if ($model->canBeReceived()) { ?>
	<?= Html::submitButton(
		Yii::t('app', 'Zahlung bestätigen'),
		[
			'class' => 'btn btn-success order-status-button',
			'name' => 'order_status',
			'value' => 'received',
			'onclick' => 'return confirm("Sind Sie sicher, dass Sie die Zahlung eingetroffen ist? ' .
				'Dies lässt sich NICHT rückgängig machen!");',
		]
	) ?>
<?php } ?>

<?php if ($model->canBeDeclined()) { ?>
	<?= Html::submitButton(
		Yii::t('app', 'Decline'),
		[
			'class' => 'btn btn-danger order-status-button',
			'name' => 'order_status',
			'value' => 'decline',
			'onclick' => 'return confirm("Sind Sie sicher, dass Sie die Bestellung ABLEHNEN möchten? ' .
				'Dies lässt sich NICHT rückgängig machen!");',
		]
	) ?>
<?php } ?>

<?= Html::endForm(); ?>

<br />
