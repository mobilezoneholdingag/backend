<?php

use common\modules\order\models\Order;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var $model Order */
?>

<?php
    $form = ActiveForm::begin(['action' => ['order/upload-file'], 'method' => 'post']);
    echo Html::hiddenInput('order_id', $model->id);
?>

<div class="row">
    <div class="col-md-6">
        <fieldset style="background-color: #fcfcfc;">
            <legend>Vertragsunterlagen</legend>
			<?php
                if ($model->hasContractFile()) {
                    echo Html::a(
                        '<span class="glyphicon glyphicon-file"></span> ' . Yii::t('app', 'Vertrag anzeigen'),
                        Url::to(['/order/order/contractfile', 'id' => $model->id]),
                        ['target' => '_blank']
                    );
                } elseif (Yii::$app->controller->action->id == 'update') {
                    echo isset($upload) ? $form->field($upload, 'file')->fileInput() : null;
                    echo Html::submitButton(Yii::t('app', 'Datei hochladen'), ['class' => 'btn btn-primary']);
                } else {
	                echo 'Keine Datei vorhanden. Zum uploaden ' .
		                Html::a(
			                'HIER',
			                Url::to(['/order/order/update', 'id' => $model->id])
		                ) .
		                ' klicken.';
                }
			?>
        </fieldset>
    </div>
</div>

<?php ActiveForm::end() ?>

<br/>
