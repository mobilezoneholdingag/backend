<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\order\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-item-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'order_id')->textInput() ?>

	<?= $form->field($model, 'item_id')->textInput() ?>

	<?= $form->field($model, 'item_product_type_id')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'parent_item_id')->textInput() ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'price_once')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'price_monthly')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'vat_rate')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'created_at')->textInput() ?>

	<?= $form->field($model, 'updated_at')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
