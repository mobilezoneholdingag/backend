<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\order\models\ItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-item-search">

	<?php $form = ActiveForm::begin(
		[
			'action' => ['index'],
			'method' => 'get',
		]
	); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'order_id') ?>

	<?= $form->field($model, 'item_id') ?>

	<?= $form->field($model, 'item_product_type_id') ?>

	<?= $form->field($model, 'parent_item_id') ?>

	<?php // echo $form->field($model, 'title') ?>

	<?php // echo $form->field($model, 'price_once') ?>

	<?php // echo $form->field($model, 'price_monthly') ?>

	<?php // echo $form->field($model, 'vat_rate') ?>

	<?php // echo $form->field($model, 'created_at') ?>

	<?php // echo $form->field($model, 'updated_at') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
