<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\order\models\Item */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Order Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-item-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(
			Yii::t('app', 'Delete'),
			['delete', 'id' => $model->id],
			[
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
					'method' => 'post',
				],
			]
		) ?>
	</p>

	<?= DetailView::widget(
		[
			'model' => $model,
			'attributes' => [
				'id',
				'order_id',
				'item_id',
				'item_product_type_id',
				'parent_item_id',
				'title',
				'price_once',
				'price_monthly',
				'vat_rate',
				'created_at:datetime',
				'updated_at:datetime',
			],
		]
	) ?>

</div>
