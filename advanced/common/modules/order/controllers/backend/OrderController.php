<?php

namespace common\modules\order\controllers\backend;

use common\modules\mobilezoneErp\lib\OrderDecorator;
use common\modules\order\factories\OrderItemFactory;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use common\modules\order\models\OrderSearch;
use common\modules\order\models\Status;
use common\modules\payment\helper\Method;
use components\filesystem\ExportService;
use LogicException;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => [
							'update',
							'update-status',
							'upload-file',
							'contractfile',
							'download-order-xml',
							'export-order-list',
							'download-order-list',
							'delete-order-item',
							'offer-order',
						],
						'allow' => true,
						'roles' => ['admin', 'editor', 'backoffice'],
					],
					[
						'actions' => ['create', 'delete'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * Lists all Order models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new OrderSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render(
			'index',
			[
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'fileUpdatedAt' => di(ExportService::class)->getFileUpdatedAt(Order::LIST_XML_NAME),
			]
		);
	}

	/**
	 * Displays a single Order model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render(
			'view',
			[
				'model' => $this->findModel($id),
				'upload' => di('pdfUpload.contract')
			]
		);
	}

	/**
	 * Finds the Order model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Order the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Order::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * Creates a new Order model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Order();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render(
				'create',
				[
					'model' => $model,
				]
			);
		}
	}

	/**
	 * Updates an existing Order model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$order = $this->findModel($id);

		$request = Yii::$app->request;
		if ($orderItemId = $request->post('order-item-id')) {
			$this->addOrderItem($order, $orderItemId, $request->post('order-item-type', Item::TYPE_ARTICLE));

			return $this->render('update', ['model' => $order, 'upload' => di('pdfUpload.contract')]);
		}

		if ($order->load($request->post()) && $order->save()) {
			return $this->redirect(['view', 'id' => $order->id]);
		} else {
			return $this->render('update', ['model' => $order, 'upload' => di('pdfUpload.contract')]);
		}
	}

	private function addOrderItem($order, $itemId, $itemType)
	{
		/** @var OrderItemFactory $orderItemFactory */
		$orderItemFactory = di(OrderItemFactory::class);
		$orderItemFactory->addItem($order, $itemId, $itemType);
	}

	public function actionUpdateStatus()
	{
		$orderId = Yii::$app->request->post('order_id');
		$order = Order::findOne($orderId);

		$this->setOrderStatus($order);

		$order->save();
		if ($order->hasErrors()) {
			foreach ($order->getErrors() as $attribute => $errors) {
				foreach ($errors as $error) {
					Yii::$app->session->addFlash('danger', $error);
				}
			}
		}
		$this->redirect(['/order/order/update', 'id' => $orderId]);
	}

	private function setOrderStatus(Order &$order)
	{
		switch (Yii::$app->request->post('order_status')) {
			case 'pause':
				$order->status = Status::ON_HOLD;
				break;
			case 'resume':
				$order->status = Status::RECEIVED;
				break;
			case 'accept':
				if ($order->price_once == 0 || $order->payment_method === Method::PREPAYMENT) {
					$order->status = Status::PAYMENT_SUCCESSFUL;
				} else {
					$order->status = Status::PENDING_PSP_CAPTURE;
				}
				break;
			case 'decline':
				$order->status = Status::DECLINED;
				break;
			case 'received':
				$order->status = Status::RECEIVED;
				break;
			default:
				throw new LogicException('Order status update action is not known.');
		}
	}

	/**
	 * Deletes an existing Order model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	public function actionUploadFile()
	{
		$request = Yii::$app->request;
		$orderId = $request->post('order_id');
		$order = Order::findOne($orderId);
		$upload = di('pdfUpload.contract');

		if (!$order) {
			Yii::$app->session->addFlash('danger', 'No matching order found!');
			$this->redirect(['order/update', 'id' => $orderId]);
		}

		$upload->file = UploadedFile::getInstance($upload, 'file');

		if ($upload->upload()) {
			$order->contract_file_name = $upload->remoteFileName;

			if (!$order->save()) {
				Yii::$app->session->addFlash(
					'danger',
					'FileUpload not working properly! Please try again. If problem persists please contact support.'
				);
			}
			Yii::$app->session->addFlash('success', 'Your file has been uploaded successfully.');
		} else {
			$error = [];
			foreach ($upload->getErrors() as $errors) {
				$error[] = implode('<br />', array_values($errors));
			}
			Yii::$app->session->addFlash('danger', implode('<br />', $error));
		}

		$this->redirect(['order/update', 'id' => $orderId]);
	}

	public function actionContractfile($id)
	{
		$order = Order::find()->select(['contract_file_name'])->where(['id' => $id])->asArray()->one();
		$fileName = $order['contract_file_name'] ?? null;

		if (empty($fileName)) {
			Yii::$app->session->addFlash(
				'danger',
				'Currently no file available!'
			);
			return $this->redirect(['order/view', 'id' => $id]);
		}

		$response = Yii::$app->response->sendContentAsFile(
			di('pdfUpload.contract')->download($fileName),
			basename($fileName),
			['inline' => true, 'mimeType' => 'application/pdf']
		);
		$response->send();

		return $response;
	}

	public function actionDownloadOrderXml($id)
	{
		$order = Order::findOne($id);
		$xml = (new OrderDecorator($order))->decorate();

		$response = Yii::$app->response;
		$response->format = Response::FORMAT_XML;

		return $response->sendContentAsFile($xml, 'Order_MZ_' . $order->id . '.xml');
	}

	public function actionExportOrderList()
	{
		di(ExportService::class)->start('mobilezone-erp/export-order-list-xml-to-filesystem');

		return $this->redirect('index');
	}

	public function actionDownloadOrderList()
	{
		return di(ExportService::class)->download(Order::LIST_XML_NAME);
	}

	public function actionDeleteOrderItem($id)
	{
		$item = Item::findOne($id);
		$order = $item->order;

		if ($this->isOrderIsOfferable($order)) {
			if ($item->childItems) {
				foreach ($item->childItems as $item) {
					$item->delete();
				}
			}
			$item->delete();
		}

		return $this->redirect(['/order/order/update', 'id' => $order->id]);
	}

	public function actionOfferOrder($id)
	{
		$order = Order::findOne($id);

		if ($this->isOrderIsOfferable($order)) {
			$order->status = Status::OFFERED;
			$order->save();
		}

		return $this->redirect(['/order/order/update', 'id' => $order->id]);
	}

	private function isOrderIsOfferable(Order $order): bool
	{
		if (!$order->isOfferable()) {
			$session = \Yii::$app->session;
			$session->addFlash('danger', 'Die Order ist nicht bearbeitbar!');

			return false;
		} else {
			return true;
		}
	}
}
