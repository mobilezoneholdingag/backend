<?php

namespace common\modules\order\controllers\frontend;

use common\modules\order\models\Item;
use yii\rest\ActiveController;

/**
 * @package frontend\controllers
 */
class ItemController extends ActiveController
{
	public $modelClass = Item::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update'], $actions['index']);

		return $actions;
	}
}
