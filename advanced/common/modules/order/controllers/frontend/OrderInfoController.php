<?php

namespace common\modules\order\controllers\frontend;

use common\modules\order\models\Order;
use common\modules\order\actions\IndexAction;
use common\modules\order\actions\ViewAction;
use Yii;
use yii\rest\ActiveController;

/**
 * Renamed from 'OrderController' to 'OrderInfoController' to avoid collision with order module.
 *
 * @package frontend\controllers
 */
class OrderInfoController extends ActiveController
{
	public $modelClass = Order::class;

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disables actions
		unset($actions['delete'], $actions['create'], $actions['update'], $actions['options']);

		// Custom index action
		$actions['index']['class'] = IndexAction::class;
		$actions['view']['class'] = ViewAction::class;

		return $actions;
	}

	public function actionOrderItems()
	{
		$data = Yii::$app->request->get();

		/**
		 * @var $orderItems Order
		 */
		$orderItems = Order::find()->where(['customer_id' => $data['customer_id']])->andWhere(['id' => $data['orderID']])->one();
		$items = $orderItems->items;

		$result = [];
		for ($i = 0; $i < sizeof($items); $i++) {
			$result[$i] = $items[$i]->getItem()->all();
		}

		if ($orderItems == null) {
			return ["statusCode" => 404];
		}

		return $result;
	}
}
