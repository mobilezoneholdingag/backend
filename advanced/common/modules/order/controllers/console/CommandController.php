<?php

namespace common\modules\order\controllers\console;

use common\modules\order\models\Order;
use common\modules\order\models\Status;
use common\modules\payment\helper\Method;
use common\modules\swissbilling\facades\InvoiceFacade;
use Exception;
use LogicException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Manage Orders asynchronous tasks.
 *
 * @package frontend\controllers
 */
class CommandController extends Controller
{
	const ALREADY_ACKNOWLEDGED_ERROR = 'Invalid transaction status (Acknowledged)';

	public function actionCapturePendingOrders()
	{
		$orders = Order::findAll(['status' => Status::PENDING_PSP_CAPTURE]);

		$this->info('Starting to capture a total of ' . count($orders) . ' payments for orders.');

		foreach ($orders as $order) {
			$this->capturePayment($order);
		}

		$this->info('Order payment capture processing completed.');
	}

	private function capturePayment(Order $order)
	{
		$this->info('Now handling order #' . $order->id . '.');

		switch ($order->payment_method) {
			case Method::ACCOUNT_PURCHASE:
				$this->info('Payment type is a SwissBilling type.');

				$response = (new InvoiceFacade())->actionAcknowledge($order->id);

				if ($response['success']) {
					$this->info('Confirmed order successfully.');
					$order->status = Status::PAYMENT_SUCCESSFUL;
				} elseif(isset($response['error'])) {
					if (strpos($response['error'], self::ALREADY_ACKNOWLEDGED_ERROR) !== false) {
						$this->warning('Order already acknowledged!');
						$order->status = Status::PAYMENT_SUCCESSFUL;
						break;
					}

					if($response['error'] == Status::SWISSBILLING_API_ERROR) {
					    $this->warning('Order has Swissbilling Status Error');
					    break;
                    }

					$this->error($response['error']);
					$order->status = Status::PAYMENT_FAILED;
				} else {
					$this->error(
						'ConfirmationRequest for order #' . $order->id . ' failed due to unknown error - data stored in DB.'
					);
					$order->status = Status::PAYMENT_FAILED;
				}

				break;
			case Method::CREDIT_CARD:
			case Method::POSTFINANCE_E_FINANCE:
			case Method::POSTFINANCE_CARD:
				$this->info('Payment type is a PostFinance type.');

				try {
					Yii::$app->runAction('postfinance/command/capture', [$order->id]);
					$order->status = Status::PAYMENT_SUCCESSFUL;
				} catch (Exception $e) {
					$this->error('PostFinance capture for order #' . $order->id . ' failed:' . $e->getMessage());
					$order->status = Status::PAYMENT_FAILED;
				}

				break;
			case Method::NO_PAYMENT_REQUIRED:
				$order->status = Status::PAYMENT_SUCCESSFUL;

				break;
			default:
				throw new LogicException(
					'Payment method "' . $order->payment_method . '" autorization can not be processed.'
				);
		}

		$order->save();
	}

	public function actionFixOrderModes()
	{
		self::fixOrderModes();
	}

	public static function fixOrderModes()
	{
		$progress = new ProgressBar(new ConsoleOutput());
		$progress->start(Order::find()->count());
		/** @var Order $order */
		foreach (Order::find()->each(20) as $order) {
			$progress->advance();
			$order->mode = null;
			$order->mode = $order->getMode();
			$order->save();
		}
		$progress->finish();
	}

	/**
	 * Wrapper for Yii::error() and console output.
	 *
	 * @see Yii::error
	 *
	 * @param string $message
	 */
	private function error($message)
	{
		$this->stderr($message . PHP_EOL, Console::FG_RED, Console::BOLD);
		Yii::error($message, 'mobilezone-erp-controller');
	}

	/**
	 * Wrapper for Yii::info() and console output.
	 *
	 * @see Yii::info
	 *
	 * @param string $message
	 */
	private function info($message)
	{
		$this->stdout($message . PHP_EOL, Console::FG_GREEN);
		Yii::info($message, 'mobilezone-erp-controller');
	}

	/**
	 * Wrapper for Yii::warning() and console output.
	 *
	 * @see Yii::info
	 *
	 * @param string $message
	 */
	private function warning($message)
	{
		$this->stdout($message . PHP_EOL, Console::FG_YELLOW);
		Yii::warning($message, 'mobilezone-erp-controller');
	}
}
