<?php

namespace common\modules\order\factories;

use common\modules\pricing\helpers\ItemPricesHelper;
use common\modules\order\models\Order;
use common\modules\order\models\Status;
use common\modules\order\OrderModule;

class OrderFactory
{
	protected $itemPricesHelper;

	public function __construct(ItemPricesHelper $itemPricesHelper)
	{
		$this->itemPricesHelper = $itemPricesHelper;
	}

	/**
	 * @param string $paymentType
	 * @param array $items
	 * @param array $customerAndAddressInfo
	 * @param array $customerPhoneNumber
	 *
	 * @return Order
	 */
	public function create($paymentType, $items, $customerAndAddressInfo, $customerPhoneNumber = [])
	{
		list($billingAddressId, $deliveryAddressId, $customerId) = $customerAndAddressInfo;

		$order = new Order();
		$order->customer_id = $customerId;
		$order->delivery_address_id = $deliveryAddressId;
		$order->billing_address_id = $billingAddressId;
		$order->vat_rate = OrderModule::getInstance()->params['vat_rate'];
		$order->payment_method = $paymentType;
		$order->status = Status::NEW;
		$order->prices = $this->itemPricesHelper->getTotals($items);

		/* Request Headers

            'X-Client-Ip' => Yii::$app->request->userIP,
            'X-Client-Agent' => Yii::$app->request->userAgent,
            'X-Client-Host' => Yii::$app->request->userHost,

       */

		$order->request_client_ip = \Yii::$app->request->headers->get('X-Client-Ip', 'NOT_DEFINED');
        $order->request_client_agent = \Yii::$app->request->headers->get('X-Client-Agent', 'NOT_DEFINED');
        $order->request_client_host = \Yii::$app->request->headers->get('X-Client-Host', 'NOT_DEFINED');

		$order->setCustomerPhoneNumber($customerPhoneNumber);

		$order->save();

		return $order;
	}
}
