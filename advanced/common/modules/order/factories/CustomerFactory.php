<?php

namespace common\modules\order\factories;

use common\models\Customer;
use common\modules\order\models\Address;
use LogicException;

class CustomerFactory
{
	/**
	 * @param $postData
	 *
	 * @return array
	 */
	public function create($postData)
	{
		$customerData = $postData['customer'];
		$customerId = $customerData['customer_id'];

		$defaultAddress = $this->createDefaultAddress($customerData);
		$billingAddress = $this->createBillingAddress($customerData, $defaultAddress);
		$deliveryAddress = $this->createDeliveryAddress($customerData, $billingAddress);

		$this->storePassportData($customerData);

		if (!$billingAddress->id || !$deliveryAddress->id) {
			$postJson = json_encode($postData);
			throw new \RuntimeException("Billing address id (value: {$billingAddress->id}) or delivery address id" .
				" (value: {$deliveryAddress->id}) not available!\n" .
				"\n" .
				"Post Data: {$postJson}\n" .
				"\n");
		}

		return [$billingAddress->id, $deliveryAddress->id, $customerId];
	}


	/**
	 * @param $customerData
	 *
	 * @return mixed
	 */
	private function createDefaultAddress($customerData)
	{
		$customerId = $customerData['customer_id'];
		$addressData = $customerData['defaultAddress'] ?? null;

		if (null === $addressData) {
			return null;
		}

		return $this->createAddress($customerId, $addressData, false);
	}

	/**
	 * @param integer $customerId
	 * @param array $addressData
	 * @param bool $safe
	 *
	 * @return Address
	 * @internal param $billingAddressData
	 */
	private function createAddress($customerId, $addressData, $safe = true)
	{
		$address = new Address();
		$address->customer_id = $customerId;
		$address->sex = $addressData['sex'];
		$address->firstname = $addressData['firstname'];
		$address->lastname = $addressData['lastname'];
		$address->company = $addressData['company'] ?? null;
		$address->street = $addressData['street'];
		$address->house_number = $addressData['house_number'];
		$address->zip = $addressData['zip'];
		$address->city = $addressData['city'];
		$address->country = $addressData['country'] ?? null;
		$address->contact_phone_number = $addressData['contact_phone_number'];

		if($safe){
			$address->save();
		}

		return $address;
	}

	/**
	 * @param array $customerData
	 * @param Address|null $defaultAddress
	 *
	 * @return Address
	 * @throws LogicException
	 */
	private function createBillingAddress($customerData, $defaultAddress): Address
	{
		$customerId = $customerData['customer_id'];
		$addressData = $customerData['billingAddress'] ?? null;

		if (!$addressData || $this->addressIsEmpty($addressData)) {
			if ($defaultAddress !== null) {
				$address = clone $defaultAddress;
				$address->id = null;
				$address->save();
			} else {
				throw new LogicException('no default address set - cannot overwrite empty billing address');
			}
		} else {
			$address = $this->createAddress($customerId, $addressData);
		}

		return $address;
	}

	/**
	 * @param $addressData
	 *
	 * @return bool
	 */
	private function addressIsEmpty($addressData)
	{
		foreach ($addressData as $key => $value) {
			if (in_array(
					$key,
					['sex', 'firstname', 'lastname', 'street', 'house_number', 'zip', 'city']
				) && empty($value)
			) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param array $customerData
	 * @param Address $billingAddress
	 *
	 * @return Address
	 */
	private function createDeliveryAddress($customerData, $billingAddress): Address
	{
		$customerId = $customerData['customer_id'];
		$addressData = $customerData['deliveryAddress'] ?? null;

		if (null === $addressData || false === (bool) $customerData['useDeliveryAddress']) {
			$address = new Address();
			$address->id = $billingAddress->id;
		} else {
			$address = $this->createAddress($customerId, $addressData);
		}

		return $address;
	}

	private function storePassportData($customerData)
	{
		$customerId = $customerData['customer_id'];
		$customer = Customer::findOne([
			'id' => $customerId
		]);

		$customer->passport_id = $customerData['passport_id'] ?? null;
		$customer->passport_type = $customerData['passport_type'] ?? null;
		$customer->nationality = $customerData['nationality'] ?? null;
		$customer->birthday = $customerData['birthday'] ?? null;

		$customer->save();

		return true;
	}
}
