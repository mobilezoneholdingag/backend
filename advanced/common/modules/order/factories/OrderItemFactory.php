<?php

namespace common\modules\order\factories;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use common\modules\pricing\models\ConsolidatedPrice;
use common\modules\pricing\helpers\ItemPricesHelper;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use common\modules\order\OrderModule;
use common\modules\payment\models\PaymentMethodContext;
use yii\db\ActiveRecord;


class OrderItemFactory
{
	/**
	 * @var Order
	 */
	protected $order;
	protected $itemPricesHelper;

	public function __construct(ItemPricesHelper $itemPricesHelper)
	{
		$this->itemPricesHelper = $itemPricesHelper;
	}

	public function create(Order $order, $items)
	{
		$this->order = $order;

		foreach ($items as $item) {
			$this->createItem($item);
		}

		$this->recalculateComissionItemPrice();
		$this->saveOrderMode();
	}

	public function addItem(Order $order, int $id, string $type = Item::TYPE_ARTICLE)
	{
		$this->order = $order;
		$this->createItemFromEntity($id, $type);
		$this->recalculateComissionItemPrice();
		$this->saveOrderMode();
	}

	private function recalculateComissionItemPrice()
	{
		$total = 0;
		$comissionItem = null;
		/** @var Item $savedItem */
		foreach ($this->order->getItems()->all() as $savedItem) {
			$total += $savedItem->price_once;
			if ($savedItem->item_id == Article::COMMISSION_ARTICLE_ID) {
				$comissionItem = $savedItem;
			}
		}

		if ($comissionItem) {
			$comissionItem->price_once = (new PaymentMethodContext($this->order->payment_method))->calculateFee($total);
			$comissionItem->save();

			$this->order->price_once = $total + $comissionItem->price_once;
			$this->order->save();
		}
	}

	private function saveOrderMode()
	{
		$this->order->mode = $this->order->getMode();
        if($this->order->price_once < 0){
            if($coupon = Item::find()->where(["order_id" => $this->order->id])->andWhere(["item_product_type_id" => "coupon"])->one()) {
                $coupon->price_once = $coupon->price_once - $this->order->price_once;
                $coupon->save();
                $this->order->price_once = 0;
            }
        }
		$this->order->save();

	}

	protected function createItemFromEntity(int $id, string $type)
	{
		if (!isset(Item::TYPE_ENTITY_MAPPING[$type])) {
			throw new \Exception("Type {$type} not found!");
		}
		/** @var ActiveRecord $entityClass */
		$entityClass = Item::TYPE_ENTITY_MAPPING[$type];
		/** @var Article|Tariffs $entity */
		$entity = $entityClass::findOne($id);

		$item = new Item();
		$item->order_id = $this->order->id;
		$item->item_id = $id;
		$item->item_product_type_id = $type;
		$item->mat_no = $entity->mat_no ?? null;
		$item->vat_rate = OrderModule::getInstance()->params['vat_rate'];

		switch ($type) {
			case Item::TYPE_ARTICLE:
				/** @var Article $entity */
				$item->title = $entity->product_name;
				$item->erp_title = $entity->product_name;
				$item->prices = new ConsolidatedPrice($entity->getPriceOnce(), $entity->getPriceMonthly());
				break;
			case Item::TYPE_TARIFF:
				/** @var Tariffs $entity */
				$item->title = $entity->title;
				$item->erp_title = $entity->erp_title;
				$item->prices = new ConsolidatedPrice($entity->getPriceOnce(), $entity->getPriceMonthly());
				break;
			case Item::TYPE_ARTICLE_TARIFF:
				/** @var ArticleTariffs $entity */
				$item->title = "{$entity->article->product_name} - {$entity->tariff->title}";
				$item->erp_title = "{$entity->article->product_name} - {$entity->tariff->erp_title}";
				$item->prices = new ConsolidatedPrice(0, 0);
				break;
		}

		$item->save();
	}

	/**
	 * @param array $importedItem
	 * @param Item $parent
	 */
	protected function createItem($importedItem = [], Item $parent = null)
	{
		$item = new Item();
		$item->order_id = $this->order->id;
		$item->item_id = $importedItem['item_id'];
		$item->item_product_type_id = $importedItem['item_product_type_id'];
		$item->mat_no = $importedItem['mat_no'] ?? null;
		$item->title = $importedItem['title'];
		$item->erp_title = $importedItem['erp_title'] ?? null;
		$item->vat_rate = $parent->vat_rate ?? OrderModule::getInstance()->params['vat_rate'];
		$item->prices = $importedItem['prices'] ?? $this->itemPricesHelper->getPrices($item->item_product_type_id, $item->item_id);

		if ($parent) {
			$item->parent_item_id = $parent->id;
		}

		$item->save();

		if (!$parent && isset($importedItem['insurance'])) {
			$this->createItem($importedItem['insurance'], $item);
		}

		if ($importedItem['item_product_type_id'] === 'article_tariff') {
			$articleTariff = ArticleTariffs::findOne(['id' => $importedItem['item_id']]);

			$this->createItem([
				'item_product_type_id' => 'article',
				'item_id' => $articleTariff->article->id,
				'title' => $articleTariff->article->product_name,
				'prices' => $articleTariff->prices['article'],
				'mat_no' => $articleTariff->article->mat_no,
			], $item);

			// add sim card to order if needed, splitt prices between sim card and tariff because
			// tariff has aggregated price, which includes the price of the sim card
			if (empty($importedItem['sim_card']) === false && $articleTariff->simCard) {
				$prices = $articleTariff->prices['tariff'];

				$this->createItem([
					'item_product_type_id' => 'sim_card',
					'item_id' => $articleTariff->simCard->id,
					'title' => $articleTariff->simCard->product_name,
					'mat_no' => $articleTariff->simCard->mat_no,
					'prices' => $articleTariff->simCard->prices,
				], $item);

				$this->createItem([
					'item_product_type_id' => 'tariff',
					'item_id' => $articleTariff->tariff->id,
					'title' => $articleTariff->tariff->title,
					'erp_title' => $articleTariff->tariff->erp_title,
					'prices' => new ConsolidatedPrice(0, $prices->getPriceMonthly()),
				], $item);
			} else {
				$this->createItem([
					'item_product_type_id' => 'tariff',
					'item_id' => $articleTariff->tariff->id,
					'title' => $articleTariff->tariff->title,
					'erp_title' => $articleTariff->tariff->erp_title,
					'prices' => $articleTariff->prices['tariff'],
				], $item);
			}
		}
	}
}
