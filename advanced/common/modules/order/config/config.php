<?php

return [
	'defaultRoute' => 'order/index',
	'params' => [
		'vat_rate' => 7.7,
	],
];
