<?php
namespace common\modules\order\events;

use common\modules\order\models\Order;

class OrderTrackingIdsChangedEvent extends AbstractOrderEvent
{
	/** @var string  */
	public $name = Order::EVENT_AFTER_ORDER_TRACKING_IDS_CHANGED;
}
