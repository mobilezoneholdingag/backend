<?php
namespace common\modules\order\events;

use common\modules\order\models\Order;
use deinhandy\yii2events\event\ClassLevelEvent;

/**
 * Class AbstractOrderEvent
 *
 * @package common\modules\order\events
 */
class AbstractOrderEvent extends ClassLevelEvent
{
	/** @var string $sender */
	public $sender = Order::class;

	/**
	 * @var Order
	 */
	protected $order;

	/**
	 * @return Order
	 */
	public function getOrder()
	{
		return $this->order;
	}

	/**
	 * @param Order $order
	 *
	 * @return $this
	 */
	public function setOrder(Order $order)
	{
		$this->order = $order;

		return $this;
	}
}