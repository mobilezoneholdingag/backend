<?php
namespace common\modules\order\events;

use common\modules\order\models\Order;


class OrderStatusDeclinedEvent extends AbstractOrderEvent
{
	/** @var string */
	public $name = Order::EVENT_AFTER_ORDER_STATUS_DECLINED;
}
