<?php
namespace common\modules\order\events;

use common\modules\order\models\Order;


class OrderStatusPaymentSuccessfulEvent extends AbstractOrderEvent
{
	/** @var string  */
	public $name = Order::EVENT_AFTER_ORDER_STATUS_PAYMENT_SUCCESSFUL;
}