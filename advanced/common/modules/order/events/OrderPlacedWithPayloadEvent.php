<?php
namespace common\modules\order\events;

use common\modules\order\models\Order;
use yii\base\Request;

class OrderPlacedWithPayloadEvent extends AbstractOrderEvent
{
	/** @var string  */
	public $name = Order::EVENT_AFTER_ORDER_PLACED_WITH_PAYLOAD;

	private $payload;

	/**
	 * @param array $payload
	 */
	public function setPayload(array $payload)
	{
		$this->payload = $payload;
	}

	/**
	 * @return array
	 */
	public function getPayload()
	{
		return $this->payload;
	}
}