<?php

namespace common\modules\order\actions;

use Yii;
use yii\data\ActiveDataProvider;

class IndexAction extends \yii\rest\IndexAction
{
	/**
	 * @return ActiveDataProvider
	 */
	public function run()
	{

		$request = Yii::$app->request;
		$customerId = $request->get('customer_id');
		$id = $request->get('id');

		$dataProvider = parent::run();
		$dataProvider->pagination->pageSize = 500;

		if ($customerId) {
			$dataProvider->query->where(['customer_id' => $customerId]);
		}

		if ($id) {
		    $dataProvider->query->andWhere(['id' => $id]);
        }

		return $dataProvider;
	}
}
