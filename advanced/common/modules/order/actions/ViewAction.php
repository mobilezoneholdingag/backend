<?php

namespace common\modules\order\actions;

use common\modules\order\models\Order;
use common\modules\order\models\Status;
use Yii;

class ViewAction extends \yii\rest\ViewAction
{
	public function findModel($id)
	{
		$request = Yii::$app->request;
		$query = Order::find()->where(['id' => $id]);

		if ($request->get('offered')) {
			return $query->andWhere(['status' => Status::OFFERED])->one();
		}

		return $query->andWhere(['customer_id' => $request->get('customer_id')])->one();
	}
}
