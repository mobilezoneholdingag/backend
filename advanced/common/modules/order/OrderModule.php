<?php

namespace common\modules\order;

use deinhandy\yii2container\Module;
use Yii;

/**
 * order module definition class
 */
class OrderModule extends Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'common\modules\order\controllers';

	public function init()
	{
		parent::init();

		switch (Yii::$app->id) {
			case 'app-console':
				$this->controllerNamespace .= '\console';
				break;
			case 'app-frontend':
				$this->controllerNamespace .= '\frontend';
				break;
			default:
				$this->controllerNamespace .= '\backend';
		}
	}
}
