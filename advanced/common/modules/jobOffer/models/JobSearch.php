<?php

namespace common\modules\jobOffer\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class JobSearch extends Job
{
	public $category;

	public function rules()
	{
		return [
			[['id', 'job_category_id', 'job_id'], 'integer'],
			[['language', 'title', 'location', 'text', 'category'], 'safe'],
		];
	}

	public function scenarios()
	{
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = Job::find();
		$query->joinWith('jobCategory');

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->sort->attributes['category'] = [
			'asc' => ['job_category.title' => SORT_ASC],
			'desc' => ['job_category.title' => SORT_DESC],
		];

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'job_id' => $this->job_id,
		]);

		$query
			->andFilterWhere(['like', 'language', $this->language])
			->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'location', $this->location])
			->andFilterWhere(['like', 'text', $this->text])
			->andFilterWhere(['like', 'job_category.title', $this->category]);

		return $dataProvider;
	}
}
