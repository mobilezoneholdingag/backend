<?php

namespace common\modules\jobOffer\models;

use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $job_category_id
 * @property string $language
 * @property string $title
 * @property string $location
 * @property string $text
 * @property integer $job_id
 *
 * @property JobCategory $jobCategory
 */
class Job extends ActiveRecord
{
	const LANGUAGES = [
		'de' => 'DE',
		'fr' => 'FR',
		'it' => 'IT',
	];

	public function behaviors()
	{
		return [
			[
				'class' => SluggableBehavior::class,
				'attribute' => 'title',
			],
			LoggableBehavior::className(),
		];
	}

	public static function tableName()
	{
		return 'job';
	}

	public function rules()
	{
		return [
			[['job_category_id', 'job_id'], 'required'],
			[['job_category_id', 'job_id'], 'integer'],
			[['language'], 'string', 'max' => 2],
			[['title', 'location'], 'string', 'max' => 255],
			[['text'], 'string'],
			[['job_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobCategory::className(), 'targetAttribute' => ['job_category_id' => 'id']],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'job_category_id' => Yii::t('app', 'Job Category ID'),
			'language' => Yii::t('app', 'Language'),
			'title' => Yii::t('app', 'Title'),
			'location' => Yii::t('app', 'Location'),
			'text' => Yii::t('app', 'Text'),
			'job_id' => Yii::t('app', 'Job ID'),
		];
	}

	public function fields()
	{
		return [
			'title',
			'location',
			'job_id',
			'text',
			'language',
			'slug',
		];
	}

	public function getJobCategory()
	{
		return $this->hasOne(JobCategory::className(), ['id' => 'job_category_id']);
	}

	public static function getAllJobCategoryTitles(): array
	{
		return JobCategory::find()->select(['id', 'title'])->createCommand()->queryAll(\PDO::FETCH_KEY_PAIR);
	}
}
