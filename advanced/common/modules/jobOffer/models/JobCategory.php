<?php

namespace common\modules\jobOffer\models;

use components\i18n\TranslatableActiveRecord;
use sammaye\audittrail\LoggableBehavior;
use Yii;

/**
 * @property integer $id
 * @property string $title
 *
 * @property Job[] $jobs
 */
class JobCategory extends TranslatableActiveRecord
{
	protected $translateAttributes = [
		'title'
	];

	public static function tableName()
	{
		return 'job_category';
	}

	public function rules()
	{
		return [
			[['title'], 'string', 'max' => 255],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
		];
	}

	public function fields()
	{
		return [
			'id',
			'title',
			'jobs' => 'jobIds',
		];
	}

	public function behaviors()
	{
		return array_merge(
			parent::behaviors(),
			Yii::$app->id == 'app-backend' ? [LoggableBehavior::className()] : []
		);
	}

	public function getJobs()
	{
		return $this->hasMany(Job::className(), ['job_category_id' => 'id']);
	}

	public function getJobIds(): array
	{
		return $this->getJobs()->select(['id'])->column();
	}
}
