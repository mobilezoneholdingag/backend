<?php

namespace common\modules\jobOffer\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class JobCategorySearch extends JobCategory
{
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['title'], 'safe'],
		];
	}

	public function scenarios()
	{
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = JobCategory::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
		]);

		$query->andFilterWhere(['like', 'title', $this->title]);

		return $dataProvider;
	}
}
