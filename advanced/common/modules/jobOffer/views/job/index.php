<?php

use common\modules\jobOffer\models\Job;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\jobOffer\models\JobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Jobs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create Job'), ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a(Yii::t('app', 'Create Job Category'), ['/joboffer/job-category/create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			[
				'attribute' => 'category',
				'value' => 'jobCategory.title',
			],
			[
				'attribute' => 'language',
				'value' => 'language',
				'filter' => Html::activeDropDownList(
					$searchModel,
					'language', Job::LANGUAGES,
					['class'=>'form-control', 'prompt' => '- ' . Yii::t('app', 'please select') . ' -']
				),
			],
			'title',
			'location',
			'job_id',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
</div>
