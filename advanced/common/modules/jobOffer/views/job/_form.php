<?php

use common\modules\jobOffer\models\Job;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this View */
/* @var $model Job */
/* @var $form ActiveForm */
?>

<div class="job-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'job_category_id')->dropDownList(
			Job::getAllJobCategoryTitles(),
			['prompt' => '- ' . Yii::t('app', 'please select') . ' -']
	) ?>

	<?= $form->field($model, 'job_id')->textInput() ?>

	<?= $form->field($model, 'language')->dropDownList(Job::LANGUAGES) ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

	<?= $form
		->field($model, 'text')
		->widget(CKEditor::className(), ['options' => ['rows' => 12], 'preset' => 'full']); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
