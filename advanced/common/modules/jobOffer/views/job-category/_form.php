<?php

use components\i18n\widget\TabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\jobOffer\models\JobCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-category-form">

	<?php $form = ActiveForm::begin(); ?>

	<p>
		<?= TabWidget::widget([
			'model' => $model,
			'form' => $form,
			'fieldsToRender' => [
				'title',
			],
			'inputType' => TabWidget::TYPE_TEXTFIELD,
		]); ?>
	</p>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
