<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\jobOffer\models\JobCategory */

$this->title = Yii::t('app', 'Create Job Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Job Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-category-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
