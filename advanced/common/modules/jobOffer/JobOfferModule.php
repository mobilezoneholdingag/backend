<?php

namespace common\modules\jobOffer;

use yii\base\Module;

class JobOfferModule extends Module
{
	public $controllerNamespace = 'common\modules\jobOffer\controllers';
}
