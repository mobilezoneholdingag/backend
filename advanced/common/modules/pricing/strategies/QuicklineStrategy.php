<?php

namespace common\modules\pricing\strategies;

use common\modules\pricing\models\Price;

class QuicklineStrategy extends DefaultPricingStrategy
{
    /**
     * Return device`s total price.
     */
    public function getArticlePriceTotal(): Price
    {
        $calculatedPrice = $this->getArticlePriceOnce()->getValue()
            + ($this->getArticlePriceMonthly()->getValue() * $this->tariff->getRatePaymentCount());

        return new Price($calculatedPrice);
    }

    /**
     * Mobilezone-customized price calculation for monthly article price.
     */
    public function getArticlePriceMonthly(): Price
    {
        $this->checkProviderTitle();
        $subtract = $this->getArticlePriceOnce()->getValue() > 0 ? $this->getArticlePriceOnce()->getValue() : 1;
        return new Price(floor((($this->article->price_sale - $subtract) / $this->tariff->getRatePaymentCount())*20)/20);
    }
}
