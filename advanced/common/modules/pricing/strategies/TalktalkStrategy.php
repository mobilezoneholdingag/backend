<?php

namespace common\modules\pricing\strategies;

use common\modules\pricing\models\Price;

class TalktalkStrategy extends DefaultPricingStrategy
{
	/**
	 * Mobilezone-customized price calculation for monthly article price.
	 */
	public function getArticlePriceMonthly(): Price
	{
		$this->checkProviderTitle();
		$calculatedPrice = ($this->getArticlePriceTotal()->getValue() - $this->getArticlePriceOnce()->getValue())
			/ $this->tariff->getRatePaymentCount();

		// round down to the next 5 Rappen here
		return new Price(floor($calculatedPrice * 20) / 20);
	}

	/**
	 * Return device`s total price.
	 */
	public function getArticlePriceTotal(): Price
	{
		return $this->article->getPriceOnce();
	}
}
