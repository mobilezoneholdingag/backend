<?php

namespace common\modules\pricing\strategies;

use common\models\ArticleTariffs;
use common\modules\pricing\models\Price;
use common\modules\pricing\models\interfaces\PricingStrategyInterface;
use RuntimeException;

class DefaultPricingStrategy implements PricingStrategyInterface
{
	protected $tariff;
	protected $article;
	protected $articleTariff;

	function __construct(ArticleTariffs $articleTariff)
	{
		$this->article = $articleTariff->article;
		$this->tariff = $articleTariff->tariff;
		$this->articleTariff = $articleTariff;
	}

	/**
	 * Price once for both the article and the tariff.
	 */
	public function getPriceOnce(): Price
	{
		return $this->getArticlePriceOnce()->add($this->getTariffPriceOnce());
	}

	/**
	 * Price once for the article.
	 */
	public function getArticlePriceOnce(): Price
	{
		return new Price($this->articleTariff->price);
	}

	/**
	 * Price once for tariff.
	 */
	public function getTariffPriceOnce(): Price
	{
		return new Price();
	}

	/**
	 * Price monthly for both the article and the tariff.
	 */
	public function getPriceMonthly(): Price
	{
		return $this->getArticlePriceMonthly()->add($this->getTariffPriceMonthly());
	}

	/**
	 * Mobilezone-customized price calculation for monthly article price.
	 *
	 * @throws RuntimeException
	 */
	public function getArticlePriceMonthly(): Price
	{
		$this->checkProviderTitle();

		return new Price();
	}

	/**
	 * Check if provider´s title is not empty.
	 *
	 * @throws RuntimeException
	 */
	protected function checkProviderTitle()
	{
		if (empty($this->tariff->provider->system_title)) {
			throw new RuntimeException(
				'Provider system title could not be determined when calculating articletariff #'
				. $this->articleTariff->id
				. ' price.'
			);
		}
	}

	/**
	 * Price monthly for the tariff.
	 */
	public function getTariffPriceMonthly(): Price
	{
		return $this->tariff->getPriceMonthly();
	}

	/**
	 * Return first month`s device price.
	 */
	public function getArticlePriceFirstMonth(): Price
	{
		return new Price();
	}

	/**
	 * Return device`s total price.
	 */
	public function getArticlePriceTotal(): Price
	{
		return new Price();
	}
}
