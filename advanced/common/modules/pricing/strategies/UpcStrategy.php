<?php

namespace common\modules\pricing\strategies;

use common\modules\pricing\models\Price;

class UpcStrategy extends DefaultPricingStrategy
{
	/**
	 * Mobilezone-customized price calculation for monthly article price.
	 */
	public function getArticlePriceMonthly(): Price
	{
		$this->checkProviderTitle();
		$additional = $this->getArticlePriceOnce()->getValue() > 0 ? 1 : 0;
		$price = ($this->getArticlePriceTotal()->getValue() - ($this->getArticlePriceOnce()->getValue() + $additional))
			/ $this->tariff->getRatePaymentCount();

		return new Price($price);
	}

	/**
	 * Return device`s total price.
	 */
	public function getArticlePriceTotal(): Price
	{
		$priceFlooredToMultipleOfSix = floor($this->article->price_sale / 6) * 6;
		$subtract = $this->getArticlePriceOnce()->getValue() > 0 ? 1 : 0;
		$calculatedPrice = $priceFlooredToMultipleOfSix - $subtract;

		return new Price($calculatedPrice);
	}
}
