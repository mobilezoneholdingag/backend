<?php

namespace common\modules\pricing\strategies;

use common\modules\pricing\models\Price;

class SaltStrategy extends DefaultPricingStrategy
{
    /**
     * Mobilezone-customized price calculation for monthly article price.
     */
    public function getArticlePriceMonthly(): Price
    {
        if(parent::getArticlePriceOnce()->getValue() > 600){

            $this->checkProviderTitle();
            $calculatedPrice = 600 / $this->tariff->getRatePaymentCount();

            // round down to the next 5 Rappen here
            return new Price(floor($calculatedPrice * 20) / 20);

        }else{
            $this->checkProviderTitle();
            $calculatedPrice = parent::getArticlePriceOnce()->getValue() / $this->tariff->getRatePaymentCount();

            // round down to the next 5 Rappen here
            return new Price(floor($calculatedPrice * 20) / 20);
        }
    }

    /**
     * Return first month`s device price.
     */
    public function getArticlePriceOnce(): Price
    {
        if(parent::getArticlePriceOnce()->getValue() > 600){
            return new Price(parent::getArticlePriceOnce()->getValue() - 600);
        }else{
            return new Price();
        }
    }







}
