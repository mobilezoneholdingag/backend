<?php

namespace common\modules\pricing\strategies;

use common\modules\pricing\models\Price;

class SwisscomStrategy extends DefaultPricingStrategy
{
	/**
	 * Return first month`s device price.
	 */
	public function getArticlePriceFirstMonth(): Price
	{
		return new Price(parent::getArticlePriceOnce()->getValue() - $this->getRemainingTotalAfterFirstMonth());
	}

	/**
	 * Return device`s total price.
	 */
	public function getArticlePriceTotal(): Price
	{
		return parent::getArticlePriceOnce();
	}

	/**
	 * Calculate the remaining price after first month´s payment.
	 */
	protected function getRemainingTotalAfterFirstMonth(): float
	{
		$contractDurationFull = (int)$this->tariff->contract_duration;
		if ($this->tariff->isRatePaymentEnabled()) {
			$contractDurationFull = $this->tariff->getRatePaymentCount();
		}

		return ($contractDurationFull - 1) * $this->getArticlePriceMonthly()->getValue();
	}

	/**
	 * Mobilezone-customized price calculation for monthly article price.
	 */
	public function getArticlePriceMonthly(): Price
	{
		$this->checkProviderTitle();
		$calculatedPrice = parent::getArticlePriceOnce()->getValue() / $this->tariff->getRatePaymentCount();

		// round down to the next 5 Rappen here
		return new Price(floor($calculatedPrice * 20) / 20);
	}

	/**
	 * Price once for the article.
	 */
	public function getArticlePriceOnce(): Price
	{
		return new Price();
	}
}
