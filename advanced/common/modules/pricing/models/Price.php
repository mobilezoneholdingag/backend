<?php

namespace common\modules\pricing\models;

/**
 * Price abstraction
 */
class Price
{
	/**
	 * @var float
	 */
	private $value;

	/**
	 * Price constructor.
	 *
	 * @param float|null $value
	 */
	public function __construct($value = null)
	{
		if (null === $value) {
			$value = 0;
		}
		$this->value = (float)$value;
	}

	public function getValue() : float
	{
		return $this->value;
	}

	/**
	 * @param Price $price
	 *
	 * @return $this Fluent interface.
	 */
	public function add(Price $price)
	{
		$this->value += $price->getValue();
		return $this;
	}

	/**
	 * @param Price $price
	 *
	 * @return $this Fluent interface.
	 */
	public function subtract(Price $price)
	{
		$this->value -= $price->getValue();
		return $this;
	}

	/**
	 * @return $this Fluent interface.
	 */
	public function setZero()
	{
		$this->value = 0;
		return $this;
	}
}
