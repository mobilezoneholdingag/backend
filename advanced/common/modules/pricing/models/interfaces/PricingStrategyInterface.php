<?php

namespace common\modules\pricing\models\interfaces;

use common\modules\pricing\models\Price;

/**
 * Interface PricingStrategyInterface
 *
 * @package common\modules\pricing
 */
interface PricingStrategyInterface
{
	/**
	 * Price once for both the article and the tariff.
	 */
	public function getPriceOnce(): Price;

	/**
	 * Price once for the article.
	 */
	public function getArticlePriceOnce(): Price;

	/**
	 * Price once for tariff.
	 */
	public function getTariffPriceOnce(): Price;

	/**
	 * Price monthly for both the article and the tariff.
	 */
	public function getPriceMonthly(): Price;

	/**
	 * Mobilezone-customized price calculation for monthly article price.
	 */
	public function getArticlePriceMonthly(): Price;

	/**
	 * Price monthly for the tariff.
	 */
	public function getTariffPriceMonthly(): Price;

	/**
	 * Return first month`s device price.
	 */
	public function getArticlePriceFirstMonth(): Price;

	/**
	 * Return device`s total price.
	 */
	public function getArticlePriceTotal(): Price;
}
