<?php

namespace common\modules\pricing\models\interfaces;

/**
 *
 */
interface PriceProviderInterface
{
	/**
	 * @return array
	 */
	public function getPrices();

	/**
	 * @return float|null
	 */
	public function getPriceOnce();

	/**
	 * @return float|null
	 */
	public function getPriceMonthly();
}
