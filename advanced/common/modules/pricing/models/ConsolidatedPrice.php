<?php

namespace common\modules\pricing\models;

use yii\base\Arrayable;

/**
 * Pricing consolidation, we often has "once" and "monthly prices together.
 */
class ConsolidatedPrice implements Arrayable
{
	private $priceOnce;
	private $priceMonthly;

	public function __construct($priceOnce = 0, $priceMonthly = 0)
	{
		$this->priceOnce = $priceOnce instanceof Price ? $priceOnce : new Price($priceOnce);
		$this->priceMonthly = $priceMonthly instanceof  Price ? $priceMonthly : new Price($priceMonthly);
	}

	public function getPriceOnce()
	{
		return $this->priceOnce;
	}

	public function getPriceMonthly()
	{
		return $this->priceMonthly;
	}

	/**
	 * @param ConsolidatedPrice $price
	 *
	 * @return $this
	 */
	public function add(ConsolidatedPrice $price)
	{
		$this->priceOnce->add($price->getPriceOnce());
		$this->priceMonthly->add($price->getPriceMonthly());

		return $this;
	}

	/**
	 * Parameters just exists to satisfy the stupid non atomic yii2 interface. This method has static logic.
	 *
	 * @param array $fields
	 * @param array $expand
	 * @param bool $recursive
	 *
	 * @return array
	 */
	public function toArray(array $fields = [], array $expand = [], $recursive = true)
	{
		return [
			'once' => $this->priceOnce->getValue(),
			'monthly' => $this->priceMonthly->getValue(),
		];
	}

	/**
	 * This just exists to satisfy the stupid non atomic yii2 interface.
	 *
	 * @return array
	 */
	public function fields()
	{
		return [];
	}

	/**
	 * This just exists to satisfy the stupid non atomic yii2 interface.
	 *
	 * @return array
	 */
	public function extraFields()
	{
		return [];
	}
}
