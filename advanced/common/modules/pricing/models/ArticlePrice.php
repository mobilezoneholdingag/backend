<?php

namespace common\modules\pricing\models;

class ArticlePrice extends ConsolidatedPrice
{
	private $priceFirstMonth;
	private $priceTotal;

	public function __construct($priceOnce = 0, $priceMonthly = 0, $priceFirstMonth = 0, $priceTotal = 0)
	{
		parent::__construct($priceOnce, $priceMonthly);
		$this->priceFirstMonth = $priceFirstMonth instanceof Price ? $priceFirstMonth : new Price($priceFirstMonth);
		$this->priceTotal = $priceTotal instanceof Price ? $priceTotal : new Price($priceTotal);
	}

	public function getPriceFirstMonth()
	{
		return $this->priceFirstMonth;
	}

	public function getPriceTotal()
	{
		return $this->priceTotal;
	}

	public function toArray(array $fields = [], array $expand = [], $recursive = true)
	{
		return array_merge(parent::toArray($fields, $expand, $recursive), [
			'first_month' => $this->priceFirstMonth->getValue(),
			'total' => $this->getPriceTotal()->getValue(),
		]);
	}
}
