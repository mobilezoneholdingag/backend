<?php

namespace common\modules\pricing\helpers;

use common\models\Article;
use common\models\ArticleTariffs;
use common\modules\pricing\models\ConsolidatedPrice;
use common\models\Coupon;
use common\models\Tariffs;

class ItemPricesHelper
{
	/**
	 * @param string $type
	 * @param mixed $id
	 *
	 * @return ConsolidatedPrice
	 */
	public function getPrices($type, $id)
	{
		$price = new ConsolidatedPrice();

		switch ($type) {
			case 'accessories':
			case 'article':
				$article = Article::findOne($id);
				$price->add($article->prices);
				break;

			case 'tariff':
				$tariff = Tariffs::findOne($id);
				$price->add($tariff->prices);
				break;

			case 'article_tariff':
				$articleTariff = ArticleTariffs::findOne($id);
				$price->add($articleTariff->prices['total']);
				if ($articleTariff->needsSimCardOrIsSwisscom()
					&& $simCard = $articleTariff->getSimCard()) {
					$price->add($simCard->getPrices());
				}
				break;

			case 'coupon':
				$coupon = Coupon::findOne($id);
				$price->add($coupon->prices);
				break;
		}

		return $price;
	}

	/**
	 * @param array $items
	 *
	 * @return ConsolidatedPrice
	 */
	public function getTotals($items)
	{
		$price = new ConsolidatedPrice();

		foreach ($items as $item) {
			$itemPrice = $this->getPrices($item['item_product_type_id'], $item['item_id']);
			$price->add($itemPrice);

			if (isset($item['insurance'])) {
				$item = $item['insurance'];
				$itemPrice = $this->getPrices($item['item_product_type_id'], $item['item_id']);
				$price->add($itemPrice);
			}
		}

		return $price;
	}
}
