<?php

use common\modules\pricing\strategies\DefaultPricingStrategy;
use common\modules\pricing\strategies\SunriseStrategy;
use common\modules\pricing\strategies\SwisscomStrategy;
use common\modules\pricing\strategies\TalktalkStrategy;
use common\modules\pricing\strategies\UpcStrategy;

/* @var \deinhandy\yii2container\Container $container */
$container = di();

$container->set('pricing.swisscom', SwisscomStrategy::class);
$container->set('pricing.sunrise', SunriseStrategy::class);
$container->set('pricing.talktalk', TalktalkStrategy::class);
$container->set('pricing.upc', UpcStrategy::class);
$container->set('pricing.yallo', \common\modules\pricing\strategies\YalloStrategy::class);
$container->set('pricing.quickline', \common\modules\pricing\strategies\QuicklineStrategy::class);
$container->set('pricing.salt', \common\modules\pricing\strategies\SaltStrategy::class);
$container->set('pricing.default', DefaultPricingStrategy::class);