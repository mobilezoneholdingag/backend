<?php

namespace common\modules\swissbilling\models;

use common\modules\apilogger\models\ApiLog;
use common\modules\order\models\Order;

/**
 * @property integer $order_id
 *
 * @property Order $order
 */
class SwissBillingApiLog extends ApiLog
{
	public static function getTableName(): string
	{
		return 'api_log_swissbilling';
	}

	public function getOrder()
	{
		return $this->hasOne(Order::className(), ['id' => 'order_id']);
	}
}
