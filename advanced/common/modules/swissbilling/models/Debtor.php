<?php

namespace common\modules\swissbilling\models;

use DateTime;

class Debtor extends SoapModel
{
	/** @var string */
	public $title = null;
	/** @var string */
	public $firstname;
	/** @var string */
	public $lastname;
	/** @var DateTime */
	public $birthdate;
	/** @var string */
	public $adr1;
	/** @var string */
	public $city;
	/** @var string */
	public $zip;
	/** @var string */
	public $country;
	/** @var string */
	public $email;
	/** @var string */
	public $phone;
	/** @var string */
	public $language;
	/** @var int */
	public $user_ID;
	/** @var int */
	public $SBMember_ID = 0;
	/** @var string */
	public $deliv_firstname;
	/** @var string */
	public $deliv_lastname;
	/** @var string */
	public $deliv_adr1;
	/** @var string */
	public $deliv_city;
	/** @var string */
	public $deliv_zip;
	/** @var string */
	public $deliv_country;
	
	public function __construct(
		string $firstname,
		string $lastname,
		string $adr1,
		string $city,
		string $zip,
		string $email,
		string $language,
		int $user_ID,
		string $deliv_firstname,
		string $deliv_lastname,
		string $deliv_adr1,
		string $deliv_city,
		string $deliv_zip,
		DateTime $birthdate = null,
		string $phone = null
	) {
		parent::__construct();
		$this->firstname = $firstname;
		$this->lastname = $lastname;
		$this->birthdate = $birthdate;
		$this->adr1 = $adr1;
		$this->city = $city;
		$this->zip = $zip;
		$this->email = $email;
		$this->phone = $phone;
		$this->language = $language;
		$this->user_ID = $user_ID;
		$this->deliv_firstname = $deliv_firstname;
		$this->deliv_lastname = $deliv_lastname;
		$this->deliv_adr1 = $deliv_adr1;
		$this->deliv_city = $deliv_city;
		$this->deliv_zip = $deliv_zip;
	}
	
	/**
	 * @return string
	 */
	public function getBirthdate()
	{
		if (!$this->birthdate) {
			return null;
		}
		return $this->birthdate->format('Y-m-d');
	}
}
