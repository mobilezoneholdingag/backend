<?php

namespace common\modules\swissbilling\models;

class Merchant extends SoapModel
{
	/** @var string */
	public $id;
	/** @var string */
	public $pwd;

	public function __construct()
	{
		parent::__construct();
	}
}
