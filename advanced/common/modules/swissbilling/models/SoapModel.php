<?php

namespace common\modules\swissbilling\models;

use common\modules\swissbilling\SwissBillingModule;

class SoapModel implements \JsonSerializable
{
	public function __construct()
	{
		$params = SwissBillingModule::getInstance()->params;
		$className = $this->getLowerCaseClassName();

		if(!isset($params[$className])) {
			return;
		}

		foreach ($params[$className] as $field => $value) {
			$this->{$field} = $value;
		}
	}

	public function jsonSerialize(): array
	{
		$data = [];
		$properties = (new \ReflectionClass($this))->getProperties();

		foreach($properties as $property) {
			$getter = 'get' . str_replace('_', '', $property->getName());
			$value = method_exists($this, $getter) ? $this->{$getter}() : $property->getValue($this);
			$data[$property->getName()] = $value;
		}

		return $data;
	}
	
	public static function getLowerCaseClassName(): string
	{
		return strtolower(substr(get_called_class(), strrpos(get_called_class(), '\\') + 1));
	}
}
