<?php

namespace common\modules\swissbilling\models;

use common\modules\order\OrderModule;

class Item extends SoapModel
{
	/** @var string */
	public $short_desc;
	/** @var int */
	public $quantity;
	/** @var float */
	public $unit_price;
	/** @var float */
	public $VAT_rate;
	/** @var float */
	public $VAT_amount;
	/** @var string */
	public $file_link;
	/** @var string */
	public $image_type;
	/** @var string */
	public $desc;

	public function __construct(
		string $short_desc,
		string $desc,
		int $quantity,
		float $unit_price = 0.0,
		string $file_link = null,
		string $image_type = null
	) {
		parent::__construct();
		$vatRate = OrderModule::getInstance()->params['vat_rate'];
		$vatAmount = ($unit_price / (100 + $vatRate)) * $vatRate;
		$this->short_desc = $short_desc;
		$this->desc = $desc;
		$this->quantity = $quantity;
		$this->unit_price = $unit_price;
		$this->VAT_rate = $vatRate;
		$this->VAT_amount = round($vatAmount, 2);
		$this->file_link = $file_link;
		$this->image_type = $image_type;
	}
}
