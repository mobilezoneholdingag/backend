<?php

namespace common\modules\swissbilling\models;

use common\modules\order\OrderModule;
use common\modules\swissbilling\services\ApiClient;
use DateTime;

class Transaction extends SoapModel
{
	/** @var string */
	public $type;
	/** @var bool */
	public $is_B2B;
	/** @var string */
	public $eshop_ID;
	/** @var string */
	public $eshop_ref;
	/** @var DateTime */
	public $order_timestamp;
	/** @var string */
	public $currency;
	/** @var float */
	public $amount;
	/** @var float */
	public $VAT_amount;
	/** @var float */
	public $admin_fee_amount;
	/** @var float */
	public $delivery_fee_amount;
	/** @var float */
	public $coupon_discount_amount;
	/** @var float */
	public $vol_discount;
	/** @var bool */
	public $phys_delivery;
	/** @var string */
	public $debtor_IP;
	/** @var string */
	public $delivery_status = 'pending';
	/** @var bool */
	public $is_DirectInvoiceByEmail;
	/** @var string */
	public $DirectSuccessStatus;
	
	public function __construct(
		string $eshop_ref,
		DateTime $order_timestamp,
		float $amount,
		string $debtor_IP,
		float $coupon_discount_amount = 0.0
	) {
		parent::__construct();
		$vatRate = OrderModule::getInstance()->params['vat_rate'];
		$vatAmount = ($amount / (100 + $vatRate)) * $vatRate;
		$this->eshop_ref = $eshop_ref;
		$this->order_timestamp = $order_timestamp;
		$this->amount = $amount;
		$this->coupon_discount_amount = $coupon_discount_amount;
		$this->VAT_amount = round($vatAmount, 2);
		$this->debtor_IP = $this->debtor_IP ?? $debtor_IP;
	}
	
	/**
	 * @return string
	 */
	public function getOrderTimestamp(): string
	{
		return $this->order_timestamp->format(ApiClient::DATETIME_FORMAT);
	}
}
