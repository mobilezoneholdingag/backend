<?php

namespace common\modules\swissbilling\controllers;

use common\modules\swissbilling\facades\InvoiceFacade;
use common\modules\swissbilling\services\ApiClient;
use common\modules\swissbilling\SwissBillingModule;
use frontend\controllers\rest\Controller;
use Yii;
use yii\base\Module;

class InvoiceController extends Controller
{
	/** @var array */
	private $params;
	/** @var InvoiceFacade */
	private $facade;

	public function __construct($id, Module $module, ApiClient $apiClient, array $config = [])
	{
		parent::__construct($id, $module, $config);
		$this->params = SwissBillingModule::getInstance()->params;
		$this->facade = new InvoiceFacade(Yii::$app->request->post());
	}

	public function actionPreScreening(): array
	{
		return $this->facade->actionPreScreening();
	}

	public function actionDirect(): array
	{
		return $this->facade->actionDirect();
	}

	public function actionAcknowledge($orderId): array
	{
		return $this->facade->actionAcknowledge($orderId);
	}

	public function actionCancel($orderId): array
	{
		return $this->facade->actionCancel($orderId);
	}

	public function actionStatusRequest($orderId)
	{
		return $this->facade->actionStatusRequest($orderId);
	}
}
