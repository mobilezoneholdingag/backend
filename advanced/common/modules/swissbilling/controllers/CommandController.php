<?php

namespace common\modules\swissbilling\controllers;

use common\modules\order\models\Order;
use common\modules\order\models\Status;
use common\modules\payment\helper\Method;
use common\modules\swissbilling\facades\InvoiceFacade;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use yii\console\Controller;
use yii\helpers\Console;

class CommandController extends Controller
{
	private $count = [];

	public function actionCancelDeclinedOrders()
	{
		$this->initStatistics();
		$this->cancelOrders();
		$this->printStatistics();
	}

	private function initStatistics()
	{
		$this->count['success'] = 0;
		$this->count['error'] = 0;
		$this->count['fail'] = 0;
	}

	private function cancelOrders()
	{
		$ordersQuery = Order::find()->where([
			'payment_method' => Method::ACCOUNT_PURCHASE,
			'status' => Status::DECLINED
		])->orderBy(['id' => SORT_DESC]);
		$invoiceFacade = new InvoiceFacade();
		$progress = new ProgressBar(new ConsoleOutput());

		$progress->start($ordersQuery->count());

		foreach ($ordersQuery->select('id')->column() as $orderId) {
			$response = $invoiceFacade->actionCancel($orderId);
			$progress->advance();
			$this->handleResponse($response);
		}

		$progress->finish();
	}

	private function handleResponse($response)
	{
		if ($response['success']) {
			$this->count['success']++;
		} elseif (isset($response['error'])) {
			$this->stdout(
				PHP_EOL . "[SwissBilling] Die Bestellung konnte aufgrund eines Fehlers nicht abgelehnt werden."
				. PHP_EOL . "Fehlermeldung: \"{$response['error']}\"",
				Console::FG_RED
			);
			$this->count['error']++;
		} else {
			$this->stdout(
				PHP_EOL . "[SwissBilling] Die Bestellung konnte aufgrund eines unbekannten Fehlers nicht abgelehnt werden."
				. PHP_EOL . "Details sind in der Datenbank gespeichert.",
				Console::FG_RED
			);
			$this->count['fail']++;
		}
	}

	private function printStatistics()
	{
		$this->stdout(
			PHP_EOL .
			"Successfully cancelled: {$this->count['success']}" . PHP_EOL .
			"Failed due to errors: {$this->count['error']}" . PHP_EOL .
			"Failed for unknown reasons: {$this->count['fail']}"
		);
	}
}
