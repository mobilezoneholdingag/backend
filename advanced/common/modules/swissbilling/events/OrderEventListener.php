<?php

namespace common\modules\swissbilling\events;

use common\modules\order\events\OrderStatusDeclinedEvent;
use common\modules\order\models\Order;
use common\modules\payment\helper\Method;
use common\modules\swissbilling\facades\InvoiceFacade;
use deinhandy\yii2events\listener\AbstractEventListener;
use Yii;

class OrderEventListener extends AbstractEventListener
{

	public function getClassEvents() : array
	{
		return [
			Order::class => [
				Order::EVENT_AFTER_ORDER_STATUS_DECLINED => [$this, 'handleOrderStatusDeclined'],
			],
		];
	}

	public function handleOrderStatusDeclined(OrderStatusDeclinedEvent $event)
	{
		/** @var $order Order */
		$order = $event->getOrder();

		if ($order->payment_method !== Method::ACCOUNT_PURCHASE) {
			return;
		}

		$response = (new InvoiceFacade())->actionCancel($order->id);

		if ($response['success']) {
			Yii::$app->session->addFlash('success', "[SwissBilling] Die Bestellung wurde erfolgreich abgelehnt.");
		} elseif (isset($response['error'])) {
			Yii::$app->session->addFlash(
				'danger',
				"[SwissBilling] Die Bestellung konnte aufgrund eines Fehlers nicht abgelehnt werden. <br>" .
				"Fehlermeldung: \"{$response['error']}\""
			);
		} else {
			Yii::$app->session->addFlash(
				'error',
				"[SwissBilling] Die Bestellung konnte aufgrund eines unbekannten Fehlers nicht abgelehnt werden. <br>" .
				"Details sind in der Datenbank gespeichert."
			);
		}
	}
}
