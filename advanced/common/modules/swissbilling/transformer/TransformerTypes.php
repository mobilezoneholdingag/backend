<?php

namespace common\modules\swissbilling\transformer;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Coupon;
use common\models\Tariffs;

class TransformerTypes
{
	const TYPES = [
		ArticleTariffs::class => ArticleTariffsToItemTransformer::class,
		Article::class => ArticleToItemTransformer::class,
		Coupon::class => CouponToItemTransformer::class,
		Tariffs::class => TariffsToItemTransformer::class,
	];
}
