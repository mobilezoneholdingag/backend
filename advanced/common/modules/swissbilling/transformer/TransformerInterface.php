<?php
namespace common\modules\swissbilling\transformer;

use common\modules\swissbilling\models\Item;

interface TransformerInterface
{
	/**
	 * @param object $object
	 *
	 * @return Item[]
	 */
	public static function transform($object);
}
