<?php

namespace common\modules\swissbilling\transformer;

use common\models\Article;
use common\models\ArticleTariffs;
use common\modules\swissbilling\models\Item;

class ArticleTariffsToItemTransformer implements TransformerInterface
{
	/**
	 * @param ArticleTariffs $articleTariff
	 *
	 * @return Item[]
	 */
	public static function transform($articleTariff)
	{
		/** @var Article $article */
		$article = $articleTariff->article;
		$imageUrl = $article->getSmallImageUrl();
		$imageType = substr($imageUrl, strrpos($imageUrl, '.') + 1);

		$items = [];

		if($article->id != Article::SIM_ONLY_ID) {
			$items[] = new Item(
				$article->product_name,
				$article->uniqueTitle,
				1,
				$articleTariff->price_article_once,
				$imageUrl,
				$imageType,
				$article->id
			);
		}

		if ($articleTariff->needsSimCardOrIsSwisscom()) {
			$simCard = $article = $articleTariff->simCard;
			$items[] = new Item(
				$simCard->product_name,
				$simCard->uniqueTitle,
				1,
				$simCard->price_sale
			);
		}

		return $items;
	}
}
