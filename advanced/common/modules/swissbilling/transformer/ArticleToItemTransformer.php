<?php

namespace common\modules\swissbilling\transformer;

use common\models\Article;
use common\modules\swissbilling\models\Item;

class ArticleToItemTransformer implements TransformerInterface
{
	/**
	 * @param Article $article
	 *
	 * @return Item[]
	 */
	public static function transform($article)
	{
		$imageUrl = $article->imageUrl;
		$imageType = substr($imageUrl, strrpos($imageUrl, '.') + 1);
		return [
			new Item(
				$article->product_name,
				$article->uniqueTitle,
				1,
                ($article->price_discounted != null) ? $article->price_discounted : $article->price_sale,
				$imageUrl,
				$imageType
			)
		];
	}
}
