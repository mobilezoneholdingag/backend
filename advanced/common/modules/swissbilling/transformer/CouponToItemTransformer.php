<?php

namespace common\modules\swissbilling\transformer;

use common\models\Coupon;
use common\modules\swissbilling\models\Item;

class CouponToItemTransformer implements TransformerInterface
{
	/**
	 * @param Coupon $coupon
	 *
	 * @return Item[]
	 */
	public static function transform($coupon)
	{
		return [
			new Item(
				'Coupon: ' . $coupon->code,
				$coupon->code,
				1,
				-floatval($coupon->discount_device_price)
			)
		];
	}
}
