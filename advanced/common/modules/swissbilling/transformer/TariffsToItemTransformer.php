<?php

namespace common\modules\swissbilling\transformer;

use common\models\Tariffs;
use common\modules\swissbilling\models\Item;

class TariffsToItemTransformer implements TransformerInterface
{
	/**
	 * @param Tariffs $tariff
	 *
	 * @return Item[]
	 */
	public static function transform($tariff)
	{
		$imageUrl = $tariff->getSmallImageUrl();
		$imageType = substr($imageUrl, strrpos($imageUrl, '.') + 1);

		return [
			new Item(
				$tariff->uniqueTitle,
				$tariff->uniqueTitle,
				1,
				0.0,
				$imageUrl,
				$imageType
			)
		];
	}
}
