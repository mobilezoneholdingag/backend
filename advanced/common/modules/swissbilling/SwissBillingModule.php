<?php

namespace common\modules\swissbilling;

use common\modules\swissbilling\events\OrderEventListener;
use deinhandy\yii2container\Module;
use Yii;
use yii\console\Application;

class SwissBillingModule extends Module
{
	public function init()
	{
		parent::init();
		if (Yii::$app instanceof Application) {
			$this->controllerNamespace = 'common\modules\swissbilling\controllers';
		}
		new OrderEventListener();
	}
}
