<?php

namespace common\modules\swissbilling\facades;

use common\models\Article;
use common\models\Coupon;
use common\models\Customer;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use common\modules\order\models\Status;
use common\modules\swissbilling\builder\DebtorBuilder;
use common\modules\swissbilling\exceptions\SwissBillingApiException;
use common\modules\swissbilling\exceptions\TimeoutException;
use common\modules\swissbilling\models\Debtor;
use common\modules\swissbilling\models\Transaction;
use common\modules\swissbilling\services\ApiClient;
use common\modules\swissbilling\SwissBillingModule;
use common\modules\swissbilling\transformer\TransformerTypes;
use DateTime;
use Exception;
use InvalidArgumentException;
use Yii;
use yii\db\ActiveQuery;

class InvoiceFacade
{
	/** @var array */
	private $params;
	/** @var ApiClient */
	private $apiClient;
	/** @var array */
	private $moduleParams;

	public function __construct(array $params = [], $extraData = [])
	{
		$this->params = array_merge($params, $extraData);
		$this->apiClient = di(ApiClient::class);
		$this->moduleParams = SwissBillingModule::getInstance()->params;
	}

	public function actionPreScreening(): array
	{
		$debtor = $this->getDebtor();
		$articleIds = $this->getArticleIds();
		$items = $this->getItemsByIds($articleIds);
		$transaction = new Transaction(
			microtime(true),
			new DateTime(),
			$this->getTotalPrice(),
			$this->params['customer_ip'] ?? null,
			$this->getCouponAmount($articleIds['coupon'] ?? [])
		);

		try {
			$response = $this->apiClient->EshopTransactionPreScreening($transaction, $debtor, $items);
		} catch (TimeoutException $e) {
			return ['success' => false];
		} catch (SwissBillingApiException $e) {
			Yii::error($e, 'swissbilling');
			return ['success' => false];
		}

		if (($response->EshopTransactionPreScreeningResult->status ?? null) !== 'Answered') {
			return $this->getErrorMessage($response->EshopTransactionPreScreeningResult ?? null);
		}

		return ['success' => true];
	}

	public function actionDirect(): array
	{
		$debtor = $this->getDebtor();
		$articleIds = $this->getArticleIds();
		$items = $this->getItemsByIds($articleIds);
		$order = $this->getOrder($this->getOrderId());
		$transaction = new Transaction(
			$order->id,
			DateTime::createFromFormat('Y-m-d H:i:s', $order->created_at),
			$this->getOrderPricesWithoutCommissionArticle($order),
			$this->params['customer_ip'] ?? null,
			$this->getCouponAmount($articleIds['coupon'] ?? [])
		);

		try{
			$response = $this->apiClient->EshopTransactionDirect($transaction, $debtor, $items);
		} catch (TimeoutException $e) {
			return ['success' => false];
		}

		if (($response->status ?? null) !== $this->moduleParams['transaction']['DirectSuccessStatus']) {
			return $this->getErrorMessage($response);
		}

		return ['success' => true];
	}

	private function getOrderPricesWithoutCommissionArticle(Order $order)
	{
		// find commission article
		$commissionArticlePrice = 0;
		foreach ($order->items as $item) {
			if ($item->item_product_type_id == Item::TYPE_ARTICLE && $item->item_id == Article::COMMISSION_ARTICLE_ID) {
				$commissionArticlePrice = $item->price_once;
			}
		}

		return $order->prices->getPriceOnce()->getValue() - $commissionArticlePrice;
	}

	public function actionAcknowledge($orderId): array
	{
		$order = $this->getOrder($orderId);
		$timestamp = DateTime::createFromFormat('Y-m-d H:i:s', $order->created_at);

		try{
			$response = $this->apiClient->EshopTransactionAcknowledge(
				$order->id,
				$timestamp->format(ApiClient::DATETIME_FORMAT)
			);
		} catch (TimeoutException $e) {
			return ['success' => false, 'error' => Status::SWISSBILLING_API_ERROR];
		}

		if (($response->EshopTransactionAcknowledgeResult->status ?? null) !== 'Acknowledged') {
			return $this->getErrorMessage($response->EshopTransactionAcknowledgeResult);
		}

		return ['success' => true];
	}

	public function actionCancel($orderId): array
	{
		$order = $this->getOrder($orderId);
		$timestamp = DateTime::createFromFormat('Y-m-d H:i:s', $order->created_at);

		$response = $this->apiClient->EshopTransactionCancel(
			$order->id,
			$timestamp->format(ApiClient::DATETIME_FORMAT)
		);

		if (($response->EshopTransactionCancelResult->status ?? null) !== 'Canceled by merchant') {
			return $this->getErrorMessage($response->EshopTransactionCancelResult ?? $response);
		}

		return ['success' => true];
	}

	public function actionStatusRequest($orderId)
	{
		$order = $this->getOrder($orderId);
		$timestamp = DateTime::createFromFormat('Y-m-d H:i:s', $order->created_at);

		return $this->apiClient->EshopTransactionStatusRequest(
			$order->id,
			$timestamp->format(ApiClient::DATETIME_FORMAT)
		);
	}

	private function getDebtor(): Debtor
	{
		$customerId = $this->params['customer_id'] ?? false;
		if (!$customerId) {
			throw new InvalidArgumentException("missing argument 'customer_id'");
		}
		$customer = $this->findCustomer($customerId);

		return DebtorBuilder::build($customer, $this->params['billing_address'], $this->params['delivery_address'] ?? []);
	}

	protected function findCustomer($id): Customer
	{
		if (($model = Customer::findOne($id)) !== null) {
			return $model;
		}
		throw new InvalidArgumentException("Customer #$id not found");
	}

	private function getArticleIds(): array
	{
		$articleIds = $this->params['items'] ?? false;
		if (!$articleIds) {
			throw new InvalidArgumentException();
		}

		return $articleIds;
	}

	private function getItemsByIds(array $articleIds): array
	{
		$items = [];
		$articleTypes = $this->moduleParams['articleTypes'];

		foreach ($articleIds as $type => $ids) {
			if (!isset($articleTypes[$type])) {
				throw new Exception("Article type \"$type\" is not configured.");
			}
			/** @var ActiveQuery $query */
			$query = call_user_func([$articleTypes[$type], 'find']);

			foreach ($ids as $articleId) {
				if ($articleId == Article::COMMISSION_ARTICLE_ID) {
					continue;
				}
				$article = $query->where(['id' => $articleId])->one();
				$items[] = call_user_func([TransformerTypes::TYPES[$article::className()], 'transform'], $article);
			}
		}

		return $items;
	}

	private function getTotalPrice(): float
	{
		if (isset($this->params['total'])) {
			$total = $this->params['total'] - $this->params['commission'] ?? 0.0;
		} else {
			throw new InvalidArgumentException("missing argument 'total'");
		}

		return floatval($total);
	}

	private function getCouponAmount(array $couponIds): float
	{
		$total = 0.0;
		if (empty($couponIds)) {
			return $total;
		}

		/** @var Coupon[] $coupons */
		$coupons = Coupon::find()->where(['in', 'id', $couponIds])->all();

		foreach ($coupons as $coupon) {
			$total += floatval($coupon->discount_device_price);
		}

		return $total;
	}

	private function getErrorMessage($soapRequestResult): array
	{
		$errorText = $soapRequestResult->failure_text_merchant ?? 'NO ERROR TEXT';
		$errorCode = $soapRequestResult->failure_code ?? 'UNKNOWN';

		return [
			'error' => "{$errorText} (Error-Code: {$errorCode})" ?? Yii::t('app', 'Invoice Error'),
			'success' => false,
		];
	}

	private function getOrder(int $orderId): Order
	{
		if (($order = Order::findOne($orderId)) !== null) {
			return $order;
		}
		throw new InvalidArgumentException("Order #$orderId not found");
	}

	private function getOrderId()
	{
		$orderId = $this->params['order_id'] ?? false;
		if (!$orderId) {
			throw new InvalidArgumentException("Missing argument 'order_id'");
		}

		return intval($orderId, 10);
	}
}
