<?php

namespace common\modules\swissbilling\builder;

use common\models\Customer;
use common\modules\swissbilling\models\Debtor;
use DateTime;

class DebtorBuilder
{
	public static function build(Customer $customer, array $billingAddress, array $deliveryAddress)
	{
		$birthdate = DateTime::createFromFormat('Y-m-d', $billingAddress['birthday'] ?? $customer->birthday ?? null) ?: null;

		if (empty($deliveryAddress)) {
			$deliveryAddress = $billingAddress;
		}

		return new Debtor(
			$billingAddress['firstname'],
			$billingAddress['lastname'],
			$billingAddress['street'] . ' ' . $billingAddress['houseNumber'],
			$billingAddress['city'],
			$billingAddress['zip'],
			$customer->email,
			$customer->languageUppercase,
			$customer->id,
			$deliveryAddress['firstname'],
			$deliveryAddress['lastname'],
			$deliveryAddress['street'] . ' ' . $deliveryAddress['houseNumber'],
			$deliveryAddress['city'],
			$deliveryAddress['zip'],
			$birthdate,
			$billingAddress['contactPhoneNumber']
		);
	}
}
