<?php

/** @var \deinhandy\yii2container\Container $container */

$container = Yii::$container;

$container->set(\common\modules\swissbilling\services\ApiClient::class, function(\deinhandy\yii2container\Container $container) {
	/** @var \common\modules\apilogger\services\Logger $apiLogger */
	$apiLogger = $container->get(
		\common\modules\apilogger\services\Logger::class,
		[\common\modules\swissbilling\models\SwissBillingApiLog::class]
	);
	return new \common\modules\swissbilling\services\ApiClient($apiLogger);
});
$container->addTags(\common\modules\swissbilling\events\OrderEventListener::class, ['events.listener']);
