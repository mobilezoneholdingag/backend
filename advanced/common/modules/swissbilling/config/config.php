<?php

return [
	'defaultRoute' => 'invoice/test',
	'params' => [
		'wsdl' => [
			'v2' => 'https://demo-shopssl.swissbilling.ch/wsdl/EShopRequestV2_SSL.wsdl',
			'v3' => 'https://demo-wsssl.swissbilling.ch/ws/EshopRequestV3.svc?WSDL',
		],
		'merchant' => [
			'id' => '201611007_TEST',
			'pwd' => 'ZiRMoPE53H3kytp',
		],
		'transaction' => [
			'type' => 'Real',
			'is_B2B' => false,
			'eshop_ID' => 'mzch',
			'currency' => 'CHF',
			'admin_fee_amount' => 0,
			'delivery_fee_amount' => 0,
			'vol_discount' => 0,
			'phys_delivery' => true,
			'is_DirectInvoiceByEmail' => true,
			 // possible values: "Acknowledged" (skip third step) or "Answered"
			'DirectSuccessStatus' => 'Answered',
		],
		'debtor' => [
			'country' => 'CH',
			'deliv_country' => 'CH',
		],
		'articleTypes' => [
			'accessories' => \common\models\Article::class,
			'article' => \common\models\Article::class,
			'tariff' => \common\models\Tariffs::class,
			'coupon' => \common\models\Coupon::class,
			'article_tariff' => \common\models\ArticleTariffs::class,
		]
	],
];
