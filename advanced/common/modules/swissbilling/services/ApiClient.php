<?php
namespace common\modules\swissbilling\services;

use common\modules\apilogger\services\Logger;
use common\modules\swissbilling\exceptions\SwissBillingApiException;
use common\modules\swissbilling\exceptions\TimeoutException;
use common\modules\swissbilling\models\Debtor;
use common\modules\swissbilling\models\Item;
use common\modules\swissbilling\models\Merchant;
use common\modules\swissbilling\models\SwissBillingApiLog;
use common\modules\swissbilling\models\Transaction;
use common\modules\swissbilling\SwissBillingModule;
use Exception;
use Yii;

class ApiClient extends \SoapClient
{
	const DEFAULT_VERSION = 'v3';
	const DATETIME_FORMAT = \DateTime::ATOM;
	const REQUEST_TIMEOUT = 10;
	/** @var array */
	private $params;
	/** @var Merchant */
	private $merchant;
	/** @var array */
	private $arguments;
	/** @var array */
	private $options;
	/** @var string */
	private $version;
	/** @var Logger|SwissBillingApiLog */
	private $logger;
	/** @var bool */
	private $wsdlAvailable;

	public function __construct(Logger $logger, array $options = [])
	{
		ini_set("default_socket_timeout", self::REQUEST_TIMEOUT);
		$this->logger = $logger;
		$this->options = array_merge($options, [
			'trace' => 1,
			'exceptions' => 0,
		]);
		$this->params = SwissBillingModule::getInstance()->params;
		$this->merchant = new Merchant(...array_values($this->params['merchant']));
		$this->arguments['merchant'] = $this->merchant->jsonSerialize();
		$this->wsdlAvailable = $this->isWsdlAvailable();
		$this->setVersion();
	}

	private function setVersion(string $version = self::DEFAULT_VERSION)
	{
		$this->version = $version;
		$wsdl = $this->params['wsdl'][$this->version];

		if (!$this->wsdlAvailable) {
			return;
		}

		parent::__construct($wsdl, $this->options);
	}

	private function isWsdlAvailable()
	{
		try {
			stream_context_set_default([
				'ssl' => [
					'verify_peer' => false,
					'verify_peer_name' => false,
				],
			]);
			$wsdl = $this->params['wsdl'][$this->version ?? self::DEFAULT_VERSION];
			list($statusCode) = get_headers($wsdl);
		} catch(Exception $e) {
			Yii::error($e);

			return false;
		}

		return strpos($statusCode, "200 OK") !== false;
	}

	public function __soapCall(
		$function_name,
		$arguments,
		$options = null,
		$input_headers = null,
		&$output_headers = null
	) {
		$arguments = array_merge($this->arguments, $arguments);

		if (!$this->wsdlAvailable) {
			return null;
		}

		$response = parent::__soapCall(
			$function_name,
			$this->version === self::DEFAULT_VERSION ? [$arguments] : $arguments,
			$options,
			$input_headers,
			$output_headers
		);

		if ($response instanceof \SoapFault) {
			if ($response->getMessage() === 'Error Fetching http headers') {
				$this->logger->timeout();

				throw new TimeoutException();
			}

			try {
				$this->logger->log("Error {$response->getCode()}", json_encode($response));
			} catch (Exception $e) {
				Yii::error('Error logging failed: ' . $e->getMessage(), 'swissbilling');
			}

			throw new SwissBillingApiException($response->getMessage());
		}

		$this->logger->log($this->__getLastResponseHeaders(), json_encode($response));

		return $response;
	}

	public function __doRequest($request, $location, $action, $version, $one_way = 0)
	{
		$this->logger->start([
			'request' => $request,
			'location' => $location,
			'action' => $action,
			'client-version' => $this->version,
		], $action, 'POST');

		return parent::__doRequest($request, $location, $action, $version, $one_way);
	}

	public function EshopTransactionPreScreening(
		Transaction $transaction,
		Debtor $debtor,
		array $items
	) {
		return $this->__soapCall(__FUNCTION__, [
			'transaction' => $transaction->jsonSerialize(),
			'debtor' => $debtor->jsonSerialize(),
			'item_count' => count($items),
			'arrayofitems' => ['items' => $this->itemsToArray($items)],
		]);
	}

	public function EshopTransactionDirect(
		Transaction $transaction,
		Debtor $debtor,
		array $items
	) {
		$this->setVersion('v2');
		$items = $this->itemsToArray($items);
		$this->logger->order_id = $transaction->eshop_ref;
		return $this->__soapCall(__FUNCTION__, [
			'transaction' => $transaction->jsonSerialize(),
			'debtor' => $debtor->jsonSerialize(),
			'count' => count($items),
			'items' => $items,
		]);
	}

	public function EshopTransactionStatusRequest(string $transactionReference, string $orderTimestamp)
	{
		$this->setVersion('v2');
		return $this->__soapCall(__FUNCTION__, [
			'transaction_ref' => $transactionReference,
			'order_timestamp' => $orderTimestamp,
		]);
	}

	public function EShopTransactionRequest(
		Transaction $transaction,
		Debtor $debtor,
		array $items
	) {
		$this->setVersion('v2');
		return $this->__soapCall(__FUNCTION__, [
			'transaction' => $transaction->jsonSerialize(),
			'debtor' => $debtor->jsonSerialize(),
			'count' => count($items),
			'items' => $this->itemsToArray($items),
		]);
	}

	public function EshopTransactionConfirmation(string $transactionReference, string $orderTimestamp)
	{
		$this->setVersion('v2');
		$this->logger->order_id = (int)$transactionReference;
		return $this->__soapCall(__FUNCTION__, [
			'transaction_ref' => $transactionReference,
			'order_timestamp' => $orderTimestamp,
		]);
	}

	public function EshopTransactionAcknowledge(string $transactionReference, string $orderTimestamp)
	{
		$this->logger->order_id = (int)$transactionReference;
		return $this->__soapCall(__FUNCTION__, [
			'transaction_ref' => $transactionReference,
			'timestamp' => $orderTimestamp,
		]);
	}

	public function EshopTransactionCancel(string $transactionReference, string $orderTimestamp)
	{
		$this->logger->order_id = (int)$transactionReference;
		return $this->__soapCall(__FUNCTION__, [
			'transaction_ref' => $transactionReference,
			'timestamp' => $orderTimestamp,
		]);
	}

	private function itemsToArray($items): array
	{
		$return = [];

		/** @var Item $item */
		foreach ($items as $item) {
			if (is_array($item)) {
				$return = array_merge($return, $this->itemsToArray($item));
			} else {
				$return[] = $item->jsonSerialize();
			}
		}

		return $return;
	}
}
