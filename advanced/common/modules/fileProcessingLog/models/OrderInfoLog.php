<?php
namespace common\modules\fileProcessingLog\models;

class OrderInfoLog extends FileProcessingLog
{
	public static function tableName()
	{
		return 'order_info_file_processing_log';
	}
}