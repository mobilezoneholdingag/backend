<?php
namespace common\modules\fileProcessingLog\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property string $filename
 * @property string $file_content
 * @property string $processed_at
 */
abstract class FileProcessingLog extends ActiveRecord
{
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['processed_at'],
				],
				'value' => new expression('NOW()'),
			],
		];
	}

	public function rules()
	{
		return [
			[['filename', 'file_content'], 'required'],
			[['filename'], 'string', 'max' => 255],
			[['file_content', 'processed_at'], 'safe'],
		];
	}

	public function log($filename, $fileContent)
	{
		$this->prepareLog($filename, $fileContent);
		return $this->save();
	}

	public function prepareLog($filename, $fileContent)
	{
		$this->filename = $filename;
		$this->file_content = $fileContent;
	}
}
