<?php

namespace common\modules\mobilezoneErp\lib;

use common\models\CustomerAddress;
use common\modules\order\models\Address;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use common\modules\payment\helper\Method;
use common\modules\postfinance\models\EcomCallback;
use DateTime;
use DateTimeZone;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use RuntimeException;

class OrderDecorator
{
    const SWISSCOM_SPECIAL_SIM_CARD_ID = 999999;
    const COMMISSION_ARTICLE_ID = 12529;
    const COUPON_ARTICLE_ID = 90025085;

    private $paymentMethodMapping = [
        Method::ACCOUNT_PURCHASE => 'swissbilling',
        Method::POSTFINANCE_E_FINANCE => 'efinance',
        Method::POSTFINANCE_CARD => 'postcard',
        'VISA' => 'visa',
        'MasterCard' => 'mastercard',
        'American Express' => 'americanexpress',
        Method::PREPAYMENT => 'prepaid',
    ];

    /**
     * @var Order
     */
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function decorate() : string
    {
        $this->validateOrder();
        // language=html
        $xml = '<?xml version="1.0"?>
			<mz:order 
				xmlns:mz="http://www.mobilezone.ch" 
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				xsi:schemaLocation="http://www.mobilezone.ch order.xsd" 
				id="' . $this->order->id . '" 
				type="create" 
				ordered_at="' . $this->getOrderTime() . '" 
				status="paid">
				<note />
				' . $this->renderCustomerPart() . '
				' . static::renderContactAddressPart($this->order->billingAddress) . '
				' . $this->renderShippingAddressPart() . '
				' . $this->renderPaymentPart() . '
				' . $this->renderDeliveryPart() . '
				' . $this->renderOrderItems() . '
			</mz:order>';

        // Mobilezone requires the XML with no line breaks and no spaces between the nodes
        $xml = str_replace(["\r", "\n"], '', $xml);
        $xml = preg_replace('/\s+/', ' ', $xml);
        $xml = str_replace('> <', '><', $xml);

        $this->order->xml_content = $xml;
        $this->order->save();

        return $xml;
    }

    public function validateOrder()
    {
        if ($this->order->hasTariffItem()) {
            if (empty($this->order->msisdn) || empty($this->order->contract_number)) {
                throw new RuntimeException("Can not generate order xml for order {$this->order->id},"
                    . " because the order has an articleTariff but msisdn or contract_number of the order is empty.");
            }
        }

        if ($this->order->hasSimcardItem() && empty($this->order->simcard_number)) {
            throw new RuntimeException("Can not generate order xml for order {$this->order->id},"
                . " because the order has a sim card item but simcard_number of the order is empty.");
        }
    }

    private function getOrderTime()
    {
        $orderTime = new DateTime($this->order->created_at);
        $orderTime->setTimezone(new DateTimeZone('Europe/Zurich'));
        return str_replace('+', '.000+', $orderTime->format(DateTime::RFC3339));
    }

    public function renderCustomerPart()
    {
        return '
			<customer external_id="9000">
				<group><![CDATA[Private]]></group>
			</customer>';
    }

    public static function renderContactAddressPart($address = null)
    {
        if (!$address) {
            return '';
        }

        return '
			<contract_address>
				' . self::renderAddressSubElements($address) . '
			</contract_address>
		';
    }

    /**
     * @param Address|CustomerAddress $address
     *
     * @return string
     */
    public static function renderAddressSubElements($address)
    {
        return '
			<salutation>' . ($address->sex == Address::SEX_MALE ? 'male' : 'female') . '</salutation>
			<first_name><![CDATA[' . $address->firstname . ']]></first_name>
			<name><![CDATA[' . $address->lastname . ']]></name>
			<company_name />
			<street><![CDATA[' . $address->street . ' ' . $address->house_number . ']]></street>
			<extra_line />
			<zip>' . $address->zip . '</zip>
			<city><![CDATA[' . $address->city . ']]></city>
			<mobile_phone>' . self::getCustomerPhoneNumber($address->contact_phone_number ?? null) . '</mobile_phone>
			<private_phone />
			<business_phone />
			<country>CH</country>';
    }

    public function renderPaymentPart() : string
    {
        $paymentMethod = $this->order->payment_method;
        $paymentMethodCode = '';
        $paymentTokenNode = '';
        $paymentAquirerTokenNode = '';
        $ecomCallback = EcomCallback::find()->where(['order_id' => $this->order->id, 'is_successful' => true])->orderBy(['id' => SORT_ASC])->one();
        if (isset($this->paymentMethodMapping[$paymentMethod])) {
            $paymentMethodCode = $this->paymentMethodMapping[$paymentMethod];
        }

        if ($ecomCallback) {
            if (empty($paymentMethodCode)) {
                $paymentMethodCode = $this->paymentMethodMapping[$ecomCallback->brand] ?? 'visa';
            }
            $paymentTokenNode = "<payment_token>{$ecomCallback->pay_id}</payment_token>";
            $paymentAquirerTokenNode = "<aquirer_token><![CDATA[{$ecomCallback->acceptance}]]></aquirer_token>";
        }

        if (empty($paymentMethodCode)) {
            $paymentMethodCode = 'visa';
        }

        return '
			<payment>
				<type>' . $paymentMethodCode . '</type>
				' . $paymentTokenNode . '
				' . $paymentAquirerTokenNode . '
			</payment>';
    }

    public function renderDeliveryPart() : string
    {
        return '
			<delivery>
				<type>standard</type>
				<partial_delivery>false</partial_delivery>
				<packing_type>standard</packing_type>
			</delivery>
		';
    }

    private static function getCustomerPhoneNumber($phoneNumber)
    {
        if (empty($phoneNumber)) {
            return '';
        }

        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $phoneNumber = $phoneUtil->parse($phoneNumber, 'CH');
            $phoneNumber = '+' . $phoneNumber->getCountryCode() . $phoneNumber->getNationalNumber();
        } catch (NumberParseException $e) {
            throw new RuntimeException(
                'Could not parse customer telephone number into normalized phone number when generating the order xml.',
                0,
                $e
            );
        }
        return $phoneNumber;
    }

    private function getDeliveryStreet() : string
    {
        $deliveryAddress = $this->order->deliveryAddress;
        return $deliveryAddress->street . ' ' . $deliveryAddress->house_number;
    }

    public function renderShippingAddressPart() : string
    {
        if ($this->order->billing_address_id == $this->order->delivery_address_id) {
            return '';
        }
        $address = $this->order->deliveryAddress;
        if (!$address) {
            return '';
        }
        return '
		<shipping_address>
			<salutation>' . ($address->sex == 1 ? 'male' : 'female') . '</salutation>
			<first_name><![CDATA[' . $address->firstname . ']]></first_name>
			<name><![CDATA[' . $address->lastname . ']]></name>
			<company_name />
			<street><![CDATA[' . $this->getDeliveryStreet() . ']]></street>
			<extra_line />
			<zip>' . $address->zip . '</zip>
			<city><![CDATA[' . $address->city . ']]></city>
			<mobile_phone>' . $this->getCustomerPhoneNumber($this->order->billingAddress->contact_phone_number ?? null) . '</mobile_phone>
			<private_phone></private_phone>
			<business_phone />
			<country>CH</country>
		</shipping_address>';
    }

    public function renderOrderItems() : string
    {
        $orderItems = $this->order->allItems;
        $orderItemsXml = '<order_items>';
        foreach ($orderItems as $orderItem) {
            if ($this->order->payment_method == Method::ACCOUNT_PURCHASE && $orderItem->item_id == self::COMMISSION_ARTICLE_ID) {
                continue;
            }
            if ($orderItem->isArticle()) {
                $orderItemsXml .= $this->renderArticleOrderItem($orderItem);
            } elseif ($orderItem->isSimCard()) {
                $orderItemsXml .= $this->renderSimCardItem($orderItem);
            } elseif ($orderItem->isCoupon()) {
                $orderItemsXml .= $this->renderCoupon($orderItem);
            }
        }
        $orderItemsXml .= '</order_items>';

        return $orderItemsXml;
    }

    public function renderOrderItemPricePart(Item $orderItem) : string
    {
        $monthly = $orderItem->price_monthly > 0 ? "<monthly>{$orderItem->price_monthly}</monthly>" : '';
        return "
			<price>
				<currency>CHF</currency>
				<amount>{$orderItem->price_once}</amount>
				{$monthly}
			</price>
		";
    }

    public function renderArticleOrderItem(Item $orderItem) : string
    {
        $xmlNode = '
			<order_item>
				<id>' . $orderItem->id . '</id>
				<amount>1</amount>
		';
        $xmlNode .= "<article external_id=\"{$orderItem->getArticle()->id}\" />";
        $xmlNode .= "<article_number>{$orderItem->getArticle()->mat_no}</article_number>";
        $xmlNode .= $this->renderOrderVoucherNode();
        $associatedTariffItem = isset($orderItem->parentItem) ? $this->getTariffItemFromParentItem($orderItem->parentItem) : null;
        if ($associatedTariffItem !== null && $orderItem->getArticle()->insurance_duration == 0) {
            $xmlNode .= "<contract_number>{$this->order->contract_number}</contract_number>";
            $xmlNode .= "<msisdn>{$this->order->msisdn}</msisdn>";
            $xmlNode .= "<offer external_id=\"{$associatedTariffItem->getTariff()->id}\">{$associatedTariffItem->getTariff()->erp_title}</offer>";
            if ($this->order->hasContractFile()) {
                $xmlNode .= "<contract_url>{$this->order->id}</contract_url>";
            }
        }

        $xmlNode .= $this->renderOrderItemPricePart($orderItem);
        $xmlNode .= "<status>unverified</status>";
        $xmlNode .= "</order_item>";

        return $xmlNode;
    }

    private function getTariffItemFromParentItem(Item $orderItem)
    {
        /** @var Item[] $childItems */
        $childItems = $orderItem->childItems ?? [];
        foreach ($childItems as $childItem) {
            if ($childItem->isTariff()) {
                return $childItem;
            }
        }

        return null;
    }

    public function renderOrderVoucherNode() : string
    {
        return '<vouchercode />';
    }

    public function renderSimCardItem(Item $orderItem) : string
    {
        return '
		<order_item>
			<id>' . $orderItem->id . '</id>
			<amount>1</amount>
			<article external_id="' . $this->getSimCardExternalId($orderItem) . '" />
			<article_number>' . $this->getSimCardArticleNumber($orderItem) . '</article_number>
			<vouchercode />
			<price>
				<currency>CHF</currency>
				<amount>' . $this->getSimCardPrice($orderItem) . '</amount>
			</price>
			<status>unverified</status>
			<serial_number>' . $this->order->simcard_number . '</serial_number>
		</order_item>';
    }

    public function renderCoupon(Item $orderItem): string
    {
        return "
			<order_item>
				<id>{$orderItem->id}</id>
				<amount>1</amount>
				<article external_id=\"" . self::COUPON_ARTICLE_ID . "\" />
				<vouchercode></vouchercode>
				{$this->renderOrderItemPricePart($orderItem)}
				<status>unverified</status>
			</order_item>
		";
    }

    private function getSimCardExternalId(Item $orderItem)
    {
        if ($orderItem->item_id == self::SWISSCOM_SPECIAL_SIM_CARD_ID) {
            return 15706;
        }
        return $orderItem->item_id;
    }

    private function getSimCardArticleNumber(Item $orderItem)
    {
        if ($orderItem->item_id == self::SWISSCOM_SPECIAL_SIM_CARD_ID) {
            return 74015084;
        }
        return $orderItem->getSimCardArticle()->mat_no;
    }

    private function getSimCardPrice(Item $orderItem)
    {
        if ($orderItem->item_id == self::SWISSCOM_SPECIAL_SIM_CARD_ID) {
            return 0;
        }
        return $orderItem->price_once;
    }
}
