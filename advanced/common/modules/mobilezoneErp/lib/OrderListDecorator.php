<?php

namespace common\modules\mobilezoneErp\lib;

use common\models\Customer;
use common\modules\order\models\Order;

class OrderListDecorator
{
    /** @var Order[] */
    private $orders;

    public function __construct(array $orders)
    {
        $this->orders = $orders;
    }

    public function decorate() : string
    {

        $xml = '<?xml version="1.0"?>
			<mz:orders 
				xmlns:mz="http://www.mobilezone.ch" 
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				xsi:schemaLocation="http://www.mobilezone.ch order.xsd">
			';

        foreach ($this->orders as $order) {
            $decorator = new OrderDecorator($order);
            try {
                $decorator->validateOrder();
            } catch (\Exception $e) {
                continue;
            }
            $xml .= '<mz:order>';
            $xml .= $this->renderOrderPart($order);
            $xml .= $this->renderCustomerPart($order->customer);
            $xml .= $decorator->renderContactAddressPart($order->billingAddress);
            $xml .= $decorator->renderShippingAddressPart();
            $xml .= $decorator->renderPaymentPart();
            $xml .= $decorator->renderDeliveryPart();
            $xml .= $decorator->renderOrderItems();
            $xml .= '</mz:order>';
        }
        $xml .= '</mz:orders>';

        $xml = str_replace(["\r", "\n"], '', $xml);
        $xml = preg_replace('/\s+/', ' ', $xml);
        $xml = str_replace('> <', '><', $xml);

        return $xml;
    }

    private function renderOrderPart(Order $order)
    {
        return "
			<id>{$order->id}</id>
			<type><![CDATA[create]]></type>
			<ordered_at>{$order->created_at}</ordered_at>
			<last_changed>{$order->updated_at}</last_changed>
			<status><![CDATA[{$order->statusCodeLabel}]]></status>
			<shipment_id>{$order->shipment_tracking_ids}</shipment_id>
			<amount>
				<total>{$order->price_once}</total>
				<monthly>{$order->price_monthly}</monthly>
			</amount>
			<offer_id></offer_id>
		";
    }

    public function renderCustomerPart(Customer $customer)
    {
        return "
			<customer external_id=\"9000\">
				<group><![CDATA[Private]]></group>
				<id>{$customer->id}</id>
				<language><![CDATA[{$customer->language}]]></language>
				<email><![CDATA[{$customer->email}]]></email>
				<nationality><![CDATA[{$customer->nationality}]]></nationality>
				<passport><![CDATA[{$customer->passport_id}]]></passport>
				<birthday><![CDATA[{$customer->birthday}]]></birthday>
			</customer>
		";
    }
}
