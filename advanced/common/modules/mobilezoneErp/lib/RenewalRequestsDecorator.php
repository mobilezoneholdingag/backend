<?php

namespace common\modules\mobilezoneErp\lib;

use common\modules\renewalRequest\models\RenewalRequest;
use common\modules\renewalRequest\models\RenewalRequestOffers;
use yii\db\ActiveQuery;

class RenewalRequestsDecorator
{
	/** @var RenewalRequest[] */
	private $renewalRequests;
	/** @var RenewalRequest */
	private $currentRenewalRequest;

	public function __construct(array $renewalRequests)
	{
		$this->renewalRequests = $renewalRequests;
	}

	public function decorate() : string
	{
		$xml = "<?xml version='1.0'?>
			<mz:offers xmlns:mz='http://www.mobilezone.ch/'>
				<note />
				{$this->renderOffers()}
			</mz:offers>";

		$xml = str_replace(["\r", "\n"], '', $xml);
		$xml = preg_replace('/\s+/', ' ', $xml);
		$xml = str_replace('> <', '><', $xml);

		return $xml;
	}

	private function renderOffers(): string
	{
		$xml = '';
		foreach ($this->renewalRequests as $renewalRequest) {
			$this->currentRenewalRequest = $renewalRequest;
			$xml .= "
				<offer>
					{$this->renderOfferInfoPart()}
					{$this->renderCustomerPart()}
					{$this->renderAddressPart()}
					{$this->renderRenewalRequestPart()}
					{$this->renderRenewalRequestOffersPart()}
				</offer>
			";
		}
		return $xml;
	}

	private function renderOfferInfoPart(): string
	{
		$order = $this->currentRenewalRequest->order;
		return "
			{$this->attribute('id', $this->currentRenewalRequest->id)}
			{$this->attribute('type', 'create')}
			{$this->attribute('last_changed', $order->updated_at ?? '')}
			{$this->attribute('status', $this->currentRenewalRequest->status)}
			{$this->attribute('order_id', $this->currentRenewalRequest->order_id ?? '')}
			{$this->attribute('order_status', $order->statusCodeLabel ?? '')}
		";
	}

	private function renderCustomerPart(): string
	{
		$customer = $this->currentRenewalRequest->customer;
		return "
			<customer>
				{$this->attribute('id', $customer->id)}
				{$this->attribute('language', $customer->language)}
				{$this->attribute('email', $customer->email)}
				{$this->attribute('nationality', $customer->nationality)}
				{$this->attribute('passport_type', $customer->passport_type)}
				{$this->attribute('passport', $customer->passport_id)}
				{$this->attribute('birthdate', $customer->birthday)}
			</customer>
		";
	}

	private function renderAddressPart(): string
	{
		$customer = $this->currentRenewalRequest->customer;
		$address = $customer->getDefaultCustomerAddress() ?? current($customer->getCustomerAddresses()->all());

		return OrderDecorator::renderContactAddressPart($address);
	}

	private function renderRenewalRequestPart(): string
	{
		$request = $this->currentRenewalRequest;

		return "
			<renewal_request>
				{$this->attribute('provider', $request->provider->title)}
				{$this->attribute('current_tariff', $request->currentTariff ? $request->currentTariff->title : null)}
				{$this->attribute('cellnumber', $request->phone_number)}
				<tariffpreference>
					{$this->attribute('data_national', $request->monthly_national_data_volume)}
					{$this->attribute('data_international', $request->monthly_international_data_volume)}
					{$this->attribute('talk_national', $request->monthly_national_phone_minutes)}
					{$this->attribute('talk_international', $request->monthly_international_phone_minutes)}
					{$this->attribute('talk_from_international', $request->monthly_phone_minutes_from_abroad)}
				</tariffpreference>
			</renewal_request>
		";
	}

	private function renderRenewalRequestOffersPart(): string
	{
		/** @var RenewalRequestOffers[] $renewalRequestOffers */
		$renewalRequestOffers = $this->currentRenewalRequest->renewalRequestOffers;
		$xml = '';
		foreach ($renewalRequestOffers as $renewalRequestOffer) {
			$xml .= $this->renderRenewalRequestOfferPart($renewalRequestOffer);
		}

		return $xml;
	}

	private function renderRenewalRequestOfferPart(RenewalRequestOffers $renewalRequestOffer): string
	{
		return "
			<renewal_offer>
				{$this->attribute('provider', $renewalRequestOffer->tariff->provider->title)}
				{$this->attribute('tariff_name', $renewalRequestOffer->tariff->title)}
				{$this->attribute('tariff_id', $renewalRequestOffer->tariff_id)}
				{$this->attribute('tariff_duration', $renewalRequestOffer->tariff->contract_duration)}
				{$this->attribute('article_id', $renewalRequestOffer->article_id)}
				{$this->attribute('article_name', $renewalRequestOffer->article->product_name)}
				{$this->attribute('offerlink', $renewalRequestOffer->offerLink)}
			</renewal_offer>
		";
	}

	private function attribute(string $name, string $value = null): string
	{
		$value = preg_replace('/[[:cntrl:]]/', '', $value);
		$value = $value ? "<![CDATA[{$value}]]>" : "";

		return "<{$name}>{$value}</{$name}>";
	}
}
