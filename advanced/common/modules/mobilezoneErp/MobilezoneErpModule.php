<?php

namespace common\modules\mobilezoneErp;

use common\modules\mobilezoneErp\events\OrderEventListener;
use yii\base\Module;

/**
 * order module definition class
 */
class MobilezoneErpModule extends Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'common\modules\mobilezoneErp\controllers';

	public function init()
	{
		parent::init();

		new OrderEventListener();
	}
}
