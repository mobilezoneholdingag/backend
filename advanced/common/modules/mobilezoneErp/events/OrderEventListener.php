<?php

namespace common\modules\mobilezoneErp\events;

use common\modules\mobilezoneErp\lib\OrderDecorator;
use common\modules\order\events\OrderStatusPaymentSuccessfulEvent;
use common\modules\order\models\Order;
use components\services\FtpsService;
use Exception;
use Yii;
use yii\base\Component;
use yii\base\Event;

class OrderEventListener extends Component
{
	public function init()
	{
		Event::on(Order::class, Order::EVENT_AFTER_ORDER_STATUS_PAYMENT_SUCCESSFUL, function(OrderStatusPaymentSuccessfulEvent $event) {
			/** @var Order $order */
			$order = $event->getOrder();
			try {
				$orderXml = (new OrderDecorator($order))->decorate();
			} catch (Exception $e) {
				Yii::error($e, 'order');
				return;
			}

			/** @var FtpsService $ftps */
			$ftps = Yii::$app->get('ftps');
			$ftps->putDataToFtps($orderXml, 'mobilezone-ch-orders/Order_MZ_' . $order->id . '.xml');

			$order->xml_created_at = (new \DateTime())->format('Y-m-d H:i:s');
			$order->save();
		});
	}
}
