<?php

namespace common\modules\platform;

use deinhandy\yii2container\Module;

class PlatformModule extends Module
{
	public $defaultRoute = 'platform/index';
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'common\modules\platform\controllers';
}
