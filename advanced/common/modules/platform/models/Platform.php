<?php
namespace common\modules\platform\models;

use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $name
 */
class Platform extends ActiveRecord
{
	static function tableName()
	{
		return 'platforms';
	}

	public function rules()
	{
		return [
			[['name'], 'required'],
			[['name'], 'string'],
			[['name'], 'unique'],
		];
	}

	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [LoggableBehavior::className()] : [];
	}
}
