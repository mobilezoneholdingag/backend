<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\platform\models\Platform */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Platform',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="platforms-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
