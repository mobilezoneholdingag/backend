<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\platform\models\Platform */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platforms-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?php if (Yii::$app->user->can('starlord')): ?>
			<?= Html::a(
				Yii::t('app', 'Delete'),
				['delete', 'id' => $model->id],
				[
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
						'method' => 'post',
					],
				]
			) ?>
		<?php endif; ?>
	</p>

	<?= DetailView::widget(
		[
			'model' => $model,
			'attributes' => [
				'id',
				'name',
			],
		]
	) ?>

</div>
