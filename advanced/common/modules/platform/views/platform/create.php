<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\platform\models\Platform */

$this->title = Yii::t('app', 'Create {modelClass}', [
	'modelClass' => 'Platforms',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platforms-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
