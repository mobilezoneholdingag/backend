<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\modules\platform\models\Platform */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Platforms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platforms-index">
	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Create new Platform'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			'id',
			'name',
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
			],
	],
	]); ?>

</div>
