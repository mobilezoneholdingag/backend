<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\platform\models\Platform */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="platforms-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'id')->textInput(['disabled' => 'disabled']) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
