<?php

namespace common\modules\payment\models;

use Yii;

class InvoiceMethod extends AbstractPaymentMethod
{
	const FEE = 2.9;

	public function preconditionsFulfilled(): bool
	{
		$accountPurchase = Yii::$app->runAction('swissbilling/invoice/pre-screening');

		return (bool) $accountPurchase['success'];
	}

	public function calculateFee(float $total): float
	{
		return self::FEE;
	}
}
