<?php

namespace common\modules\payment\models;

use common\modules\payment\exceptions\PreconditionsFailedException;
use common\modules\payment\helper\Method;
use InvalidArgumentException;

class PaymentMethodContext
{
	const METHOD_CLASSES = [
		Method::PREPAYMENT => PrePaymentMethod::class,
		Method::CREDIT_CARD => CreditCardMethod::class,
		Method::POSTFINANCE_CARD => PostFinanceMethod::class,
		Method::POSTFINANCE_E_FINANCE => EFinanceMethod::class,
		Method::ACCOUNT_PURCHASE => InvoiceMethod::class,
		Method::NO_PAYMENT_REQUIRED => NoPaymentMethod::class,
	];

	/** @var AbstractPaymentMethod */
	private $method;

	public function __construct(string $paymentMethod)
	{
		if (!in_array($paymentMethod, Method::METHODS)) {
			throw new InvalidArgumentException("Method '$paymentMethod' does not exist!");
		}
		$class = static::METHOD_CLASSES[$paymentMethod];
		$this->method = new $class;
	}

	public function calculateFee(float $totalPrice)
	{
		return round($this->method->calculateFee($totalPrice), 2);
	}

	/**
	 * @throws PreconditionsFailedException
	 */
	public function checkPreconditions()
	{
		if(!$this->method->preconditionsFulfilled()) {
			throw new PreconditionsFailedException();
		}
	}
}
