<?php

namespace common\modules\payment\models;

abstract class AbstractPaymentMethod
{
	const FEE = 0.0;

	public function preconditionsFulfilled(): bool
	{
		return true;
	}

	public function calculateFee(float $total): float
	{
		return $total * static::FEE;
	}
}
