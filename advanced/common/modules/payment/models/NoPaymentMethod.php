<?php

namespace common\modules\payment\models;

class NoPaymentMethod extends AbstractPaymentMethod
{
	const FEE = 0;
}
