<?php

namespace common\modules\payment\exceptions;

use RuntimeException;

class PreconditionsFailedException extends RuntimeException
{
}
