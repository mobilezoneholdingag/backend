<?php

namespace common\modules\payment\helper;

class Method
{
	const ACCOUNT_PURCHASE = 'account-purchase';
	const CREDIT_CARD = 'credit-card';
	const POSTFINANCE_E_FINANCE = 'pf-e-finance';
	const POSTFINANCE_CARD = 'pf-card';
	const PREPAYMENT = 'pre-payment';
	const NO_PAYMENT_REQUIRED = 'no-payment-required';

	const METHODS = [
		self::PREPAYMENT,
		self::CREDIT_CARD,
		self::POSTFINANCE_E_FINANCE,
		self::POSTFINANCE_CARD,
		self::ACCOUNT_PURCHASE,
		self::NO_PAYMENT_REQUIRED,
	];

	const POSTFINANCE_METHODS = [
		Method::CREDIT_CARD,
		Method::POSTFINANCE_E_FINANCE,
		Method::POSTFINANCE_CARD,
	];
}
