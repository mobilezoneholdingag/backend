<?php

namespace common\modules\payment;

use deinhandy\yii2container\Module;
use Yii;
use yii\console\Application;

class PaymentModule extends Module
{
	public function init()
	{
		parent::init();
		if (Yii::$app instanceof Application) {
//			$this->controllerNamespace = 'common\modules\postfinance\controllers';
		}
	}
}
