<?php

namespace common\modules\prepayment\controllers;

use common\modules\homer\models\mails\PrePaymentCancellationMail;
use common\modules\homer\models\mails\PrePaymentReminderMail;
use common\modules\order\models\Order;
use common\modules\order\models\Status;
use yii\console\Controller;

class CommandController extends Controller
{
	public function actionSendReminderMails()
	{
		foreach ($this->findOrdersAwaitingPaymentSince(5) as $order) {
			$mail = new PrePaymentReminderMail();
			$mail->setEntity($order);
			$mail->send();
		}
	}

	public function actionSendCancellationMails()
	{
		foreach ($this->findOrdersAwaitingPaymentSince(12) as $order) {
			$mail = new PrePaymentCancellationMail();
			$mail->setEntity($order);
			$mail->send();
		}
	}

	public function actionCancelOrders()
	{
		foreach ($this->findOrdersAwaitingPaymentSince(15) as $order) {
			$order->status = Status::PAYMENT_FAILED;
			$order->save();
		}
	}

	/**
	 * @param int $days
	 *
	 * @return Order[]|array|\yii\db\ActiveRecord[]
	 */
	private function findOrdersAwaitingPaymentSince(int $days)
	{
		return Order::find()
			->where("DATE(created_at) = DATE(DATE_SUB(NOW(), INTERVAL {$days} DAY))")
			->andWhere(['status' => Status::AWAITING_PREPAYMENT])
			->all();
	}
}
