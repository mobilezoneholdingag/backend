<?php

namespace common\modules\prepayment;

use deinhandy\yii2container\Module;
use Yii;
use yii\console\Application;

class PrePaymentModule extends Module
{
	public function init()
	{
		parent::init();
		if (Yii::$app instanceof Application) {
			$this->controllerNamespace = 'common\modules\prepayment\controllers';
		}
	}
}
