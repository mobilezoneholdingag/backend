<?php
namespace common\modules\swisscom\models\logger;

use Psr\Http\Message\ResponseInterface;
use yii\db\ActiveRecord;

interface CommunicationLoggerInterface
{
	const METHOD_GET = 'get';
	const METHOD_POST = 'post';

	/**
	 * @param string $method
	 * @param string $endpoint
	 * @param array $options
	 * @param integer|null $orderId
	 *
	 * @return mixed
	 */
	public function logRequest($method, $endpoint, $options, $orderId = null);

	/**
	 * @param ResponseInterface $response
	 * @param ActiveRecord|null $requestLog
	 *
	 * @return mixed
	 */
	public function logResponse(ResponseInterface $response, ActiveRecord $requestLog = null);
}
