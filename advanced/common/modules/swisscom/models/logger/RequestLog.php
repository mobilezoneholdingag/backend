<?php
namespace common\modules\swisscom\models\logger;

use common\models\behaviors\TimestampBehaviorSettings;
use yii\db\ActiveRecord;

/**
 * Class RequestLog
 *
 * @package common\modules\swisscom\models\logger
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $endpoint
 * @property string $method
 * @property string $request_options
 *
 * @property ResponseLog $responseLog
 */
class RequestLog extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'swisscom_request';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAt(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['request_options', 'method'], 'required'],
			[['request_options', 'endpoint', 'method'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'endpoint' => 'Endpoint',
			'method' => 'Method',
			'request_options' => 'Request Options',
			'created_at' => 'Created At',
		];
	}

	public function getResponseLog()
	{
		return ResponseLog::find()->where(['request_id' => $this->id]);
	}
}
