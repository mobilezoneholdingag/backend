<?php
namespace common\modules\swisscom\models\logger;

use common\models\behaviors\TimestampBehaviorSettings;
use yii\db\ActiveRecord;

/**
 * Class ResponseLog
 *
 * @package common\modules\swisscom\models\logger
 * @property integer $id
 * @property integer $request_id
 * @property string $response
 *
 */
class ResponseLog extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'swisscom_response';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAt(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'request_id'], 'integer'],
			[['response', 'created_at', 'request_id'], 'required'],
			[['response'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'request_id' => 'Endpoint',
			'response' => 'Response',
			'created_at' => 'Created At',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRequestLog()
	{
		return $this->hasOne(RequestLog::class, ['id' => 'request_id']);
	}
}
