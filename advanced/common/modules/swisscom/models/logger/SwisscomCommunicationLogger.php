<?php
namespace common\modules\swisscom\models\logger;



use Psr\Http\Message\ResponseInterface;
use yii\db\ActiveRecord;

class SwisscomCommunicationLogger implements CommunicationLoggerInterface
{
	private $lastRequestLog;

	/**
	 * @param string $method
	 * @param string $endpoint
	 * @param array $requestOptions
	 * @param integer|null $orderId
	 *
	 * @return RequestLog
	 */
	public function logRequest($method, $endpoint, $requestOptions, $orderId = null)
	{
		$requestLog = new RequestLog();
		$requestLog->order_id = $orderId;
		$requestLog->method = $method;
		$requestLog->endpoint = $endpoint;
		$requestLog->request_options = json_encode($requestOptions);
		$requestLog->save();
		$this->lastRequestLog = $requestLog;
		return $requestLog;
	}

	/**
	 * @return RequestLog|null
	 */
	public function getLastRequestLog()
	{
		return $this->lastRequestLog;
	}

	/**
	 * @param ResponseInterface|string $response
	 * @param ActiveRecord|null $requestLog
	 *
	 * @return ResponseLog
	 */
	public function logResponse(ResponseInterface $response, ActiveRecord $requestLog = null)
	{
		if (is_null($requestLog)) {
			$requestLog = $this->getLastRequestLog();
		}

		$responseParts = [
			'status_code' => $response->getStatusCode(),
			'header' => $response->getHeaders(),
			'body' => json_decode((string)$response->getBody())
		];

		$responseLog = new ResponseLog();
		$responseLog->response = json_encode($responseParts);
		$responseLog->link('requestLog', $requestLog);
		$responseLog->save();
		return $responseLog;
	}
}
