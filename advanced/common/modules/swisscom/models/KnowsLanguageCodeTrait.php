<?php

namespace common\modules\swisscom\models;

use common\modules\swisscom\SwisscomModule;

trait KnowsLanguageCodeTrait
{
	/**
	 * @return string
	 */
	public function getLanguageCode()
	{
		return SwisscomModule::getInstance()->getLanguageAlpha2Code();
	}
}