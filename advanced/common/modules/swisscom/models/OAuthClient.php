<?php

namespace common\modules\swisscom\models;

use common\helper\UrlHelper;
use common\modules\swisscom\models\logger\CommunicationLoggerInterface;
use common\modules\swisscom\SwisscomModule;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\BadResponseException;
use RuntimeException;
use Yii;
use yii\helpers\Url;

/**
 * Client to handle the Swisscom Consent (OAuth2) API.
 */
class OAuthClient
{
	use KnowsLanguageCodeTrait;

	private $httpClient;
	private $logger;

	/**
	 * OAuthClient constructor.
	 *
	 * @param HttpClient $httpClient
	 * @param CommunicationLoggerInterface $logger
	 */
	public function __construct(HttpClient $httpClient, CommunicationLoggerInterface $logger)
	{
		$this->httpClient = $httpClient;
		$this->logger = $logger;
	}

	/**
	 * @return string
	 */
	public function buildOAuthStartUrl() : string
	{
		$module = SwisscomModule::getInstance();
		$oAuthParams = $module->params['oauth'];
		$endpoint = $oAuthParams['endpoint'] . '/c/oauth2/auth';
		$getParams = [
			'response_type' => 'code',
			'redirect_uri' => self::getCallbackUrl(),
			'client_id' => $module->params['api']['clientId'],
			'lang' => $this->getLanguageCode(),
		];
		$this->logger->logRequest('redirect', $endpoint, $getParams);
		$url = $endpoint . '?' . http_build_query($getParams);

		return $url;
	}

	/**
	 * @param string $code Return code from given consent by user
	 *
	 * @see buildOAuthStartUrl()
	 * @return string
	 */
	public function requestToken(string $code) : string
	{
		$module = SwisscomModule::getInstance();
		$apiParams = $module->params['api'];
		$oAuthParams = $module->params['oauth'];
		$endpoint = $oAuthParams['endpoint'] . '/o/oauth2/token';
		$requestOptions = [
			'auth' => [
				$apiParams['clientId'],
				$apiParams['clientSecret'],
			],
			'headers' => [
				'Accept' => 'application/json; charset=utf-8',
			],
			'query' => [
				'grant_type' => 'authorization_code',
				'code' => $code,
				'redirect_uri' => self::getCallbackUrl(),
			],
		];
		$this->logger->logRequest(CommunicationLoggerInterface::METHOD_GET, $endpoint, $requestOptions);
		try {
			$response = $this->httpClient->get($endpoint, $requestOptions);
			$this->logger->logResponse($response);
		} catch (BadResponseException $e) {
			$msg = 'Unknown error when communicating to swisscom OAuth API: ' . $e->getMessage();
			Yii::error($msg, 'swisscom');
			throw new RuntimeException($msg, 0, $e);
		}

		$data = json_decode((string)$response->getBody(), true);

		if (empty($data['access_token'])) {
			$msg = 'Access Token could not be retrieved from swisscom OAuth API with code: ' . $code;
			Yii::error($msg, 'swisscom');
			throw new RuntimeException($msg);
		}

		return $data['access_token'];
	}

	/**
	 * @param string $token Access token of the identified user.
	 *
	 * @return string The URL to redirect the user to.
	 */
	public function buildSubscriptionSelectionStartUrl(string $token) : string
	{
		$module = SwisscomModule::getInstance();
		$oAuthParams = $module->params['oauth'];
		$endpoint = $oAuthParams['endpoint'] . '/c/support/subscriptions';
		$getParams = [
			'access_token' => $token,
			'redirect_uri' => self::getCallbackUrl(),
			'client_id' => $module->params['api']['clientId'],
			'payment_type' => 'postpaid',
			'subscription_type' => 'mobile',
			'lang' => $this->getLanguageCode(),
		];

		$url = $endpoint . '?' . http_build_query($getParams);

		return $url;
	}

	private static function getCallbackUrl(): string
	{
		$module = SwisscomModule::getInstance();

		$callbackAction = $module->params['oauth']['callback'];
		$callbackAction['lang'] = Yii::$app->language;

		return UrlHelper::getFrontendUrl(UrlHelper::DEFAULT_LANGUAGE) . Url::to($callbackAction);
	}
}
