<?php

namespace common\modules\swisscom\models;

use backend\models\LogBackend;
use common\modules\swisscom\models\logger\CommunicationLoggerInterface;
use common\modules\swisscom\SwisscomModule;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\BadResponseException;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Yii;

/**
 * Client to handle the Swisscom API.
 */
class ApiClient
{
	use KnowsLanguageCodeTrait;

	private $httpClient;
	private $accessToken;
	private $subscriptionKey;
	private $logger;

	/**
	 * ApiClient constructor.
	 *
	 * @param HttpClient $httpClient
	 * @param CommunicationLoggerInterface $logger
	 */
	public function __construct(HttpClient $httpClient, CommunicationLoggerInterface $logger)
	{
		$this->httpClient = $httpClient;
		$this->logger = $logger;
	}

	/**
	 * @param string $token
	 */
	public function setAccessToken(string $token)
	{
		$this->accessToken = $token;
	}

	/**
	 * @return string
	 */
	private function getAccessToken() : string
	{
		if (empty($this->accessToken)) {
			throw new RuntimeException('Access token not available when required in Swisscom API client.');
		}
		return $this->accessToken;
	}

	/**
	 * @param string $key
	 */
	public function setSubscriptionKey(string $key)
	{
		$this->subscriptionKey = $key;
	}

	/**
	 * @return string
	 */
	private function getSubscriptionKey() : string
	{
		if (empty($this->subscriptionKey)) {
			throw new RuntimeException('Subscription key not available when required in Swisscom API client.');
		}
		return $this->subscriptionKey;
	}

	/**
	 * @return array
	 */
	private function buildCallHeaders() : array
	{
		return [
			'Accept' => 'application/json; charset=utf-8',
			'Authorization' => 'Bearer ' . $this->getAccessToken(),
		];
	}

	/**
	 * @param string $resource
	 *
	 * @return string
	 */
	private function buildCallEndpoint(string $resource)
	{
		$module = SwisscomModule::getInstance();

		if($resource == 'profile') {
		    return $module->params['api']['endpointProfile'];
        }

		return $module->params['api']['endpoint'] . $resource;
	}

	/**
	 * @param BadResponseException $e
	 */
	private function handleApiException(BadResponseException $e)
	{
		$this->logger->logResponse($e->getResponse());
		$msg = 'Unknown error when communicating to swisscom API: ' . $e->getMessage();
		Yii::error($msg, 'swisscom');
		throw new RuntimeException($msg, 0, $e);
	}

	/**
	 * @param ResponseInterface $response
	 *
	 * @return array
	 */
	private function parseHttpResponse(ResponseInterface $response) : array
	{
		return json_decode((string)$response->getBody(), true);
	}

	/**
	 * Handles actual calling of the API (GET requests).
	 *
	 * @param string $resource Resource path name in the URL.
	 * @param array $queryParams Key => value mapping of all query parameters.
	 *
	 * @return array Response content
	 */
	private function callApiGet(string $resource, array $queryParams = []) : array
	{
		$endpoint = $this->buildCallEndpoint($resource);
		$options = [
			'headers' => $this->buildCallHeaders(),
			'query' => $queryParams,
		];
		$this->logger->logRequest(CommunicationLoggerInterface::METHOD_GET, $endpoint, $options);
		try {
			$response = $this->httpClient->get($endpoint, $options);
			$this->logger->logResponse($response);
			return $this->parseHttpResponse($response);
		} catch (BadResponseException $e) {
			$this->handleApiException($e);

			return [];
		}
	}

	/**
	 * Handles actual calling of the API (POST requests).
	 *
	 * @param string $resource Resource path name in the URL.
	 * @param array $bodyParams Key => value mapping of all post parameters.
	 * @param array $queryParams
	 * @param null $orderId
	 *
	 * @return array Response content
	 */
	private function callApiPost(string $resource, array $bodyParams = [], array $queryParams = [], $orderId = null) : array
	{
		$endpoint = $this->buildCallEndpoint($resource);
		$requestOptions = [
			'headers' => $this->buildCallHeaders(),
			'json' => $bodyParams,
			'query' => $queryParams,
		];
		$this->logger->logRequest(CommunicationLoggerInterface::METHOD_POST, $endpoint, $requestOptions, $orderId);
		try {
			$response = $this->httpClient->post($endpoint, $requestOptions);
			$this->logger->logResponse($response);
			return $this->parseHttpResponse($response);
		} catch (BadResponseException $e) {
			$this->handleApiException($e);

			return [];
		}
	}

	/**
	 * @return array
	 */
	public function getRetentionData() : array
	{

        $log = new LogBackend();
        $log->level = 4;
        $log->category = "swisscom";
        $log->message = "/subscriptions/~/retention";
        $log->save();

		return $this->callApiGet(
			'/subscriptions/~/retention', [
				'subscriptionKey' => $this->getSubscriptionKey(),
				// for some unknown reason swisscom needs the language code lowercased ONLY HERE
				'language' => strtolower($this->getLanguageCode()),
				'channel' => 'RESELLER_ONLINE',
			]
		);
	}

	public function getProfile() : array
    {
        return $this->callApiGet('profile');
    }

	/**
	 * @return array
	 */
	public function getSimcards() : array
	{
        $log = new LogBackend();
        $log->level = 4;
        $log->category = "swisscom";
        $log->message = "/subscriptions/~/simcards";
        $log->save();

		return $this->callApiGet(
			'/subscriptions/~/simcards', [
				'subscriptionKey' => $this->getSubscriptionKey(),
			]
		);
	}

	/**
	 * @return array
	 */
	private function buildDefaultRetentionEligibilityParams() : array
	{
		return [
			'subscriptionKey' => $this->getSubscriptionKey(),
			'channel' => 'RESELLER_ONLINE',
			'useCase' => 'RETENTION',
			'segment' => 'RES',
			'isDeviceWithoutVZ' => 'false',
		];
	}

	/**
	 * @param array $params
	 *
	 * @return array
	 */
	private function callRetentionEligibility(array $params) : array
	{
        $log = new LogBackend();
        $log->level = 4;
        $log->category = "swisscom";
        $log->message = "/subscriptions/~/eligibility";
        $log->save();

		return $this->callApiGet('/subscriptions/~/eligibility', $params);
	}

	/**
	 * @return array
	 */
	public function getRetentionEligibility() : array
	{
		return $this->callRetentionEligibility($this->buildDefaultRetentionEligibilityParams());
	}

	/**
	 * @param int $deviceId swisscom ID
	 *
	 * @return array
	 */
	public function getRetentionEligibilityForDevice($deviceId = null) : array
	{
		$params = $this->buildDefaultRetentionEligibilityParams() + ['deviceId' => $deviceId];
		return $this->callRetentionEligibility($params);
	}

	/**
	 * @param array $params
	 */
	private function guardAgainstEmptyRetentionOrderParams(array $params)
	{
		foreach ([
			         'productName',
			         'commitmentPeriod',
			         'newContractStartDate',
			         'originIpAddress',
			         'deviceId',
		         ] as $index
		) {
			if (empty($params[$index])) {
				throw new InvalidArgumentException('Parameter "' . $index . '" is mandatory.');
			}
		}

		foreach ([
			         'title',
			         'emailAddress', // eMail address has to be specified even though documentation states otherwise
			         'firstName',
			         'lastName',
		         ] as $index
		) {
			if (empty($params['oneTimeContact'][$index])) {
				throw new InvalidArgumentException('Parameter "oneTimeContact->' . $index . '" is mandatory.');
			}
		}
	}

	/**
	 * @param array $params Required parameters defined by the following keys in this array:
	 * <pre>
	 * [
	 *      productName -> tariff->swisscom_id
	 *      commitmentPeriod -> tariff->contract_duration -> RententionController::COMMITMENT_PERIOD_12M
	 *      newContractStartDate -> proposed_subscription_start_date aus request
	 *      originIpAddress -> IP of the customer
	 *      deviceId -> article->swisscom_id
	 *      oneTimeContact => [ -> order->BillingAddress
	 *          title
	 *          emailAddress
	 *          firstName
	 *          lastName
	 *      ]
	 * ]
	 * </pre>
	 * @param integer|null $orderId
	 *
	 * @return array
	 */
	public function makeRetentionOrder($params, $orderId = null) : array
	{
		$this->guardAgainstEmptyRetentionOrderParams($params);

		$apiParams = SwisscomModule::getInstance()->params['api'];

        $log = new LogBackend();
        $log->level = 4;
        $log->category = "swisscom";
        $log->message = "/subscriptions/~/orders";
        $log->save();

		return $this->callApiPost(
			'/subscriptions/~/orders',
			array_merge_recursive(
				// This array only contains the "static" parameters, as they are either defined to be hard coded
				// or read from configuration, but not dynamic on a per-request basis, this is then extended with
				// dynamic parameters from the callee of this method
				[
					'language' => $this->getLanguageCode(),
					'AppleCare' => false,
					'incentiveUserId' => 'OADMIN',
					'dealerId' => $apiParams['dealerId'],
					'context' => 'ONLINE',
					'brokerID' => $apiParams['brokerId'],
					'useCase' => 'RETENTION',
					'externalUserLoginId' => $apiParams['loginId'],
					'channel' => 'RESELLER_ONLINE',
					'oneTimeContact' => [
						'contactChannel' => 'Letter',
					],
				],
				$params
			),
			[
				'subscriptionKey' => $this->getSubscriptionKey(),
			],
			$orderId
		);
	}
}
