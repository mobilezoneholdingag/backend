<?php

namespace common\modules\swisscom;

use deinhandy\yii2container\Module;
use Yii;

class SwisscomModule extends Module
{
	/**
	 * Gets alpha-2 code for current user language as required by swisscom API.
	 *
	 * @return string
	 */
	public function getLanguageAlpha2Code()
	{
		$parts = explode('-', Yii::$app->language); // language be like 'de-CH'
		return strtoupper($parts[0]);
	}
}
