<?php

namespace common\modules\swisscom\controllers;

use common\modules\swisscom\models\OAuthClient;
use frontend\controllers\rest\Controller;

class AuthController extends Controller
{

	/** @var OAuthClient */
	private $client;

	public function __construct($id, $module, OAuthClient $client, $config = [])
	{
		$this->client = $client;

		parent::__construct($id, $module, $config);
	}

	public function actionStart(): array
	{
		return [
			'url' => $this->client->buildOAuthStartUrl(),
		];
	}

	public function actionRequestToken($code): array
	{
		return [
			'token' => $this->client->requestToken($code),
		];
	}

	public function actionStartSelectSubscription($token): array
	{
		return [
			'url' => $this->client->buildSubscriptionSelectionStartUrl($token),
		];
	}
}
