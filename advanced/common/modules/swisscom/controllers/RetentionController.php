<?php

namespace common\modules\swisscom\controllers;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\OfferType;
use common\models\Provider;
use common\models\Tariffs;
use common\modules\swisscom\factories\RenewalRequestFactory;
use common\modules\swisscom\models\ApiClient;
use frontend\controllers\rest\Controller;
use LogicException;
use Yii;
use yii\db\ActiveRecord;

/**
 * Retention controller for the `swisscom` module
 */
class RetentionController extends Controller
{
	const COMMITMENT_PERIOD_12M = 'ONE_YEAR';
	const COMMITMENT_PERIOD_24M = 'TWO_YEARS';
	const COMMITMENT_PERIOD_NONE = 'NONE';
	const HASH_KEY = 'swisscom_api';

	const MULTI_SIM_CARD_ID = 999999;
	/**
	 * @var ApiClient
	 */
	private $client;
	/**
	 * @var array
	 */
	private $simCardData;

	/**
	 * RetentionController constructor.
	 *
	 * @param string $id
	 * @param \yii\base\Module $module
	 * @param ApiClient $client
	 * @param array $config
	 */
	public function __construct($id, $module, ApiClient $client, $config = [])
	{
		$this->client = $client;

		parent::__construct($id, $module, $config);
	}

	/**
	 * Retrieves sim card data with local caching.
	 */
	private function retrieveSimCardData() : array
	{
		if ($this->simCardData === null) {
			$this->simCardData = $this->client->getSimcards();
		}
		return $this->simCardData;
	}

	private function isSimCardIndeterminable() : bool
	{
		return $this->hasCustomerMultipleSimCards() || $this->canRecognizeSimCard() === false;
	}

	private function hasCustomerMultipleSimCards() : bool
	{
		return count($this->retrieveSimCardData()) > 1;
	}

	/**
	 * Recognizable sim card types from the API are only 'mini', 'micro' and 'nano'.
	 */
	private function canRecognizeSimCard() : bool
	{
		if ($this->hasCustomerMultipleSimCards()) {
			return false;
		}

		$simCardData = $this->retrieveSimCardData();

		return in_array(
			$simCardData[0]['type'] ?? null,
			[
				'mini',
				'micro',
				'nano',
			]
		);
	}

	private function customerNeedsNewMultiSimCardForArticle(int $articleId) : bool
	{
		if ($this->canRecognizeSimCard() === false) {
			return false;
		}

		$simCardData = $this->retrieveSimCardData();
		$simCard = $this->getSimCardForArticle($articleId);

		// In case it is not configured, we do not want to fail here
		if (empty($simCard)) {
			return false;
		}

		return $this->translateSimCardTypeToInternalType($simCardData[0]['type']) !== $simCard->simcard_type;
	}

	private function translateSimCardTypeToInternalType(string $type)
	{
		// See application params 'simCardTypes' index for reference
		switch ($type) {
			case 'mini': return 0;
			case 'micro': return 1;
			case 'nano': return 2;
			default: throw new LogicException('Sim card type can not be determined for passed type "' . $type . '".');
		}
	}

	public function afterAction($action, $result)
	{
		$result = parent::afterAction($action, $result);

		if ($action->id === "check-retention") {
			$renewalRequest = RenewalRequestFactory::createFromRetention($result);
			$result['renewalRequestId'] = $renewalRequest->id;
		}

		return $result;
	}

	/**
	 * @param $accessToken
	 * @param $subscriptionKey
	 * @param $articleId
	 * @param null $customerId
	 *
	 * @return array
	 */
	public function actionCheckRetention($accessToken, $subscriptionKey, $articleId = null, $customerId = null)
	{
		$result = ['eligible' => false];

		$client = $this->client;
		$client->setAccessToken($accessToken);
		$client->setSubscriptionKey($subscriptionKey);

		$retentionData = $client->getRetentionData();
        $result['profile'] = $client->getProfile();

		if ($retentionData['trafficLight'] == 'red') {
			if (isset($retentionData['subscriptionEndDate'])) {
				$result['subsciption_end_date'] = $retentionData['subscriptionEndDate'];
			}
		}

		if ($retentionData['trafficLight'] != 'green') {
			return $result;
		}

		$result['eligible'] = true;

		if ($this->isSimCardIndeterminable()) {
			$result['simCardIndeterminable'] = true;
		}

		$result['proposedSubscriptionStartDate'] = $retentionData['proposedSubscriptionStartDate'];

		if ($articleId && $this->customerNeedsNewMultiSimCardForArticle($articleId)) {
			$result['simCard'] = self::MULTI_SIM_CARD_ID;
		}

		$article = $this->getArticle($articleId);
		$eligibleTariffs = [];
		if (!$article || empty($article->swisscom_id) === false) {
			$eligibleTariffs = $client->getRetentionEligibilityForDevice($article->swisscom_id ?? null);
		}

		$this->prepareResult($result, $eligibleTariffs, $articleId, $customerId);

		return $result;
	}

	private function prepareResult(array &$result, $eligibleTariffs, $articleId = null, $customerId = null)
	{
		$articleTariffs = [];
		$tariffs = [];

		$request = Yii::$app->request;
		$oldParams = $request->get();

		foreach ($this->getSubscriptions($eligibleTariffs) as $subscriptionData) {
			if (isset($subscriptionData['commitmentPeriods']) && !is_array($subscriptionData['commitmentPeriods'])) {
				$subscriptionData['commitmentPeriods'] = [$subscriptionData['commitmentPeriods']];
			}

			foreach ($subscriptionData['commitmentPeriods'] as $term) {
				$this->addTariffAndArticleTariff($articleId, $subscriptionData, $term, $tariffs, $articleTariffs);
			}
		}

		$request->setQueryParams($oldParams);
		$result['articleTariffs'] = $articleTariffs;
		$result['tariffs'] = $tariffs;
		$result['articleId'] = $articleId;
		$result['customerId'] = $customerId;
	}

	private function getSubscriptions($eligibleTariffs): array
	{
		$subscriptions = $eligibleTariffs['devices'][0]['subscriptions'] ?? [];

		if (isset($subscriptions['productName'])) { // when there is only one tariff, $subscriptions won't be an array
			$subscriptions = [$subscriptions];
		}

		return $subscriptions;
	}

	private function addTariffAndArticleTariff($articleId, $subscriptionData, $term, &$tariffs, &$articleTariffs)
	{
		$tariff = $this->findTariffBySwisscomData(
			$subscriptionData['productName'],
			$this->translateCommitmentPeriod($term)
		);
		if (empty($tariff)) {
			return;
		}
		$tariffs[] = $tariff->id;
		$articleTariff = $this->findArticleTariff($articleId, $tariff->id);
		if (empty($articleTariff)) {
			return;
		}
		$articleTariffs[] = $articleTariff->id;
	}

	/**
	 * @param $articleId
	 * @param $tariffId
	 *
	 * @return null|ArticleTariffs|ActiveRecord
	 */
	private function findArticleTariff($articleId, $tariffId)
	{
		Yii::$app->request->setQueryParams(['article' => $articleId, 'tariff' => $tariffId, 'swisscom_id' => true]);
		$response = Yii::$app->runAction('article-tariff/search');

		return $response[0]['items'][0] ?? null;
	}

	/**
	 * @param string $commitmentPeriod
	 *
	 * @return int In months
	 */
	private function translateCommitmentPeriod($commitmentPeriod)
	{
		switch ($commitmentPeriod) {
			case self::COMMITMENT_PERIOD_12M:
				return 12;
			case self::COMMITMENT_PERIOD_24M:
				return 24;
			case self::COMMITMENT_PERIOD_NONE:
				return 0;
			default:
				throw new LogicException('Unknown commitment period for swisscom article: ' . $commitmentPeriod . '.');
		}
	}

	/**
	 * @param string $swisscomTariffId
	 * @param int $contractDuration In months
	 *
	 * @return Tariffs|ActiveRecord|null
	 */
	private function findTariffBySwisscomData($swisscomTariffId, $contractDuration)
	{
		return Tariffs::find()
			->where(['swisscom_id' => $swisscomTariffId])
			->orWhere(['swisscom_id_2' => $swisscomTariffId])
			->andWhere([
				'contract_duration' => $contractDuration,
				'offer_type_id' => OfferType::RENEWAL_TYPE,
				'is_b2b_allowed' => 0,
				'is_active' => 1,
				'is_active_erp' => 1,
			])
			->one();
	}

	/**
	 * @param int $articleId
	 *
	 * @return Article|null
	 */
	private function getArticle($articleId)
	{
		return Article::findOne($articleId);
	}

	/**
	 * @param int $articleId
	 *
	 * @return Article|null
	 */
	private function getSimCardForArticle($articleId)
	{
		$article = $this->getArticle($articleId);
		$providerId = Provider::findOne(['system_title' => Provider::TITLE_SWISSCOM])->id ?? null;
		return $article->articleGroup->getSimCard($providerId);
	}
}
