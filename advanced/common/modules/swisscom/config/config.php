<?php

return [
	'defaultRoute' => 'auth/start',
	'params' => [
		'oauth' => [
			'endpoint' => 'https://consent.swisscom.com',
			'callback' => ['swisscom/index'],
		],
		'api' => [
			'endpoint' => 'https://api.swisscom.com/v1',
            'endpointProfile' => 'https://api.swisscom.com/parties/v1/customers/me',
			'clientId' => 'IvVcRm9phfXmTz0U0UGWNCQcpIl3qIyh',
			'clientSecret' => 'FhUDyYw1ZZofBwbd',
			'dealerId' => 139752,
			'brokerId' => 139752,
			'loginId' => 'OD139752',
		]
	],
];
