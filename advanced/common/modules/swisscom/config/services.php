<?php

use deinhandy\yii2container\Container;

/** @var Container $container */
$container = Yii::$container;
// HTTP Client
$container->setSingleton(\GuzzleHttp\Client::class, function(Container $container) {
	return new GuzzleHttp\Client();
});

$container->setSingleton(\common\modules\swisscom\models\logger\CommunicationLoggerInterface::class, function(Container $container) {
	return new \common\modules\swisscom\models\logger\SwisscomCommunicationLogger();
});

// OAuth Client
$container->setSingleton(\common\modules\swisscom\models\OAuthClient::class, function(Container $container) {
	return new \common\modules\swisscom\models\OAuthClient(
		$container->get(\GuzzleHttp\Client::class),
		$container->get(\common\modules\swisscom\models\logger\CommunicationLoggerInterface::class)
	);
});

// API Client
$container->setSingleton(\common\modules\swisscom\models\ApiClient::class, function(Container $container) {
	return new \common\modules\swisscom\models\ApiClient(
		$container->get(\GuzzleHttp\Client::class),
		$container->get(\common\modules\swisscom\models\logger\CommunicationLoggerInterface::class)
	);
});

$container->addTags(\common\modules\swisscom\events\OrderEventListener::class, ['events.listener']);