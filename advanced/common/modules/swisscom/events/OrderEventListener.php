<?php

namespace common\modules\swisscom\events;

use backend\models\LogBackend;
use common\models\Provider;
use common\modules\order\events\OrderPlacedWithPayloadEvent;
use common\modules\order\models\Address;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use common\modules\swisscom\controllers\RetentionController;
use common\modules\swisscom\models\ApiClient;
use deinhandy\yii2events\listener\AbstractEventListener;

class OrderEventListener extends AbstractEventListener
{
	private $apiClient;

	/**
	 * OrderEventListener constructor.
	 *
	 * @param ApiClient $apiClient
	 */
	public function __construct(ApiClient $apiClient)
	{
		$this->apiClient = $apiClient;
	}

	/**
	 * @return array
	 */
	public function getClassEvents() : array
	{
		return [
			Order::class => [
				Order::EVENT_AFTER_ORDER_PLACED_WITH_PAYLOAD => [$this, 'handleOrderPlacedWithPayload'],
			],
		];
	}

	/**
	 * @param OrderPlacedWithPayloadEvent $event
	 */
	public function handleOrderPlacedWithPayload(OrderPlacedWithPayloadEvent $event)
	{
        /*$log = new LogBackend();
        $log->level = 4;
        $log->category = "swisscom";
        $log->message = "handleOrderPlacedWithPayload getpayload(): "+serialize($event->getPayload());
        $log->save();*/

        /*$log = new LogBackend();
        $log->level = 4;
        $log->category = "swisscom";
        $log->message = "handleOrderPlacedWithPayload EVENT: "+serialize($event);
        $log->save();*/

		if ($this->hasValidSwisscomInformation($event)) {
			$order = $event->getOrder();
			$payload = $event->getPayload();
			$tariff = $this->getTariffItemFromOrder($order)->getTariff();
			$params = [
				'productName' => $tariff->swisscom_id,
				'commitmentPeriod' => $this->getCommitmentPeriodString($tariff->contract_duration),
				'newContractStartDate' => $payload['swisscom']['proposed_subscription_start_date'],
				'originIpAddress' => '37.120.50.158',
				'deviceId' => $this->getSwisscomArticleFromOrder($order)->swisscom_id,
				'oneTimeContact' => [
					'title' => $order->billingAddress->sex == Address::SEX_MALE ? 'Mr.' : 'Ms.',
					'emailAddress' => $order->billingAddress->customer->email,
					'firstName' => $order->billingAddress->firstname,
					'lastName' => $order->billingAddress->lastname,
				],
			];
			$this->apiClient->setSubscriptionKey($payload['swisscom']['subscription_key']);
			$this->apiClient->setAccessToken($payload['swisscom']['token']);
			$response = $this->apiClient->makeRetentionOrder($params, $order->id);
			$order->sr_integration_id = $response['srIntegrationId'];
			$order->save();

            $log = new LogBackend();
            $log->level = 4;
            $log->category = "swisscom";
            $log->message = "hasValidSwisscomInformation";
            $log->save();
		}
	}

	/**
	 * @param OrderPlacedWithPayloadEvent $event
	 *
	 * @return bool
	 */
	private function hasValidSwisscomInformation(OrderPlacedWithPayloadEvent $event)
	{
		$payload = $event->getPayload();
        $log = new LogBackend();
        $log->level = 4;
        $log->category = "swisscom";
        $log->message = "hasValidSwisscomInformation Payload:"+serialize($payload);
        $log->save();
		if (
			empty($payload['swisscom']['proposed_subscription_start_date'])
			|| empty($payload['swisscom']['subscription_key'])
			|| empty($payload['swisscom']['token'])
		) {
			return false;
		}
		$order = $event->getOrder();
		if ($this->hasSwisscomTariff($order) === false) {
			return false;
		}

		if ($this->getSwisscomArticleFromOrder($order) === null) {
			return false;
		}

		return true;
	}

	/**
	 * @param Order $order
	 *
	 * @return bool
	 */
	private function hasSwisscomTariff(Order $order)
	{
		$tariff = $this->getTariffItemFromOrder($order);
		return ($tariff->getTariff()->provider->system_title ?? null) == Provider::TITLE_SWISSCOM;
	}

	/**
	 * @param Order $order
	 *
	 * @return Item
	 */
	private function getTariffItemFromOrder(Order $order)
	{
		/** @var Item[] $items */
		$items = $order->getItems()->all();
		foreach ($items as $item) {
			if ($item->isTariff()) {
				return $item;
			}
		}
		return null;
	}

	/**
	 * @param $contractDuration
	 *
	 * @return null|string
	 */
	private function getCommitmentPeriodString($contractDuration)
	{
		if ($contractDuration == 12) {
			return RetentionController::COMMITMENT_PERIOD_12M;
		} elseif ($contractDuration == 24) {
			return RetentionController::COMMITMENT_PERIOD_24M;
		}
		return null;
	}

	/**
	 * @param Order $order
	 *
	 * @return \common\models\Article|null
	 */
	private function getSwisscomArticleFromOrder(Order $order)
	{
		$items = $order->allItems;
		foreach ($items as $item) {
			if ($item->isArticleTariff()) {
				$articleTariffChildItems = $item->childItems;
				foreach ($articleTariffChildItems as $childItem) {
					if ($childItem->isArticle()) {
						$article = $childItem->getArticle();
						if ($article->hasSwisscomId()) {
							return $article;
						}
					}
				}
			}
		}
		return null;
	}
}
