<?php

namespace common\modules\swisscom\factories;

use common\models\ArticleTariffs;
use common\models\Customer;
use common\models\CustomerAddress;
use common\models\CustomerPhoneNumber;
use common\models\Provider;
use common\models\Tariffs;
use common\modules\apilogger\services\Logger;
use common\modules\renewalRequest\models\RenewalRequest;
use common\modules\renewalRequest\models\RenewalRequestOffers;
use common\modules\renewalRequest\mappers\CustomerMapper;
use common\modules\swisscom\controllers\RetentionController;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

class RenewalRequestFactory
{
	const TYPE_ARTICLE_TARIFFS = 'articleTariffs';
	const TYPE_TARIFFS = 'tariffs';

	public static function createFromRetention(array $data): RenewalRequest
	{
		$renewalRequest = new RenewalRequest();

		$renewalRequest->provider_id = Provider::SWISSCOM_ID;

		if(!empty($data['subsciption_end_date'])) {
            $renewalRequest->comment = $data['subsciption_end_date'];
        }

		$renewalRequest->status = $data['eligible'] ?? false
				? RenewalRequest::STATUS_OFFERED
				: RenewalRequest::STATUS_DENIED;

		self::saveCustomer($renewalRequest, $data);

		$renewalRequest->save(false);

		if ($renewalRequest->status === RenewalRequest::STATUS_DENIED) {
			return $renewalRequest;
		}

		$validOfferAvailable = self::saveRenewalRequestOffers($data, $renewalRequest->id);

		$renewalRequest->status = $validOfferAvailable
			? RenewalRequest::STATUS_VIEWED
			: RenewalRequest::STATUS_UNKNOWN_OFFERS;
		$renewalRequest->save(false);

		return $renewalRequest;
	}

	private static function saveCustomer(RenewalRequest &$renewalRequest, array $data)
	{


	    \Yii::error(VarDumper::export($data), 'swisscom-profile');

		if (!isset($data['customerId'])) {

		    if(!empty($data['profile'])) {

		        if(empty($data['profile']['email'])) {
                    $data['profile']['email'] = md5($data['profile']['firstName'] . $data['profile']['lastName'] . $data['profile']['dateOfBirth']) . '@do-not-import.please';
                }

		        $customer = Customer::findOne(['email' => $data['profile']['email']]);

                \Yii::error("Duplicate Check", 'swisscom-profile');
		        \Yii::error(VarDumper::export($customer), 'swisscom-profile');


		        if(empty($customer)) {

		            $lang = 'de-CH';

		            if($lang == 'fr') {
		                $lang = 'fr-FR';
                    } elseif($lang == 'it') {
		                $lang = 'it-IT';
                    }

                    $customer = new Customer(
                        [
                            'email' => $data['profile']['email'],
                            'first_name' => $data['profile']['firstName'],
                            'last_name' => $data['profile']['lastName'],
                            'gender' => ($data['profile']['gender'] == 'male') ? 1 : 2,
                            'language' => $lang,
                            'birthday' => $data['profile']['dateOfBirth'],
                            'via_newsletter' => 1
                        ]
                    );
                    $customer->save();

                    \Yii::error("New Customer", 'swisscom-profile');
                    \Yii::error(VarDumper::export($customer), 'swisscom-profile');

                    foreach($data['profile']['phoneNumbers'] as $phone) {

                        (new CustomerPhoneNumber(
                            [
                                'number' => preg_replace('/[^0-9]/', '', $phone['value']),
                                'customer_id' => $customer->id,
                            ]
                        ))->save();

                    }

                    foreach($data['profile']['addresses'] as $address) {
                        (new CustomerAddress(
                            [
                                'sex' => ($data['profile']['gender'] == 'male') ? 1 : 2,
                                'customer_id' => $customer->id,
                                'firstname' => $data['profile']['firstName'],
                                'lastname' => $data['profile']['lastName'],
                                'street' => $address['street'],
                                'house_number' => $address['houseNumber'],
                                'zip' => $address['zip'],
                                'city' => $address['city'],
                            ]
                        ))->save();
                    }

                }

            } else {
		        return;
            }

		}

		if(!empty($customer)) {
		    $customer = Customer::findOne($customer->id);
        } else {
            $customer = Customer::findOne($data['customerId']);
        }

		if (!$customer) {
            \Yii::error("Customer empty?", 'swisscom-profile');
            \Yii::error(VarDumper::export($customer), 'swisscom-profile');
			return;
		}

        \Yii::error("Return Customer", 'swisscom-profile');
        \Yii::error(VarDumper::export($customer), 'swisscom-profile');

		CustomerMapper::map($customer, $renewalRequest);
	}

	private static function saveRenewalRequestOffers(array $data, $renewalRequestId): bool
	{
		$isTariffType = !isset($data['articleId']) || is_null($data['articleId']);
		$type = $isTariffType ? self::TYPE_TARIFFS : self::TYPE_ARTICLE_TARIFFS;
		$entities = self::getEntities($data, $type);
		$validOfferAvailable = false;

		foreach ($entities as $entity) {
			$articleId = $isTariffType ? null : ($data['articleId'] ?? $entity->article_id);
			$tariffId = $isTariffType ? $entity->id : $entity->tariff_id;
			$renewalRequestOffer = new RenewalRequestOffers(
				[
					'article_id' => $articleId,
					'renewal_request_id' => $renewalRequestId,
					'tariff_id' => $tariffId,
					'hash' => RetentionController::HASH_KEY,
				]
			);
			$renewalRequestOffer->save();
			$validOfferAvailable = true;
		}

		return $validOfferAvailable;
	}

	private static function getEntities(array $data, string $type)
	{
		switch ($type) {
			case self::TYPE_ARTICLE_TARIFFS:
				return ArticleTariffs::find()->where(['IN', 'id', $data[$type] ?? []])->all();
			case self::TYPE_TARIFFS:
				return Tariffs::find()->where(['IN', 'id', $data[$type] ?? []])->all();
		}

		return new ActiveRecord();
	}
}
