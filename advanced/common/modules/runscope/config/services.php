<?php

$container = Yii::$container;

$container->set(\common\modules\runscope\services\ApiClient::class, function() {
	$params = \common\modules\runscope\RunscopeModule::getInstance()->params;
	$apiUrl = $params['apiUrl'];
	$client = new \Guzzle\Http\Client($apiUrl);
	$client->setDefaultOption('headers/Authorization', 'Bearer ' . $params['token']);

	return new \common\modules\runscope\services\ApiClient($client);
});
