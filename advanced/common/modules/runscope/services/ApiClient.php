<?php
namespace common\modules\runscope\services;

use common\modules\runscope\RunscopeModule;
use Guzzle\Http\Client;
use Yii;

class ApiClient
{
	const CACHE_KEY_SCRIPT_VARIABLES = 'runscope-script-variables';

	/** @var Client */
	private $client;
	/** @var array */
	private $params;

	public function __construct(Client $client)
	{
		$this->client = $client;
		$this->params = RunscopeModule::getInstance()->params;
		$this->cache = Yii::$app->cache;
	}

	public function getScriptVariables()
	{
		if ($this->cache->exists(self::CACHE_KEY_SCRIPT_VARIABLES)) {
			return $this->cache->get(self::CACHE_KEY_SCRIPT_VARIABLES);
		}

		$responseData = $this->getResponseData();

		if (!isset($responseData->data->script)) {
			throw new \Exception('API response does not contain the expected data!');
		}

		$script = $responseData->data->script;
		$parsedVariables = $this->parseVariables($script);
		$this->cache->set(self::CACHE_KEY_SCRIPT_VARIABLES, $parsedVariables);

		return $parsedVariables;
	}

	private function getResponseData()
	{
		$resource = "buckets/{$this->params['bucketId']}/environments/{$this->params['environmentId']}";
		$request = $this->client->get($resource);
		$response = $request->send();

		if ($response->getStatusCode() !== 200) {
			throw new \Exception('Runscope API returned an error!');
		}

		return json_decode($response->getBody(true));
	}

	private function parseVariables(string $script)
	{
		preg_match_all("|variables\.set\(\'(.*?)\'\,\s'?(.*?)'?\)\;|", $script, $matches);
		$variables = [];
		foreach ($matches[1] as $index => $key) {
			if (!isset($matches[2][$index])) {
				continue;
			}
			$currentValue = $matches[2][$index];
			$variables[$key] = is_numeric($currentValue) ? intval($currentValue) : $currentValue;
		}
		return $variables;
	}
}
