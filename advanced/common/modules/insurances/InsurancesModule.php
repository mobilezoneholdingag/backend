<?php

namespace common\modules\insurances;

use common\modules\insurances\events\OrderEventListener;
use Yii;
use yii\web\Application;

/**
 * insurance-order module definition class
 */
class InsurancesModule extends \yii\base\Module
{
	public function init()
	{
		parent::init();
		new OrderEventListener();
		if (Yii::$app instanceof Application) {
			$this->controllerMap = [
				'article-insurance' => 'common\modules\insurances\controllers\backend\ArticleInsuranceController',
				'insurance-order' => 'common\modules\insurances\controllers\backend\InsuranceOrderController'
			];
		}
	}
}
