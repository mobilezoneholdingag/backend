<?php

namespace common\modules\insurances\controllers\backend;

use common\modules\insurances\models\ArticleGroupsInsurance;
use common\modules\insurances\models\ArticleInsurance;
use common\modules\insurances\models\ArticleInsuranceSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ArticleGroupsInsuranceController implements the CRUD actions for ArticleGroupsInsurance model.
 */
class ArticleInsuranceController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['admin', 'editor', 'user'],
					],
					[
						'actions' => ['create', 'delete'],
						'allow' => true,
						'roles' => ['starlord'],
					],
					[
						'actions' => ['error'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * Lists all ArticleGroupsInsurance models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ArticleInsuranceSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render(
			'index',
			[
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]
		);
	}

	/**
	 * Parse POST request data and handle safe/delete of model(s).
	 *
	 * @return bool
	 * @throws NotFoundHttpException
	 */
	protected function handlePostRequest()
	{
		$request = Yii::$app->request->post();
		$articleId = $request['ArticleInsurance']['id'] ?? null;

		if (!$articleId) {
			Yii::$app->session->addFlash('danger', 'No ID found for update.');

			return false;
		}

		$article = ArticleInsurance::findOne($articleId);
		if (isset($article) === false) {
			throw new NotFoundHttpException("Insurance not found: $articleId");
		}
		$article->load($request);
		$article->group_id = 0;
		$article->save(false);

		$groupIds = [];
		if (empty($request['ArticleInsurance']['group_id']) === false) {
			$groupIds = $request['ArticleInsurance']['group_id'];
		}

		$insurances = ArticleGroupsInsurance::find()->where(['insurance_id' => $articleId])->all();
		list($deleteGroups, $addGroups) = $this->clusterGroupIds($insurances, $groupIds);

		if (!empty($deleteGroups)) {
			$this->handleDelete($articleId, $deleteGroups);
		}

		if (!empty($addGroups)) {
			$this->handleAdd($articleId, $addGroups);
		}

		return true;
	}

	/**
	 * Cluster group_ids from post into "to be added and "tb removed".
	 *
	 * @param $insurances
	 * @param $groupIds
	 *
	 * @return array containing info about add/remove group_ids.
	 */
	protected function clusterGroupIds($insurances, $groupIds)
	{
		$insuranceGroupIds = [];
		foreach ($insurances as $insuranceObj) {
			/* @var $insuranceObj ArticleGroupsInsurance */
			$insuranceGroupIds[] = $insuranceObj->group_id;
		}

		return [
			array_diff($insuranceGroupIds, $groupIds),
			array_diff($groupIds, $insuranceGroupIds),
		];
	}

	/**
	 * Delete insurance-group entries.
	 *
	 * @param $insuranceId
	 * @param $deleteGroups
	 *
	 * @throws \Exception
	 */
	private function handleDelete($insuranceId, $deleteGroups)
	{
		$insurances = ArticleGroupsInsurance::find()->where(
			['insurance_id' => $insuranceId, 'group_id' => $deleteGroups]
		)->all();
		foreach ($insurances as $insurance) {
			/* @var $insurance ArticleGroupsInsurance */
			$insurance->delete();
		}
	}

	/**
	 * Add new insurance entries.
	 *
	 * @param $insuranceId
	 * @param $addGroups
	 */
	private function handleAdd($insuranceId, $addGroups)
	{
		foreach ($addGroups as $groupId) {
			$insurance = new ArticleGroupsInsurance();
			$insurance->insurance_id = (int)$insuranceId;
			$insurance->group_id = (int)$groupId;
			$insurance->save();
		}
	}

	/**
	 * Updates an existing ArticleGroupsInsurance model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException
	 */
	public function actionUpdate($id)
	{
		if (Yii::$app->request->isPost) {
			if ($this->handlePostRequest()) {
				return $this->redirect(['view', 'id' => $id]);
			} else {
				return $this->redirect(['update', 'id' => $id]);
			}
		} else {
			$article = ArticleInsurance::findOne($id);
			if (isset($article) === false) {
				throw new NotFoundHttpException("Insurance not found: $id");
			}
			$articleGroupInsurances = ArticleGroupsInsurance::find()
				->select(['group_id'])
				->where(['insurance_id' => $id])
				->all();

			$groups = [];

			foreach ($articleGroupInsurances as $articleGroupInsurance) {
				$groups[] = $articleGroupInsurance->group_id;
			}

			$article->group_id = $groups;

			return $this->render('update', ['article' => $article]);
		}
	}

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionView($id)
	{
		$article = ArticleInsurance::findOne($id);
		if (isset($article) === false) {
			throw new NotFoundHttpException("Insurance not found: $id");
		}

		return $this->render('view', ['article' => $article]);
	}

	/**
	 * Finds the ArticleGroupsInsurance model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return ArticleGroupsInsurance the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = ArticleGroupsInsurance::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
