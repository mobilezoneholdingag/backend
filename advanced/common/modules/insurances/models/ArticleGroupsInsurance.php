<?php

namespace common\modules\insurances\models;

use common\models\Article;
use common\models\ArticleGroups;
use components\i18n\TranslatableActiveRecord;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "article_groups_insurance".
 *
 * @property integer $id
 * @property integer $insurance_id
 * @property integer $group_id
 * @property Article $insurance
 * @property ArticleGroups $group
 */
class ArticleGroupsInsurance extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'article_groups_insurance';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'insurance_id', 'group_id'], 'integer'],
			[
				['insurance_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Article::className(),
				'targetAttribute' => ['insurance_id' => 'id'],
			],
			[
				['group_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => ArticleGroups::className(),
				'targetAttribute' => ['group_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'groupName' => Yii::t('app', 'Gerätegruppe(n)'),
			'insuranceName' => Yii::t('app', 'Versicherung'),
		];
	}

	public function fields()
	{
		return [
			'insurance_id',
			'insuranceName',
			'insurancePrice',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getInsurance()
	{
		return $this->hasOne(Article::className(), ['id' => 'insurance_id']);
	}

	/**
	 * @return string
	 */
	public function getInsuranceName()
	{
		return $this->insurance->product_name ?? null;
	}

	/**
	 * @return string
	 */
	public function getInsurancePrice()
	{
		return $this->insurance->price_sale ?? null;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup()
	{
		return $this->hasOne(ArticleGroups::className(), ['id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroupsForInsurance()
	{
		return $this->insurance->hasMany(ArticleGroups::className(), ['id' => 'group_id']);
	}

	/**
	 * @return string
	 */
	public function getGroupName()
	{
		return $this->group->short_name ?? null;
	}
}
