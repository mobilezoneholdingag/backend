<?php
namespace common\modules\insurances\models;

use common\models\Article;
use common\models\ArticleGroups;
use common\models\ProductCategory;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * @property string $highlights
 * @property string $highlight1
 * @property string $highlight2
 * @property string $highlight3
 * @property string $highlight4
 */
class ArticleInsurance extends Article
{
	protected $translateAttributes = [
		'additional_information',
		'term_link',
		'highlight1',
		'highlight2',
		'highlight3',
		'highlight4',
		'product_name'
	];
	public static function className($withNamespace = true)
	{
		if ($withNamespace) {
			return 'common\models\Article';
		}

		return 'Article';
	}
	public function rules()
	{
		return array_merge(parent::rules(), [
			[['insurance_duration'], 'number'],
		]);
	}

	public function fields()
	{
		return [
			'id',
			'manufacturer_id',
			'product_name',
			'insurance_duration',
			'additional_information',
			'term_link',
			'highlights' => 'highlightArray',
			'prices',
		];
	}

	public function getCommaSeparatedArticleGroupNames($defaultOnEmpty = 'Keine Zuweisung') : string
	{
		$result = implode(
			', ',
			ArrayHelper::map(
				ArticleGroupsInsurance::find()->leftJoin(ArticleGroups::tableName(), ['group_id' => 'id'])
					->where(['insurance_id' => $this->id])
					->all(),
				'group_id',
				'group.short_name'
			));
		return empty($result) ? $defaultOnEmpty : $result;
	}

	public static function find() : ActiveQuery
	{
		$query = parent::find();
		$query->where(['product_category_id' => ProductCategory::INSURANCE]);
		return $query;
	}

	public function getHighlight1(): string
	{
		return $this->getHighlight(0);
	}

	public function setHighlight1(string $highlight)
	{
		$this->setHighlight(0, $highlight);
	}

	public function getHighlight2(): string
	{
		return $this->getHighlight(1);
	}

	public function setHighlight2(string $highlight)
	{
		$this->setHighlight(1, $highlight);
	}

	public function getHighlight3(): string
	{
		return $this->getHighlight(2);
	}

	public function setHighlight3(string $highlight)
	{
		$this->setHighlight(2, $highlight);
	}

	public function getHighlight4(): string
	{
		return $this->getHighlight(3);
	}

	public function setHighlight4(string $highlight)
	{
		$this->setHighlight(3, $highlight);
	}

	public function getHighlight(int $index): string
	{
		return $this->getHighlights()[$index] ?? '';
	}

	public function setHighlight(int $index, string $highlight)
	{
		$highlights = $this->getHighlights();
		$highlights[$index] = $highlight;
		$this->highlights = json_encode($highlights);
	}

	public function getHighlights(): array
	{
		$highlights = json_decode($this->highlights, true) ?? [];
		ksort($highlights, SORT_ASC);

		return $highlights;
	}

	public function getHighlightArray(): array
	{
		return array_map(function($highlight) {
			return ['title' => $highlight];
		}, $this->getHighlights());
	}
}
