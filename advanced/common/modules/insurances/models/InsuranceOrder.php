<?php

namespace common\modules\insurances\models;

use common\models\Article;
use common\models\Customer;
use common\models\Manufacturer;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use components\filesystem\FileUploadBehavior;
use sammaye\audittrail\LoggableBehavior;
use yii\base\ModelEvent;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "order_insurance".
 *
 * @property integer $id
 * @property integer $insurance_article_id
 * @property string $insurance_name
 * @property integer $insurance_duration
 * @property integer $article_id
 * @property string $article_number
 * @property string $article_name
 * @property integer $customer_id
 * @property integer $order_id
 * @property integer $order_item_id
 * @property string $customer_lastname
 * @property string $customer_firstname
 * @property string $customer_street
 * @property string $customer_house_number
 * @property integer $customer_zip
 * @property string $customer_city
 * @property string $customer_phone_number
 * @property string $customer_email
 * @property string $customer_language
 * @property string $status
 * @property string $created_at
 * @property string $link
 * @property string $contract
 *
 * @property Article $article
 * @property Article $insuranceArticle
 * @property Customer $customer
 */
class InsuranceOrder extends \yii\db\ActiveRecord
{
	const STATUS_OPEN = 'open';
	const STATUS_ORDERED = 'ordered';
	const STATUS_CONTRACTED = 'contracted';
	const STATUS_ALL = ['open', 'ordered', 'contracted'];

	const LINK_PROTECT_CLEVER = 'http://sales.mz.helveticwarranty.ch/';

	public function __construct(array $config = [])
	{
		parent::__construct($config);

		$this->on(InsuranceOrder::EVENT_BEFORE_UPDATE, function(ModelEvent $event) {
			// if contract has been uploaded, change the status of the insurance order
			if ($this->isAttributeChanged('contract') && !empty($this->contract)) {
				$this->status = InsuranceOrder::STATUS_CONTRACTED;
			}
		});
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => new Expression('NOW()'),
			],
			'upload' => [
				'class' => FileUploadBehavior::class,
				'attributes' => ['contract'],
			],
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'order_insurance';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['insurance_article_id', 'insurance_duration', 'article_id', 'customer_id', 'customer_zip', 'order_id', 'order_item_id'], 'integer'],
			[['insurance_name', 'article_number', 'article_name', 'customer_lastname', 'customer_firstname', 'customer_street', 'customer_house_number', 'customer_city', 'customer_phone_number', 'customer_email', 'customer_language', 'status'], 'string', 'max' => 255],
			[['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
			[['insurance_article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['insurance_article_id' => 'id']],
			[['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
			[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
			[['order_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['order_item_id' => 'id']],
			[['contract'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'insurance_article_id' => 'Insurance Article ID',
			'insurance_name' => 'Insurance Name',
			'insurance_duration' => 'Insurance Duration',
			'article_id' => 'Article ID',
			'article_number' => 'Article Number',
			'article_name' => 'Article Name',
			'order_id' => 'Order ID',
			'order_item_id' => 'Order Item ID',
			'customer_id' => 'Customer ID',
			'customer_lastname' => 'Customer Lastname',
			'customer_firstname' => 'Customer Firstname',
			'customer_street' => 'Customer Street',
			'customer_house_number' => 'Customer House Number',
			'customer_zip' => 'Customer Zip',
			'customer_city' => 'Customer City',
			'customer_phone_number' => 'Customer Phone Number',
			'customer_email' => 'Customer Email',
			'customer_language' => 'Customer Language',
			'contract' => 'Contract',
			'status' => 'Status',
		];
	}

	/**
	 * @return string
	 */
	public function getLink()
	{
		if ($this->insuranceArticle->manufacturer->id !== Manufacturer::APPLE) {
			return static::LINK_PROTECT_CLEVER;
		}

		return '--';
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getArticle()
	{
		return $this->hasOne(Article::className(), ['id' => 'article_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getInsuranceArticle()
	{
		return $this->hasOne(Article::className(), ['id' => 'insurance_article_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}
}
