<?php

namespace common\modules\insurances\models;

use common\models\Article;
use common\models\ProductCategory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ArticleGroupsInsuranceSearch represents the model behind the search form about
 * `common\modules\insurances\models\ArticleGroupsInsurance`.
 */
class ArticleInsuranceSearch extends ArticleGroupsInsurance
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'insurance_id', 'group_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = ArticleInsurance::find();

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
			]
		);

		$dataProvider->query->orderBy(['group_id' => SORT_DESC]);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere(
			[
				'id' => $this->id,
			]
		);

		return $dataProvider;
	}
}
