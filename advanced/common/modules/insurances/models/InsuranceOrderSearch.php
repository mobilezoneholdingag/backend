<?php

namespace common\modules\insurances\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrderInsuranceSearch represents the model behind the search form about `common\modules\insuranceOrder\models\OrderInsurance`.
 */
class InsuranceOrderSearch extends InsuranceOrder
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'insurance_article_id', 'insurance_duration', 'article_id', 'customer_id', 'customer_zip'], 'integer'],
			[['insurance_name', 'article_number', 'article_name', 'customer_lastname', 'customer_firstname', 'customer_street', 'customer_house_number', 'customer_city', 'customer_phone_number', 'customer_email', 'customer_language', 'status'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = InsuranceOrder::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'insurance_article_id' => $this->insurance_article_id,
			'insurance_duration' => $this->insurance_duration,
			'article_id' => $this->article_id,
			'customer_id' => $this->customer_id,
			'customer_zip' => $this->customer_zip,
		]);

		$query->andFilterWhere(['like', 'insurance_name', $this->insurance_name])
			->andFilterWhere(['like', 'article_number', $this->article_number])
			->andFilterWhere(['like', 'article_name', $this->article_name])
			->andFilterWhere(['like', 'customer_lastname', $this->customer_lastname])
			->andFilterWhere(['like', 'customer_firstname', $this->customer_firstname])
			->andFilterWhere(['like', 'customer_street', $this->customer_street])
			->andFilterWhere(['like', 'customer_house_number', $this->customer_house_number])
			->andFilterWhere(['like', 'customer_city', $this->customer_city])
			->andFilterWhere(['like', 'customer_phone_number', $this->customer_phone_number])
			->andFilterWhere(['like', 'customer_email', $this->customer_email])
			->andFilterWhere(['like', 'customer_language', $this->customer_language])
			->andFilterWhere(['like', 'status', $this->status]);

		return $dataProvider;
	}
}
