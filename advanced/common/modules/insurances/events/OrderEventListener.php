<?php

namespace common\modules\insurances\events;

use common\models\Article;
use common\models\ArticleTariffs;
use common\modules\insurances\models\InsuranceOrder;
use common\modules\order\models\Item;
use common\modules\order\models\Order;
use yii\base\Component;
use yii\base\Event;
use yii\db\AfterSaveEvent;

class OrderEventListener extends Component
{
	public function init()
	{
		Event::on(Order::class, Order::EVENT_AFTER_UPDATE, function(AfterSaveEvent $event) {
			/** @var Order $order */
			$order = $event->sender;
			$attributes = array_keys($event->changedAttributes);

			if (in_array('serial_numbers', $attributes) && !empty($order->serial_numbers)) {
				if (empty($order->insuranceOrder) === false) {
					$order->insuranceOrder->status = InsuranceOrder::STATUS_OPEN;
					$order->insuranceOrder->save();
				}
			}
		});

		Event::on(Item::class, Item::EVENT_AFTER_INSERT, function($event) {
			/** @var Item $item */
			$item = $event->sender;

			if ($item->isInsurance()) {

				/** @var Article $parentItem */
				$parentItem = $item->parentItem->item;
				if ($item->parentItem->item instanceof ArticleTariffs) {
					$parentItem = $item->parentItem->item->article;
				}

				$insuranceOrder = new InsuranceOrder([
					'insurance_article_id' => $item->item->id,
					'insurance_name' => $item->item->product_name,
					'insurance_duration' => $item->item->insurance_duration,

					'article_id' => $parentItem->id,
					'article_number' => $parentItem->mat_no,
					'article_name' => $parentItem->product_name,

					'order_id' => $item->order->id,
					'order_item_id' => $item->id,

					'customer_id' => $item->order->customer->id,
					'customer_firstname' => $item->order->billingAddress->firstname,
					'customer_lastname' => $item->order->billingAddress->lastname,
					'customer_street' => $item->order->billingAddress->street,
					'customer_house_number' => $item->order->billingAddress->house_number,
					'customer_zip' => $item->order->billingAddress->zip,
					'customer_city' => $item->order->billingAddress->city,
					'customer_phone_number' => $item->order->billingAddress->contact_phone_number,
					'customer_email' => $item->order->customer->email,
					'customer_language' => $item->order->customer->language,
				]);

				// false for not validating, because string containing only numbers are casted to integer
				// and are failing
				$insuranceOrder->save(false);
			}
		});
	}
}
