<?php

use common\modules\insurances\models\ArticleInsurance;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $article ArticleInsurance */

$this->title = $article->id;
$this->params['breadcrumbs'][] = ['label' => 'Article Insurances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-insurance-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Update', ['update', 'id' => $article->id], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $article,
		'attributes' => [
			'id',
			'product_name',
			'insurance_duration',
			'highlight1',
			'highlight2',
			'highlight3',
			'highlight4',
			[
				'label' => Yii::t('app', 'Article group assignment'),
				'value' => $article->getCommaSeparatedArticleGroupNames(),
			],
		],

	]) ?>

</div>
