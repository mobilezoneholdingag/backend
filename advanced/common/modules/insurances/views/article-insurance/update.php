<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $article common\models\Article */

$this->title = Yii::t('app', 'Update {modelClass}: ', ['modelClass' => 'Article Insurance']) . $article->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Insurances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
	'label' => $article->id,
	'url' => ['view', 'id' => $article->id],
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="article-insurance-update">
	<h1><?= Html::encode($this->title) ?></h1>
	<?= $this->render('_form', ['article' => $article]) ?>
</div>




