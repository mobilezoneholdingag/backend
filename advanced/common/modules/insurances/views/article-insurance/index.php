<?php


use common\models\Article;
use common\models\ArticleGroups;
use common\modules\insurances\models\ArticleGroupsInsurance;
use yii\grid\GridView;
use components\Locale;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\insurances\models\ArticleInsuranceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$additionalInformationLength = 50;
$localeCode = Locale::SUISSE;

$additionalInformationClosure = function(Article $article, $key, $index, $columnObj) use ($additionalInformationLength) {
		$translatedAdditionalInfo = $article->getTranslatedAttribute('additional_information', $columnObj->contentOptions['languageCode']);
		if (isset($translatedAdditionalInfo)) {
			$suffix = '';
			if (strlen($translatedAdditionalInfo) > $additionalInformationLength) {
				$suffix = '...';
			}
			return substr($translatedAdditionalInfo, 0, $additionalInformationLength) . ' ' . $suffix;
		}
		return '';
	};


$this->title = Yii::t('app', 'Article_Insurances');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-insurance-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= GridView::widget(
		[
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				'id',
				[
					'attribute' => 'product_name',
					'label' => Yii::t('app', 'Article_Insurance')
				],
				'insurance_duration',
				[
					'label' => Yii::t('app', 'Gerätegruppenzuweisung'),
					'value' => function($article) {
						/** @var $article \common\modules\insurances\models\ArticleInsurance */
						return $article->getCommaSeparatedArticleGroupNames();
					},
				],
				[
					'label' => Yii::t('app', 'additional_information_ch'),
					'value' => $additionalInformationClosure,
					'contentOptions' => ['languageCode' => Locale::SUISSE],
				],
				[
					'label' => Yii::t('app', 'additional_information_fr'),
					'value' => $additionalInformationClosure,
					'contentOptions' => ['languageCode' => Locale::FRANCE],
				],
				[
					'label' => Yii::t('app', 'additional_information_it'),
					'value' => $additionalInformationClosure,
					'contentOptions' => ['languageCode' => Locale::ITALY],
				],
				'price_sale',
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{update}',
					'buttons' => [
						'update' => function($url, $article, $key) {
							$options = [
								'title' => Yii::t('yii', 'Update'),
								'aria-label' => Yii::t('yii', 'Update'),
								'data-pjax' => '0',
							];

							return Html::a(
								'<span class="glyphicon glyphicon-pencil"></span>',
								Url::to(['article-insurance/update', 'id' => $article->id]),
								$options
							);
						},
					],
				],
			],
		]
	); ?>


</div>
