<?php

use common\models\ArticleGroups;
use components\i18n\widget\TabWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $article common\modules\insurances\models\ArticleGroupsInsurance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-insurance-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($article, 'id')->textInput(['disabled' => 'disabled']) ?>
	<?= $form->field($article, 'id')->hiddenInput()->label(false) ?>
	<?= $form->field($article, 'product_name')->textInput(['disabled' => 'disabled']) ?>
	<?= $form->field($article, 'insurance_duration')->input('number') ?>
	<?= TabWidget::widget([
		'model' => $article,
		'form' => $form,
		'fieldsToRender' => [
			'highlight1',
			'highlight2',
			'highlight3',
			'highlight4',
		],
		'inputType' => TabWidget::TYPE_TEXTFIELD
	]); ?>
	<?= TabWidget::widget([
		'model' => $article,
		'form' => $form,
		'fieldsToRender' => [
			'term_link',
		],
		'inputType' => TabWidget::TYPE_TEXTFIELD
	]); ?>
	<?= TabWidget::widget(
		[
			'model' => $article,
			'form' => $form,
			'fieldsToRender' => ['additional_information',],
		]
	); ?>
	<br />
	<?= $form->field($article, 'group_id')->widget(Select2::class, [
		'data' => ArrayHelper::map(ArticleGroups::find()->asArray()->all(), 'id', 'short_name'),
		'options' => [
			'placeholder' => '- ' . Yii::t('app', 'please select') . ' -',
			'multiple' => true
		]
	]) ?>
	<div class="form-group">
		<?= Html::submitButton(
			Yii::t('app', 'Save'),
			['class' => 'btn btn-success']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
