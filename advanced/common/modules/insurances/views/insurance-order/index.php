<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\insurances\models\InsuranceOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Insurances';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-insurance-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (Yii::$app->user->can('starlord')): ?>
        <p>
            <?= Html::a('Create Order Insurance', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif;; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
//            'insurance_article_id',
            'insurance_name',
            'insurance_duration',
//            'article_id',
             'article_number',
             'article_name',
            // 'customer_id',
            // 'customer_firstname',
             'customer_lastname',
            // 'customer_street',
            // 'customer_house_number',
            // 'customer_zip',
            // 'customer_city',
            // 'customer_phone_number',
            // 'customer_email:email',
            // 'customer_language',
             'status',

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'template' => '{view} {update} ' . (Yii::$app->user->can('starlord') ? ' {delete}' : ''),
            ],
        ],
    ]); ?>
</div>
