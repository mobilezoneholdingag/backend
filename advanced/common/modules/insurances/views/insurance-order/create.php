<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\insurances\models\InsuranceOrder */

$this->title = 'Create Order Insurance';
$this->params['breadcrumbs'][] = ['label' => 'Order Insurances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-insurance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
