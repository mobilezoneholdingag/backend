<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\insurances\models\InsuranceOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Order Insurances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-insurance-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if (Yii::$app->user->can('starlord')): ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'contract',
	        // TODO: need formatter so shown as link??
	        'link',
            'insurance_article_id',
            'insurance_name',
            'insurance_duration',
            'article_id',
            'article_number',
            'article_name',
	        'order_id',
            'customer_id',
            'customer_lastname',
            'customer_firstname',
            'customer_street',
            'customer_house_number',
            'customer_zip',
            'customer_city',
            'customer_phone_number',
            'customer_email:email',
            'customer_language',
            'status',
        ],
    ]) ?>

</div>
