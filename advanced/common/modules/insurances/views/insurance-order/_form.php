<?php

use common\modules\insurances\models\InsuranceOrder;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\insurances\models\InsuranceOrder */
/* @var $form yii\widgets\ActiveForm */

$disabled = ['disabled' => 'disabled'];

?>

<div class="order-insurance-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-sm-6">
			<?php if ($model->status === InsuranceOrder::STATUS_OPEN): ?>
				<?= $form->field($model, 'contract')->fileInput() ?>
			<?php elseif ($model->status === InsuranceOrder::STATUS_CONTRACTED): ?>
				<div class="form-group">
					<label><?= Yii::t('app', 'Contract') ?></label><br>
					<a href="<?= $model->contract ?>" target="_blank"><?= $model->contract ?></a>
				</div>
			<?php endif; ?>

			<?php
			$statuses = array_combine(InsuranceOrder::STATUS_ALL, InsuranceOrder::STATUS_ALL);
			echo $form->field($model, 'status')->dropDownList($statuses, $disabled)
			?>

		    <?= $form->field($model, 'order_id')->textInput($disabled) ?>

		    <?= $form->field($model, 'order_item_id')->textInput($disabled) ?>

		    <?= $form->field($model, 'insurance_article_id')->textInput($disabled) ?>

		    <?= $form->field($model, 'insurance_name')->textInput($disabled + ['maxlength' => true]) ?>

		    <?= $form->field($model, 'insurance_duration')->textInput($disabled) ?>

		    <?= $form->field($model, 'article_id')->textInput($disabled) ?>

		    <?= $form->field($model, 'article_number')->textInput($disabled + ['maxlength' => true]) ?>

		    <?= $form->field($model, 'article_name')->textInput($disabled + ['maxlength' => true]) ?>

		</div>
		<div class="col-sm-6">
			<?= $form->field($model, 'customer_id')->textInput($disabled) ?>

			<?= $form->field($model, 'customer_lastname')->textInput($disabled + ['maxlength' => true]) ?>

			<?= $form->field($model, 'customer_firstname')->textInput($disabled + ['maxlength' => true]) ?>

			<?= $form->field($model, 'customer_street')->textInput($disabled + ['maxlength' => true]) ?>

			<?= $form->field($model, 'customer_house_number')->textInput($disabled + ['maxlength' => true]) ?>

			<?= $form->field($model, 'customer_zip')->textInput($disabled) ?>

			<?= $form->field($model, 'customer_city')->textInput($disabled + ['maxlength' => true]) ?>

			<?= $form->field($model, 'customer_phone_number')->textInput($disabled + ['maxlength' => true]) ?>

			<?= $form->field($model, 'customer_email')->textInput($disabled + ['maxlength' => true]) ?>

			<?= $form->field($model, 'customer_language')->textInput($disabled + ['maxlength' => true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
