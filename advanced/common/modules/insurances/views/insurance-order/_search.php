<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\insurances\models\InsuranceOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-insurance-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'insurance_article_id') ?>

    <?= $form->field($model, 'insurance_name') ?>

    <?= $form->field($model, 'insurance_duration') ?>

    <?= $form->field($model, 'article_id') ?>

    <?php // echo $form->field($model, 'article_number') ?>

    <?php // echo $form->field($model, 'article_name') ?>

    <?php // echo $form->field($model, 'customer_id') ?>

    <?php // echo $form->field($model, 'customer_lastname') ?>

    <?php // echo $form->field($model, 'customer_firstname') ?>

    <?php // echo $form->field($model, 'customer_street') ?>

    <?php // echo $form->field($model, 'customer_house_number') ?>

    <?php // echo $form->field($model, 'customer_zip') ?>

    <?php // echo $form->field($model, 'customer_city') ?>

    <?php // echo $form->field($model, 'customer_phone_number') ?>

    <?php // echo $form->field($model, 'customer_email') ?>

    <?php // echo $form->field($model, 'customer_language') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
