<?php

use deinhandy\yii2events\event\DispatchableEventInterface;

if (!function_exists('di')) {
	/**
	 * @param null|string $name
	 * @param array $params
	 *
	 * @return \deinhandy\yii2container\Container|mixed
	 */
	function di($name = null, $params = []) {
		if ($name === null) {
			return \Yii::$container;
		}

		return \Yii::$container->get($name, $params);
	}
}

if (!function_exists('dispatch')) {
	/**
	 * @param DispatchableEventInterface $event
	 *
	 * @return string
	 */
	function dispatch(DispatchableEventInterface $event) {
		/** @var \deinhandy\yii2events\dispatcher\EventDispatcherInterface $dispatcher */
		$dispatcher = di(\deinhandy\yii2events\dispatcher\EventDispatcherInterface::class);

		$dispatcher->dispatch($event);
	}
}
