<?php

use common\modules\jobOffer\JobOfferModule;
use common\modules\mobilezoneErp\MobilezoneErpModule;
use common\modules\order\OrderModule;
use common\modules\postfinance\PostFinanceModule;
use common\modules\prepayment\PrePaymentModule;
use common\modules\runscope\RunscopeModule;
use common\modules\swissbilling\SwissBillingModule;
use common\modules\swisscom\SwisscomModule;
use common\modules\testCleanup\TestCleanupModule;
use common\modules\feed\FeedModule;
use common\modules\homer\HomerModule;
use common\modules\insurances\InsurancesModule;
use common\modules\pricing\PricingModule;
use common\modules\platform\PlatformModule;
use common\modules\renewalRequest\RenewalRequestModule;
use common\modules\set\SetModule;
use components\Locale;

require_once 'services.php';

return [
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'basePath' => dirname(__DIR__),
	'sourceLanguage' => 'de-DE',
	'language' => 'de-CH',
	'name' => 'Mobilezone. Online-Shop & Website',
	'on beforeAction' => function($event) {
		Locale::setEnvironmentConfigurations(dirname(dirname(__DIR__)));
	},
	'bootstrap' => [
		'eventManager',
		'insurances',
		'renewalrequest',
		'postfinance',
		'swisscom',
		'mobilezoneerp',
		'order',
		'homer',
		'swissbilling',
		'set',
		'pricing',
		'platform',
		'runscope',
		'testcleanup',
		'feed',
		'joboffer',
		'prepayment',
	],
	'modules' => [
		'insurances' => [
			'class' => InsurancesModule::class,
		],
		'renewalrequest' => [
			'class' => RenewalRequestModule::class,
		],
		'postfinance' => [
			'class' => PostFinanceModule::class,
		],
		'swisscom' => [
			'class' => SwisscomModule::class,
		],
		'order' => [
			'class' => OrderModule::class,
		],
		'mobilezoneerp' => [
			'class' => MobilezoneErpModule::class,
		],
		'homer' => [
			'class' => HomerModule::class,
		],
		'swissbilling' => [
			'class' => SwissBillingModule::class,
		],
		'set' => [
			'class' => SetModule::class,
		],
		'pricing' => [
			'class' => PricingModule::class,
		],
		'platform' => [
			'class' => PlatformModule::class,
		],
		'feed' => [
			'class' => FeedModule::class,
		],
		'runscope' => [
			'class' => RunscopeModule::class,
		],
		'testcleanup' => [
			'class' => TestCleanupModule::class,
		],
		'joboffer' => [
			'class' => JobOfferModule::class,
		],
		'prepayment' => [
			'class' => PrePaymentModule::class,
		],
	],

	'components' => [
		'eventManager' => [
			'class' => 'events.manager',
		],
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=mysql;dbname=deinhandy',
			'username' => 'root',
			'password' => '123',
			'charset' => 'utf8',
		],
		'redis' => [
			'class' => yii\redis\Connection::class,
			'hostname' => 'redis',
		],
		'redis-session' => [
			'class' => yii\redis\Connection::class,
			'hostname' => 'redis',
			'database' => 1,
		],
		'cache' => [
			'class' => yii\redis\Cache::class,
		],
		'colors' => 'components\services\ColorService',
		'index' => [
			'class' => 'components\services\IndexService',
			'indexFiles' => [
				'devices' => '@frontend/runtime/index/devices.idx',
				'tariffs' => '@frontend/runtime/index/tariffs.idx',
			],
		],
		'ftps' => [
			'class' => 'components\services\FtpsService',
			'credentials' => [
				'host' => 'ftp.mistermobile.rocks',
				'port' => '21',
				'user' => 'mistermobile',
				'password' => 'BcHbKhp$rDw6XaqXVmX6',
			],
		],
		'cdn' => [
			'class' => 'components\services\RemoteFileService',
			'host' => 'rackspace',
		],
		'export' => [
			'class' => 'components\services\ExportService',
			'partners' => [
				'components\services\ExportService\partners\BilligerDePartner',
				'components\services\ExportService\partners\GuenstigerDePartner',
				'components\services\ExportService\partners\GoogleShoppingPartner',
				'components\services\ExportService\partners\DynamicYieldPartner',
				'components\services\ExportService\partners\VerivoxPartner',
				'components\services\ExportService\partners\ZanoxPartner',
				'components\services\ExportService\partners\LeadAlliancePartner',
				'components\services\ExportService\partners\AffilinetPartner',
				'components\services\ExportService\partners\CriteoPartner',
				'components\services\ExportService\partners\WonoPartner',
				'components\services\ExportService\partners\InternalPartner',
			],
			'formats' => [
				'csv' => [
					'class' => 'components\services\ExportService\formats\CsvExportFormat',
				],
				'xml' => [
					'class' => 'components\services\ExportService\formats\XmlExportFormat',
				],
			],
			'exportDir' => '@frontend/runtime/export',
		],
		'urlManager' => [
			'class' => 'yii\web\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'hostInfo' => 'http://www.stardust.dev',
		],
		'urlManagerFrontend' => [
			'class' => 'yii\web\UrlManager',
			'hostInfo' => 'http://www.stardust.dev',
			'enablePrettyUrl' => true,
			'showScriptName' => false,
		],
		'formatter' => [
			'currencyCode' => 'EUR',
			'timeZone' => 'Europe/Berlin',
		],
		'i18n' => [
			'translations' => [
				'app*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@common/messages',
					'forceTranslation' => true,
					'fileMap' => [
						'app' => 'app.php',
						'app/error' => 'error.php',
						'app/colors' => 'colors.php',
					],
				],
				'*' => [
					'class' => 'yii\i18n\DbMessageSource',
					'db' => 'db',
					'sourceMessageTable' => '{{%language_source}}',
					'messageTable' => '{{%language_translate}}',
					'cachingDuration' => 86400,
					'enableCaching' => false,
					// 'on missingTranslation' => ['components\i18n\TranslationEventHandler', 'handleMissingTranslation'],
				],
			],
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'viewPath' => '@common/mail',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'in-v3.mailjet.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
				'username' => '6b8c04807775547459b10284790855ac',
				'password' => 'e71f19a1a89ce4ad5fed7c6e64a5b244',
				'port' => '25',
				'encryption' => 'tls',
			],
		],
		'homerMailer' => [
			'class' => yii\swiftmailer\Mailer::class,
			'viewPath' => '@homer',
			'htmlLayout' => false,
			'textLayout' => false,
			'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'in-v3.mailjet.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
				'username' => '6b8c04807775547459b10284790855ac',
				'password' => 'e71f19a1a89ce4ad5fed7c6e64a5b244',
				'port' => '25',
				'encryption' => 'tls',
			],
			'view' => [
				'class' => 'yii\web\View',
				'renderers' => [
					'twig' => [
						'class' => yii\twig\ViewRenderer::class,
						'options' => [
							'auto_reload' => true,
						],
						'extensions' => YII_DEBUG ? [
							Twig_Extension_Debug::class,
							Twig_Extensions_Extension_I18n::class,
                            common\lib\twig\CurrencyExtension::class,
                            common\lib\twig\TranslationExtension::class,
						] : [
							Twig_Extensions_Extension_I18n::class,
                            common\lib\twig\CurrencyExtension::class,
                            common\lib\twig\TranslationExtension::class,
						],
						'globals' => [
							'OfferHelper' => common\helper\OfferHelper::class,
						],
						'functions' => ['t' => 'Yii::t'],
					],
				],
			],
		],
		'authClientCollection' => [
			'class' => 'yii\authclient\Collection',
			'clients' => [
				//                'google' => [
				//                    'class' => 'yii\authclient\clients\GoogleOAuth',
				//                    'clientId' => '945427952386-oej6m3j0n10o5dnjsibeun62ed5labri.apps.googleusercontent.com',
				//                    'clientSecret' => 'BOPAOv1KFDxGyZMztc56Fim4'
				//                ]
			],
		],
		'authManager' => [
			'class' => 'yii\rbac\DbManager',
		],
		'newrelic' => [
			'class' => 'bazilio\yii\newrelic\Newrelic',
			//            'name' => 'My App Frontend', // optional, uses Yii::$app->name by default
			//            'handler' => 'class/name', // optional, your custom handler
			//            'licence' => '...', // optional
			'enabled' => true // optional, default = true
		],
	],
];
