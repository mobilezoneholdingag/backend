<?php

Yii::$container = new \deinhandy\yii2container\Container();

require('helpers.php');

// backend, from server-side perspective
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('components', dirname(dirname(__DIR__)) . '/components');
Yii::setAlias('utils', dirname(dirname(__DIR__)) . '/utils');
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('tools', dirname(dirname(__DIR__)) . '/tools');
Yii::setAlias('imagesSavePath', dirname(dirname(__DIR__)) . '/frontend/web/images');
Yii::setAlias('mail', dirname(dirname(__DIR__)) . '/common/mail');
Yii::setAlias('mailAttachments', dirname(dirname(__DIR__)) . '/common/mail/attachments');
Yii::setAlias('rawData', dirname(dirname(__DIR__)) . '/common/rawData');
Yii::setAlias('orderUploadPath', dirname(dirname(__DIR__)) . '/backend/runtime/orders');
Yii::setAlias('backendRuntimePath', dirname(dirname(__DIR__)) . '/backend/runtime');

// frontend, from client-side perspective
Yii::setAlias('webImages', '/images');
Yii::setAlias('providerImages', '/images/provider');
Yii::setAlias('manufacturerImages', '/images/manufacturer');

Yii::setAlias('hostInfoLocal', 'http://deinhandy.dev');
Yii::setAlias('hostInfoStaging', 'http://deinhandy.rs');
Yii::setAlias('hostInfoLive', 'https://www.deinhandy.de');

// TODO Obsolete the following aliases in favor of UrlManager methods (APP-141)
Yii::setAlias('domainName', YII_ENV_DEV ? 'deinhandy.dev' : 'deinhandy.de');
Yii::setAlias('frontendUrl', YII_ENV_DEV ? 'http://deinhandy.dev' : 'https://www.deinhandy.de');
Yii::setAlias('backendUrl', YII_ENV_DEV ? 'http://backend.deinhandy.dev' : 'http://backend.deinhandy.de');