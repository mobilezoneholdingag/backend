<?php
use components\dataLayer\DynamicYieldDataLayerBehavior;
use OpenCloud\Rackspace;

return [
	'shop' => 'de',
	'adminEmail' => 'maik.manthey@deinhandy.de',
	'supportEmail' => 'shitstorm@deinhandy.de',
	'domain' => 'deinhandy.de',
	'titleSuffix' => ' | deinhandy.de',
	'user.passwordResetTokenExpire' => 3600,
	'simCardTypes' => [
		0 => 'Mini SIM',
		1 => 'Micro SIM',
		2 => 'Nano SIM',
	],
	'os_platforms' => [
		1 => 'Android',
		2 => 'Windows',
		3 => 'iOS',
		4 => 'Blackberry OS',
		5 => 'Sonstige',
	],
	'filter' => [
		'article' => [
			'camera' => [
				'small' => ['<', 1.3],
				'medium' => ['<', 5.1],
				'big' => ['<', 8.1],
				'huge' => ['>=', 8.1]
			],
			'display' => [
				'small' => ['<', 4.1],
				'medium' => ['<', 4.7],
				'big' => ['<', 5.3],
				'huge' => ['>=', 5.3]
			]
		],
		'TariffSpecial.types' => [
			0 => '---',
			1 => 'Kombi-Vorteil',
			2 => 'ADAC-Rabatt'
		],
		'tariff' => [
			'datavolumes' => [
				'basis' => [1, 500],
				'allround' => [501, 1000],
				'addicted' => [1001, 2000],
				'surplus' => [2001]
			],
			'pricing' => [
				'budget' => [0, 30],
				'medium' => [30, 40],
				'premium' => [40]
			]
		]
	],
	'payment_types' => [
		0 => 'Vorkasse',
		1 => 'Nachnahme',
		2 => 'Sofortüberweisung',
		3 => 'Paypal',
		4 => 'Bankeinzug'

	],
	'passportTypes' => [
		1 => 'Personalausweis (alt)',
		2 => 'Personalausweis (neu)',
		3 => 'Reisepass (Deutschland)',
		4 => 'Reisepass (Ausland)',
		5 => 'Schweizerpass',
		6 => 'Identitätskarte',
		7 => 'Führerausweis',
		8 => 'Ausländerausweis B',
		9 => 'Ausländerausweis C',
		10 => 'Ausländerausweis G',
		11 => 'Ausländerausweis L',
		12 => 'Ausländerausweis F',
		13 => 'Ausländerausweis N',
		14 => 'Handelsregistereintrag',
	],
	'passportTypesNamedKeys' => [
		'Personalausweis (alt)' => 'Personalausweis (alt)',
		'Personalausweis (neu)' => 'Personalausweis (neu)',
		'Reisepass (Deutschland)' => 'Reisepass (Deutschland)',
		'Reisepass (Ausland)' => 'Reisepass (Ausland)',
	],
	// EAM's cancellation codes, it's necessary to put this here as there's no API to get all stati
	'withdrawalStatusCodesEAM' => [
		1 => 'eAm: Suspekte Bestellung',
		2 => 'eAm: Kartengrenze erreicht',
		3 => 'eAm: Mehrfachbestellung',
		4 => 'Kunde: Falsche Angaben vom Kunden',
		5 => 'Kunde: Spassbestellung',
		6 => 'Kunde: Storno auf Kundenwunsch',
		7 => 'Kunde: Vertragsunterlagen nicht eingereicht',
		8 => 'Rücksendung: Annahme verweigert',
		9 => 'Rücksendung: Empfänger Unbekannt',
		10 => 'Rücksendung: Ident Negativ',
		11 => 'Rücksendung: Nicht abgeholt',
		12 => 'sonstiges',
		13 => 'eAm: Zuzahlung nicht geleistet',
	],
	// key of our provider for bank account validation checks
	'externalServices.ckonto.key' => '150318:robert.ermich@deinhandy.de:293973977037',

	//DE order confirmation mail
	'mailing.confirmation.senderEmail' => 'support@deinhandy.de',
	'mailing.confirmation.senderEmailNoReplay' => 'no-reply@deinhandy.de',
	'mailing.confirmation.senderEmailNewsletter' => 'newsletter@deinhandy.de',
	'mailing.senderEmailSupport' => ['onlineshop@mobilezone.ch' => 'MOBILEZONE.CH'],
	'mailing.confirmation.senderEmailVersicherung' => ['versicherung@deinhandy.de' => 'Versicherung'],
	'mailing.confirmation.subject' => 'Bestätigung Ihrer Bestellung.',
	'mailing.resubmitOrder.email' => ['kundenhilfe@deinhandy.de' => 'Kundenhilfe DEINHANDY.'],
	'mailing.resubmitOrder.subject' => '❗Es ist was schief gelaufen❗',
	'mailing.renewalRequestOffer.senderEmailSupport' => ['onlineshop@mobilezone.ch' => 'MOBILEZONE.CH'],
	'mailing.renewalRequestOffer.subject' => 'Verlängerungsangebot',

	//CH order confirmation mail
	'mailing.confirmation.senderEmailBestellung.ch' => ['bestellung@deinhandy.ch' => 'DEINHANDY.'],

	//Newsletter
	'mailing.newsletter.optin.subject' => 'Newsletteranmeldung DEINHANDY 💚',
	//DE
	'mailing.newsletter.optin.senderEmail.de' => 'newsletter@deinhandy.de',
	//CH
	'mailing.newsletter.optin.senderEmail.ch' => 'newsletter@deinhandy.ch',

	// send confirmation mails in development to this address (to prevent using real customers' addresses)
	// in development environment, put your email here to send mails to YOU
	'mailing.confirmation.developmentAddress' => 'christian.soth@deinhandy.de',
	// newsletter
	'mailing.newsletter.optin.senderEmail' => 'support@deinhandy.de',

	// contact page receiver
	//DE
	'contactPage.senderEmail' => 'support@deinhandy.de',
	'contactPage.receiverEmail' => 'info@deinhandy.de',
	//CH
	'contactPage.receiverEmail.ch' => 'info@deinhandy.ch',

	// EAM's API credentials
	'api.contentChange.mailAddress' => [
		'maik.manthey@deinhandy.de',
		'puya.rahimkhan@deinhandy.de',
		'raphael.leifkes@deinhandy.de',
		'christina.jesser@deinhandy.de',
		'daniel.engelbrecht@deinhandy.de'
	],
	'api.drillisch.cartFail' => ['maik.manthey@deinhandy.de', 'puya.rahimkhan@deinhandy.de',
		'raphael.leifkes@deinhandy.de', 'kolja.zuelsdorf@deinhandy.de', 'christian.soth@deinhandy.de'],
	'searchIndex' => [
		'directory' => '@frontend/runtime/index',
		'files' => [
			'devices' => 'devices.idx',
			'tariffs' => 'tariffs.idx'
		]
	],
	// upper device limits in the keys
	'schutzklick.prices' => [
		250 => 34.95,
		500 => 44.95,
		750 => 49.95,
		1000 => 59.95,
		1500 => 69.95,
	],
	'googleStoreId' => 101273403,
	'google.country' => 'de',

	'filesystems' => [
		'default' => 'rackspace',
		'rackspace' => [
			'adapter' => 'fs.adapter.rackspace',
			'endpoint' => Rackspace::UK_IDENTITY_ENDPOINT,
			'region' => 'LON',
			'username' => 'mobilezone.apiuser',
			'container' => 'dev-mobilezone-marketing-images',
			'apiKey' => '28b8e409559e4c3cbcb96fb72f11eedc',
			'rootDir' => 'contracts/',
			'url' => 'https://1c0638385cabc463a7a1-beaedfd8b033357ad701cf7f66dc92e9.ssl.cf3.rackcdn.com/',
		],
		'contracts' => [
			'adapter' => 'fs.adapter.rackspace',
			'endpoint' => Rackspace::UK_IDENTITY_ENDPOINT,
			'region' => 'LON',
			'username' => 'mobilezone.apiuser',
			'container' => 'mobilezone-contracts',
			'apiKey' => '28b8e409559e4c3cbcb96fb72f11eedc',
			'rootDir' => '',
			'url' => 'https://13c13951b7e9e17bc2aa-46a67c120cede4318ee150b05a9ddac1.ssl.cf3.rackcdn.com/',
		],
		'feeds' => [
			'adapter' => 'fs.adapter.rackspace',
			'endpoint' => Rackspace::UK_IDENTITY_ENDPOINT,
			'region' => 'LON',
			'username' => 'mobilezone.apiuser',
			'container' => 'dev-mobilezone-feeds',
			'apiKey' => '28b8e409559e4c3cbcb96fb72f11eedc',
			'rootDir' => '',
			'url' => 'https://13c13951b7e9e17bc2aa-46a67c120cede4318ee150b05a9ddac1.ssl.cf3.rackcdn.com/',
		],
		'exports' => [
			'adapter' => 'fs.adapter.rackspace',
			'endpoint' => Rackspace::UK_IDENTITY_ENDPOINT,
			'region' => 'LON',
			'username' => 'mobilezone.apiuser',
			'container' => 'mobilezone-exports',
			'apiKey' => '28b8e409559e4c3cbcb96fb72f11eedc',
			'rootDir' => '',
			'url' => '',
		],

	],

	'host' => [
		'rackspace' => [
			'directories' => [
				'default' => [
					'container' => [
						'name' => 'dev-mobilezone-marketing-images',
						'address' => 'https://1c0638385cabc463a7a1-beaedfd8b033357ad701cf7f66dc92e9.ssl.cf3.rackcdn.com'
					],
					'dir' => null,
					'region' => 'LON'
				],
				'images' => [
					'dir' => 'images'
				],
				'articleImages' => [
					'dir' => 'images/article'
				],
			]
		]
	],
	'jira' => [
		'url' => 'https://deinhandy.atlassian.net',
		'credentials' => [
			'username' => 'Deploy Account',
			'password' => 'FUtmIxN0k5tg5wNkdlGC'
		]
	],
	// FTP credentials for Verivox
	'VerivoxImportsFTPConfig' => [
		'host' => 'app.verivox.de',
		'port' => 22,
		'username' => 'deinhandy',
		'password' => 'rsimEAaZAE1XDBYR4QM7',
		//'privateKey' => 'path/to/or/contents/of/privatekey',
		'root' => '/home/deinhandy',
		'timeout' => 10,
	],
	// FTP credentials for Check24
	'Check24ImportsFTPConfig' => [
		'host' => 'ftps-telco.check24.de',
		'username' => 'deinhandy',
		'password' => 'RfZz3hOMPhZBMA8W',

		/** optional config settings */
		'port' => 21,
		'root' => '/',
		'passive' => true,
		'ssl' => true,
		'timeout' => 30,
	],
	DynamicYieldDataLayerBehavior::ACTIVE_STATE_PARAM_NAME => true,
	'picasso' => [
		'http://picasso.vokky.net/storage',
		'http://picasso.vokky.net/api/v1/',
		'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg0ODdmNGI0N2UxY2RiMzI0YzI2MTU1MGM2Y2QwMWNkYTY1YzQ5OTJiMGEyNmE0N2FhOTMxOTQ4YjRhNmYzMDJmNmQ4YmY4ZTgyMjhhNjYzIn0.eyJhdWQiOiIxIiwianRpIjoiODQ4N2Y0YjQ3ZTFjZGIzMjRjMjYxNTUwYzZjZDAxY2RhNjVjNDk5MmIwYTI2YTQ3YWE5MzE5NDhiNGE2ZjMwMmY2ZDhiZjhlODIyOGE2NjMiLCJpYXQiOjE0ODExMzQzODksIm5iZiI6MTQ4MTEzNDM4OSwiZXhwIjo0NjM2ODA3OTg5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.cQjNu88XB6_q_DeL1ZeSF2tl3OtBxljAva8F8MvG8yR9md7nsLiDTv3I3drIJEAvfJ1H9WUMflU5i5J2MuAgJ1LslOk6cfKbQ9sLSwn8CqY4htoIsGKxuhvNkMsIWizkwT_aCcKRR0isMNvwJbfLV6ZMzfHqoDDX1GdFmB7FEzEX8YzPDqF87cgrJ08N_n8u0p85V1kwyRC5SRAOARO3MwoQY0oF4mCGFvwY6GlCjtzTm8j4yTtpU67ExVdLd9ZOXEXOVVmqYyMZ5KdIOKkw10ca-FyG6tB3ZS5N93GEn93XjiZ5_1DHRrq-PRewLxNNBbV-UeCaYD7cg5P-ChhNFf59iPayc8zbPZpY3nkT-cxpObuAT6GogzJFJvAVrlmtqSG87QWrxl8v8AHgx2v0VvxNuAs45aLQjjBGMDD6NIKG6G53J17bPwaMFH5c8PbljBmfl3-dbmzT2tei0L05T4VyMUbvrGBqhdF15Kjke_3jEUiKfmcnxp524FoWv5ViwSgabwWGO1--NlIoj5GmQxmXJjyOn_lQuotRoqljnNGyvdYkvdgKxRRKkG8xwz1KF5Y1cDneNiw1A907EVXCBa0FSugXbh-hsktdQSyMZx7dvP0M98VxGdE7TW1Bd_i19JaD1IpR5Z0_XakLKAxTBZ4sQNFzNx_C0KGyx_H3eTc',
	],
	'picasso_namespace' => 'mobilezone',
	'language_domain_map' => [
		'de' => 'http://www.stardust.dev',
		'de-CH' => 'http://www.stardust.dev',
		'fr' => 'http://fr.stardust.dev',
		'fr-FR' => 'http://fr.stardust.dev',
		'it' => 'http://it.stardust.dev',
		'it-IT' => 'http://it.stardust.dev',
	],
];
