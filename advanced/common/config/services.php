<?php

use backend\models\content\file\PdfUpload;
use common\events\ArticleTariffPricesListener;
use components\filesystem\FilesystemProvider;
use components\filesystem\FilesystemProviderInterface;
use OpenCloud\Rackspace;
use yii\di\Container;
use League\Flysystem\Rackspace\RackspaceAdapter;

/** @var \deinhandy\yii2container\Container $container */
$container = Yii::$container;

$container->set(FilesystemProviderInterface::class, FilesystemProvider::class);

$container->set('fs.adapter.rackspace', function(Container $container, $config) {
	$client = new Rackspace($config['endpoint'], [
		'username' => $config['username'],
		'apiKey' => $config['apiKey'],
	]);

	$store = $client->objectStoreService('cloudFiles', $config['region']);
	$container = $store->getContainer($config['container']);

	return new RackspaceAdapter($container, $config['rootDir']);
});

$container->set('fs.adapter.local', function(Container $container, $config) {
	return new \League\Flysystem\Adapter\Local($config['rootDir']);
});

$container->setSingleton(ArticleTariffPricesListener::class, ArticleTariffPricesListener::class);
$container->addTags(ArticleTariffPricesListener::class, ['events.listener']);

$container->set('pdfUpload.contract', function(Container $container) {
	$fsProvider = di(FilesystemProviderInterface::class);

	return new PdfUpload($fsProvider, 'contracts');
});


