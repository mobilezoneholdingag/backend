<?php

namespace common\events;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use deinhandy\yii2events\listener\AbstractEventListener;
use yii\base\Event;

class ArticleTariffPricesListener extends AbstractEventListener
{
	public function getClassEvents() : array
	{
		return [
			ArticleTariffs::class => [
				ArticleTariffs::EVENT_BEFORE_UPDATE => [$this, 'onArticleTariff'],
				ArticleTariffs::EVENT_BEFORE_INSERT => [$this, 'onArticleTariff'],
			],
			Article::class => [
				Article::EVENT_BEFORE_UPDATE => [$this, 'onArticle'],
			],
			Tariffs::class => [
				Tariffs::EVENT_BEFORE_UPDATE => [$this, 'onTariff'],
			]
		];
	}

	/**
	 * @param Event $event
	 */
	public function onArticleTariff(Event $event)
	{
		/** @var ArticleTariffs $articleTariff */
		$articleTariff = $event->sender;

		if (empty($articleTariff->getDirtyAttributes(['price', 'price_monthly'])) === false) {
			$articleTariff->updateCachedPrices();
		}
	}

	/**
	 * @param Event $event
	 */
	public function onArticle(Event $event)
	{
		/** @var Article $article */
		$article = $event->sender;

		if (empty($article->getDirtyAttributes(['price_sale', 'sunrisenetprice'])) === false) {
			$article->on(Article::EVENT_AFTER_UPDATE, function(Event $event) {
				/** @var Article $article */
				$article = $event->sender;

				foreach($article->articleTariffs as $articleTariff) {
					$articleTariff->updateCachedPrices()->save();
				}
			});
		}
	}

	/**
	 * @param Event $event
	 */
	public function onTariff(Event $event)
	{
		/** @var Tariffs $tariff */
		$tariff = $event->sender;
		$tariff->updateRatePayment();

		if (empty($tariff->getDirtyAttributes(['price_monthly', 'contract_duration'])) === false) {
			$tariff->on(Tariffs::EVENT_AFTER_UPDATE, function(Event $event) {
				/** @var Tariffs $tariff */
				$tariff = $event->sender;

				foreach($tariff->articleTariffs as $articleTariff) {
					$articleTariff->updateCachedPrices()->save();
				}
			});
		}
	}
}
