<?php
return [
	'Service_Headline_Accessory' => '',
	'Service_Text_Accessory' => '',
	'Service_Link_Accessory' => 'http://accessories.mobilezone.ch',

	'Service_Headline' => 'Haben Sie Fragen?',
	'Service_Send_Mail' => 'Schreiben Sie uns ein Email an',
	'Mail_Address' => 'onlineshop@mobilezone.ch',
	'Web_Address' => 'www.mobilezone.ch',
	'Service_Call' => 'oder rufen Sie uns an unter',
	'Fax_Number' => '+41433887790',
	'Phone_Number' => '+41433887799',
	'Company_Name' => 'mobilezone ag',
	'Company_Street' => 'Riedthofstrasse 124',
	'Company_PLZ' => '8105',
	'Company_City' => 'Regensdorf',
	'Short_Tel' => 'Tel',
	'Short_Fax' => 'Fax',
	'Short_Mail' => 'Mail',
	'Service_Ending' => 'Wir freuen uns auf Ihre Antwort!',
	'Service_Greeting' => 'Ihr Team von mobilezone',

	'Service_DoNotReply' => 'Dies ist eine automatisch versendete Nachricht. Bitte antworte nicht auf dieses Schreiben, da die Adresse nur zur Versendung von E-Mails eingerichtet ist.',

	'Shipment_Title' => 'Versandbestätigung Ihrer mobilezone Bestellung',
	'Shipment_Success' => 'Ihre Bestellung wurde für den Versand an die Post weitergeleitet. Gerne können Sie die Bestellung unter folgendem Link verfolgen:',
	'Shipment_Ranking' => 'Damit wir unseren Service laufend verbessern können, freuen wir uns auf eine Bewertung auf',
	'Contract_Configurator_Subject' => 'Anfrage für TV, Internet und Festnetz',
	'Contract_Confirmation_Title' => 'Anfrage für TV, Internet und Festnetz',
];
