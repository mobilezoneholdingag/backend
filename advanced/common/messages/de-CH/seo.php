<?php
return [
	#manufacturer
	'Lg' => 'LG',
	'Htc' => 'HTC',

	#groups
	'Iphone 6' => 'iPhone 6',
	'Iphone 5s' => 'iPhone 5s',
	'Iphone 6 Plus' => 'iPhone 6 Plus',
	'Ipad Air 2' => 'iPad Air 2',
	'Ipad Mini 3' => 'iPad Mini 3',

	#provider
	'Eplusyildiz' => 'eplus ay yildiz',

	#bundles
	'Moto G Galaxy Tab 3 70' => 'Moto G Galaxy + Tab 3 7.0',
	'Galaxy Alpha Galaxy Tab 3 70' => 'Galaxy Alpha + Galaxy Tab 3 7.0',
	'Galaxy S5 Galaxy Tab 3 70' => 'Galaxy S5 + Galaxy Tab 3 7.0',
	'Galaxy S5 Mini Galaxy Tab 3 70' => 'Galaxy S5 Mini + Galaxy Tab 3 7.0',
	'One Mini 2 Galaxy Tab 3 70' => 'One Mini 2 + Galaxy Tab 3 7.0',
	'Galaxy Tab A 97' => 'Galaxy Tab A 9.7 LTE',

	// meta description
	'Du suchst nach einem passenden ' => 'Du suchst nach dem passenden ',
	'Vertrag für dein neues Smartphone oder Tablet' => 'Abo für Dein Smartphone oder Tablet',
	'Bei uns findest Du eine Auswahl der neuesten Smartphones auf deinhandy.de' => 'Wir haben die besten Abos mit unlimitiertem Internet und Telefonie auf deinhandy.ch!',
	'Bei uns findest Du aktuelle Tarifangebote aller großen Tarifanbieter auf deinhandy.de' => 'Bei uns findest Du aktuelle Aboangebote aller großen Provider auf deinhandy.ch!',
	'Tarife und Vertragsangebote' => 'Abos und Angebote',
	'mit einem passenden Vertrag' => 'mit einem passenden Abo',
	' mit Vertrag' => ' mit Abo',
	' ohne Vertrag' => ' ohne Abo',
	'Bei uns findest du aktuelle Angebote aller namhaften Tarifanbieter!' => 'Bei uns findest Du Dein Handy zum Top-Preis mit dem passenden Vertrag im Netz Deiner Wahl!'
];