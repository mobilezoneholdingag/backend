<?php
return [
	'white' => 'weiß',
	'grey'  => 'grau',
	'black' => 'schwarz',
	'red'   => 'rot',
	'gold'  => 'gold',
	'silver'=> 'silber',
	'blue'  => 'blau',
	'yellow'=> 'gelb'
];