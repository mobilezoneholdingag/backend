<?php
return [

	#Common Translation
	'Save' => 'Speichern',
	'Create' => 'Erstellen',
	'Update' => 'Bearbeiten',
	'Delete' => 'Löschen',
	'Select' => 'Auswählen',
	'Please select' => 'Bitte auswählen',
	'Devices' => 'Geräte',
	'Device' => 'Gerät',
	'Tariff' => 'Tarif',
	'Update all' => 'Alle aktualisieren',
	'Edit all' => 'Alle Bearbeiten',

	#article
	'Create Device' => 'Gerät erstellen',
	'Create new Device' => 'Neues Gerät erstellen',
	'Save details for all variations' => 'Produktdetails für alle Variationen speichern',
	'Operating System' => 'Betriebssystem',

	#articleDetails
	'Article details' => 'Gerätedetails',

	#article-tariffs-keep-list
	'Create Keep List Entry' => 'Blockierung einrichten',
	'API Price Blocker for Article Tariffs' => 'API Überschreibschutz für Artikeltarife',

	#Columns
	'Availability' => 'Verfügbarkeit',
	'Delivery Status' => 'Lieferstatus',
	'API Price' => 'API Preis',
	'DB Price' => 'DB Preis',
	'Price' => 'Preis',
	

];