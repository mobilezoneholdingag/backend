<?php
return [
	#manufacturer
	'Lg' => 'LG',
	'Htc' => 'HTC',

	#groups
	'Iphone 6' => 'iPhone 6',
	'Iphone 5s' => 'iPhone 5s',
	'Iphone 6 Plus' => 'iPhone 6 Plus',
	'Ipad Air 2' => 'iPad Air 2',
	'Ipad Mini 3' => 'iPad Mini 3',

	#provider
	'Eplusyildiz' => 'eplus ay yildiz',

	#bundles
	'Moto G Galaxy Tab 3 70' => 'Moto G Galaxy + Tab 3 7.0',
	'Galaxy Alpha Galaxy Tab 3 70' => 'Galaxy Alpha + Galaxy Tab 3 7.0',
	'Galaxy S5 Galaxy Tab 3 70' => 'Galaxy S5 + Galaxy Tab 3 7.0',
	'Galaxy S5 Mini Galaxy Tab 3 70' => 'Galaxy S5 Mini + Galaxy Tab 3 7.0',
	'One Mini 2 Galaxy Tab 3 70' => 'One Mini 2 + Galaxy Tab 3 7.0',
	'Galaxy Tab A 97' => 'Galaxy Tab A 9.7 LTE',

];