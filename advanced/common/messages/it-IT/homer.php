<?php
return [
    'Service_Headline_Accessory' => 'Non avete trovato gli accessori giusti?',
    'Service_Text_Accessory' => 'Nel nostro online shop dedicato agli accessori potrete trovare una selezione di oltre 10\'000 articoli. Fateci visita subito su:',
    'Service_Link_Accessory' => 'http://accessories.mobilezone.ch',

    'Service_Headline' => 'Ha delle domande?',
    'Service_Send_Mail' => 'Scriva una email all\'indirizzo',
    'Mail_Address' => 'onlineshop@mobilezone.ch',
    'Web_Address' => 'www.mobilezone.ch',
    'Service_Call' => 'o ci chiami al numero',
    'Fax_Number' => '+41433887790',
    'Phone_Number' => '+41433887799',
    'Company_Name' => 'mobilezone ag',
    'Company_Street' => 'Riedthofstrasse 124',
    'Company_PLZ' => '8105',
    'Company_City' => 'Regensdorf',
    'Short_Tel' => 'Tel',
    'Short_Fax' => 'Fax',
    'Short_Mail' => 'Mail',
    'Service_Ending' => 'Rimaniamo in attesa della vostra risposta!',
    'Service_Greeting' => 'Il vostro Team di mobilezone',

    'Service_DoNotReply' => 'Questo è un messaggio automatico. Vi preghiamo di non rispondere a questo messaggio poiché questo indirizzo e-mail è configurato solamente per l’invio di e-mail.',

	'Shipment_Title' => 'Conferma d\'invio della suo ordine mobilezone',
	'Shipment_Success' => 'Il suo ordine è stato inoltrato alla posta per l\'invio. Può controllare il suo ordine sfruttando il link qui sotto:',
	'Shipment_Ranking' => 'Al fine di migliorare continuamente il nostro servizio, ci rallegriamo di una sua valutazione tramite questo link',
	'Contract_Configurator_Subject' => 'Richiesta d\'offerta per TV digitale, internet e rete fissa',
	'Contract_Confirmation_Title' => 'Richiesta d\'offerta per TV digitale, internet e rete fissa',
];
