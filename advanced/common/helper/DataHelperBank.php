<?php

namespace common\helper;

use Yii;

class DataHelperBank
{
	public static function getIbanFromBankAccount($accountNumber, $accountBlz)
	{
		// remove empty spaces, transform to uppercase (necessary for proper check)
		$accountBlz = strtoupper(trim(str_replace(' ', '', $accountBlz)));
		// don't validate if string does not consist of A-Z and 0-9 characters
		if (empty($accountBlz) OR !preg_match('/[0-9]/', $accountBlz)) {
			return false;
		}

		// remove empty spaces, transform to uppercase (necessary for proper check)
		$accountNumber = strtoupper(trim(str_replace(' ', '', $accountNumber)));
		// don't validate if string does not consist of A-Z and 0-9 characters
		if (empty($accountNumber) OR !preg_match('/[0-9]/', $accountNumber)) {
			return false;
		}

		// key for using the validation tool
		$key = Yii::$app->params['externalServices.ckonto.key'];

		$result = shell_exec(Yii::getAlias('@tools') . '/cKonto iban "knr=' . $accountNumber . '&blz=' . $accountBlz . '&ccd=DE&key=' . $key . '"');

		// the code is the sixth character of the returned string
		$resultCode = substr($result, 5, 1);
		if ($resultCode == '1') {
			// extract 22-char IBAN number from result string
			$accountIBAN = substr($result, 11, 22);
			return $accountIBAN;
		} else {
			return false;
		}
	}
}