<?php

namespace common\helper;

use Yii;

class DataHelperCheck24
{
	/**
	 * Returns EAM's provider id for any given Check24 "provider id"
	 *
	 * @param $providerIdString string Check24's provider id
	 * @return null|integer EAM's provider id (or null if nothing fits)
	 */
	public static function getEAMProviderIdByCheck24ProviderId($providerIdString)
	{
		// left is Check24's id, right is EAM's provider id
		$providerMappingArray = [
			'ALPH' => 8,
			'ARCO' => 9,
			'CELL' => 89,
			//'DPLU'
			'DEBI' => 26,
			'DTAG' => 138,
			'DRIL' => 34,
			'HUTC' => 149,
			'LIFT' => 78,
			'MOBC ' => 89,
			//'TEL2
			//'RSLC
			'TALK' => 140,
			'TANG' => 144,
			//'TELE
			'TMOB' => 138,
			'O2DE' => 102,
			'VICT' => 160,
			'VODA' => 164,
			'EWET' => 44,
			'ACNM' => 2,
			'EASY' => 21,
			'KLAR' => 74,
			'SIMP' => 124,
			'SYMP' => 137,
			'SIMY' => 126,
			'BLAU' => 16,
			'SERV' => 101,
			'TBIN' => 145,
			'DELI' => 27,
			'TONE' => 138,
			'ALMO' => 164,
			'VSTR' => 162,
			'ORTE' => 104,
			'MSIM' => 85,
			'ENSE' => 41,
			'1UN1' => 1,
			'VTEL' => 159,
			'CONG' => 23,
			'NECO' => 96,
			'ECOM' => 38,
			//'CALX
			'GEMO' => 58,
			//'TLVS
			'GMOS' => 59,
			'LYCA' => 80,
			'FONI' => 52,
			//'MOCO
			'GTCM' => 64,
			'EDEK' => 39,
			'HNET' => 66,
			'MNET' => 81,
			'AMWL' => 7,
			'PTTM' => 112,
			//'UMOB
			'AMOB' => 6,
			//'ENCO
			'KDMO' => 73,
			'CKCM' => 15,
			'ONEP' => 103,
			'LEBA' => 76,
			'KBWM' => 72,
			'WTEL' => 169,
			'CYWL' => 7,
			'TURK' => 154,
		];

		if (array_key_exists($providerIdString, $providerMappingArray)) {
			return $providerMappingArray[$providerIdString];
		} else {
			return null;
		}
	}
}
