<?php

namespace common\helper;

use Yii;

class DateHelper
{
	/**
	 * Returns the current time in Berlin timezone.
	 *
	 * @return string
	 */
	public static function getDateTimeNow()
	{
		$timezone = new \DateTimeZone('Europe/Berlin');
		$datetime = new \DateTime();
		$datetime->setTimezone($timezone);

		return $datetime->format('Y-m-d H:i:s');
	}

	/**
	 * Takes a datetime string (!) like '04/01/2013 03:08 PM' that is London time,
	 * returns this time converted to Berlin time.
	 * We use this to convert our server's times (Rackspace hosts in London) to more useful Berlin time.
	 *
	 * @param $dateTimeLondon
	 * @return string
	 */
	public static function convertDateTimeLondonToDateTimeBerlin($dateTimeLondon)
	{
		$dateTime = new \DateTime($dateTimeLondon, new \DateTimeZone('Europe/London') );
		$dateTime->setTimeZone(new \DateTimeZone('Europe/Berlin'));

		return $dateTime->format('Y-m-d H:i:s');
	}

	/**
	 * Rollback-method for down-migration (to revert the results from convertDateTimeLondonToDateTimeBerlin())
	 *
	 * @param $dateTimeBerlin
	 * @return string
	 */
	public static function convertDateTimeBerlinToDateTimeLondon($dateTimeBerlin)
	{
		$dateTime = new \DateTime($dateTimeBerlin, new \DateTimeZone('Europe/Berlin') );
		$dateTime->setTimeZone(new \DateTimeZone('Europe/London'));

		return $dateTime->format('Y-m-d H:i:s');
	}
}
