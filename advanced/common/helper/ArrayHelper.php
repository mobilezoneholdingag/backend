<?php
/**
 * Static class for array utility functions
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace common\helper;


class ArrayHelper {

	/**
	 * Recursive filtering of arrays for properties
	 * @param $data
	 * @param array $properties
	 *
	 * @return array
	 */
	static public function cropProperties(array $data = [], array $properties = [])
	{
		$returnData = [];
		foreach($data as $property => $dataEntry)
		{
			if (array_key_exists($property, $properties)) {
				if (is_array($dataEntry) && is_array($properties[$property])) {
					$returnData[$property] = self::cropProperties($dataEntry, $properties[$property]);
				} elseif (is_string($properties[$property])) {
					$property = $properties[$property];
					$returnData[$property] = $dataEntry;
				}
			} elseif (in_array($property, $properties)) {
				$returnData[$property] = $dataEntry;
			}
		}

		return $returnData;
	}

	/**
	 * Creates a condition array for Yii.
	 *
	 * @param string $column        The column name.
	 * @param array $conditions     The conditions array. ['operator', 'value']
	 * @return array                The complete condition
	 */
	static public function createConditionForColumn($column, array $conditions)
	{
		return [$conditions[0], $column, $conditions[1]];
	}
}
