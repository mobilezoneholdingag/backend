<?php

namespace common\helper;

use common\models\Article;
use Yii;
use yii\helpers\Url;
use yii\web\UrlManager;

class UrlHelper
{
	const DEFAULT_LANGUAGE = 'de-CH';

	public static function urlToDevice(Article $article, $bid = '')
	{
		return Url::to([$article->urlToGroup . '/' . $article->variationUrlPart, 'bid' => $bid], true);
	}

	public static function determineUrlManager($language = null)
	{
		if (isset($language) === false) {
			$language = Yii::$app->language;
		}
		/** @var UrlManager $urlManager */
		$urlManager = clone Yii::$app->urlManagerFrontend;
		if (isset(Yii::$app->params['language_domain_map'][$language])) {
			$urlManager->setHostInfo(Yii::$app->params['language_domain_map'][$language]);
		}

		return $urlManager;
	}

	public static function getFrontendUrl(string $language = null): string
	{

		$language = $language ?? Yii::$app->language;

		if (!self::isValidLanguageCode($language)) {
			$language = self::DEFAULT_LANGUAGE;
		}

		return Yii::$app->params['language_domain_map'][$language];
	}

	private static function isValidLanguageCode(string $language): bool
	{
		return array_key_exists($language, Yii::$app->params['language_domain_map']);
	}
}
