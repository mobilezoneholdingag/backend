<?php

namespace common\helper;


use common\models\Article;
use common\models\ProductCategory;
use common\modules\set\models\Sets;
use common\models\Tariffs;

class SaleableHelper
{
	const DEFAULT_CATEGORY = ProductCategory::SMARTPHONE;

	const ALL_CATEGORIES = [
		ProductCategory::SMARTPHONE => 'Smartphones',
		ProductCategory::TABLET     => 'Tablets'
	];

	/**
	 * @param int       $kind   A product category.
	 *
	 * @return Article[]|Tariffs[]
	 */
	static public function generateList($kind)
	{
		if ($kind == 0)
			$kind = ProductCategory::SMARTPHONE;

		$list       = [];

		$sets = [
			ProductCategory::SMARTPHONE => Sets::ALL_SMARTPHONES,
			ProductCategory::TABLET     => Sets::ALL_TABLETS,
		];

		$products   = Sets::getItemsFor($sets[$kind]);

		foreach ($products as $fullModel)
		{
			if ($kind == ProductCategory::TABLET || $kind == ProductCategory::SMARTPHONE) {
				$name = join(' ', [$fullModel->articleGroup->manufacturerTitle, $fullModel->articleGroup->short_name]);
			} else {
				$name = join(' ', [$fullModel->provider->title, $fullModel->title]);
			}

			$list[] = ['id' => $fullModel->id, 'name' => $name];
		}

		if (count($list) > 0)
		{
			usort($list, function ($a, $b) {
				return strtolower($a['name']) > strtolower($b['name']) ? 1 : -1;
			});
		}

		return $list;
	}

}
