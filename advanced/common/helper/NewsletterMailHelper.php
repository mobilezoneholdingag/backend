<?php

namespace common\helper;

use components\Locale;
use frontend\models\Newsletter;
use Yii;
use yii\log\Logger;

class NewsletterMailHelper
{
	public static function sendOptInMail($email)
	{
		$subscriber = Newsletter::findOne(['email' => $email]);

		if (Locale::in(Locale::SUISSE)) {
			$template = ['html' => 'de-ch/newsletterOptInHtml.php', 'text' => 'de-ch/newsletterOptInText.php'];
		} else {
			$template = ['html' => 'de-de/newsletterOptInHtml.php', 'text' => 'de-de/newsletterOptInText.php'];
		}

		return self::composeMail($template, $subscriber);
	}

	private static function composeMail($template, $subscriber)
	{
		$fromMailAddress = '';

		if (Locale::in(Locale::SUISSE)) {
			$fromMailAddress = Yii::$app->params['mailing.newsletter.optin.senderEmail.ch'];
		} else {
			$fromMailAddress = Yii::$app->params['mailing.newsletter.optin.senderEmail.de'];
		};

		try {
			Yii::$app->mailer->compose($template, ['subscriber' => $subscriber])
				->setFrom([$fromMailAddress => "Newsletter DEINHANDY"])
				->setTo((!YII_ENV_PROD) ? Yii::$app->params['mailing.confirmation.developmentAddress'] : $subscriber->email)
				->setSubject(Yii::$app->params['mailing.newsletter.optin.subject'])
				->send();
			return true;
		} catch (\Swift_SwiftException $e) {
			Yii::getLogger()->log('MailJet API NOT REACHABLE!!', Logger::LEVEL_ERROR, 'newsletter');
			return false;
		}
	}
}