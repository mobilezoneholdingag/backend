<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 **/

namespace common\helper;


class EmailHelper
{
	/**
	 * Makes a unified hash out of an email address.
	 *
	 * @param string    $email
	 *
	 * @return string
	 */
	static public function tokenize($email)
	{
		if (!empty($email)) {
			if (mb_detect_encoding($email) == 'ISO-8859-1') {
				$email = mb_convert_encoding($email, 'UTF-8', 'ISO-8859-1');
			}
		}

		$email = md5(trim(strtolower($email)));

		return $email;
	}

}