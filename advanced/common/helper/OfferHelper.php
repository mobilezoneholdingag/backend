<?php

namespace common\helper;

use common\models\Article;
use common\models\ArticleTariffs;
use common\modules\set\models\Sets;
use common\models\Tariffs;
use yii\i18n\Formatter;

/**
 * Class OfferHelper
 * @package common\helper
 *
 * This class gives u the possibility to access Dynamic Offers from a TWIG.
 * Have a look at dynamic_offers_example how to use.
 * Go into the Model to see what properties u can access.
 */
class OfferHelper
{
	public static function getArticle($articleId)
	{
		$article = Article::findOne(['id' => $articleId]);
		return $article;
	}

	public static function getTariff($tariffId)
	{
		$tariff = Tariffs::findOne(['id' => $tariffId]);
		return $tariff;
	}

	public static function getArticleTariff($articleId, $tariffId)
	{
		$articleTariff = ArticleTariffs::findOne(['article_id' => $articleId, 'tariff_id' => $tariffId]);
		return $articleTariff;
	}


	public static function getPriceString($price)
	{
		$price = str_replace(',', '.', $price);
		$price = \Yii::$app->formatter->asCurrency($price);

		return $price;
	}

	public static function getTariffRecommendations()
	{
		$result = Sets::getItemsFor('tariff_recommendations');

		return $result;
	}
}