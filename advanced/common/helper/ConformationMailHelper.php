<?php

namespace common\helper;

use common\models\Article;
use common\models\Sales;
use common\models\Tariffs;
use components\Locale;
use Swift_SwiftException;
use Yii;
use yii\log\Logger;

/**
 * Class ConformationMailHelper
 * Helper Class to send different conformation mails to Customer.
 *
 * Sends an email (confirmation of order) to the customer.
 * We are using the external mail-sending provider MAILJET.COM for this.
 * IMPORTANT: Even when using the views (confirmationMailHtml.php etc) Yii2 still needs to have html.php and
 * text.php inside /layouts! This does not make much sense, but it's necessary.
 *
 * @param $data object the user's data (= the database row)
 * @return bool whether the mail has been sent successfully or not
 *
 * @package common\helper
 */
class ConformationMailHelper
{

	/**
	 * @param Sales $data
	 * @return bool
	 */
	public static function hardwareWithTariff($data)
	{

		if (Locale::in(Locale::SUISSE)) {
			$template = ['html' => 'de-ch/confirmationMailHtml.php', 'text' => 'de-ch/confirmationMailText.php'];
			$mailData = ['data' => $data, 'article' => Article::findOne($data->article_id), 'tariff' => Tariffs::findOne($data->tariff_id)];
		} else {
			$template = ['html' => 'de-de/confirmationMailHtml.php', 'text' => 'de-de/confirmationMailText.php'];
			$mailData = ['data' => $data, 'article' => Article::findOne($data->article_id), 'tariff' => Tariffs::findOne($data->tariff_id)];
		}

		return self::composeMail($data, $template, $mailData);
	}


	public static function hardwareOnly($data)
	{

		$template = ['html' => 'hardwareOnlyConfirmationMailHtml.php', 'text' => 'hardwareOnlyConfirmationMailText.php'];
		$mailData = ['data' => $data, 'article' => Article::findOne($data->article_id)];

		return self::composeMail($data, $template, $mailData);
	}


	public static function simOnly($data)
	{
		$template = ['html' => 'simOnlyConfirmationMailHtml.php', 'text' => 'simOnlyConfirmationMailText.php'];
		$mailData = ['data' => $data, 'tariff' => Tariffs::findOne($data->tariff_id)];

		return self::composeMail($data, $template, $mailData);
	}

	private static function composeMail($data, $template, $mailData)
	{
		$cancellationPolicyFile = '';
		$fromMailAddress = '';
		$agbs = '';

		if (Locale::in(Locale::SUISSE)) {
			$fromMailAddress = Yii::$app->params['mailing.confirmation.senderEmailBestellung.ch'];
			$agbs = Yii::getAlias('@mailAttachments') . '/Allgemeine-Geschaeftsbedingungen-Mobilezone.pdf';
		} else {
			if ($data->isEamSale()) {
				$cancellationPolicyFile = Yii::getAlias('@mailAttachments') . '/' . 'Widerrufsbelehrung.pdf';
			} else if ($data->isDrillischSale()) {
				$cancellationPolicyFile = Yii::getAlias('@mailAttachments') . '/' . 'Widerrufsbelehrung-Drillisch.pdf';
			} else if ($data->isEinsUndEinsSale()) {
				$cancellationPolicyFile = Yii::getAlias('@mailAttachments') . '/' . 'Widerrufsbelehrung-1und1.pdf';
			}

			$fromMailAddress = Yii::$app->params['mailing.confirmation.senderEmailSupport'];
			$agbs = Yii::getAlias('@mailAttachments') . '/Allgemeine-Geschaeftsbedingungen.pdf';
		};

		try {
			$mailer = Yii::$app->mailer->compose($template, $mailData)
				->setFrom($fromMailAddress)
				->setTo((($data->marketing_partner_id) && !YII_ENV_PROD) ? Yii::$app->params['mailing.confirmation.developmentAddress'] : $data->customer_email)
				->setSubject(Yii::$app->params['mailing.confirmation.subject'])
				->attach($agbs);


			if ($cancellationPolicyFile) {
				$mailer->attach($cancellationPolicyFile);
			}

			$mailer->send();

			return true;
		} catch (\Swift_SwiftException $e) {
			Yii::getLogger()->log('MailJet API NOT REACHABLE!!', Logger::LEVEL_ERROR, 'warenkorb');
			return false;
		}
	}


	public static function sendDebugMail($data, $ourOrderId)
	{
		return Yii::$app->mailer->compose(
			['html' => 'debugMailDrillischApiFailedSale.php', 'text' => 'debugMailDrillischApiFailedSale.php'],
			['data' => $data, 'html' => $data, 'ourOrderId' => $ourOrderId]
		)
			->setFrom(Yii::$app->params['mailing.confirmation.senderEmail'])
			->setTo(Yii::$app->params['api.drillisch.cartFail'])
			->setSubject('DEBUG Mail: cart API problem')
			->send();
	}

	public static function sendConfirmationMail(Sales $data)
	{
		$submitType = $data->submitType;
		if ($submitType == 'hardware-only') {
			$sent = self::hardwareOnly($data);
		} elseif ($submitType == 'sim-only') {
			$sent = self::simOnly($data);
		} else {
			$sent = self::hardwareWithTariff($data);
		}

		return $sent;
	}

	public static function sendCustomerHelpMail($data)
	{
		$template = ['html' => 'de-de/resubmitOrderHtml.php', 'text' => 'de-de/resubmitOrderHtml.php'];
		$mailData = ['data' => $data];

		$fromMailAddress = Yii::$app->params['mailing.resubmitOrder.email'];
		$subject = Yii::$app->params['mailing.resubmitOrder.subject'];

		try {
			$mailer = Yii::$app->mailer->compose($template, $mailData)
				->setFrom($fromMailAddress)
				->setTo($data->customer_email)
				->setSubject($subject);
			$mailer->send();

			return true;
		} catch (Swift_SwiftException $e) {
			Yii::getLogger()->log('MailJet API NOT REACHABLE!!', Logger::LEVEL_ERROR, 'resubmit');
			return false;
		}
	}
}


