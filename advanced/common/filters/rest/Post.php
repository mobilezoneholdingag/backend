<?php

namespace common\filters\rest;

use Yii;
use yii\base\ActionFilter;

/**
 * This post filter is taking the full body JSON-object, and putting its indices into single body request params,
 * in order for Yii to have them available properly.
 *
 * E.g. from a request body:
 *
 * {
 *   "foo": {
 *     "code": "foobar"
 *   },
 *   "blubb": 42
 * }
 *
 * This filter would create two single request parameters, namely 'foo' with an array content, and 'blubb' with
 * an integer content, as expected from the above example.
 *
 * @package common\filters\rest
 */
class Post extends ActionFilter
{
	public function beforeAction($action)
	{
		$request = Yii::$app->getRequest();

		if ($request->getIsPost() === false) {
			return parent::beforeAction($action);
		}

		$data = json_decode($request->getRawBody(), true);

		if (is_array($data)) {
			$request->setBodyParams($data);
		}

		return parent::beforeAction($action);
	}
}