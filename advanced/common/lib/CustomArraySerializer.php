<?php

namespace common\lib;

use yii\data\DataProviderInterface;
use yii\rest\Serializer;

class CustomArraySerializer extends Serializer
{
	public function serializeDataProviderOnDemand(DataProviderInterface $dataProvider)
	{
		return parent::serializeDataProvider($dataProvider);
	}
}