<?php

namespace common\lib\twig;

use Twig_SimpleFilter;
use Yii;

class TranslationExtension extends \Twig_Extension
{
	/**
	 * Returns a list of filters to add to the existing list.
	 *
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new Twig_SimpleFilter('trans', [$this, 'trans']),
		];
	}

	/**
	 * @param string $value
	 * @param string $category
	 * @param array $params
	 * @return string
	 */
	public function trans($value, $category = null, $params = [], $lang = null)
	{
		if ($category === null) {
			$category = Yii::$app->controller->module->id;
		}

		return Yii::t($category, $value, $params, $lang);
	}

	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'i18n';
	}
}
