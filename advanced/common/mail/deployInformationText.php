<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */
?>
Liebe Entwicklerin,
lieber Entwickler,

soeben wurde eine neue Version der Software, an der Du mitgewirkt hast, im Internet verteilt.
Herzlichen Glückwunsch!

<?php if (is_array($list) && count($list) > 0): ?>
Hier eine Liste aller neuen Dinge, die in der Website funktionieren (sollen):

<?php foreach($list as $item => $value): ?>
<?= "* [$item] $value ($baseUrl/browse/$item)".PHP_EOL; ?>
<?php endforeach; ?>

Die hervorragende Qualität Deiner Arbeit hat dazu geführt, dass unsere Kunden unsere Website noch besser,
noch schneller und noch aktueller finden werden als jemals zuvor. Vielen Dank!
<?php else: ?>
Diese neue Version beinhaltet einige Korrekturen und behebt einige Fehler, die in der vorangegangenen Version
aufgetaucht sind.
<?php endif; ?>

DEIN SHOP. DEINE LEISTUNG. FERTIG.


PS: Gleich testbestellen. ;) https://www.deinhandy.de?bid=test