<?php

/**
 * @var $ourOrderId integer Our order id
 * @var $data array|string Error(s) as human-readable text
 */

// affected order
echo 'Problem mit OrderId: ' . $ourOrderId . ' im Environment ' . YII_ENV . '<br>';

// all errors that occurred while trying to submit this order to API
if (is_array($data)) {
	foreach ($data as $info) {
		echo $info . "<br>";
	}
} else if (is_string($data)) {
	echo $data . "<br>";
}

?>