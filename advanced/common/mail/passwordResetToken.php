<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = $url . '/customer-account/customer-account/reset-password?token=' . $user->password_reset_token;
?>

Hello <?= Html::encode($user->email) ?>,<br>

Follow the link below to reset your password: <br>

<?= Html::a(Html::encode($resetLink), $resetLink) ?>
