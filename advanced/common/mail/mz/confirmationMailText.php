*Bestellbestätigung*

Hallo <?= $billingAddress->firstname ?> <?= $billingAddress->lastname ?>,

vielen Dank für Deine Bestellung bei *mobilezone Ag*.

In dieser Mail haben wir für Dich noch einmal alle Informationen zu Deiner Bestellung aufgelistet:

*Smartphone*
<?php foreach ($articles as $article): ?>
<?= $article->manufacturer->title ?> <?= $article->product_name ?>

<?php endforeach; ?>
Einmalige Kosten: CHF <?= number_format($order->price_once, 2, '.', '.') ?>

<?php if($tariff): ?>
*Tariff*
<?= $tariff->provider->title ?> <?= $tariff->title ?>
CHF <?= number_format($order->price_monthly, 2, '.', '.') ?>
Vertragslaufzeit: <?= $tariff->contract_duration ?> Monate
<?php endif; ?>

*Gesamtpreis* (inkl. 8% MwSt.):
CHF <?= number_format($order->price_once, 2, '.', '.') ?>

*Rechnungsadresse*
<?= $billingAddress->firstname ?> <?= $billingAddress->lastname ?>
<?= $billingAddress->street ?> <?= $billingAddress->house_number ?>
<?= $billingAddress->zip ?> <?= $billingAddress->city ?>

*Lieferadresse*
<?= $deliveryAddress->firstname ?> <?= $deliveryAddress->lastname ?>
<?= $deliveryAddress->street ?> <?= $deliveryAddress->house_number ?>
<?= $deliveryAddress->zip ?> <?= $deliveryAddress->city ?>


Bei eventuell bestehenden Rückfragen stehen wir Dir gerne zur Verfügung.

*Wir wünschen Dir viel Spaß mit Deinem neuen Handy!*


Dein Team von mobilezone.

mobilezone ag
Riedthofstrasse 124
8105 Regensdorf
Schweiz

Kontakt
onlineshop@mobilezone.ch
043 388 77 99 (Mo.-Fr. 08:00 – 12:00 / 13:00 – 18:00 Uhr)
www.mobilezone.ch
