<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width"/>
</head>
<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
	<tr>
		<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
			<br/>
			<!-- 600px container (white background) -->
			<table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
				<tr>
					<td colspan="2" class="container-padding content" align="left"
					    style="padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;background-color:#ffffff">
						<div align="center">
							<a href="https://www.mobilezone.ch">
								<img src="<?= $headerImage ?>" alt="mobilezone.ch"/>

							</a>
						</div>
						<div style="height:0;clear: both;">&nbsp;</div>
						<br/>
						<div class="hr" style="height:1px;border-bottom:1px solid #eee;clear: both;">&nbsp;</div>
						<br/>
						<div class="title" style="font-family:Arial, sans-serif;font-size:22px;font-weight:600;color:#ec0c0c">
							Vielen Dank für Deine Bestellung
						</div>
						<br/>
						<div class="body-text"
						     style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
							Hallo <?= $billingAddress->firstname ?> <?= $billingAddress->lastname ?>,<br/>
							in dieser Mail haben wir für Dich noch einmal alle Informationen zu Deiner Bestellung aufgelistet:
							<br/>
							<br/>
							Bestellnummer: <?= $order->id ?>
						</div>
						<br/>
						<!-- #Deine Auswahl -->
						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Deine Auswahl</div>
						</div>
						<table width="264" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row">
							<tr>
								<td class="col" valign="top"
								    style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
									<table style="width: 100%;">
										<?php foreach ($articles as $article): ?>
											<tr>
												<td style="width: 70%;">
													<div class="text"
													     style="height: 45px; padding-top: 15px; display:block; font-weight: bold; font-size: 15px;">
														<?= $article->manufacturer->title ?>
													</div>
													<div class="text" style="width: 100%; display:block; font-size: 15px;">
														<?= $article->product_name ?>
													</div>
												</td>
												<td style="width: 30%; text-align: right; padding: 15px;">
													<!--TODO IMAGE-->
												</td>
											</tr>
										<?php endforeach; ?>
									</table>
								</td>
							</tr>
						</table>
						<?php if ($tariff): ?>
							<table width="264" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row">
								<tr>
									<td class="col" valign="top"
									    style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
										<table style="width: 100%;">
											<tr>
												<td style="width: 70%;">
													<div
														style="height: 45px; padding-top: 15px; display: block; font-weight: bold; font-size: 15px;">
														<?= $tariff->provider->title ?>
													</div>
													<div class="text" style="width: 100%; display:block;  font-size: 15px;">
														<?= $tariff->title ?><br/>
														<?= $tariff->contract_duration ?> Monate
													</div>
												</td>
												<td style="width: 30%; text-align: right; padding: 15px;">
													<!--TODO IMAGE-->
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						<?php endif; ?>
						<!-- #Deine Auswahl -->
						<div class="hr" style="height:0;clear: both;">&nbsp;</div>
						<br/>
						<!-- #Einmalige Kosten -->
						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Kosten</div>
						</div>
						<table style="width: 100%;" cellspacing="0">
							<tr>
								<td class="pricing-text"
								    style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									Monatlich CHF<br/>
								</td>
								<td class="pricing-text"
								    style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									CHF <?= number_format($order->price_monthly, 2, '.', '.') ?>
								</td>
							</tr>
							<tr>
								<td class="pricing-text"
								    style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									Einmalig CHF
								</td>
								<td class="pricing-text"
								    style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									CHF <?= number_format($order->price_once, 2, '.', '.') ?>
								</td>
							</tr>
							<tr>
								<td class="pricing-text"
								    style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; color: #333; background-color: #eee; border: none;">
									Gesamt (inkl. 8% MwSt.)
								</td>
								<td class="pricing-text"
								    style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; color: #333; background-color: #eee; border: none;">
									CHF <?= number_format($order->price_once, 2, '.', '.') ?>
								</td>
							</tr>
						</table>
						<!-- #Einmalige Kosten -->
						<br/>
						<!-- #Rechnungsadresse -->
						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Rechnungsadresse</div>
						</div>
						<table>
							<tr>
								<td class="text" style="padding: 5px;">
									<br/>
									<?= $billingAddress->firstname ?>
									<?= $billingAddress->lastname ?><br/>
									<?= $billingAddress->street ?>
									<?= $billingAddress->house_number ?><br/>
									<?= $billingAddress->zip ?>
									<?= $billingAddress->city ?><br/>
								</td>
							</tr>
						</table>
						<!-- #Rechnungsadresse -->

						<div style="height:0;clear: both;">&nbsp;</div>
						<br/>

						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Lieferadresse</div>
						</div>

						<table>
							<tr>
								<td class="text" style="padding: 5px;">
									<br/>
									<?= $deliveryAddress->firstname ?>
									<?= $deliveryAddress->lastname ?><br/>
									<?= $deliveryAddress->street ?>
									<?= $deliveryAddress->house_number ?><br/>
									<?= $deliveryAddress->zip ?>
									<?= $deliveryAddress->city ?><br/>
								</td>
							</tr>
						</table>

						<div style="height:0;clear: both;">&nbsp;</div>
						<br/>
						<div class="hr" style="height:1px;border-bottom:1px solid #eee;clear: both;">&nbsp;</div>
						<br/><br/>

						<div class="body-text"
						     style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:center;color:#333333; padding-bottom: 30px;">
							Im Anhang dieser E-Mail findest Du des Weiteren unsere Allgemeinen Geschäftsbedingungen.
							<br/><br/>
							Bei eventuell bestehenden Rückfragen stehen wir Dir gern zur Verfügung.
							<br/><br/>
							<span style="color: #ec0c0c; font-weight: bold;">Wir wünschen Dir viel Spass mit Deinem neuen Handy!</span>
						</div>
					</td>
				</tr>
				<!-- Footer For Email change  -->
				<tr style="font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;">
					<td align="left" style="padding-left:24px;padding-right:24px;padding-bottom:24px;">
						<br/><br/>
						<strong>mobilezone ag</strong><br/>
						Riedthofstrasse 124<br/>
						8105 Regensdorf<br/>
						Schweiz<br/><br/>
					</td>
					<td align="right"
					    style="vertical-align: top; padding-top: 32px;padding-left:24px;padding-right:24px;padding-bottom:24px;">
						<strong>Kontakt</strong><br/>
						onlineshop@mobilezone.ch<br/>
						043 388 77 99 (Mo.-Fr. 08:00 – 12:00 / 13:00 – 18:00 Uhr)<br/>
						<a href="https://www.mobilezone.ch" style="color:#aaaaaa">www.mobilezone.ch</a><br/>
					</td>
				</tr>
				<!--/600px container -->
			</table>
		</td>
	</tr>
</table>
<!--/100% background wrapper-->
</body>
</html>
