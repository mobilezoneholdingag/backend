<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- So that mobile will display zoomed in -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- enable media queries for windows phone 8 -->
	<meta name="format-detection" content="telephone=no">
	<!-- disable auto telephone linking in iOS -->
</head>

<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
	<tr>
		<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
			<br>
			<!-- 600px container (white background) -->
			<table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
				<tr>
					<td colspan="2" class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;background-color:#ffffff">
						<div align="center">
							<a href="https://www.deinhandy.de">
								<img src="https://www.deinhandy.de/images/mails/dh_nl_logo.png" alt="deinhandy"/>
							</a>
						</div>
						<br/>
						<div align="center">
							<a href="https://www.financefox.de/customers/signup?locale=de&utm_source=deinhandy&utm_medium=email&utm_campaign=handyversicherung">
								<img src="https://www.deinhandy.de/images/mails/financefox/tt_img.png" alt="FinanceFox"/>
							</a>
						</div>
						<br/>
						<div class="title" style="font-family:Arial, sans-serif;font-size:22px;font-weight:600;color:#777; text-align: left;">
							Deine Gratis-Handyversicherung<br/>
						</div>
						<br/>
						<div class="body-text" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#777">
							Vielen Dank für Deine Bestellung bei <span style="color: #51ae3d">DEIN<span style="color: #000000">HANDY</span></span>.
							<br/>
							<br/>
							Heute bekommst Du von uns Deinen Gutscheincode für eine Gratis-Handyversicherung von Schutzklick.
							<p style="font-weight: bold; font-size: 18px; padding: 15px; color: #4fae3d;">Dein Gutschein-Code: DH2016</p>
						</div>
						<div class="title" style="font-family:Arial, sans-serif;font-size:22px;font-weight:600;color:#777; text-align: left;">
							Wie geht es jetzt weiter?<br/>
						</div>
						<br/>
						<div class="body-text" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#777">
							Ganz einfach - Gib Deinen persönlichen Berechtigungscode für eine
							einjährige Gratis-Handyversicherung auf der Website von FinanceFox ein
							oder lade die App von FinanceFox auf Dein neues Smartphone, um Deine Versicherung zu
							aktivieren.
							<br/>
							<br/>
							Mit FinanceFox alle bestehenden Versicherungen ganz einfach in 2 Minuten optimieren und bis zu 40% sparen.
							<br/>
							<br/>
								<img src="https://www.deinhandy.de/images/mails/financefox/phones_mail.png" alt="FinanceFox"/>
							<br/>
							<br/>
							<?php if ($sim_only) {
								echo "Wir wünschen Dir viel Spaß mit Deinem neuen Tarif und ein sicheres Jahr mit FinanceFox!";
							} else {
								echo "Wir wünschen Dir viel Spaß mit Deinem Smartphone und ein sicheres Jahr mit FinanceFox!";
							} ?>

						</div>
						<br/>
						<table border="0" cellpadding="0" cellspacing="0" style="background-color:#51ae3d; box-shadow: 1px 1px 0px #357b1f; border-radius:3px; width:100%;" align="center">
							<tr>
								<td align="center" valign="middle"
									style="color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding: 12px 20px 12px 20px;">
									<a href="https://www.financefox.de/customers/signup?locale=de&utm_source=deinhandy&utm_medium=email&utm_campaign=handyversicherung" target="_blank" style="color:#FFFFFF; text-decoration:none;">Jetzt Deine Gratis-Versicherung abschließen!</a>
								</td>
							</tr>
						</table>
						<div style="font-size: 8px; color: #AAA; text-align: center; padding: 2px;">
							Falls du noch fragen zum Ablauf hast, kannst du uns gerne unter <a href="mailto:versicherung@deinhandy.de">versicherung@deinhandy.de</a> kontaktieren.
						</div>
						<br/>
						<div align="center" style="width: 100%">
							<img src="https://www.deinhandy.de/images/mails/template/divider.png" width="100%" alt=""/>
						</div>
						<br/>
						<div class="body-text" style="font-family:Arial, sans-serif;font-size:14px;line-height:15px;text-align:center;color:#777">
							<div style="padding: 5px">Vielen Dank für Deinen Einkauf bei <span style="color: #51ae3d">DEIN<span style="color: #000000">HANDY</span></span>.</div>
							<?php if ($sim_only) {
								echo "<div style=\"padding: 5px\">Wir wünschen Dir viel Spaß mit Deinem neuen Tarif</div>";
							} else {
								echo "<div style=\"padding: 5px\">Wir wünschen Dir viel Spaß mit Deinem neuen Handy</div>";
							} ?>
							<div style="padding: 5px">und Deiner Gratis Versicherung von Schuzklick.</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="container-padding footer-text" align="left"
						style="font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
						<br><br>
						<span style="font-weight: bold;font-size: 12px;line-height: 16px;color: #AAA;">DEINHANDY Onlineportal</span><br/>
						<span style="font-size: 12px;line-height: 16px;color: #AAA;">
						Eine Marke der Mister Mobile GmbH  <br/>
						Samerwiesen 6<br/>
						63179 Obertshausen<br/>
						<a href="https://www.deinhandy.de" style="color:#aaaaaa">www.deinhandy.de</a><br>
						<br>
						Sitz der Gesellschaft: Obertshausen<br/>
						Handelregister AG: Offenbach am Main<br/>
						HRB - Nr.: 47873<br/>
						Vertretungsberechtiger: Robert Ermich<br/>
						<br></span>
					</td>
					<td class="container-padding footer-text" align="right"
						style="vertical-align: top; padding-top: 32px; font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
						<strong>Kontakt</strong><br/>
						<span>
							versicherung@deinhandy.de<br/>
						</span>
						<br>
					</td>
				</tr>
			</table>
			<!--/600px container -->
		</td>
	</tr>
</table>
<!--/100% background wrapper-->
</body>
</html>
