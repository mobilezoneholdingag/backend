<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- So that mobile will display zoomed in -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- enable media queries for windows phone 8 -->
	<meta name="format-detection" content="telephone=no">
	<!-- disable auto telephone linking in iOS -->

	<style type="text/css">
		body {
			margin: 0;
			padding: 0;
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
			color: #333;
		}

		table {
			border-spacing: 0;
		}

		table td {
			border-collapse: collapse;
		}

		.ExternalClass {
			width: 100%;
		}

		.ExternalClass,
		.ExternalClass p,
		.ExternalClass span,
		.ExternalClass font,
		.ExternalClass td,
		.ExternalClass div {
			line-height: 100%;
		}

		.ReadMsgBody {
			width: 100%;
			background-color: #ebebeb;
		}

		table {
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		img {
			-ms-interpolation-mode: bicubic;
		}

		@media screen and (max-width: 599px) {
			.force-row,
			.container {
				width: 100% !important;
				max-width: 100% !important;
			}
		}

		@media screen and (max-width: 400px) {
			.container-padding {
				padding-left: 12px !important;
				padding-right: 12px !important;
			}
		}
	</style>
</head>

<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
	<tr>
		<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

			<br>

			<!-- 600px container (white background) -->
			<table border="0" width="600" cellpadding="0" cellspacing="0" class="container"
				   style="width:600px;max-width:600px">
				<tr>
					<td colspan="2" class="container-padding content" align="left"
						style="padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;background-color:#ffffff">
						<table style="width: 100%;">
							<tr>
								<td align="left" style="text-align: center;">
									<a href="https://www.deinhandy.de">
										<img src="https://www.deinhandy.de/images/mails/dh_nl_logo.png" alt="deinhandy"/>
									</a>
								</td>
							</tr>
						</table>
						<br>
						<div>
							<a href="http://de.caseable.com/partner/deinhandy-deinehuelle">
							<img src="https://www.deinhandy.de/images/mails/caseable/tt_img.png" alt="caseable"/>
							</a>
						</div>
						<br>
						<br>
						<div class="title"
							 style="font-family:Arial, sans-serif;font-size:22px;font-weight:600;color:#4fae3d; text-align: center;">
							Herzlichen Glückwunsch!<br/>
							Du hast zu Deinem neuen Handy einen 20 € Gutschein von caseable bekommen.
						</div>
						<br/><br/>

							<span style="font-weight: bold; text-align:left; font-family: Verdana, Arial, sans-serif; font-size: 14px;">
								Was ist jetzt zu tun?
							</span>
						<br/>
						<br/>
						<div class="body-text"
							 style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
							Löse Deinen persönlichen Gutscheincode auf <a href="http://de.caseable.com/partner/deinhandy-deinehuelle" alt="caseable">http://www.caseable.com</a> ein und erhalte 20 € Rabatt auf Deinen Einkauf!
							<br/>
							<p style="font-weight: bold; font-size: 18px; padding: 15px; color: #4fae3d;">
								<?php /* @var $code string coupon code */
								echo $code; ?>
							</p>
							<span style="font-weight: bold;">
								Mindestbestellwert 30 €
								Gültig bis 30.06.2016
							</span>
							<br/><br/>

							<form action="http://de.caseable.com/partner/deinhandy-deinehuelle">
								<input style="background-color: #3ca028;text-decoration: none; color: #fff; width: 100%; border-radius:
							3px;box-shadow: 0 0 0 0; padding: 10px 16px; font-size: 18px; line-height: 1.3333333; cursor: pointer; border: none;"
									   type="submit" value="Jetzt Deinen Gutschein einlösen!">
							</form>

							<div style="font-size: 10px; color: #AAA; text-align: center; line-height: 10px;">Bitte antworte nicht an diese Email-Adresse.<br/>
								Es handelt sich hierbei um einen automatischen Versanddienst, der Deine Nachricht nicht beantworten kann.<br/>
								Falls du noch Fragen zum Gutschein hast, kannst uns gerne unter gutschein@deinhandy.de kontaktieren.
							</div>
							<br/><br/>
							Wir wünschen Dir viel Freude mit Deinem neuen Handy und Deiner Smartphone-Hülle von caseable.
							<br/><br/>
							<span style="color: #4fae3d;">Dein Team von DeinHandy.</span>
							<br>
						</div>
						<br/>
					</td>
				</tr>
				<tr>
					<td class="container-padding footer-text" align="left"
						style="font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
						<br><br>
						<span style="font-weight: bold;font-size: 12px;line-height: 16px;color: #AAA;">DEINHANDY Onlineportal</span><br/>
						<span style="font-size: 12px;line-height: 16px;color: #AAA;">
						Eine Marke der Mister Mobile GmbH  <br/>
						Samerwiesen 6<br/>
						63179 Obertshausen<br/>
						<a href="https://www.deinhandy.de" style="color:#aaaaaa">www.deinhandy.de</a><br>

						<br>
						Sitz der Gesellschaft: Obertshausen<br/>
						Handelregister AG: Offenbach am Main<br/>
						HRB - Nr.: 47873<br/>
						Vertretungsberechtiger: Robert Ermich<br/>
						<br></span>

					</td>
					<td class="container-padding footer-text" align="right"
						style="vertical-align: top; padding-top: 32px; font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
						<strong>Kontakt</strong><br/>
						<span class="ios-footer">
							gutschein@deinhandy.de<br/>
							030 - 223 865 18 (Mo.-Fr. 09:00-19:00 Uhr) <br/>
						</span>
						<br>
					</td>
				</tr>
			</table>
			<!--/600px container -->
		</td>
	</tr>
</table>
<!--/100% background wrapper-->
</body>
</html>
