<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- So that mobile will display zoomed in -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- enable media queries for windows phone 8 -->
	<meta name="format-detection" content="telephone=no">
	<!-- disable auto telephone linking in iOS -->

	<style type="text/css">
		body {
			margin: 0;
			padding: 0;
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
			color: #333;
		}

		table {
			border-spacing: 0;
		}

		table td {
			border-collapse: collapse;
		}

		.ExternalClass {
			width: 100%;
		}

		.ExternalClass,
		.ExternalClass p,
		.ExternalClass span,
		.ExternalClass font,
		.ExternalClass td,
		.ExternalClass div {
			line-height: 100%;
		}

		.ReadMsgBody {
			width: 100%;
			background-color: #ebebeb;
		}

		table {
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		img {
			-ms-interpolation-mode: bicubic;
		}

		.yshortcuts a {
			border-bottom: none !important;
		}

		@media screen and (max-width: 599px) {
			.force-row,
			.container {
				width: 100% !important;
				max-width: 100% !important;
			}
		}

		@media screen and (max-width: 400px) {
			.container-padding {
				padding-left: 12px !important;
				padding-right: 12px !important;
			}
		}

		.ios-footer a {
			color: #aaaaaa !important;
			text-decoration: underline;
		}

		.pricing-table {
			width: 100%;
			border-radius: 5px;
			font-family: Arial, sans-serif;
			font-size: 14px;
			line-height: 20px;
		}

		.pricing-table .cell-70-percent-width {
			width: 70%;
			border-left: 1px solid #ddd;

		}

		.pricing-table .cell-30-percent-width {
			width: 30%;
			text-align: right;
			border-right: 1px solid #ddd;
		}

		.pricing-table .cell-50-percent-width {
			width: 50%;
			border-left: 1px solid #ddd;
			border-right: 1px solid #ddd;
		}

		.pricing-table .border-bottom {
			border-bottom: 1px solid #ddd;
		}

		.pricing-text {
			font-family:Arial, sans-serif;
			padding: 12px 12px;
			font-size: 14px;
			font-weight: bold;
			border-bottom: 1px solid #ededed;
		}

		.pricing-table .header-cell-100-percent-width,
		.pricing-table .header-cell-0-percent-width {
			background-color: #333;
			color: #fff;
		}

		.delivery-center {
			font-family:Arial, sans-serif;
			padding: 6px 0;
			font-size: 14px;
			text-align: center;
		}

		.text {
			font-family:Arial, sans-serif;
			font-size:14px;
			line-height:20px;
			text-align:left;
			color:#333333;
		}

		.link-green {
			color: #4fae3d;
			text-decoration: none;
		}
		.text-bold {
			font-weight: bold;
		}
		.padding-left-10px {
			padding-left: 10px;
		}
		a {
			color:#4fae3d;
			text-decoration: none;
		}
	</style>
</head>

<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
	<tr>
		<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

			<br>

			<!-- 600px container (white background) -->
			<table border="0" width="600" cellpadding="0" cellspacing="0" class="container"
				   style="width:600px;max-width:600px">
				<tr>
					<td class="container-padding header" align="left">
						<table width="100%" style="width: 100%">
							<tr>
								<td align="left" style="text-align: left; width: 50%; padding-bottom: 10px;">
									<div class="headerText" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;">Server "Ministry"</div>
								</td>
								<td align="right" style="text-align:right; width: 50%; padding-bottom: 10px;">
									<img src="https://www.deinhandy.de/images/mails/dh_nl_trust.png" alt="" style="margin-right: 10px;" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="container-padding content" align="left"
						style="padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;background-color:#ffffff">
						<table style="width: 100%;">
							<tr>
								<td width="100%" colspan="2" align="center" style="text-align: center; padding-top: 20px; padding-bottom: 20px;">
									<a href="https://www.deinhandy.de?bid=test">
										<img src="https://www.deinhandy.de/images/mails/dh_nl_logo.png" alt="" />
									</a> &ndash;
									<span style="font-family: Arial, sans-serif; color: darkgreen; font-style: italic; font-size: 0.8em;">&nbsp; (Klicken für Testbestellung)</span>
								</td>
							</tr>
						</table>
						<br>
						<br>
						<div class="body-text" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
							Liebe Entwicklerin,<br />
							lieber Entwickler,<br />
							<br />
							soeben wurde eine neue Version der Software, an der Du mitgewirkt hast, im Internet verteilt. Herzlichen Glückwunsch!<br />
							<br />
							<?php if (is_array($list) && count($list) > 0): ?>
							Hier eine Liste aller neuen Dinge, die in der Website funktionieren (sollen):<br />
							<br />
							<ul>
							<?php foreach($list as $item => $value): ?>
								<?= "<li><a href='$baseUrl/browse/$item'>$item &mdash; $value</a></li>" ?>
							<?php endforeach; ?>
							</ul>
							<br />
							Die hervorragende Qualität Deiner Arbeit hat dazu geführt, dass unsere Kunden unsere Website noch
							besser, noch schneller und noch aktueller finden werden als jemals zuvor. Vielen Dank!
							<?php else: ?>
							Diese neue Version beinhaltet einige Korrekturen und behebt einige Fehler, die in der vorangegangenen Version
							aufgetaucht sind.<br />
							<?php endif; ?>
						</div>

						<div class="body-text" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:center;color:#333333">
							<br />
							<a href="https://www.deinhandy.de?bid=test"><span style="padding: 8px; background-color: green; color: white;">Jetzt Testbestellung durchführen.</span></a>
						</div>

						<div class="body-text" style="font-family:Arial, sans-serif;font-size:18px;line-height:20px;text-align:center;color:#333333; padding-top: 50px; padding-bottom: 50px; font-weight: bold;">
							Dein Shop. Deine Leistung. Fertig.
							<br/><br/><br/>
							<img src="https://www.deinhandy.de/images/mails/schufa/trust_footer.png" alt="" style="width: 333px; height: 44px;" width="333" height="44" />
						</div>


					</td>
				</tr>
				<tr>
					<td class="container-padding footer-text" align="left"
						style="font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
						<br><br>
						<span style="font-weight: bold;font-size: 12px;line-height: 16px;color: #AAA;">DEINHANDY Onlineportal</span><br/>
						<span style="font-size: 12px;line-height: 16px;color: #AAA;">
						Eine Marke der Mister Mobile GmbH  <br/>
						Samerwiesen 6<br/>
						63179 Obertshausen<br/>
						<a href="https://www.deinhandy.de" style="color:#aaaaaa">www.deinhandy.de</a><br>

						<br>
						Sitz der Gesellschaft: Obertshausen<br/>
						Handelregister AG: Offenbach am Main<br/>
						HRB - Nr.: 47873<br/>
						Vertretungsberechtiger: Robert Ermich<br/>
						<br></span>

						<strong>Kontakt:</strong><br/>
						<span class="ios-footer">
							info@deinhandy.de<br/>
							030 - 223 865 18 (Mo.-Fr. 09:00-19:00 Uhr) <br/>
						</span>
						<br>
					</td>
				</tr>
			</table>
			<!--/600px container -->
		</td>
	</tr>
</table>
<!--/100% background wrapper-->

</body>
</html>

