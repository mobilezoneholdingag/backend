Beratung: 030 - 223 865 18 (Mo. - Fr. 09:00 - 19:00 Uhr)

Online-Version anzeigen
Dein Handy.
Trusted Shops Logo
eKomi - The Feedback Company
TÜV Siegel

Da ist was schief gelaufen

Lieber <?= $data->customer_prename ?>,

leider gab es im Zeitraum zwischen dem 11. Mai und 25. Mai immer wieder technische Störungen in der Schnittstelle der Datenprüfung bei <?= $data->tariff->getProviderTitle() ?>. Daher kann es sein, dass Deine letzte Bestellung eventuell voreilig abgelehnt wurde. Wir sind für Dich der Sache auf den Grund gegangen und haben mit unserer IT das Problem lösen können. Nach Behebung des technischen Fehlers wurden die Daten erneut übermittelt und zur nun korrigierten Prüfung gegeben.

Deine neue Bestell-ID lautet: <?= $data->order_id ?>

Wir bitten in Namen von <?= $data->tariff->getProviderTitle() ?> und DEINHANDY. um Entschuldigung für die Unannehmlichkeiten und den zeitlichen Verzug. Solltest Du von Deiner Bestellung zurücktreten wollen, dann antworte uns ganz einfach auf diese E-Mail. Dein 14-tägiges Widerrufsrecht ist von all dem unberührt und beginnt erst ab dem Zeitpunkt, ab dem Du Dein Handy und Deine SIM-Karte vom Postboten überreicht bekommst.

Für Fragen stehen wir Dir jederzeit per E-Mail oder per privater Nachricht bei Facebook zur Verfügung. Bitte habe Verständnis dafür, dass die Telefonhotline gegebenenfalls etwas überlastet ist, da wir mit einem erhöhten Aufkommen an Kundenanfragen rechnen.

Beste Grüße, Dein Team von DEINHANDY.

https://www.facebook.com/deinhandy
https://www.instagram.com/deinhandyde/
https://twitter.com/deinHandyDE
https://plus.google.com/+DeinHandyDeinTarifGanzEinfach/videos
https://www.youtube.com/channel/UChwLFW_7f3q2aKtmw_hvqAg
https://www.xing.com/companies/deinhandy.de
https://blog.deinhandy.de/

DEINHANDY Onlineportal
Eine Marke der Mister Mobile GmbH
Samerwiesen 6
63179 Obertshausen

Sitz der Gesellschaft: Obertshausen
Handelregister AG: Offenbach am Main
HRB - Nr.: 47873
Vertretungsberechtigter: Robert Ermich

mail-icon
kundenhife@deinhandy.de

phone-icon
030 - 223 865 18

link-icon
www.deinhandy.de