<table style="border-collapse: separate;border-spacing: 0;width:
		600px;margin: 0;font-family: Arial, sans-serif;font-size:
		13px;color: #616161" border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td style="text-align:center;padding:20px 0;;border:solid
			  #e4e4e4;border-width: 1px;border:1px solid
			  #e4e4e4;border-width: 1px">
			<a href="https://www.deinhandy.de">
				<img moz-do-not-send="true" src="http://deinhandy.de/frontend/template_deinhandy/images/logo_header.png">
			</a>
		</td>
	</tr>
	</tbody>
</table>
<table style="border-collapse: separate;border-spacing: 0;width:
		600px;margin: 0;font-family: Arial, sans-serif;font-size:
		13px;color: #616161" border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td style="padding:30px;border:solid #e4e4e4;border-width: 0
			  1px 1px 1px"> <span style="font-size: 20px;font-weight:
				bold">Newsletter-Anmeldung</span> <br>
			<br>
			<br>
				<span style="font-size: 14px">Hallo,</span>
			<br>
			<br>
			vielen Dank für Dein Interesse am deinHandy-Newsletter. Um die Anmeldung abzuschliessen und
			Missbrauch Deiner Email-Adresse (<span style="font-weight: bold;"><?php echo $subscriber->email; ?></span>)
			vorzubeugen ist es notwendig deine Eintragung mit einem Klick auf diesen Link zu bestätigen:
			<br>
			<br>
			<span style="font-weight: bold;">
				<a href="https://www.deinhandy.de/site/newsletter-confirm?id=<?php echo urlencode($subscriber->id); ?>&email=<?php echo urlencode($subscriber->email); ?>">
					https://www.deinhandy.de/site/newsletter-confirm?id=<?php echo urlencode($subscriber->id); ?>&email=<?php echo urlencode($subscriber->email); ?>
				</a>
			</span>
		</td>
	</tr>
	</tbody>
</table>
<table style="border-collapse: separate;border-spacing: 0;width:
		600px;margin: 0;font-family: Arial, sans-serif;;font-size:
		13px;color: #616161" border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td style="padding:30px;border:solid #e4e4e4;border-width:
			  1px;border:1px solid #e4e4e4;border-width: 0 1px 1px 1px">Mit
			freundlichen Grüßen<br>
			Dein Team von deinHandy<br><br>

			deinHandy.de Onlineportal<br>
			Eine Marke der Mister Mobile GmbH  <br>
			Samerwiesen 6<br>
			63179 Obertshausen<br>
			<br>
			<b>Kontakt</b><br>
			Email: <a class="moz-txt-link-abbreviated" href="mailto:info@deinhandy.de">info@deinhandy.de</a><br>
			Tel. 06104 - 4057 182<br>
			Fax. 06104-4057 78<br>
			Sie erreichen uns telefonisch von Montag - Freitag von
			9.00 - 19.00 Uhr<br>
			<br>
			Sitz der Gesellschaft: Obertshausen<br>
			Handelsregister Ag. Offenbach/Main <br>
			HRB - Nr.: 47873 <br>
			Vertretungsberechtigter Geschäftsführer: Robert Ermich
		</td>
	</tr>
	<tr>
		<td style="padding:30px 0 30px 0;border:solid
			  #e4e4e4;border-width: 1px;border:1px solid
			  #e4e4e4;border-width: 0 1px 1px 1px">
			<a moz-do-not-send="true" href="http://de.caseable.com/partner/deinhandy-deinehuelle">
				<img moz-do-not-send="true" src="http://deinhandy.de/frontend/template_deinhandy/images/content/bestellprozess/caseable_banner_mail.png">
			</a>
		</td>
	</tr>
	<tr>
		<td style="padding:30px 0 30px 0;border:solid
			  #e4e4e4;border-width: 1px;border:1px solid
			  #e4e4e4;border-width: 0 1px 1px 1px">
			<a moz-do-not-send="true" href="https://www.aklamio.com/recommend?uid=0b816e31599cf86c46addd115226b4e5&amp;utm_source=cm&amp;utm_medium=conmail&amp;utm_campaign=deinhandy_de">
				<img moz-do-not-send="true" src="http://deinhandy.de/frontend/template_deinhandy/images/content/bestellprozess/aklamio_banner_mail.png">
			</a>
		</td>
	</tr>
	</tbody>
</table>
