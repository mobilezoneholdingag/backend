<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- So that mobile will display zoomed in -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- enable media queries for windows phone 8 -->
	<meta name="format-detection" content="telephone=no">
	<!-- disable auto telephone linking in iOS -->

	<style type="text/css">
		body {
			margin: 0;
			padding: 0;
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
			color: #333;
		}

		table {
			border-spacing: 0;
		}

		table td {
			border-collapse: collapse;
		}

		.ExternalClass {
			width: 100%;
		}

		.ExternalClass,
		.ExternalClass p,
		.ExternalClass span,
		.ExternalClass font,
		.ExternalClass td,
		.ExternalClass div {
			line-height: 100%;
		}

		.ReadMsgBody {
			width: 100%;
			background-color: #ebebeb;
		}

		table {
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		img {
			-ms-interpolation-mode: bicubic;
		}

		.yshortcuts a {
			border-bottom: none !important;
		}

		@media screen and (max-width: 599px) {
			.force-row,
			.container {
				width: 100% !important;
				max-width: 100% !important;
			}
		}

		@media screen and (max-width: 400px) {
			.container-padding {
				padding-left: 12px !important;
				padding-right: 12px !important;
			}
		}

		.ios-footer a {
			color: #aaaaaa !important;
			text-decoration: underline;
		}

		.pricing-table {
			width: 100%;
			border-radius: 5px;
			font-family: Arial, sans-serif;
			font-size: 14px;
			line-height: 20px;
		}

		.pricing-table .cell-70-percent-width {
			width: 70%;
			border-left: 1px solid #ddd;

		}

		.pricing-table .cell-30-percent-width {
			width: 30%;
			text-align: right;
			border-right: 1px solid #ddd;
		}

		.pricing-table .cell-50-percent-width {
			width: 50%;
			border-left: 1px solid #ddd;
			border-right: 1px solid #ddd;
		}

		.pricing-table .border-bottom {
			border-bottom: 1px solid #ddd;
		}

		.pricing-text {
			font-family: Arial, sans-serif;
			padding: 12px 12px;
			font-size: 14px;
			font-weight: bold;
			border-bottom: 1px solid #ededed;
		}

		.pricing-table .header-cell-100-percent-width,
		.pricing-table .header-cell-0-percent-width {
			background-color: #333;
			color: #fff;
		}

		.delivery-center {
			font-family: Arial, sans-serif;
			padding: 6px 0;
			font-size: 14px;
			text-align: center;
		}

		.text {
			font-family: Arial, sans-serif;
			font-size: 14px;
			line-height: 20px;
			text-align: left;
			color: #333333;
		}

		.link-green {
			color: #4fae3d;
			text-decoration: none;
		}

		.text-bold {
			font-weight: bold;
		}

		.padding-left-10px {
			padding-left: 10px;
		}
	</style>
</head>

<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
	<tr>
		<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

			<br>

			<!-- 600px container (white background) -->
			<table border="0" width="600" cellpadding="0" cellspacing="0" class="container"
				   style="width:600px;max-width:600px">
				<tr>
					<td class="container-padding header" align="left"
						style="font-family:Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#DF4726;padding-left:0;padding-right:0">
						<img src="https://www.deinhandy.de/images/mails/dh_nl_tv.png" alt=""/>
					</td>
				</tr>
				<tr>
					<td class="container-padding content" align="left"
						style="padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;background-color:#ffffff">
						<table style="width: 100%;">
							<tr>
								<td align="left" style="text-align: left; padding-right: 20px;">
									<a href="https://www.deinhandy.de">
										<img src="https://www.deinhandy.de/images/mails/dh_nl_logo.png" alt=""/>
									</a>
								</td>
								<td align="right" style="text-align: right;">
									<img src="https://www.deinhandy.de/images/mails/dh_nl_trust.png" alt=""/>
								</td>
							</tr>
						</table>
						<br>

						<div class="title"
							 style="font-family:Arial, sans-serif;font-size:22px;font-weight:600;color:#4fae3d">
							Vielen Dank für Deine Bestellung
						</div>
						<br>

						<div class="body-text"
							 style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
							Hallo <?= $data->customer_prename; ?> <?= $data->customer_surname; ?>,
							<?php if ($data->fulfillment_partner_id == \common\models\FulfillmentPartners::EINSUNDEINS_ID): ?>
								in Kürze erhälst Du Deine Bestellbestätigung von 1&1 mit Deiner Kundennummer und allen weiteren Infos.
								Falls Du noch Fragen zu Deiner Bestellung hast, kannst Du ganz einfach eine Mail an 1und1@deinhandy.de schreiben.
							<?php else: ?>
								in dieser Mail haben wir für Dich noch einmal alle Informationen zu Deiner Bestellung mit der
								<span style="color: #4fae3d;">Bestellnummer <?= $data->order_id ?></span> aufgelistet:
							<?php endif; ?>
							<br><br>
						</div>

						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Deine Auswahl</div>
						</div>

						<!--[if mso]>
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="50%" valign="top"><![endif]-->

						<table width="264" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row">
							<tr>
								<td class="col" valign="top"
									style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">

									<table style="width: 100%;">
										<tr>
											<td style="width: 70%;">
												<div style="height: 45px; padding-top: 15px;">
													<img
														src="<?= $article->manufacturer->image ?>"
														height="35" style="height: 35px;"
														alt="<?= $article->manufacturerTitle; ?>"/>
												</div>
												<div class="text"
													 style="width: 100%; display:block; font-weight: bold; font-size: 15px;">
													<?= $article->manufacturer->title ?> <?= $article->shortName ?>
												</div>
												<div
													style="font-weight: normal; font-size: 14px;"><?= $article->memory ?>
													GB, <?= $article->color_label ?></div>
											</td>
											<td style="width: 30%; text-align: right; padding: 15px;">
												<img src="<?= $article->image->getUrl('small', true); ?>"
													 alt="" style="height: 100px; width: 50px;"/>
											</td>
										</tr>
									</table>

								</td>
							</tr>
						</table>

						<!--[if mso]></td>
						<td width="50%" valign="top"><![endif]-->

						<table width="264" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row">
							<tr>
								<td class="col" valign="top"
									style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
									<table style="width: 100%;">
										<tr>
											<td style="width: 70%;">
												<div style="height: 45px; padding-top: 15px;">
													<img
														src="https://www.deinhandy.de/<?= $tariff->provider->image; ?>"
														height="35" style="height: 35px;"
														alt="<?= $tariff->provider->title; ?>"/>
												</div>
												<div class="text"
													 style="width: 100%; display:block; font-weight: bold; font-size: 15px;">
													<?= $tariff->provider->title ?> <?= $tariff->title ?>
												</div>
												<div style="font-weight: normal; font-size: 14px;">
													Vertragslaufzeit: <?= $tariff->contract_duration ?> Monate
												</div>
											</td>
											<td style="width: 30%; text-align: right; padding: 15px;">
												<img src="https://www.deinhandy.de/images/mails/dh_nl_sim.png" alt=""
													 style="height: 100px; width: 50px;"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

						<!--[if mso]></td></tr></table><![endif]-->

						<!--/ end example -->

						<div class="hr" style="height:0;clear: both;">&nbsp;</div>
						<br>

						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Einmalige Kosten</div>
						</div>

						<table style="width: 100%;">
							<tr>
								<td class="pricing-text"
									style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									<?= $article->manufacturer->title ?> <?= $article->product_name ?>
									<?php if ($data->coupon_id) { ?>
										(inkl. -<?= number_format(\common\models\Coupon::findOne(['id' => $data->coupon_id])->getDiscountDevicePrice(), 2, ',', '.') ?> € Gutschein)
									<?php } ?>
								</td>
								<td class="pricing-text"
									style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									<?php if (isset($data->tariff_device_final_price) && $data->tariff_device_final_price > 0) { ?>
										<?= number_format($data->tariff_device_final_price, 2, ',', '.') ?> €
									<?php } else if (isset($data->tariff_device_final_price) && $data->tariff_device_final_price == 0 && $data->coupon_id) { ?>
										<?= number_format($data->tariff_device_final_price, 2, ',', '.') ?> €
									<?php } else if ($data->fulfillment_partner_id == \common\models\FulfillmentPartners::EINSUNDEINS_ID) { ?>
										0,00 €
									<?php } else { ?>
										1,00 €<br/>
										- 1,00 € Rabatt<br/>
										= 0,00 €
									<?php } ?>
								</td>
							</tr>

							<tr>
								<td class="pricing-text"
									style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									Versandkosten
								</td>
								<td class="pricing-text"
									style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									<?= number_format(0, 2, ',', '.') ?> €
								</td>
							</tr>
							<tr>
								<td class="pricing-text"
									style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333; background-color: #eee;">
									Gesamt (inkl. 19% MwSt.)
								</td>
								<td class="pricing-text"
									style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333; background-color: #eee;">
									<?= number_format($data->total_once, 2, ',', '.') ?> €
								</td>
							</tr>
						</table>

						<div class="hr" style="height:0;clear: both;">&nbsp;</div>
						<br><br>

						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Monatliche Kosten</div>
						</div>

						<table style="width: 100%;">
							<tr>
								<td class="pricing-text"
									style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									<?= $tariff->provider->title ?> <?= $tariff->title ?>
								</td>
								<td class="pricing-text"
									style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									<?php if ($data->tariff_monthly_special_discount) { ?>
										<div
											style="color: #C0C0C0;"><?= number_format($data->tariff_monthly_price, 2, ',', '.') ?>
											€
										</div>
										<div style="font-size: 14px;font-weight: normal;">
											<?= number_format($data->tariff_monthly_special_discount, 2, ',', '.') ?> €
											Rabatt<br/><br/>
										</div>
										<div><?= number_format($data->tariff_monthly_price_after_special_discount, 2, ',', '.') ?>
											€
										</div>
									<?php } else { ?>
										<div><?= number_format($data->tariff_monthly_price, 2, ',', '.') ?> €</div>
									<?php } ?>
								</td>
							</tr>
						</table>

						<br/>

						<div class="hr" style="height:0;clear: both;">&nbsp;</div>
						<br>

						<!-- only show this block if device is > 0 € -->
						<?php if (is_numeric($data->payment_type) && $data->tariff_device_final_price > 0) { ?>

							<div
								style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
								<div style="padding: 8px 12px;">Zahlung</div>
							</div>

							<table>
								<tr>
									<td class="text">
										<br>
										Gewählte Zahlungskondition:
									<span style="font-weight: bold;">
										<?php
										if ($data->payment_type == '0') {
											echo 'Vorkasse';
										} else if ($data->payment_type == '1') {
											echo 'Nachnahme';
										} else if ($data->payment_type == '2') {
											echo 'Sofortüberweisung';
										} else if ($data->payment_type == '3') {
											echo 'PayPal';
										} else if ($data->payment_type == '4') {
											echo 'Bankeinzug';
										}
										?>
									</span>
										<br/><br/>
									</td>
								</tr>
								<tr>
									<td class="text">
										<?php if ($data->payment_type == '0') { ?>
											<div class="text"
												 style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;">
												Bitte überweise den Gesamtbetrag von <span
													class="text-bold"><?= number_format($data->tariff_device_final_price, 2, ',', '.') ?>
													€</span> auf unser hier aufgeführtes Bankkonto:
											</div>
											<br/><br/>
											<table>
												<tbody>
												<tr>
													<td class="text">Kontoinhaber:</td>
													<td class="text text-bold padding-left-10px">einsAmobile GmbH</td>
												</tr>
												<tr>
													<td class="text">Kreditinstitut:</td>
													<td class="text text-bold padding-left-10px">Sparkasse
														Langen-Seligenstadt
													</td>
												</tr>
												<tr>
													<td class="text">Bankleitzahl:</td>
													<td class="text text-bold padding-left-10px">506 521 24</td>
												</tr>
												<tr>
													<td class="text">Kontonummer:</td>
													<td class="text text-bold padding-left-10px">112 55 66</td>
												</tr>
												<tr>
													<td><br></td>
													<td><br></td>
												</tr>
												<tr>
													<td class="text">IBAN:</td>
													<td class="text text-bold padding-left-10px">DE08 5065 2124 0001
														1255 66
													</td>
												</tr>
												<tr>
													<td class="text">BIC:</td>
													<td class="text text-bold padding-left-10px">HELADEF1SLS</td>
												</tr>
												<tr>
													<td><br></td>
													<td><br></td>
												</tr>
												<tr>
													<td class="text">Verwendungszweck:</td>
													<td class="text text-bold padding-left-10px">
														40003 <?= $data->order_id ?>
														/ <?= $data->customer_prename ?> <?= $data->customer_surname ?></td>
												</tr>
												</tbody>
											</table>
										<?php } else if ($data->payment_type == '1') { ?>
											Bitte halte den Gesamtbetrag von <?= number_format($data->tariff_device_final_price, 2, ',', '.') ?> € plus 6,90 € Nachnahmegebühr für den Zusteller bereit.
										<?php } else if ($data->payment_type == '2') { ?>
											Bitte zahle per Sofortüberweisung über diesen Link:
											<a href="<?= $data->payment_link ?>">Hier klicken um Zahlung
												einzuleiten</a>.
										<?php } else if ($data->payment_type == '3') { ?>
											Bitte zahle per PayPal über diesen Link:
											<a href="<?= $data->payment_link ?>">Hier klicken um Zahlung
												einzuleiten</a>.
										<?php } else if ($data->payment_type == '4') { ?>
											Der Betrag wird automatisch von Deinem Konto abgebucht.
										<?php } ?>
									</td>
								</tr>
							</table>
						<?php } ?>

						<div style="height:0;clear: both;">&nbsp;</div>
						<br>

						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Rechnungsadresse</div>
						</div>

						<table>
							<tr>
								<td class="text" style="padding: 5px;">
									<br>
									<?= $data->customer_prename; ?> <?= $data->customer_surname; ?><br/>
									<?= $data->customer_street; ?> <?= $data->customer_streetnumber; ?><br/>
									<?= $data->customer_zip; ?> <?= $data->customer_city; ?><br/>
								</td>
							</tr>
						</table>

						<div style="height:0;clear: both;">&nbsp;</div>
						<br>

						<div
							style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Lieferadresse</div>
						</div>

						<table>
							<tr>
								<td class="text" style="padding: 5px;">
									<br>
									<?php if ($data->delivery_address) { ?>
										<?php if (!empty($data->delivery_company)) { ?>
											<?= $data->delivery_company; ?><br/>
										<?php } ?>
										<?= $data->customer_prename; ?> <?= $data->customer_surname; ?><br/>
										<?= $data->delivery_street; ?> <?= $data->delivery_streetnumber; ?><br/>
										<?= $data->delivery_zipcode; ?> <?= $data->delivery_city; ?><br/>
									<?php } else { ?>
										<?= $data->customer_prename; ?> <?= $data->customer_surname; ?><br/>
										<?= $data->customer_street; ?> <?= $data->customer_streetnumber; ?><br/>
										<?= $data->customer_zip; ?> <?= $data->customer_city; ?><br/>
									<?php } ?>
								</td>
							</tr>
						</table>

						<div style="height:0;clear: both;">&nbsp;</div>
						<br>

						<div class="hr" style="height:1px;border-bottom:1px solid #eee;clear: both;">&nbsp;</div>

						<div style="font-family:Arial, sans-serif;font-size:16px;text-align:center;font-weight: bold;">
							<br><br><br>
							Der Versandablauf bei DEINHANDY
							<br><br><br>
						</div>

						<div>
							<table style="width: 100%;">
								<tr>
									<td class="delivery-center"
										style="width: 100%;font-family:Arial, sans-serif;padding: 6px 0;font-size: 14px;text-align: center;">
										<img src="https://www.deinhandy.de/images/mails/dh_nl_bestellung-1.png" alt="">
									</td>
								</tr>
								<tr>
									<td class="delivery-center"
										style="width: 100%;font-family:Arial, sans-serif;padding: 6px 0;font-size: 14px;text-align: center; color: #333;">
										Überprüfung Deiner Daten
									</td>
								</tr>
								<tr>
									<td class="delivery-center"
										style="width: 100%;font-family:Arial, sans-serif;padding: 6px 0;font-size: 14px;text-align: center;">
										<img src="https://www.deinhandy.de/images/mails/dh_nl_bestellung-2.png" alt="">
									</td>
								</tr>
								<tr>
									<?php if ($data->fulfillment_partner_id == \common\models\FulfillmentPartners::EINSUNDEINS_ID): ?>
										<td class="delivery-center"
											style="width: 100%;font-family:Arial, sans-serif;padding: 6px 0;font-size: 14px;text-align: center; color: #333;">
											Auslieferung mit DHL
										</td>
									<?php else: ?>
										<td class="delivery-center"
											style="width: 100%;font-family:Arial, sans-serif;padding: 6px 0;font-size: 14px;text-align: center; color: #333;">
											Auslieferung mit DHL Express
										</td>
									<?php endif; ?>
								</tr>
								<tr>
									<td class="delivery-center"
										style="width: 100%;font-family:Arial, sans-serif;padding: 6px 0;font-size: 14px;text-align: center;">
										<img src="https://www.deinhandy.de/images/mails/dh_nl_bestellung-3.png" alt="">
									</td>
								</tr>
								<tr>
									<?php if ($data->fulfillment_partner_id == \common\models\FulfillmentPartners::EINSUNDEINS_ID): ?>
										<td class="delivery-center"
											style="width: 100%;font-family:Arial, sans-serif;padding: 6px 0;font-size: 14px;text-align: center; color: #333;">
											Ohne viel Aufwand hälst Du Dein neues Handy bald in Deiner Hand
										</td>
									<?php else: ?>
										<td class="delivery-center"
											style="width: 100%;font-family:Arial, sans-serif;padding: 6px 0;font-size: 14px;text-align: center; color: #333;">
											Auspacken und freuen!
										</td>
									<?php endif; ?>
								</tr>
							</table>
						</div>
						<br>

						<div class="hr" style="height:1px;border-bottom:1px solid #eee;clear: both;">&nbsp;</div>

						<br><br>

						<?php if (!empty($data->order_id)) { ?>

							<div class="body-text"
								 style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:center;color:#333333">
								Den aktuellen Status Deiner Bestellung kannst Du hier unter Angabe Deiner Bestellnummer
								<?= $data->order_id ?> abfragen:
								<a class="link-green" href="https://www.deinhandy.de/bestellstatus">https://www.deinhandy.de/bestellstatus</a>
							</div>

						<?php } ?>

						<br><br>

						<div class="hr" style="height:1px;border-bottom:1px solid #eee;clear: both;">&nbsp;</div>

						<br><br>

						<div class="body-text"
							 style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:center;color:#333333; padding-bottom: 30px;">
							Im Anhang dieser E-Mail findest Du des Weiteren unsere Allgemeinen Geschäftsbedingungen und
							die Widerrufsbelehrung.
							<br/><br/>
							Bei eventuell bestehenden Rückfragen stehen wir Dir gern zur Verfügung.
							<?php if ($data->fulfillment_partner_id == \common\models\FulfillmentPartners::EINSUNDEINS_ID): ?>
								<br/>
								<span style="font-weight: bold">Schick uns einfach Deine Frage per Mail an die 1und1@deinhandy.de.<br/>
							Wir kümmern uns schnellstmöglich um Dein Anliegen.</span>
							<?php endif; ?>
							<br/><br/>
							<span style="color: #4fae3d; font-weight: bold;">Wir wünschen Dir viel Spass mit Deinem neuen Handy!</span>
						</div>
					</td>
				</tr>

				<?php if ($data->insurance == 0) { ?>
					<tr>
						<td style="border:0;border-bottom-right-radius: 10px; border-bottom-left-radius: 10px; padding: 30px; background-color: #f7f7f7; text-align: center; color: #222; text-decoration: none;">
							<div style="font-size: 16px;font-weight:bold;margin-bottom: 5px; color: #222;">SICHER IST SICHER</div>
							<a href="https://www.schutzklick.de/handyversicherung?partner=110785&utm_source=ipvt&utm_medium=aff" target="_blank">
								<img src="https://www.deinhandy.de/images/mails/schutzklick_logo.png" style="margin-bottom: 10px;" alt="Schutzklick Logo">
							</a>
							<div style="font-size: 13px; color: #222; text-decoration: none;">
								Mit der Handyversicherung, von unserem Versicherungspartner Schutzklick,
								versicherst Du Dein neues Handy mit einem Rundum-Smartphone-Schutz gegen Diebstahl,
								Sturzschäden, Fremdschäden, Flüssigkeit, Kurzschluss, Bedienungsfehler und Feuer!
							</div>
							<br/><br/>
							<a href="https://www.schutzklick.de/handyversicherung?partner=110785&utm_source=ipvt&utm_medium=aff" target="_blank">
								<span style="display: inline-block; margin-top10px; color: #FFF;background-color: #51AE3D;padding: 5px 10px;border-radius: 0;font-size: 14px;
				font-weight: 400;line-height: 1.42857;text-align: center;white-space: nowrap;vertical-align: middle;">Jetzt informieren!</span>
							</a>
						</td>
					</tr>
				<?php } elseif ($data->insurance == 1) { ?>
					<tr>
						<td style="border:0;border-bottom-right-radius: 10px; border-bottom-left-radius: 10px; padding: 30px; background-color: #f7f7f7; text-align: center; color: #222; text-decoration: none;">
							<div style="font-size: 16px;font-weight:bold;margin-bottom: 5px; color: #222;">SICHER IST SICHER</div>
							<img src="https://www.deinhandy.de/images/mails/financefox_logo.png" style="margin-bottom: 10px;" alt="Schutzklick Logo">
							<div style="font-size: 13px; color: #222; text-decoration: none;">
								Genaue Details und Informationen, zu Deiner Gratis-Handyverischerung inklusive Gutscheincode,
								folgen nach Versand Deines neuen Handys, sodass Du Dein Handy ganz einfach versichern kannst!
							</div>
							<br/><br/>
						</td>
					</tr>
				<?php } ?>
				<tr>
					<!--                    <td style="text-align: center; padding: 20px 0 0 0;">-->
					<!--                        <a href="http://www.caseable.com/partner/deinhandy-deinehuelle">-->
					<!--                            <img src="https://www.deinhandy.de/images/mails/dh_nl_caseable.png" alt=""-->
					<!--                                 style="width: 100%;">-->
					<!--                        </a>-->
					<!--                    </td>-->
				</tr>
				<tr>
					<td style="text-align: center; padding: 20px 0 0 0;">
						<a href="https://www.deinhandy.de/aklamio?source=confmail">
							<img src="https://www.deinhandy.de/images/mails/dh_nl_aklamio.png" alt=""
								 style="width: 100%;">
						</a>
					</td>
				</tr>
				<tr>
					<td class="container-padding footer-text" align="left"
						style="font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
						<br><br>
						<span style="font-weight: bold;font-size: 12px;line-height: 16px;color: #AAA;">DEINHANDY Onlineportal</span><br/>
						<span style="font-size: 12px;line-height: 16px;color: #AAA;">
						Eine Marke der Mister Mobile GmbH  <br/>
						Samerwiesen 6<br/>
						63179 Obertshausen<br/>
						<a href="https://www.deinhandy.de" style="color:#aaaaaa">www.deinhandy.de</a><br>

						<br>
						Sitz der Gesellschaft: Obertshausen<br/>
						Handelregister AG: Offenbach am Main<br/>
						HRB - Nr.: 47873<br/>
						Vertretungsberechtiger: Robert Ermich<br/>
						<br></span>

						<strong>Kontakt:</strong><br/>
						<span class="ios-footer">
							<?php if ($data->fulfillment_partner_id == \common\models\FulfillmentPartners::EINSUNDEINS_ID): ?>
								1und1@deinhandy.de
							<?php else: ?>
								info@deinhandy.de
								<br/>
							030 - 223 865 18 (Mo.-Fr. 09:00-19:00 Uhr) <br/>
							<?php endif; ?>

						</span>
						<br>
					</td>
				</tr>
			</table>
			<!--/600px container -->
		</td>
	</tr>
</table>
<!--/100% background wrapper-->

</body>
</html>
