<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<title>Da ist was schief gelaufen!</title>
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Lato);

		#outlook a {
			padding: 0;
		}

		.ReadMsgBody {
			width: 100%;
		}

		.ExternalClass {
			width: 100%;
		}

		.ExternalClass * {
			line-height: 100%;
		}

		body {
			margin: 0;
			padding: 0;
			-webkit-text-size-adjust: 100%;
			-ms-text-size-adjust: 100%;
		}

		table, td {
			border-collapse: collapse;
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		img {
			border: 0;
			height: auto;
			line-height: 100%;
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
		}
	</style>
	<!--[if mso]>
	<style type="text/css">
		.body-text {
			font-family: Arial, sans-serif !important;
		}
	</style>
	<![endif]-->
	<style type="text/css">
		@media only screen and (min-width: 480px) {
			.mj-column-per-66, * [aria-labelledby="mj-column-per-66"] {
				width: 66% !important;
			}

			.mj-column-per-33, * [aria-labelledby="mj-column-per-33"] {
				width: 33.3% !important;
			}

			.mj-column-per-100, * [aria-labelledby="mj-column-per-100"] {
				width: 100% !important;
			}

			.mj-column-per-50, * [aria-labelledby="mj-column-per-50"] {
				width: 50% !important;
			}
		}
	</style>
</head>
<body style="background: #efefef; margin:0; padding:0;">
<div style="background-color:#efefef;font-size:14px; margin:0;">
	<!--[if mso]>
	<table border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="width:640px;">
		<tr>
			<td>
	<![endif]-->
	<div style="margin:0 auto;max-width:640px;background:#ffffff;">
		<table style="font-size:0;width:100%;background:#ffffff;" align="center" border="0" cellpadding="0" cellspacing="0">
			<tbody>
			<tr>
				<td style="text-align: center; vertical-align: top; font-size: 0px; padding: 0px;">
					<!--[if mso]>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="vertical-align:top;width:640px;">
					<![endif]-->
					<div aria-labelledby="mj-column-per-100" class="mj-column-per-100" style="vertical-align:top;display:inline-block;font-size:12px;text-align:left;width:100%;">
						<table style="vertical-align:top;" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
							<tr>
								<td style="text-align: center; vertical-align: middle; font-size: 0px; padding: 0px; background-color: #51ae3d;">
									<!--[if mso]>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td style="vertical-align:top;width:300px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-50" class="mj-column-per-50" style="vertical-align: top; display: inline-block; font-size: 12px; text-align: left; width: 50%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="font-size: 0px; padding: 0px 0px 0px 20px;" align="left">
													<div style="cursor: auto; color: #ffffff; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 22px;">
														<p style="font-family:'Lato',helvetica,arial,sans-serif; font-size:12px;line-height: 22px; color:#ffffff; padding:0; margin-top:12px;margin-bottom:12px;margin-left:0;margin-right:0;">
															Beratung: 030 - 223 865 18 (Mo. - Fr. 09:00 - 19:00 Uhr)
														</p>
													</div>
												</td>
											</tr>
										</table>
									</div>
									<!--[if mso]>
									</td>
									<td style="vertical-align:top;width:300px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-50" class="mj-column-per-50" style="vertical-align: top; display: inline-block; font-size: 12px; text-align: left; width: 50%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="font-size: 0px; padding: 0px 20px 0px 0px;" align="right">
													<div style="cursor: auto; color: #ffffff; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 22px;">
														<p style="font-family:'Lato',helvetica,arial,sans-serif; font-size:12px;line-height: 22px; color:#ffffff; padding:0; margin-top:12px;margin-bottom:12px;margin-left:0;margin-right:0;">
														</p>
													</div>
												</td>
											</tr>
										</table>
									</div>
									<!--[if mso]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
							</tr>
							<tr>
								<td style="text-align: center; vertical-align: top; font-size: 0px; padding: 35px 0px 20px;">
									<!--[if mso]>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td style="vertical-align:top;width:300px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-50" class="mj-column-per-50" style="vertical-align: top; display: inline-block; font-size: 14px; text-align: left; width: 100%;">
										<table style="vertical-align:top;" border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="font-size: 0px; padding: 0px 20px;" align="left">
													<table style="border-collapse:collapse;border-spacing:0px;" align="left" border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td style="width:168px;">
																<img alt="Dein Handy." title="Dein Handy. Dein Tarif. Ganz einfach." src="https://www.deinhandy.de/images/mails/resubmit/deinhandy_logo.jpg"
																	 style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" height="auto" width="168">
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!--[if mso]>
									</td>
									<td style="vertical-align:top;width:300px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-50" class="mj-column-per-50" style="vertical-align: top; display: inline-block; font-size: 14px; text-align: right; width: 100%;">
										<table style="vertical-align: middle;" border="0" cellpadding="0" cellspacing="0" width="100%">
											<tbody>
											<tr>
												<td style="font-size: 0px; padding: 0px 20px;" align="center">
													<div style="text-align: right;">
														<!--[if mso]>
														<table border="0" cellpadding="0" cellspacing="0" align="center">
															<tr>
																<td>
														<![endif]-->
														<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td style="vertical-align:middle;padding:0 4px;">
																	<table style="width: 31px;" border="0" cellpadding="0" cellspacing="0">
																		<tr>
																			<td style="text-align: center; vertical-align: middle; height: 30px; width: 31px;">
																				<img alt="Trusted Shops Logo" src="https://www.deinhandy.de/images/mails/resubmit/trusted_siegel.jpg" style="display:block;" height="30" width="31">
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<!--[if mso]>
														</td>
														<td>
														<![endif]-->
														<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td style="vertical-align:middle;padding:0 4px;">
																	<table style="width: 30px;" border="0" cellpadding="0" cellspacing="0">
																		<tr>
																			<td style="text-align: center; vertical-align: middle; width: 30px; height: 30px;">
																				<img alt="eKomi - The Feedback Company" src="https://www.deinhandy.de/images/mails/resubmit/ekomi_siegel.jpg" style="display:block;" height="30" width="30">
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<!--[if mso]>
														</td>
														<td>
														<![endif]-->
														<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
															<tbody>
															<tr>
																<td style="vertical-align:middle;padding:0 4px;">
																	<table style="width: 55px;" border="0" cellpadding="0" cellspacing="0">
																		<tbody>
																		<tr>
																			<td style="text-align: center; vertical-align: middle; height: 30px; width: 55px;">
																				<img alt="TÜV Siegel" src="https://www.deinhandy.de/images/mails/resubmit/tuev-siegel.jpg" style="display:block;" height="30" width="55">
																			</td>
																		</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															</tbody>
														</table>
														<!--[if mso]>
														</td>
														</tr>
														</table>
														<![endif]-->
													</div>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
							</tr>
							<tr>
								<td style="font-size: 0px; padding: 0px 20px;">
									<p style="font-size: 1px; margin: 0px auto; width: 100%; border-top: 1px solid #cfcfcf;"></p>
									<!--[if mso]>
									<table align="center" border="0" cellpadding="0" cellspacing="0" style="font-size:1px;margin:0 auto;border-top:4px solid #000000;width:100%;" width="600">
										<tr>
											<td></td>
										</tr>
									</table>
									<![endif]-->
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" align="center">
									<table style="border-collapse:collapse;border-spacing:0px;" align="center" border="0" cellpadding="0" cellspacing="0">
										<tbody>
										<tr>
											<td style="width: 640px;">
												<img alt="header" src="https://www.deinhandy.de/images/mails/resubmit/rrorders_header.jpg" style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" height="auto"
													 width="640">
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="font-size: 0px; padding: 25px 20px 0px;" align="center">
									<div style="cursor: auto; color: #252521; font-family: 'Lato',Helvetica,Arial,sans-serif; line-height: 20px; font-size: 26px;">
										<p style="cursor: auto; color: #252521; font-family: 'Lato',Helvetica,Arial,sans-serif; line-height: 20px; font-size: 26px;">
											<b>Lieber <?= $data->customer_prename ?>,</b>
										</p>
									</div>
								</td>
							</tr>
							<tr>
								<td style="font-size: 0px; padding: 5px;">
									<p style="font-size: 1px; margin: 0px auto; width: 10%; border-top: 2px solid #51ae3d;"></p>
									<!--[if mso]>
									<table align="center" border="0" cellpadding="0" cellspacing="0" style="font-size:1px;margin:0 auto;border-top:4px solid #000000;width:100%;" width="600">
										<tr>
											<td></td>
										</tr>
									</table>
									<![endif]-->
								</td>
							</tr>
							<tr>
								<td style="font-size: 0px; padding: 0px 20px;" align="center">
									<div style="cursor:auto;color:#777777;font-family:'Lato', Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;">
										<p style="cursor:auto;color:#777777;font-family:'Lato', Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;">leider gab es im Zeitraum zwischen dem 11. Mai und 25. Mai immer wieder technische
											Störungen in der Schnittstelle der Datenprüfung bei <?= $data->tariff->getProviderTitle() ?>. Daher kann es sein, dass Deine letzte Bestellung eventuell voreilig abgelehnt wurde.</br>
											Wir sind für Dich der Sache auf den Grund
											gegangen
											und haben mit unserer IT das Problem lösen können. Nach Behebung des technischen Fehlers wurden die Daten erneut übermittelt und zur nun korrigierten Prüfung gegeben.
										</p>
									</div>
								</td>
							</tr>
							<tr>
								<td style="font-size: 0px; padding: 0px 20px;" align="center">
									<div style="cursor: auto; color: #51ae3d; font-family: 'Lato',Helvetica,Arial,sans-serif; line-height: 20px; font-size: 20px;">
										<p style="cursor: auto; color: #51ae3d; font-family: 'Lato',Helvetica,Arial,sans-serif; line-height: 20px; font-size: 20px;">
											<u><b>Deine neue Bestell-ID lautet: <?= $data->order_id ?></b></u>
										</p>
									</div>
								</td>
							</tr>
							<tr>
								<td style="font-size: 0px; padding: 0px 20px;" align="center">
									<div style="cursor:auto;color:#777777;font-family:'Lato', Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;">
										<br style="cursor:auto;color:#777777;font-family:'Lato', Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;">Wir bitten in Namen von <?= $data->tariff->getProviderTitle() ?> und DEINHANDY. um Entschuldigung für die
											Unannehmlichkeiten und den zeitlichen Verzug. Solltest Du von Deiner Bestellung zurücktreten wollen, dann antworte uns ganz einfach auf diese E-Mail.</br>
										Dein 14-tägiges Widerrufsrecht ist von all dem unberührt
											und beginnt erst ab dem Zeitpunkt, ab dem Du Dein Handy und Deine SIM-Karte vom Postboten überreicht bekommst.
										</p>
										<p style="cursor:auto;color:#777777;font-family:'Lato', Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;">Für Fragen stehen wir Dir jederzeit per E-Mail oder per privater Nachricht bei Facebook zur
											Verfügung. Bitte habe Verständnis dafür, dass die Telefonhotline gegebenenfalls etwas überlastet ist, da wir mit einem erhöhten Aufkommen an Kundenanfragen rechnen.
										</p>
									</div>
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top; font-size: 0px; padding: 15px 0px;" align="left">
									<!--[if mso]>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td style="vertical-align:top;width:198px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-33" class="mj-column-per-33" style="vertical-align: top; display: inline-block; font-size: 14px; width: 100%; text-align: left;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tbody>
											<tr>
												<td style="word-break:break-word;font-size:0;padding:0 0 0 0;" align="left">
													<table style="border-collapse:collapse;border-spacing:0px;" align="left" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="width:165px;"><img alt="" src="https://www.deinhandy.de/images/mails/resubmit/greenhand_left.jpg"
																						  style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" height="auto" width="165"></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso]>
									</td>
									<td style="vertical-align:top;width:198px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-33" class="mj-column-per-33" style="vertical-align: top; display: inline-block; font-size: 14px; text-align: left; width: 100%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tbody>
											<tr>
												<td style="word-break:break-word;font-size:0;padding:0 0 0 0;" align="center">
													<div style="cursor:auto;color:#777777;font-family:'Lato', Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;">
														<p style="cursor:auto;color:#777777;font-family:'Lato', Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;">Beste Grüße,
															Dein Team von DEINHANDY.
														</p>
													</div>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso]>
									</td>
									<td style="vertical-align:top;width:198px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-33" class="mj-column-per-33" style="vertical-align: top; display: inline-block; font-size: 14px; text-align: right; width: 100%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tbody>
											<tr>
												<td style="word-break:break-word;font-size:0;padding:0 0 0 0;" align="right">
													<table style="border-collapse:collapse;border-spacing:0px;" align="right" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="width:165px;"><img alt="" src="https://www.deinhandy.de/images/mails/resubmit/greenhand_right.jpg"
																						  style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" height="auto" width="165"></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
							</tr>
							<tr>
								<td style="font-size: 0px; padding: 10px 20px; background-color: #51ae3d;" align="center">
									<div style="text-align:center;">
										<!--[if mso]>
										<table border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td>
										<![endif]-->
										<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table style="width: 25px;" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="text-align: center; vertical-align: middle; width: 25px; height: 25px;"><a href="https://www.facebook.com/deinhandy"><img alt="facebook"
																																																 src="https://www.deinhandy.de/images/mails/resubmit/dhsocial_fb.png"
																																																 style="display:block;" height="25" width="25"></a></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table style="width: 25px;" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="text-align: center; vertical-align: middle; width: 25px; height: 25px;"><a href="https://www.instagram.com/deinhandyde/"><img alt="instagram"
																																																	 src="https://www.deinhandy.de/images/mails/resubmit/dhsocial_in.png"
																																																	 style="display:block;" height="25" width="25"></a>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table style="width: 25px;" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="text-align: center; vertical-align: middle; width: 25px; height: 25px;"><a href="https://twitter.com/deinHandyDE"><img alt="twitter"
																																															  src="https://www.deinhandy.de/images/mails/resubmit/dhsocial_tw.png"
																																															  style="display:block;" height="25" width="25"></a></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table style="width: 25px;" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="text-align: center; vertical-align: middle; width: 25px; height: 25px;"><a href="https://plus.google.com/+DeinHandyDeinTarifGanzEinfach/videos"><img alt="google plus"
																																																							src="https://www.deinhandy.de/images/mails/resubmit/dhsocial_gp.png"
																																																							style="display:block;"
																																																							height="25" width="25"></a>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table style="width: 25px;" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="text-align: center; vertical-align: middle; width: 25px; height: 25px;"><a href="https://www.youtube.com/channel/UChwLFW_7f3q2aKtmw_hvqAg">
																	<img alt="youtube" src="https://www.deinhandy.de/images/mails/resubmit/dhsocial_yt.png" style="display:block;" height="25" width="25"></a></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table style="width: 25px;" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="text-align: center; vertical-align: middle; width: 25px; height: 25px;"><a href="https://www.xing.com/companies/deinhandy.de"><img alt="xing"
																																																		  src="https://www.deinhandy.de/images/mails/resubmit/dhsocial_xi.png"
																																																		  style="display:block;" height="25"
																																																		  width="25"></a></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table style="float:none;display:inline-table;" align="left" border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table style="width: 25px;" border="0" cellpadding="0" cellspacing="0">
														<tbody>
														<tr>
															<td style="text-align: center; vertical-align: middle; width: 25px; height: 25px;"><a href="https://blog.deinhandy.de/"><img alt="blog"
																																														 src="https://www.deinhandy.de/images/mails/resubmit/dhsocial_rs.png"
																																														 style="display:block;"
																																														 height="25" width="25"></a></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										</tr>
										</table>
										<![endif]-->
									</div>
								</td>
							</tr>
							<tr>
								<td style="text-align: center; vertical-align: top; font-size: 0px; padding: 0px; background-color: #efefef;">
									<!--[if mso]>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td style="vertical-align:top;width:198px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-33" class="mj-column-per-33" style="vertical-align:top;display:inline-block;font-size:12px;text-align:left;width:33%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tbody>
											<tr>
												<td style="word-break:break-word;font-size:0;padding:0 0 0 0;" align="left">
													<div style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px;">
														<p style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px;">
															DEINHANDY Onlineportal<br>
															Eine Marke der Mister Mobile GmbH<br>
															Samerwiesen 6<br>
															63179 Obertshausen<br>
														</p>
													</div>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso]>
									</td>
									<td style="vertical-align:top;width:198px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-33" class="mj-column-per-33" style="vertical-align:top;display:inline-block;font-size:12px;text-align:left;width:33%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tbody>
											<tr>
												<td style="word-break:break-word;font-size:0;padding:0 0 0 0;" align="left">
													<div style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px;">
														<p style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px;">
															Sitz der Gesellschaft: Obertshausen<br>
															Handelregister AG: Offenbach am Main<br>
															HRB - Nr.: 47873<br>
															Vertretungsberechtigter: Robert Ermich<br>
														</p>
													</div>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso]>
									</td>
									<td style="vertical-align:top;width:198px;">
									<![endif]-->
									<div aria-labelledby="mj-column-per-33" class="mj-column-per-33" style="vertical-align:top;display:inline-block;font-size:12px;text-align:left;width:33%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tbody>
											<tr>
												<td style="padding-right: 10px; margin: 0px; padding-bottom: 0px; font-size: 12px; font-weight: normal; color: #777777; font-family: 'Lato',Arial,sans-serif; padding-left: 15px;" align="left"
													valign="middle" width="13">
													<img editable="true" src="https://www.deinhandy.de/images/mails/resubmit/dhicon_mail.png" alt="mail-icon" style="display: block; " height="12" width="15">
												</td>
												<td style="font-size: 0px;" align="left">
													<div style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px;">
														<p style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; margin: 5px 0px;">
															kundenhife@deinhandy.de
														</p>
													</div>
												</td>
											</tr>
											<tr>
												<td style="padding-right: 10px; margin: 0px; padding-bottom: 0px; font-size: 12px; font-weight: normal; color: #777777; font-family: 'Lato',Arial,sans-serif; line-height: 23px; padding-left: 15px;"
													align="left" valign="middle" width="13">
													<img editable="true" src="https://www.deinhandy.de/images/mails/resubmit/dhicon_phone.png" alt="phone-icon" style="display: block; " height="12" width="15">
												</td>
												<td style="font-size: 0px;" align="left">
													<div style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px;">
														<p style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px; margin: 5px 0px;">
															030 - 223 865 18
														</p>
													</div>
												</td>
											</tr>
											<tr>
												<td style="padding-right: 10px; margin: 0px; font-size: 12px; font-weight: normal; color: #777777; font-family: 'Lato',Arial,sans-serif; line-height: 23px; padding-bottom: 0px; padding-left: 15px;"
													align="left" valign="middle" width="13"><img editable="true" src="https://www.deinhandy.de/images/mails/resubmit/dhicon_link.png" alt="link-icon" style="display: block; " height="12" width="15">
												</td>
												<td style="font-size: 0px;" align="left">
													<div style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px;">
														<p style="cursor: auto; color: #777777; font-family: 'Lato',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px; margin: 5px 0px;">
															www.deinhandy.de
														</p>
													</div>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!--[if mso]>
					</td>
					</tr>
					</table>
					<![endif]-->
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
</div>
</body>
</html>

