<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- So that mobile will display zoomed in -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- enable media queries for windows phone 8 -->
	<meta name="format-detection" content="telephone=no">
	<!-- disable auto telephone linking in iOS -->
</head>

<body style="margin:0; padding:0; padding-top: 25px;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
	<tr>
		<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
			<!-- 600px container (white background) -->
			<table border="0" width="600" cellpadding="0" cellspacing="0" style="width:600px;max-width:600px">
				<tr style="background-color:#ffffff">
					<td style="text-align:left;padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;">
						<a href="https://www.deinhandy.de">
							<img src="https://www.deinhandy.de/images/mails/dh_nl_logo.png" alt=""/>
						</a>
					</td>
					<td style="text-align:right;padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;">
						<img src="https://www.deinhandy.de/images/mails/dh_nl_trust.png" alt=""/>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:left;padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;background-color:#ffffff">
						<div style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
							<strong>Liebe DeinHandy-Kundin, lieber DeinHandy-Kunde,</strong>
							<br/><br/>
							##### TEXT #####
							<br/><br/>
							<span style="color: #4fae3d;">Dein Team von DeinHandy</span>
							<br/>
						</div>
						<br/>
					</td>
				</tr>
				<!-- Footer For Email change  -->
				<tr style="font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;">
					<td align="left" style="padding-left:24px;padding-right:24px;padding-bottom:24px;">
						<br/><br/>
						<strong>DEINHANDY Onlineportal</strong><br/>
						Eine Marke der Mister Mobile GmbH  <br/>
						Samerwiesen 6<br/>
						63179 Obertshausen<br/>
						<a href="https://www.deinhandy.de" style="color:#aaaaaa">www.deinhandy.de</a><br/>
						<br/>
						Sitz der Gesellschaft: Obertshausen<br/>
						Handelregister AG: Offenbach am Main<br/>
						HRB - Nr.: 47873<br/>
						Vertretungsberechtiger: Robert Ermich
					</td>
					<td align="right" style="vertical-align: top; padding-top: 32px;padding-left:24px;padding-right:24px;padding-bottom:24px;">
						<strong>Kontakt</strong><br/>
						info@deinhandy.de<br/>
						030 - 223 865 18 (Mo.-Fr. 09:00-19:00 Uhr)
					</td>
				</tr>
			</table>
			<!--/600px container -->
		</td>
	</tr>
</table>
<!--/100% background wrapper-->
</body>
</html>
