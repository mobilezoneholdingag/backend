Bestellbestätigung


Liebe<?php if ($data->customer_gender == 2) echo 'r'; ?> <?= $data->customer_prename; ?> <?= $data->customer_surname; ?>,

vielen Dank für Deine Bestellung bei *DeinHandy*.

<?php if ($data->fulfillment_partner_id == 3) { ?>
	du erhältst in Kürze eine Bestellbestätigung per E-Mail von 1&1. In Folge informieren wir Sie gern über den aktuellen Status Ihrer Bestellung.
<?php } else { ?>
	in dieser Mail haben wir für Dich noch einmal alle Informationen zu Deiner Bestellung mit der Bestellnummer <?= $data->order_id ?> aufgelistet:
<?php } ?>

Monatliche Kosten: 0,00 €
Einmalige Kosten: <?php if (isset($data->tariff_device_final_price) && $data->tariff_device_final_price > 0) { ?><?= number_format($data->tariff_device_final_price, 2, ',', '.') ?> €<?php } else if (isset($data->tariff_device_final_price) && $data->tariff_device_final_price == 0 && $data->coupon_id) { ?><?= number_format($data->tariff_device_final_price, 2, ',', '.') ?> €<?php } else { ?>1,00 € - 1,00 € Rabatt = 0,00 €<?php } ?> Gerätezuzahlung

Versandkosten 0,00 €

*Gesamtpreis* (inkl. 19% MwSt.):
Monatliche Kosten: <?= number_format($data->total_monthly, 2, ',', '.') ?> €
Einmalige Kosten: <?= number_format($data->total_once, 2, ',', '.') ?> €


Gewählte Zahlungskondition: *<?php
if ($data->payment_type == '0') { echo 'Vorkasse'; }
else if ($data->payment_type == '1') { echo 'Nachnahme'; }
else if ($data->payment_type == '2') { echo 'Sofortüberweisung'; }
else if ($data->payment_type == '3') { echo 'PayPal'; }
else if ($data->payment_type == '4') { echo 'Bankeinzug'; }
?>*
Gewählter Spediteur: *DHL*

<?php if (is_numeric($data->payment_type) && $data->tariff_device_final_price > 0) { ?>
	<?php if ($data->payment_type == '0') { ?>
		*Bitte überweise den Gesamtbetrag von <?= number_format($data->tariff_device_final_price, 2, ',', '.') ?> € auf unser hier aufgeführtes Bankkonto:*

		Kontoinhaber: *einsAmobile GmbH*
		Kreditinstitut: *Sparkasse Langen-Seligenstadt*
		Bankleitzahl: *506 521 24*
		Kontonummer: *112 55 66*

		IBAN: *DE08 5065 2124 0001 1255 66*
		BIC: *HELADEF1SLS*

		Verwendungszweck: *40003 <?= $data->order_id ?> / <?= $data->customer_prename ?> <?= $data->customer_surname ?>*

	<?php } else if ($data->payment_type == '1') { ?>
		Bitte halte den Gesamtbetrag von <?= number_format($data->tariff_device_final_price, 2, ',', '.') ?> € plus 6,90 € Nachnahmegebühr für den Zusteller bereit.
	<?php } else if ($data->payment_type == '2') { ?>
		Bitte zahle per Sofortüberweisung über diesen Link:
		<a href="<?= $data->payment_link ?>">Hier klicken um Zahlung einzuleiten</a>.
	<?php } else if ($data->payment_type == '3') { ?>
		Bitte zahle per PayPal über diesen Link:
		<a href="<?= $data->payment_link ?>">Hier klicken um Zahlung einzuleiten</a>.
	<?php } else if ($data->payment_type == '4') { ?>
		Der Betrag wird automatisch von Deinem Konto abgebucht.
	<?php } ?>

<?php } ?>

<?php if (!empty($data->order_id)) { ?>
Den aktuellen Status deiner Bestellung kannst du hier unter Angabe deiner Bestellnummer <?= $data->order_id ?> abfragen:
http://www.deinhandy.de/bestellstatus
<?php } ?>

Im Anhang dieser Email findest Du des Weiteren unsere Allgemeinen
Geschäftsbedingungen und die Widerrufsbelehrung.

Bei eventuell bestehenden Rückfragen stehen wir Dir gerne zur Verfügung.

*Wir wünschen Dir viel Spass mit Deinem neuen Vertrag!*

Mit freundlichen Grüßen
Dein Team von deinHandy

deinhandy.de Onlineportal
Eine Marke der Mister Mobile GmbH  
Samerwiesen 6
63179 Obertshausen

*Kontakt*
Email: info@deinhandy.de
Tel. 06104 - 4057 182
Fax. 06104 - 4057 78

Sie erreichen uns telefonisch von Montag - Freitag von 9.00 - 19.00 Uhr

Sitz der Gesellschaft: Obertshausen
Handelsregister Ag. Offenbach/Main
HRB - Nr.: 47873
Vertretungsberechtigter Geschäftsführer: Robert Ermich

<?php if ($data->insurance == 0) { ?>
	SICHER IST SICHER
	Mit der Handyversicherung, von unserem Versicherungspartner Schutzklick, versicherst Du Dein neues Handy mit einem Rundum-Smartphone-Schutz gegen Diebstahl, Sturzschäden, Fremdschäden, Flüssigkeit, Kurzschluss, Bedienungsfehler und Feuer!
	Jetzt informieren!
	https://www.schutzklick.de/handyversicherung?partner=110785&utm_source=ipvt&utm_medium=aff
<?php } elseif ($data->insurance == 1) { ?>
	SICHER IST SICHER
	Genaue Details und Informationen, zu Deiner Gratis-Handyverischerung inklusive Gutscheincode, folgen nach Versand Deines neuen Handys, sodass Du Dein Handy ganz einfach versichern kannst!
<?php } ?>

https://www.aklamio.com/recommend?uid=0b816e31599cf86c46addd115226b4e5&utm_source=cm&utm_medium=conmail&utm_campaign=deinhandy_de