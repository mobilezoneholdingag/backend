<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>extrA-Paket</title>
	<style type="text/css">
		#outlook a {
			padding: 0;
		}

		.ReadMsgBody {
			width: 100%;
		}

		.ExternalClass {
			width: 100%;
		}

		.ExternalClass * {
			line-height: 100%;
		}

		body {
			margin: 0;
			padding: 0;
			-webkit-text-size-adjust: 100%;
			-ms-text-size-adjust: 100%;
		}

		table, td {
			border-collapse: collapse;
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		img {
			border: 0;
			height: auto;
			line-height: 100%;
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
		}

		p {
			display: block;
			margin: 13px 0;
		}
	</style>
	<!--[if !mso]><!-->
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
	</style>
	<style type="text/css">
		@media only screen and (max-width: 480px) {
			@-ms-viewport {
				width: 320px;
			}
			@viewport {
				width: 320px;
			}
		}
	</style>
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
	<!--<![endif]-->
	<style type="text/css">
		@media only screen and (min-width: 480px) {
			.mj-column-per-66, * [aria-labelledby="mj-column-per-66"] {
				width: 66% !important;
			}

			.mj-column-per-33, * [aria-labelledby="mj-column-per-33"] {
				width: 33% !important;
			}

			.mj-column-per-100, * [aria-labelledby="mj-column-per-100"] {
				width: 100% !important;
			}

			.mj-column-per-50, * [aria-labelledby="mj-column-per-50"] {
				width: 50% !important;
			}
		}
	</style>
</head>
<body style="background: #e6e1e1;">
<div style="background-color:#e6e1e1;font-size:13px;">
	<!--[if mso]>
	<table border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
		<tr>
			<td>
	<![endif]-->
	<div style="margin:0 auto;max-width:600px;background:#ffffff;">
		<table cellpadding="0" cellspacing="0" style="font-size:0;width:100%;background:#ffffff;" align="center" border="0">
			<tbody>
			<tr>
				<td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;padding-bottom:0px;padding-top:0px;"><!--[if mso]>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="vertical-align:top;width:600px;">
					<![endif]-->
					<div aria-labelledby="mj-column-per-100" class="mj-column-per-100" style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;">
						<table cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
							<tbody>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:25px;padding-bottom:16px;padding-right:25px;padding-left:25px;" align="center">
									<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0">
										<tbody>
										<tr>
											<td style="width:344px;"><a href="https://www.deinhandy.de/" target="_blank"><img alt="" src="http://3xp8.mj.am/img/3xp8/b/15p9g/66q7.png"
																															  style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="344" height="auto"></a>
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" align="center">
									<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0">
										<tbody>
										<tr>
											<td style="width:600px;"><a href="http://www.samsung.com/de/offer/extra-paket/" target="_blank">
												<img alt="Samsung ExtrA-Paket" src="https://www.deinhandy.de/images/mails/samsung/newsletter-extra-paket.png"

													 style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"
													 height="auto"></a></td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:0;padding-right:25px;padding-left:25px;" align="center">
									<div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;"><p><u>
										<span style="font-size:19px"><b>Herzlichen Glückwunsch zu Deinem neuen<br/>Samsung Galaxy A3 (2016) oder A5 (2016)</b></span></u></p></div>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:0;padding-right:25px;padding-left:25px;" align="left">
									<div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;"><p><span style="font-size:15px"><b><span
											style="font-size:17px">Du hast noch nicht genug?<br/>Dann hol dir jetzt Dein ExtrA -Paket!</span></b></span>
									</p>
										<p><b>Um das Erlebnis Deines neuen Handys so aufregend wie möglich zu machen, bietet Samsung allen Kunden, die zwischen dem 19. April und 11. Mai 2016 ein Samsung Galaxy A3 (2016) oder A5 (2016) erworben haben,
											die Möglichkeit sich Online für ein Gratis ExtrA-Paket zu registrieren.</b></p></div>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:8px;padding-bottom:0;padding-right:25px;padding-left:25px;" align="left">
									<div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:15px;"><p><b><span style="font-size:18px">Gratis ExtrA-Paket? so geht's:</span></b></p>
										<p><b> <span style="font-size:13px"> 1. Bis zum <span style="color:#de1313">25.05.2016</span></span> auf der Samsung Aktionsseite<span style="font-size:13px"> <a
												href="https://www.markenmehrwert.com/campaign/extra-paket" target="_blank" style="color: inherit; text-decoration: none;"><u>registrieren</u></a> </span></b></p>
										<p><b><span style="font-size:13px">  2. IMEI-Nummer, Bestellbestätigung und Rechnung zur Hand haben </span></b></p>
										<p><b><span style="font-size:13px">  3. Daten eingeben </span></b></p>
										<p><b><span style="font-size:13px">  4. Warten bis das ExtrA-Paket geliefert wird </span></b></p>
										<p><b><span style="font-size:13px">  5. Auspacken, Handy upgraden und genießen!</span></b></p></div>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:15px 30px;padding-top:12px;padding-bottom:12px;padding-right:25px;padding-left:25px;" align="center">
									<table cellpadding="0" cellspacing="0" style="border:none;border-radius:3px;" align="center" border="0">
										<tbody>
										<tr>
											<td style="background:#21b500;border-radius:3px;color:#FFFFFF;cursor:auto;" align="center" valign="top" bgcolor="#21b500">
												<a href="https://www.markenmehrwert.com/campaign/extra-paket"
												   style="display:inline-block;text-decoration:none;background:#21b500;border:1px solid #21b500;border-radius:3px;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;padding:15px 30px;"
												   target="_blank">Hier geht's zum gratis ExtrA-Paket</a></td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:0;padding-right:25px;padding-left:25px;" align="center">
									<div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;"><p><span style="font-size:15px"><b><span
											style="font-size:16px">Was ist das ExtrA-Paket?</span></b></span></p>
										<p style="color:rgb(0, 0, 0); font-family:ubuntu,helvetica,arial,sans-serif; font-size:13px"><b>Das ExrA -Paket enthält passend zu Deinem neuen Handy eine 64GB Micro SD Karte, ein Clear View Cover und einen externen
											Akku. Damit kannst Du noch mehr Fotos machen, noch länger mit den Liebsten telefonieren und im Fall eines Sturzes bist Du auf der sicheren Seite. Weitere Informationen findest Du unter <a target="_blank"
																																																								style="color: inherit; text-decoration: none;"
																																																								href="http://www.samsung.com/de/offer/extra-paket/"><u>www.samsung.de</u></a></b>
										</p></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!--[if mso]>
					</td></tr></table>
					<![endif]--></td>
			</tr>
			</tbody>
		</table>
	</div>
	<!--[if mso]>
	</td></tr></table>
	<![endif]-->
	<!--[if mso]>
	<table border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
		<tr>
			<td>
	<![endif]-->
	<div style="margin:0 auto;max-width:600px;background:#ffffff;">
		<table cellpadding="0" cellspacing="0" style="font-size:0;width:100%;background:#ffffff;" align="center" border="0">
			<tbody>
			<tr>
				<td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;padding-bottom:0px;padding-top:0px;"><!--[if mso]>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="vertical-align:top;width:600px;">
					<![endif]-->
					<div aria-labelledby="mj-column-per-100" class="mj-column-per-100" style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;">
						<table cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
							<tbody>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:10px;padding-bottom:0px;padding-right:25px;padding-left:25px;" align="center">
									<div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:13px;"><h2 style="font-size: 19px; font-weight: 700; line-height: 30px;">Sicher, schnell und
										zuverlässig</h2></div>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:0px;padding-bottom:10px;padding-right:25px;padding-left:25px;" align="center">
									<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0">
										<tbody>
										<tr>
											<td style="width:333px;"><img alt="" src="http://3xp8.mj.am/img/3xp8/b/15p9g/6g07.png" style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="333" height="auto">
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!--[if mso]>
					</td></tr></table>
					<![endif]--></td>
			</tr>
			</tbody>
		</table>
	</div>
	<!--[if mso]>
	</td></tr></table>
	<![endif]-->
	<!--[if mso]>
	<table border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
		<tr>
			<td>
	<![endif]-->
	<div style="margin:0 auto;max-width:600px;background:#ffffff;">
		<table cellpadding="0" cellspacing="0" style="font-size:0;width:100%;background:#ffffff;" align="center" border="0">
			<tbody>
			<tr>
				<td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;padding-bottom:10px;padding-top:9px;"><!--[if mso]>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="vertical-align:top;width:300px;">
					<![endif]-->
					<div aria-labelledby="mj-column-per-50" class="mj-column-per-50" style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;">
						<table cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
							<tbody>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:10px;padding-bottom:10px;padding-right:25px;padding-left:25px;" align="left">
									<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="left" border="0">
										<tbody>
										<tr>
											<td style="width:100px;"><a href="https://www.deinhandy.de/" target="_blank"><img alt="" src="http://3xp8.mj.am/img/3xp8/b/15p9g/6g8o.png"
																															  style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="100" height="auto"></a>
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!--[if mso]>
					</td>
					<td style="vertical-align:top;width:300px;">
					<![endif]-->
					<div aria-labelledby="mj-column-per-50" class="mj-column-per-50" style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;">
						<table cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
							<tbody>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:7px;padding-bottom:10px;padding-right:25px;padding-left:25px;" align="center">
									<div style="text-align:center;"><!--[if mso]>
										<table border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td>
										<![endif]-->
										<table cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table cellpadding="0" cellspacing="0" style="background:#3b5998;border-radius:3px;width:20px;" border="0">
														<tbody>
														<tr>
															<td style="text-align:center;vertical-align:middle;width:20px;height:20px;"><a href="https://www.facebook.com/deinhandy/?fref=ts">
																<img alt="facebook" height="20"
																	 src="https://www.mailjet.com/images/theme/v1/icons/ico-social/facebook.png"
																	 style="display:block;border-radius:3px;" width="20"></a>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table cellpadding="0" cellspacing="0" style="background:#55acee;border-radius:3px;width:20px;" border="0">
														<tbody>
														<tr>
															<td style="text-align:center;vertical-align:middle;width:20px;height:20px;"><a href="https://www.twitter.com/deinHandyDE">
																<img alt="twitter" height="20"
																	 src="https://www.mailjet.com/images/theme/v1/icons/ico-social/twitter.png"
																	 style="display:block;border-radius:3px;" width="20"></a></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table cellpadding="0" cellspacing="0" style="background:#dc4e41;border-radius:3px;width:20px;" border="0">
														<tbody>
														<tr>
															<td style="text-align:center;vertical-align:middle;width:20px;height:20px;"><a href="https://plus.google.com/+DeinHandyDeinTarifGanzEinfach/about">
																<img alt="google" height="20"
																	 src="https://www.mailjet.com/images/theme/v1/icons/ico-social/google-plus.png"
																	 style="display:block;border-radius:3px;"
																	 width="20"></a></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<td>
										<![endif]-->
										<table cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
											<tbody>
											<tr>
												<td style="vertical-align:middle;padding:0 4px;">
													<table cellpadding="0" cellspacing="0" style="background:#3f729b;border-radius:3px;width:20px;" border="0">
														<tbody>
														<tr>
															<td style="text-align:center;vertical-align:middle;width:20px;height:20px;"><a href="https://www.instagram.com/deinhandyde">
																<img alt="instagram" height="20"
																	 src="https://www.mailjet.com/images/theme/v1/icons/ico-social/instagram.png"
																	 style="display:block;border-radius:3px;" width="20"></a></td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td></tr></table>
										<![endif]--></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!--[if mso]>
					</td></tr></table>
					<![endif]--></td>
			</tr>
			</tbody>
		</table>
	</div>
	<!--[if mso]>
	</td></tr></table>
	<![endif]-->
	<!--[if mso]>
	<table border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
		<tr>
			<td>
	<![endif]-->
	<div style="margin:0 auto;max-width:600px;background:transparent;">
		<table cellpadding="0" cellspacing="0" style="font-size:0;width:100%;background:transparent;" align="center" border="0">
			<tbody>
			<tr>
				<td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;padding-bottom:0px;padding-top:0px;"><!--[if mso]>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="vertical-align:top;width:300px;">
					<![endif]-->
					<div aria-labelledby="mj-column-per-50" class="mj-column-per-50" style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;">
						<table cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
							<tbody>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0;padding-left:0;" align="left">
									<div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:13px;"><p>
										<span style="color:rgb(170, 170, 170); font-size:12px">
											<span style="color:rgb(153, 153, 153)">DEINHANDY Onlineportal
											</span>
										</span>
										<br>
										<span style="color:rgb(170, 170, 170); font-size:12px">
											<span style="color:rgb(153, 153, 153)">Eine Marke der Mister Mobile GmbH  
											</span>
											<br>
											<span style="color:rgb(153, 153, 153)">Samerwiesen 6
										</span>
											<br>
											<span style="color:rgb(153, 153, 153)">63179 Obertshausen
											</span>
											<br>
											<a href="https://www.deinhandy.de/" target="_blank" style="text-decoration: none;">
												<span style="color:rgb(153, 153, 153)"><u>www.deinhandy.de</u>
												</span>
											</a>
											<br><br>
											<span style="color:rgb(153, 153, 153)">Sitz der Gesellschaft: Obertshausen
											</span>
											<br>
											<span style="color:rgb(153, 153, 153)">Handelregister AG: Offenbach am Main
											</span>
											<br>
											<span style="color:rgb(153, 153, 153)">HRB - Nr.: 47873
											</span>
											<br>
											<span style="color:rgb(153, 153, 153)">Vertretungsberechtiger: Robert Ermich
											</span>
										</span><br>
									</p></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!--[if mso]>
					</td>
					<td style="vertical-align:top;width:300px;">
					<![endif]-->
					<div aria-labelledby="mj-column-per-50" class="mj-column-per-50" style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;">
						<table cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
							<tbody>
							<tr>
								<td style="word-break:break-word;font-size:0;padding:10px 25px;padding-top:10px;padding-bottom:10px;padding-right:0;padding-left:0;" align="right">
									<div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:3px;"><p><span style="color: rgb(153, 153, 153);"><b>Kontakt </b></span></p>
										<p><span style="color: rgb(153, 153, 153);">fragen@deinhandy.de </span></p>
										<p><span style="color: rgb(153, 153, 153);">030 - 223 865 18 </span></p>
										<p><span style="color: rgb(153, 153, 153);">(Mo.-Fr. 09:00-19:00 Uhr)</span></p></div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!--[if mso]>
					</td></tr></table>
					<![endif]--></td>
			</tr>
			</tbody>
		</table>
	</div>
	<!--[if mso]>
	</td></tr></table>
	<![endif]-->
</div>
</body>
</html>