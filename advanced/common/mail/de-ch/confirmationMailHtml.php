<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width"/>
</head>
<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
	<tr>
		<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
			<br/>
			<!-- 600px container (white background) -->
			<table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
				<tr>
					<td colspan="2" class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:24px;padding-bottom:12px;background-color:#ffffff">
						<div align="center">
							<a href="https://www.deinhandy.de">
								<img src="https://www.deinhandy.de/images/mails/dh_nl_logo.png" alt="deinhandy"/>
							</a>
						</div>
						<div style="height:0;clear: both;">&nbsp;</div>
						<br/>
						<div class="hr" style="height:1px;border-bottom:1px solid #eee;clear: both;">&nbsp;</div>
						<br/>
						<div class="title" style="font-family:Arial, sans-serif;font-size:22px;font-weight:600;color:#4fae3d">
							Vielen Dank für Deine Bestellung
						</div>
						<br/>
						<div class="body-text" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
							Hallo <?= $data->customer_prename; ?> <?= $data->customer_surname; ?>,
							in dieser Mail haben wir für Dich noch einmal alle Informationen zu Deiner Bestellung aufgelistet:
						</div>
						<br/>
						<!-- #Deine Auswahl -->
						<div style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Deine Auswahl</div>
						</div>
						<table width="264" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row">
							<tr>
								<td class="col" valign="top" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
									<table style="width: 100%;">
										<tr>
											<td style="width: 70%;">
												<div style="height: 45px; padding-top: 15px;">
													<img src="<?= $article->manufacturer->image ?>" height="35" style="height: 35px;" alt="<?= $article->manufacturerTitle; ?>"/>
												</div>
												<div class="text" style="width: 100%; display:block; font-weight: bold; font-size: 15px;">
													<?= $article->manufacturer->title ?> <?= $article->shortName ?>
												</div>
												<div style="font-weight: normal; font-size: 14px;"><?= $article->memory ?>
													GB, <?= $article->color_label ?>
												</div>
											</td>
											<td style="width: 30%; text-align: right; padding: 15px;">
												<img src="<?= $article->image->getUrl('small', true); ?>"
													 alt="" style="height: 100px; width: 50px;"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="264" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row">
							<tr>
								<td class="col" valign="top" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
									<table style="width: 100%;">
										<tr>
											<td style="width: 70%;">
												<div style="height: 45px; padding-top: 15px;">
													<img src="https://www.deinhandy.de/<?= $tariff->provider->image; ?>" height="35" style="height: 35px;" alt="<?= $tariff->provider->title; ?>"/>
												</div>
												<div class="text" style="width: 100%; display:block; font-weight: bold; font-size: 15px;">
													<?= $tariff->provider->title ?> <?= $tariff->title ?>
												</div>
												<div style="font-weight: normal; font-size: 14px;">
													Vertragslaufzeit: <?= $tariff->contract_duration ?> Monate
												</div>
											</td>
											<td style="width: 30%; text-align: right; padding: 15px;">
												<img src="https://www.deinhandy.de/images/mails/dh_nl_sim.png" alt=""
													 style="height: 100px; width: 50px;"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- #Deine Auswahl -->
						<div class="hr" style="height:0;clear: both;">&nbsp;</div>
						<br/>
						<!-- #Einmalige Kosten -->
						<div style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Einmalige Kosten</div>
						</div>
						<table style="width: 100%;" cellspacing="0">
							<tr>
								<td class="pricing-text" style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									<?= $article->manufacturer->title ?> <?= $article->product_name ?>
								</td>
								<td class="pricing-text" style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 1px solid #ededed; color: #333;">
									CHF <?= number_format($data->tariff_device_final_price, 2, '.', '.') ?>
								</td>
							</tr>

							<tr>
								<td class="pricing-text" style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 0px solid #ededed; color: #333;">
									Versandkosten
								</td>
								<td class="pricing-text" style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; border-bottom: 0px solid #ededed; color: #333;">
									CHF <?= number_format(0, 2, '.', '.') ?>
								</td>
							</tr>
							<tr>
								<td class="pricing-text" style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; color: #333; background-color: #eee; border: none;">
									Gesamt (inkl. 8% MwSt.)
								</td>
								<td class="pricing-text" style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; color: #333; background-color: #eee; border: none;">
									CHF <?= number_format($data->total_once, 2, '.', '.') ?>
								</td>
							</tr>
						</table>
						<!-- #Einmalige Kosten -->
						<br/>
						<!-- #Monatliche Kosten -->
						<div style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Monatliche Kosten</div>
						</div>
						<table style="width: 100%;">
							<tr>
								<td class="pricing-text" style="text-align: left; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; color: #333;">
									<?= $tariff->provider->title ?> <?= $tariff->title ?>
								</td>
								<td class="pricing-text" style="text-align: right; font-family:Arial, sans-serif; padding: 12px 12px; font-size: 14px; font-weight: bold; color: #333;">
									<?php if ($data->tariff_monthly_special_discount) { ?>
									<div style="color: #C0C0C0;">CHF <?= number_format($data->tariff_monthly_price, 2, '.', '.') ?>

									</div>
									<div style="font-size: 14px;font-weight: normal;">
										CHF <?= number_format($data->tariff_monthly_special_discount, 2, '.', '.') ?>
										Rabatt<br/><br/>
									</div>
									<div>
										CHF <?= number_format($data->tariff_monthly_price_after_special_discount, 2, '.', '.') ?>
									</div>
									<?php } else { ?>
									<div>CHF <?= number_format($data->tariff_monthly_price, 2, '.', '.') ?></div>
									<?php } ?>
								</td>
							</tr>
						</table>
						<!-- #Monatliche Kosten -->
						<br/>
						<!-- #Rechnungsadresse -->
						<div style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Rechnungsadresse</div>
						</div>
						<table>
							<tr>
								<td class="text" style="padding: 5px;">
									<br/>
									<?= $data->customer_prename; ?> <?= $data->customer_surname; ?><br/>
									<?= $data->customer_street; ?> <?= $data->customer_streetnumber; ?><br/>
									<?= $data->customer_zip; ?> <?= $data->customer_city; ?><br/>
								</td>
							</tr>
						</table>
						<!-- #Rechnungsadresse -->

						<div style="height:0;clear: both;">&nbsp;</div>
						<br/>

						<div style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#fff; background-color: #333; width: 100%; font-weight: bold;">
							<div style="padding: 8px 12px;">Lieferadresse</div>
						</div>

						<table>
							<tr>
								<td class="text" style="padding: 5px;">
									<br/>
									<?php if ($data->delivery_address) { ?>
									<?php if (!empty($data->delivery_company)) { ?>
									<?= $data->delivery_company; ?><br/>
									<?php } ?>
									<?= $data->customer_prename; ?> <?= $data->customer_surname; ?><br/>
									<?= $data->delivery_street; ?> <?= $data->delivery_streetnumber; ?><br/>
									<?= $data->delivery_zipcode; ?> <?= $data->delivery_city; ?><br/>
									<?php } else { ?>
									<?= $data->customer_prename; ?> <?= $data->customer_surname; ?><br/>
									<?= $data->customer_street; ?> <?= $data->customer_streetnumber; ?><br/>
									<?= $data->customer_zip; ?> <?= $data->customer_city; ?><br/>
									<?php } ?>
								</td>
							</tr>
						</table>

						<div style="height:0;clear: both;">&nbsp;</div>
						<br/>
						<div class="hr" style="height:1px;border-bottom:1px solid #eee;clear: both;">&nbsp;</div>
						<br/><br/>

						<div class="body-text" style="font-family:Arial, sans-serif;font-size:14px;line-height:20px;text-align:center;color:#333333; padding-bottom: 30px;">
							Im Anhang dieser E-Mail findest Du des Weiteren unsere Allgemeinen Geschäftsbedingungen.
							<br/><br/>
							Bei eventuell bestehenden Rückfragen stehen wir Dir gern zur Verfügung.
							<br/><br/>
							<span style="color: #4fae3d; font-weight: bold;">Wir wünschen Dir viel Spass mit Deinem neuen Handy!</span>
						</div>
					</td>
				</tr>
				<!-- Footer For Email change  -->
				<tr style="font-family:Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;">
					<td align="left" style="padding-left:24px;padding-right:24px;padding-bottom:24px;">
						<br/><br/>
						<strong>mobilezone ag</strong><br/>
						<strong>DEINHANDY.CH</strong><br/>
						Riedthofstrasse 124<br/>
						8105 Regensdorf<br/>
						Schweiz<br/><br/>
						DeinHandy.ch ist ein Unternehmen<br/>
						der mobilezone Gruppe<br/><br/>
					</td>
					<td align="right" style="vertical-align: top; padding-top: 32px;padding-left:24px;padding-right:24px;padding-bottom:24px;">
						<strong>Kontakt</strong><br/>
						info@deinhandy.ch<br/>
						043 388 77 99 (Mo.-Fr. 08:00 – 12:00 / 13:00 – 18:00 Uhr)<br/>
						<a href="https://www.deinhandy.ch" style="color:#aaaaaa">www.deinhandy.ch</a><br/>
					</td>
				</tr>
				<!--/600px container -->
			</table>
		</td>
	</tr>
</table>
<!--/100% background wrapper-->
</body>
</html>
