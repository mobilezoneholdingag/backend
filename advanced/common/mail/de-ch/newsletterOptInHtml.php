<table style="border-collapse: separate;border-spacing: 0;width:
		600px;margin: 0;font-family: Arial, sans-serif;font-size:
		13px;color: #616161" border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td style="text-align:center;padding:20px 0;;border:solid
			  #e4e4e4;border-width: 1px;border:1px solid
			  #e4e4e4;border-width: 1px">
			<a href="https://www.deinhandy.ch">
				<img moz-do-not-send="true" src="http://deinhandy.ch/frontend/template_deinhandy/images/logo_header.png">
			</a>
		</td>
	</tr>
	</tbody>
</table>
<table style="border-collapse: separate;border-spacing: 0;width:
		600px;margin: 0;font-family: Arial, sans-serif;font-size:
		13px;color: #616161" border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td style="padding:30px;border:solid #e4e4e4;border-width: 0
			  1px 1px 1px"> <span style="font-size: 20px;font-weight:
				bold">Newsletter-Anmeldung</span> <br>
			<br>
			<br>
				<span style="font-size: 14px">Hallo,</span>
			<br>
			<br>
			vielen Dank für Dein Interesse am deinHandy-Newsletter. Um die Anmeldung abzuschliessen und
			Missbrauch Deiner Email-Adresse (<span style="font-weight: bold;"><?php echo $subscriber->email; ?></span>)
			vorzubeugen ist es notwendig deine Eintragung mit einem Klick auf diesen Link zu bestätigen:
			<br>
			<br>
			<span style="font-weight: bold;">
				<a href="https://www.deinhandy.ch/site/newsletter-confirm?id=<?php echo urlencode($subscriber->id); ?>&email=<?php echo urlencode($subscriber->email); ?>">
					https://www.deinhandy.ch/site/newsletter-confirm?id=<?php echo urlencode($subscriber->id); ?>&email=<?php echo urlencode($subscriber->email); ?>
				</a>
			</span>
		</td>
	</tr>
	</tbody>
</table>
<table style="border-collapse: separate;border-spacing: 0;width:
		600px;margin: 0;font-family: Arial, sans-serif;;font-size:
		13px;color: #616161" border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td style="padding:30px;border:solid #e4e4e4;border-width:
			  1px;border:1px solid #e4e4e4;border-width: 0 1px 1px 1px">Mit
			freundlichen Grüßen<br>
			Dein Team von deinHandy<br><br>

			<strong>mobilezone ag</strong><br/>
			<strong>DEINHANDY.CH</strong><br/>
			Riedthofstrasse 124<br/>
			8105 Regensdorf<br/>
			Schweiz<br/><br/>
			DeinHandy.ch ist ein Unternehmen<br/>
			der mobilezone Gruppe<br/><br/>
			<strong>Kontakt</strong><br/>
			info@deinhandy.ch<br/>
			043 388 77 99 (Mo.-Fr. 08:00 – 12:00 / 13:00 – 18:00 Uhr)<br/>
			<a href="https://www.deinhandy.ch" style="color:#aaaaaa">www.deinhandy.ch</a><br/>
		</td>
	</tr>
	</tbody>
</table>
