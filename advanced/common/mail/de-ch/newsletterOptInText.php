Hallo,

vielen Dank für Dein Interesse am deinHandy-Newsletter.

Um die Anmeldung abzuschliessen und Missbrauch Deiner Email-Adresse (<?php echo $subscriber->email; ?>) vorzubeugen ist es notwendig deine Eintragung mit einem Klick auf diesen Link zu bestätigen:

https://www.deinhandy.ch/site/newsletter-confirm?id=<?php echo urlencode($subscriber->id); ?>&email=<?php echo urlencode($subscriber->email); ?>

Bei eventuell bestehenden Rückfragen stehen wir Dir gerne zur Verfügung.

Mit freundlichen Grüßen
Dein Team von deinHandy

mobilezone ag
DEINHANDY.CH
Riedthofstrasse 124
8105 Regensdorf
Schweiz

DeinHandy.ch ist ein Unternehmen der mobilezone Gruppe

*Kontakt*
info@deinhandy.ch
043 388 77 99 (Mo.-Fr. 08:00 – 12:00 / 13:00 – 18:00 Uhr)
https://www.deinhandy.ch

