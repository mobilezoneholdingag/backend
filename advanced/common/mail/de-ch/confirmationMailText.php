*Bestellbestätigung*

Liebe<?php if ($data->customer_gender == 2) echo 'r'; ?> <?= $data->customer_prename; ?> <?= $data->customer_surname; ?>,

vielen Dank für Deine Bestellung bei *DeinHandy*.

In dieser Mail haben wir für Dich noch einmal alle Informationen zu Deiner Bestellung aufgelistet:

*Smartphone*
<?= $article->manufacturer->title ?> <?= $article->product_name ?> <?= $article->color_label ?>

Einmalige Kosten: <?php if (isset($data->tariff_device_final_price) && $data->tariff_device_final_price > 0) { ?>CHF <?= number_format($data->tariff_device_final_price, 2, ',', '.') ?><?php } else if (isset($data->tariff_device_final_price) && $data->tariff_device_final_price == 0 && $data->coupon_id) { ?>CHF <?= number_format($data->tariff_device_final_price, 2, ',', '.') ?><?php } else { ?>CHF 1,00 - CHF 1,00 Rabatt = CHF 0,00 <?php } ?> Gerätezuzahlung


*Tariff*
<?= $tariff->provider->title ?> <?= $tariff->title ?>

Vertragslaufzeit: <?= $tariff->contract_duration ?> Monate
Monatliche Kosten: <?php if ($data->tariff_monthly_special_discount) { ?>CHF <?= number_format($data->tariff_monthly_price, 2, ',', '.') ?>CHF <?= number_format($data->tariff_monthly_special_discount, 2, ',', '.') ?>Rabatt = CHF <?= number_format($data->tariff_monthly_price_after_special_discount, 2, ',', '.') ?>
<?php } else { ?>CHF <?= number_format($data->tariff_monthly_price, 2, ',', '.') ?><?php } ?>

*Versandkosten*

*Gesamtpreis* (inkl. 8% MwSt.):
Monatliche Kosten: CHF <?= number_format($data->total_monthly, 2, ',', '.') ?>
Einmalige Kosten: CHF <?= number_format($data->total_once, 2, ',', '.') ?>


Im Anhang dieser Email findest Du des Weiteren unsere Allgemeinen
Geschäftsbedingungen und die Widerrufsbelehrung.

Bei eventuell bestehenden Rückfragen stehen wir Dir gerne zur Verfügung.

*Wir wünschen Dir viel Spaß mit Deinem neuen Handy!*

Mit freundlichen Grüßen
Dein Team von deinHandy

mobilezone ag
DEINHANDY.CH
Riedthofstrasse 124
8105 Regensdorf
Schweiz

DeinHandy.ch ist ein Unternehmen der mobilezone Gruppe

*Kontakt*
info@deinhandy.ch
043 388 77 99 (Mo.-Fr. 08:00 – 12:00 / 13:00 – 18:00 Uhr)
https://www.deinhandy.ch
