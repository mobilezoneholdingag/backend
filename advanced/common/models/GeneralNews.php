<?php

namespace common\models;
use components\i18n\TranslatableActiveRecord;

use Yii;

/**
 * This is the model class for table "general_news".
 *
 * @property integer $id
 * @property string $date
 * @property string $name
 * @property string $link
 */
class GeneralNews extends TranslatableActiveRecord
{
    protected $translateAttributes = [
        'name',
        'link'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'general_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return parent::translatableRules([
            ['date', 'safe'],
            [['name', 'link'], 'string', 'max' => 255],
        ]);
    }

    public function afterValidate()
    {
        parent::afterValidate();

        $this->date = strtotime($this->date);

        if($this->date == 0) {
            $this->date = '';
        }

    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Datum'),
            'name' => Yii::t('app', 'Name'),
            'link' => Yii::t('app', 'Link')
        ];
    }
}
