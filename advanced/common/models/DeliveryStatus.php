<?php

namespace common\models;

use common\models\Article as Articles;
use components\i18n\TranslatableActiveRecord;
use sammaye\audittrail\LoggableBehavior;
use Yii;

/**
 * This is the model class for table "delivery_status".
 *
 * @property integer $id
 * @property string $label
 * @property string $css_class
 * @property \DateInterval|int $due
 *
 * @property Articles[] $articles
 */
class DeliveryStatus extends TranslatableActiveRecord
{
	const ID_IN_STOCK = 1;
	const ID_REORDERED = 2;
	const ID_NOT_IN_STOCK = 3;

	protected $translateAttributes = ['label'];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'delivery_status';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules([
			[['label', 'css_class'], 'required'],
			[['label'], 'string', 'max' => 25],
			[['due'], 'string', 'max' => 80],
			[['css_class'], 'string', 'max' => 255]
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'label' => 'Label',
			'css_class' => 'CSS Class',
			'due' => 'Due',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getArticles()
	{
		return $this->hasMany(Articles::className(), ['delivery_status_id' => 'id']);
	}

	/**
	 * @return \DateInterval|int  Returns -1, if it's a preorder.
	 */
	public function getDue()
	{
		$due = $this->getAttribute('due');
		if ($due == 'preorder') {
			return -1;
		} elseif (!empty($due)) {
			return new \DateInterval($due);
		} else {
			return false;
		}
	}

	/**
	 * @param $article
	 * @param $tariff
	 * @return null|static
	 */
	public static function getDeliveryStatusIdFromArticleTariff($article, $tariff)
	{
		$deliveryStatusId = $article->delivery_status_id;
		/**
		 * @var $articleTariff ArticleTariffs
		 */
		$articleTariffModel = ArticleTariffs::find()->where(['article_id' => $article->id, 'tariff_id' => $tariff->id])->one();

		if (isset($articleTariffModel->delivery_status_id)) {
			$deliveryStatusId = $articleTariffModel->delivery_status_id;
		}

		return $deliveryStatusId;
	}
}
