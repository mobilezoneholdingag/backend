<?php

namespace common\models;

use common\modules\order\models\Item;
use DateInterval;
use DateTimeImmutable;
use DateTimeZone;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "stock".
 *
 * @property int $id
 * @property int $quantity
 * @property string $next_delivery_date
 * @property string $next_delivery_amount
 * @property string $status
 *
 * @property Article $article
 * @property ProductCategory $productCategory
 */
class Stock extends ActiveRecord
{
	const STATUS_IMMIDIATELY_READY_FOR_DISPATCH = 'stock_status_available';
	const STATUS_IN_REORDER = 'stock_status_in_reorder';
	const STATUS_NOT_IN_STOCK = 'stock_status_not_available';
	const STATUS_PREORDERABLE = 'stock_status_in_preorder';

	public static function stockValues(){
	    return [
            self::STATUS_IN_REORDER => Yii::t('app', self::STATUS_IN_REORDER),
            self::STATUS_IMMIDIATELY_READY_FOR_DISPATCH => Yii::t('app', self::STATUS_IMMIDIATELY_READY_FOR_DISPATCH),
            self::STATUS_NOT_IN_STOCK => Yii::t('app', self::STATUS_NOT_IN_STOCK),
            self::STATUS_PREORDERABLE => Yii::t('app', self::STATUS_PREORDERABLE),
        ];
    }
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'is_active' => Yii::t('app', 'Is Active'),
			'product_name' => Yii::t('app', 'Product Name'),
		];
	}

	public function fields()
	{
		return [
			'id',
			'status',
		];
	}

	/**
	 * Implemented according to spec as agreed upon with MZ. See
	 * https://deinhandy.atlassian.net/wiki/display/STAR/ERP-XML and the part "inventories.xml" for details.
	 *
	 * @return null|string
	 */
	public function getDeliveryDate()
	{
		if ($this->getQuantityMinusOrderedQuantity() > 0) {
			$nowInSwitzerland = new DateTimeImmutable('now', new DateTimeZone('Europe/Zurich'));
			if ($nowInSwitzerland->format('H') < 16) {
				$deliveryDate = $nowInSwitzerland->add(new DateInterval('P1D'));
			} else {
				$deliveryDate = $nowInSwitzerland->add(new DateInterval('P2D'));
			}

			return $deliveryDate->format('Y-m-d');
		}
		if (empty($this->next_delivery_date) === false) {
			$nextDeliveryDate = new DateTimeImmutable($this->next_delivery_date, new DateTimeZone('Europe/Zurich'));
			$deliveryDate = $nextDeliveryDate->add(new DateInterval('P2D'));

			return $deliveryDate->format('Y-m-d');
		}

		return null; // No delivery date could be determined, results in "not in stock"
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		if($this->isPreorderable()) {
			return self::STATUS_PREORDERABLE;
		}

		if($this->article->override_positive_stock_to_reorder && !empty($this->article->custom_stock)){
            return $this->article->custom_stock;
        }

        if ($this->quantity > 0) {
            if (empty($this->article->override_positive_stock_to_reorder)) {
                return self::STATUS_IMMIDIATELY_READY_FOR_DISPATCH;
            } else {
                return self::STATUS_IN_REORDER;
 			}
        } else {
            if (empty($this->next_delivery_date)) {
                return self::STATUS_NOT_IN_STOCK;
            } else {
                return self::STATUS_IN_REORDER;
            }
        }
	}

	/**
	 * Substracts all ordered items since the last (suspected) stock import from the stored quantity in order to get a
	 * "status quo quantity".
	 *
	 * @return int
	 */
	private function getQuantityMinusOrderedQuantity()
	{
		$now = new DateTimeImmutable();
		$lastStockImportTime = $now;
		// Stock import happens always 10 minutes after the full hour, thats why we substract one hour if under 10 mins
		if ($now->format('i') < 10) {
			$lastStockImportTime = $now->sub(new DateInterval('PT1H'));
		}

		// This query matches an index over all queried attributes; be sure to maybe adjust it if you change the query
		$itemQuery = Item::find();
		// Stock import happens always 10 minutes after the full hour, thats why the minutes and seconds are hardcoded
		$itemQuery->andWhere(['>=', 'created_at', $lastStockImportTime->format('Y-m-d H:10:00')]);
		$itemQuery->andWhere(['=', 'item_product_type_id', 'article']);
		$itemQuery->andWhere(['=', 'item_id', $this->id]);
		$soldItemCount = $itemQuery->count();

		return ($this->quantity - $soldItemCount);
	}

	/**
	 * @return \yii\db\ActiveQuery|Article
	 */
	public function getArticle()
	{
		return $this->hasOne(Article::class, ['id' => 'id']);
	}

	public function isPreorderable()
	{
		$article = Article::findOne(['id' => $this->id]);
		$groupId = $article->group_id;
		$articleGroup = ArticleGroups::findOne(['id' => $groupId]);

		return $articleGroup->preorder ?? null;
	}

	public function isDispatchable()
	{
		return $this->status === self::STATUS_IMMIDIATELY_READY_FOR_DISPATCH;
	}
}
