<?php

namespace common\models;

use common\modules\platform\models\Platform;
use sammaye\audittrail\LoggableBehavior;
use Yii;

/**
 * This is the model class for table "operating_systems".
 *
 * @property integer $id
 * @property string $title
 * @property string $version
 * @property integer $platform_id
 * @property Platform $platform
 */
class OperatingSystems extends \yii\db\ActiveRecord
{
	const PLATFORM_ANDROID = 1;
	const PLATFORM_WINDOWS = 2;
	const PLATFORM_IOS     = 3;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'operating_systems';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'version', 'platform_id'], 'required'],
			[['platform_id'], 'integer'],
			[['title', 'version'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'version' => Yii::t('app', 'Version'),
			'platform_id' => Yii::t('app', 'Platform ID'),
		];
	}

	public function getPlatform()
	{
		return $this->hasOne(Platform::className(), ['id' => 'platform_id']);
	}
}
