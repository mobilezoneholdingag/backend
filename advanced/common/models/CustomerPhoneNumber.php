<?php

namespace common\models;

use sammaye\audittrail\LoggableBehavior;
use common\models\behaviors\TimestampBehaviorSettings;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "customer_phone_number".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $number
 * @property bool $wait_until_contract_ends
 * @property integer $type
 * @property string $provider
 * @property string $updated_at
 * @property string $created_at
 * @property Customer $customer
 */
class CustomerPhoneNumber extends ActiveRecord
{
	const TYPE_SUBSCRIPTION = 1;
	const TYPE_PREPAID = 2;

	const TYPES = [
		self::TYPE_SUBSCRIPTION => 'Number_Type_Subscription',
		self::TYPE_PREPAID => 'Number_Type_Prepaid',
	];

	protected $porting = false;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'customer_phone_number';
	}

	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['customer_id'], 'required'],
			[['customer_id', 'number', 'type'], 'integer'],
			[['wait_until_contract_ends'], 'boolean'],
			[['updated_at', 'created_at'], 'safe'],
			[['provider'], 'string', 'max' => 100],
			[['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'customer_id' => 'Customer ID',
			'number' => 'Aktuelle Rufnummer',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
			'wait_until_contract_ends' => 'Portierung zum',
			'contractType' => 'Aktuelle Vertragsart',
			'providerTitle' => 'Aktueller Anbieter',
		];
	}

	public function getCustomer(): ActiveQuery
	{
		return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}

	public function getProviderTitle()
	{
		return $this->provider ? Provider::findOne($this->provider)->title : null;
	}

	public function getContractType()
	{
		if (!array_key_exists($this->type, self::TYPES)) {
			return null;
		}
		return Yii::t('app', self::TYPES[$this->type]);
	}

	public function setWaitUntilContractEnds(bool $waitUntilContractEnds)
	{
		$this->wait_until_contract_ends = $waitUntilContractEnds;
	}

	public function setPorting($porting)
	{
		$this->porting = (bool)$porting;
	}

	public function getPorting()
	{
		return (bool)$this->porting;
	}
}
