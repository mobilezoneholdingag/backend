<?php

namespace common\models;

use backend\models\Certification;
use backend\models\ShopCertification;
use backend\models\ShopLanguage;
use backend\models\ShopSpecial;
use backend\models\SpeakLanguage;
use backend\models\Special;
use common\behaviors\DateBehavior;
use components\i18n\TranslatableActiveRecord;
use deinhandy\yii2picasso\behavior\ImageAttributeBehavior;
use deinhandy\yii2picasso\behavior\ImageBehaviorConfig;
use deinhandy\yii2picasso\behavior\ImageUploadBehavior;
use deinhandy\yii2picasso\models\Image;
use sammaye\audittrail\LoggableBehavior;
use Yii;

/**
 * This is the model class for table "shop".
 *
 * @property integer $id
 * @property string $name
 * @property integer $shop_tree_id
 * @property integer $shop_tree_name
 * @property string $street
 * @property string $extra_line
 * @property string $zip
 * @property string $city
 * @property string $canton
 * @property double $latitude
 * @property double $longitude
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $sort
 * @property string $state
 * @property string $manager_name
 * @property string $manager_function
 * @property integer $is_helpcenter
 * @property ShopHour[] $shopHours
 * @property Image[] $shopImages
 * @property Image $manager_image
 * @property Image $shop_image_1
 * @property Image $shop_image_2
 * @property Image $shop_image_3
 * @property string $closure_date
 * @property string $opening_date
 * @property string $special_opening
 * @property ShopCertification[] $shopCertifications
 * @property Certification[] $certifications
 * @property ShopLanguage[] $shopLanguages
 * @property SpeakLanguage[] $languages
 * @property ShopSpecial[] $shopSpecials
 * @property Special[] $specials
 */
class Shop extends TranslatableActiveRecord
{
	const IMAGE_FIELDS = [
		'manager_image',
		'shop_image_1',
		'shop_image_2',
		'shop_image_3',
	];
	protected $translateAttributes = [
		'name',
		'manager_function',
		'description',
        'special_opening',
	];
	public $distance;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'shop';
	}

	public function behaviors()
	{
		$accountNamespace = Yii::$app->params['picasso_namespace'];
		$collectionName = self::tableName();

		$attributes = [];
		$imageBehaviorConfig = new ImageBehaviorConfig($accountNamespace, $collectionName);

		foreach (self::IMAGE_FIELDS as $field) {
			$attributes[$field] = $imageBehaviorConfig;
		}

		return array_merge(
			[
				[
					'class' => DateBehavior::className(),
					'attributes' => ['closure_date', 'opening_date'],
				],
				'image_upload' => [
					'class' => ImageUploadBehavior::class,
					'attributes' => $attributes,
				],
				'image_access' => [
					'class' => ImageAttributeBehavior::class,
					'attributes' => self::IMAGE_FIELDS,
				],
				LoggableBehavior::className(),
			],
			parent::behaviors()
		);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules(
			[
				[
					[
						'shop_tree_id',
						'shop_tree_name',
						'is_helpcenter',
					],
					'integer',
				],
				[['latitude', 'longitude'], 'number'],
				[['opening_date'], 'required'],
				[['closure_date', 'opening_date'], 'date'],
				[['closure_date'], 'default', 'value' => null],
				[
					[
						'name',
						'street',
						'extra_line',
						'zip',
						'city',
						'canton',
						'phone',
						'fax',
						'email',
						'sort',
						'state',
						'manager_name',
						'manager_function',
					],
					'string',
					'max' => 255,
				],
				[['description', 'special_opening'], 'string'],
				[
					self::IMAGE_FIELDS,
					'file',
					'skipOnEmpty' => true,
					'extensions' => ['png', 'jpg', 'jpeg'],
					'wrongExtension' => Yii::t('app', 'wrong extension - only PNG and JP(E)G allowed'),
				],
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'shop_tree_id' => 'Shop Tree ID',
			'shop_tree_name' => 'Shop Tree Name',
			'street' => 'Street',
			'extra_line' => 'Extra Line',
			'zip' => 'Zip',
			'city' => 'City',
			'canton' => 'Canton',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'email' => 'Email',
			'sort' => 'Sort',
			'state' => 'State',
			'manager_name' => 'Manager Name',
			'manager_function' => 'Manager Function',
			'is_helpcenter' => 'Is Helpcenter',
			'closure_date' => 'Closure Date',
			'opening_date' => 'Opening Date',
            'special_opening' => 'Spezielle Öffnungszeiten',
		];
	}

	public function getShopHours(): array
	{
		return $this->hasMany(ShopHour::className(), ['shop_id' => 'id'])->select(['id'])->column();
	}

	public function getShopLanguages()
	{
		return $this->hasMany(ShopLanguage::className(), ['shop_id' => 'id']);
	}

	public function getLanguages()
	{
		return $this->hasMany(SpeakLanguage::className(), ['id' => 'language_id'])->viaTable(
			'shop_language',
			['shop_id' => 'id']
		);
	}

	public function getShopCertifications()
	{
		return $this->hasMany(ShopCertification::className(), ['shop_id' => 'id']);
	}

	public function getCertifications()
	{
		return $this->hasMany(Certification::className(), ['id' => 'certification_id'])->viaTable(
			'shop_certification',
			['shop_id' => 'id']
		);
	}

	public function getShopSpecials()
	{
		return $this->hasMany(ShopSpecial::className(), ['shop_id' => 'id']);
	}

	public function getSpecials()
	{
		return $this->hasMany(Special::className(), ['id' => 'special_id'])->viaTable(
			'shop_special',
			['shop_id' => 'id']
		);
	}

	public function getShopImages(): array
	{
		$result = [];

		foreach (static::IMAGE_FIELDS as $attribute) {
			$result[$attribute] = $this->getAttribute($attribute);
		}

		return $result;
	}

	public function fields()
	{
		return [
			'id',
			'name',
			'description',
			'street',
			'extra_line',
			'zip',
			'city',
			'latitude',
			'longitude',
			'phone',
			'manager_name',
			'manager_function',
			'is_helpcenter',
            'special_opening',
			'shop_hours' => 'shopHours',
			'distance',
			'manager_image',
			'shop_image_1',
			'shop_image_2',
			'shop_image_3',
			'specials' => 'specials',
			'languages' => 'languages',
			'certifications' => 'certifications',
		];
	}
}
