<?php

namespace common\models;

use components\i18n\TranslatableActiveRecord;
use Yii;

/**
 * This is the model class for table "tariff_special".
 *
 * @property integer $id
 * @property string $type
 * @property string $name
 * @property double $price_change
 * @property string $infotext
 * @property string $infotext_2
 * @property string $custom_infotext_tariff
 * @property string $custom_infotext_cart
 * @property string $short_name
 * @property int    $filter_type
 */
class TariffSpecial extends TranslatableActiveRecord
{
	protected $translateAttributes = [
		'infotext',
		'infotext_2',
		'custom_infotext_tariff',
		'custom_infotext_cart',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tariff_special';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules([
			[['price_change'], 'number'],
			[['filter_type'], 'integer'],
			[['type', 'name', 'infotext', 'infotext_2', 'custom_infotext_tariff', 'custom_infotext_cart', 'short_name'], 'string', 'max' => 255],
			['short_name', 'match', 'pattern' => '/^\w[\d\w\-_]+$/'],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'type' => Yii::t('app', 'Type'),
			'name' => Yii::t('app', 'Name'),
			'short_name' => Yii::t('app', 'Shorthand Name'),
			'price_change' => Yii::t('app', 'Price Change'),
			'infotext' => Yii::t('app', 'Infotext'),
			'infotext_2' => Yii::t('app', 'Infotext 2'),
			'custom_infotext_tariff' => Yii::t('app', 'Custom Infotext Tariff'),
			'custom_infotext_cart' => Yii::t('app', 'Custom Infotext Cart'),
			'filter_type' => Yii::t('app', 'Filter Type')
		];
	}

	/**
	 * Get a human-readable EUR-like price string. *-1 is to convert negative value to positive.
	 *
	 * @return string
	 */
	public function getTariffSpecialPriceString()
	{
		return number_format($this->price_change * -1, 2, ',', '.');
	}

	public function getUniqueTariffSpecial() {
		return $this->name.' ('.$this->price_change.' €)';
	}

	/**
	 * Returns a list of types which can be filtered for.
	 *
	 * @return array
	 */
	static public function getAvailableFilterTypes()
	{
		return Yii::$app->params['filter']['TariffSpecial.types'];
	}

	/**
	 * Returns the name of the selected filter type.
	 *
	 * @return string
	 */
	public function getFilterTypeName()
	{
		$filterValues = self::getAvailableFilterTypes();

		if (isset($filterValues[$this->filter_type]))
			return $filterValues[$this->filter_type];

		return '';
	}


}
