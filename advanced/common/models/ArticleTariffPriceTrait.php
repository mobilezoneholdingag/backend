<?php

namespace common\models;

use common\modules\pricing\models\ArticlePrice;
use common\modules\pricing\models\ConsolidatedPrice;
use common\modules\pricing\models\interfaces\PricingStrategyInterface;
use common\modules\pricing\models\Price;
use ReflectionException;
use Yii;

/**
 * @property PricingStrategyInterface $pricingStrategy
 */
trait ArticleTariffPriceTrait
{
	/**
	 * Overview of all prices of this article tariff, both monthly and once for both article and tariff.
	 * Totals as well
	 *
	 * @return ConsolidatedPrice[]
	 */
	public function getPrices()
	{
		$articlePrice = new ArticlePrice();
		$articlePrice->getPriceOnce()->add($this->getArticlePriceOnce());
		$articlePrice->getPriceMonthly()->add($this->getArticlePriceMonthly());
		$articlePrice->getPriceFirstMonth()->add($this->getArticlePriceFirstMonth());
		$articlePrice->getPriceTotal()->add($this->getArticlePriceTotal());

		$tariffPrice = new ConsolidatedPrice();
		$tariffPrice->getPriceOnce()->add($this->getTariffPriceOnce());
		$tariffPrice->getPriceMonthly()->add($this->getTariffPriceMonthly());

		$total = new ConsolidatedPrice();
		$total->getPriceOnce()->add($this->getPriceOnce());
		$total->getPriceMonthly()->add($this->getPriceMonthly());

		return [
			'article' => $articlePrice,
			'tariff' => $tariffPrice,
			'total' => $total,
		];
	}

	/**
	 * @return $this
	 */
	public function updateCachedPrices()
	{
		$this->price_article_once = $this->getArticlePriceOnce()->getValue();
		$this->price_article_monthly = $this->getArticlePriceMonthly()->getValue();
		$this->price_tariff_once = $this->getTariffPriceOnce()->getValue();
		$this->price_tariff_monthly = $this->getTariffPriceMonthly()->getValue();
		$this->price_article_first_month = $this->getArticlePriceFirstMonth()->getValue();
		$this->price_article_total = $this->getArticlePriceTotal()->getValue();

		return $this;
	}

	/**
	 * Price once for both the article and the tariff.
	 */
	public function getPriceOnce() : Price
	{
		return $this->getPricingStrategy()->getPriceOnce();
	}

	/**
	 * Price once for the article.
	 */
	public function getArticlePriceOnce() : Price
	{
		return $this->getPricingStrategy()->getArticlePriceOnce();
	}

	/**
	 * Price once for tariff.
	 */
	public function getTariffPriceOnce() : Price
	{
		return $this->getPricingStrategy()->getTariffPriceOnce();
	}

	/**
	 * Price monthly for both the article and the tariff.
	 */
	public function getPriceMonthly() : Price
	{
		return $this->getPricingStrategy()->getPriceMonthly();
	}

	/**
	 * Mobilezone-customized price calculation for monthly article price.
	 * @see https://deinhandy.atlassian.net/browse/MZ-244 for details.
	 */
	public function getArticlePriceMonthly() : Price
	{
		return $this->getPricingStrategy()->getArticlePriceMonthly();
	}

	/**
	 * Price monthly for the tariff.
	 */
	public function getTariffPriceMonthly() : Price
	{
		return $this->getPricingStrategy()->getTariffPriceMonthly();
	}

	/**
	 * Return first month`s device price.
	 *
	 * @return Price
	 */
	public function getArticlePriceFirstMonth(): Price
	{
		return $this->getPricingStrategy()->getArticlePriceFirstMonth();
	}

	/**
	 * Return device`s total price.
	 */
	public function getArticlePriceTotal(): Price
	{
		return $this->getPricingStrategy()->getArticlePriceTotal();
	}

	protected function getPricingStrategy(): PricingStrategyInterface
	{
		if (null === $this->pricingStrategy || !($this->pricingStrategy instanceof PricingStrategyInterface)) {
			try {
				$this->pricingStrategy = di('pricing.' . $this->tariff->provider->pricing_strategy, [$this]);
			} catch (ReflectionException $e) {
				Yii::getLogger()->log(
					'ArticleTariff[ID:' . $this->tariff_id . '].provider[ID:'
					. $this->tariff->provider_id . '] has no matching pricing_strategy set!',
					LOG_ERR
				);
			}
		}

		return $this->pricingStrategy;
	}
}
