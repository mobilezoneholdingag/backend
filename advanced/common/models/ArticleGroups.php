<?php

namespace common\models;


use common\modules\insurances\models\ArticleGroupsInsurance;
use common\modules\set\models\Sets;
use components\i18n\TranslatableActiveRecord;
use deinhandy\yii2picasso\behavior\ImageAttributeBehavior;
use deinhandy\yii2picasso\behavior\ImageBehaviorConfig;
use deinhandy\yii2picasso\behavior\ImageUploadBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use deinhandy\yii2picasso\models\Image as PicassoImage;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "article_groups".
 *
 * @property integer $id
 * @property string $title
 * @property integer $tariff_set_id
 * @property integer $start_tariff_id
 * @property integer $top_variation_id
 * @property integer $accessory_set_id
 * @property integer $manufacturer_id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $footnote_text
 * @property string $frontend_text
 * @property bool $is_bundle
 * @property string $description_1
 * @property string $description_2
 * @property string $description_3
 * @property string $seo_text_1
 * @property string $seo_text_2
 * @property string $seo_text_3
 * @property string $seo_text_4
 * @property integer $product_category_id
 * @property string $short_name
 * @property integer $os_id
 * @property integer $preorder
 *
 * @property Article $simCard
 * @property Tariffs $startTariff
 * @property Sets $tariffSet
 * @property Manufacturer $manufacturer
 * @property Article $topVariation
 * @property boolean $isTopVariation
 * @property Article[] $articles
 * @property string $manufacturerTitle
 * @property string $startTariffTitle
 * @property string $tariffSetTitle
 * @property string $accessorySetTitle
 * @property string $topVariationTitle
 * @property string $fullName
 * @property ProductCategory $productCategory
 * @property OperatingSystems $operatingSystem
 * @property string $url
 * @property string $article_feed_description
 * @property ArticleGroupsInsurance $insurances
 * @property Sets $accessorySet
 * @property array $insuranceIds
 * @property ArticleGroupsTariff[] $articleGroupTariffs
 * @property Article $firstActiveArticle
 * @property PicassoImage $marketing_image
 * @property integer $show_marketing_image
 */
class ArticleGroups extends TranslatableActiveRecord
{
	public $articleGroupTariffs;

	protected $translateAttributes = [
		'footnote_text',
		'article_feed_description',
		'description_1',
		'description_2',
		'description_3',
		'frontend_text',
		'meta_title',
		'meta_description',
		'seo_text_1',
		'seo_text_2',
		'seo_text_3',
		'seo_text_4',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'article_groups';
	}

	/**
	 * @param int|string $manufacturer Title or Id of Manufacturer
	 * @param bool $activeOnly
	 * @return array|\yii\db\ActiveRecord[]
	 */
	static public function getFromManufacturer($manufacturer, $activeOnly = false)
	{
		if (is_int($manufacturer) == false && is_string($manufacturer) == false) {
			return [];
		}

		/** @var Manufacturer $manufacturer */
		$manufacturer = Manufacturer::find()->where(['or', ['title' => $manufacturer], ['id' => $manufacturer]])->one();

		$devices = [];

		if (empty($manufacturer) == false) {
			$deviceGroups = self::find()->where(['manufacturer_id' => $manufacturer->id, 'is_bundle' => 0])->all();

			/**
			 * @var ArticleGroups $deviceGroup
			 */
			foreach ($deviceGroups as $deviceGroup) {
				$topVariation = $deviceGroup->topVariation;

				if (empty($topVariation) || $topVariation->is_active == false) {
					continue;
				}

				$devices[$topVariation->id] = $topVariation->shortName;
			}
		}

		return $devices;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules([
			[['title', 'short_name', 'os_id', 'manufacturer_id', 'description_1', 'description_2', 'description_3',
				'seo_text_1', 'seo_text_2', 'seo_text_3', 'seo_text_4'], 'required'],
			[['tariff_set_id', 'start_tariff_id', 'product_category_id', 'manufacturer_id', 'os_id', 'is_bundle',
				'top_variation_id', 'accessory_set_id', 'show_marketing_image', 'preorder'], 'integer'],
			[['title', 'short_name'], 'string', 'max' => 64],
			[['meta_title'], 'string', 'max' => 49],
			[['article_feed_description'], 'string', 'max' => 60000],
			[['meta_description', 'frontend_text', 'description_1', 'description_2', 'description_3',
				'seo_text_1', 'seo_text_2', 'seo_text_3', 'seo_text_4'], 'string', 'max' => 60000],
			[['footnote_text'], 'string'],
			[['title', 'short_name'], 'unique'],
			[['accessory_set_id'], 'required', 'message' => Yii::t('app', 'accessory_set_empty')],
			[['product_category_id'], 'required', 'message' => Yii::t('app', 'product_category_empty')],
			[['marketing_image'], 'file', 'skipOnEmpty' => true, 'extensions'=>['png'], 'wrongExtension' => Yii::t('app', 'wrong extension - only PNG allowed')],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Group Notation'),
			'short_name' => Yii::t('app', 'Short Name'),
			'tariff_set_id' => Yii::t('app', 'Tariff Set'),
			'start_tariff_id' => Yii::t('app', 'Start Tariff'),
			'top_variation_id' => Yii::t('app', 'Top Variation'),
			'accessory_set_id' => Yii::t('app', 'Accessory Set ID'),
			'accessorySetTitle' => Yii::t('app', 'Accessory Set'),
			'manufacturer_id' => Yii::t('app', 'Manufacturer'),
			'manufacturerTitle' => Yii::t('app', 'Manufacturer Title'),
			'startTariffTitle' => Yii::t('app', 'Start Tariff Title'),
			'tariffSetTitle' => Yii::t('app', 'Tariff Set Title'),
			'topVariationTitle' => Yii::t('app', 'Top Variation Title'),
			'footnote_text' => Yii::t('app', 'Footnote Text'),
			'frontend_text' => Yii::t('app', 'Frontend Text'),
			'is_bundle' => Yii::t('app', 'Bundle'),
			'os_id' => Yii::t('app', 'Operating System'),
			'product_category_id' => Yii::t('app', 'Product Category'),
			'description_1' => Yii::t('app', 'Description') . ' 1',
			'description_2' => Yii::t('app', 'Description') . ' 2: Highlights auf PDP',
			'description_3' => Yii::t('app', 'Description') . ' 3',
			'seo_text_1' => 'Seo Text 1',
			'seo_text_2' => 'Seo Text 2',
			'seo_text_3' => 'Seo Text 3',
			'seo_text_4' => 'Seo Text 4',
			'marketing_image' => Yii::t('app', 'Artikel Gruppe Marketing Banner (Upload/Update)'),
			'show_marketing_image' => Yii::t('app', 'Auf PDP-Seite anzeigen'),
			'preorder' => Yii::t('app', 'Preorder'),
		];
	}

	public function behaviors()
	{
		$accountNamespace = Yii::$app->params['picasso_namespace'];

		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image_upload' => [
					'class' => ImageUploadBehavior::class,
					'attributes' => [
						'marketing_image' => new ImageBehaviorConfig($accountNamespace, 'marketing-images'),
					],
				],
				'image_access' => [
					'class' => ImageAttributeBehavior::class,
					'attributes' => [
						'marketing_image',
					],
				],
			]
		);
	}

	/**
	 * @param null $providerId
	 *
	 * @return ActiveRecord|Article|null
	 */
	public function getSimCard($providerId = null)
	{
		$simId = ArticleGroupsSim::find()
				->select(['simcard_id'])
				->where([
					'provider_id' => $providerId,
					'article_group_id' => $this->id
				])
				->scalar()
			?? null;

		if (empty($simId)) {
			return null;
		}

		return Article::find()->where(['id' => $simId])->one();
	}

	/**
	 * Adds related models as fields.
	 * @return array
	 */
	public function fields()
	{
		return [
			'id',
			'title',
			'short_name',
			'description_1',
			'description_2',
			'accessory_set_id',
			'insurances' => 'insuranceIds',
			'articles_in_group' => 'articlesInGroup',
			'show_marketing_image',
			'marketing_image',
			'preorder',
		];
	}

	/**
	 * @return ActiveQuery|ArticleGroupsTariff[]
	 */
	public function getArticleGroupTariffs()
	{
		return $this->hasMany(ArticleGroupsTariff::className(), ['article_group_id' => 'id']);
	}

	/**
	 * @return ActiveQuery|Article[]
	 */
	public function getArticles()
	{
		return $this->hasMany(Article::className(), ['group_id' => 'id'])->orderBy('memory');
	}

	/**
	 * @return ActiveQuery|Tariffs
	 */
	public function getStartTariff()
	{
		return $this->hasOne(Tariffs::className(), ['id' => 'start_tariff_id']);
	}

	/**
	 * @return ActiveQuery|Sets
	 */
	public function getTariffSet()
	{
		return $this->hasOne(Sets::className(), ['id' => 'tariff_set_id']);
	}


	/**
	 * @return ActiveQuery|Sets
	 */
	public function getAccessorySet()
	{
		return $this->hasOne(Sets::className(), ['id' => 'accessory_set_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery | Article
	 */
	public function getTopVariation()
	{
		return $this->hasOne(Article::className(), ['id' => 'top_variation_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery | Manufacturer
	 */
	public function getManufacturer()
	{
		return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
	}

	/**
	 * Finds all models of group with different memory sizes
	 * @return Article[]
	 */
	public function getMemoryVariations()
	{
		return Article::find()->where(['group_id' => $this->id, 'is_active' => true])->select('memory')->distinct()->all();
	}

	/**
	 * Finds all models of group with different colors
	 * @return Article[]
	 */
	public function getColorVariations()
	{
		return Article::find()->where(['group_id' => $this->id, 'is_active' => true])->select('color, color_label')->distinct()->all();
	}

	/**
	 * @return string
	 */
	public function getManufacturerTitle()
	{
		return $this->manufacturer->title;
	}

	/**
	 * @return string
	 */
	public function getTariffSetTitle()
	{
		return $this->tariffSet->title;
	}

	/**
	 * @return string
	 */
	public function getAccessorySetTitle()
	{
		return $this->accessorySet->title ?? null;
	}

	/**
	 * @return string
	 */
	public function getStartTariffTitle()
	{
		return $this->startTariff->title ?? null;
	}

	/**
	 * @return string
	 */
	public function getTopVariationTitle()
	{
		return $this->topVariation->color_label . ', ' . $this->topVariation->memory . ' GB';
	}

	/**
	 * @return string
	 */
	public function getFullName()
	{
		return $this->manufacturerTitle . ' ' . $this->title;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductCategory()
	{
		return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
	}

	public function getOperatingSystem()
	{
		return $this->hasOne(OperatingSystems::className(), ['id' => 'os_id']);
	}

	public function getUrl()
	{
		return $this->productCategory->getUrlRepresentation() . strtolower($this->manufacturerTitle) . '/' . $this->title . '/';
	}

	/**
	 * @param Article $article
	 * @return bool
	 */
	public function isTopVariation(Article $article)
	{
		$isTopVariation = false;

		if (isset($this->topVariation) && $this->topVariation->id == $article->id)
			$isTopVariation = true;

		return $isTopVariation;
	}

	public function getInsurances()
	{
		return $this->hasMany(Article::className(), ['id' => 'insurance_id'])
			->viaTable(ArticleGroupsInsurance::tableName(), ['group_id' => 'id']);
	}

	public function getInsuranceIds() {
		$insurances = $this->hasMany(ArticleGroupsInsurance::className(), ['group_id' => 'id']);

		return $insurances->select(['insurance_id'])->column();
	}

	public function hasNoArticleGroupTariffs($value)
	{
		if (!$this->id) {
			return false;
		}
		return !is_array($value) || count(array_filter($value)) == 0;
	}

	public function getArticlesInGroup()
	{
		return $this
			->hasMany(Article::className(), ['group_id' => 'id'])
			->where(['is_active' => true])
			->where(['is_active_erp' => true])
			->orderBy('memory');
	}

	public function getFirstActiveArticle()
	{
		$query = $this
			->getArticles()
			->joinWith(['stock'])
			->where(['is_active' => true, 'is_active_erp' => true])
			->andWhere(['>', 'quantity', 0]);

		if (!$query->exists()) {
			$query->where(['is_active' => true, 'is_active_erp' => true]);
		}

		return $query->one();
	}
}
