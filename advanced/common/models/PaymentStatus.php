<?php

namespace common\models;

use components\i18n\TranslatableActiveRecord;
use Yii;

/**
 * This is the model class for table "payment_status".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property integer $fulfillment_partner_id
 */
class PaymentStatus extends TranslatableActiveRecord
{
	protected $translateAttributes = ['title'];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'payment_status';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules([
			[['fulfillment_partner_id'], 'integer'],
			[['code', 'title'], 'string', 'max' => 255]
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'code' => Yii::t('app', 'Code'),
			'title' => Yii::t('app', 'Title'),
			'fulfillment_partner_id' => Yii::t('app', 'Fulfillment Partner ID'),
		];
	}
}
