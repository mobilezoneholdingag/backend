<?php

namespace common\models;

use components\i18n\TranslatableActiveRecord;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "tariff_highlight_details".
 *
 * @property integer $id
 * @property string $title
 * @property string $subtext
 * @property string $css_class
 * @property integer $sort
 */
class TariffHighlightDetails extends TranslatableActiveRecord
{
	const INTERNET_FLAT = 'internetflat';
	const TRAVEL_FLAT   = 'reiseflat';
	const MUSIC_FLAT    = 'musicflat';
	const LANDLINE_FLAT = 'festnetzflat';
	const MOBILE_FLAT   = 'mobilfunkflat';
	const SMS_FLAT      = 'smsflat';
	/** Mobile Number Portability */
	const MNP           = 'mnp';

	protected $translateAttributes = [
		'title',
		'subtext',
	];

	public function hasClass($class)
	{
		return $this->css_class == $class;
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tariff_highlight_details';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules([
			[['title'], 'required'],
			[['title', 'subtext', 'css_class'], 'string'],
			[['sort'], 'integer'],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'subtext' => Yii::t('app', 'Subtext'),
			'css_class' => Yii::t('app', 'CSS Class'),
			'sort' => Yii::t('app', 'Sort')
		];
	}

	/**
	 * @return ActiveQuery
	 */
	public function getHighlight()
	{
		return $this->hasOne(TariffHighlights::className(),['detail_id' => 'id']);
	}
}
