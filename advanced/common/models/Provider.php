<?php

namespace common\models;

use common\modules\set\models\Sets;
use components\i18n\TranslatableActiveRecord;
use deinhandy\yii2picasso\behavior\ImageAttributeBehavior;
use deinhandy\yii2picasso\behavior\ImageBehaviorConfig;
use deinhandy\yii2picasso\behavior\ImageUploadBehavior;
use deinhandy\yii2picasso\models\Image;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "provider".
 *
 * @property integer $id
 * @property string $title
 * @property string $system_title
 * @property integer $net_id
 * @property boolean $status
 * @property integer $set_id
 * @property string $link_tos
 * @property integer $eam_id
 * @property boolean $mnp_possible
 * @property string $mnp_text
 * @property Image $image
 * @property Image $alt_image
 * @property string $mnp_blocked_providers
 * @property string $speed_automatic_title
 * @property integer $speed_automatic_footnote
 * @property integer $minimum_legal_status_term
 * @property boolean $hardware_related_tariff_price
 * @property string $css_system_title Only On-Set attribute for valid css selectors
 * @property integer $sort
 * @property string $api_updated_at
 * @property integer $simcard_id
 * @property integer $young_age_limit
 * @property boolean $contract_renewal_direct_offer
 * @property string $rate_payment_count_strategy
 * @property string $pricing_strategy
 * @property boolean $renewable
 * @property boolean $visible_homepage
 *
 * @property Tariffs[] $tariffs
 * @property Sets $tariffSet
 * @property Net $net
 * @property Article $simCard
 */
class Provider extends TranslatableActiveRecord
{
	const IS_ACTIV = 1;
	const IS_INACTIV = 0;

	const TITLE_UPC_CABLECOM = 'upc';
	const TITLE_SWISSCOM = 'swisscom';
	const TITLE_TALKTALK = 'talktalk';
	const TITLE_SALT = 'salt';
	const TITLE_SUNRISE = 'sunrise';
	const TITLE_MTV_MOBILE = 'mtv-mobile';
	const TITLE_COOP_MOBILE = 'coopmobile';

	const SWISSCOM_ID = 2;

	protected $translateAttributes = [
		'mnp_text',
		'speed_automatic_title',
        'link'
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'provider';
	}

	public function fields()
	{
		return [
			'id',
			'title',
			'link_tos',
			'image',
			'alt_image',
			'young_age_limit',
			'renewable',
			'contract_renewal_direct_offer',
            'visible_homepage',
            'status',
            'link'
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules([
			[['system_title'], 'unique'],
			[['net_id', 'system_title'], 'required'],
			[['renewable', 'visible_homepage'], 'safe'],
			[['title', 'system_title', 'link_tos', 'mnp_text', 'mnp_blocked_providers',
				'speed_automatic_title', 'eam_id'], 'string'],
			[['net_id', 'set_id', 'status', 'mnp_possible', 'minimum_legal_status_term', 'simcard_id',
				'hardware_related_tariff_price', 'speed_automatic_footnote', 'sort', 'young_age_limit', 'contract_renewal_direct_offer'], 'integer'],
			[['image', 'alt_image'], 'file', 'extensions'=>['png'], 'wrongExtension' => Yii::t('app', 'wrong extension - only PNG allowed')]
		]);
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		$accountNamespace = Yii::$app->params['picasso_namespace'];
		$collectionName = self::tableName();

		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image_upload' => [
					'class' => ImageUploadBehavior::class,
					'attributes' => [
						'image' => new ImageBehaviorConfig($accountNamespace, $collectionName),
						'alt_image' => new ImageBehaviorConfig($accountNamespace, $collectionName),
					],
				],
				'image_access' => [
					'class' => ImageAttributeBehavior::class,
					'attributes' => [
						'image',
						'alt_image',
					],
				],
				// LoggableBehavior::className(),
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'system_title' => Yii::t('app', 'System Title'),
			'net_id' => Yii::t('app', 'Net ID'),
			'set_id' => Yii::t('app', 'Set ID'),
			'status' => Yii::t('app', 'Is Active'),
			'link_tos' => Yii::t('app', 'Link to TOS'),
			'eam_id' => Yii::t('app', 'EAM ID'),
			'mnp_possible' => Yii::t('app', 'MNP Possible'),
			'mnp_text' => Yii::t('app', 'MNP text'),
			'mnp_blocked_providers' => Yii::t('app', 'MNP Blocked Providers'),
			'speed_automatic_title' => Yii::t('app', 'Speed Automatic Title'),
			'speed_automatic_footnote' => Yii::t('app', 'Speed Automatic Footnote'),
			'minimum_legal_status_term' => Yii::t('app', 'Minimum Legal Status Term'),
			'hardware_related_tariff_price' => Yii::t('app', 'Hardware Related Tariff Price'),
			'sort' => Yii::t('app', 'Sort'),
			'young_age_limit' => Yii::t('app', 'Young Age Limit'),
			'simcard_id' => Yii::t('app', 'Default SIM Card'),
			'image' => Yii::t('app', 'Providerbild (Upload/Update)'),
			'alt_image' => Yii::t('app', 'Alternatives Providerbild (Upload/Update)'),
			'renewable' => Yii::t('app', 'Vertragsverlängerung möglich'),
            'visible_homepage' => 'Auf Frontpage sichtbar',
            'link' => 'Link',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributes()
	{
		return array_merge(
			parent::attributes(),
			[
				'css_system_title',
				'net_name'
			]
		);
	}

	/**
	 * @param string $title
	 * @return array|Provider|Provider[]
	 */
	static public function findByTitle($title = '')
	{
		if (empty($title)) {
			$results = [];
		} elseif (($results = self::findAll(['title' => $title])) == false) {
			$results = self::find()->where(['like', 'title', $title])->all();
		}

		return $results;
	}

	/**
	 * @return \yii\db\ActiveQuery|Tariffs[]
	 */
	public function getTariffs()
	{
		return $this->hasMany(Tariffs::className(), ['provider_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery|Article
	 */
	public function getSimCard()
	{
		return $this->hasOne(Article::className(), ['id' => 'simcard_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery | Net
	 */
	public function getNet()
	{
		return $this->hasOne(Net::className(), ['id' => 'net_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTariffSet()
	{
		return $this->hasOne(Sets::className(), ['id' => 'set_id']);
	}

	/**
	 * @return array
	 */
	public function getBlockedProviders()
	{
		$blockedProviders = [];

		if ($this->mnp_blocked_providers) {
			$unfilteredBlockedProviders = explode(',', $this->mnp_blocked_providers);
			$blockedProviders = array_map("trim", $unfilteredBlockedProviders);
		}

		return $blockedProviders;
	}

	public function getCssSystemTitle()
	{
		if (!preg_match('/^-?[_a-zA-Z]/', $this->system_title))
			return 'css'.$this->system_title;

		return $this->system_title;
	}

	/**
	 * @return bool
	 */
	public function isUpcCablecom()
	{
		return ($this->system_title == self::TITLE_UPC_CABLECOM);
	}
}
