<?php

namespace common\models;

use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tarifhighlights".
 *
 * @property integer $id
 * @property integer $tariff_id
 * @property integer $detail_id
 *
 * @property TariffHighlightDetails $detail
 */
class TariffHighlights extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tariff_highlights';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['tariff_id', 'detail_id'], 'required'],
			[['tariff_id', 'detail_id'], 'integer']
		];
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'tariff_id' => Yii::t('app', 'Tarif ID'),
			'detail_id' => Yii::t('app', 'Detail ID'),
		];
	}

	/**
	 * @return ActiveQuery
	 */
	public function getTariff()
	{
		return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getDetail()
	{
		return $this->hasOne(TariffHighlightDetails::className(),['id' => 'detail_id']);
	}
}
