<?php

namespace common\models;

use components\i18n\TranslatableActiveRecord;
use Yii;

/**
 * This is the model class for table "articledetails_groups".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $title
 * @property integer $sort
 *
 * @property ArticleDetails $details
 */
class ArticleDetailsGroup extends TranslatableActiveRecord
{
	protected $translateAttributes = ['title'];

	const GROUP_SOFTWARE = 10;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'article_details_group';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules([
			[['group_id', 'title', 'sort'], 'required'],
			[['group_id', 'sort'], 'integer'],
			[['title'], 'string', 'max' => 64],
			[['group_id', 'sort'], 'unique']
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'group_id' => Yii::t('app', 'Detail Group ID'),
			'title' => Yii::t('app', 'Title'),
			'sort' => Yii::t('app', 'Sort'),
		];
	}

	/**
	 * @return ArticleDetailsGroup[]
	 */
	public static function getGroups()
	{
		return self::find()->orderBy('sort')->all();
	}

	/**
	 * @return ArticleDetails[]
	 */
	public function getDetails()
	{
		return $this->hasMany(ArticleDetails::className(), ['group_id' => 'group_id'])->orderBy('sort_id');
	}
}
