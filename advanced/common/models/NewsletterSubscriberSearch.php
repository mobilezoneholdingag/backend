<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * NewsletterSubscriberSearch represents the model behind the search form about `common\models\NewsletterSubscriber`.
 */
class NewsletterSubscriberSearch extends NewsletterSubscriber
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = NewsletterSubscriber::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
			]
		);

		$dataProvider->setSort([
			'attributes' => [
				'id',
				'customer_id',
				'statusLabel' => [
					'asc' => ['status' => SORT_ASC],
					'desc' => ['status' => SORT_DESC],
				],
				'updated_at',
				'customerEmail' => [
					'asc' => ['customer.email' => SORT_ASC],
					'desc' => ['customer.email' => SORT_DESC],
				],
				'customerGenderLabel' => [
					'asc' => ['customer.gender' => SORT_ASC],
					'desc' => ['customer.gender' => SORT_DESC],
					'label' => Yii::t('app', 'Gender')
				],
				'customerFirstName' => [
					'asc' => ['customer.first_name' => SORT_ASC],
					'desc' => ['customer.first_name' => SORT_DESC],
				],
				'customerLastName' => [
					'asc' => ['customer.last_name' => SORT_ASC],
					'desc' => ['customer.last_name' => SORT_DESC],
				],
				'customerLanguage' => [
					'asc' => ['customer.language' => SORT_ASC],
					'desc' => ['customer.language' => SORT_DESC],
				],
			]
		]);

		if (!($this->load($params) && $this->validate())) {
			$query->joinWith(['customer']);
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere(
			[
				'id' => $this->id,
				'customer_id' => $this->customer_id,
				'standard' => $this->standard,
				'press' => $this->press,
				'investor_relations' => $this->investor_relations,
				'updated_at' => $this->updated_at,
				'created_at' => $this->created_at,
			]
		);

		return $dataProvider;
	}
}
