<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article_groups_sim".
 *
 * @property integer $article_group_id
 * @property integer $simcard_id
 * @property integer $provider_id
 * @property Provider $provider
 */
class ArticleGroupsSim extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_groups_sim';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_group_id', 'simcard_id', 'provider_id'], 'required'],
            [['article_group_id', 'simcard_id', 'provider_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_group_id' => Yii::t('app', 'Article Group ID'),
            'simcard_id' => Yii::t('app', 'Simcard ID'),
            'provider_id' => Yii::t('app', 'Provider ID'),
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProvider() {
        return $this->hasOne(Provider::className(), ['id' => 'provider_id']);
    }

	/**
	 * @return mixed
	 */
	public function getProviderId() {
		return $this->provider->id ?? null;
    }

	/**
	 * @return string
	 */
	public function getProviderTitle() {
		return $this->provider->title ?? null;
    }
}
