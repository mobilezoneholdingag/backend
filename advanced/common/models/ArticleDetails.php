<?php

namespace common\models;

use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "articledetails".
 *
 * @property integer $detail_id
 * @property string $detail_name
 * @property string $tooltip_de
 * @property integer $sort_id
 * @property integer $group_id
 * @property string $unit
 * @property integer $highlight
 */
class ArticleDetails extends ActiveRecord
{
	const DETAIL_UMTS = 1;
	const DETAIL_GSM = 2;
	const DETAIL_LTE = 3;
	const DETAIL_AIRPLANE_MODE = 4;
	const DETAIL_SAR = 5;
	const DETAIL_HEIGHT = 7;
	const DETAIL_WIDTH = 8;
	const DETAIL_DEPTH = 9;
	const DETAIL_WEIGHT = 10;
	const DETAIL_KEYBOARD = 11;
	const DETAIL_CAMERAS = 13;
	const DETAIL_MAIN_CAMERA = 14;
	const DETAIL_MAIN_CAMERA_RESOLUTION = 15;
	const DETAIL_DIGITAL_ZOOM = 16;
	const DETAIL_AUTO_FOCUS = 17;
	const DETAIL_FACE_RECOGNITION = 18;
	const DETAIL_COUNT_FLASHLIGHTS = 19;
	const DETAIL_VIDEO_RESOLUTION = 20;
	const DETAIL_MP3_PLAYER = 22;
	const DETAIL_HAS_RADIO = 23;
	const DETAIL_SPEAKERPHONE = 24;
	const DETAIL_VIBRATION_ALERT = 25;
	const DETAIL_TOUCHSCREEN = 27;
	const DETAIL_KIND = 28;
	const DETAIL_DISPLAY_TYPE = 29;
	const DETAIL_MAIN_DISPLAY_SIZE = 30;
	const DETAIL_MAIN_DISPLAY_RESOLUTION = 31;
	const DETAIL_MAIN_DISPLAY_COLOR = 32;
	const DETAIL_INTERNAL_MEMORY = 34;
	const DETAIL_MEMORY_EXPANSION = 35;
	const DETAIL_MEMORY_EXPANSION_MAX = 36;
	const DETAIL_PROCESSOR = 38;
	const DETAIL_CORES = 39;
	const DETAIL_CLOCK_SPEED = 40;
	const DETAIL_RAM = 41;
	const DETAIL_POSITIONING_SENSOR = 42;
	const DETAIL_AMBIENT_LIGHT_SENSOR = 43;
	const DETAIL_PROXIMITY_SENSOR = 44;
	const DETAIL_COMPASS = 45;
	const DETAIL_BATTERY_TYPE = 47;
	const DETAIL_BATTERY_CAPACITY = 48;
	const DETAIL_STANDY_WCDMA = 49;
	const DETAIL_STANDY_GSM = 50;
	const DETAIL_CALLTIME_WCDMA = 51;
	const DETAIL_CALLTIME_GSM = 52;
	const DETAIL_WIFI = 54;
	const DETAIL_MODEM = 55;
	const DETAIL_BLUETOOTH = 56;
	const DETAIL_WAP = 57;
	const DETAIL_OPERATING_SYSTEM = 59;
	const DETAIL_VERSION = 60;
	const DETAIL_USER_INTERFACE = 61;
	const DETAIL_SPEECH_RECOGNITION = 62;
	const DETAIL_INTERNET_BROWSER = 63;
	const DETAIL_GAMES = 64;
	const DETAIL_JAVA = 65;
	const DETAIL_WINDOWS_MEDIA_PLAYER = 66;
	const DETAIL_SMS = 68;
	const DETAIL_MMS = 69;
	const DETAIL_EMAIL_SERVICES = 70;
	const DETAIL_NFC = 71;
	const DETAIL_MICRO_USB = 73;
	const DETAIL_AUDIO_JACK = 74;
	const DETAIL_GPS_ANTENNA = 76;
	const DETAIL_NAVIGATION_SOFTWARE = 77;
	const DETAIL_GEO_TAGGING = 78;
	const DETAIL_HARDWARE_BRANDING = 80;
	const DETAIL_SOFTWARE_BRANDING = 81;
	const DETAIL_SIM_LOCK = 82;
	const DETAIL_NET_LOCK = 83;
	const DETAIL_FINGERPRINT_SENSOR = 84;
	const DETAIL_SECOND_CAMERA_RESOLUTION = 86;
	const DETAIL_DISPLAY_SIZE_INCH = 87;
	const DETAIL_DISPLAY_PPI = 88;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'article_details';
	}

	/**
	 * Returns the column name for an article detail.
	 *
	 * @param int $detail An integer defined as constant in ArticleDetails.
	 * @return string|null  Column name, if detail column exists.
	 */
	static public function getDetailAttribute($detail)
	{
		$r = new \ReflectionClass('\common\models\ArticleDetails');
		$allDetails = $r->getConstants();

		if (in_array($detail, array_values($allDetails)))
			return 'detail_' . $detail;

		return null;
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['detail_id', 'sort_id', 'group_id', 'highlight'], 'integer'],
			[['detail_name', 'sort_id'], 'required'],
			[['detail_name'], 'string', 'max' => 40],
			[['tooltip'], 'string', 'max' => 256],
			[['unit'], 'string', 'max' => 32]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'detail_id' => Yii::t('app', 'Detail ID'),
			'detail_name' => Yii::t('app', 'Detail Name'),
			'tooltip' => Yii::t('app', 'Tooltip'),
			'sort_id' => Yii::t('app', 'Sort ID'),
			'group_id' => Yii::t('app', 'Group'),
			'unit' => Yii::t('app', 'Unit'),
			'highlight' => Yii::t('app', 'Highlight'),
		];
	}

	public function getGroup()
	{
		return $this->hasOne(ArticleDetailsGroup::className(), ['group_id' => 'group_id']);
	}

	/**
	 * @param $article_id
	 * @return ArticleDetailsValues
	 */
	public function getValue($article_id)
	{
		$value = ArticleDetailsValues::findOne(['article_id' => $article_id])->{'detail_' . $this->detail_id};
		if ($value === 1 OR $value === 0) {
			$value = '';
		}
		return $value;
	}

	/**
	 * @param $article_id
	 * @return string
	 */
	public function getClass($article_id)
	{
		$class = '';
		$value = ArticleDetailsValues::findOne(['article_id' => $article_id])->{'detail_' . $this->detail_id};
		if ($value === 1 OR $value === 0) {
			$class = ($value === 1) ? 'yes' : 'no';
		}
		return $class;
	}

	/**
	 * @return bool
	 */
	public function isBool()
	{
		$model = new ArticleDetailsValues();
		$booleanAttributes = [];
		$detailName = 'detail_' . $this->detail_id;
		$rules = $model->rules();

		foreach ($rules as $rule) {
			if (end($rule) == 'boolean') {
				$booleanAttributes = $rule[0];
			}
		}

		$is_bool = (in_array($detailName, $booleanAttributes)) ? true : false;

		return $is_bool;
	}

}
