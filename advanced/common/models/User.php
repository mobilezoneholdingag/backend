<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UserEvent;

/**
 * User model
 *
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $role
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $password write-only password
 *
 * @property string $roleName
 * @property string $statusName
 */
class User extends ActiveRecord implements IdentityInterface
{
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;

	const ROLE_STARLORD = 2;
	const ROLE_ADMIN = 4;
	const ROLE_EDITOR = 20;
	const ROLE_BACKOFFICE = 15;
	const ROLE_USER = 10;


	/**
	 * This mapping has to match the roles as defined in \console\controllers\RbacController.
	 *
	 * @var array
	 */
	const ROLE_NAME_ID_MAPPING = [
		self::ROLE_STARLORD => 'starlord',
		self::ROLE_ADMIN => 'admin',
		self::ROLE_EDITOR => 'editor',
		self::ROLE_BACKOFFICE => 'backoffice',
		self::ROLE_USER => 'user',
	];

	/**
	 * Handles assigning the roles for the logging in user.
	 *
	 * @param UserEvent $event
	 */
	public function afterLogin(UserEvent $event)
	{
		$auth = Yii::$app->authManager;
		$role = $auth->getRole($this->getRoleNameById($this->role));

		$assignment = $auth->getAssignment($role->name, $this->id);
		if (empty($assignment)) {
			foreach ($auth->getRolesByUser($this->id) as $previousRole) {
				$auth->revoke($previousRole, $this->id);
			}
			$auth->assign($role, $this->id);
		}
	}

	/**
	 * @param int $roleId
	 *
	 * @return string|null
	 */
	private function getRoleNameById($roleId)
	{
		$roles = self::ROLE_NAME_ID_MAPPING;
		if (isset($roles[$roleId])) {
			return $roles[$roleId];
		}

		return null;
	}

	/**
	 * @return string
	 */
	public function getRoleName()
	{
		return ucfirst($this->getRoleNameById($this->role));
	}

	/**
	 * @return string
	 */
	public function getStatusName()
	{
		return ($this->status == self::STATUS_ACTIVE) ? Yii::t('app', 'active') : Yii::t('app', 'inactive');
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%user}}';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['status', 'default', 'value' => self::STATUS_ACTIVE],
			['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

			['role', 'default', 'value' => self::ROLE_USER],
			[
				'role',
				'in',
				'range' => [
					self::ROLE_USER,
					self::ROLE_BACKOFFICE,
					self::ROLE_EDITOR,
					self::ROLE_ADMIN,
					self::ROLE_STARLORD,
				],
			],

			[['username', 'name', 'email', 'password', 'role', 'status'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 *
	 * @return static|null
	 */
	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 *
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token)
	{
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}

		return static::findOne(
			[
				'password_reset_token' => $token,
				'status' => self::STATUS_ACTIVE,
			]
		);
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 *
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid($token)
	{
		if (empty($token)) {
			return false;
		}
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode('_', $token);
		$timestamp = (int)end($parts);

		return $timestamp + $expire >= time();
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 *
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password)
	{
		if (!empty($password))  // Password remains unchanged if no password is transmitted
		{
			$this->password_hash = Yii::$app->security->generatePasswordHash($password);
		}
	}

	public function getPassword()
	{
		return;
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}

	public static function getPossibleRoles()
	{
		$roles = [
			self::ROLE_ADMIN => 'Admin',
			self::ROLE_USER => 'User',
			self::ROLE_BACKOFFICE => 'Backoffice',
			self::ROLE_EDITOR => 'Editor',
		];

		if (Yii::$app->user->can('manageStarlordUsers')) {
			$roles[self::ROLE_STARLORD] = 'Starlord';
		}

		return $roles;
	}

	public static function getPosssibleStatuses()
	{
		return [
			self::STATUS_ACTIVE => 'Active',
			self::STATUS_DELETED => 'Deleted',
		];
	}

	public function getFirstName()
	{
		preg_match('/^[\w\-]+/', $this->name, $firstName);

		if (count($firstName) > 0) {
			return $firstName[0];
		}

		return $this->name;
	}

	public function getApprovedSales()
	{
		return $this->hasMany(SalesUnapproved::className(), ['id' => 'approving_user_id']);
	}

	public function getCurrentRoleName()
	{
		return ucfirst(current(array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id))));
	}
}


