<?php

namespace common\models;

use backend\models\interfaces\ListUpInterface;
use common\modules\set\models\Sets;
use components\i18n\TranslatableActiveRecord;
use deinhandy\yii2picasso\behavior\ImageAttributeBehavior;
use deinhandy\yii2picasso\behavior\ImageBehaviorConfig;
use deinhandy\yii2picasso\behavior\ImageUploadBehavior;
use deinhandy\yii2picasso\models\Image as PicassoImage;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "manufacturer".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $tablet_set_id
 * @property integer $is_active
 * @property integer $is_active_erp
 * @property integer $smartphone_set_id
 * @property integer $accessory_set_id
 * @property integer $sort
 * @property string $smartphone_title
 * @property string $smartphone_subtext
 * @property string $tablet_title
 * @property string $tablet_subtext
 * @property string $api_updated_at
 * @property string $erp_title
 * @property boolean $visible_homepage
 *
 * @property ArticleGroups[] $groups
 * @property Sets $accessorySet
 * @property int $groupCount
 * @property PicassoImage $image
 * @property PicassoImage $insurance_image
 * @property PicassoImage $marketing_image
 * @property integer $show_on_category
 * @property integer show_on_pdp
 */
class Manufacturer extends TranslatableActiveRecord implements ListUpInterface
{
    const APPLE = 32;
    const SIM_ONLY = 123;

    protected $translateAttributes = [
        'smartphone_title',
        'smartphone_subtext',
        'tablet_title',
        'tablet_subtext',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer';
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'image',
            'insurance_image',
            'marketing_image',
            'show_on_category',
            'show_on_pdp',
            'visible_homepage',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return parent::translatableRules([
            [['title'], 'required'],
            [['title'], 'unique'],
            [['smartphone_title', 'smartphone_subtext', 'tablet_title', 'tablet_subtext'], 'string'],
            [
                [
                    'is_active',
                    'is_active_erp',
                    'show_on_category',
                    'show_on_pdp',
                    'visible_homepage',
                    'nav_hide'
                ],
                'boolean',
            ],
            [
                [
                    'tablet_set_id',
                    'smartphone_set_id',
                    'accessory_set_id',
                    'sort',
                ],
                'integer',
            ],
            [['title', 'description', 'smartphone_title', 'tablet_title', 'erp_title'], 'string', 'max' => 255],
            [
                ['image', 'insurance_image', 'marketing_image'],
                'file',
                'skipOnEmpty' => true,
                'extensions'=>['png'],
                'wrongExtension' => Yii::t('app', 'wrong extension - only PNG allowed')
            ],
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $accountNamespace = Yii::$app->params['picasso_namespace'];
        $collectionName = self::tableName();

        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'image_upload' => [
                    'class' => ImageUploadBehavior::class,
                    'attributes' => [
                        'image' => new ImageBehaviorConfig($accountNamespace, $collectionName),
                        'insurance_image' => new ImageBehaviorConfig($accountNamespace, $collectionName),
                        'marketing_image' => new ImageBehaviorConfig($accountNamespace, 'marketing-images'),
                    ],
                ],
                'image_access' => [
                    'class' => ImageAttributeBehavior::class,
                    'attributes' => [
                        'image',
                        'insurance_image',
                        'marketing_image',
                    ],
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Manufacturer ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'articleCount' => Yii::t('app', 'Article Count'),
            'groupCount' => Yii::t('app', 'Group Count'),
            'smartphone_set_id' => Yii::t('app', 'Smartphone Set'),
            'accessory_set_id' => Yii::t('app', 'Accessory Set'),
            'tablet_set_id' => Yii::t('app', 'Tablet Set'),
            'is_active' => Yii::t('app', 'Is Active'),
            'is_active_erp' => Yii::t('app', 'Is Active Erp'),
            'sort' => Yii::t('app', 'Sort'),
            'smartphone_title' => Yii::t('app', 'title for smartphone section'),
            'smartphone_subtext' => Yii::t('app', 'subtext for smartphone section'),
            'tablet_title' => Yii::t('app', 'title for tablet section'),
            'tablet_subtext' => Yii::t('app', 'subtext of tablet section'),
            'erp_title' => Yii::t('app', 'ERP Title'),
            'image' => Yii::t('app', 'Herstellerbild (Upload/Update)'),
            'insurance_image' => Yii::t('app', 'Versicherungslogo (Upload/Update)'),
            'marketing_image' => Yii::t('app', 'Hersteller Marketing Banner (Upload/Update)'),
            'show_on_category' => Yii::t('app', 'Auf Category-Seite anzeigen'),
            'show_on_pdp' => Yii::t('app', 'Auf PDP-Seite anzeigen'),
            'visible_homepage' => 'Auf Frontpage sichtbar',
            'nav_hide' => 'In der Navigation ausblenden',
        ];
    }

    /**
     * A simple list of all available manufacturers.
     *
     * @return array
     */
    public static function getList()
    {
        $manufacturers = [];
        foreach (self::find()->select('title')->where(['is_active' => 1])->asArray()->orderBy('sort')->all() as $manufacturer) {
            $manufacturers[strtolower($manufacturer['title'])] = $manufacturer['title'];
        }

        return $manufacturers;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(ArticleGroups::className(), ['manufacturer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmartphoneSet()
    {
        return $this->hasOne(Sets::className(), ['id' => 'smartphone_set_id']);
    }

    public static function getManufacturerWithActiveArticles($id)
    {
        $subquery = Article::find()->select('manufacturer_id')->where([
            'is_active' => true,
            'is_active_erp' => true,
            'nav_hide' => false,
            'product_category_id' => $id,
        ])->distinct();

        $query = self::find()->select(['title', 'id'])
            ->where(['in', 'id', $subquery])
            ->andWhere(['!=', 'id', self::SIM_ONLY])
            ->orderBy('title');

        $nav = [
            'elements' => $query->all(),
        ];



        return $nav;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessorySet()
    {
        return $this->hasOne(Sets::className(), ['id' => 'accessory_set_id']);
    }

    /**
     * @return \yii\db\ActiveQuery | Sets
     */
    public function getTabletSet()
    {
        return $this->hasOne(Sets::className(), ['id' => 'tablet_set_id']);
    }

    /**
     * @return \yii\db\ActiveQuery | Article[]
     */
    public function getTopVariations()
    {
        return $this->hasMany(Article::className(), ['id' => 'top_variation_id'])
            ->viaTable(ArticleGroups::tableName(), ['manufacturer_id' => 'id'])
            ->where(['product_category_id' => ProductCategory::SMARTPHONE]);
    }

    /**
     * @return \yii\db\ActiveQuery | Article[]
     */
    public function getTopTabletVariations()
    {
        return $this->hasMany(Article::className(), ['id' => 'top_variation_id'])
            ->viaTable(ArticleGroups::tableName(), ['manufacturer_id' => 'id'])
            ->where(['product_category_id' => ProductCategory::TABLET]);
    }

    /**
     * @return int
     */
    public function getGroupCount()
    {
        return count($this->groups);
    }
}
