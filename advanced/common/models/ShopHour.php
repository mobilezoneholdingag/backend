<?php

namespace common\models;

/**
 * This is the model class for table "shop_hour".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property integer $day
 * @property string $opened_at
 * @property string $closed_at
 * @property Shop $shop
 */
class ShopHour extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'shop_hour';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['shop_id'], 'required'],
			[['shop_id', 'day'], 'integer'],
			[['opened_at', 'closed_at'], 'safe'],
			[
				['shop_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Shop::className(),
				'targetAttribute' => ['shop_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'shop_id' => 'Shop ID',
			'day' => 'Day',
			'opened_at' => 'Opened At',
			'closed_at' => 'Closed At',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getShop()
	{
		return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
	}
}
