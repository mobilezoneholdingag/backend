<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SeoSearch represents the model behind the search form about `common\models\Seo`.
 */
class SeoSearch extends Seo
{

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['url_applying', 'page_title', 'page_description', 'redirect_url', 'canonical_url'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Seo::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'updated_at' => SORT_DESC,
				]
			],
			'pagination' => [
				'pageSizeLimit' => [1,200]
			]
		]);


		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
		]);

		$query->andFilterWhere(['like', 'url_applying', $this->url_applying])
			->andFilterWhere(['like', 'page_title', $this->page_title])
			->andFilterWhere(['like', 'page_description', $this->page_description])
			->andFilterWhere(['like', 'redirect_url', $this->redirect_url])
			->andFilterWhere(['like', 'canonical_url', $this->canonical_url]);
		// ->orderBy(['url_applying' => SORT_ASC]);

		return $dataProvider;
	}

	public function searchByDeduction($url)
	{
		$query = Seo::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query
		]);

		$urlParts = explode('/', ltrim($url, '/'));

		for ($j = count($urlParts) - 1; $j >= 0; $j--) {
			$urlQuery = '';
			for ($i = 0; $i <= $j; $i++) {
				$urlQuery .= '/' . $urlParts[$i];
			}

			$query->orFilterWhere(['url_applying' => $urlQuery]);
		}

		$query->orFilterWhere(['url_applying' => '/']);
		$query->orderBy(['url_applying' => SORT_DESC]);

		return $dataProvider;
	}

	/**
	 * @param $url
	 * @return Seo[]|array
	 */
	public static function searchByUrl($url)
	{
		if (($pos = strpos($url, '?')) !== false)
			$url = substr($url, 0, $pos);

		return Seo::findOne(['url_applying' => $url]);
	}
}
