<?php

namespace common\models;

use common\models\behaviors\TimestampBehaviorSettings;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "newsletter_subscriber".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Customer $customer
 * @property string $statusLabel
 * @property string $customerEmail
 * @property string $customerGenderLabel
 * @property string $customerFirstName
 * @property string $customerLastName
 * @property string $customerLanguage
 */
class NewsletterSubscriber extends ActiveRecord
{
	const STATUS_NEVER_SUBSCRIBED = 1;
	const STATUS_DOI_STARTED = 2;
	const STATUS_DOI_CONFIRMED = 3;
	const STATUS_UNSUBSCRIBED = 4;

	const STATUS_LABELS = [
		self::STATUS_NEVER_SUBSCRIBED => 'never subscribed',
		self::STATUS_DOI_STARTED => 'subscribed',
		self::STATUS_DOI_CONFIRMED => 'DOI confirmed',
		self::STATUS_UNSUBSCRIBED => 'unsubscribed',
	];

	const LIST_CSV_NAME = 'newsletter-subscriber.csv';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'newsletter_subscriber';
	}

	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['customer_id', 'status'], 'required'],
			[['customer_id', 'status'], 'integer'],
			[['updated_at', 'created_at'], 'safe'],
			[['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'customer_id' => 'Customer ID',
			'status' => 'Status',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
			'statusLabel' => 'Status',
			'customerEmail' => 'Email',
			'customerGenderLabel' => 'Gender',
			'customerFirstName' => 'First Name',
			'customerLastName' => 'Last Name',
			'customerLanguage' => 'Language',
		];
	}

	public function fields()
	{
		return array_merge(parent::fields(), [
			'customer' => 'customerId',
		]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}

	public function getCustomerId()
	{
		return $this->customer_id;
	}

	/**
	 * @return string
	 */
	public function getCustomerEmail()
	{
		return $this->customer->email;
	}

	/**
	 * @return string
	 */
	public function getCustomerGenderLabel()
	{
		return $this->customer->genderLabel;
	}

	/**
	 * @return string
	 */
	public function getCustomerFirstName()
	{
		return $this->customer->first_name;
	}

	/**
	 * @return string
	 */
	public function getCustomerLastName()
	{
		return $this->customer->last_name;
	}

	/**
	 * @return string
	 */
	public function getCustomerLanguage()
	{
		return $this->customer->language;
	}

	/**
	 * @return string
	 */
	public function getStatusLabel()
	{
		return static::STATUS_LABELS[$this->status];
	}
}
