<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "short_urls".
 *
 * @property integer $id
 * @property string $code
 * @property string $url
 */
class ShortUrls extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'short_urls';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['code', 'url'], 'required'],
			[['code'], 'string', 'max' => 255],
			[['url'], 'string', 'max' => 2048],
			['url', 'match', 'pattern' => '/^\/\S*/', 'message' => 'the url has to start with a slash (e.g. "/smartphones/apple")'],
			[['code'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'code' => Yii::t('app', 'Code'),
			'url' => Yii::t('app', 'Url'),
		];
	}
}
