<?php

namespace common\models;

interface GenderInterface
{
	const MALE = 1;
	const FEMALE = 2;
	const ALL = [
		1 => 'Gender_Male',
		2 => 'Gender_Female',
	];
}
