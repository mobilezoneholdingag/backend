<?php

namespace common\models;

use common\helper\SaleableHelper;
use common\models\behaviors\TimestampBehaviorSettings;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use components\Locale;

/**
 * This is the model class for table "landingpages".
 *
 * @property integer $id
 * @property string $title
 * @property string $headline
 * @property string $url
 * @property string $meta_description
 * @property string $redirect_url
 * @property string $canonical_url
 * @property string $html_code
 * @property string $url_parameter
 * @property integer $updated_at
 * @property string $offers         (Format: JSON)
 * @property integer $sitemap_category_id
 * @property boolean $active
 * @property boolean $noindex
 *
 * @property string $categoryLabel
 */
class LandingPages extends ActiveRecord
{
	const SERVICE = 1;
	const SMARTPHONES_MANUFACTURER = 2;
	const PROVIDER = 4;
	const TABLETS_MANUFACTURER = 3;
	const PRODUCTS = 5;
	const ARTICLE_PROVIDER_COMBINATION = 6;

	const SITEMAP_CATEGORY_LABELS = [1 => 'service', 2 => 'smartphone manufacturers', 4 => 'tablet manufacturers', 3 => 'provider', 5 => 'products'];
	const URL_PARAMETERS = ['special' => 'special', 'partner' => 'partner', 'service' => 'service'];

	static public $languages = [
		Locale::SUISSE => Locale::SUISSE,
		Locale::FRANCE => Locale::FRANCE,
		Locale::ITALY => Locale::ITALY,
	];

	public function init()
	{
		$this->active = true;
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'landingpages';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [
			LoggableBehavior::className(),
			'datetime' => TimestampBehaviorSettings::updatedAt(),
		] : [];
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'url', 'meta_description'], 'required'],
			[['sitemap_category_id', 'active', 'noindex'], 'integer'],
			[['html_code', 'url_parameter', 'language'], 'string'],
			[['url', 'language'], 'unique', 'targetAttribute' => ['url', 'language']],
			[['title', 'headline', 'url', 'meta_description', 'canonical_url', 'redirect_url'], 'string', 'max' => 256],
			[['offers'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'url' => Yii::t('app', 'Url'),
			'meta_description' => Yii::t('app', 'Meta Description'),
			'html_code' => Yii::t('app', 'Html Code'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'offers' => Yii::t('app', 'Offers'),
			'sitemap_category_id' => Yii::t('app', 'Category ID'),
			'active' => Yii::t('app', 'Active'),
			'redirect_url' => Yii::t('app', 'Redirect URL'),
			'canonical_url' => Yii::t('app', 'Canonical URL'),
			'noindex' => Yii::t('app', 'no index'),
			'url_parameter' => Yii::t('app', 'URL Parameter'),
			'language' => Yii::t('app', 'Language'),

		];
	}

	public function fields()
	{
		return [
			'id',
			'active',
			'title',
			'url',
			'html_code',
		];
	}

	/**
	 * @param $seo
	 * @return mixed
	 */
	public static function getSeoInformations($seo)
	{
		$special_name = Yii::$app->request->get('special_name');
		$landingPage = LandingPages::findOne(['url' => $special_name]);
		if ($landingPage != null) {
			$seo->page_title = $landingPage->title;
			$seo->page_description = $landingPage->meta_description;
			$seo->canonical_url = $landingPage->canonical_url;
		} else {
			Yii::$app->response->redirect(Url::home());
		}

		return $seo;
	}

	/**
	 * @return int
	 */
	public function getOffersType()
	{
		return $this->getOffersAsArray()['kind'];
	}

	/**
	 * @return array
	 */
	public function getOffersAsArray()
	{
		$convertedProperty = null;
		$items = null;

		if (is_string($this->offers)) {
			$convertedProperty = json_decode($this->offers, true);
		} else {
			$convertedProperty = [
				'items' => [],
				'kind' => SaleableHelper::DEFAULT_CATEGORY,
				'alignment' => 1
			];
		}

		return $convertedProperty;
	}

	/**
	 * @param array $offers
	 */
	public function setOffersFromArray(array $offers)
	{
		if (empty($offers) || empty($offers['offers']))
			return;

		$offers = [
			'items' => [],
			'kind' => $offers['kind'],
			'alignment' => $offers['alignment']
		];

		foreach ($offers['items'] as $item) {
			if ($item instanceof Article || $item instanceof Tariffs) {
				$offers['items'][] = $item->id;
			} else {
				$offers['items'][] = $item;
			}
		}

		$this->offers = json_encode($offers);
	}

	public function getCategoryLabel()
	{
		$categoryLabelArray = self::SITEMAP_CATEGORY_LABELS;
		return Yii::t('app', $categoryLabelArray[$this->sitemap_category_id]);
	}

	/**
	 * @return LandingPages[]
	 */
	public static function buildSiteMapArray()
	{
		$siteMapArray = [];
		$siteMapArray[self::SERVICE] = self::find()->where(['sitemap_category_id' => self::SERVICE, 'active' => true])->all();
		$siteMapArray[self::SMARTPHONES_MANUFACTURER] = self::find()->where(['sitemap_category_id' => self::SMARTPHONES_MANUFACTURER, 'active' => true])->all();
		$siteMapArray[self::PROVIDER] = self::find()->where(['sitemap_category_id' => self::PROVIDER, 'active' => true])->all();
		$siteMapArray[self::TABLETS_MANUFACTURER] = self::find()->where(['sitemap_category_id' => self::TABLETS_MANUFACTURER, 'active' => true])->all();
		$siteMapArray[self::PRODUCTS] = self::find()->where(['sitemap_category_id' => self::PRODUCTS, 'active' => true])->all();
		$siteMapArray[self::ARTICLE_PROVIDER_COMBINATION] = self::getArticleProviderLandingPages();

		return $siteMapArray;
	}

	/**
	 * @return LandingPages[]
	 */
	protected static function getArticleProviderLandingPages()
	{
		$result = [];
		$groups = ArticleGroups::find()->where(['product_category_id' => ProductCategory::SMARTPHONE])->all();
		$providers = Provider::find()->where(['status' => true])->all();

		/**
		 * @var $groups ArticleGroups[]
		 * @var $providers Provider[]
		 */
		foreach ($groups as $group) {
			foreach ($providers as $provider) {
				if (ArticleTariffs::find()->where(['article_id' => $group->top_variation_id, 'tariff_id' => $group->start_tariff_id])->exists()) {
					$model = new self;
					$model->url = $group->url . $provider->system_title . '-vertrag';
					$model->title = $group->short_name . ' mit ' . $provider->title . ' Vertrag';
					$result[] = $model;
				}
			}
		}

		return $result;
	}
}
