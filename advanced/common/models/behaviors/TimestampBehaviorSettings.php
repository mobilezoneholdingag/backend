<?php

namespace common\models\behaviors;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class TimestampBehaviorSettings
{
	public static function createdAt(): array
	{
		return [
			'class' => TimestampBehavior::className(),
			'attributes' => [
				ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
			],
			'value' => new Expression('NOW()'),
		];
	}

	public static function updatedAt(): array
	{
		return [
			'class' => TimestampBehavior::className(),
			'attributes' => [
				ActiveRecord::EVENT_BEFORE_INSERT => 'updated_at',
				ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
			],
			'value' => function() {
				return date('Y-m-d H:i:s');
			},
		];
	}

	public static function createdAtAndUpdatedAt(): array
	{
		return [
			'class' => TimestampBehavior::className(),
			'attributes' => [
				ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
				ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
			],
			'value' => new Expression('NOW()'),
		];
	}
}
