<?php

namespace common\models;

use common\models\behaviors\TimestampBehaviorSettings;
use common\modules\pricing\models\ConsolidatedPrice;
use common\modules\pricing\models\interfaces\PriceProviderInterface;
use common\modules\pricing\models\Price;
use common\modules\set\models\Chart;
use common\modules\set\models\ChartItemInterface;
use common\modules\set\models\Sets;
use components\i18n\TranslatableActiveRecord;
use PDO;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tariffs".
 *
 * @property integer $id
 * @property integer $is_active
 * @property integer $is_active_erp
 * @property integer $network
 * @property integer $provider_id
 * @property string $title
 * @property string $erp_title
 * @property string $provision                  Provider's provision to us.
 * @property string $description
 * @property float $price_monthly              Introducing price for the first period.
 * @property string $plain_price_monthly        Plain price from configuration without any manipulation.
 * @property float $price_monthly_later         Alternate price for contract periods to follow.
 * @property integer $price_monthly_runtime     Duration of the first period.
 * @property string $hint
 * @property integer $anytime_minutes
 * @property integer $free_sms
 * @property string $flatrate_landline
 * @property boolean $flatrate_mobile_networks
 * @property boolean $sms_flatrate_mobile_networks
 * @property boolean $mms_flatrate_mobile_networks
 * @property string $internet_flat
 * @property string $price_mms
 * @property float $price_sms
 * @property string $contract_duration
 * @property integer $updated_at
 * @property string $bill_online
 * @property string $data_volume
 * @property boolean $lte
 * @property string $speed_reduced
 * @property string $speed_max
 * @property string $footnote_id
 * @property integer $is_prepaid
 * @property string $comments
 * @property integer $count_signatures
 * @property integer $count_contractpages
 * @property integer $is_b2b_allowed
 * @property integer $portable_number
 * @property integer $young_people
 * @property integer $voicecall_data
 * @property integer $set_id
 * @property string $rating_value
 * @property string $footnote_text
 * @property string $specific_legal_text
 * @property bool $data_only
 * @property string meta_title
 * @property string meta_description
 * @property string external_id
 * @property integer fulfillment_partner_id
 * @property integer $special_id
 * @property string $tariff_feed_description
 * @property integer $is_sim_only
 * @property integer $offer_type_id
 * @property string $api_updated_at
 * @property string $itemType
 * @property string $sim_only_tariff_id
 * @property integer $rate_payment_count
 *
 * ActiveQuery Attributes
 * @property string $uniqueTitle
 * @property TariffHighlightDetails[] $highlights
 * @property Provider $provider
 * @property OfferType $offerType
 * @property FulfillmentPartners $partner
 * @property Sets $setTitle
 * @property TariffSpecial $tariffSpecial
 * @property string $MonthlyPricePreText
 * @property string $LowestMonthlyPrice
 * @property string $swisscom_id
 * @property string $swisscom_id_2
 * @property ConsolidatedPrice $prices
 * @property ArticleTariffs[] $articleTariffs
 * @property Tariffs $simOnlyTariff
 * @property bool $hasSimOnlyArticleTariff
 * @property integer $show_alt_provider_img
 */
class Tariffs extends TranslatableActiveRecord implements ChartItemInterface, PriceProviderInterface
{
	const RATING_VALUES = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5];
	const INTERNET_FLAT_TITLE = 'Internet-Flat';
	const KILOBITS_PER_SECOND = 'Kbit/s';
	const MEGABITS_PER_SECOND = 'Mbit/s';
	const LTE = 'LTE';
	const PRICE_STARTING_FROM = 'tariff monthly price starting from';
	const PRICE_FIX = 'tariff monthly price only';
	const DEFAULT_TARIFF_RUNTIME = 24;

	const RATE_PAYMENT_COUNT_EQUAL_CONTRACT_DURATION = 'equal_contract_duration';
	const RATE_PAYMENT_COUNT_ALWAYS_24 = 'always_24';
	const RATE_PAYMENT_COUNT_NONE = null;

	protected $translateAttributes = [
		'hint',
		'description',
		'text_call',
		'text_sms',
		'text_internet',
		'flatrate_landline',
		'bill_online',
		'comments',
		'footnote_text',
		'specific_legal_text',
		'meta_title',
		'meta_description',
		'tariff_feed_description',
	];

	public $plain_price_monthly;
	public $itemType = 'tariff';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tariffs';
	}

	/**
	 * Adds related models as fields.
	 * @return array
	 */
	public function fields()
	{
		return [
			'id',
			'contract_duration',
			'data_volume',
			'description',
			'erp_title',
			'footnote_text',
			'specific_legal_text',
			'itemType',
			'price_monthly',
			'prices',
			'provider' => 'providerId',
			'provider_id' => 'providerId',
			'rate_payment_count',
			'text_call',
			'text_internet',
			'text_sms',
			'title',
			'young_people',
			'highlights',
			'itemType',
			'prices',
			'offer_type_id',
			'sim_only_article_tariff_id' => 'simOnlyArticleTariffId',
			'show_alt_provider_img',
		];
	}

	/**
	 * @return ActiveQuery|ArticleTariffs
	 */
	public function getArticleTariffs()
	{
		return $this->hasMany(ArticleTariffs::class, ['tariff_id' => 'id']);
	}

	/**
	 * @param bool $withNamespace
	 * @return string
	 */
	public static function className($withNamespace = true)
	{
		$class = parent::className();
		if ($withNamespace) {
			return $class;
		} else {
			$tmp = explode('\\', $class);
			return end($tmp);
		}
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			(Yii::$app->id == 'app-backend') ? [
				LoggableBehavior::className(),
				'datetime' => TimestampBehaviorSettings::updatedAt(),
			] : []
		);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::translatableRules(
			[
				[
					[
						'is_active',
						'is_sim_only',
						'network',
						'provider_id',
						'fulfillment_partner_id',
						'is_prepaid',
						'flatrate_mobile_networks',
						'sms_flatrate_mobile_networks',
						'is_b2b_allowed',
						'portable_number',
						'young_people',
						'voicecall_data',
						'set_id',
						'data_only',
						'contract_duration',
						'special_id',
						'price_monthly_runtime',
						'data_volume',
						'anytime_minutes',
						'free_sms',
						'external_id',
						'mms_flatrate_mobile_networks',
						'internet_flat',
						'lte',
						'show_alt_provider_img',
					],
					'integer',
				],
				[
					[
						'provision',
						'profit',
						'price_monthly',
						'price_monthly_later',
						'price_mms',
						'price_sms',
					],
					'number',
				],
				[
					[
						'title',
						'price_monthly',
						'provider_id',
					],
					'required',
				],
				[['description', 'comments', 'activation_code', 'meta_description', 'hint'], 'string'],
				[['title'], 'string', 'max' => 60],
				[['rating_value'], 'string', 'max' => 1],
				[
					[
						'flatrate_landline',
						'bill_online',
						'speed_reduced',
						'speed_max',
						'footnote_id',
					],
					'string',
					'max' => 32,
				],
				[['meta_title'], 'string', 'max' => 49],
				[['footnote_text', 'meta_title', 'meta_description', 'tariff_feed_description', 'specific_legal_text'], 'string', 'max' => 8000],
				[
					[
						'swisscom_id',
						'swisscom_id_2',
						'sim_only_tariff_id',
					],
					'safe',
				],
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'is_active' => Yii::t('app', 'Is Active'),
			'is_active_erp' => Yii::t('app', 'Is Active Erp'),
			'network' => Yii::t('app', 'Network'),
			'provider_id' => Yii::t('app', 'Provider ID'),
			'title' => Yii::t('app', 'Shoptitle'),
			'description' => Yii::t('app', 'Description'),
			'provision' => Yii::t('app', 'Provision'),
			'profit' => Yii::t('app', 'Profit'),
			'price_monthly' => Yii::t('app', 'Price Monthly'),
			'price_monthly_later' => Yii::t('app', 'Price Monthly Later'),
			'price_monthly_runtime' => Yii::t('app', 'Price Monthly Runtime'),
			'footnote_id' => Yii::t('app', 'Footnote ID'),
			'is_prepaid' => Yii::t('app', 'Prepaid'),
			'comments' => Yii::t('app', 'Comments'),
			'is_b2b_allowed' => Yii::t('app', 'Is B2b Allowed'),
			'portable_number' => Yii::t('app', 'Portable Number'),
			'young_people' => Yii::t('app', 'Young People'),
			'voicecall_data' => Yii::t('app', 'Voicecall Data'),
			'set_id' => Yii::t('app', 'Set ID'),
			'rating_value' => Yii::t('app', 'Rating Value'),
			'activation_code' => Yii::t('app', 'Activation Code'),
			'data_only' => Yii::t('app', 'Data Only'),
			'swisscom_id' => Yii::t('app', 'Swisscom-ID'),
			'swisscom_id_2' => Yii::t('app', 'Swisscom-ID 2'),
			'hint' => Yii::t('app', 'usp label'),
			'anytime_minutes' => Yii::t('app', 'Freiminuten'),
			'free_sms' => Yii::t('app', 'Frei-SMS'),
			'flatrate_landline' => Yii::t('app', 'Flatrate Landline'),
			'flatrate_mobile_networks' => Yii::t('app', 'Flatrate Mobile Networks'),
			'sms_flatrate_mobile_networks' => Yii::t('app', 'SMS Flatrate Mobile Networks'),
			'mms_flatrate_mobile_networks' => Yii::t('app', 'MMS Flatrate Mobile Networks'),
			'internet_flat' => Yii::t('app', 'Internet Flat'),
			'price_mms' => Yii::t('app', 'Price MMS'),
			'price_sms' => Yii::t('app', 'Price SMS'),
			'contract_duration' => Yii::t('app', 'Contract Duration'),
			'bill_online' => Yii::t('app', 'Bill Online'),
			'data_volume' => Yii::t('app', 'Data Volume'),
			'lte' => Yii::t('app', 'LTE'),
			'speed_reduced' => Yii::t('app', 'Speed Reduced'),
			'speed_max' => Yii::t('app', 'Speed Max'),
			'footnote_text' => Yii::t('app', 'Footnote Text'),
			'specific_legal_text' => Yii::t('app', 'Specific Legal Text'),
			'fulfillment_partner_id' => Yii::t('app', 'Fulfillment Partner'),
			'external_id' => Yii::t('app', 'ERP ID'),
			'partnerTitle' => Yii::t('app', 'Partner Title'),
			'providerTitle' => Yii::t('app', 'Provider Title'),
			'setTitle' => Yii::t('app', 'Set Title'),
			'special_id' => Yii::t('app', 'Tariff Specials'),
			'is_sim_only' => Yii::t('app', 'Sim Only Tariff'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'ratePaymentCount' => Yii::t('app', 'Rate Payment Count'),
			'show_alt_provider_img' => Yii::t('app', 'Alternativlogo des Providers anzeigen'),
		];
	}

	/**
	 * @return Tariffs[]
	 */
	public static function getTopTariffs()
	{
		$topTariffs = [];
		//TODO replace following 2 statements with a join
		$topTariffsSetId = Sets::findOne(['system_title' => Sets::TOP_TARIFFS])->id;
		$charts = Chart::findAll(['set_id' => $topTariffsSetId]);

		foreach ($charts as $chart) {
			$topTariffs[] = self::findOne($chart->product_id);
		}

		return $topTariffs;
	}

	/**
	 * @return ActiveQuery | Provider
	 */
	public function getProvider()
	{
		return $this->hasOne(Provider::className(), ['id' => 'provider_id']);
	}

	/**
	 * @return ActiveQuery | OfferType
	 */
	public function getOfferType()
	{
		return $this->hasOne(OfferType::className(), ['id' => 'offer_type_id']);
	}

	public function getOfferTypeTitle()
	{
		return $this->offerType->title ?? null;
	}

	/**
	 * @return string
	 */
	public function getProviderTitle()
	{
		return $this->provider->title;
	}

	/**
	 * @return ActiveQuery | TariffHighlightDetails[]
	 */
	public function getHighlights()
	{
		return $this->hasMany(TariffHighlightDetails::className(), ['id' => 'detail_id'])
			->viaTable(TariffHighlights::tableName(), ['tariff_id' => 'id'])
			->orderBy('sort');
	}

	/**
	 * @return float
	 */
	public function getCalculatedPrice()
	{
		return $this->price_monthly;
	}

	/**
	 * helper function to use the same attributes (article_id, bezeichnung) on both models (Article, Tariffs)
	 *
	 * @return int
	 */
	public function getArticle_id()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getUniqueTitle()
	{
		if (empty($this->provider->title)) {
			return null;
		}

		return $this->provider->title . ' ' . $this->title . ' (' . $this->price_monthly . '€)';
	}

	/**
	 * @return string
	 */
	public function getUniqueShortName()
	{
		return $this->uniqueTitle;
	}

	/**
	 * @return Sets|\yii\db\ActiveQuery|array
	 */
	public function getDeviceSet()
	{
		if (!empty($this->set_id)) {
			return $this->hasOne(Sets::className(), ['id' => 'set_id']);
		} else {
			return Sets::getItemsFor(Sets::ALL_SMARTPHONES);
		}
	}

	/**
	 * @return ActiveQuery | Sets
	 */
	public function getSet()
	{
		return $this->hasOne(Sets::className(), ['id' => 'set_id']);
	}

	/**
	 * @return ActiveQuery | FulfillmentPartners
	 */
	public function getPartner()
	{
		return $this->hasOne(FulfillmentPartners::className(), ['id' => 'fulfillment_partner_id']);
	}

	/**
	 * @return string
	 */
	public function getPartnerTitle()
	{
		return $this->partner->title ?? null;
	}

	/**
	 * @return string
	 */
	public function getSetTitle()
	{
		if (isset($this->set)) {
			return $this->set->title;
		}

		return null;
	}

	/**
	 * @param array $highlights
	 *
	 * @return bool
	 */
	public function saveHighlights(array $highlights)
	{
		$highlights = isset($highlights['detailIds']) ? $highlights['detailIds'] : [];
		TariffHighlights::deleteAll(['tariff_id' => $this->id]);
		foreach ($highlights as $detail_id) {
			$model = new TariffHighlights();
			$model->tariff_id = $this->id;
			$model->detail_id = $detail_id;
			$model->save();
		}

		return true;
	}

	/**
	 * @param $article_id
	 *
	 * @return bool|mixed
	 */
	public function getArticlePrice($article_id)
	{
		$price = false;
		/** @var ArticleTariffs $model */
		$model = ArticleTariffs::find()->where(['article_id' => $article_id, 'tariff_id' => $this->id])->one();
		if ($model != null) {
			$price = $model->price;
		}

		return str_replace('.', ',', $price);
	}

	/**
	 * @return int
	 */
	public function getSpecialId()
	{
		return $this->special_id;
	}

	/**
	 * @return ActiveQuery | TariffSpecial
	 */
	public function getTariffSpecial()
	{
		return $this->hasOne(TariffSpecial::className(), ['id' => 'special_id']);
	}

	public function getFulfillmentPartner()
	{
		return $this->hasOne(FulfillmentPartners::className(), ['id' => 'fulfillment_partner_id']);
	}

	/**
	 * @param Provider $provider
	 * @param array $parameters Further filtering of tariffs.
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	static public function getByProvider(Provider $provider, $parameters = [])
	{
		$parameters = array_merge(['provider_id' => $provider->id], $parameters);

		return self::find()->where($parameters)->all();
	}

	/**
	 * @return string
	 */
	public function getInternetFlatText()
	{
		$data_automation = ($this->provider->speed_automatic_title) ?: $this->speed_reduced . ' ' . self::KILOBITS_PER_SECOND;
		$format = 'bis zu %s ' . self::MEGABITS_PER_SECOND . ', danach %s';
		$text = sprintf($format, $this->speed_max, $data_automation);

		return $text;
	}

	/**
	 * Returns if the tariff is a BASE tariff (BASE is currently provider with ID "1").
	 * Needed sometimes as BASE does not need passport data in cart process.
	 *
	 * @return bool
	 */
	public function isBaseTariff()
	{
		return ($this->provider_id === 1 && $this->provider->system_title == 'base');
	}

	/**
	 * Returns if the tariff is a Telekom tariff (Telekom is currently provider with ID "2").
	 * Needed in frontend as Telekom offers special options that need to show special frontend code.
	 *
	 * @return bool
	 */
	public function isTelekomTariff()
	{
		return ($this->provider_id === 2);
	}

	/**
	 * Returns if the tariff is a Telekom tariff AND has the MagentaEINS tariff special.
	 *
	 * @return bool
	 */
	public function hasTelekomMagentaSpecial()
	{
		return ($this->provider_id === 2 && in_array($this->special_id, [6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18]));
	}

	public function getMonthlyPriceAfterMagentaDiscount()
	{
		if ($this->hasTelekomMagentaSpecial() && $this->tariffSpecial) {
			return $this->getCalculatedPrice() + (float)$this->tariffSpecial->price_change;
		} else {
			return false;
		}
	}

	/**
	 * Returns the short text before every monthly tariff price. If the provider has hardware related tariff prices
	 * the changes a little bit.
	 *
	 * @return string
	 *
	 * @param $device mixed
	 */
	public function getMonthlyPricePreText($device = null)
	{
		$withDevice = ($device instanceof Article) ? true : false;

		return (($this->provider->hardware_related_tariff_price && !$withDevice) && !$this->is_sim_only) ?
			Yii::t('app', self::PRICE_STARTING_FROM) :
			Yii::t('app', self::PRICE_FIX);
	}

	public function getLowestMonthlyPrice()
	{
		$price = $this->findLowestMonthlyPrice();

		return $price;
	}

	public function getMonthlyPrice($article_id)
	{
		$result = null;

		if ($this->provider->hardware_related_tariff_price) {
			/** @var ArticleTariffs $model */
			$model = ArticleTariffs::find()->where(['article_id' => $article_id, 'tariff_id' => $this->id])->one();

			if ($model) {
				$result = $model->price_monthly;
			}
		} else {
			$result = $this->price_monthly;
		}

		return $result;
	}

	public function findLowestMonthlyPrice()
	{
		$result = null;

		if ($this->provider->hardware_related_tariff_price) {
			/** @var ArticleTariffs $model */
			$model = ArticleTariffs::find()->where(['tariff_id' => $this->id, 'is_active' => true])
				->andWhere(['not', ['price_monthly' => null]])
				->orderBy('price_monthly')
				->one();

			if ($model) {
				$result = $model->price_monthly;
			}
		}

		if (!isset($result)) {
			$result = $this->price_monthly;
		}

		return $result;
	}

	public function getId() : int
	{
		return $this->id;
	}

	public function hasImage() : bool
	{
		return false;
	}

	public function getSmallImageUrl() : string
	{
		return '';
	}

	public function hasGroup() : bool
	{
		return false;
	}

	public function getPrices() : ConsolidatedPrice
	{
		$price = new ConsolidatedPrice();
		$price->getPriceOnce()->add($this->getPriceOnce());
		$price->getPriceMonthly()->add($this->getPriceMonthly());

		return $price;
	}

	public function getPriceOnce()
	{
		return new Price();
	}

	public function getPriceMonthly()
	{
		return new Price($this->price_monthly);
	}

	/**
	 * Returns the maximal number of month a rate payment can run. Returns null if rate payment is disabled.
	 *
	 * @return int|null
	 */
	public function getRatePaymentCount()
	{
		$provider = $this->provider;
		switch ($provider->rate_payment_count_strategy) {
			case self::RATE_PAYMENT_COUNT_EQUAL_CONTRACT_DURATION:
				return (int)$this->contract_duration;
			case self::RATE_PAYMENT_COUNT_ALWAYS_24:
				return 24;
			default:
				return null;
		}
	}

	public function isRatePaymentEnabled() : bool
	{
		return $this->getRatePaymentCount() !== null;
	}

	public function updateRatePayment(): self
	{
		$this->rate_payment_count = $this->getRatePaymentCount();

		return $this;
	}

	public function hasYoungAgeLimit(): bool
	{
		return $this->provider->young_age_limit > 0;
	}

	public function isB2B(): bool
	{
		return (bool)$this->is_b2b_allowed;
	}

	public function isDetailPageViewable()
	{
		return $this->is_active
			&& $this->is_active_erp
			&& !$this->isB2B();
	}

	public static function getAllHardwareArticlesForDropdown($id)
	{
		$matchingArticleTariffs = ArrayHelper::map(
			ArticleTariffs::find()->select(['id', 'article_id'])->where(
				['tariff_id' => $id, 'is_active' => true]
			)->asArray()->all(),
			'id',
			'article_id'
		);

		$matchingHardware = [0 => 'Bitte wählen...'];

		$matchingHardware = $matchingHardware + ArrayHelper::map(
				Article::find()->select(['`id`', '`product_name`'])->where(
					['is_active' => true]
				)->andWhere(['in', 'id', $matchingArticleTariffs])->asArray(true)->all(),
				'id',
				'product_name'
			);

		return $matchingHardware;
	}

	public function getProviderId(): int
	{
		return $this->provider_id;
	}

	public function getSimOnlyArticleTariffId()
	{
		$id = ArticleTariffs::find()
			->select('id')
			->where(['tariff_id' => $this->sim_only_tariff_id, 'article_id' => Article::SIM_ONLY_ID])
			->scalar();

		return is_numeric($id) ? intval($id) : $id;
	}

	public function getHasSimOnlyArticleTariff()
	{
		return ArticleTariffs::find()
			->where(['article_id' => Article::SIM_ONLY_ID, 'tariff_id' => $this->id])
			->exists();
	}

	public function getViableSimOnlyTariffs()
	{
		$articleTariffsTable = ArticleTariffs::tableName();
		$tariffsTable = Tariffs::tableName();
		$simOnlyId = Article::SIM_ONLY_ID;

		return Tariffs::find()
			->leftJoin($articleTariffsTable, new Expression(
				"{$articleTariffsTable}.tariff_id = {$tariffsTable}.id AND " .
				"{$articleTariffsTable}.article_id = {$simOnlyId}"
			))
			->select(["{$tariffsTable}.id", "CONCAT(erp_title, ' [ID: ', {$tariffsTable}.id, ']')"])
			->where([
				"{$articleTariffsTable}.article_id" => $simOnlyId,
				"provider_id" => $this->provider_id
			])
			->createCommand()
			->queryAll(PDO::FETCH_KEY_PAIR);
	}

	public function getSimOnlyTariff()
	{
		return $this->hasOne(self::class, ['id' => 'sim_only_tariff_id']);
	}

	public function getSimOnlyTariffTitle()
	{
		return $this->simOnlyTariff->title ?? null;
	}

	public function getAltProviderImg()
	{
		return $this->provider->alt_image ?? null;
	}
}
