<?php

namespace common\models;

use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $type
 * @property bool $is_active
 * @property string $api_updated_at
 * @property string $related_class
 *
 * @property string $label
 * @property string $frontendRouteName
 */
class ProductCategory extends ActiveRecord
{
	const SIM_CARD = 11;
	const SMARTPHONE = 1;
	const TABLET = 48;
	const SMARTWATCH = 69;
	const INSURANCE = 41;
	const REFURBISHED = 73;
	const ACCESSORIES = [2,3,4,5,6,7,8,9,10,14,15,25,26,27,28,30,31,34,44,49,50,51,52,53,55,58,59,60,61,62,65,74,63,86];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product_category';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [LoggableBehavior::className()] : [];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['type', 'related_class'], 'required'],
			[['type'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'type' => Yii::t('app', 'Type'),
			'related_class' => Yii::t('app', 'Related Class'),
		];
	}

	public function fields()
	{
		return [
			'id',
			'type',
			'frontend_title',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery | Article[]
	 */
	public function getArticles()
	{
		return $this->hasMany(Article::className(), ['id' => 'product_category_id'])
			->viaTable(ArticleGroups::tableName(), ['id' => 'group_id']);
	}

	public function getLabel()
	{
		return ucfirst($this->type);
	}

	public function getUrlRepresentation()
	{
		switch ($this->id) {
			case self::SMARTPHONE:
				return '/smartphones/';
			case self::TABLET:
				return '/tablets/';
			default:
				return '/';
		}
	}

	/**
	 * @return string
	 */
	public function getFrontendRouteName()
	{
		$routeName = '';
		switch ($this->id)
		{
			case self::SMARTPHONE:
				$routeName  = 'smartphones';
				break;
			case self::TABLET:
				$routeName  = 'tablets';
				break;
			case self::SMARTWATCH:
				$routeName  = 'smartwatches';
				break;
            case self::REFURBISHED:
                $routeName = 'refurbished';
                break;
			default:
				if(in_array($this->id, self::ACCESSORIES)) {
					$routeName  = 'accessories';
				}
		}

		return $routeName;
	}
}
