<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ShopSearch represents the model behind the search form about `common\models\Shop`.
 */
class ShopSearch extends Shop
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'id',
					'shop_tree_id',
					'shop_tree_name',
					'is_helpcenter',
				],
				'integer',
			],
			[
				[
					'name',
					'street',
					'extra_line',
					'zip',
					'city',
					'canton',
					'phone',
					'fax',
					'email',
					'sort',
					'state',
					'manager_name',
					'manager_function',
				],
				'safe',
			],
			[['latitude', 'longitude'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Shop::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
			]
		);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere(
			[
				'id' => $this->id,
				'shop_tree_id' => $this->shop_tree_id,
				'shop_tree_name' => $this->shop_tree_name,
				'latitude' => $this->latitude,
				'longitude' => $this->longitude,
				'is_helpcenter' => $this->is_helpcenter,
			]
		);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'street', $this->street])
			->andFilterWhere(['like', 'extra_line', $this->extra_line])
			->andFilterWhere(['like', 'zip', $this->zip])
			->andFilterWhere(['like', 'city', $this->city])
			->andFilterWhere(['like', 'canton', $this->canton])
			->andFilterWhere(['like', 'phone', $this->phone])
			->andFilterWhere(['like', 'fax', $this->fax])
			->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', 'sort', $this->sort])
			->andFilterWhere(['like', 'state', $this->state])
			->andFilterWhere(['like', 'manager_name', $this->manager_name])
			->andFilterWhere(['like', 'manager_function', $this->manager_function]);

		return $dataProvider;
	}
}
