<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $article_id
 * @property integer $partner_id
 */
class ArticleMarketingPartner extends ActiveRecord
{
	public static function tableName()
	{
		return 'articles_marketing_partners';
	}

	public function rules()
	{
		return [
			[['article_id', 'partner_id'], 'required'],
			[['article_id', 'partner_id'], 'integer'],
		];
	}

	public function getArticles()
	{
		return $this->hasOne(Article::class, ['id' => 'article_id']);
	}
}
