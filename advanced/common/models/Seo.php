<?php

namespace common\models;

use common\models\behaviors\TimestampBehaviorSettings;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "seo".
 *
 * @property integer $id
 * @property string $url_applying
 * @property string $page_title
 * @property string $page_description
 * @property integer $visible
 * @property string $html_content
 * @property string $redirect_url
 * @property string $canonical_url
 * @property integer $updated_at
 */
class Seo extends ActiveRecord
{

	/**
	 * Default logic for html titles and meta descriptions
	 */
	public function init()
	{
		if (Yii::$app->id == 'app-frontend') {
			$this->visible = true;
			$action = Yii::$app->controller->action->id;
			$providerURLPart = Yii::$app->request->get('providerTitle', false);
			$variation = Yii::$app->request->get('variationName', false);
			$tariffURLPart = Yii::$app->request->get('tariffUrlText', null);
			$groupURLPart = Yii::$app->request->get('groupUrlText', false);

			$hardwareOnly = (in_array(Article::HARDWARE_ONLY_URL_PART, [
				$providerURLPart,
				$variation
			])) ? true : false;

			if (in_array($action, ['smartphones', 'tablets', 'handys'])) {
				$manufacturerTitle = Yii::t('seo', ucfirst(Yii::$app->request->get('manufacturerTitle', false)));
				$article = Article::getModelByUrlParameter($groupURLPart, $variation);
				$variationName = \Yii::$app->get('seo', false)->getVariationPart($article);
				$groupTitle = $groupURLPart ? ArticleGroups::findOne(['title' => $groupURLPart])->short_name .
					(($variationName) ? ' ' . $variationName : '') : null;

				$productCategory = ucfirst($action);
				$title_appendix = $hardwareOnly ? Yii::t('seo',' ohne Vertrag') : Yii::t('seo',' mit Vertrag');
				$this->page_title = (($manufacturerTitle AND $groupTitle) ? $manufacturerTitle . ' ' .
						$groupTitle : $manufacturerTitle . ' ' . $productCategory) . $title_appendix;

				$this->page_description = $this->buildDeviceMetaDescription($manufacturerTitle, $groupTitle, $productCategory, $hardwareOnly);

				$isBundle = ArticleGroups::findOne(['title' => $groupURLPart])->is_bundle;
				if ($isBundle) {
					$devices = explode('+', $groupTitle);
					$this->page_title = 'Handy Bundle mit ' . $manufacturerTitle . ' ' . $devices[0] .
						($variationName ? $variationName : "") . ' und ' . str_replace($variationName, '', $devices[1]);

					$this->page_description = 'Das ' . $manufacturerTitle . ' ' . $devices[0] .
						($variationName ? $variationName : "") . ' und ' .
						str_replace($variationName, '', $devices[1]) .
						' im unvergleichlich günstigen Bundle! Bei DeinHandy bekommst Du zwei Geräte zu einem Preis' . Yii::t('seo', 'mit einem passenden Vertrag!');
				}
			} elseif ($action == 'tarife') {
				// TODO: remove usage of url_text
				$tariffModel = Tariffs::findOne(['url_text' => $tariffURLPart]);
				$providerModel = Provider::findOne(['system_title' => $providerURLPart]);
				$providerTitle = $providerModel ? $providerModel->title : null;
				$tariffTitle = $tariffModel ? $tariffModel->title : null;
				$isSimOnly = $tariffModel && $tariffModel->is_sim_only ? $tariffModel->is_sim_only : false;
				$title_appendix = $isSimOnly ? ' ohne Handy' : ' mit Handy';
				$this->page_title = ($providerTitle AND $tariffTitle) ?
					$providerTitle . ' ' . $tariffTitle . $title_appendix :
					$providerTitle . ' ' . Yii::t('seo', 'Tarife und Vertragsangebote') . $title_appendix;
				$this->page_description = $this->buildTariffMetaDescription($providerTitle, $tariffTitle, $isSimOnly);
			}
		}
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'seo';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [
			LoggableBehavior::className(),
			'datetime' => TimestampBehaviorSettings::updatedAt(),
		] : [];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['visible'], 'integer'],
			[['page_title', 'page_description', 'url_applying', 'redirect_url', 'canonical_url'], 'string', 'max' => 255],
			[['html_content'], 'string'],
			[['url_applying'], 'unique'],
			[['url_applying'], 'required'],
			[['redirect_url', 'canonical_url'], 'url', 'defaultScheme' => 'http'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'page_title' => Yii::t('app', 'Page Title'),
			'url_applying' => Yii::t('app', 'Url Applying'),
			'page_description' => Yii::t('app', 'Page Description'),
			'visible' => Yii::t('app', 'Index'),
			'html_content' => Yii::t('app', 'Html Code'),
			'redirect_url' => Yii::t('app', 'Redirect URL'),
			'canonical_url' => Yii::t('app', 'Canonical URL'),
			'updated_at' => Yii::t('app', 'Updated At'),
		];
	}

	/**
	 * Build and return tariff meta-description string.
	 *
	 * @param $providerTitle
	 * @param $tariffTitle
	 * @param $is_sim_only
	 * @return string
	 */
	protected function buildTariffMetaDescription($providerTitle, $tariffTitle, $is_sim_only)
	{
		if ($is_sim_only) {
			$metaDescription = $this->generateSimOnlyDescription($providerTitle, $tariffTitle);
		} else {
			$metaDescription = ($providerTitle AND $tariffTitle) ?
				$this->generateProviderInclTariffDescription($providerTitle, $tariffTitle) :
				$this->generateProviderOnlyDescription($providerTitle);
		}

		return $metaDescription;
	}

	/**
	 * Build and return device meta-description string.
	 *
	 * @param $manufacturerTitle
	 * @param $groupTitle
	 * @param $productCategory
	 * @param $hardwareOnly
	 * @return string
	 */
	protected function buildDeviceMetaDescription($manufacturerTitle, $groupTitle, $productCategory, $hardwareOnly)
	{
		if($hardwareOnly) {
			$description = $this->generateDeviceOnlyDescription($manufacturerTitle, $groupTitle);
		} else {
			$description = ($manufacturerTitle AND $groupTitle) ?
				$this->generateDeviceManufacturerGroupDescription($manufacturerTitle, $groupTitle) :
				$this->generateDeviceManufacturerOnlyDescription($manufacturerTitle, $productCategory);
		}

		return $description;
	}

	/**
	 * Build and return meta description for tarife/[provider]/[tariffTitle] page.
	 *
	 * @param $providerTitle
	 * @param $tariffTitle
	 * @return string
	 */
	protected function generateProviderInclTariffDescription($providerTitle, $tariffTitle)
	{
		return 'Du suchst nach einem passenden Smartphone oder Tablet für deinen ' .
			$providerTitle . ' ' . $tariffTitle . '? ' .
			Yii::t('seo', 'Bei uns findest Du eine Auswahl der neuesten Smartphones auf deinhandy.de');
	}

	/**
	 * Build and return meta description for tarife/[provider] page.
	 *
	 * @param $providerTitle
	 * @return string
	 */
	protected function generateProviderOnlyDescription($providerTitle)
	{
		return Yii::t('seo', 'Du suchst nach einem passenden ') . $providerTitle . ' ' .
			Yii::t('seo', 'Vertrag für dein neues Smartphone oder Tablet') . '? ' .
			Yii::t('seo', 'Bei uns findest Du aktuelle Tarifangebote aller großen Tarifanbieter auf deinhandy.de');
	}

	/**
	 *  Build and return meta description for sim-only page.
	 *
	 * @param $providerTitle
	 * @param $tariffTitle
	 * @return string
	 */
	protected function generateSimOnlyDescription($providerTitle, $tariffTitle)
	{
		return 'Der ' . $providerTitle . ' ' . $tariffTitle .
			'- Preiswert, praktisch und flexibel. Jetzt zum günstigen
			Preis surfen und telefonieren! Bei DEINHANDY findest Du die besten SIM-Only-Tarife aller bekannten Anbieter.';
	}

	/**
	 * @param $manufacturerTitle
	 * @param $groupTitle
	 * @return string
	 */
	protected function generateDeviceOnlyDescription($manufacturerTitle, $groupTitle)
	{
		return 'Du suchst das ' . $manufacturerTitle . ' ' . $groupTitle . ' ' . Yii::t('seo',' ohne Vertrag')  . '? ' .
			'Bei uns findest Du die besten Angebote zu Deinem Wunschgerät!';
	}

	/**
	 * @param $manufacturerTitle
	 * @param $groupTitle
	 * @return string
	 */
	protected function generateDeviceManufacturerGroupDescription($manufacturerTitle, $groupTitle)
	{
		return 'Du suchst das ' . $manufacturerTitle . ' ' . $groupTitle . ' ' . Yii::t('seo','mit einem passenden Vertrag') . '? ' .
			Yii::t('seo', 'Bei uns findest Du aktuelle Tarifangebote aller großen Tarifanbieter auf deinhandy.de');
	}

	/**
	 * @param $manufacturerTitle
	 * @param $productCategory
	 * @return string
	 */
	protected function generateDeviceManufacturerOnlyDescription($manufacturerTitle, $productCategory)
	{
		return 'Du suchst ein aktuelles ' . $manufacturerTitle . ' ' . substr($productCategory, 0, -1) .
			Yii::t('seo', ' mit Vertrag') . '? ' . Yii::t('seo', 'Bei uns findest du aktuelle Angebote aller namhaften Tarifanbieter!');
	}
}
