<?php

namespace common\models;

use Yii;
use sammaye\audittrail\LoggableBehavior;

/**
 * This is the model class for table "fulfillment_partners".
 *
 * @property integer $id
 * @property string $title
 * @property string $debitor_id
 * @property integer $api_exist
 * @property integer $api_is_active
 */
class FulfillmentPartners extends \yii\db\ActiveRecord
{
	const EAM_ID = 1;
	const DRILLISCH_ID = 2;
	const EINSUNDEINS_ID = 3;
	const MOBILEZONE_ID = 4;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'fulfillment_partners';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'debitor_id'], 'string', 'max' => 255],
			[['api_exist', 'api_is_active'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'debitor_id' => Yii::t('app', 'Debitor ID'),
			'api_exist' => Yii::t('app', 'API Exist'),
			'api_is_active' => Yii::t('app', 'API is active'),
		];
	}
}
