<?php

namespace common\models;

use backend\models\MarketingPartner;
use common\helper\ArrayHelper;
use common\models\behaviors\TimestampBehaviorSettings;
use common\modules\pricing\models\ConsolidatedPrice;
use common\modules\pricing\models\interfaces\PriceProviderInterface;
use common\modules\pricing\models\Price;
use common\modules\set\models\Chart;
use common\modules\set\models\ChartItemInterface;
use common\modules\set\models\Sets;
use components\i18n\TranslatableActiveRecord;
use deinhandy\yii2picasso\behavior\ImageAttributeBehavior;
use deinhandy\yii2picasso\behavior\ImageBehaviorConfig;
use deinhandy\yii2picasso\behavior\ImageUploadBehavior;
use deinhandy\yii2picasso\models\ImageCollection;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Url;

/**
 * This is the model class for table "articles".

 *
 * @property integer $id
 * @property boolean $is_active
 * @property boolean $is_active_erp
 * @property string $product_name
 * @property integer $giveaway_id
 * @property string $group_id
 * @property integer $bonus_id                      We have a Bon
 * @property string $color
 * @property string $color_label
 * @property integer $updated_at
 * @property integer $manufacturer_article_id
 * @property float $price_buy                        The price we pay for a device.
 * @property float $price_sale
 * @property float $price_feed
 * @property float $price_discounted                 Discount price for hardware only orders.
 * @property string $regional_code
 * @property integer $delivery_status_id
 * @property string $delivery_includes
 * @property integer $simcard_type
 * @property string $mpn
 * @property integer $memory
 * @property string $testing_institute
 * @property string $price_strike                     A price label, struck out.
 * @property string $variationLabel
 * @property string $image_overlay_text
 * @property int $tariff_set_id
 * @property string $mat_no
 * @property string $barcode
 * @property float $sunrisenetprice
 * @property bool $is_discountable
 * @property float $vat_rate
 * @property string $discount_type
 * @property bool $is_nettozone_active
 * @property string $api_updated_at
 * @property int $manufacturer_id
 * @property int $product_category_id
 * @property string $itemType
 * @property bool $override_positive_stock_to_reorder
 * @property string $additional_information
 * @property string $swisscom_id
 * @property string $term_link
 * @property string $description_1
 * @property string $description_2
 *
 * Active Query Attributes
 * @property ProductCategory $productCategory
 * @property Manufacturer $manufacturer
 * @property Tariffs $startTariff
 * @property string $manufacturerTitle
 * @property Article[] $allVariations
 * @property Article $topVariation
 * @property string $groupTitle
 * @property string $uniqueTitle
 * @property string $urlToGroup
 * @property string $urlToVariation
 * @property string $variationUrlPart
 * @property ArticleDetailsValues $articleDetailsValues
 * @property ArticleGroups $articleGroup
 * @property DeliveryStatus $deliveryStatus
 * @property OperatingSystems $operatingSystem
 * @property Sets $tariffSet
 * @property string $productCategoryLabel
 * @property string $uniqueShortName
 * @property string $uniqueShortNameWithManufacturer
 * @property string $shortName
 * @property array $allVariationLabels
 * @property Stock $stock
 * @property integer $insurance_duration
 * @property ConsolidatedPrice $prices
 * @property ArticleTariffs[] $articleTariffs
 * @property MarketingPartner[] $marketingPartners
 *
 * @property integer $isOffered
 *
 * @property ImageCollection $images
 */
class Article extends TranslatableActiveRecord implements ChartItemInterface, PriceProviderInterface
{
	const HARDWARE_ONLY_URL_PART = 'ohne-vertrag';
	const COMMISSION_ARTICLE_ID = 12529;
	const SIM_ONLY_ID = 5522;
	const BOOLEAN_ARTICLE_DETAILS = [
		'82',
		'27',
		'11',
		'35',
		'84',
		'74',
		'18',
		'54',
		'55',
		'56',
		'71',
		'62',
		'76',
		'78',
		'80',
		'81',
		'83',
		'17',
	];
	public $imageUrl = 'http://dogr.io/wow/suchapi/verybackend/muchstardust.png';
	public $itemType = 'article';
	protected $translateAttributes = [
		'delivery_includes',
		'image_overlay_text',
		'additional_information',
		'product_name',
		'color_label',
		'description_1',
		'description_2',
        'checkout_hint',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'articles';
	}

	/**
	 * @return ActiveQuery
	 */
	static public function getSimCards()
	{
		return self::find()->where(['product_category_id' => ProductCategory::SIM_CARD])->orderBy('product_name');
	}

	public function getProductNameAndId()
	{
		return $this->product_name . ' [ID: ' . $this->id . ']';
	}

	/**
	 * @param bool $withNamespace
	 * @return string
	 */
	public static function className($withNamespace = true)
	{
		$class = parent::className();
		if ($withNamespace) {
			return $class;
		} else {
			$tmp = explode('\\', $class);
			return end($tmp);
		}
	}

	/**
	 * @return Article[]
	 * @var $charts Chart[]
	 */
	public static function getTopArticleHandys()
	{
		$top_article_handys = [];
		$top_article_set_id = Sets::findOne(['system_title' => Sets::TOP_ARTICLE_SMARTPHONES])->id;
		$charts = Chart::find()->where(['set_id' => $top_article_set_id])->orderBy('position')->all();

		foreach ($charts as $chart) {
			$top_article_handys[] = self::findOne($chart->product_id);
		}

		return $top_article_handys;
	}

	/**
	 * @return Article[]
	 */
	public static function getTopArticleTablets()
	{
		$top_article_tablets = [];
		$top_article_set_id = Sets::findOne(['system_title' => Sets::TOP_ARTICLE_TABLETS])->id;
		$charts = Chart::find()->where(['set_id' => $top_article_set_id])->orderBy('position')->all();

		foreach ($charts as $chart) {
			$top_article_tablets[] = self::findOne($chart->product_id);
		}

		return $top_article_tablets;
	}

	/**
	 * @param Manufacturer[] $manufacturers
	 * @param ActiveQuery $query
	 */
	static public function filterByManufacturer(array $manufacturers, ActiveQuery &$query)
	{
		$manufacturer_ids = [];

		/** @var Manufacturer $manufacturer */
		foreach ($manufacturers as $manufacturer) {
			$manufacturer_ids[] = $manufacturer->id;
		}

		$query->joinWith('articleGroup')->andWhere(['manufacturer_id' => $manufacturer_ids]);
	}

	/**
	 * @param array $colors
	 * @param ActiveQuery $query
	 */
	static public function filterByColor(array $colors, ActiveQuery &$query)
	{
		$query->andWhere(['or like', 'color', $colors]);
	}

	static public function filterByDisplaySize(array $map, array $filter, ActiveQuery &$query)
	{
		$column = ArticleDetails::getDetailAttribute(ArticleDetails::DETAIL_DISPLAY_SIZE_INCH);

		$sizes = ['or'];
		foreach ($filter as $size) {
			$sizes[] = ArrayHelper::createConditionForColumn($column, $map[$size]);
		}

		$query->joinWith('articleDetailsValues')
			->andWhere($sizes);
	}

	static public function filterByCameraSize(array $map, array $filter, ActiveQuery &$query)
	{
		$column = ArticleDetails::getDetailAttribute(ArticleDetails::DETAIL_MAIN_CAMERA);

		$cameraSizes = [];
		foreach ($filter as $size) {
			$cameraSizes[] = ArrayHelper::createConditionForColumn($column, $map[$size]);
		}

		$query->joinWith('articleDetailsValues')
			->andWhere([$column => $cameraSizes]);
	}

	static public function filterByMemorySize(array $filter, ActiveQuery &$query)
	{
		$query->andWhere(['memory' => $filter]);
	}

	static public function filterBySystem(array $filter, ActiveQuery &$query)
	{
		$query->joinWith('articleGroup')->andWhere(['os_id' => $filter]);
	}

	/**
	 * @param string $group_url_text
	 * @param string $variationName
	 *
	 * @return Article
	 */
	public static function getModelByUrlParameter($group_url_text, $variationName = null)
	{
		$model = null;
		$group = null;

		if (!$variationName || $variationName == self::HARDWARE_ONLY_URL_PART) {
			/** @var ArticleGroups $group */
			$group = ArticleGroups::findOne(['title' => $group_url_text]);
			if ($group) {
				$model = Article::findOne([$group->top_variation_id]);
			}
		} else {
			$model = self::findByVariation($group_url_text, $variationName);
		}

		if (!$model && $group) //no top_variation selected
		{
			$model = Article::findOne(['group_id' => $group->id, 'is_active' => true]);
		}

		return $model;
	}

	/**
	 * with the new url structure the $variationName could be a color_label or memory
	 *
	 * @param string $group_url_text
	 * @param string $variationName
	 *
	 * @return Article
	 */
	protected static function findByVariation($group_url_text, $variationName)
	{
		if ($model = Article::findOne(
			['group_id' => ArticleGroups::findOne(['title' => $group_url_text])->id, 'color_label' => $variationName, 'is_active' => true]
		)
		) {
			return $model;
		}

		if ($model = Article::findOne(
			[
				'group_id' => ArticleGroups::findOne(['title' => $group_url_text])->id,
				'memory' => str_replace('gb', '', $variationName),
				'is_active' => true,
			]
		)
		) {
			return $model;
		}

		$arr = explode('_', $variationName);
		$memory = str_replace('gb', '', $arr[count($arr) - 1]);
		unset($arr[count($arr) - 1]);
		$color = implode(' ', $arr);
		$model = Article::findOne(
			[
				'group_id' => ArticleGroups::findOne(['title' => $group_url_text])->id,
				'color_label' => $color,
				'memory' => $memory,
				'is_active' => true,
			]
		);

		if (!$model) {
			$model = ArticleGroups::findOne(['title' => $group_url_text])->topVariation;
		}

		return $model;
	}

	/**
	 * Adds related models as fields.
	 * @return array
	 */
	public function fields()
	{
		$fields = [
			'is_active',
			'id',
			'manufacturer_id',
			'product_category_id',
			'product_name',
			'group_id',
			'color',
			'color_label',
			'delivery_status_id',
			'delivery_includes',
			'memory',
			'image_overlay_text',
			'stock_id' => 'stockId',
			'images',
			'prices' => 'allPrices',
			'product_category_frontend_route_name' => 'productCategoryFrontendRouteName',
			'mat_no',
			'mpn',
			'description_1',
			'description_2',
			'show_technical_information',
			'manufacturer_title' => 'manufacturerTitle',
            'bonus_id',
            'checkout_hint',
            'custom_stock',
		];


		if ($this->isInsurance()) {
			$fields = array_merge($fields, [
				'insurance_duration',
				'additional_information',
				'term_link',
				'highlights',
			]);
		}

		return $fields;
	}

	public function extraFields()
	{
		return array_merge(
			parent::fields(),
			[
				'articleDetails',
				'accessories',
			]
		);
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		$accountNamespace = Yii::$app->params['picasso_namespace'];
		$collectionName = self::tableName();

		return \yii\helpers\ArrayHelper::merge(
			parent::behaviors(),
			[
				'datetime' => TimestampBehaviorSettings::updatedAt(),
				'image_upload' => [
					'class' => ImageUploadBehavior::class,
					'attributes' => [
						'images' => new ImageBehaviorConfig($accountNamespace, $collectionName),
					],
				],
				'image_access' => [
					'class' => ImageAttributeBehavior::class,
					'attributes' => ['images'],
				],
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$requiredAttributes = [
			'product_name',
			'color',
			'color_label',
			'price_sale',
			'delivery_status_id',
			'simcard_type',
			'memory',
			'manufacturer_article_id',
		];

		if (in_array(
			$this->product_category_id,
			[
				ProductCategory::SMARTPHONE,
				ProductCategory::TABLET,
			]
		)) {
			$requiredAttributes[] = 'group_id';
		}

		if($this->isSimcard() == 1){
            $requiredAttributes = [];
        }


		return parent::translatableRules([
			[$requiredAttributes, 'required'],
			[['giveaway_id', 'bonus_id', 'group_id', 'delivery_status_id', 'simcard_type'], 'integer'],
			[['price_sale', 'price_feed', 'price_buy', 'price_strike', 'memory', 'price_discounted'], 'number'],
			[['product_name', 'color'], 'string', 'max' => 80],
			[['mat_no','testing_institute', 'image_overlay_text', 'term_link'], 'string', 'max' => 255],
			[['color_label', 'manufacturer_article_id'], 'string', 'max' => 40],
			[['regional_code'], 'string', 'max' => 2],
			[['mpn'], 'string', 'max' => 64],
			[['description_1', 'description_2'], 'string'],
			[['additional_information'], 'string'],
			[['checkout_hint'], 'string'],
			[['custom_stock'], 'string'],
			[
				[
					'is_active',
					'is_active_erp',
					'override_positive_stock_to_reorder',
					'show_technical_information'
				],
				'boolean',
			],
			[
				[
					'mat_no',
					'articleDetailsValues',
					'delivery_includes',
					'swisscom_id',
					'marketingPartners',
					'images',
                    'checkout_hint'
				],
				'safe',
			],
			[
				['images'],
				'file',
				'extensions'=>['png', 'jpg', 'jpeg'],
				'wrongExtension' => Yii::t('app', 'wrong extension - only PNG and JP(E)G allowed'),
				'maxFiles' => 8,
			],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID (ERP)',
			'mat_no' => 'MAT_NO (Kasse)',
			'is_active' => Yii::t('app', 'Is Active'),
			'is_active_erp' => Yii::t('app', 'Is Active Erp'),
			'product_name' => Yii::t('app', 'Product Name'),
			'giveaway_id' => 'Giveaway ID',
			'group_id' => Yii::t('app', 'Group ID'),
			'bonus_id' => 'Bon Legal Text anzeigen',
			'color' => Yii::t('app', 'Color'),
			'color_label' => Yii::t('app', 'Color Label'),
			'manufacturer_article_id' => Yii::t('app', 'EAN'),
			'swisscom_id' => Yii::t('app', 'Swisscom-ID'),
			'price_sale' => Yii::t('app', 'Price Sale'),
			'price_buy' => Yii::t('app', 'Price Buy'),
			'price_discounted' => Yii::t('app', 'Sales Price'),
			'regional_code' => 'Regional Code',
			'delivery_status_id' => Yii::t('app', 'Delivery Status'),
			'delivery_includes' => Yii::t('app', 'Delivery Includes'),
			'simcard_type' => 'Simcard Type',
			'mpn' => Yii::t('app', 'Manufacturer\'s Part Number'),
			'memory' => Yii::t('app', 'Memory'),
			'testing_institute' => Yii::t('app', 'Testing Institute'),
			'price_strike' => Yii::t('app', 'Price Strike'),
			'manufacturerTitle' => Yii::t('app', 'Manufacturer Title'),
			'groupTitle' => Yii::t('app', 'Article Group'),
			'deliveryStatus' => Yii::t('app', 'Delivery Status'),
			'productCategoryLabel' => Yii::t('app', 'Product Category'),
			'image_overlay_text' => Yii::t('app', 'Image Overlay Text'),
			'shortName' => Yii::t('app', 'Short Name'),
			'insurance_duration' => Yii::t('app', 'Insurance Duration'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'override_positive_stock_to_reorder' => Yii::t('app', 'Ignore stock for availability'),
			'additional_information' => Yii::t('app', 'Additional Information'),
			'term_link' => Yii::t('app', 'Term Link'),
			'images' => Yii::t('app', 'Article Images'),
			'description_1' => Yii::t('app', 'Description') . ' 1',
			'description_2' => Yii::t('app', 'Description') . ' 2',
			'show_technical_information' => Yii::t('app', 'Show Technical Information'),
            'checkout_hint' => Yii::t('app', 'Hinweistext'),
            'custom_stock' => Yii::t('app', 'Custom Stock'),
		];
	}

	/**
	 * @param ArticleDetailsValues $articleDetailsValues
	 */
	public function setArticleDetailsValues(ArticleDetailsValues $articleDetailsValues)
	{
		$this->articleDetailsValues = $articleDetailsValues;
	}

	/**
	 * @return ProductCategory|ActiveQuery
	 */
	public function getProductCategory()
	{
		return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
	}

	public function getStock()
	{
		return $this->hasOne(Stock::className(), ['id' => 'id']);
	}

	public function getStockQuantity()
	{
		return $this->stock->quantity ?? 0;
	}

	public function getProductCategoryLabel()
	{
		return ucfirst($this->productCategory->type);
	}

	/**
	 * @return string
	 */
	public function getProductCategoryFrontendRouteName()
	{
		return $this->productCategory->frontendRouteName;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getManufacturer()
	{
		return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
	}

	/**
	 * @param bool $asUrlPart
	 * @return array
	 */
	public function getAllVariationLabels($asUrlPart = false)
	{
		$activeLabels = [];
		foreach ($this->getAllVariations() as $variation) {
			foreach ($variation->getVariationLabel(true, $asUrlPart) as $label) {
				$activeLabels[$label] = $label;
			}
		}

		return $activeLabels;
	}

	/**
	 * @return Article[]|array
	 */
	public function getAllVariations()
	{
		return Article::findAll(['group_id' => $this->group_id, 'is_active' => true]);
	}

	/**
	 * @param bool $asArray
	 * @param bool $asUrlPart
	 * @return array|string
	 */
	public function getVariationLabel($asArray = false, $asUrlPart = false)
	{
		$variation = [
			$this->color_label ?: $this->color,
			$asUrlPart ? $this->memory . 'gb' : $this->memory . ' GB',
		];

		if (!$asArray)
			return join(' ', $variation);

		return $variation;
	}

	/**
	 * Finds models with the same color and various memory
	 * @return Article[]
	 */
	public function getMemoryVariations()
	{
		return Article::find()->where(['group_id' => $this->group_id, 'is_active' => true, 'color' => $this->color])->select('id, memory')->distinct()->all();
	}

	public function variationExistWithTariff($variation, Tariffs $tariff)
	{
		return ArticleTariffs::find()->where([
			'tariff_id' => $tariff->id,
			'article_id' => $variation->id,
			'fulfillment_partner_id' => $tariff->fulfillment_partner_id,
			'is_active' => true,
		])->exists();
	}

	/**
	 * Finds models with the same memory size and various colors
	 * @return Article[]
	 */
	public function getColorVariations()
	{
		return Article::find()->where(['group_id' => $this->group_id, 'is_active' => true, 'memory' => $this->memory])->select('id, color, color_label')->distinct()->all();
	}

	/**
	 * @return array|Article[]
	 */
	public function getAllMemoryVariations()
	{
		return Article::find()->where(['group_id' => $this->group_id, 'is_active' => true])->select('memory')->distinct()->all();
	}

	/**
	 * @return array|Article[]
	 */
	public function getAllColorVariations()
	{
		return Article::find()->where(['group_id' => $this->group_id, 'is_active' => true])->select('color, color_label')->distinct()->all();
	}

	/**
	 * @return string
	 */
	public function getUniqueTitle()
	{
		return $this->product_name . ' (' . $this->color_label . ', ' . $this->memory . ' GB)';
	}

	/**
	 * @return string
	 */
	public function getUniqueShortName()
	{
		$name = $this->product_name;
		if (isset($this->articleGroup->short_name)) {
			$name = $this->articleGroup->short_name;
		}
		return $name . ' (' . $this->color_label . ', ' . $this->memory . ' GB)';
	}

	/**
	 * @return string
	 */
	public function getUniqueShortNameWithManufacturer()
	{
		return $this->articleGroup->manufacturerTitle . " " . $this->articleGroup->short_name . ' (' . $this->color_label . ', ' . $this->memory . ' GB)';
	}

	/**
	 * @return string
	 */
	public function getUrlToGroup()
	{
		if (isset($this->articleGroup)) {
			$category = $this->productCategory;
			return $category->getUrlRepresentation() . strtolower($this->manufacturerTitle) . '/' . $this->groupTitle;
		}

		return Url::home();
	}

	/**
	 * @return string
	 */
	public function getUrlToVariation()
	{
		return $this->urlToGroup . '/' . $this->variationUrlPart;
	}

	/**
	 * @param $detail_id
	 * @return mixed
	 */
	public function getValue($detail_id)
	{
		return $this->{'detail_' . $detail_id};
	}

	/**
	 * @return ActiveQuery
	 * @deprecated Use ArticleGroup's startTariff property
	 */
	public function getStartTariff()
	{
		$articleGroup = $this->getArticleGroup()->one();
		return $articleGroup->hasOne(Tariffs::className(), ['id' => 'start_tariff_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getArticleGroup()
	{
		return $this->hasOne(ArticleGroups::className(), ['id' => 'group_id']);
	}

	/**
	 * @return string
	 */
	public function getGroupTitle()
	{
		return $this->articleGroup->title ?? null;
	}

	/**
	 * @return string
	 */
	public function getManufacturerTitle()
	{
		return $this->manufacturer->title ?? null;
	}

	/**
	 * @return string
	 */
	public function getShortName()
	{
		return $this->articleGroup->short_name ?? null;
	}

	/**
	 * Please note: I am fully aware that this implementation is pretty dirty; but that is what it is: A quick and dirty
	 * solution for a problem at hand - getting all article detail values in a structured format on our Frontend-Backend
	 * REST-API. --Kolja
	 *
	 * @return array
	 */
	public function getArticleDetails()
	{
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand(
			/** @lang MySQL */
			'SELECT * FROM article_details
			 INNER JOIN article_details_group USING (group_id)
			 ORDER BY article_details_group.sort, article_details.sort_id');

		$result = $command->queryAll();

		$details = [];

		foreach ($result as $row) {
			$details['detail_' . $row['detail_id']] = $row;
		}

		$detailValues = $this->articleDetailsValues->attributes ?? [];

		$namedDetails = [];
		$groups = [];
		foreach ($detailValues as $detailValueKey => $detailValue) {
			if (strpos($detailValueKey, 'detail_') === false
				|| isset($details[$detailValueKey]) === false) {
				continue;
			}

			$detail = $details[$detailValueKey];
			$detailId = isset($detail['detail_id']) ? $detail['detail_id'] : null;

			$groups[$detail['group_id']] = [
				'title' => $detail['title'],
				'sort' => $detail['sort'],
			];

			$determinedValue = in_array($detailId, self::BOOLEAN_ARTICLE_DETAILS) ? (bool)$detailValue : $detailValue;

			// For Smartphones category, we want to display bool=false values as well; for all other articles NOT
			if ($this->product_category_id == ProductCategory::SMARTPHONE || empty($determinedValue) === false) {
				$namedDetails[$detail['group_id']][] = [
					'title' => $detail['detail_name'],
					'value' => $determinedValue,
					'unit' => $detail['unit'],
					'sort' => $detail['sort_id'],
					'detail_id' => $detail['detail_id']
				];
			}
		}

		$namedDetails = $this->augmentArticleDetailsWithOperatingSystemInformation($namedDetails);

		uasort($groups, function($a, $b) {
			return $a['sort'] <=> $b['sort'];
		});

		$finalResult = [];
		foreach ($groups as $groupId => $groupName) {

			if (isset($namedDetails[$groupId]) === false) {
				continue;
			}

			uasort($namedDetails[$groupId], function($a, $b) {
				return $a['sort'] <=> $b['sort'];
			});

			$finalResult[] = [
				'group' => $groupName,
				'items' => array_values($namedDetails[$groupId]),
			];
		}

		return $finalResult;
	}

	private function augmentArticleDetailsWithOperatingSystemInformation($namedDetails)
	{
		$platform = $this->articleGroup->operatingSystem->platform->name ?? null;
		$version = $this->articleGroup->operatingSystem->version ?? null;
		$namedDetails[ArticleDetailsGroup::GROUP_SOFTWARE][] = [
			'title' => Yii::t('app', 'Operating System'),
			'value' => $platform,
			'unit' => '',
			'sort' => 0,
		];

		$namedDetails[ArticleDetailsGroup::GROUP_SOFTWARE][] = [
			'title' => Yii::t('app', 'Version'),
			'value' => $version,
			'unit' => '',
			'sort' => 1,
		];

		return $namedDetails;
	}

	/**
	 * @return ArticleDetailsValues|ActiveQuery
	 */
	public function getArticleDetailsValues()
	{
		return $this->hasOne(ArticleDetailsValues::className(), ['article_id' => 'id']);
	}

	public function getArticleDetailsValue($attributeName)
	{
		$details = ArticleDetailsValues::findOne(['article_id' => $this->id]);
		return $details->{$attributeName};
	}

	public function getAccessories()
	{
		$accessories = [];

		if (isset($this->articleGroup->accessorySet)) {
			$accessories = array_values($this->articleGroup->accessorySet->productList);
		}

		if (empty($accessories) && isset($this->manufacturer->accessorySet)) {
			$accessories = array_values($this->manufacturer->accessorySet->productList);
		}

		return $accessories;
	}

	/**
	 * @return ActiveQuery | DeliveryStatus
	 */
	public function getDeliveryStatus()
	{
		return $this->hasOne(DeliveryStatus::className(), ['id' => 'delivery_status_id']);
	}

	public function getDeliveryStatusTitle()
	{
		return $this->deliveryStatus->label ?? null;
	}

	public function getNextDeliveryDate()
	{
		return $this->stock->next_delivery_date ?? null;
	}

	public function getNextDeliveryAmount()
	{
		return $this->stock->next_delivery_amount ?? null;
	}

	public function getIsOffered()
	{
		return (isset($this->offer)) ? true : false;
	}

	/**
	 * @return ActiveQuery | OperatingSystems
	 */
	public function getOperatingSystem()
	{
		return $this->articleGroup->operatingSystem ?? null;
	}

	/**
	 * @return string
	 */
	public function getVariationUrlPart()
	{
		return ($this->color_label ? str_replace(' ', '_', $this->color_label) : '')
		. ($this->memory ? '_' . $this->memory . 'gb' : '');
	}

	/**
	 * @return array|null|Sets
	 */
	public function getTariffSet()
	{
		if (($this->tariffSet = Sets::find()->where(['id' => $this->tariff_set_id])->one()) == false) {
			$this->tariffSet = Sets::find()->where(['id' => $this->articleGroup->tariff_set_id])->one();
		}

		return $this->tariffSet;
	}

	/**
	 * @param Sets $tariffSet
	 * @return $this
	 */
	public function setTariffSet(Sets $tariffSet = null)
	{
		$this->tariffSet = $tariffSet;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getMissingTariffs()
	{
		$tariffs = Tariffs::findBySql('SELECT * FROM tariffs WHERE id NOT IN (SELECT tariff_id FROM article_tariffs WHERE article_id = :article_id)',
			[':article_id' => $this->id])->all();

		return $tariffs;
	}

	/**
	 * @return Article
	 */
	public function getTopVariation()
	{
		return $this->articleGroup->topVariation ?? null;
	}

	public function saveDetailsForAllVariations()
	{
		$articles = $this->articleGroup->articles;
		// unset($this->articleDetailsValues->article_id);
		$detailsValues = $this->articleDetailsValues->attributes;
		unset($detailsValues['article_id']);

		foreach ($articles as $article) {
			if ($article->id != $this->id) {
				// failover - just in case of ...
				if(null === $article->articleDetailsValues) {
					$article->articleDetailsValues = new ArticleDetailsValues();
				}
				$article->articleDetailsValues->setAttributes($detailsValues);
				$article->articleDetailsValues->article_id = $article->id;
				$article->articleDetailsValues->save();
			}
		}
	}

	/**
	 * @return ActiveQuery|ArticleTariffs
	 */
	public function getArticleTariffs()
	{
		return $this->hasMany(ArticleTariffs::class, ['article_id' => 'id']);
	}

	public function getDisplaySpecs()
	{
		return $this->articleDetailsValues->attributes['detail_1'];
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function hasGroup(): bool
	{
		return (empty($this->articleGroup) === false);
	}

	public function getPrices(): ConsolidatedPrice
	{
		$price = new ConsolidatedPrice();
		$price->getPriceOnce()->add($this->getPriceOnce());

		return $price;
	}

	public function getPriceOnce(): Price
	{
		return new Price($this->price_discounted ?? $this->price_sale);
	}

	public function getPriceMonthly() {
		return null;
	}

	public function getSmallImageUrl(): string
	{
		if ($this->hasImage()) {
			return $this->images[0] ?? '';
		}
		return '';
	}

	public function hasImage() : bool
	{
		return (empty($this->images[0]) === false);
	}

	public function isSimcard() : bool
	{
		return $this->product_category_id == ProductCategory::SIM_CARD;
	}

	/**
	 * @return ActiveQuery|MarketingPartner[]
	 */
	public function getMarketingPartners()
	{
		return $this->hasMany(MarketingPartner::class, ['id' => 'partner_id'])
			->viaTable(ArticleMarketingPartner::tableName(), ['article_id' => 'id']);
	}

	public function setMarketingPartners($value)
	{
		/** @var ArticleMarketingPartner $marketingPartner */
		$removedArticleMarketingPartners = ArticleMarketingPartner::find()
			->where(['NOT IN', 'partner_id', $value])
			->andWhere(['article_id' => $this->id])
			->all();

		foreach ($removedArticleMarketingPartners as $marketingPartner) {
			$marketingPartner->delete();
		}

		foreach ($value as $partnerId) {
			if (ArticleMarketingPartner::find()
				->where(['partner_id' => $partnerId])
				->andWhere(['article_id' => $this->id])
				->exists()
			) {
				continue;
			}
			$marketingPartner = new ArticleMarketingPartner();
			$marketingPartner->partner_id = (int)$partnerId;
			$marketingPartner->article_id = $this->id;
			$marketingPartner->save();
		}
	}

	public function getAllPrices(): array
	{
		return array_merge($this->getPrices()->toArray(), [
			'price_sale' => $this->price_sale,
			'vat_rate' => $this->vat_rate,
			'price_discounted' => $this->price_discounted,
			'price_strike' => $this->price_strike,
		]);
	}

	public function isInsurance(): bool
	{
		return $this->product_category_id === ProductCategory::INSURANCE;
	}

	public function getPrice_sale()
	{
		return $this->price_discounted ?? $this->price_sale;
	}

	public function isAccessory()
	{
		return in_array($this->product_category_id, ProductCategory::ACCESSORIES);
	}

	public function getFeedDescription()
	{
		if ($this->description_1) {
			return $this->description_1 . ($this->description_2 ?? '');
		}

		if (isset($this->articleGroup->article_feed_description) && $this->articleGroup->article_feed_description) {
			return $this->articleGroup->article_feed_description;
		}

		return $this->product_name;
	}

	public function hasSwisscomId(): bool
	{
		return !empty($this->swisscom_id);
	}

	public function getStockId()
	{
		return $this->stock ? $this->id : null;
	}
}
