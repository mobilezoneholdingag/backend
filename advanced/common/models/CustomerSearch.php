<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CustomerSearch represents the model behind the search form about `common\models\Customer`.
 */
class CustomerSearch extends Customer
{
	public $first_name;
	public $last_name;
	public $city;
	public $number;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'passport_type', 'number'], 'integer'],
			[
				[
					'email',
					'source',
					'source_details',
					'password_hash',
					'password_salt',
					'password_reset_token',
					'language',
					'nationality',
					'passport_id',
					'birthday',
					'place_of_birth',
					'first_name',
					'last_name',
					'city',
					'number',
					'updated_at',
					'created_at',
				],
				'safe',
			],

		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Customer::find();

		$query->joinWith(['customerAddresses', 'customerPhoneNumbers']);

		// add conditions that should always apply here

		/** @var $dataProvider */
		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
			]
		);

		$dataProvider->sort->attributes['customerAddress'] = [
			'asc' => ['customerAddress.firstname' => SORT_ASC],
			'desc' => ['customerAddress.firstname' => SORT_DESC],
		];

		$dataProvider->sort->attributes['customerAddress'] = [
			'asc' => ['customerAddress.lastname' => SORT_ASC],
			'desc' => ['customerAddress.lastname' => SORT_DESC],
		];

		$dataProvider->sort->attributes['customerAddress'] = [
			'asc' => ['customerAddress.city' => SORT_ASC],
			'desc' => ['customerAddress.city' => SORT_DESC],
		];

		$dataProvider->sort->attributes['customerPhoneNumber'] = [
			'asc' => ['customerPhoneNumber.number' => SORT_ASC],
			'desc' => ['customerPhoneNumber.number' => SORT_DESC],
		];

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere(
			[
				'id' => $this->id,
				'passport_type' => $this->passport_type,
			]
		);

		$query->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', 'source', $this->source])
			->andFilterWhere(['like', 'source_details', $this->source_details])
			->andFilterWhere(['like', 'password_hash', $this->password_hash])
			->andFilterWhere(['like', 'password_salt', $this->password_salt])
			->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
			->andFilterWhere(['like', 'language', $this->language])
			->andFilterWhere(['like', 'nationality', $this->nationality])
			->andFilterWhere(['like', 'passport_id', $this->passport_id])
			->andFilterWhere(['like', 'birthday', $this->birthday])
			->andFilterWhere(['like', 'place_of_birth', $this->place_of_birth])
			->andFilterWhere(['like', 'customer_address.firstname', $this->first_name])
			->andFilterWhere(['like', 'customer_address.lastname', $this->last_name])
			->andFilterWhere(['like', 'customer_address.city', $this->city])
			->andFilterWhere(['like', 'customer_phone_number.number', $this->number])
			->andFilterWhere(['like', static::tableName() . '.updated_at', $this->updated_at])
			->andFilterWhere(['like', static::tableName() . '.created_at', $this->created_at])
		;

		return $dataProvider;
	}
}
