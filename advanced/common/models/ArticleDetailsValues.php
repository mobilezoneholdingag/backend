<?php

namespace common\models;

use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "article_details_values".
 *
 * @property integer $article_id
 * @property string $detail_1
 * @property string $detail_2
 * @property string $detail_3
 * @property string $detail_4
 * @property string $detail_5
 * @property string $detail_6
 * @property string $detail_7
 * @property string $detail_8
 * @property string $detail_9
 * @property string $detail_10
 * @property string $detail_11
 * @property string $detail_12
 * @property string $detail_13
 * @property string $detail_14
 * @property string $detail_15
 * @property string $detail_16
 * @property string $detail_17
 * @property string $detail_18
 * @property string $detail_19
 * @property string $detail_20
 * @property string $detail_21
 * @property string $detail_22
 * @property string $detail_23
 * @property string $detail_24
 * @property string $detail_25
 * @property string $detail_26
 * @property string $detail_27
 * @property string $detail_28
 * @property string $detail_29
 * @property string $detail_30
 * @property string $detail_31
 * @property string $detail_32
 * @property string $detail_33
 * @property string $detail_34
 * @property integer $detail_35
 * @property string $detail_36
 * @property string $detail_37
 * @property string $detail_38
 * @property string $detail_39
 * @property string $detail_40
 * @property string $detail_41
 * @property string $detail_42
 * @property string $detail_43
 * @property string $detail_44
 * @property string $detail_45
 * @property string $detail_46
 * @property string $detail_47
 * @property string $detail_48
 * @property string $detail_49
 * @property string $detail_50
 * @property string $detail_51
 * @property string $detail_52
 * @property string $detail_53
 * @property string $detail_54
 * @property string $detail_55
 * @property string $detail_56
 * @property string $detail_57
 * @property string $detail_58
 * @property string $detail_59
 * @property string $detail_60
 * @property string $detail_61
 * @property string $detail_62
 * @property string $detail_63
 * @property string $detail_64
 * @property string $detail_65
 * @property string $detail_66
 * @property string $detail_67
 * @property string $detail_68
 * @property string $detail_69
 * @property string $detail_70
 * @property string $detail_71
 * @property string $detail_72
 * @property string $detail_73
 * @property string $detail_74
 * @property string $detail_75
 * @property string $detail_76
 * @property string $detail_77
 * @property string $detail_78
 * @property string $detail_79
 * @property string $detail_80
 * @property string $detail_81
 * @property string $detail_82
 * @property string $detail_83
 * @property string $detail_84
 * @property string $detail_85
 * @property string $detail_86
 * @property string $detail_87
 * @property string $detail_88
 * @property string $detail_89
 * @property string $detail_90
 * @property string $detail_91
 * @property string $detail_92
 * @property string $detail_93
 * @property string $detail_94
 * @property string $detail_95
 * @property string $detail_96
 * @property string $detail_97
 * @property string $detail_98
 * @property string $detail_99
 * @property string $detail_100
 *
 * @property Article $article
 */
class ArticleDetailsValues extends ActiveRecord
{
	const SCENARIO_ACCESSORY = 'accessory';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'article_details_values';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return (Yii::$app->id == 'app-backend') ? [LoggableBehavior::className()] : [];
	}

	public function save($runValidation = true, $attributeNames = null)
	{
		$this->presetValues();
		return parent::save($runValidation, $attributeNames);
	}

	public function beforeValidate()
	{
		if (in_array($this->article->product_category_id ?? 0, ProductCategory::ACCESSORIES)) {
			$this->setScenario(self::SCENARIO_ACCESSORY);
		}
		return parent::beforeValidate();
	}

	public function scenarios()
	{
		return array_merge(parent::scenarios(), [
			self::SCENARIO_ACCESSORY => ['article_id']
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['detail_4', 'detail_11', 'detail_17', 'detail_18', 'detail_22', 'detail_23', 'detail_24', 'detail_25',
				'detail_27', 'detail_35', 'detail_42', 'detail_43', 'detail_44', 'detail_45', 'detail_54', 'detail_55',
				'detail_56', 'detail_57', 'detail_62', 'detail_63', 'detail_64', 'detail_68', 'detail_69', 'detail_70',
				'detail_71', 'detail_73', 'detail_74', 'detail_76', 'detail_78', 'detail_80', 'detail_81', 'detail_82',
				'detail_83', 'detail_84'], 'boolean'],
			[[
				'detail_1', 'detail_2', 'detail_3', 'detail_7', 'detail_8', 'detail_9',
				'detail_10', 'detail_13', 'detail_14', 'detail_15', 'detail_16', 'detail_19', 'detail_20',
				'detail_28', 'detail_29', 'detail_30', 'detail_31', 'detail_32',
				'detail_34', 'detail_36', 'detail_38', 'detail_39', 'detail_40', 'detail_41',
				'detail_47', 'detail_48', 'detail_50', 'detail_52', 'detail_59', 'detail_60', 'detail_65', 'detail_66',
				'detail_77', 'detail_85', 'detail_86', 'detail_87', 'detail_88'
			], 'string', 'max' => 80],
			[['article_id'], 'integer'],
			[['detail_7', 'detail_8', 'detail_9', 'detail_10', 'detail_13', 'detail_16', 'detail_29',
				'detail_38', 'detail_39', 'detail_40', 'detail_41', 'detail_87',
				'article_id'
			], 'required', 'on' => self::SCENARIO_DEFAULT],
			['detail_31', 'match', 'pattern' => '/^\d+ x \d+$/', 'message' => 'please use the following format: "{height} x {width}"']
		];
	}

	/**
	 * @inheritdoc
	 */
	public
	function attributeLabels()
	{
		return [
			'article_id' => Yii::t('app', 'Article ID'),
			'detail_1' => Yii::t('app', 'Detail 1'),
			'detail_2' => Yii::t('app', 'Detail 2'),
			'detail_3' => Yii::t('app', 'Detail 3'),
			'detail_4' => Yii::t('app', 'Detail 4'),
			'detail_5' => Yii::t('app', 'Detail 5'),
			'detail_6' => Yii::t('app', 'Detail 6'),
			'detail_7' => Yii::t('app', 'Detail 7'),
			'detail_8' => Yii::t('app', 'Detail 8'),
			'detail_9' => Yii::t('app', 'Detail 9'),
			'detail_10' => Yii::t('app', 'Detail 10'),
			'detail_11' => Yii::t('app', 'Detail 11'),
			'detail_12' => Yii::t('app', 'Detail 12'),
			'detail_13' => Yii::t('app', 'Detail 13'),
			'detail_14' => Yii::t('app', 'Detail 14'),
			'detail_15' => Yii::t('app', 'Detail 15'),
			'detail_16' => Yii::t('app', 'Detail 16'),
			'detail_17' => Yii::t('app', 'Detail 17'),
			'detail_18' => Yii::t('app', 'Detail 18'),
			'detail_19' => Yii::t('app', 'Detail 19'),
			'detail_20' => Yii::t('app', 'Detail 20'),
			'detail_21' => Yii::t('app', 'Detail 21'),
			'detail_22' => Yii::t('app', 'Detail 22'),
			'detail_23' => Yii::t('app', 'Detail 23'),
			'detail_24' => Yii::t('app', 'Detail 24'),
			'detail_25' => Yii::t('app', 'Detail 25'),
			'detail_26' => Yii::t('app', 'Detail 26'),
			'detail_27' => Yii::t('app', 'Detail 27'),
			'detail_28' => Yii::t('app', 'Detail 28'),
			'detail_29' => Yii::t('app', 'Detail 29'),
			'detail_30' => Yii::t('app', 'Detail 30'),
			'detail_31' => Yii::t('app', 'Detail 31'),
			'detail_32' => Yii::t('app', 'Detail 32'),
			'detail_33' => Yii::t('app', 'Detail 33'),
			'detail_34' => Yii::t('app', 'Detail 34'),
			'detail_35' => Yii::t('app', 'Detail 35'),
			'detail_36' => Yii::t('app', 'Detail 36'),
			'detail_37' => Yii::t('app', 'Detail 37'),
			'detail_38' => Yii::t('app', 'Detail 38'),
			'detail_39' => Yii::t('app', 'Detail 39'),
			'detail_40' => Yii::t('app', 'Detail 40'),
			'detail_41' => Yii::t('app', 'Detail 41'),
			'detail_42' => Yii::t('app', 'Detail 42'),
			'detail_43' => Yii::t('app', 'Detail 43'),
			'detail_44' => Yii::t('app', 'Detail 44'),
			'detail_45' => Yii::t('app', 'Detail 45'),
			'detail_46' => Yii::t('app', 'Detail 46'),
			'detail_47' => Yii::t('app', 'Detail 47'),
			'detail_48' => Yii::t('app', 'Detail 48'),
			'detail_49' => Yii::t('app', 'Detail 49'),
			'detail_50' => Yii::t('app', 'Detail 50'),
			'detail_51' => Yii::t('app', 'Detail 51'),
			'detail_52' => Yii::t('app', 'Detail 52'),
			'detail_53' => Yii::t('app', 'Detail 53'),
			'detail_54' => Yii::t('app', 'Detail 54'),
			'detail_55' => Yii::t('app', 'Detail 55'),
			'detail_56' => Yii::t('app', 'Detail 56'),
			'detail_57' => Yii::t('app', 'Detail 57'),
			'detail_58' => Yii::t('app', 'Detail 58'),
			'detail_59' => Yii::t('app', 'Detail 59'),
			'detail_60' => Yii::t('app', 'Detail 60'),
			'detail_61' => Yii::t('app', 'Detail 61'),
			'detail_62' => Yii::t('app', 'Detail 62'),
			'detail_63' => Yii::t('app', 'Detail 63'),
			'detail_64' => Yii::t('app', 'Detail 64'),
			'detail_65' => Yii::t('app', 'Detail 65'),
			'detail_66' => Yii::t('app', 'Detail 66'),
			'detail_67' => Yii::t('app', 'Detail 67'),
			'detail_68' => Yii::t('app', 'Detail 68'),
			'detail_69' => Yii::t('app', 'Detail 69'),
			'detail_70' => Yii::t('app', 'Detail 70'),
			'detail_71' => Yii::t('app', 'Detail 71'),
			'detail_72' => Yii::t('app', 'Detail 72'),
			'detail_73' => Yii::t('app', 'Detail 73'),
			'detail_74' => Yii::t('app', 'Detail 74'),
			'detail_75' => Yii::t('app', 'Detail 75'),
			'detail_76' => Yii::t('app', 'Detail 76'),
			'detail_77' => Yii::t('app', 'Detail 77'),
			'detail_78' => Yii::t('app', 'Detail 78'),
			'detail_79' => Yii::t('app', 'Detail 79'),
			'detail_80' => Yii::t('app', 'Detail 80'),
			'detail_81' => Yii::t('app', 'Detail 81'),
			'detail_82' => Yii::t('app', 'Detail 82'),
			'detail_83' => Yii::t('app', 'Detail 83'),
			'detail_84' => Yii::t('app', 'Detail 84'),
			'detail_85' => Yii::t('app', 'Detail 85'),
			'detail_86' => Yii::t('app', 'Detail 86'),
			'detail_87' => Yii::t('app', 'Detail 87'),
			'detail_88' => Yii::t('app', 'Detail 88'),
			'detail_89' => Yii::t('app', 'Detail 89'),
			'detail_90' => Yii::t('app', 'Detail 90'),
			'detail_91' => Yii::t('app', 'Detail 91'),
			'detail_92' => Yii::t('app', 'Detail 92'),
			'detail_93' => Yii::t('app', 'Detail 93'),
			'detail_94' => Yii::t('app', 'Detail 94'),
			'detail_95' => Yii::t('app', 'Detail 95'),
			'detail_96' => Yii::t('app', 'Detail 96'),
			'detail_97' => Yii::t('app', 'Detail 97'),
			'detail_98' => Yii::t('app', 'Detail 98'),
			'detail_99' => Yii::t('app', 'Detail 99'),
			'detail_100' => Yii::t('app', 'Detail 100'),
		];
	}

	public function getArticle()
	{
		return $this->hasOne(Article::className(), ['id' => 'article_id']);
	}

	public function presetValues()
	{
		$this->calculatePixelsPerInch();
		$this->setInternalMemory();
	}

	public function calculatePixelsPerInch()
	{
		$dimensions = $this->getHeightAndWidth();
		if (count($dimensions) <= 1)
			return false;

		list($height, $width) = $this->getHeightAndWidth();
		$displaySizeInInch = $this->detail_87;

		if ($height && $width && $displaySizeInInch > 0)
			$this->detail_88 = strval(intval(sqrt(pow($height, 2) + pow($width, 2)) / $displaySizeInInch));
	}

	public function setInternalMemory()
	{
		$memory = $this->article ? $this->article->memory : '';
		$this->{'detail_' . ArticleDetails::DETAIL_INTERNAL_MEMORY} = "$memory";
	}

	/**
	 * @return array
	 */
	public function getHeightAndWidth()
	{
		return explode('x', str_replace(' ', '', $this->detail_31));
	}

	/**
	 * @param $article_id
	 * @param $id
	 * @param bool|false $withUnit
	 * @return mixed|string
	 */
	static public function getValueByNumber($article_id, $id, $withUnit = false)
	{
		$attr = is_int($id) ? 'detail_' . $id : $id;
		$value = self::findOne(['article_id' => $article_id])->$attr;

		if ($value && $withUnit && ($unit = ArticleDetails::findOne(['detail_id' => $id])->unit) != false && !empty($unit)) {
			$value .= ' ' . $unit;
		}

		return $value;
	}


}
