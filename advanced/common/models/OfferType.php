<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "offer_type".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 * @property boolean $simcard_needed
 */
class OfferType extends ActiveRecord
{
	const NEW_CONTRACT_TYPE = 1;
	const RENEWAL_TYPE = 3;
	const NEW_DATA_CONTRACT_TYPE = 4;

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			[['simcard_needed', 'is_active'], 'filter', 'filter' => 'boolval'],

			[['title', 'simcard_needed', 'is_active'], 'required'],
			[['title'], 'string', 'max' => 64],
			[['simcard_needed', 'is_active'], 'boolean'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'OfferType ID'),
			'title' => Yii::t('app', 'Title'),
			'is_active' => Yii::t('app', 'Is Active'),
			'simcard_needed' => Yii::t('app', 'Sim Card Needed'),
		];
	}

	/**
	 * @return bool
	 */
	public function isRenewal()
	{
		return $this->id === static::RENEWAL_TYPE;
	}
}
