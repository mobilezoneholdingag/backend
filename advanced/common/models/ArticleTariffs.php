<?php

namespace common\models;

use common\modules\pricing\models\ConsolidatedPrice;
use common\modules\pricing\models\interfaces\PriceProviderInterface;
use common\modules\pricing\models\interfaces\PricingStrategyInterface;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "article_tariffs".
 *
 * @property integer $id
 * @property integer $article_id
 * @property integer $tariff_id
 * @property float $price
 * @property string $subsidy_payment            Promotion fee as part of our margin.
 * @property integer $is_active
 * @property integer $is_active_erp
 * @property float $profit                          The margin to expect.
 * @property float $fix_profit                      The margin to expect.
 * @property integer $fulfillment_partner_id
 * @property string $external_id
 * @property integer $delivery_status_id
 * @property float $price_monthly
 * @property string $footnote_text
 * @property string $frontend_text
 * @property string $valid_from Date
 * @property string $valid_until Date
 * @property string $api_updated_at DateTime
 * @property string $itemType
 * @property ConsolidatedPrice[] $prices
 * @property float $price_article_once
 * @property float $price_article_monthly
 * @property float $price_tariff_once
 * @property float $price_tariff_monthly

 * @property Article $simCard
 * @property Article $article
 * @property Tariffs $tariff
 * @property string $productName
 * @property string $offerTypeTitle
 * @property string $tariffName
 * @property FulfillmentPartners $partner
 * @property float $priceMonthly
 * @property float $price_article_first_month
 * @property float $price_article_total
 * @property PricingStrategyInterface pricingStrategy
*/
class ArticleTariffs extends ActiveRecord implements PriceProviderInterface
{
	use ArticleTariffPriceTrait;

	protected $pricingStrategy = null;

	public $itemType = 'article_tariff';
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'article_tariffs';
	}

	/**
	 * @param $device Article
	 * @param $tariff Tariffs
	 *
	 * @return string
	 */
	public static function getPrice($device, $tariff)
	{
		return self::findOne(['article_id' => $device->article_id, 'tariff_id' => $tariff->tariff_id])->price;
	}

	public function init()
	{
		$this->is_active = true;
	}

	/**
	 * @return Article
	 */
	public function getSimCard()
	{
		if ($this->isSwisscomRenewal()) {
			return null;
		}

		if ($this->hasArticleGroup()) {
			$simCard = $this->getArticleGroupSimCard();
		}

		return $simCard ?? $this->tariff->provider->simCard;
	}

	public function getSimCardId()
	{
		if ($this->isSwisscomRenewal()) {
			return null;
		}

		$simCard = $this->hasArticleGroup() ? $this->getArticleGroupSimCard() : null;

		return $simCard->id ?? $this->tariff->provider->simcard_id;
	}

	private function isSwisscomRenewal(): bool
	{
		return $this->tariff->provider_id == Provider::TITLE_SWISSCOM
			&& $this->tariff->offer_type_id == OfferType::RENEWAL_TYPE;
	}

	public function hasArticleGroup()
	{
		return Article::find()->where(['id' => $this->article_id], ['!=', 'group_id', 0]);
	}

	public function getArticleGroupSimCard()
	{
		return $this->article->articleGroup
			? $this->article->articleGroup->getSimCard($this->tariff->provider_id)
			: null;
	}

	/**
	 * Adds related models as fields.
	 *
	 * @return array
	 */
	public function fields()
	{
		$fields = [
			'id',
			'article' => 'articleId',
			'tariff' => 'tariffId',
			'prices',
		];

		if ($this->needsSimCardOrIsSwisscom()) {
			$fields['sim_card'] = 'simCardId';
		}

		return $fields;
	}

	public function needsSimCardOrIsSwisscom()
	{
		return ($this->tariff->provider->title ?? null) != Provider::TITLE_SWISSCOM
			&& ($this->tariff->offer_type_id ?? null) !== OfferType::RENEWAL_TYPE;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['article_id', 'tariff_id', 'price', 'is_active', 'fulfillment_partner_id', 'subsidy_payment'], 'required'],
			[
				[
					'article_id',
					'tariff_id',
					'is_active',
					'is_active_erp',
					'fulfillment_partner_id',
					'delivery_status_id',
				],
				'integer',
			],
			[['external_id', 'footnote_text','frontend_text'], 'string'],
			[['external_id'], 'unique'],
			[['price', 'subsidy_payment', 'price_monthly', 'fix_profit'], 'number']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'Article Tariff ID'),
			'article_id' => Yii::t('app', 'Article ID'),
			'tariff_id' => Yii::t('app', 'Tariff ID'),
			'price' => Yii::t('app', 'Additional Charge'),
			'price_article_once' => Yii::t('app', 'Price Article Once'),
			'price_article_monthly' => Yii::t('app', 'Price Article Monthly'),
			'subsidy_payment' => Yii::t('app', 'Subsidy Payment'),
			'is_active' => Yii::t('app', 'Is Active'),
			'is_active_erp' => Yii::t('app', 'Is Active Erp'),
			'productName' => Yii::t('app', 'Article'),
			'offerTypeTitle' => Yii::t('app', 'Offer Type Title'),
			'tariffName' => Yii::t('app', 'Tariff'),
			'article' => Yii::t('app', 'Article'),
			'profit' => Yii::t('app', 'Profit'),
			'fix_profit' => Yii::t('app', 'Fix Profit'),
			'fulfillment_partner_id' => Yii::t('app', 'Partner ID'),
			'external_id' => Yii::t('app', 'External ID'),
			'partnerTitle' => Yii::t('app', 'Partner Title'),
			'providerTitle' => Yii::t('app', 'Provider Title'),
			'delivery_status_id' => Yii::t('app', 'Delivery Status'),
			'price_monthly' => Yii::t('app', 'Price Monthly'),
			'footnote_text' => Yii::t('app', 'Footnote Text'),
			'frontend_text' => Yii::t('app', 'Frontend Text'),
		];
	}

	/**
	 * @return ActiveQuery
	 */
	public function getArticle()
	{
		return $this->hasOne(Article::className(), ['id' => 'article_id']);
	}

	/**
	 * @return ActiveQuery|ArticleGroups
	 */
	public function getArticleGroup()
	{
		return $this->article->articleGroup;
	}

	/**
	 * @return ActiveQuery
	 */
	public function getTariff()
	{
		return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getPartner()
	{
		return $this->hasOne(FulfillmentPartners::className(), ['id' => 'fulfillment_partner_id']);
	}

	/**
	 * @return string
	 */
	public function getPartnerTitle()
	{
		return $this->partner->title ?? null;
	}

	/**
	 * @return string
	 */
	public function getProductName()
	{
		return ($this->article) ? $this->article->uniqueTitle : null;
	}

	public function getOfferTypeTitle()
	{
		return $this->tariff->offerType->title ?? null;
	}

	/**
	 * @return string
	 */
	public function getTariffName()
	{
		return ($this->tariff) ? $this->tariff->title : null;
	}

	/**
	 * @return string
	 */
	public function getProvider()
	{
		if ($this->tariff && $this->tariff->provider) {
			return $this->tariff->provider;
		}

		return null;
	}

	/**
	 * @return string
	 */
	public function getProviderTitle()
	{
		if ($this->tariff && $this->tariff->provider) {
			return $this->tariff->provider->title;
		}

		return null;
	}

	public function getProfit()
	{
		return (!empty($this->fix_profit)) ? $this->fix_profit : self::calcProfit($this->article, $this->tariff, $this);
	}

	public static function calcProfit(Article $article, Tariffs $tariffs, ArticleTariffs $articleTariffs)
	{
		$provision = floatval($tariffs->provision);
		$price_buy = floatval($article->price_buy);
		$wkz = floatval($articleTariffs->subsidy_payment);
		$price = floatval($articleTariffs->price / 1.19);

		return $provision - $price_buy + $wkz + $price;
	}

	public function getRowOption()
	{
		$optionArr = [];

		if ($this->profit <= 60 && $this->profit >= 50)
			$optionArr = ['class' => 'yellowRow'];

		if ($this->profit < 50)
			$optionArr = ['class' => 'redRow'];

		if (!$this->is_active)
			$optionArr = ['class' => 'inactiveRow'];


		return $optionArr;
	}

	public function getArticleId()
	{
		return $this->article_id;
	}

	public function getTariffId()
	{
		return $this->tariff_id;
	}
}
