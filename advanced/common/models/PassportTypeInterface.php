<?php

namespace common\models;

interface PassportTypeInterface
{
	const ALL = [
		1 => 'Schweizerpass',
		2 => 'Identitätskarte',
		3 => 'Führerausweis',
		4 => 'Ausländerausweis B',
		5 => 'Ausländerausweis C',
		6 => 'Ausländerausweis G',
		7 => 'Ausländerausweis L',
		8 => 'Ausländerausweis F',
		9 => 'Ausländerausweis N',
		10 => 'Handelsregistereintrag',
	];
}
