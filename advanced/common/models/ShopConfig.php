<?php

namespace common\models;

use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "shop_config".
 *
 * @property integer $id
 * @property integer $kategoriebaum
 * @property integer $design_id
 * @property string $absender_name
 * @property string $absender_email
 * @property string $kontakt_email
 * @property string $fusszeile_email
 * @property string $fusszeile_1
 * @property string $fusszeile_2
 * @property string $fusszeile_3
 * @property string $meta_angaben
 * @property string $css_angaben
 * @property string $html_code_links
 * @property string $seitentitle
 * @property integer $info_einkauf
 * @property string $marge_handy
 * @property string $marge_mixer
 * @property string $versand_1
 * @property string $versand_2
 * @property string $versand_3
 * @property string $versand_4
 * @property string $versand_5
 * @property string $versand_handy
 * @property string $versand_grenze
 * @property string $nachnahme
 * @property string $bestelltext
 * @property string $bestelltext_vertrag
 * @property string $lieferzeit_1
 * @property string $lieferzeit_2
 * @property string $lieferzeit_3
 * @property string $lieferzeit_4
 * @property string $lieferzeit_5
 * @property string $lieferzeit_6
 * @property string $lieferzeit_7
 * @property integer $zahlung_vorkasse
 * @property integer $zahlung_nachnahme
 * @property integer $zahlung_lastschrift
 * @property integer $zahlung_paypal
 * @property string $email_paypal
 * @property string $text_vorkasse
 * @property string $text_nachnahme
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $versandart
 * @property string $schufatext
 * @property string $html_einkauf
 * @property string $html_festnetz
 * @property string $html_newsletter
 * @property string $html_ssl_einkauf
 * @property string $html_ssl_festnetz
 * @property string $html_ssl_newsletter
 * @property integer $giveaway_article_id
 * @property integer $giveaway
 * @property integer $null_prozent_aktion
 *
 * new attributes
 *
 * @property integer $top10smartphones_set_id
 * @property string $publish_token
 */
class ShopConfig extends ActiveRecord
{
	/**
	 * Defines the id of used banner set (on deinhandy.de startpage)
	 */
	const DEINHANDY = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'shop_config';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['top10smartphones_set_id'], 'required'],
			[['id', 'top10smartphones_set_id'], 'integer'],
			[['publish_token'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'kategoriebaum' => Yii::t('app', 'Kategoriebaum'),
			'design_id' => Yii::t('app', 'Design ID'),
			'absender_name' => Yii::t('app', 'Absender Name'),
			'absender_email' => Yii::t('app', 'Absender Email'),
			'kontakt_email' => Yii::t('app', 'Kontakt Email'),
			'fusszeile_email' => Yii::t('app', 'Fusszeile Email'),
			'fusszeile_1' => Yii::t('app', 'Fusszeile 1'),
			'fusszeile_2' => Yii::t('app', 'Fusszeile 2'),
			'fusszeile_3' => Yii::t('app', 'Fusszeile 3'),
			'meta_angaben' => Yii::t('app', 'Meta Angaben'),
			'css_angaben' => Yii::t('app', 'Css Angaben'),
			'html_code_links' => Yii::t('app', 'Html Code Links'),
			'seitentitle' => Yii::t('app', 'Seitentitle'),
			'info_einkauf' => Yii::t('app', 'Info Einkauf'),
			'marge_handy' => Yii::t('app', 'Marge Handy'),
			'marge_mixer' => Yii::t('app', 'Marge Mixer'),
			'versand_1' => Yii::t('app', 'Versand 1'),
			'versand_2' => Yii::t('app', 'Versand 2'),
			'versand_3' => Yii::t('app', 'Versand 3'),
			'versand_4' => Yii::t('app', 'Versand 4'),
			'versand_5' => Yii::t('app', 'Versand 5'),
			'versand_handy' => Yii::t('app', 'Versand Handy'),
			'versand_grenze' => Yii::t('app', 'Versand Grenze'),
			'nachnahme' => Yii::t('app', 'Nachnahme'),
			'bestelltext' => Yii::t('app', 'Bestelltext'),
			'bestelltext_vertrag' => Yii::t('app', 'Bestelltext Vertrag'),
			'lieferzeit_1' => Yii::t('app', 'Lieferzeit 1'),
			'lieferzeit_2' => Yii::t('app', 'Lieferzeit 2'),
			'lieferzeit_3' => Yii::t('app', 'Lieferzeit 3'),
			'lieferzeit_4' => Yii::t('app', 'Lieferzeit 4'),
			'lieferzeit_5' => Yii::t('app', 'Lieferzeit 5'),
			'lieferzeit_6' => Yii::t('app', 'Lieferzeit 6'),
			'lieferzeit_7' => Yii::t('app', 'Lieferzeit 7'),
			'zahlung_vorkasse' => Yii::t('app', 'Zahlung Vorkasse'),
			'zahlung_nachnahme' => Yii::t('app', 'Zahlung Nachnahme'),
			'zahlung_lastschrift' => Yii::t('app', 'Zahlung Lastschrift'),
			'zahlung_paypal' => Yii::t('app', 'Zahlung Paypal'),
			'email_paypal' => Yii::t('app', 'Email Paypal'),
			'text_vorkasse' => Yii::t('app', 'Text Vorkasse'),
			'text_nachnahme' => Yii::t('app', 'Text Nachnahme'),
			'meta_description' => Yii::t('app', 'Meta Description'),
			'meta_keywords' => Yii::t('app', 'Meta Keywords'),
			'versandart' => Yii::t('app', 'Versandart'),
			'schufatext' => Yii::t('app', 'Schufatext'),
			'html_einkauf' => Yii::t('app', 'Html Einkauf'),
			'html_festnetz' => Yii::t('app', 'Html Festnetz'),
			'html_newsletter' => Yii::t('app', 'Html Newsletter'),
			'html_ssl_einkauf' => Yii::t('app', 'Html Ssl Einkauf'),
			'html_ssl_festnetz' => Yii::t('app', 'Html Ssl Festnetz'),
			'html_ssl_newsletter' => Yii::t('app', 'Html Ssl Newsletter'),
			'giveaway_article_id' => Yii::t('app', 'Giveaway Article ID'),
			'giveaway' => Yii::t('app', 'Giveaway'),
			'null_prozent_aktion' => Yii::t('app', 'Null Prozent Aktion'),

			'top10smartphones_set_id' => Yii::t('app', 'Top 10 Smartphones'),
			'publish_token' => Yii::t('app', 'Publish Token')
		];
	}

	/**
	 * @param $valueName
	 * @return mixed|string
	 */
	public static function getValue($valueName)
	{
		$value = self::findOne(1)->$valueName;
		return ($value) ? $value : '';
	}

	public static function topTenSmartphones()
	{
		return Sets::findOne(self::getValue('top10smartphones_set_id'))->getProductList(['is_active' => true]);
	}

}
