<?php

namespace common\models;

use common\behaviors\DateBehavior;
use common\models\behaviors\TimestampBehaviorSettings;
use components\Locale;
use deinhandy\yii2picasso\behavior\ImageAttributeBehavior;
use deinhandy\yii2picasso\behavior\ImageBehaviorConfig;
use deinhandy\yii2picasso\behavior\ImageUploadBehavior;
use deinhandy\yii2picasso\models\Image;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "marketing_images".
 *
 * @property integer $id
 * @property Image $image
 * @property string $target_url
 * @property string $tag
 * @property string $language
 * @property string $updated_at
 * @property string $created_at
 * @property string $publish_at
 * @property string $remoteFileUrl
 * @property string $alt_text
 * @property string $link_target
 */
class MarketingImages extends ActiveRecord
{
	const LANGUAGES = [
		Locale::SUISSE => Locale::SUISSE,
		Locale::FRANCE => Locale::FRANCE,
		Locale::ITALY => Locale::ITALY,
	];

	const BANNER_POSITIONS = [
		'homepage-side-teaser-1' => 'homepage-side-teaser-1',
		'homepage-side-teaser-2' => 'homepage-side-teaser-2',
		'homepage-slider-image-1' => 'homepage-slider-image-1',
		'homepage-slider-image-2' => 'homepage-slider-image-2',
		'homepage-slider-image-3' => 'homepage-slider-image-3',
		'homepage-slider-image-4' => 'homepage-slider-image-4',
		'homepage-slider-image-5' => 'homepage-slider-image-5',
        'homepage-slider-image-6' => 'homepage-slider-image-6',
        'homepage-slider-image-7' => 'homepage-slider-image-7',
        'homepage-slider-image-8' => 'homepage-slider-image-8',
        'homepage-slider-image-9' => 'homepage-slider-image-9',
		'homepage-promo-1' => 'homepage-promo-1',
		'homepage-promo-2' => 'homepage-promo-2',
		'header-teaser'=> 'header- teaser',
		'navi-layer-accessories-teaser' => 'navi-layer-accessories-teaser',
		'navi-layer-smartphones-teaser' => 'navi-layer-smartphones-teaser',
		'navi-layer-smartwatches-teaser' => 'navi-layer-smartwatches-teaser',
		'navi-layer-tablets-teaser' => 'navi-layer-tablets-teaser',
		'category-accessories-teaser-1' => 'category-accessories-teaser-1',
		'category-accessories-teaser-2' => 'category-accessories-teaser-2',
		'category-smartphones-teaser-1' => 'category-smartphones-teaser-1',
		'category-smartphones-teaser-2' => 'category-smartphones-teaser-2',
		'category-smartwatches-teaser-1' => 'category-smartwatches-teaser-1',
		'category-smartwatches-teaser-2' => 'category-smartwatches-teaser-2',
		'category-tablets-teaser-1' => 'category-tablets-teaser-1',
		'category-tablets-teaser-2' => 'category-tablets-teaser-2',
		'contract-configurator' => 'contract-configurator',
		'tv-banner-1' => 'tv-banner-1',
		'tv-banner-2' => 'tv-banner-2',
		'tv-banner-3' => 'tv-banner-3',
	];

	const PICASSO_COLLECTION_NAME = 'marketing-images';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'marketing_images';
	}

	public function behaviors()
	{
		$accountNamespace = Yii::$app->params['picasso_namespace'];
		$collectionName = self::PICASSO_COLLECTION_NAME;

		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
				'image_upload' => [
					'class' => ImageUploadBehavior::class,
					'attributes' => ['image' => new ImageBehaviorConfig($accountNamespace, $collectionName)],
				],
				'image_access' => [
					'class' => ImageAttributeBehavior::class,
					'attributes' => ['image'],
				],
				[
					'class' => DateBehavior::className(),
					'attributes' => ['publish_at'],
				],
				LoggableBehavior::className(),
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['tag', 'link_target'], 'required'],
			[['updated_at', 'created_at'], 'safe'],
			[['target_url', 'language', 'alt_text'], 'string', 'max' => 255],
			[['target_url'], 'url', 'defaultScheme' => 'https'],
			[['tag'], 'string', 'max' => 64],
			[['tag'], 'unique', 'targetAttribute' => ['tag', 'language', 'publish_at']],
			[
				'tag',
				'match',
				'pattern' => '/^[a-z0-9-]*$/',
				'message' => 'Please use only lowercase letters, numbers and dashes',
			],
			[['publish_at'], 'date'],
			[['image'], 'file', 'extensions'=>['png', 'jpg', 'jpeg'], 'wrongExtension' => Yii::t('app', 'wrong extension - only PNG and JP(E)G allowed')],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'image' => Yii::t('app', 'Image'),
			'target_url' => Yii::t('app', 'Target URL'),
			'tag' => Yii::t('app', 'Position / Tag'),
			'language' => Yii::t('app', 'Language'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'created_at' => Yii::t('app', 'Created At'),
			'alt_text' => Yii::t('app', 'Image Alt Text'),
            'link_target' => Yii::t('app', 'Link Target (Window)'),
		];
	}

	public function getRemoteFileUrl()
	{
		return $this->image;
	}

	public function fields()
	{
		return [
			'id',
			'target_url',
			'remote_file_url' => 'remoteFileUrl',
			'alt_text',
            'link_target',
		];
	}

	public function getLinkTargetOptions()
    {
        return [
            '_self' => 'Im selben Fenster',
            '_blank' => 'Neues Fenster'
        ];
    }

}
