<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "seo_available_canonical_url".
 *
 * @property integer $id
 * @property string $url
 * @property boolean $available
 */
class SeoAvailableCanonicalUrl extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'seo_available_canonical_url';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['url','available'], 'required'],
			[['url'], 'string', 'max' => 255],
			[['available'], 'boolean'],
			[['url'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'url' => 'Url',
			'available' => 'available'
		];
	}
}
