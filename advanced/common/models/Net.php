<?php

namespace common\models;

use backend\models\interfaces\ListUpInterface;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "net".
 *
 * @property integer $id
 * @property string $title
 */
class Net extends ActiveRecord
	implements ListUpInterface
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'net';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'title'], 'required'],
			[['id'], 'integer'],
			[['title'], 'string']
		];
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
		];
	}

	/**
	 * @return Provider[]
	 */
	public function getProvider()
	{
		return $this->hasMany(Provider::className(), ['net_id' => 'id']);
	}

	/**
	 * A simple list of all available nets.
	 *
	 * @return array
	 */
	static public function getList()
	{
		$nets = [];
		foreach(self::find()->select(['id', 'title'])->orderBy(['title' => SORT_ASC])->asArray()->each() as $netRecord) {
			$nets[$netRecord['id']] = $netRecord['title'];
		}

		return $nets;
	}
}
