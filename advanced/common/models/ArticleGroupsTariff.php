<?php

namespace common\models;

use Yii;

/**
 * @property integer $article_group_id
 * @property integer $tariff_id
 * @property integer $provider_id
 * @property Provider $provider
 */
class ArticleGroupsTariff extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_groups_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_group_id', 'tariff_id', 'provider_id'], 'required'],
            [['article_group_id', 'tariff_id', 'provider_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_group_id' => Yii::t('app', 'Article Group ID'),
            'tariff_id' => Yii::t('app', 'Tariff ID'),
            'provider_id' => Yii::t('app', 'Provider ID'),
        ];
    }
}
