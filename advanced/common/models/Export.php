<?php

namespace common\models;

use components\services\ExportService;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\widgets\ActiveField;
use yii\widgets\ActiveForm;

/**
 * This is the model class for table "export".
 *
 * @property integer $id
 * @property string $partner
 * @property string $preferred_format
 * @property string $key
 * @property string $filename
 * @property string $last_run
 * @property integer $runs_today
 * @property integer $status
 * @property string $delimiter
 * @property string $bid
 * @property string $extra_parameters
 * @property string $link
 * @property float $min_profit
 */
class Export extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'export';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['last_run'], 'safe'],
			[['runs_today', 'status'], 'integer'],
			[['min_profit'], 'number'],
			[['partner', 'preferred_format', 'key', 'delimiter', 'bid', 'link', 'extra_parameters', 'filename'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'partner' => Yii::t('app', 'Partner'),
			'preferred_format' => Yii::t('app', 'Preferred Format'),
			'key' => Yii::t('app', 'Key'),
			'last_run' => Yii::t('app', 'Last Run'),
			'runs_today' => Yii::t('app', 'Runs Today'),
			'status' => Yii::t('app', 'Active'),
			'delimiter' => Yii::t('app', 'Delimiter'),
			'bid' => Yii::t('app', 'BID'),
			'link' => Yii::t('app', 'Link'),
			'min_profit' => Yii::t('app', 'Minimal Profit'),
		];
	}

	public function getHasModule()
	{
		/** @var ExportService $exportService */
		$exportService = Yii::$app->get('export');
		return $exportService->hasModuleForPartner($this->partner);
	}

	/**
	 * A list of default delimiters. (CSV only.)
	 * @return array
	 */
	public function getDefaultDelimitersList()
	{
		$list = [
			'tab' => 'Tab',
			'space' => 'Space',
			';' => ';',
			',' => ','
		];

		return $list;
	}

	/**
	 * Whether the delimiter is custom or not.
	 * @return bool
	 */
	public function isCustomDelimiter()
	{
		return !in_array($this->delimiter, array_keys($this->getDefaultDelimitersList()));
	}

	public function load($data, $formName = null)
	{
		$returnAfterLoad = parent::load($data, $formName);

		if ($this->delimiter == 'custom' && isset($data['custom']))
		{
			$this->delimiter = $data['custom'];
		}

		return $returnAfterLoad;
	}

	/**
	 * Returns the filename of the export.
	 *
	 * @return string
	 */
	public function getFilename()
	{
		$filename = strtolower($this->partner);
		if ($this->filename) {
			$filename = $this->filename;
		}
		return urlencode(join('.', [$filename, $this->preferred_format]));
	}

}
