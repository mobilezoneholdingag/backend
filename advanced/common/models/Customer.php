<?php

namespace common\models;

use common\models\behaviors\TimestampBehaviorSettings;
use common\modules\order\models\Order;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $source
 * @property string $source_details
 * @property string $password_hash
 * @property string $password_salt
 * @property string $password_reset_token
 * @property string $language
 * @property string $nationality
 * @property string $passport_id
 * @property integer $passport_type
 * @property string $birthday
 * @property string $place_of_birth
 * @property string $updated_at
 * @property string $created_at
 * @property string $genderLabel
 * @property string $languageUppercase
 * @property CustomerAddress[] $customerAddresses
 * @property CustomerAddress $activeCustomerAddress
 * @property CustomerPhoneNumber[] $customerPhoneNumbers
 * @property NewsletterSubscriber[] $newsletterSubscribers
 * @property NewsletterSubscriber $newsletterSubscriber
 * @property Order[] $orders
 */
class Customer extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'customer';
	}

	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['email', 'first_name', 'last_name', 'gender', 'language'], 'required'],
			[['email'], 'email'],
			[['passport_type', 'via_newsletter'], 'integer'],
			['language', 'validateLanguage'],
			[['gender'], 'in', 'range' => array_keys(GenderInterface::ALL)],
			[['updated_at', 'created_at'], 'safe'],
			[
				[
					'email',
					'source',
					'source_details',
					'password_hash',
					'password_salt',
					'password_reset_token',
					'language',
					'nationality',
					'passport_id',
					'birthday',
					'place_of_birth',
				],
				'string',
				'max' => 255,
			],
			[['email'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'email' => 'Email',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'gender' => 'Gender',
			'source' => 'Source',
			'source_details' => 'Source Details',
			'password_hash' => 'Password Hash',
			'password_salt' => 'Password Salt',
			'password_reset_token' => 'Password Reset Token',
			'language' => 'Language',
			'nationality' => 'Nationality',
			'passport_id' => 'Passport',
			'passport_type' => 'Passport Type',
			'birthday' => 'Birthday',
			'place_of_birth' => 'place of birth',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomerAddresses()
	{
		return $this->hasMany(CustomerAddress::className(), ['customer_id' => 'id']);
	}

	/**
	 * @return array|null|ActiveRecord|CustomerAddress
	 */
	public function getDefaultCustomerAddress()
	{
		return $this->hasOne(CustomerAddress::className(), ['customer_id' => 'id'])->where(['is_default' => 1])->one();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomerPhoneNumbers()
	{
		return $this->hasMany(CustomerPhoneNumber::className(), ['customer_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getNewsletterSubscribers()
	{
		return $this->hasMany(NewsletterSubscriber::className(), ['customer_id' => 'id']);
	}

	public function getNewsletterSubscriber()
	{
		return $this->hasOne(NewsletterSubscriber::className(), ['customer_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrders()
	{
		return $this->hasMany(Order::class, ['customer_id' => 'id']);
	}

	/**
	 * @return null|string
	 */
	public function getGenderLabel()
	{
		switch ($this->gender) {
			case 1:
				return "Herr";
			case 2:
				return "Frau";
			default:
				return null;
		}
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 *
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid($token)
	{
		if (empty($token)) {
			return false;
		}
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode('_', $token);
		$timestamp = (int)end($parts);

		return $timestamp + $expire >= time();
	}


	/**
	 * Adds related models as fields.
	 * @return array
	 */
	public function fields()
	{
		return array_merge(
			parent::fields(),
			[
				'addresses' => 'customerAddressIds',
				'phoneNumbers' => 'customerPhoneNumberIds',
				'newsletters' => 'newsletterSubscriberIds',
				'orders' => 'orderIds',
			]
		);
	}

	public function getCustomerAddressIds()
	{
		return $this->getCustomerAddresses()->select('id')->column();
	}

	public function getCustomerPhoneNumberIds()
	{
		return $this->getCustomerPhoneNumbers()->select('id')->column();
	}

	public function getFirstCustomerPhoneNumber()
	{
		$customerPhoneNumber = $this->getCustomerPhoneNumbers()->one();

		return $customerPhoneNumber instanceof CustomerPhoneNumber ? $customerPhoneNumber->number : null;
	}

	public function getNewsletterSubscriberIds()
	{
		return $this->getNewsletterSubscribers()->select('id')->column();
	}

	public function getOrderIds()
	{
		return $this->getOrders()->select('id')->column();
	}

	public function validatePassword($password)
	{
		if (empty($this->password_hash)) {
			return false;
		}

		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	public function setPasswordHashByPassword(string $password)
	{
		if (!empty($password)) {
			$this->password_hash = Yii::$app->security->generatePasswordHash($password);
		}
	}

	public function load($data, $formName = null): bool
	{
		$saved = parent::load($data, $formName);
		if (empty($data['password_hash']) === false) {
			$this->setPasswordHashByPassword($data['password_hash']);
		}
		return $saved;
	}

	public function getLanguageUppercase()
	{
		return strtoupper(explode('-', $this->language ?? 'de-CH')[0]);
	}

	public static function findAllLanguages(): array
	{
		return Yii::$app->db
			->createCommand('SELECT `language_id`, `name` FROM `language`')
			->queryAll(\PDO::FETCH_KEY_PAIR);
	}

	public function validateLanguage($attribute)
	{
		$languages = array_keys(static::findAllLanguages());
		if (!in_array($this->{$attribute}, $languages)) {
			$message = Yii::t('yii', '{attribute} is invalid.', ['attribute' => $this->getAttributeLabel($attribute)]);
			$this->addError($attribute, $message);
		}
	}
}
