<?php

namespace common\models;

use common\models\behaviors\TimestampBehaviorSettings;
use sammaye\audittrail\LoggableBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "customer_address".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $guid
 * @property integer $is_active
 * @property integer $is_default
 * @property integer $sex
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $street
 * @property string $house_number
 * @property integer $zip
 * @property string $city
 * @property string $country
 * @property integer $contact_phone_number
 * @property string $additional_info
 * @property string $updated_at
 * @property string $created_at
 * @property Customer $customer
 */
class CustomerAddress extends ActiveRecord
{
	public function init()
	{
		$unsetDefaultAddress = function($event) {
			/** @var CustomerAddress $address */
			$address = $event->sender;

			if ($address->is_default) {
				/** @var CustomerAddress $defaultAddress */
				$defaultAddress = $address->customer->getCustomerAddresses()
					->where(['is_default' => true])
					->andWhere(['!=', 'id', $address->id])
					->one();

				if ($defaultAddress) {
					$defaultAddress->is_default = false;
					$defaultAddress->save(false);
				}
			}
		};

		$this->on(self::EVENT_AFTER_UPDATE, $unsetDefaultAddress);
		$this->on(self::EVENT_AFTER_INSERT, $unsetDefaultAddress);
		$this->on(self::EVENT_AFTER_DELETE, $unsetDefaultAddress);
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'customer_address';
	}

	public function behaviors()
	{
		return [
			'timestamp' => TimestampBehaviorSettings::createdAtAndUpdatedAt(),
			LoggableBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['customer_id'], 'required'],
			[['customer_id', 'is_active', 'is_default', 'sex', 'zip', 'contact_phone_number'], 'integer'],
			[['updated_at', 'created_at'], 'safe'],
			[['guid', 'firstname', 'lastname', 'additional_info'], 'string', 'max' => 255],
			[['company', 'city', 'country'], 'string', 'max' => 100],
			[['street'], 'string', 'max' => 150],
			[['house_number'], 'string', 'max' => 25],
			[
				['customer_id'],
				'exist',
				'skipOnError' => true,
				'targetClass' => Customer::className(),
				'targetAttribute' => ['customer_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'customer_id' => 'Customer ID',
			'guid' => 'Guid',
			'is_active' => 'Is Active',
			'is_default' => 'Is Default',
			'sex' => 'Sex',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'company' => 'Company',
			'street' => 'Street',
			'house_number' => 'House Number',
			'zip' => 'Zip',
			'city' => 'City',
			'country' => 'Country',
			'contact_phone_number' => 'Contact Phone Number',
			'additional_info' => 'Additional Info',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
		];
	}

	public function fields()
	{
		return [
			'id',
			'customer_id',
			'is_default',
			'sex',
			'firstname',
			'lastname',
			'company',
			'street',
			'house_number',
			'zip',
			'city',
			'country',
			'contact_phone_number',
			'birthday',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}

	public function getBirthday()
	{
		return $this->customer->birthday;
	}
}
