<?php

use yii\db\Migration;

class m170612_125019_MZ_1043_create_tables_job_and_job_category extends Migration
{
	const JOB_CATEGORY_TABLE = 'job_category';
	const JOB_TABLE = 'job';

	public function up()
	{
		$this->createTable(
			self::JOB_CATEGORY_TABLE,
			[
				'id' => $this->primaryKey(),
				'title' => $this->string(),
			]
		);

		$this->createTable(
			self::JOB_TABLE,
			[
				'id' => $this->primaryKey(),
				'job_category_id' => $this->integer()->notNull(),
				'language' => $this->char(2),
				'title' => $this->string(),
				'location' => $this->string(),
				'text' => $this->text(),
				'job_id' => $this->integer()->unsigned()->notNull(),
				'slug' => $this->string(),
			]
		);

		$this->addForeignKey(
			'fk_job_job_category_id',
			self::JOB_TABLE,
			'job_category_id',
			self::JOB_CATEGORY_TABLE,
			'id',
			'cascade'
		);

		return true;
	}

	public function down()
	{
		$this->dropForeignKey('fk_job_job_category_id', self::JOB_TABLE);
		$this->dropTable(self::JOB_CATEGORY_TABLE);
		$this->dropTable(self::JOB_TABLE);

		return true;
	}
}
