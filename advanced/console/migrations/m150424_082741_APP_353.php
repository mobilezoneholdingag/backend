<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_082741_APP_353 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Export::tableName(),'link', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Export::tableName(),'link');

		return true;
	}
}
