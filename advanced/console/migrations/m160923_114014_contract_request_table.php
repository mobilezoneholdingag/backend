<?php

use yii\db\Migration;

class m160923_114014_contract_request_table extends Migration
{
	const TABLE = 'contract_requests';

	public function up()
	{
		$this->createTable(self::TABLE, [
			'id' => $this->primaryKey(),
			'gender' => $this->integer(1),
			'phone_number' => $this->string(),
			'firstname' => $this->string(),
			'lastname' => $this->string(),
			'birthday' => $this->date(),
			'passport_type' => $this->integer(2),
			'passport_number' => $this->string(),
			'email' => $this->string(),
			'provider_id' => $this->integer()->notNull(),
			'article_id' => $this->integer(9)->notNull(),
		]);

		$this->addForeignKey('fk_provider_provider_id', self::TABLE, 'provider_id', 'provider', 'id');
		$this->addForeignKey('fk_articles_article_id', self::TABLE, 'article_id', 'articles', 'id');

		return true;
	}

	public function down()
	{
		$this->dropTable(self::TABLE);

		return true;
	}
}
