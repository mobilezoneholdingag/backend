<?php

use yii\db\Schema;
use yii\db\Migration;

class m150407_091602_app_295_real_coupon extends Migration
{
	public function up()
	{
		$this->update('coupons_normal', ['code' => 'SPQ1'], ['id' => 1]);
	}

	public function down()
	{
		$this->update('coupons_normal', ['code' => 'MINUS10'], ['id' => 1]);
	}
}
