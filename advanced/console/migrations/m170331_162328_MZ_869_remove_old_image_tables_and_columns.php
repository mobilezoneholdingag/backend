<?php

use yii\db\Migration;

class m170331_162328_MZ_869_remove_old_image_tables_and_columns extends Migration
{
	public function up()
	{
		$this->dropTable('article_image');
		$this->dropTable('images');
		$this->dropTable('shop_image');

		return true;
	}

	public function down()
	{
		echo "m170331_162328_MZ_869_remove_old_image_tables_and_columns cannot be reverted.".PHP_EOL;
	
		return false;
	}
}
