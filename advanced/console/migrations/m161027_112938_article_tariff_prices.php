<?php

use yii\db\Migration;

class m161027_112938_article_tariff_prices extends Migration
{
	const TABLE = 'article_tariffs';

    public function up()
    {
	    $this->addColumn(self::TABLE, 'price_article_once', $this->decimal(7, 2));
	    $this->addColumn(self::TABLE, 'price_article_monthly', $this->decimal(7, 2));
	    $this->addColumn(self::TABLE, 'price_tariff_once', $this->decimal(7, 2));
	    $this->addColumn(self::TABLE, 'price_tariff_monthly', $this->decimal(7, 2));
	    $this->addColumn(self::TABLE, 'price_total_once', $this->decimal(7, 2));
	    $this->addColumn(self::TABLE, 'price_total_monthly', $this->decimal(7, 2));

	    return true;
    }

    public function down()
    {
	    $this->dropColumn(self::TABLE, 'price_article_once');
	    $this->dropColumn(self::TABLE, 'price_article_monthly');
	    $this->dropColumn(self::TABLE, 'price_tariff_once');
	    $this->dropColumn(self::TABLE, 'price_tariff_monthly');
	    $this->dropColumn(self::TABLE, 'price_total_once');
	    $this->dropColumn(self::TABLE, 'price_total_monthly');

        return true;
    }
}
