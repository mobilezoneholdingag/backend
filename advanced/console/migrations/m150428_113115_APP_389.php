<?php

use common\models\Sales;
use yii\db\Schema;
use yii\db\Migration;

class m150428_113115_APP_389 extends Migration
{
	public function up()
	{
		Sales::updateAll(['ip' => null]);
	}

	public function down()
	{
		echo "m150428_113115_APP_389 cannot be reverted.\n";
		return false;
	}
}
