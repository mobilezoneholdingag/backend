<?php

use common\models\Tariffs;
use common\modules\set\models\Sets;
use frontend\controllers\tariff\SearchAction;
use yii\db\Migration;

class m170814_132725_MZ_1283_create_tariff_set extends Migration
{
	const SETS_TABLE = 'sets';
	const CHARTS_TABLE = 'charts';
	const COLUMNS = [
		'title' => 'Tarife',
		'system_title' => 'tariffs',
		'description' => 'Alle Tarife, die im Frontend sichtbar sein sollen.',
		'type' => 2,
	];

	public function up()
	{
		$this->insert(self::SETS_TABLE, self::COLUMNS);

		try {
			Yii::$app->set('request', new \yii\web\Request());
			$query = Tariffs::find();
			SearchAction::applyBaseConditions($query);
			$tariffIds = $query->column();
			$set = Sets::findOne(['system_title' => self::COLUMNS['system_title']]);
			$columns = [];

			foreach ($tariffIds as $index => $id) {
				$columns[] = [
					'product_id' => $id,
					'set_id' => $set->id,
					'position' => $index + 1,
				];
			}

			$this->batchInsert(self::CHARTS_TABLE, ['product_id', 'set_id', 'position'], $columns);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}

		return true;
	}

	public function down()
	{
		try {
			$set = Sets::findOne(['system_title' => self::COLUMNS['system_title']]);
			$this->delete(self::CHARTS_TABLE, ['set_id' => $set->id]);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}

		$columns = self::COLUMNS;

		unset($columns['title'], $columns['description']);

		$this->delete(self::SETS_TABLE, $columns);

		return true;
	}
}
