<?php

use yii\db\Migration;

class m170731_121920_MZ_1275_add_order_id_column_to_api_log_swissbilling_table extends Migration
{
	const ORDER_ID_COLUMN = 'order_id';
	const API_LOG_TABLE = 'api_log_swissbilling';
	const INDEX_NAME = 'idx_api_log_swissbilling_order_id';

	public function up()
	{
		$this->addColumn(self::API_LOG_TABLE, self::ORDER_ID_COLUMN, $this->integer()->unsigned());

		$this->createIndex(self::INDEX_NAME, self::API_LOG_TABLE, self::ORDER_ID_COLUMN);
	}

	public function down()
	{
		$this->dropIndex(self::INDEX_NAME, self::API_LOG_TABLE);

		$this->dropColumn(self::API_LOG_TABLE, self::ORDER_ID_COLUMN);
	}
}
