<?php

use yii\db\Migration;

class m161004_122324_MZ_55_add_order_msisdn_attribute extends Migration
{
	const TABLE = 'order';

	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->dropColumn(self::TABLE, 'serial_number');

		$this->addColumn(self::TABLE, 'serial_number', $this->string()->after('shipment_tracking_id'));
		$this->addColumn(self::TABLE, 'msisdn', $this->string()->after('serial_number'));

		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropColumn(self::TABLE, 'msisdn');

		return true;
	}
}
