<?php

use yii\db\Migration;

class m161213_130215_MZ_423_delete_old_DH_tables extends Migration
{
	public function up()
	{
		$this->dropTable('newsletter_subscribers');
		$this->dropTable('glossary');
		$this->dropTable('verivox_article_matching');
		$this->dropTable('verivox_tariff_matching');
		$this->dropTable('no_ads_customer');
		$this->dropTable('hardware_only_offers');
		$this->dropTable('pathfinder');
		$this->dropTable('mailing_caseable_gutschein');
		$this->dropTable('mailing_financefox_gutschein');
		$this->dropTable('mailing_samsung_extra');
	}

	public function down()
	{
		echo "m161213_130215_MZ_423_delete_old_DH_tables cannot be reverted.\n";

		return false;
	}
}
