<?php

use yii\db\Migration;

class m170123_123455_MZ_506_add_provider_young_age_limit extends Migration
{
	const TABLE_NAME = 'provider';

	public function up()
	{
		$this->addColumn(self::TABLE_NAME, 'young_age_limit', $this->integer());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_NAME, 'young_age_limit');

		return false;
	}
}
