<?php

use yii\db\Schema;
use yii\db\Migration;

class m150402_181635_APP_176 extends Migration
{
	public function up()
	{
		$this->insert(\common\models\DeliveryStatus::tableName(),['id' => 5, 'label' => 'Vorbestellung', 'css_class' =>'pending']);

		return true;
	}

	public function down()
	{
		echo "m150402_181635_APP_176 cannot be reverted.\n";

		return false;
	}
	

}
