<?php

use yii\db\Migration;

class m170321_164755_MZ_874_remove_renewal_requests_with_old_test_email extends Migration
{
	const OLD_EMAIL = "test@test.de";

	public function up()
	{
		$renewalRequests = \common\modules\renewalRequest\models\RenewalRequest::find()->where(['email' => self::OLD_EMAIL])->all();
		foreach ($renewalRequests as $renewalRequest) {
			$renewalRequest->delete();
		}

		return true;
	}

	public function down()
	{
		echo "m170321_164755_MZ_874_remove_renewal_requests_with_old_test_email cannot be reverted.\n";

		return false;
	}
}
