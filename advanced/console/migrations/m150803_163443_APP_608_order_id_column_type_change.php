<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_163443_APP_608_order_id_column_type_change extends Migration
{
	public function up()
	{
		$this->alterColumn(\common\models\Sales::tableName(), 'order_id', Schema::TYPE_BIGINT);

		return true;
	}

	public function down()
	{
		//echo "m150803_163443_APP_608_order_id_column_type_change should be reverted as we would eventually loose data (order_id). Let's keep it safe and don't revert this one.\n";
		$this->alterColumn(\common\models\Sales::tableName(), 'order_id', Schema::TYPE_INTEGER);

		return true;
	}
}
