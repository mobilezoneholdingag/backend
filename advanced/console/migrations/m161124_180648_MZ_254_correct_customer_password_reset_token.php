<?php

use yii\db\Migration;

class m161124_180648_MZ_254_correct_customer_password_reset_token extends Migration
{
	public function up()
	{
		$this->renameColumn('customer', 'passwort_reset_token', 'password_reset_token');

		return true;
	}

	public function down()
	{
		$this->renameColumn('customer', 'password_reset_token', 'passwort_reset_token');

		return false;
	}
}
