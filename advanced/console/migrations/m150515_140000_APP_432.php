<?php

use yii\db\Schema;
use yii\db\Migration;

class m150515_140000_APP_432 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Export::tableName(), 'min_profit', Schema::TYPE_DECIMAL.'(7,2)');
		$this->update(\common\models\Export::tableName(),['min_profit' => 0]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Export::tableName(), 'min_profit');

		return true;
	}
}
