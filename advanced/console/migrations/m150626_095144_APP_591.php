<?php

use yii\db\Schema;
use yii\db\Migration;

class m150626_095144_APP_591 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Tariffs::tableName(), 'price_monthly_later', 'DECIMAL(6,2) AFTER price_monthly');
		$this->addColumn(\common\models\Tariffs::tableName(), 'price_monthly_runtime', 'SMALLINT(3) AFTER price_monthly');

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Tariffs::tableName(), 'price_monthly_later');
		$this->dropColumn(\common\models\Tariffs::tableName(), 'price_monthly_runtime');

		return true;
	}
}
