<?php

use yii\db\Migration;

class m160928_080442_MZ_94_create_manufacturer_accessory_set_id extends Migration
{
	const MANUFACTURER_TABLE_NAME = 'manufacturer';

	public function up()
	{
		$this->addColumn(
			self::MANUFACTURER_TABLE_NAME,
			'accessory_set_id',
			$this->integer()->after('smartphone_set_id')
		);

		$this->addForeignKey(
			'fk_manufacturer_accessory_set_id',
			self::MANUFACTURER_TABLE_NAME,
			'accessory_set_id',
			'sets',
			'id',
			'SET NULL',
			'CASCADE'
		);

		return true;
	}

	public function down()
	{
		$this->dropForeignKey('fk_manufacturer_accessory_set_id', self::MANUFACTURER_TABLE_NAME);
		$this->dropColumn(self::MANUFACTURER_TABLE_NAME, 'accessory_set_id');

		return true;
	}
}
