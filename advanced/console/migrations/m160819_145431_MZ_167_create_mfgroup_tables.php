<?php

use yii\db\Migration;

class m160819_145431_MZ_167_create_mfgroup_tables extends Migration
{
	const CARD_NUMBER_REQUEST_TABLE = 'mfgroup_card_number_request';
	const CARD_NUMBER_RESPONSE_TABLE = 'mfgroup_card_number_response';

	const FINANCIAL_REQUEST_TABLE = 'mfgroup_financial_request';
	const FINANCIAL_RESPONSE_TABLE = 'mfgroup_financial_response';

	const CONFIRMATION_REQUEST_TABLE = 'mfgroup_confirmation_request';
	const CONFIRMATION_RESPONSE_TABLE = 'mfgroup_confirmation_response';

	public function up()
	{
		$this->createTable(
			self::CARD_NUMBER_REQUEST_TABLE,
			[
				'id' => $this->primaryKey()->unsigned(),
				'gender' => $this->string()->notNull(),
				'first_name' => $this->string()->notNull(),
				'last_name' => $this->string()->notNull(),
				'street' => $this->string()->notNull(),
				'city' => $this->string()->notNull(),
				'zip' => $this->char(4)->notNull(),
				'country' => $this->char(2)->notNull(),
				'language' => $this->char(2)->notNull(),
				'phone_number' => $this->string(),
				'mobile_number' => $this->string(),
				'email' => $this->string()->notNull(),
				'birthdate' => $this->date()->notNull(),
				'merchant_id' => $this->integer()->unsigned()->notNull(),
				'filial_id' => $this->integer()->unsigned()->notNull(),
				'terminal_id' => $this->string()->notNull(),
				'amount' => $this->integer()->unsigned(),
				'currency_code' => $this->char(3),
				'created_at' => $this->dateTime(),
			]
		);

		$this->createTable(
			self::CARD_NUMBER_RESPONSE_TABLE,
			[
				'id' => $this->primaryKey()->unsigned(),
				'card_number_request_id' => $this->integer()->unsigned()->notNull(),
				'mf_reference' => $this->integer()->unsigned(),
				'available_credit' => $this->integer()->unsigned(),
				'maximal_credit' => $this->integer()->unsigned(),
				'credit_refusal_reason' => $this->string(),
				'card_number' => $this->string()->comment(
					'This is intentionally a VARCHAR rather than integer, as the numbers get pretty big'
				),
				'response_code' => $this->string(),
				'created_at' => $this->dateTime(),
			]
		);
		$this->addForeignKey(
			'fk_mfgroup_card_number_response_mfgroup_card_number_request',
			self::CARD_NUMBER_RESPONSE_TABLE,
			'card_number_request_id',
			self::CARD_NUMBER_REQUEST_TABLE,
			'id',
			'RESTRICT',
			'CASCADE'
		);

		$this->createTable(
			self::FINANCIAL_REQUEST_TABLE,
			[
				'id' => $this->primaryKey()->unsigned(),
				'card_number_response_id' => $this->integer()->unsigned()->notNull(),
				'card_number' => $this->string()->notNull(),
				'transaction_type' => $this->string()->notNull(),
				'currency' => $this->char(3)->notNull(),
				'amount' => $this->integer()->unsigned()->notNull(),
				'merchant_id' => $this->integer()->unsigned()->notNull(),
				'filial_id' => $this->integer()->unsigned()->notNull(),
				'terminal_id' => $this->string()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
			]
		);
		$this->addForeignKey(
			'fk_mfgroup_financial_request_mfgroup_card_number_response1',
			self::FINANCIAL_REQUEST_TABLE,
			'card_number_response_id',
			self::CARD_NUMBER_RESPONSE_TABLE,
			'id',
			'RESTRICT',
			'CASCADE'
		);

		$this->createTable(
			self::FINANCIAL_RESPONSE_TABLE,
			[
				'id' => $this->primaryKey()->unsigned(),
				'financial_request_id' => $this->integer()->unsigned()->notNull(),
				'response_code' => $this->string()->notNull(),
				'response_date' => $this->string(),
				'authorization_code' => $this->integer()->unsigned(),
				'currency' => $this->char(3),
				'balance' => $this->integer()->unsigned(),
				'card_number' => $this->string(),
				'expiration_date' => $this->integer()->unsigned(),
				'created_at' => $this->dateTime()->notNull(),
			]
		);
		$this->addForeignKey(
			'fk_mfgroup_financial_response_mfgroup_financial_request1',
			self::FINANCIAL_RESPONSE_TABLE,
			'financial_request_id',
			self::FINANCIAL_REQUEST_TABLE,
			'id',
			'RESTRICT',
			'CASCADE'
		);

		$this->createTable(
			self::CONFIRMATION_REQUEST_TABLE,
			[
				'id' => $this->primaryKey()->unsigned(),
				'financial_request_id' => $this->integer()->unsigned()->notNull(),
				'financial_response_id' => $this->integer()->unsigned()->notNull(),
				'request_xml' => $this->text()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
			]
		);
		$this->addForeignKey(
			'fk_mfgroup_confirmation_request_mfgroup_financial_request1',
			self::CONFIRMATION_REQUEST_TABLE,
			'financial_request_id',
			self::FINANCIAL_REQUEST_TABLE,
			'id',
			'RESTRICT',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk_mfgroup_confirmation_request_mfgroup_financial_response1',
			self::CONFIRMATION_REQUEST_TABLE,
			'financial_response_id',
			self::FINANCIAL_RESPONSE_TABLE,
			'id',
			'RESTRICT',
			'CASCADE'
		);

		$this->createTable(
			self::CONFIRMATION_RESPONSE_TABLE,
			[
				'id' => $this->primaryKey()->unsigned(),
				'confirmation_request_id' => $this->integer()->unsigned()->notNull(),
				'response_xml' => $this->text()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
			]
		);
		$this->addForeignKey(
			'fk_mfgroup_confirmation_response_mfgroup_confirmation_request1',
			self::CONFIRMATION_RESPONSE_TABLE,
			'confirmation_request_id',
			self::CONFIRMATION_REQUEST_TABLE,
			'id',
			'RESTRICT',
			'CASCADE'
		);

		return true;
	}

	public function down()
	{
		$this->dropTable(self::CONFIRMATION_RESPONSE_TABLE);
		$this->dropTable(self::CONFIRMATION_REQUEST_TABLE);

		$this->dropTable(self::FINANCIAL_RESPONSE_TABLE);
		$this->dropTable(self::FINANCIAL_REQUEST_TABLE);

		$this->dropTable(self::CARD_NUMBER_RESPONSE_TABLE);
		$this->dropTable(self::CARD_NUMBER_REQUEST_TABLE);

		return true;
	}
}
