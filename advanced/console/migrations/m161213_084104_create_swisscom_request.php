<?php

use yii\db\Migration;

/**
 * Handles the creation for table `swisscom_request`.
 */
class m161213_084104_create_swisscom_request extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable(
			'swisscom_request',
			[
				'id' => $this->primaryKey(),
				'endpoint' => $this->text(),
				'method' => $this->string(),
				'request_options' => $this->text(),
				'created_at' => $this->dateTime(),
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropTable('swisscom_request');
	}
}
