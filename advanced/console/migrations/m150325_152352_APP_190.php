<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Tariffs;

class m150325_152352_APP_190 extends Migration
{
	public function up()
	{
		$this->update(Tariffs::tableName(), ['set_id' => 33], ['set_id' => null]);

		return true;
	}

	public function down()
	{
		echo "m150325_152352_APP_190 cannot be reverted.\n";

		return false;
	}

}
