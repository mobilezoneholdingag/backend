<?php

use common\models\Customer;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use yii\db\Migration;

class m170207_134628_MZ_510_fix_non_converted_passwords extends Migration
{
	public function up()
	{
		$customerQuery = Customer::find()->where(['IS NOT', 'password_hash', NULL]);
		/** @var Customer[] $customers */
		$customers = $customerQuery->all();
		$count = $customerQuery->count();
		echo "Processing $count customer passwords.";
		$progressBar = new ProgressBar(new ConsoleOutput(), $count);
		foreach ($customers as $customer) {
			$progressBar->advance();
			if (strlen($customer->password_hash) >= 40) {
				continue; // do not hash hashed passwords again
			}
			$customer->setPasswordHashByPassword($customer->password_hash);
			$customer->save(false);
		}
		$progressBar->finish();

		return true;
	}

	public function down()
	{
		echo "m170207_134628_MZ_510_fix_non_converted_passwords cannot be reverted.\n";

		return false;
	}
}
