<?php

use yii\db\Schema;
use yii\db\Migration;

class m150401_150901_APP_295_feature_payment_fix extends Migration
{
	public function up()
	{
		$this->execute('ALTER TABLE sales CHANGE payment_type payment_type int(11) DEFAULT NULL');
	}

	public function down()
	{
		$this->execute('ALTER TABLE sales CHANGE payment_type payment_type int(11) NOT NULL');
	}
}
