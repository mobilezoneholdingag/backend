<?php

use common\models\Article;
use yii\db\Migration;

class m160815_154846_MZ_138_add_rate_payment_attribute_to_article extends Migration
{
	public function up()
	{
		$this->addColumn(
			Article::tableName(),
			'rate_payment_enabled',
			$this->boolean()->notNull()->defaultValue(1)->after('price_buy')
		);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Article::tableName(), 'rate_payment_enabled');

		return true;
	}
}
