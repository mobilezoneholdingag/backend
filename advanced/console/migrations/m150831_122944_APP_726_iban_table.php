<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_122944_APP_726_iban_table extends Migration
{
	public function up()
	{
		$this->createTable('sales_whitelist_iban', [
			'id' => Schema::TYPE_PK,
			'iban' => Schema::TYPE_STRING,
		]);

		$this->createIndex('iban', 'sales_whitelist_iban', 'iban', true);

		return true;
	}

	public function down()
	{
		$this->dropTable('sales_whitelist_iban');

		return true;
	}
}
