<?php

use yii\db\Migration;

class m160928_121716_MZ_145_create_order_item_index_for_stock_calculation extends Migration
{
	const ORDER_ITEM_TABLE = 'order_item';

	public function up()
	{
		$this->createIndex(
			'idx_created_at__product_type_id__item_id',
			self::ORDER_ITEM_TABLE,
			[
				'created_at',
				'item_product_type_id',
				'item_id',
			]
		);

		return true;
	}

	public function down()
	{
		$this->dropIndex('idx_created_at__product_type_id__item_id', self::ORDER_ITEM_TABLE);

		return true;
	}
}
