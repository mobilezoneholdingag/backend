<?php
/**
 * @author Sascha Mummert
 */

use yii\db\Migration;
use common\models\Export;
use yii\db\Schema;

class m160517_135822_APP_2300 extends Migration
{
	public function up()
	{
		$this->addColumn(Export::tableName(), 'filename', Schema::TYPE_STRING . ' ' . 'UNIQUE');
		return true;
	}

	public function down()
	{
		$this->dropColumn(Export::tableName(), 'filename');
		return true;
	}
}
