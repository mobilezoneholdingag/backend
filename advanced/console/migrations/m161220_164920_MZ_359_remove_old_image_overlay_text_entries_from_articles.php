<?php

use yii\db\Migration;

/**
 * Class m161220_164920_MZ_359_remove_old_image_overlay_text_entries_from_articles
 */
class m161220_164920_MZ_359_remove_old_image_overlay_text_entries_from_articles extends Migration
{
	const TABLE_NAME = 'articles';
	const COLUMN_NAME = 'image_overlay_text';

	/**
	 * @return bool
	 */
	public function up()
	{
		$this->update(self::TABLE_NAME, [self::COLUMN_NAME => null]);

		return true;
	}

	/**
	 * @return bool
	 */
	public function down()
	{
		echo "m161220_164920_MZ_359_remove_old_image_overlay_text_entries_from_articles cannot be reverted.\n";

		return false;
	}
}
