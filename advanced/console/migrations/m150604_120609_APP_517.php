<?php

use yii\db\Schema;
use yii\db\Migration;

class m150604_120609_APP_517 extends Migration
{
	public function up()
	{
		$this->createTable('partners', [
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING,
		]);

		$this->insert('partners', ['id' => 1, 'title' => 'EAM']);
		$this->insert('partners', ['id' => 2, 'title' => 'Drillisch']);

		$this->addColumn(\common\models\ArticleTariffs::tableName(), 'external_id', Schema::TYPE_STRING);
		$this->addColumn(\common\models\ArticleTariffs::tableName(), 'partner_id', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropTable('partners');

		$this->dropColumn(\common\models\ArticleTariffs::tableName(), 'external_id');

		return true;
	}
}
