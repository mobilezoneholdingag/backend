<?php

use common\models\Article;
use yii\db\Migration;

class m160829_141803_MZ_179_move_barcode_to_manufacturer_article_id_of_article extends Migration
{
    public function up()
    {
	    $this->execute(
		    /** @lang MySQL */
		    'UPDATE articles SET manufacturer_article_id = barcode'
	    );

	    $this->dropColumn(Article::tableName(), 'barcode');

	    return true;
    }

    public function down()
    {
        echo "m160829_141803_MZ_179_move_barcode_to_manufacturer_article_id_of_article cannot be reverted.\n";

        return false;
    }
}
