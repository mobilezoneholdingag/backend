<?php

use yii\db\Schema;
use yii\db\Migration;

class m150423_162533_app_383 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_monthly_price', Schema::TYPE_FLOAT);
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_connection_fee', Schema::TYPE_FLOAT);
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_contract_duration', Schema::TYPE_INTEGER);

		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'tariff_monthly_price', Schema::TYPE_FLOAT);
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'tariff_connection_fee', Schema::TYPE_FLOAT);
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'tariff_contract_duration', Schema::TYPE_INTEGER);
	}

	public function down()
	{
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_monthly_price');
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_connection_fee');
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_contract_duration');

		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'tariff_monthly_price');
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'tariff_connection_fee');
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'tariff_contract_duration');
	}
}
