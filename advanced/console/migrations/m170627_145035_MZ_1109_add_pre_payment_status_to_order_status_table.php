<?php

use yii\db\Migration;

class m170627_145035_MZ_1109_add_pre_payment_status_to_order_status_table extends Migration
{
	const TABLE = 'order_status';
	const STATUS_ID = 11;

	public function up()
	{
		$this->insert(self::TABLE, ['id' => self::STATUS_ID, 'code' => 'awaiting_prepayment']);

		return true;
	}

	public function down()
	{
		$this->delete(self::TABLE, ['id' => self::STATUS_ID]);

		return true;
	}
}
