<?php

use yii\db\Migration;

class m170428_094241_MZ_825_remove_unusued_image_titles_column_in_shop extends Migration
{
	const SHOP_TABLE = 'shop';
	const IMAGES_TITLE_COLUMN = 'images_title';

	public function up()
	{
		$this->dropColumn(self::SHOP_TABLE, self::IMAGES_TITLE_COLUMN);
		
		return true;
	}

	public function down()
	{
		$this->addColumn(self::SHOP_TABLE, self::IMAGES_TITLE_COLUMN, $this->string(255));

		return true;
	}
}
