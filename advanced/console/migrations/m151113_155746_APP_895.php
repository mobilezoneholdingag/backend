<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_155746_APP_895 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\LandingPages::tableName(), 'headline', 'VARCHAR(255) DEFAULT NULL');
	}

	public function down()
	{
		$this->dropColumn(\common\models\LandingPages::tableName(), 'headline');
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
