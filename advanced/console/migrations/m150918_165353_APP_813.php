<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_165353_APP_813 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Sales::tableName(), 'old_provider_type', 'INTEGER AFTER old_phone_number_main');
		$this->addColumn(\common\models\Sales::tableName(), 'old_provider_port_request_status', 'INTEGER AFTER old_phone_number_main');
		$this->addColumn(\common\models\Sales::tableName(), 'old_provider_port_request_expiration_date', 'VARCHAR(255) AFTER old_phone_number_main');
	}

	public function down()
	{
		$this->dropColumn(\common\models\Sales::tableName(), 'old_provider_type');
		$this->dropColumn(\common\models\Sales::tableName(), 'old_provider_port_request_status');
		$this->dropColumn(\common\models\Sales::tableName(), 'old_provider_port_request_expiration_date');
	}
}
