<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_111301_APP_1408 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\FulfillmentPartners::tableName(), 'api_is_active', Schema::TYPE_BOOLEAN);
		$this->update(\common\models\FulfillmentPartners::tableName(), ['api_is_active' => true]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\FulfillmentPartners::tableName(), 'api_is_active');

		return true;
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
