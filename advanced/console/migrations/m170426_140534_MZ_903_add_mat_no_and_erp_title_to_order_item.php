<?php

use yii\db\Migration;

class m170426_140534_MZ_903_add_mat_no_and_erp_title_to_order_item extends Migration
{
	const ORDER_ITEM_TABLE = 'order_item';
	const MAT_NO_COLUMN = 'mat_no';
	const ERP_TITLE_COLUMN = 'erp_title';

	public function up()
	{
		$this->addColumn(self::ORDER_ITEM_TABLE, self::MAT_NO_COLUMN, $this->string(255)->after('parent_item_id'));
		$this->addColumn(self::ORDER_ITEM_TABLE, self::ERP_TITLE_COLUMN, $this->string(255)->after('title'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::ORDER_ITEM_TABLE, self::MAT_NO_COLUMN);
		$this->dropColumn(self::ORDER_ITEM_TABLE, self::ERP_TITLE_COLUMN);

		return true;
	}
}
