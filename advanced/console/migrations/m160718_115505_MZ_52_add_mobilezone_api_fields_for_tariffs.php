<?php

use common\models\Tariffs;
use yii\db\Migration;

class m160718_115505_MZ_52_add_mobilezone_api_fields_for_tariffs extends Migration
{
	public function up()
	{
		$this->addColumn(Tariffs::tableName(), 'offer_type_id', $this->integer()->notNull()->after('provider_id'));
		$this->addColumn(Tariffs::tableName(), 'api_updated_at', $this->dateTime()->after('updated_at'));
	}

	public function down()
	{
		$this->dropColumn(Tariffs::tableName(), 'offer_type_id');
		$this->dropColumn(Tariffs::tableName(), 'api_updated_at');
	}
}
