<?php

use yii\db\Migration;

/**
 * Class m170117_141735_MZ_330_alter_table_tariffs_add_column_princing_strategy
 */
class m170117_141735_MZ_330_alter_table_tariffs_add_column_princing_strategy extends Migration
{
	const TABLE_NAME = 'tariffs';
	const COLUMN_NAME = 'pricing_strategy';

	/**
	 * @return bool
	 */
	public function up()
	{
		$this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->string(50));
		$this->update(self::TABLE_NAME, [self::COLUMN_NAME => 'swisscom'], ['provider_id' => 2]);
		$this->update(self::TABLE_NAME, [self::COLUMN_NAME => 'sunrise'], ['provider_id' => 1]);
		$this->update(self::TABLE_NAME, [self::COLUMN_NAME => 'upc'], ['provider_id' => 13]);
		$this->update(self::TABLE_NAME, [self::COLUMN_NAME => 'talktalk'], ['provider_id' => 20]);

		return true;
	}

	/**
	 * @return bool
	 */
	public function down()
	{
		$this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);

		return true;
	}
}
