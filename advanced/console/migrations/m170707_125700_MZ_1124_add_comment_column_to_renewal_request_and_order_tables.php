<?php

use yii\db\Migration;

class m170707_125700_MZ_1124_add_comment_column_to_renewal_request_and_order_tables extends Migration
{
	const COMMENT_FIELD = 'comment';
	const RENEWAL_REQUESTS_TABLE = 'renewal_requests';
	const ORDER_TABLE = 'order';

	public function up()
	{
		$this->addColumn(self::ORDER_TABLE, self::COMMENT_FIELD, $this->text());
		$this->addColumn(self::RENEWAL_REQUESTS_TABLE, self::COMMENT_FIELD, $this->text());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::ORDER_TABLE, self::COMMENT_FIELD);
		$this->dropColumn(self::RENEWAL_REQUESTS_TABLE, self::COMMENT_FIELD);

		return true;
	}
}
