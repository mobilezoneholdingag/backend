<?php

use yii\db\Migration;

class m170123_163039_MZ_405_rename_serial_number_and_shipment_tracking_id_of_order extends Migration
{
	const ORDER_TABLE = 'order';

	public function up()
	{
		$this->renameColumn(self::ORDER_TABLE, 'serial_number', 'serial_numbers');
		$this->renameColumn(self::ORDER_TABLE, 'shipment_tracking_id', 'shipment_tracking_ids');

		return true;
	}

	public function down()
	{
		$this->renameColumn(self::ORDER_TABLE, 'serial_numbers', 'serial_number');
		$this->renameColumn(self::ORDER_TABLE, 'shipment_tracking_ids', 'shipment_tracking_id');

		return true;
	}
}
