<?php

use yii\db\Schema;
use yii\db\Migration;

class m151012_142924_APP_940 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Sales::tableName(), 'withdrawal_reason_id', Schema::TYPE_INTEGER);
	}

	public function down()
	{
		$this->dropColumn(\common\models\Sales::tableName(), 'withdrawal_reason_id');
	}
}
