<?php

use yii\db\Schema;
use yii\db\Migration;

class m150330_135245_app_185b extends Migration
{
	public function up()
	{
		$this->addColumn('sales', 'ip', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->addColumn('sales', 'ip', Schema::TYPE_STRING);

		return true;
	}
	
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}
	
	public function safeDown()
	{
	}
	*/
}
