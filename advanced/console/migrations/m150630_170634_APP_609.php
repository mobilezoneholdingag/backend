<?php

use yii\db\Migration;
use yii\db\Schema;
use common\models\Sales;
use backend\models\SalesUnapproved;
use common\models\Tariffs;
use common\models\ArticleTariffs;
use common\models\FulfillmentPartners;


class m150630_170634_APP_609 extends Migration
{
	public function up()
	{
		$this->renameColumn(Sales::tableName(), 'order_id_eam', 'order_id');
		$this->renameColumn(SalesUnapproved::tableName(), 'order_id_eam', 'order_id');

		$this->renameColumn(Sales::tableName(), 'partner_id', 'marketing_partner_id');
		$this->renameColumn(SalesUnapproved::tableName(), 'partner_id', 'marketing_partner_id');

		$this->addColumn(Sales::tableName(), 'fulfillment_partner_id', Schema::TYPE_STRING);
		$this->addColumn(SalesUnapproved::tableName(), 'fulfillment_partner_id', Schema::TYPE_STRING);

		$this->renameColumn(Tariffs::tableName(), 'partner_id', 'fulfillment_partner_id');
		$this->renameColumn(ArticleTariffs::tableName(), 'partner_id', 'fulfillment_partner_id');

		$this->renameTable('partners', 'fulfillment_partners');
		$this->addColumn(FulfillmentPartners::tableName(), 'debitor_id', Schema::TYPE_STRING);

		$this->update(Sales::tableName(), ['fulfillment_partner_id' => 1]);
		$this->update(SalesUnapproved::tableName(), ['fulfillment_partner_id' => 1]);
		$this->update(Tariffs::tableName(), ['fulfillment_partner_id' => 1]);
		$this->update(ArticleTariffs::tableName(), ['fulfillment_partner_id' => 1]);

		$this->dropPrimaryKey('articletariffsindex', ArticleTariffs::tableName());
		$this->addPrimaryKey('articletariffsindex', ArticleTariffs::tableName(), ['article_id', 'tariff_id', 'fulfillment_partner_id']);

		return true;
	}

	public function down()
	{
		$this->renameColumn(Sales::tableName(), 'order_id', 'order_id_eam');
		$this->renameColumn(SalesUnapproved::tableName(), 'order_id', 'order_id_eam');

		$this->renameColumn(Sales::tableName(), 'marketing_partner_id', 'partner_id');
		$this->renameColumn(SalesUnapproved::tableName(), 'marketing_partner_id', 'partner_id');

		$this->dropColumn(Sales::tableName(), 'fulfillment_partner_id');
		$this->dropColumn(SalesUnapproved::tableName(), 'fulfillment_partner_id');

		$this->renameColumn(Tariffs::tableName(), 'fulfillment_partner_id', 'partner_id');
		$this->renameColumn(ArticleTariffs::tableName(), 'fulfillment_partner_id', 'partner_id');

		$this->dropColumn(FulfillmentPartners::tableName(), 'debitor_id');
		$this->renameTable('fulfillment_partners', 'partners');

		$this->dropPrimaryKey('articletariffsindex', ArticleTariffs::tableName());
		$this->addPrimaryKey('articletariffsindex', ArticleTariffs::tableName(), ['article_id', 'tariff_id']);


		return true;
	}
}
