<?php

use yii\db\Schema;
use yii\db\Migration;

class m151020_111302_APP_881 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_special_telekom_magenta_telephone_number', Schema::TYPE_STRING);
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_special_telekom_magenta_customer_number', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_special_telekom_magenta_telephone_number');
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_special_telekom_magenta_customer_number');

		return true;
	}
}
