<?php

use yii\db\Schema;
use yii\db\Migration;

class m151021_145003_APP_947 extends Migration
{
	public function up()
	{
		$this->createTable('sales_rejects_contacted', [
			'id' => Schema::TYPE_PK,
			'email' => Schema::TYPE_STRING,
		]);

		$this->createIndex('email', 'sales_rejects_contacted', 'email', true);

		return true;
	}

	public function down()
	{
		$this->dropTable('sales_rejects_contacted');

		return true;
	}
}
