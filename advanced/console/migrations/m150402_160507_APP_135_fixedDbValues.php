<?php

use yii\db\Schema;
use yii\db\Migration;

class m150402_160507_APP_135_fixedDbValues extends Migration
{
	public function up()
	{
		$this->update(common\models\ArticleDetailsValues::tableName(), ['detail_29' => 'Multi-Touch Retina Display'],["article_id"=>12613]);
		$this->update(common\models\ArticleDetailsValues::tableName(), ['detail_29' => 'Multi-Touch Retina Display'],["article_id"=>12630]);
	}

	public function down()
	{
		echo "m150402_160507_APP_135_fixedDbValues cannot be reverted.\n";

		return false;
	}
	
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}
	
	public function safeDown()
	{
	}
	*/
}
