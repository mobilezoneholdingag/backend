<?php

use yii\db\Schema;
use yii\db\Migration;

class m150915_141952_APP_598 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\LandingPages::tableName(), 'offers', 'TEXT');
		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\LandingPages::tableName(), 'offers');
		return true;
	}
}
