<?php

use yii\db\Migration;

/**
 * Handles adding insurance_duration to table `articles`.
 */
class m160929_112459_add_insurance_duration_to_articles extends Migration
{
	const TABLE = 'articles';

	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->addColumn(self::TABLE, 'insurance_duration', $this->integer()->defaultValue(0));

		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropColumn(self::TABLE, 'insurance_duration');

		return true;
	}
}
