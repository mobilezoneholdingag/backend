<?php

use yii\db\Migration;

class m160727_125012_MZ_57_Customer_database extends Migration
{
	public function up()
	{

		$this->createTable(
			'customer',
			[
				'id' => $this->primaryKey(),
				'email' => $this->string(255)->unique()->notNull(),
				'crm_id' => $this->integer(11),
				'source' => $this->string(255),
				'source_details' => $this->string(255),
				'is_guest_account' => $this->boolean(),
				'password_hash' => $this->string(255),
				'password_salt' => $this->string(255),
				'passwort_reset_token' => $this->string(255),
				'language' => $this->string(255),
				'nationality' => $this->string(255),
				'passport' => $this->string(255),
				'passport_type' => $this->integer(5),
				'birthday' => $this->string(255),
				'birthdayplace' => $this->string(255),
				'updated_at' => $this->timestamp(),
				'created_at' => $this->timestamp(),
			]
		);

		$this->createTable(
			'customer_address',
			[
				'id' => $this->primaryKey(),
				'customer_id' => $this->integer(11)->notNull(),
				'guid' => $this->string(255),
				'is_active' => $this->boolean(),
				'is_default' => $this->boolean(),
				'sex' => $this->boolean(),
				'firstname' => $this->string(255),
				'lastname' => $this->string(255),
				'company' => $this->string(100),
				'street' => $this->string(150),
				'house_number' => $this->string(25),
				'zip' => $this->integer(20),
				'city' => $this->string(100),
				'country' => $this->string(100),
				'contact_phone_number' => $this->integer(25),
				'additional_info' => $this->string(255),
				'updated_at' => $this->timestamp(),
				'created_at' => $this->timestamp(),
			]

		);

		$this->createTable(
			'customer_phone_number',
			[
				'id' => $this->primaryKey(),
				'customer_id' => $this->integer(11)->notNull(),
				'number' => $this->integer(25),
				'provider' => $this->string(100),
				'updated_at' => $this->timestamp(),
				'created_at' => $this->timestamp(),
			]
		);

		$this->createTable(
			'newsletter_subscriber',
			[
				'id' => $this->primaryKey(),
				'customer_id' => $this->integer(11)->notNull(),
				'standard' => $this->integer(1),
				'press' => $this->integer(1),
				'investor_relations' => $this->integer(1),
				'updated_at' => $this->timestamp(),
				'created_at' => $this->timestamp(),
			]
		);

		$this->addForeignKey('fk_customer_address_customer_id', 'customer_address', 'customer_id', 'customer', 'id', 'cascade');
		$this->addForeignKey('fk_customer_phone_number_customer_id', 'customer_phone_number', 'customer_id', 'customer', 'id', 'cascade');
		$this->addForeignKey('fk_newsletter_subscriber_customer_id', 'newsletter_subscriber', 'customer_id', 'customer', 'id', 'cascade');
	}

	public function down()
	{
		$this->dropTable('customer_address');
		$this->dropTable('customer_phone_number');
		$this->dropTable('newsletter_subscriber');
		$this->dropTable('customer');

		return true;
	}
}
