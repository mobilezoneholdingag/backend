<?php

use yii\db\Schema;
use yii\db\Migration;

class m151123_115530_APP_976 extends Migration
{

	const MP_TABLE = 'marketing_partners';
	const VERIVOX = 'Verivox';
	const CHECK24 = 'CHECK24';

	public function up()
	{
		$this->createTable(self::MP_TABLE, [
			'id' => 'pk',
			'title' => Schema::TYPE_STRING,
		]);

		$this->insert(self::MP_TABLE, ['title' => self::VERIVOX]);
		$this->insert(self::MP_TABLE, ['title' => self::CHECK24]);

		return true;
	}

	public function down()
	{
		$this->dropTable(self::MP_TABLE);

		return true;
	}
}
