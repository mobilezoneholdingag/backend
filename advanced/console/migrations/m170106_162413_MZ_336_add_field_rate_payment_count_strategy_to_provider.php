<?php

use yii\db\Migration;

class m170106_162413_MZ_336_add_field_rate_payment_count_strategy_to_provider extends Migration
{
	const NEW_FIELD = 'rate_payment_count_strategy';

	public function up()
	{
		$this->addColumn('provider', self::NEW_FIELD, $this->string());
		$this->update('provider', [self::NEW_FIELD => 'equal_contract_duration'], ['system_title' => 'swisscom']);
		$this->update('provider', [self::NEW_FIELD => 'always_24'], ['system_title' => 'upc']);
		$this->update('provider', [self::NEW_FIELD => 'always_24'], ['system_title' => 'sunrise']);
		$this->update('provider', [self::NEW_FIELD => 'always_24'], ['system_title' => 'talktalk']);

		return true;
	}

	public function down()
	{
		$this->dropColumn('provider', self::NEW_FIELD);

		return true;
	}
}
