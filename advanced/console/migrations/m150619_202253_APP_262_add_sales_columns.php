<?php

use yii\db\Schema;
use yii\db\Migration;

class m150619_202253_APP_262_add_sales_columns extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_monthly_special_discount', Schema::TYPE_FLOAT);
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_monthly_price_after_special_discount', Schema::TYPE_FLOAT);
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_special_adac_member_id', Schema::TYPE_INTEGER);
		$this->addColumn(\common\models\Sales::tableName(), 'tariff_special_id', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_monthly_special_discount');
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_monthly_price_after_special_discount');
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_special_adac_member_id');
		$this->dropColumn(\common\models\Sales::tableName(), 'tariff_special_id');

		return true;
	}
}
