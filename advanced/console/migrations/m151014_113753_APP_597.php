<?php

use yii\db\Schema;
use yii\db\Migration;

class m151014_113753_APP_597 extends Migration
{
	public function up()
	{
		$this->dropColumn(\common\models\LandingPages::tableName(), 'meta_keywords');
		$this->addColumn(\common\models\LandingPages::tableName(), 'redirect_url', Schema::TYPE_STRING);
		$this->addColumn(\common\models\LandingPages::tableName(), 'canonical_url', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->addColumn(\common\models\LandingPages::tableName(), 'meta_keywords', Schema::TYPE_STRING);
		$this->dropColumn(\common\models\LandingPages::tableName(), 'redirect_url');
		$this->dropColumn(\common\models\LandingPages::tableName(), 'canonical_url');

		return true;
	}
}
