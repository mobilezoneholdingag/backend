<?php

use yii\db\Migration;

/**
 * Class m161108_154535_MZ_265_alter_table_add_column_accessory_set_id
 */
class m161108_154535_MZ_265_alter_table_add_column_accessory_set_id extends Migration
{
    private $articleGroupTableName = 'article_groups';

	/**
     * @return bool
     */
    public function up()
    {
        $this->addColumn($this->articleGroupTableName, 'accessory_set_id', $this->integer(11)->after('top_variation_id'));

        return true;
    }

	/**
     * @return bool
     */
    public function down()
    {
        $this->dropColumn($this->articleGroupTableName, 'accessory_set_id');

        return true;
    }
}
