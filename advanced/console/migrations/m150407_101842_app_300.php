<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;

class m150407_101842_app_300 extends Migration
{
	public function up()
	{
		$this->addColumn(Sales::tableName(), 'tariff_device_final_price', Schema::TYPE_FLOAT);
	}

	public function down()
	{
		$this->dropColumn(Sales::tableName(), 'tariff_device_final_price');
	}
}
