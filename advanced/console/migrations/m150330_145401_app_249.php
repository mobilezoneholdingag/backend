<?php

use yii\db\Schema;
use yii\db\Migration;

class m150330_145401_app_249 extends Migration
{
	public function up()
	{
		$this->addColumn('sales', 'total_once', Schema::TYPE_FLOAT);
		$this->addColumn('sales', 'total_monthly', Schema::TYPE_FLOAT);

		return true;
	}

	public function down()
	{
		$this->dropColumn('sales', 'total_once');
		$this->dropColumn('sales', 'total_monthly');

		return true;
	}
}
