<?php

use yii\db\Schema;
use yii\db\Migration;

class m150629_152150_APP_603_new_coupon extends Migration
{
	public function up()
	{
		$this->insert(\common\models\Coupon::tableName(), [
			'code' => 'SPAR10',
			'discount_device_price' => 10
		]);

		return true;
	}

	public function down()
	{
		$this->delete(\common\models\Coupon::tableName(), [
			'code' => 'SPAR10',
			'discount_device_price' => 10
		]);

		return true;
	}
}
