<?php

use yii\db\Schema;
use yii\db\Migration;
use \common\models\ArticleGroups;
use common\models\Tariffs;

class m150529_174211_APP_418 extends Migration
{
	public function up()
	{
		$this->addColumn(ArticleGroups::tableName(), 'meta_title', Schema::TYPE_STRING.'(49)');
		$this->addColumn(ArticleGroups::tableName(), 'meta_description', Schema::TYPE_STRING);

		$this->addColumn(Tariffs::tableName(), 'meta_title', Schema::TYPE_STRING.'(49)');
		$this->addColumn(Tariffs::tableName(), 'meta_description', Schema::TYPE_STRING);
	}

	public function down()
	{
		$this->dropColumn(ArticleGroups::tableName(), 'meta_title');
		$this->dropColumn(ArticleGroups::tableName(), 'meta_description');

		$this->dropColumn(Tariffs::tableName(), 'meta_title');
		$this->dropColumn(Tariffs::tableName(), 'meta_description');
		return true;
	}
}
