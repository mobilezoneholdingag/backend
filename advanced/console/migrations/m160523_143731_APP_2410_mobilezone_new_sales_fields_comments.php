<?php

use common\models\Sales;
use yii\db\Migration;
use yii\db\Schema;

class m160523_143731_APP_2410_mobilezone_new_sales_fields_comments extends Migration
{
	public function up()
	{
		$this->addColumn(Sales::tableName(), 'comments', Schema::TYPE_TEXT);
	}

	public function down()
	{
		$this->dropColumn(Sales::tableName(), 'comments');
	}
}
