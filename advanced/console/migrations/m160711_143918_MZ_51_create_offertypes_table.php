<?php

use yii\db\Migration;
use yii\db\Schema;

class m160711_143918_MZ_51_create_offertypes_table extends Migration
{
	public function up()
	{
		$this->createTable('offer_type', [
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING,
			'is_active' => Schema::TYPE_BOOLEAN,
		]);
	}

	public function down()
	{
		$this->dropTable('offer_type');
	}
}
