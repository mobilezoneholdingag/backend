<?php

use yii\db\Migration;
use yii\db\Schema;

class m160314_132332_APP_1764 extends Migration
{
	public function up()
	{

		$this->createTable('mailing_financefox_gutschein', [
			'id' => Schema::TYPE_PK,
			'sale_id' => Schema::TYPE_INTEGER,
		]);

		$this->createIndex('sale_id', 'mailing_financefox_gutschein', 'sale_id', true);
	}

	public function down()
	{
		$this->dropTable('mailing_financefox_gutschein');
	}
}
