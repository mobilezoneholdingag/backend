<?php

use yii\db\Migration;
use common\models\FulfillmentPartners as FPartners;

/**
 * Adds new order statuses for mobile zone ch fulfilled orders.
 */
class m160412_155351_APP_2025_mobilezonech_order_status extends Migration
{
	public function up()
	{
		$this->insert('order_status', ['fulfillment_partner_id' => FPartners::MOBILEZONE_ID, 'code' => 1, 'successful_sale' => 0, 'rejected_order' => 0, 'title' => 'Neue Bestellung', 'final' => 0]);
		$this->insert('order_status', ['fulfillment_partner_id' => FPartners::MOBILEZONE_ID, 'code' => 2, 'successful_sale' => 0, 'rejected_order' => 0, 'title' => 'In Bearbeitung', 'final' => 0]);
		$this->insert('order_status', ['fulfillment_partner_id' => FPartners::MOBILEZONE_ID, 'code' => 3, 'successful_sale' => 0, 'rejected_order' => 0, 'title' => 'Aktiviert', 'final' => 1]);
		$this->insert('order_status', ['fulfillment_partner_id' => FPartners::MOBILEZONE_ID, 'code' => 4, 'successful_sale' => 0, 'rejected_order' => 1, 'title' => 'Abgelehnt', 'final' => 1]);
	}

	public function down()
	{
		$this->delete('order_status', ['fulfillment_partner_id' => FPartners::MOBILEZONE_ID]);
	}
}
