<?php

use yii\db\Schema;
use yii\db\Migration;
use backend\models\SalesUnapproved;
use common\models\Sales;


class m150519_174616_APP_452 extends Migration
{
	public function up()
	{
		$this->addColumn(SalesUnapproved::tableName(), 'partner_id', Schema::TYPE_STRING);
		$this->addColumn(Sales::tableName(), 'partner_id', Schema::TYPE_STRING);
	}

	public function down()
	{
		$this->dropColumn(SalesUnapproved::tableName(), 'partner_id');
		$this->dropColumn(Sales::tableName(), 'partner_id');
	}
}
