<?php

use yii\db\Migration;
use yii\db\Schema;

class m160426_084548_APP_2126 extends Migration
{
	public function up()
	{

		$this->createTable('mailing_samsung_extra', [
			'id' => Schema::TYPE_PK,
			'sale_id' => Schema::TYPE_INTEGER,
		]);

		$this->createIndex('sale_id', 'mailing_samsung_extra', 'sale_id', true);
	}

	public function down()
	{
		$this->dropTable('mailing_samsung_extra');
	}
}
