<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Tariffs;

class m150817_172352_APP_751 extends Migration
{
	public function up()
	{
		$this->update(Tariffs::tableName(), ['special_id' => 1], ['id' => 1173]);

		return true;
	}

	public function down()
	{
		$this->update(Tariffs::tableName(), ['special_id' => null], ['id' => 1173]);

		return true;
	}
}
