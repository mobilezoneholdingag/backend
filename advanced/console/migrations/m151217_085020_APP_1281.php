<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_085020_APP_1281 extends Migration
{
	public function up()
	{
		$this->createTable('mailing_teufel_gutschein', [
			'id' => Schema::TYPE_PK,
			'code' => Schema::TYPE_STRING,
			'sale_id' => Schema::TYPE_INTEGER,
		]);

		$this->createIndex('code', 'mailing_teufel_gutschein', 'code', true);
		$this->createIndex('sale_id', 'mailing_teufel_gutschein', 'sale_id', true);
	}

	public function down()
	{
		$this->dropTable('mailing_teufel_gutschein');
	}
}
