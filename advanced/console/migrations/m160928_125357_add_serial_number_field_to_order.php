<?php

use yii\db\Migration;

/**
 * Handles adding serial_number_field to table `order`.
 */
class m160928_125357_add_serial_number_field_to_order extends Migration
{
	const TABLE = 'order';

    /**
     * @inheritdoc
     */
    public function up()
    {
    	$this->addColumn(self::TABLE, 'serial_number', $this->string());

	    return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    $this->dropColumn(self::TABLE, 'serial_number');

	    return true;
    }
}
