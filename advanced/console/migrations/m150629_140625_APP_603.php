<?php

use yii\db\Schema;
use yii\db\Migration;

class m150629_140625_APP_603 extends Migration
{
	public function up()
	{
		$this->update(\common\models\Coupon::tableName(), ['code' => 'SPQ2'], ['id' => 1]);
		return true;
	}

	public function down()
	{
		$this->update(\common\models\Coupon::tableName(), ['code' => 'SPQ1'], ['id' => 1]);
		return true;
	}
}
