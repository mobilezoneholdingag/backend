<?php

use yii\db\Migration;

class m170704_130033_MZ_1106_sim_only extends Migration
{
	const TABLE = 'articles';
	const COLUMN_ID = 'id';
	const COLUMN_IS_ACTIVE = 'is_active';
	const SIM_ONLY_ID = '5522';

	public function up()
	{
		$this->update(
			self::TABLE,
			[
				self::COLUMN_IS_ACTIVE => '1',
			],
			[
				self::COLUMN_ID => self::SIM_ONLY_ID,
			]
		);

		return true;
	}

	public function down()
	{
		$this->update(
			self::TABLE,
			[
				self::COLUMN_IS_ACTIVE => '0',
			],
			[
				self::COLUMN_ID => self::SIM_ONLY_ID,
			]
		);

		return true;
	}
}
