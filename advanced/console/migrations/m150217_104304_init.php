<?php

use yii\db\Schema;
use yii\db\Migration;

class m150217_104304_init extends Migration
{
	public function up()
	{
//        if (!YII_ENV_DEV) {
//            return false;
//        }

		$migrationScriptsPath = dirname(Yii::$app->basePath).'/migrations/';

		$dirIt = new DirectoryIterator($migrationScriptsPath);

		$dbName = $this->getDbName();

		if ($dbName) {
			$cmds = [];
			$host = [];
			preg_match('/host=([^\s]+);/', $this->db->dsn, $host);
			if (($host = $host[1]) == false) {
				throw new \Exception('DSN does not contain a host name.', 1);
			}

			foreach ($dirIt as $fi) {
				if ($fi->isDot() == false && $fi->getExtension() == 'sql') {
					$cmds[$fi->getFilename()] = 'mysql -u' . $this->db->username
						. ' -p' . $this->db->password
						. ' --host=' . $host
						. ' < ' . $fi->getRealPath();
				}
			}

			ksort($cmds);

			foreach ($cmds as $cmd) {
				try {
					@exec($cmd);
				}
				catch (\Exception $e)
				{
					echo 'COMMAND: '.$cmd.PHP_EOL.PHP_EOL;
					echo $e->getMessage().PHP_EOL;
					echo $e->getTraceAsString();
				}
			}

			return true;
		}
	}

	public function down()
	{
		$dbName = $this->getDbName();
		$this->execute("DROP DATABASE ".$dbName.";");
		$this->execute("CREATE DATABASE ".$dbName." DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;");
		return true;
	}

	protected function getDbName($default = 'deinhandy')
	{
		$dbName = [];
		preg_match('/database=([\S+]);?/', $this->db->dsn, $dbName);
		$dbName = count($dbName) > 1 ? $dbName[1] : $default;

		return $dbName;
	}
}
