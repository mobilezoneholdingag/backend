<?php

use yii\db\Migration;
use yii\db\Schema;

class m160121_080003_APP_1388 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\ArticleGroups::tableName(), 'article_feed_description', Schema::TYPE_STRING);
		$this->addColumn(\common\models\Tariffs::tableName(), 'tariff_feed_description', Schema::TYPE_STRING);
	}

	public function down()
	{
		$this->dropColumn(\common\models\ArticleGroups::tableName(), 'article_feed_description');
		$this->dropColumn(\common\models\Tariffs::tableName(), 'tariff_feed_description');

		return false;
	}

}
