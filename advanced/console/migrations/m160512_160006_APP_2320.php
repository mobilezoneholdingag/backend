<?php

use yii\db\Migration;
use \common\models\Export;
use \yii\db\mysql\Schema;

class m160512_160006_APP_2320 extends Migration
{
	public function up()
	{
		$this->addColumn(Export::tableName(), 'extra_parameters', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Export::tableName(), 'extra_parameters');

		return true;
	}
}
