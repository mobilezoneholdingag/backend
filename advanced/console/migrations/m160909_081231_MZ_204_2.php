<?php

use yii\db\Migration;
use common\models\Provider;

class m160909_081231_MZ_204_2 extends Migration
{
    public function up()
    {
        $this->alterColumn(Provider::tableName(), 'eam_id', $this->string());

        $providers = Provider::find()->all();

        foreach ($providers as $provider) {
            $this->update(
                Provider::tableName(),
                ['eam_id' => $provider->title],
                ['id' => $provider->id]
            );
        }

        return true;
    }

    public function down()
    {
        $this->alterColumn(Provider::tableName(), 'eam_id', $this->integer());
        $this->update(Provider::tableName(), ['eam_id' => null]);

        return true;
    }
}
