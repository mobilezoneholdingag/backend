<?php

use yii\db\Schema;
use yii\db\Migration;

class m150414_160136_APP_348 extends Migration
{
	public function up()
	{
		$this->addColumn('sales', 'bid', Schema::TYPE_STRING);
	}

	public function down()
	{
		$this->dropColumn('sales', 'bid');
	}
}
