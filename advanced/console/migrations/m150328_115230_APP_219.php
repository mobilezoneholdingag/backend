<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Tariffs;

class m150328_115230_APP_219 extends Migration
{
	public function up()
	{
		$this->addColumn(Tariffs::tableName(), 'rating_value', Schema::TYPE_STRING);
		$this->update(Tariffs::tableName(), ['rating_value' => '5']);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Tariffs::tableName(),'rating_value');

		return true;
	}
}
