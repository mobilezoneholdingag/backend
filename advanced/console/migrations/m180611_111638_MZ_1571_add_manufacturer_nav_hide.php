<?php

use yii\db\Migration;

class m180611_111638_MZ_1571_add_manufacturer_nav_hide extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\Manufacturer::tableName(), 'nav_hide', 'tinyint(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(\common\models\Manufacturer::tableName(), 'nav_hide', 'tinyint');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
