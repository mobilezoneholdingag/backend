<?php

use yii\db\Schema;
use yii\db\Migration;

class m160204_102158_APP_1528 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Provider::tableName(), 'speed_automatic_footnote', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Provider::tableName(), 'speed_automatic_footnote');

		return true;
	}
}
