<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_135743_APP_827_3 extends Migration
{
	public function up()
	{
		$this->dropColumn(\common\models\TariffHighlights::tableName(), 'sort');
		$this->addColumn(\common\models\TariffHighlightDetails::tableName(), 'sort', Schema::TYPE_INTEGER);

		foreach (\common\models\TariffHighlightDetails::find()->all() as $highlight) {
			$this->update(\common\models\TariffHighlightDetails::tableName(), ['sort' => $highlight->id], ['id' => $highlight->id]);
		}

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\TariffHighlightDetails::tableName(), 'sort');
		$this->addColumn(\common\models\TariffHighlights::tableName(), 'sort', Schema::TYPE_INTEGER);

		return true;
	}
}
