<?php

use common\models\Sales;
use yii\db\Schema;
use yii\db\Migration;

class m160510_142934_APP_2026_mobilezone_order_xml extends Migration
{
	public function up()
	{
		$this->addColumn(Sales::tableName(), 'contract_id', Schema::TYPE_STRING);
		$this->addColumn(Sales::tableName(), 'msisdn', Schema::TYPE_STRING);
		$this->addColumn(Sales::tableName(), 'serial_number', Schema::TYPE_STRING);
	}

	public function down()
	{
		$this->dropColumn(Sales::tableName(), 'contract_id');
		$this->dropColumn(Sales::tableName(), 'msisdn');
		$this->dropColumn(Sales::tableName(), 'serial_number');
	}
}
