<?php

use yii\db\Schema;
use yii\db\Migration;

class m150505_180429_APP_401_order_status extends Migration
{
	public function up()
	{
		$this->createTable('order_status', [
			'id' => Schema::TYPE_PK,
			'code' => Schema::TYPE_INTEGER,
			'title' => Schema::TYPE_STRING,
			'final' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
		]);

		$this->createIndex('code', 'order_status', 'code', true);

		$this->insert('order_status', ['code' => 0, 'title' => 'Neue Bestellung', 'final' => 0]);
		$this->insert('order_status', ['code' => 1, 'title' => 'Wartend auf Rückinfo', 'final' => 0]);
		$this->insert('order_status', ['code' => 2, 'title' => 'In Bearbeitung', 'final' => 0]);
		$this->insert('order_status', ['code' => 3, 'title' => 'In Freischaltung', 'final' => 0]);
		$this->insert('order_status', ['code' => 4, 'title' => 'Abgelehnt', 'final' => 1]);
		$this->insert('order_status', ['code' => 5, 'title' => 'Aktiviert', 'final' => 0]);
		$this->insert('order_status', ['code' => 7, 'title' => 'Vorbestellung', 'final' => 0]);
		$this->insert('order_status', ['code' => 9, 'title' => 'Storno', 'final' => 1]);
		$this->insert('order_status', ['code' => 10, 'title' => 'Test', 'final' => 1]);
		$this->insert('order_status', ['code' => 17, 'title' => 'Versendet - DHLIdent', 'final' => 1]);
		$this->insert('order_status', ['code' => 19, 'title' => 'Warten auf Zahlung', 'final' => 0]);
	}

	public function down()
	{
		$this->dropTable('order_status');
	}
}
