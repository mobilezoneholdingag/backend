<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_121436_APP_1225 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\FulfillmentPartners::tableName(), 'api_exist', Schema::TYPE_BOOLEAN);

		$this->update(\common\models\FulfillmentPartners::tableName(), ['api_exist' => true], ['id' => \common\models\FulfillmentPartners::EAM_ID]);
		$this->update(\common\models\FulfillmentPartners::tableName(), ['api_exist' => true], ['id' => \common\models\FulfillmentPartners::DRILLISCH_ID]);
		$this->update(\common\models\FulfillmentPartners::tableName(), ['api_exist' => false], ['id' => \common\models\FulfillmentPartners::EINSUNDEINS_ID]);
		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\FulfillmentPartners::tableName(), 'api_exist');

		return true;
	}
}
