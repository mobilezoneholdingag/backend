<?php

use yii\db\Migration;

class m161201_153741_MZ_422_refine_landing_page_editor_in_backend extends Migration
{
	const TABLE_NAME = 'landingpages';
	const COLUMN_NAME = 'language';

	public function up()
	{
		$this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->string());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);

		return true;
	}
}
