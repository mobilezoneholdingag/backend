<?php

use yii\db\Migration;

class m161216_093443_MZ_381_add_column_term_link_to_articles extends Migration
{
	const TABLE = 'articles';

	public function up()
	{
		$this->addColumn(self::TABLE, 'term_link', $this->string());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, 'term_link');

		return true;
	}
}
