<?php

use yii\db\Schema;
use yii\db\Migration;

class m160105_155437_APP_1358 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Sales::tableName(), 'dhl_tracking_id', Schema::TYPE_STRING);
		$this->addColumn(\common\models\Sales::tableName(), 'payment_status_id', Schema::TYPE_INTEGER);

		$this->createTable('payment_status', [
			'id' => Schema::TYPE_PK,
			'code' => Schema::TYPE_STRING,
			'title' => Schema::TYPE_STRING,
			'fulfillment_partner_id' => Schema::TYPE_INTEGER,
		]);

		$this->insert('payment_status', ['code' => '0', 'title' => 'Offen', 'fulfillment_partner_id' => \common\models\FulfillmentPartners::EAM_ID]);
		$this->insert('payment_status', ['code' => '1', 'title' => 'Bezahlt', 'fulfillment_partner_id' => \common\models\FulfillmentPartners::EAM_ID]);
		$this->insert('payment_status', ['code' => '2', 'title' => 'In Klärung', 'fulfillment_partner_id' => \common\models\FulfillmentPartners::EAM_ID]);
		$this->insert('payment_status', ['code' => '3', 'title' => 'Rückerstattet', 'fulfillment_partner_id' => \common\models\FulfillmentPartners::EAM_ID]);
		$this->insert('payment_status', ['code' => '4', 'title' => 'Teil-Rückerstattet', 'fulfillment_partner_id' => \common\models\FulfillmentPartners::EAM_ID]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Sales::tableName(), 'dhl_tracking_id');
		$this->dropColumn(\common\models\Sales::tableName(), 'payment_status_id');

		$this->dropTable('payment_status');

		return true;
	}

}
