<?php

use yii\db\Migration;

class m160316_101930_APP_1764_clean extends Migration
{
	public function up()
	{
		$this->dropTable('mailing_teufel_gutschein');
		$this->dropTable('mailing_chocri_gutschein');
	}

	public function down()
	{

		echo "\nCant revert Teufel und Chocri Table.\n\n";

		return false;
	}
}
