<?php

use common\models\ArticleTariffs;
use yii\db\Migration;

class m160913_162026_MZ_98_add_index_for_articletariffs_related_ids extends Migration
{
	public function up()
	{
		$this->createIndex(
			'idx_articletariffs_tariff_id_article_id',
			ArticleTariffs::tableName(),
			['article_id', 'tariff_id']
		);

		return true;
	}

	public function down()
	{
		$this->dropIndex('idx_articletariffs_tariff_id_article_id', ArticleTariffs::tableName());

		return true;
	}
}
