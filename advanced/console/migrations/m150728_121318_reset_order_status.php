<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_121318_reset_order_status extends Migration
{
	public function up()
	{
		$this->update(\common\models\Sales::tableName(), ['order_status' => null]);

		return true;
	}

	public function down()
	{
		echo "m150728_121318_reset_order_status cannot be reverted, but it is okay as we want to refill these fields.\n";

		return true;
	}
}
