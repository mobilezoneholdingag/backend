<?php

use yii\db\Schema;
use yii\db\Migration;

class m160119_140030_APP_1403 extends Migration
{
	public function up()
	{
		$glossaryEntries = \common\models\Glossary::find()->all();

		foreach ($glossaryEntries as $glossaryEntry) {
			$glossaryEntry->text = str_replace('.html', '', $glossaryEntry->text);
			$glossaryEntry->save();
		}

		return true;
	}

	public function down()
	{
		echo "m160119_140030_APP_1403 cannot be reverted.\n";

		return true;
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
