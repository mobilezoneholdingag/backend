<?php

use yii\db\Migration;

class m170421_095646_MZ_868_add_logic_and_warranty_column_to_marketing_partner extends Migration
{
	const MARKETING_PARTNERS_TABLE = 'marketing_partners';
	const LOGIC_COLUMN = 'logic';
	const WARRANTY_COLUMN = 'warranty';

	public function up()
	{
		$this->addColumn(self::MARKETING_PARTNERS_TABLE, self::LOGIC_COLUMN, $this->smallInteger());
		$this->addColumn(self::MARKETING_PARTNERS_TABLE, self::WARRANTY_COLUMN, $this->smallInteger());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::MARKETING_PARTNERS_TABLE, self::LOGIC_COLUMN);
		$this->dropColumn(self::MARKETING_PARTNERS_TABLE, self::WARRANTY_COLUMN);

		return true;
	}
}
