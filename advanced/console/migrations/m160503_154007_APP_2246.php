<?php

use yii\db\Migration;
use common\models\Export;

class m160503_154007_APP_2246 extends Migration
{
	private $key = 'iut845jhfkjsduew45';

	public function up()
	{
		$this->insert(Export::tableName(), [
			'partner' => 'Dynamic Yield',
			'preferred_format' => 'csv',
			'key' => $this->key,
			'status' => 1,
			'delimiter' => 'tab',
			'bid' => 'DY',
			'link' => ''
		]);
		return true;
	}

	public function down()
	{
		Export::deleteAll(['key' => $this->key]);
		return true;
	}
}
