<?php

use yii\db\Migration;

class m170215_152555_MZ_701_add_column_url_to_renewal_request_table extends Migration
{
	const TABLE_NAME = 'renewal_requests';
	const COLUMN_NAME = 'url';

	public function up()
	{
		$this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->string(2048));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);

		return true;
	}
}
