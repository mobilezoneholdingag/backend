<?php

use yii\db\Migration;

class m171106_101559_mz_1443_add_client_to_order extends Migration
{
    public function up()
    {

        /* IPV6 = 45 Chars Max */

        $this->addColumn(\common\modules\order\models\Order::tableName(), 'request_client_ip', 'varchar(45)');
        $this->addColumn(\common\modules\order\models\Order::tableName(), 'request_client_agent', 'text');
        $this->addColumn(\common\modules\order\models\Order::tableName(), 'request_client_host', 'varchar(255)');
    }

    public function down()
    {
        $this->dropColumn(\common\modules\order\models\Order::tableName(), 'request_client_ip');
        $this->dropColumn(\common\modules\order\models\Order::tableName(), 'request_client_agent');
        $this->dropColumn(\common\modules\order\models\Order::tableName(), 'request_client_host');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
