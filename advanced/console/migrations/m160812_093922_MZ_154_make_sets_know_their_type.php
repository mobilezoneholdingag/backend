<?php

use common\modules\set\models\Sets;
use yii\db\Migration;

class m160812_093922_MZ_154_make_sets_know_their_type extends Migration
{
	public function up()
	{
		$this->dropIndex('system_title_3', Sets::tableName());
		$this->dropIndex('system_title_2', Sets::tableName());

		$this->renameColumn(Sets::tableName(), 'product_category_id', 'type');
		$this->createIndex('idx_type', Sets::tableName(), 'type');

		return true;
	}

	public function down()
	{
		$this->dropIndex('idx_type', Sets::tableName());
		$this->renameColumn(Sets::tableName(), 'type', 'product_category_id');

		$this->createIndex('system_title_2', Sets::tableName(), 'system_title');
		$this->createIndex('system_title_3', Sets::tableName(), 'system_title');

		return true;
	}
}
