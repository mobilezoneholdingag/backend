<?php

use yii\db\Migration;

class m170329_125043_MZ_496_update_manufacturer_entries extends Migration
{
	public function up()
	{
		$this->execute(
			"
			UPDATE `manufacturer` SET image = NULL, insurance_image = '[\"imid://mobilezone/manufacturer/e2b64fe148e1b9427c036a64ed3c7e33.png\"]' WHERE id = 9;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/d2257b45edeb1feea99e97866dd84bff.png\"]', insurance_image = NULL WHERE id = 11;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/c9f5cf7f0c461df09c3906dc2aa0b2cf.png\"]', insurance_image = NULL WHERE id = 17;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/d1955f7215a03a22ecd7ac193f28f5b9.png\"]', insurance_image = NULL WHERE id = 19;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/c045b747e86eaef8e5454471fb370b14.png\"]', insurance_image = NULL WHERE id = 25;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/f2bf56d9918b0f6e0654f7b1560cdb21.png\"]', insurance_image = NULL WHERE id = 27;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/b2fe666738d96e07991079faa37d16b9.png\"]', insurance_image = '[\"imid://mobilezone/manufacturer/b3545a98b6f7745979a716032eec40c2.png\"]' WHERE id = 32;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/1de0f067bc8d210467071f0d6997d5d4.png\"]', insurance_image = NULL WHERE id = 38;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/fc0ecd0ef1288b560b1db25a6ba80900.png\"]', insurance_image = NULL WHERE id = 44;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/928993f3c77a7b759331ad1975a9ac79.png\"]', insurance_image = NULL WHERE id = 48;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/fee67a81f6ad50d8fbc0668ccb7422fe.png\"]', insurance_image = NULL WHERE id = 50;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/ea3231c090110f47e136f80d20be9cd6.png\"]', insurance_image = NULL WHERE id = 52;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/da9375a2c5611504ace0b0b6ced72b7f.png\"]', insurance_image = NULL WHERE id = 55;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/369204724064f6cdb67ebcc46543f582.png\"]', insurance_image = NULL WHERE id = 58;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/80db543e60e96a33234ece24fe75b91d.png\"]', insurance_image = NULL WHERE id = 60;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/ff2cd9b165b3cea03fa616b106be093a.png\"]', insurance_image = NULL WHERE id = 63;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/c9c79a4171dec6ed0a7f3d6dbc97bf6d.png\"]', insurance_image = NULL WHERE id = 65;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/7d058062cc4194d5f7fac3d3ac311d48.png\"]', insurance_image = NULL WHERE id = 66;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/db9c723597a9355abd3d16ba18d63e34.png\"]', insurance_image = NULL WHERE id = 69;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/159b25a1cfab6f59148bfe0f4e089f8d.png\"]', insurance_image = NULL WHERE id = 72;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/34ef721942c75354e5af31cf9de749d6.png\"]', insurance_image = NULL WHERE id = 74;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/8b58121bd4ddcebc6addccace3fff078.png\"]', insurance_image = NULL WHERE id = 81;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/cbb35762d431060604e5a37f5044d07e.png\"]', insurance_image = NULL WHERE id = 82;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/9c4071f987b741a90d8eda7414cb9eb1.png\"]', insurance_image = NULL WHERE id = 87;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/2a457c80b66b9173bd32d21e825f5671.png\"]', insurance_image = NULL WHERE id = 89;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/57f7950015bb2d82a249d01608db83fd.png\"]', insurance_image = NULL WHERE id = 90;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/46681849d061d7ea789942de1ea44359.png\"]', insurance_image = NULL WHERE id = 91;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/d43723e0affe02a66c6311b52d595c1c.png\"]', insurance_image = NULL WHERE id = 92;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/51d0cc5c099165373165a3a090bda57a.png\"]', insurance_image = NULL WHERE id = 93;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/d0f36622801291ed6421bc364069d238.png\"]', insurance_image = NULL WHERE id = 94;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/5bd7fdc6d4bd98c5088b8df883bd33cb.png\"]', insurance_image = NULL WHERE id = 95;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/5d4eafac0a2d60c08fc3049bb850b537.png\"]', insurance_image = NULL WHERE id = 96;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/566a2ea527b67c49b78bd301e88bbfec.png\"]', insurance_image = NULL WHERE id = 97;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/bcc00562f72bdd698079416bd8117836.png\"]', insurance_image = NULL WHERE id = 100;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/7e7006995a55afc14b2fae3656d97137.png\"]', insurance_image = NULL WHERE id = 101;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/5b1215ce03845d43c6715471cc3a4b98.png\"]', insurance_image = NULL WHERE id = 102;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/eab6944fb38c406759eddd59cb9582f5.png\"]', insurance_image = NULL WHERE id = 104;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/bc86d85c31f27863e09eb6331d3241b6.png\"]', insurance_image = NULL WHERE id = 105;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/bc959c28b953d5d1b119be04257138c5.png\"]', insurance_image = NULL WHERE id = 106;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/9291855c7c3e65fd21f72a39bfafb8a4.png\"]', insurance_image = NULL WHERE id = 107;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/8bcb4ee5f86ff246155891a81e881b27.png\"]', insurance_image = NULL WHERE id = 110;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/05a097da03377a98a9b11ddda2de21eb.png\"]', insurance_image = NULL WHERE id = 111;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/54689dac8561471c7c6bb932d019320b.png\"]', insurance_image = NULL WHERE id = 113;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/6dccccc0230eace2fdeeebde4f2f068f.png\"]', insurance_image = NULL WHERE id = 114;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/7c4f0ffa3795042baec7c0847b739344.png\"]', insurance_image = NULL WHERE id = 122;
			UPDATE `manufacturer` SET image = '[\"imid://mobilezone/manufacturer/4b0d7778a83f8dc1bcef0062be992f5b.png\"]', insurance_image = NULL WHERE id = 124;
		"
		);

		return true;
	}

	public function down()
	{
		echo "m170329_125043_MZ_496_update_manufacturer_entries cannot be reverted.\n";

		return false;
	}
}
