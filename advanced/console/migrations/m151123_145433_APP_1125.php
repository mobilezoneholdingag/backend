<?php

use yii\db\Schema;
use yii\db\Migration;

class m151123_145433_APP_1125 extends Migration
{
	public function up()
	{
		$this->createTable('mailing_teufel', [
			'id' => Schema::TYPE_PK,
			'email' => Schema::TYPE_STRING,
			'code' => Schema::TYPE_STRING,
			'sent' => Schema::TYPE_BOOLEAN,
		]);
	}

	public function down()
	{
		$this->dropTable('mailing_teufel');
	}
}
