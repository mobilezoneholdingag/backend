<?php

use yii\db\Schema;
use yii\db\Migration;

class m150701_154333_APP_590_teufel_contest_table extends Migration
{
	public function up()
	{
		$this->createTable('contest_teufel', [
			'id' => Schema::TYPE_PK,
			'subscriber_id' => Schema::TYPE_INTEGER,
			'device' => Schema::TYPE_STRING,
			'contract_runtime_selected' => Schema::TYPE_INTEGER,
			'contract_runtime_end_month' => Schema::TYPE_INTEGER,
			'contract_runtime_end_year' => Schema::TYPE_INTEGER,
			'contract_costs_type' => Schema::TYPE_INTEGER,
			'contract_costs_amount' => Schema::TYPE_INTEGER,
		]);

		$this->createIndex('subscriber_id', 'contest_teufel', 'subscriber_id', true);
	}

	public function down()
	{
		$this->dropTable('contest_teufel');
	}
}
