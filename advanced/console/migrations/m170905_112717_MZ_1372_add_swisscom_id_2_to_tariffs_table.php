<?php

use yii\db\Migration;

class m170905_112717_MZ_1372_add_swisscom_id_2_to_tariffs_table extends Migration
{
	const TABLE = 'tariffs';
	const SWISSCOM_ID_2_COLUMN = 'swisscom_id_2';

	public function up()
	{
		$this->addColumn(self::TABLE, self::SWISSCOM_ID_2_COLUMN, $this->string()->after('swisscom_id'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::SWISSCOM_ID_2_COLUMN);

		return true;
	}
}
