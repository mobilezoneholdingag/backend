<?php

use yii\db\Migration;

class m170822_120737_MZ_1230_add_offered_status_to_order_status_table extends Migration
{
	const TABLE = 'order_status';
	const STATUS_ID = 12;

	public function up()
	{
		$this->insert(self::TABLE, ['id' => self::STATUS_ID, 'code' => 'offered']);

		return true;
	}

	public function down()
	{
		$this->delete(self::TABLE, ['id' => self::STATUS_ID]);

		return true;
	}
}
