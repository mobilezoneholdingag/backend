<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Provider;

class m150716_130353_APP_611 extends Migration
{
	public function up()
	{
		$this->addColumn(Provider::tableName(), 'mnp_possible', Schema::TYPE_BOOLEAN);

		// set "MNP is possible" for providers BASE, O2, Vodafone
		$this->update(Provider::tableName(), ['mnp_possible' => 1], ['id' => 1]);
		$this->update(Provider::tableName(), ['mnp_possible' => 1], ['id' => 4]);
		$this->update(Provider::tableName(), ['mnp_possible' => 1], ['id' => 6]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Provider::tableName(), 'mnp_possible');

		return true;
	}
}
