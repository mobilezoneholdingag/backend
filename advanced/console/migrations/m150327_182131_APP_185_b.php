<?php

use yii\db\Schema;
use yii\db\Migration;

class m150327_182131_APP_185_b extends Migration
{
	public function up()
	{
		$this->addColumn('sales', 'verivox_id', Schema::TYPE_INTEGER);
		$this->addColumn('sales', 'verivox_transaction_id', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn('sales', 'verivox_id');
		$this->dropColumn('sales', 'verivox_transaction_id');

		return true;
	}
}
