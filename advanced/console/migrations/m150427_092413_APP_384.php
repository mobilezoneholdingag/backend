<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;

class m150427_092413_APP_384 extends Migration
{
	public function up()
	{
		$this->addColumn(Sales::tableName(),'order_status', Schema::TYPE_INTEGER);
	}

	public function down()
	{
		$this->dropColumn(Sales::tableName(),'order_status');
	}
}
