<?php

use yii\db\Migration;

class m170116_103019_MZ_405_new_table_order_info_file_processing_log extends Migration
{
	const NEW_TABLE = 'order_info_file_processing_log';

	public function up()
	{
		$this->createTable(
			self::NEW_TABLE,
			[
				'id' => $this->primaryKey(),
				'filename' => $this->string(),
				'file_content' => $this->text(),
				'processed_at' => $this->dateTime(),
			]
		);

		return true;
	}

	public function down()
	{
		$this->dropTable(self::NEW_TABLE);

		return true;
	}
}
