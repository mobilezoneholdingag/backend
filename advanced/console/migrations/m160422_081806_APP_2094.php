<?php

use yii\db\Migration;
use common\models\Provider;

class m160422_081806_APP_2094 extends Migration
{
	public function up()
	{
		$this->addColumn(Provider::tableName(), 'sort', \yii\db\Schema::TYPE_INTEGER);

		foreach (Provider::find()->all() as $provider) {
			$this->update(Provider::tableName(), ['sort' => $provider->id], ['id' => $provider->id]);
		}

		return true;
	}

	public function down()
	{
		$this->dropColumn(Provider::tableName(), 'sort');

		return true;
	}
}
