<?php

use common\models\ArticleTariffs;
use yii\db\Migration;
use yii\db\Schema;

class m160519_091620_APP_2346 extends Migration
{
	public function up()
	{
		$this->addColumn(ArticleTariffs::tableName(), 'frontend_text', Schema::TYPE_TEXT);
	}

	public function down()
	{
		$this->dropColumn(ArticleTariffs::tableName(), 'frontend_text');
	}
}
