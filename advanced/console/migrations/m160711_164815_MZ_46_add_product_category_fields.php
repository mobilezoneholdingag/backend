<?php

use common\models\ProductCategory;
use yii\db\Migration;
use yii\db\Schema;

class m160711_164815_MZ_46_add_product_category_fields extends Migration
{
	public function up()
	{
		$this->addColumn(ProductCategory::tableName(), 'is_active', Schema::TYPE_BOOLEAN);
		$this->addColumn(ProductCategory::tableName(), 'api_updated_at', Schema::TYPE_DATETIME);
	}

	public function down()
	{
		$this->dropColumn(ProductCategory::tableName(), 'is_active');
		$this->dropColumn(ProductCategory::tableName(), 'api_updated_at');
	}
}
