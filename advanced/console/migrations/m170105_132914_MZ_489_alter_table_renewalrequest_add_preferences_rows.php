<?php

use yii\db\Migration;

class m170105_132914_MZ_489_alter_table_renewalrequest_add_preferences_rows extends Migration
{
	const TABLE_NAME = 'renewal_requests';

	public function up()
	{
		$this->addColumn(self::TABLE_NAME, 'monthly_national_data_volume', $this->integer());
		$this->addColumn(self::TABLE_NAME, 'monthly_international_data_volume', $this->integer());
		$this->addColumn(self::TABLE_NAME, 'monthly_national_phone_minutes', $this->integer());
		$this->addColumn(self::TABLE_NAME, 'monthly_international_phone_minutes', $this->integer());
		$this->addColumn(self::TABLE_NAME, 'monthly_phone_minutes_from_abroad', $this->integer());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_NAME, 'monthly_national_data_volume');
		$this->dropColumn(self::TABLE_NAME, 'monthly_international_data_volume');
		$this->dropColumn(self::TABLE_NAME, 'monthly_national_phone_minutes');
		$this->dropColumn(self::TABLE_NAME, 'monthly_international_phone_minutes');
		$this->dropColumn(self::TABLE_NAME, 'monthly_phone_minutes_from_abroad');

		return true;
	}
}
