<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_143413_app_348_tracker extends Migration
{
	public function up()
	{
		$this->createTable('tracker', [
			'id' => Schema::TYPE_PK,
			'code' => Schema::TYPE_STRING,
			'prio' => Schema::TYPE_INTEGER,
			'title' => Schema::TYPE_STRING,
		]);

		$this->createIndex('code', 'tracker', 'code', true);

		// normal tracker codes
		$this->insert('tracker', ['code' => 'GIG', 'title' => 'Giga', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'LAL-SPW', 'title' => 'sparwelt - ECONA', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'LAL', 'title' => 'Lead Alliance', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'AFN', 'title' => 'Affili.net', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'ZAX', 'title' => 'Zanox', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'DIS-CRI', 'title' => 'Criteo', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'RET-CRI', 'title' => 'Criteo', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'DZM-EMM', 'title' => 'DZM-EMM', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'DZM-PRE', 'title' => 'DZM-PRE', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'DZM-RET', 'title' => 'DZM-RET', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'RET-REX', 'title' => 'RET-REX', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'DZM-FB', 'title' => 'DZM-FB', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'C24', 'title' => 'Check24', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'VVX', 'title' => 'verivox', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'BLDE', 'title' => 'billiger.de', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'GUN', 'title' => 'guenstiger.de', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'HHS', 'title' => 'Handyhase', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'HV24', 'title' => 'handyvertrag24.biz', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'TT', 'title' => 'TopTarif', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'AKL', 'title' => 'aklamio', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'ABO', 'title' => 'aboalarm', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'TEL-OCT', 'title' => 'octopodo', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'PLI', 'title' => 'plinga', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'google_shopping', 'title' => 'G Shopping', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'SEM GOO', 'title' => 'SEM Non-Brand (generic) Google', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'SEM Bing', 'title' => 'SEM Non-Brand (generic) Bing', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'SEM-TM GOO', 'title' => 'SEM Brand Google', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'SEM-TM Bing', 'title' => 'SEM Brand Bing', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'AMZ', 'title' => 'Amazon', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'SOC-FB', 'title' => 'Facebook', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'psmv', 'title' => 'DZM (Sonstiges)', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'nexeps', 'title' => 'DZM (Sonstiges)', 'prio' => 1]);
		$this->insert('tracker', ['code' => 'OFF', 'title' => 'Offline', 'prio' => 1]);

		// fallback-codes, if normal codes were not found
		$this->insert('tracker', ['code' => 'DZM', 'title' => 'DZM', 'prio' => 2]);
		$this->insert('tracker', ['code' => 'SEM', 'title' => 'SEM Non-Brand (generic) Google', 'prio' => 2]);
	}

	public function down()
	{
		$this->dropTable('tracker');
	}
}
