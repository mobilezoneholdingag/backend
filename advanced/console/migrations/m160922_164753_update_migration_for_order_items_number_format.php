<?php

use yii\db\Migration;

/**
 * Class m160922_164753_update_migration_for_order_items_number_format
 */
class m160922_164753_update_migration_for_order_items_number_format extends Migration
{
	const ORDER_ITEM_TABLE_NAME = 'order_item';

	/**
	 * @return bool
	 */
	public function up()
	{
		$this->alterColumn(self::ORDER_ITEM_TABLE_NAME, 'price_once', $this->decimal(8, 2));
		$this->alterColumn(self::ORDER_ITEM_TABLE_NAME, 'price_monthly', $this->decimal(8, 2));
		$this->alterColumn(self::ORDER_ITEM_TABLE_NAME, 'vat_rate', $this->decimal(2, 2));

		return true;
	}

	/**
	 * @return bool
	 */
	public function down()
	{
		echo "m160922_164753_update_migration_for_order_items_number_format cannot be reverted.\n";

		return false;
	}
}
