<?php

use yii\db\Migration;

/**
 * Handles adding sim_card_id to table `article_group`.
 */
class m161028_141521_add_sim_card_id_to_article_group extends Migration
{
	const TABLE = 'article_groups';

	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->addColumn(self::TABLE, 'simcard_id', $this->integer());

		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropColumn(self::TABLE, 'simcard_id');

		return true;
	}
}
