<?php

use yii\db\Migration;
use common\models\Manufacturer;

class m160906_124956_MZ_205 extends Migration
{
	public function up()
	{
		$this->addColumn(Manufacturer::tableName(), 'erp_title', $this->string());

		$manufacturers = Manufacturer::find()->all();

		foreach ($manufacturers as $manufacturer) {
			$this->update(
				Manufacturer::tableName(),
				['erp_title' => $manufacturer->title],
				['id' => $manufacturer->id]
			);
		}

		return true;
	}

	public function down()
	{
		$this->dropColumn(Manufacturer::tableName(), 'erp_title');

		return true;
	}
}
