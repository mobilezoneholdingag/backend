<?php

use yii\db\Migration;

class m161118_153357_MZ_355_add_swisscom_id_to_article extends Migration
{
	const TABLE = 'articles';

	public function up()
	{
		$this->addColumn(self::TABLE, 'swisscom_id', $this->string()->after('manufacturer_article_id'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, 'swisscom_id');

		return true;
	}
}
