<?php

use yii\db\Migration;

class m170905_121358_MZ_1332_add_sr_integration_id_column_to_order_table extends Migration
{
	const SR_INTEGRATION_ID_COLUMN = 'sr_integration_id';
	const ORDER_COLUMN = 'order';

	public function up()
	{
		$this->addColumn(self::ORDER_COLUMN, self::SR_INTEGRATION_ID_COLUMN, $this->string());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::ORDER_COLUMN, self::SR_INTEGRATION_ID_COLUMN);

		return true;
	}
}
