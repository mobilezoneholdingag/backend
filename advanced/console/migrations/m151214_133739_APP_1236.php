<?php

use yii\db\Schema;
use yii\db\Migration;

class m151214_133739_APP_1236 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\OrderStatus::tableName(), 'order_information_step', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\OrderStatus::tableName(), 'order_information_step');

		return true;
	}
}
