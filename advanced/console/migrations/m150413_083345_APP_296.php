<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Article;

class m150413_083345_APP_296 extends Migration
{
	public function up()
	{
		$this->addColumn(Article::tableName(),'is_active', Schema::TYPE_BOOLEAN);
		$this->update(Article::tableName(), ['is_active' => true]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Article::tableName(), 'is_active');

		return true;
	}
}
