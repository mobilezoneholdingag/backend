<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Tariffs;

class m150409_073547_TARIFFDETAILS extends Migration
{
	public function up()
	{
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_38', 'price_mms');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_47', 'bill_online');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_7', 'flatrate_landline');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_12', 'flatrate_mobile_networks');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_17', 'sms_flatrate_mobile_networks');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_29', 'internet');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_86', 'data_volume');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_100', 'speed_max');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_99', 'speed_reduced');
		$this->renameColumn(Tariffs::tableName(), 'tarifdetail_98', 'lte');

		return true;
	}

	public function down()
	{
		$this->renameColumn(Tariffs::tableName(), 'price_mms', 'tarifdetail_38');
		$this->renameColumn(Tariffs::tableName(), 'bill_online', 'tarifdetail_47');
		$this->renameColumn(Tariffs::tableName(), 'flatrate_landline', 'tarifdetail_7');
		$this->renameColumn(Tariffs::tableName(), 'flatrate_mobile_networks', 'tarifdetail_12');
		$this->renameColumn(Tariffs::tableName(), 'sms_flatrate_mobile_networks', 'tarifdetail_17');
		$this->renameColumn(Tariffs::tableName(), 'internet', 'tarifdetail_29');
		$this->renameColumn(Tariffs::tableName(), 'data_volume', 'tarifdetail_86');
		$this->renameColumn(Tariffs::tableName(), 'speed_max', 'tarifdetail_100');
		$this->renameColumn(Tariffs::tableName(), 'speed_reduced', 'tarifdetail_99');
		$this->renameColumn(Tariffs::tableName(), 'lte', 'tarifdetail_98');

		return true;
	}

}
