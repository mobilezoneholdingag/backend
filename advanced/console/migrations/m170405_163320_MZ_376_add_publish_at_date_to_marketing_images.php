<?php

use yii\db\Migration;

class m170405_163320_MZ_376_add_publish_at_date_to_marketing_images extends Migration
{
	const MARKETING_IMAGES_TABLE = 'marketing_images';
	const PUBLISH_AT_COLUMN = 'publish_at';

	public function up()
	{
		$this->addColumn(
			self::MARKETING_IMAGES_TABLE,
			self::PUBLISH_AT_COLUMN,
			$this->date()->notNull()->defaultValue("2017-01-01")
		);
		$this->dropIndex('unique_tag_language', self::MARKETING_IMAGES_TABLE);
		$this->createIndex(
			'unique_tag_language_publish_at',
			self::MARKETING_IMAGES_TABLE,
			['tag', 'language', self::PUBLISH_AT_COLUMN],
			true
		);

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::MARKETING_IMAGES_TABLE, self::PUBLISH_AT_COLUMN);
		$this->dropIndex('unique_tag_language_publish_at', self::MARKETING_IMAGES_TABLE);
		$this->createIndex(
			'unique_tag_language',
			self::MARKETING_IMAGES_TABLE,
			['tag', 'language'],
			true
		);

		return true;
	}
}
