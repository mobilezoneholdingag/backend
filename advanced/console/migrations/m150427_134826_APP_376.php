<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;
use common\models\Article;
use backend\controllers\SalesController;

class m150427_134826_APP_376 extends Migration
{
	public function up()
	{
		$this->alterColumn(Article::tableName(), 'price_buy', Schema::TYPE_FLOAT);

		$this->addColumn(Sales::tableName(), 'marge', Schema::TYPE_FLOAT);

		$sales = Sales::find()->all();
		foreach ($sales as $sale) {
			$sale->marge = SalesController::calculateMarge($sale);
			$sale->save();
		}
	}

	public function down()
	{
		$this->dropColumn(Sales::tableName(), 'marge');
	}
}
