<?php

use common\models\FulfillmentPartners;
use yii\db\Migration;

/**
 * Class m160825_122246_MZ_178_remove_fulfillmentpartners_different_mobilezone
 */
class m160825_122246_MZ_178_remove_fulfillmentpartners_different_mobilezone extends Migration
{
	/**
	 * Delete fulfillment_partners entries different from mobilezone (id != 4).
	 *
	 * @return bool
	 */
	public function up()
	{
		$this->delete(FulfillmentPartners::tableName(), ['id' => [1, 2, 3]]);

		return true;
	}

	/**
	 * Re-insert old fulfillmentpartners.
	 *
	 * @return bool
	 */
	public function down()
	{
		/*
			INSERT INTO `fulfillment_partners` (`id`,`title`,`debitor_id`,`api_exist`,`api_is_active`) VALUES (1,'EAM','',1,1);
			INSERT INTO `fulfillment_partners` (`id`,`title`,`debitor_id`,`api_exist`,`api_is_active`) VALUES (2,'Drillisch',NULL,1,1);
			INSERT INTO `fulfillment_partners` (`id`,`title`,`debitor_id`,`api_exist`,`api_is_active`) VALUES (3,'1&1','',0,1);

		*/

		$vals = [
			[1, 'EAM', '', 1, 1],
			[2, 'Drillisch', null, 1, 1],
			[3, '1&1', '', 0, 1],
		];

		foreach ($vals as $values) {
			$this->insert(
				FulfillmentPartners::tableName(),
				[
					'id' => $values[0],
					'title' => $values[1],
					'debitor_id' => $values[2],
					'api_exist' => $values[3],
					'api_is_active' => $values[4],
				]
			);
		}

		return true;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
