<?php

use yii\db\Migration;
use yii\db\Schema;

class m160209_100640_APP_1536 extends Migration
{
	public function up()
	{
		$this->createTable('mailing_chocri_gutschein', [
			'id' => Schema::TYPE_PK,
			'code' => Schema::TYPE_STRING,
			'sale_id' => Schema::TYPE_INTEGER,
		]);

		$this->createIndex('code', 'mailing_chocri_gutschein', 'code', true);
		$this->createIndex('sale_id', 'mailing_chocri_gutschein', 'sale_id', true);
	}

	public function down()
	{
		$this->dropTable('mailing_chocri_gutschein');
	}
}
