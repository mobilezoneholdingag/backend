<?php

use yii\db\Migration;

class m170818_094932_MZ_1315_add_column_order_id_to_swisscom_request_table extends Migration
{
	const COLUMN = 'order_id';
	const TABLE = 'swisscom_request';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->integer()->unsigned()->after('id'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}
}
