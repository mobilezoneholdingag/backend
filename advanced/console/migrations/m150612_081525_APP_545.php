<?php

use yii\db\Schema;
use yii\db\Migration;

class m150612_081525_APP_545 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\ArticleGroups::tableName(), 'frontend_text', Schema::TYPE_TEXT);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\ArticleGroups::tableName(), 'frontend_text');

		return true;
	}
}
