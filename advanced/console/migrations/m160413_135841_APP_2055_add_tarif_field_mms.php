<?php

use yii\db\Migration;

class m160413_135841_APP_2055_add_tarif_field_mms extends Migration
{
	private $colName = 'mms_flatrate_mobile_networks';

	public function up()
	{
		$this->addColumn(\common\models\Tariffs::tableName(), $this->colName, \yii\db\Schema::TYPE_BOOLEAN);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Tariffs::tableName(), $this->colName);

		return true;
	}
}
