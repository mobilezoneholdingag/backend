<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Migration adding column update_at as DateTime to tables article and tariffs.
 */
class m160426_194505_APP_2140_addcol_updatedat_on_article_and_tariffs extends Migration
{
	/**
	 * Provide data on tables and column to update.
	 *
	 * @return object
	 */
	private function provisionData() {
		return (object)[
			'colName' => 'updated_at',
			'schema' => Schema::TYPE_DATETIME,
			'currDateTime' => date('Y-m-d H:i:s'),
			'tables' => [
				\common\models\Article::tableName(),
				\common\models\Tariffs::tableName(),
			],
		];
	}

	/**
	 * @return bool
	 */
	public function safeUp()
	{
		$data = $this->provisionData();

		foreach ($data->tables as $tableName) {
			$this->addColumn($tableName, $data->colName, $data->schema);
			$this->update($tableName, [$data->colName => $data->currDateTime]);
		}

		return true;
	}

	/**
	 * @return bool
	 */
	public function safeDown()
	{
		$data = $this->provisionData();

		foreach ($data->tables as $tableName) {
			$this->dropColumn($tableName, $data->colName);
		}

		return true;
	}
}
