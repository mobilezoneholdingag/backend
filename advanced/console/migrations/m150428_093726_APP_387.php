<?php

use yii\db\Schema;
use yii\db\Migration;

class m150428_093726_APP_387 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\ShopConfig::tableName(), 'publish_token', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\ShopConfig::tableName(), 'publish_token');

		return true;
	}
}
