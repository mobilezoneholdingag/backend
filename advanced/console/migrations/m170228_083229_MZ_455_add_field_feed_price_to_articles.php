<?php

use yii\db\Migration;

class m170228_083229_MZ_455_add_field_feed_price_to_articles extends Migration
{
	const TABLE = 'articles';
	const NEW_FIELD = 'price_feed';

	public function up()
	{
		$this->addColumn(self::TABLE, self::NEW_FIELD, $this->decimal(8, 2));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::NEW_FIELD);

		return true;
	}
}
