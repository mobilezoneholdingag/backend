<?php

use yii\db\Migration;

class m170130_085348_MZ_323_add_renewal_request_offers extends Migration
{
	const TABLENAME = 'renewal_request_offers';
	const TARIFFS_TABLE = 'tariffs';
	const ARTICLES_TABLE = 'articles';
	const RENWALREQUEST_TABLE = 'renewal_requests';

	public function up()
	{
		$this->createTable(
			self::TABLENAME,
			[
				'id' => $this->primaryKey(),
				'article_id' => $this->integer(),
				'tariff_id' => $this->integer(),
				'renewal_request_id' => $this->integer(),
			]
		);

		$this->addForeignKey(
			'fk_renewal_request_offers_article',
			self::TABLENAME,
			'article_id',
			self::ARTICLES_TABLE,
			'id',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_renewal_request_offers_tariffs',
			self::TABLENAME,
			'tariff_id',
			self::TARIFFS_TABLE,
			'id',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_renewal_request_offers',
			self::TABLENAME,
			'renewal_request_id',
			self::RENWALREQUEST_TABLE,
			'id',
			'CASCADE'
		);

		$this->addColumn(self::RENWALREQUEST_TABLE, 'nationality', $this->string());

		return true;
	}

	public function down()
	{
		$this->dropForeignKey('fk_renewal_request_offers_article', self::TABLENAME);
		$this->dropForeignKey('fk_renewal_request_offers_tariffs', self::TABLENAME);
		$this->dropForeignKey('fk_renewal_request_offers', self::TABLENAME);

		$this->dropTable(self::TABLENAME);
		$this->dropColumn(self::RENWALREQUEST_TABLE, 'nationality');
		
		return true;
	}
}
