<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_110440_APP_1056 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\LandingPages::tableName(), 'noindex', Schema::TYPE_BOOLEAN);
		$this->update(\common\models\LandingPages::tableName(), ['noindex' => false]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\LandingPages::tableName(), 'noindex');

		return true;
	}
}
