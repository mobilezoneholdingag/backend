<?php

use common\models\Article;
use deinhandy\yii2picasso\models\ImageCollection;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use yii\db\Migration;


class m170317_143626_MZ_522_convert_article_images extends Migration
{
	/** @var ConsoleOutput */
	private $output;
	private $processedImagesCount = 0;

	public function up()
	{
		$this->output = new ConsoleOutput();

		$imageData = $this->getImageData();
		$articleImages = $this->getArticleImages($imageData);
		$this->saveImages($articleImages);

		$this->output->writeln(PHP_EOL . "<info>{$this->processedImagesCount} images saved!</info>");
	}

	public function down()
	{
		return true;
	}

	private function getImageData(): array
	{
		$command = $this->db->createCommand('
			SELECT i.imageFile imid, ai.sort position, ai.article_id id 
			FROM article_image ai
			    LEFT JOIN images i ON ai.image_id = i.id
			WHERE imageFile IS NOT NULL
		');

		$result = $command->queryAll();

		$imageCount = count($result);
		$this->output->writeln("<info>Processing {$imageCount} images...</info>");

		return $result;
	}

	private function getArticleImages($imageData = []): array
	{
		$articleImages = [];
		foreach ($imageData as $item) {
			if (!isset($articleImages[intval($item['id'])])) {
				$articleImages[intval($item['id'])] = [];
			}
			$articleImages[intval($item['id'])][intval($item['position']) - 1] = $item['imid'];
		}

		return $articleImages;
	}

	private function saveImages($articleImages)
	{
		$articleCount = count($articleImages);
		$this->output->writeln("<info>Processing {$articleCount} articles...</info>");
		$progressBar = new ProgressBar($this->output, $articleCount);

		foreach ($articleImages as $articleId => $images) {
			$imageCollection = new ImageCollection($images);
			$article = Article::findOne($articleId);
			$article->images = $imageCollection;
			$article->save();
			$progressBar->advance();
			$this->processedImagesCount += $imageCollection->count;
		}

		$progressBar->finish();
	}
}
