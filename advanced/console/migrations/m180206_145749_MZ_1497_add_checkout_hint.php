<?php

use yii\db\Migration;

class m180206_145749_MZ_1497_add_checkout_hint extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\Article::tableName(), 'checkout_hint', 'text');
    }

    public function down()
    {
        $this->dropColumn(\common\models\Article::tableName(), 'checkout_hint', 'text');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
