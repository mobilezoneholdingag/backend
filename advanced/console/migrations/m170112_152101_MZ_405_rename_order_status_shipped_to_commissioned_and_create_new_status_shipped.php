<?php

use yii\db\Migration;

class m170112_152101_MZ_405_rename_order_status_shipped_to_commissioned_and_create_new_status_shipped extends Migration
{
	public function up()
	{
		$this->update('order_status', ['code' => 'commissioned'], ['code' => 'shipped']);
		$this->insert('order_status', ['code' => 'shipped']);

		return true;
	}

	public function down()
	{
		$this->delete('order_status', ['code' => 'shipped']);
		$this->update('order_status', ['code' => 'shipped'], ['code' => 'commissioned']);

		return true;
	}
}
