<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\TariffHighlights;
use common\models\Tariffs;

class m150325_203602_APP_178 extends Migration
{
	public function up()
	{
		$this->truncateTable(TariffHighlights::tableName());
		foreach (Tariffs::find()->all() as $tariff) {
			for ($i = 1; $i <= 5; $i++) {
				$this->insert(TariffHighlights::tableName(), [
					'tariff_id' => $tariff->id,
					'sort' => $i,
					'detail_id' => $i,
				]);
			}
		}

		return true;
	}

	public function down()
	{
		echo "m150325_203602_APP_178 cannot be reverted.\n";

		return false;
	}
}
