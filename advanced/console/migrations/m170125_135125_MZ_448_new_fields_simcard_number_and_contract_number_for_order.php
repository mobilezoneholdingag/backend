<?php

use yii\db\Migration;

class m170125_135125_MZ_448_new_fields_simcard_number_and_contract_number_for_order extends Migration
{
	const ORDER_TABLE = 'order';
	const FIELD_SIMCARD_NUMBER = 'simcard_number';
	const FIELD_CONTRACT_NUMBER = 'contract_number';

	public function up()
	{
		$this->addColumn(self::ORDER_TABLE, self::FIELD_SIMCARD_NUMBER, $this->string());
		$this->addColumn(self::ORDER_TABLE, self::FIELD_CONTRACT_NUMBER, $this->string());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::ORDER_TABLE, self::FIELD_SIMCARD_NUMBER);
		$this->dropColumn(self::ORDER_TABLE, self::FIELD_CONTRACT_NUMBER);

		return true;
	}
}
