<?php

use yii\db\Migration;
use yii\db\Schema;

class m170113_081833_MZ_340_create_new_table_platform extends Migration
{
	const TABLE_PLATFORM = 'platforms';

	public function up()
	{
		$this->createTable(self::TABLE_PLATFORM,
			[
				'id' => $this->primaryKey(),
				'name' => $this->string()->unique(),
			]
		);
		$this->insert(self::TABLE_PLATFORM, ['id' => 1, 'name' => 'Android']);
		$this->insert(self::TABLE_PLATFORM, ['id' => 2, 'name' => 'Windows']);
		$this->insert(self::TABLE_PLATFORM, ['id' => 3, 'name' => 'IOS']);
		$this->insert(self::TABLE_PLATFORM, ['id' => 4, 'name' => 'Blackberry OS']);
		$this->insert(self::TABLE_PLATFORM, ['id' => 5, 'name' => 'Sonstiges']);

		return true;
	}

	public function down()
	{
		$this->dropTable(self::TABLE_PLATFORM);

		return true;
	}
}
