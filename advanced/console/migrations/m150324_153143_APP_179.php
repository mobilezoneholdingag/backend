<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Tariffs;

class m150324_153143_APP_179 extends Migration
{
	public function up()
	{
		$this->renameColumn(Tariffs::tableName(),'tarifdetail_44', 'contract_duration');

		return true;
	}

	public function down()
	{
		$this->renameColumn(Tariffs::tableName(),'contract_duration', 'tarifdetail_44');

		return true;
	}
}
