<?php

use yii\db\Migration;

class m170117_120645_MZ_333_delete_obsolete_columns_article_tariff extends Migration
{
	const TABLE_NAME = 'article_tariffs';

	public function up()
	{
		$this->dropColumn(self::TABLE_NAME, 'price_total_once');
		$this->dropColumn(self::TABLE_NAME, 'price_total_monthly');
	}

	public function down()
	{
		$this->addColumn(self::TABLE_NAME, 'price_total_once', $this->decimal(7, 2)->after('price_article_total'));
		$this->addColumn(self::TABLE_NAME, 'price_total_monthly', $this->decimal(7, 2)->after('price_total_once'));
	}
}
