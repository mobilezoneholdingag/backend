<?php

use yii\db\Migration;

class m160216_080953_APP_1573 extends Migration
{
	public function up()
	{
		$this->createTable('hardware_only_offers', [
			'id' => \yii\db\Schema::TYPE_PK,
			'article_id' => \yii\db\Schema::TYPE_INTEGER,
			'marge' => \yii\db\Schema::TYPE_DECIMAL . '(7,2)',
			'fulfillment_partner_id' => \yii\db\Schema::TYPE_INTEGER,
		]);

		return true;
	}

	public function down()
	{
		$this->dropTable('hardware_only_offers');

		return true;
	}
}
