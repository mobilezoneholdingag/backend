<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Pathfinder;
use common\modules\set\models\Sets;
use common\models\Tariffs;

class m150731_105757_APP_664 extends Migration
{
	public function up()
	{

		#$this->dropTable(Pathfinder::tableName());

		$this->db->createCommand('DROP TABLE IF EXISTS '.Pathfinder::tableName())->execute();


		$this->createTable(Pathfinder::tableName(), [
			'id' => Schema::TYPE_PK,
			'session_id' => Schema::TYPE_STRING,
			'location' => Schema::TYPE_INTEGER,
			'category' => Schema::TYPE_INTEGER,
			'size' => Schema::TYPE_INTEGER,
			'purpose' => Schema::TYPE_INTEGER,
			'os' => Schema::TYPE_INTEGER,
			'created_at' => Schema::TYPE_INTEGER,
		]);

		$this->db->createCommand(
			"UPDATE ".Tariffs::tableName()." SET data_volume = ".
			"REPLACE(data_volume, '.', '') WHERE id != 0;"
		)->execute();

		$this->alterColumn(Tariffs::tableName(), 'data_volume', Schema::TYPE_INTEGER);

		foreach([Pathfinder::DEVICE_SET_ALLROUND, Pathfinder::DEVICE_SET_PRACTICAL, Pathfinder::DEVICE_SET_HIGHEND] as $setName) {
			if (Sets::find()->where(['system_title' => $setName])->count() == 0) {
				$set = new Sets();
				$set->title = 'Wegfinder: ' . ucfirst($setName);
				$set->system_title = $setName;
				$set->product_category_id = \common\models\ProductCategory::SMARTPHONE;
				$set->save(false);
			}
		}

		return true;
	}

	public function down()
	{
		$this->dropTable(\common\models\Pathfinder::tableName());
		$this->alterColumn(\common\models\Tariffs::tableName(), 'data_volume', Schema::TYPE_STRING);

		return true;
	}
}
