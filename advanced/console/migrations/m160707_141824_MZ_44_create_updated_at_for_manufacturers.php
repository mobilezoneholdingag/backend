<?php

use common\models\Manufacturer;
use yii\db\Migration;
use yii\db\Schema;

class m160707_141824_MZ_44_create_updated_at_for_manufacturers extends Migration
{
	public function up()
	{
		$this->addColumn(Manufacturer::tableName(), 'api_updated_at', Schema::TYPE_DATETIME);
	}

	public function down()
	{
		$this->dropColumn(Manufacturer::tableName(), 'api_updated_at');
	}
}
