<?php

use yii\db\Migration;

/**
 * Handles the creation for table `swisscom_response`.
 */
class m161213_084248_create_swisscom_response extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable(
			'swisscom_response',
			[
				'id' => $this->primaryKey(),
				'request_id' => $this->integer(),
				'response' => $this->text(),
				'created_at' => $this->dateTime(),
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropTable('swisscom_response');
	}
}
