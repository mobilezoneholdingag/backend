<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Article;

class m150326_153404_APP_24 extends Migration
{
	public function up()
	{
		$this->addColumn(Article::tableName(), 'tariff_set_id', 'INT(11) DEFAULT NULL');
	}

	public function down()
	{
		$this->dropColumn(Article::tableName(), 'tariff_set_id');
	}
}
