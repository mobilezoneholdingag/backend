<?php

use yii\db\Migration;
use backend\models\Tracker;
use common\models\Seo;
use common\models\SalesWhitelist;
use common\models\SalesWhitelistIban;
use common\models\SalesBlacklist;
use common\models\LandingPages;
use common\models\NewsletterSubscriber;

class m160902_151457_APP_3099 extends Migration
{
    public function up()
    {
        $this->truncateTable(Tracker::tableName());
        $this->truncateTable(Seo::tableName());
        $this->truncateTable(SalesWhitelist::tableName());
        $this->truncateTable(SalesWhitelistIban::tableName());
        $this->truncateTable(SalesBlacklist::tableName());
        $this->truncateTable(LandingPages::tableName());
        $this->truncateTable(NewsletterSubscriber::tableName());

        return true;
    }

    public function down()
    {
        echo "m160902_151457_APP_3099 cannot be reverted.\n";

        return false;
    }
}
