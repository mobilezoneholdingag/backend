<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ArticleTariffs;

class m150608_140347_APP_517_2 extends Migration
{
	public function up()
	{
		$articleTariffs = ArticleTariffs::find()->all();

		/**
		 * @var $articleTariff common\models\ArticleTariffs
		 */
		foreach ($articleTariffs as $articleTariff) {
			$external_id = $articleTariff->tariff_id . "-" . $articleTariff->article_id;
			$this->update(ArticleTariffs::tableName(), ['external_id' => $external_id, 'partner_id' => 1], ['tariff_id' => $articleTariff->tariff_id, 'article_id' => $articleTariff->article_id]);
		}

		return true;
	}

	public function down()
	{
		$this->update(ArticleTariffs::tableName(), ['external_id' => null]);

		return true;
	}
}
