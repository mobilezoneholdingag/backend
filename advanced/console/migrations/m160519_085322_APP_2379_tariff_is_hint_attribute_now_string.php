<?php

use common\models\Tariffs;
use yii\db\Migration;
use yii\db\Schema;

class m160519_085322_APP_2379_tariff_is_hint_attribute_now_string extends Migration
{
	public function up()
	{
		$tariffsWithHint = Tariffs::findAll(['is_hint' => 1]);

		$this->renameColumn(Tariffs::tableName(), 'is_hint', 'hint');
		$this->alterColumn(Tariffs::tableName(), 'hint', Schema::TYPE_STRING);
		$this->update(Tariffs::tableName(), ['hint' => null]);

		foreach ($tariffsWithHint as $tariff) {
			$tariff->hint = 'Tipp';
			$tariff->save(false, ['hint']);
		}
	}

	public function down()
	{
		$tariffsWithHint = Tariffs::findAll(['hint' => 'Tipp']);

		$this->renameColumn(Tariffs::tableName(), 'hint', 'is_hint');
		$this->alterColumn(Tariffs::tableName(), 'is_hint', Schema::TYPE_BOOLEAN);
		$this->update(Tariffs::tableName(), ['is_hint' => 0]);

		foreach ($tariffsWithHint as $tariff) {
			$tariff->is_hint = 1;
			$tariff->save(false, ['is_hint']);
		}
	}
}
