<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;
use backend\models\Tracker;

class m150528_103054_APP_497 extends Migration
{
	public function up()
	{
		foreach (Sales::find()->each() as $sale) {
			$sale->tracker_id = Tracker::getTrackerIdBySale($sale);
			$sale->save(false);
		}
	}

	public function down()
	{
		echo "m150528_103054_APP_497 cannot be reverted.\n";
		return true;
	}
}
