<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ArticleDetailsValues;

class m151229_141749_APP_1062 extends Migration
{
	public function up()
	{
		$valuesArr = ArticleDetailsValues::find()->all();
		foreach ($valuesArr as $values) {
			/**
			 * @param $values ArticleDetailsValues
			 */
			if (isset($values->detail_31) AND isset($values->detail_87) AND $values->detail_87 >= 1) {
				$values->presetValues();
				$values->save();
			}
		}

		$this->update(\common\models\ArticleDetails::tableName(), [
			'sort_id' => 7,
			'group_id' => 5,
			'detail_name' => 'Pixel pro Inch',
			'unit' => 'ppi'
		], ['detail_id' => 88]);

		return true;
	}

	public function down()
	{
		$this->update(\common\models\ArticleDetails::tableName(), [
			'sort_id' => 0,
			'group_id' => 0,
			'detail_name' => 'Detail 88',
			'unit' => ''
		], ['detail_id' => 88]);
		$this->update(ArticleDetailsValues::tableName(), ['detail_88' => null]);

		return true;
	}

	public function getHeightAndWidth($resolution)
	{
		return explode('x', str_replace(' ', '', $resolution));
	}
}
