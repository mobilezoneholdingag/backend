<?php

use yii\db\Migration;

class m161213_090310_MZ_419_adjust_user_model extends Migration
{
	const CUSTOMER_TABLE_NAME = 'customer';
	const NEWSLETTER_SUBSCRIBER_TABLE_NAME = 'newsletter_subscriber';

	public function up()
	{
		$this->addColumn(self::CUSTOMER_TABLE_NAME, 'gender', $this->integer());
		$this->addColumn(self::CUSTOMER_TABLE_NAME, 'first_name', $this->string());
		$this->addColumn(self::CUSTOMER_TABLE_NAME, 'last_name', $this->string());

		$this->renameColumn(self::CUSTOMER_TABLE_NAME, 'passport', 'passport_id');
		$this->renameColumn(self::CUSTOMER_TABLE_NAME, 'birthdayplace', 'place_of_birth');

		$this->dropColumn(self::CUSTOMER_TABLE_NAME, 'crm_id');
		$this->dropColumn(self::CUSTOMER_TABLE_NAME, 'is_guest_account');

		$this->addColumn(self::NEWSLETTER_SUBSCRIBER_TABLE_NAME, 'status', $this->integer());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::CUSTOMER_TABLE_NAME, 'gender');
		$this->dropColumn(self::CUSTOMER_TABLE_NAME, 'first_name');
		$this->dropColumn(self::CUSTOMER_TABLE_NAME, 'last_name');

		$this->renameColumn(self::CUSTOMER_TABLE_NAME, 'passport_id', 'passport');
		$this->renameColumn(self::CUSTOMER_TABLE_NAME, 'place_of_birth', 'birthdayplace');

		$this->addColumn(self::CUSTOMER_TABLE_NAME, 'crm_id', $this->string());
		$this->addColumn(self::CUSTOMER_TABLE_NAME, 'is_guest_account', $this->string());

		$this->dropColumn(self::NEWSLETTER_SUBSCRIBER_TABLE_NAME, 'status');

		return true;
	}
}
