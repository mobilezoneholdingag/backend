<?php

use yii\db\Migration;

class m170322_101556_MZ_837_create_index_for_article_is_active extends Migration
{
	const ARTICLES_TABLE = 'articles';
	const IS_ACTIVE = 'is_active';
	const IS_ACTIVE_ERP = 'is_active_erp';

	public function up()
	{
		$this->createIndex(self::IS_ACTIVE, self::ARTICLES_TABLE, self::IS_ACTIVE);
		$this->createIndex(self::IS_ACTIVE_ERP, self::ARTICLES_TABLE, self::IS_ACTIVE_ERP);
	}

	public function down()
	{
		$this->dropIndex(self::IS_ACTIVE, self::ARTICLES_TABLE);
		$this->dropIndex(self::IS_ACTIVE_ERP, self::ARTICLES_TABLE);
	}
}
