<?php

use yii\db\Migration;

class m170427_131445_MZ_903_add_missing_data_to_order_columns extends Migration
{
    public function up()
    {
		$this->execute("
			UPDATE order_item oi, articles a
			SET oi.mat_no = a.mat_no
			WHERE 
			(oi.item_product_type_id = 'article' OR oi.item_product_type_id = 'sim_card' OR oi.item_product_type_id = 'accessories')
			AND oi.mat_no IS NULL
			AND oi.item_id = a.id
		");

		$this->execute("
			UPDATE order_item oi, tariffs t
			SET oi.erp_title = t.erp_title
			WHERE oi.item_product_type_id = 'tariff'
			AND oi.erp_title IS NULL
			AND oi.item_id = t.id
		");

		$this->execute("
			UPDATE order_item oi, article_tariffs at
			LEFT JOIN articles a ON at.article_id = a.id
			LEFT JOIN tariffs t ON at.tariff_id = t.id
			SET oi.mat_no = a.mat_no, oi.erp_title = t.erp_title
			WHERE oi.item_product_type_id = 'article_tariff'
			AND oi.erp_title IS NULL
			AND oi.mat_no IS NULL
			AND oi.item_id = at.id
		");
    }

    public function down()
    {
        echo "m170427_131445_MZ_903_add_missing_data_to_order_columns cannot be reverted.\n";

        return false;
    }
}
