<?php

use common\models\Article;
use yii\db\Migration;

class m170328_084410_MZ_936_article_manufacturer_id_to_mpn extends Migration
{
	public function up()
	{
		$this->dropIndex('index_for_article_search', Article::tableName());

		$this->createIndex(
			'mat_no',
			Article::tableName(),
			[
				'mat_no',
			]
		);

		$this->createIndex(
			'mpn',
			Article::tableName(),
			[
				'mpn',
			]
		);

		return true;
	}

	public function down()
	{
		$this->dropIndex('mat_no', Article::tableName());
		$this->dropIndex('mpn', Article::tableName());

		return true;
	}
}
