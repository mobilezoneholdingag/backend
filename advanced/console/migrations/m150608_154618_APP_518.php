<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Tariffs;

class m150608_154618_APP_518 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Tariffs::tableName(), 'external_id', Schema::TYPE_STRING);
		$this->addColumn(\common\models\Tariffs::tableName(), 'partner_id', Schema::TYPE_INTEGER);

		$tariffs = Tariffs::find()->all();

		/**
		 * @var $tariff common\models\Tariffs
		 */
		foreach ($tariffs as $tariff) {
			$this->update(Tariffs::tableName(), ['external_id' => $tariff->id, 'partner_id' => 1], ['id' => $tariff->id]);
		}

		return true;
	}

	public function down()
	{
		$this->dropColumn(Tariffs::tableName(), 'external_id');
		$this->dropColumn(Tariffs::tableName(), 'partner_id');

		return true;
	}
}
