<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;

class m150429_133529_APP_369 extends Migration
{
	public function up()
	{
		$this->addColumn(Sales::tableName(), 'insurance_price_yearly', Schema::TYPE_FLOAT);
	}

	public function down()
	{
		$this->dropColumn(Sales::tableName(), 'insurance_price_yearly');
	}
}
