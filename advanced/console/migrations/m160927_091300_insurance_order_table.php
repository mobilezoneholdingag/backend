<?php

use yii\db\Migration;

class m160927_091300_insurance_order_table extends Migration
{
	const TABLE = 'order_insurance';

    public function up()
    {
	    $this->createTable(self::TABLE, [
	    	'id' => $this->primaryKey(),

	    	'insurance_article_id' => $this->integer(9),
		    'insurance_name' => $this->string(),
		    'insurance_duration' => $this->integer(),

		    'article_id' => $this->integer(9),
		    'article_number' => $this->string(),
		    'article_name' => $this->string(),

		    'order_id' => $this->integer()->unsigned(),
		    'order_item_id' => $this->integer()->unsigned(),

		    'customer_id' => $this->integer(),
		    'customer_lastname' => $this->string(),
		    'customer_firstname' => $this->string(),
		    'customer_street' => $this->string(),
		    'customer_house_number' => $this->string(),
		    'customer_zip' => $this->integer(),
		    'customer_city' => $this->string(),
		    'customer_phone_number' => $this->string(),
		    'customer_email' => $this->string(),
		    'customer_language' => $this->string(),

		    'contract' => $this->string(),

		    'status' => $this->string()->defaultValue('ordered'),
		    'created_at' => $this->date(),
		    'updated_at' => $this->date(),
	    ]);

	    $this->addForeignKey('fk_'.self::TABLE.'_articles_insurance_article_id', self::TABLE, 'insurance_article_id', 'articles', 'id');
	    $this->addForeignKey('fk_'.self::TABLE.'_articles_article_id', self::TABLE, 'article_id', 'articles', 'id');
	    $this->addForeignKey('fk_'.self::TABLE.'_customer_customer_id', self::TABLE, 'customer_id', 'customer', 'id');
	    $this->addForeignKey('fk_'.self::TABLE.'_order_order_id', self::TABLE, 'order_id', 'order', 'id');
	    $this->addForeignKey('fk_'.self::TABLE.'_order_item_order_item_id', self::TABLE, 'order_item_id', 'order_item', 'id');

		return true;
    }

    public function down()
    {
    	$this->dropTable(self::TABLE);

        return true;
    }
}
