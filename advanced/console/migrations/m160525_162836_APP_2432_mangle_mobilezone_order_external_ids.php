<?php

use common\models\Sales;
use components\Locale;
use yii\db\Migration;

class m160525_162836_APP_2432_mangle_mobilezone_order_external_ids extends Migration
{
	public function up()
	{
		if (Locale::inSuisse() === false) {
			echo 'Nothing to do for non-CH locales.';
			return;
		}

		/** @var Sales[] $sales */
		$sales = Sales::find()->all();
		foreach ($sales as $sale) {
			$articleTariff = $sale->articleTariff;
			if (empty($articleTariff)) {
				echo 'Could not find article tariff for sale #' . $sale->id . PHP_EOL;
				continue;
			}
			$sale->article_tariff_external_id = $articleTariff->external_id;
			$sale->save(false, ['article_tariff_external_id']);
		}
	}

	public function down()
	{
		echo 'm160525_162836_APP_2432_mangle_mobilezone_order_external_ids cannot be reverted.' . PHP_EOL;

		return false;
	}
}
