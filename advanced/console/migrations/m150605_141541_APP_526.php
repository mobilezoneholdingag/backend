<?php

use yii\db\Schema;
use yii\db\Migration;

class m150605_141541_APP_526 extends Migration
{
	public function up()
	{
		$this->update(\common\models\ArticleDetailsValues::tableName(), ['detail_35' => 1], ['not in', 'detail_35', ['', '-']]);
		$this->update(\common\models\ArticleDetailsValues::tableName(), ['detail_35' => 0], ['not', ['detail_35' => 1]]);
		$this->alterColumn(\common\models\ArticleDetailsValues::tableName(), 'detail_35', Schema::TYPE_BOOLEAN);

		$this->update(\common\models\ArticleDetails::tableName(), ['detail_name' => 'Frontkamera', 'group_id' => 3, 'sort_id' => 30, 'unit' => 'Megapixel'], ['detail_id' => 85]);
		$this->update(\common\models\ArticleDetails::tableName(), ['detail_name' => 'Auflösung Frontkamera', 'group_id' => 3, 'sort_id' => 40, 'unit' => 'Pixel'], ['detail_id' => 86]);

		return true;
	}

	public function down()
	{
		echo "m150605_141541_APP_526 cannot be reverted.\n";

		return false;
	}
}
