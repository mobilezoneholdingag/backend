<?php

use common\models\ArticleTariffs;
use yii\db\Migration;

class m160722_112535_MZ_54_restructured_article_tariffs extends Migration
{
	public function up()
	{
		$this->dropPrimaryKey('PRIMARY', ArticleTariffs::tableName());
		$this->addColumn(ArticleTariffs::tableName(), 'id', $this->primaryKey()->first());

		$this->addColumn(ArticleTariffs::tableName(), 'api_updated_at', $this->dateTime());
		$this->addColumn(ArticleTariffs::tableName(), 'valid_from', $this->date()->after('is_active'));
		$this->addColumn(ArticleTariffs::tableName(), 'valid_until', $this->date()->after('valid_from'));

		$this->createIndex(
			'articletariffsindex',
			ArticleTariffs::tableName(),
			['article_id', 'tariff_id', 'fulfillment_partner_id', 'valid_from'],
			true
		);
	}

	public function down()
	{
		$this->dropColumn(ArticleTariffs::tableName(), 'id');

		$this->dropIndex('articletariffsindex', ArticleTariffs::tableName());

		$this->addPrimaryKey(
			'articletariffsindex',
			ArticleTariffs::tableName(),
			['article_id', 'tariff_id', 'fulfillment_partner_id']
		);

		$this->dropColumn(ArticleTariffs::tableName(), 'api_updated_at');
		$this->dropColumn(ArticleTariffs::tableName(), 'valid_from');
		$this->dropColumn(ArticleTariffs::tableName(), 'valid_until');
	}
}
