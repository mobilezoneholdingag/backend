<?php

use yii\db\Schema;
use yii\db\Migration;

class m151013_140223_APP_916 extends Migration
{
	public function up()
	{
		$this->alterColumn(\common\models\Article::tableName(), 'insurance_price', Schema::TYPE_DECIMAL.'(6,2) DEFAULT NULL') ;

		return true;
	}

	public function down()
	{
		$this->alterColumn(\common\models\Article::tableName(), 'insurance_price', Schema::TYPE_FLOAT. 'DEFAULT NULL');

		return true;
	}
}
