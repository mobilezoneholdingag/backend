<?php

use yii\db\Migration;

class m170609_145933_MZ_1031_add_description_columns_to_article extends Migration
{
	const ARTICLE_TABLE = 'articles';
	const COLUMN_1 = 'description_1';
	const COLUMN_2 = 'description_2';

	public function up()
	{
		$this->addColumn(self::ARTICLE_TABLE, self::COLUMN_1, $this->text());
		$this->addColumn(self::ARTICLE_TABLE, self::COLUMN_2, $this->text());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::ARTICLE_TABLE, self::COLUMN_1);
		$this->dropColumn(self::ARTICLE_TABLE, self::COLUMN_2);

		return true;
	}
}
