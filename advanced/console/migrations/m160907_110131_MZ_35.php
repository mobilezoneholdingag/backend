<?php

use yii\db\Migration;

class m160907_110131_MZ_35 extends Migration
{
	public $tableName = 'marketing_images';

    public function up()
    {
        $this->createTable($this->tableName, [
	        'id' => $this->primaryKey(),
	        'filename' => $this->string(255)->unique()->notNull(),
	        'target_url' => $this->string(255),
	        'language' => $this->string(255),
	        'tag' => $this->string(64)->unique()->notNull(),
	        'updated_at' => $this->timestamp(),
	        'created_at' => $this->timestamp(),
        ]);

	    return true;
    }

    public function down()
    {
        $this->dropTable($this->tableName);

	    return true;
    }
}
