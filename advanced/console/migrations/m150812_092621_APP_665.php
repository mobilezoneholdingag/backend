<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_092621_APP_665 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Seo::tableName(), 'updated_at', Schema::TYPE_DATETIME);
		$this->addColumn(\common\models\LandingPages::tableName(), 'updated_at', Schema::TYPE_DATETIME);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Seo::tableName(), 'updated_at');
		$this->dropColumn(\common\models\LandingPages::tableName(), 'updated_at');

		return true;
	}
}
