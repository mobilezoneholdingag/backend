<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_153208_APP_219 extends Migration
{
	public function up()
	{
		$this->dropColumn(\common\models\Article::tableName(), 'article_id_int');
	}

	public function down()
	{
		echo "m150331_153208_APP_219 cannot be reverted.\n";

		return false;
	}
}
