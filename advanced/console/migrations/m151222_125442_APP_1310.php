<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_125442_APP_1310 extends Migration
{
	public function up()
	{
		$this->update(\frontend\models\Newsletter::tableName(), ['confirmed' => false], ['confirmed' => null]);

		return true;
	}

	public function down()
	{
		echo "m151222_125442_APP_1310 cannot be reverted.\n";

		return false;
	}
}
