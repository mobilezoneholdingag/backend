<?php

use common\models\Article;
use yii\db\Migration;

class m170322_102742_MZ_921_article_indexes extends Migration
{
    public function up()
    {
	    $this->createIndex(
	    	'index_for_article_search',
		    Article::tableName(),
		    [
		    	'mat_no',
			    'manufacturer_article_id'
		    ]
	    );

	    return true;
    }

    public function down()
    {
	    $this->dropIndex('index_for_article_search', Article::tableName());

	    return true;
    }
}
