<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ArticleDetailsValues;

class m150611_144400_APP_470f01 extends Migration
{
	public function up()
	{
		$this->alterColumn(ArticleDetailsValues::tableName(), 'detail_87', 'DECIMAL(3,1) NOT NULL DEFAULT 0.00');
	}

	public function down()
	{
		$this->alterColumn(ArticleDetailsValues::tableName(), 'detail_87', "VARCHAR(80) NOT NULL DEFAULT ''");

		return false;
	}
}
