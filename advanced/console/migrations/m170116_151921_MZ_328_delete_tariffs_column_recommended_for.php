<?php

use yii\db\Migration;

class m170116_151921_MZ_328_delete_tariffs_column_recommended_for extends Migration
{
	public function up()
	{
		$this->dropColumn('tariffs', 'recommended_for');
	}

	public function down()
	{
		echo "m170116_151921_MZ_328_delete_tariffs_column_recommended_for cannot be reverted.\n";

		return false;
	}
}
