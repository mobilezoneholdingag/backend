<?php

use yii\db\Schema;
use yii\db\Migration;

class m150330_081952_APP_214_2 extends Migration
{
	public function up()
	{
		$this->alterColumn(\common\models\Seo::tableName(), 'html_content', 'text');

		return true;
	}

	public function down()
	{
		echo "m150330_081952_APP_214_2 cannot be reverted.\n";

		return false;
	}
}
