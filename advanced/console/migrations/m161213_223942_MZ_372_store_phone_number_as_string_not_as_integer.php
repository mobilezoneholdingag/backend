<?php

use yii\db\Migration;

class m161213_223942_MZ_372_store_phone_number_as_string_not_as_integer extends Migration
{
	public function up()
	{
		$this->alterColumn('customer_address', 'contact_phone_number', $this->string());

		return true;
	}

	public function down()
	{
		$this->alterColumn('customer_address', 'contact_phone_number', $this->integer(25));

		return true;
	}
}
