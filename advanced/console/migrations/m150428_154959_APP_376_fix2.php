<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;
use common\models\Article;
use backend\controllers\SalesController;

class m150428_154959_APP_376_fix2 extends Migration
{
	public function up()
	{
		$sales = Sales::find()->all();
		foreach ($sales as $sale) {
			$sale->marge = SalesController::calculateMarge($sale);
			$sale->save();
		}
	}

	public function down()
	{
		echo "m150428_154959_APP_376_fix2 cannot be reverted.\n";

		return false;
	}
}
