<?php

use yii\db\Migration;

class m170215_132600_MZ_698_add_customer_phone_number_to_order_table extends Migration
{
	const ORDER_TABLE = 'order';
	const NEW_COLUMN = 'customer_phone_number';

	public function up()
	{
		$this->addColumn(self::ORDER_TABLE, self::NEW_COLUMN, $this->text());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::ORDER_TABLE, self::NEW_COLUMN);

		return true;
	}
}
