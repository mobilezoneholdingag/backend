<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_113832_APP_827_2 extends Migration
{
	public function up()
	{
		$internetFlatIds = [];
		$this->addColumn(\common\models\Provider::tableName(), 'speed_automatic_title', Schema::TYPE_STRING);

		$tariffHighlightDetails = \common\models\TariffHighlightDetails::find()->select('id')->where(['css_class' => 'internetflat'])->all();

		foreach ($tariffHighlightDetails as $model) {
			$internetFlatIds[] = $model->id;
		}


		$this->delete(\common\models\TariffHighlights::tableName(), ['in', 'detail_id', $internetFlatIds]);
		$this->delete(\common\models\TariffHighlightDetails::tableName(), ['css_class' => 'internetflat']);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Provider::tableName(), 'speed_automatic_title');

		return true;
	}
}
