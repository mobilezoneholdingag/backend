<?php

use yii\db\Migration;

class m161215_104032_MZ_417_remove_unused_attributes_from_newsletter_subscriber extends Migration
{
	const TABLE_NAME = 'newsletter_subscriber';

	public function up()
	{
		$this->dropColumn(self::TABLE_NAME, 'standard');
		$this->dropColumn(self::TABLE_NAME, 'press');
		$this->dropColumn(self::TABLE_NAME, 'investor_relations');

		return true;
	}

	public function down()
	{
		$this->addColumn(self::TABLE_NAME, 'standard', $this->integer());
		$this->addColumn(self::TABLE_NAME, 'press', $this->integer());
		$this->addColumn(self::TABLE_NAME, 'investor_relations', $this->integer());

		return true;
	}
}
