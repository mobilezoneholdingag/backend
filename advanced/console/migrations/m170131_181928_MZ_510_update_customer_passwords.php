<?php

use common\models\Customer;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use yii\db\Migration;

class m170131_181928_MZ_510_update_customer_passwords extends Migration
{
	public function up()
	{
		$customerQuery = Customer::find()->where(['IS NOT', 'password_hash', NULL]);
		/** @var Customer[] $customers */
		$customers = $customerQuery->all();
		$count = $customerQuery->count();
		echo "Processing $count customer passwords.";
		$progressBar = new ProgressBar(new ConsoleOutput(), $count);
		foreach ($customers as $customer) {
			$customer->setPasswordHashByPassword($customer->password_hash);
			$customer->save();
			$progressBar->advance();
		}
		$progressBar->finish();

		return true;
	}

	public function down()
	{
		return false;
	}
}
