<?php

use common\models\SeoAvailableCanonicalUrl;
use yii\db\Migration;

/**
 * Class m160521_144704_APP_2321_create_new_table_for_canonical_urls
 */

/** @noinspection PhpIllegalPsrClassPathInspection */
class m160521_144704_APP_2321_create_new_table_for_canonical_urls extends Migration
{
	/**
	 * Create new seo urls table.
	 * @return bool
	 */
	public function up()
	{
		$this->createTable(SeoAvailableCanonicalUrl::tableName(), [
			'id' => $this->primaryKey(),
			'url' => $this->string()->notNull()->unique(),
			'available' => $this->boolean()->notNull(),
		]);

		return true;
	}

	/**
	 * Remove seo urls table.
	 * @return bool
	 */
	public function down()
	{
		$this->dropTable(SeoAvailableCanonicalUrl::tableName());

		return true;
	}
}
