<?php

use common\modules\mfgroup\models\FinancialResponse;
use yii\db\Migration;

class m160922_151905_MZ_233_mfgroup_card_number_has_to_be_longer extends Migration
{
	public function up()
	{
		$this->alterColumn(FinancialResponse::tableName(), 'card_number', $this->string(1024));

		return true;
	}

	public function down()
	{
		$this->alterColumn(FinancialResponse::tableName(), 'card_number', $this->string());

		return true;
	}
}
