<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_132628_APP_1097 extends Migration
{
	public function up()
	{
		$this->createTable('verivox_tariff_matching', [
			'id' => Schema::TYPE_PK,
			'verivox_tariff_name' => Schema::TYPE_STRING,
			'eam_tariff_id' => Schema::TYPE_INTEGER,
		]);

		$this->createTable('verivox_article_matching', [
			'id' => Schema::TYPE_PK,
			'verivox_article_name' => Schema::TYPE_STRING,
			'eam_article_id' => Schema::TYPE_INTEGER,
		]);
	}

	public function down()
	{
		$this->dropTable('verivox_tariff_matching');
		$this->dropTable('verivox_article_matching');
	}
}
