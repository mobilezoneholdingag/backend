<?php

use yii\db\Schema;
use yii\db\Migration;

class m150416_173316_APP_225 extends Migration
{
	public function up()
	{
		$this->dropColumn(\common\models\Article::tableName(), 'price_buy');

		return true;
	}

	public function down()
	{
		$this->addColumn(\common\models\Article::tableName(), 'price_buy', Schema::TYPE_DECIMAL);

		return true;
	}
}
