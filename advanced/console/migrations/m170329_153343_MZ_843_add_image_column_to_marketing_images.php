<?php

use deinhandy\picasso\Client;
use deinhandy\picasso\File;
use yii\db\Migration;

class m170329_153343_MZ_843_add_image_column_to_marketing_images extends Migration
{
	const CDN = 'https://f390f95260ca9ee462f7-2cb64ab23bfb1ae42dfbb76e032b194c.ssl.cf3.rackcdn.com/';
	const PICASSO_COLLECTION_NAME = 'marketing-images';
	const MARKETING_IMAGES_TABLE = 'marketing_images';

	/** @var Client */
	private $client;

	public function __construct(array $config = [])
	{
		parent::__construct($config);
		$this->client = di(Client::class);
	}

	public function up()
	{
		$this->addColumn(self::MARKETING_IMAGES_TABLE, 'image', $this->string()->after('filename'));

		$targetUrls = $this->getTargetUrls();

		$this->uploadImagesToPicasso($targetUrls);

		$this->dropColumn(self::MARKETING_IMAGES_TABLE, 'filename');
	}

	public function down()
	{
		$this->addColumn(self::MARKETING_IMAGES_TABLE, 'filename', $this->string()->after('image'));

		$this->dropColumn(self::MARKETING_IMAGES_TABLE, 'image');
	}

	private function uploadImagesToPicasso($targetUrls)
	{
		foreach ($targetUrls as $key => $targetUrl) {
			$uploaded = false;
			$imid = '';

			while (!$uploaded) {
				$imageFile = $this->getImageFile($targetUrl);

				if (!$imageFile) {
					unset($targetUrls[$key]);
					continue 2;
				}

				try {
					$imid = $this->client->upload(
						$imageFile,
						Yii::$app->params['picasso_namespace'],
						self::PICASSO_COLLECTION_NAME
					);
					$uploaded = true;
				} catch (Exception $e) {
					sleep(30);
				}
			}

			$targetUrls[$key] = $imid;
		}

		foreach ($targetUrls as $id => $targetUrl) {
			$this->update(self::MARKETING_IMAGES_TABLE, ['image' => $targetUrl], ['id' => $id]);
		}
	}

	private function getTargetUrls(): array
	{
		$connection = Yii::$app->getDb();

		$targetUrls = $connection->createCommand('
	        SELECT id, filename FROM marketing_images
	    ')->queryAll(\PDO::FETCH_KEY_PAIR);

		$targetUrls = array_map(
			function($filename) {
				return self::CDN . $filename;
			},
			$targetUrls
		);

		return $targetUrls;
	}

	private function getImageFile(string $imageUrl)
	{
		$fileNameParts = explode('/', $imageUrl);
		$fileName = end($fileNameParts);

		try {
			$imageResource = fopen($imageUrl, 'r');
		} catch (Exception $e) {
			echo PHP_EOL . "Missing Image: $imageUrl" . PHP_EOL;
			return null;
		}

		return new File($fileName, $imageResource);
	}
}
