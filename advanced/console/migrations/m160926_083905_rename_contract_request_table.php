<?php

use yii\db\Migration;

class m160926_083905_rename_contract_request_table extends Migration
{
	const TABLE_OLD = 'contract_requests';
	const TABLE = 'renewal_requests';

    public function up()
    {
		$this->renameTable(self::TABLE_OLD, self::TABLE);

	    $this->addColumn(self::TABLE, 'status', $this->string()->notNull()->defaultValue('open'));
	    $this->addColumn(self::TABLE, 'offer_id', $this->integer());
	    $this->addColumn(self::TABLE, 'customer_id', $this->integer());
	    $this->addColumn(self::TABLE, 'created_at', $this->dateTime());
	    $this->addColumn(self::TABLE, 'updated_at', $this->dateTime());

	    $this->addForeignKey('fk_article_tariffs_offer_id', self::TABLE, 'offer_id', 'article_tariffs', 'id');
	    $this->addForeignKey('fk_customer_customer_id', self::TABLE, 'customer_id', 'customer', 'id');

	    return true;
    }

    public function down()
    {
	    $this->dropColumn(self::TABLE, 'status');
	    $this->dropColumn(self::TABLE, 'offer_id');
	    $this->dropColumn(self::TABLE, 'customer_id');
	    $this->dropColumn(self::TABLE, 'created_at');
	    $this->dropColumn(self::TABLE, 'updated_at');

	    $this->dropForeignKey('fk_article_tariffs_offer_id', self::TABLE);
	    $this->dropForeignKey('fk_customer_customer_id', self::TABLE);

		$this->renameTable(self::TABLE, self::TABLE_OLD);

        return true;
    }
}
