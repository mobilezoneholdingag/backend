<?php

use yii\db\Migration;

/**
 * Handles adding sim_card_option to table `offer_type`.
 */
class m161031_154607_add_sim_card_option_to_offer_type extends Migration
{
	const TABLE = 'offer_type';

    /**
     * @inheritdoc
     */
    public function up()
    {
	    $this->addColumn(self::TABLE, 'simcard_needed', $this->boolean()->defaultValue(false));

	    return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    $this->dropColumn(self::TABLE, 'simcard_needed');

	    return true;
    }
}
