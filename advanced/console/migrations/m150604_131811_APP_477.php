<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Glossary;

class m150604_131811_APP_477 extends Migration
{
	public function up()
	{
		$this->addColumn(Glossary::tableName(), 'url_name', 'string(72)');
	}

	public function down()
	{
		$this->dropColumn(Glossary::tableName(), 'url_name');
	}
}
