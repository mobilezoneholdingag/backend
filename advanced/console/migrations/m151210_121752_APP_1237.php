<?php

use yii\db\Schema;
use yii\db\Migration;

class m151210_121752_APP_1237 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Sales::tableName(), 'submit_errors', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Sales::tableName(), 'submit_errors');

		return true;
	}
}
