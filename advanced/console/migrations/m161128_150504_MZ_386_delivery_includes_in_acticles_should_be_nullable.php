<?php

use yii\db\Migration;

class m161128_150504_MZ_386_delivery_includes_in_acticles_should_be_nullable extends Migration
{
	public function up()
	{
		$this->alterColumn('articles', 'delivery_includes', $this->text());

		return true;
	}

	public function down()
	{
		$this->alterColumn('articles', 'delivery_includes', $this->text()->notNull());

		return true;
	}
}
