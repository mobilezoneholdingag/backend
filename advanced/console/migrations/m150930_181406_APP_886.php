<?php

use yii\db\Schema;
use yii\db\Migration;

class m150930_181406_APP_886 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Article::tableName(), 'insurance_price', Schema::TYPE_FLOAT);
	}

	public function down()
	{
		$this->dropColumn(\common\models\Article::tableName(), 'insurance_price');
	}
}
