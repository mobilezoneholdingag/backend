<?php

use yii\db\Schema;
use yii\db\Migration;
use backend\models\SalesUnapproved;
use common\models\Sales;
use common\models\User;

class m160201_124534_APP_1481_001 extends Migration
{
	public function up()
	{
		$this->addColumn(SalesUnapproved::tableName(), 'approved_sale_id', 'INT(11)');
		$this->addColumn(SalesUnapproved::tableName(), 'approving_user_id', 'INT(11)');

		$this->addForeignKey('approved_sale', SalesUnapproved::tableName(), 'approved_sale_id', Sales::tableName(), 'id');
		$this->addForeignKey('approving_user', SalesUnapproved::tableName(), 'approving_user_id', User::tableName(), 'id');
	}

	public function down()
	{
		$this->dropForeignKey('approved_sale', SalesUnapproved::tableName());
		$this->dropForeignKey('approving_user', SalesUnapproved::tableName());

		$this->dropColumn(SalesUnapproved::tableName(), 'approved_sale_id');
		$this->dropColumn(SalesUnapproved::tableName(), 'approving_user_id');
	}
}
