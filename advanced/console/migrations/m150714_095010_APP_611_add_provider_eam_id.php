<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Provider;

class m150714_095010_APP_611_add_provider_eam_id extends Migration
{
	public function up()
	{
		// FYI we don't have a provider with id 7
		$providerIds = [
			['our_provider_id' => 1, 'eam_provider_id' => 11],
			['our_provider_id' => 2, 'eam_provider_id' => 138],
			['our_provider_id' => 3, 'eam_provider_id' => 106],
			['our_provider_id' => 4, 'eam_provider_id' => 164],
			['our_provider_id' => 5, 'eam_provider_id' => 92],
			['our_provider_id' => 6, 'eam_provider_id' => 102],
			['our_provider_id' => 8, 'eam_provider_id' => 23],
			['our_provider_id' => 9, 'eam_provider_id' => 35],
			['our_provider_id' => 10, 'eam_provider_id' => 1],
			['our_provider_id' => 11, 'eam_provider_id' => 128],
		];

		foreach ($providerIds as $providerId) {
			$this->update(Provider::tableName(), ['eam_id' => $providerId['eam_provider_id']], ['id' => $providerId['our_provider_id']]);
		}

		return true;
	}

	public function down()
	{
		// FYI we don't have a provider with id 7
		$providerIds = [
			['our_provider_id' => 1, 'eam_provider_id' => null],
			['our_provider_id' => 2, 'eam_provider_id' => null],
			['our_provider_id' => 3, 'eam_provider_id' => null],
			['our_provider_id' => 4, 'eam_provider_id' => null],
			['our_provider_id' => 5, 'eam_provider_id' => null],
			['our_provider_id' => 6, 'eam_provider_id' => null],
			['our_provider_id' => 8, 'eam_provider_id' => null],
			['our_provider_id' => 9, 'eam_provider_id' => null],
			['our_provider_id' => 10, 'eam_provider_id' => null],
			['our_provider_id' => 11, 'eam_provider_id' => null],
		];

		foreach ($providerIds as $providerId) {
			$this->update(Provider::tableName(), ['eam_id' => null], ['id' => $providerId['our_provider_id']]);
		}

		return true;
	}
}
