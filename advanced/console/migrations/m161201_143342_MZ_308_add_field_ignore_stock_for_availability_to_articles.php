<?php

use yii\db\Migration;

class m161201_143342_MZ_308_add_field_ignore_stock_for_availability_to_articles extends Migration
{
	const TABLE_ARTICLES = 'articles';
	const NEW_FIELD = 'override_positive_stock_to_reorder';

	public function up()
	{
		$this->addColumn(self::TABLE_ARTICLES, self::NEW_FIELD, $this->boolean()->defaultValue(false)->notNull());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_ARTICLES, self::NEW_FIELD);

		return true;
	}
}
