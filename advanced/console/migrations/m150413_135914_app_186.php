<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_135914_app_186 extends Migration
{
	public function up()
	{
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'check24_id', Schema::TYPE_STRING);
		$this->addColumn(\common\models\Sales::tableName(), 'check24_id', Schema::TYPE_STRING);

		$this->execute('ALTER TABLE sales_unapproved CHANGE passport_valid_until passport_valid_until DATE DEFAULT NULL');
		$this->execute('ALTER TABLE sales CHANGE passport_valid_until passport_valid_until DATE DEFAULT NULL');
	}

	public function down()
	{
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'check24_id');
		$this->dropColumn(\common\models\Sales::tableName(), 'check24_id');

		$this->execute('ALTER TABLE sales_unapproved CHANGE passport_valid_until passport_valid_until DATE NOT NULL');
		$this->execute('ALTER TABLE sales CHANGE passport_valid_until passport_valid_until DATE NOT NULL');
	}
}
