<?php

use yii\db\Migration;

class m170822_140851_MZ_1219_add_column_via_newsletter_to_customer_table extends Migration
{
	const CUSTOMER_TABLE = 'customer';
	const VIA_NEWSLETTER_COLUMN = 'via_newsletter';

	public function up()
	{
		$this->addColumn(self::CUSTOMER_TABLE, self::VIA_NEWSLETTER_COLUMN, $this->boolean()->defaultValue(false));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::CUSTOMER_TABLE, self::VIA_NEWSLETTER_COLUMN);

		return true;
	}
}
