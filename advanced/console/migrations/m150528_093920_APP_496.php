<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;

class m150528_093920_APP_496 extends Migration
{
	public function up()
	{
		// 148411 is the correct id, my last commit comment was wrong :)
		$sale = Sales::find()->where(['order_id_eam' => 148411])->one();
		if ($sale) {
			$sale->partner_id = '76';
			$sale->bid = null;
			$sale->verivox_id = 195262;
			$sale->verivox_transaction_id = '68sr1p';

			$sale->save(false);
		}
	}

	public function down()
	{
		echo "m150528_093920_APP_496 cannot be reverted.\n";

		return false;
	}
}
