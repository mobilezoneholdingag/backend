<?php

use yii\db\Schema;
use yii\db\Migration;

class m150327_145142_APP_193 extends Migration
{
	public function up()
	{
		foreach (\common\models\LandingPages::find()->all() as $model) {
			$model->html_code = html_entity_decode($model->html_code);
			if (substr($model->url, -1) == '/') {
				$model->url = substr($model->url, 0, -1);
			}
			$model->save();
		}

		return true;
	}

	public function down()
	{
		echo "m150327_145142_APP_193 cannot be reverted.\n";

		return false;
	}
}
