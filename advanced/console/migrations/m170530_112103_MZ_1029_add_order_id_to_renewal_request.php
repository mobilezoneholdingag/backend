<?php

use yii\db\Migration;

class m170530_112103_MZ_1029_add_order_id_to_renewal_request extends Migration
{
	const TABLE = 'renewal_requests';
	const COLUMN = 'order_id';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->integer()->unsigned());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}
}
