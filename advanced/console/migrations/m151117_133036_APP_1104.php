<?php

use yii\db\Schema;
use yii\db\Migration;

class m151117_133036_APP_1104 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Tariffs::tableName(), 'price_sms', Schema::TYPE_DECIMAL . '(7,2)');
		$this->addColumn(\common\models\ArticleTariffs::tableName(), 'fix_profit', Schema::TYPE_DECIMAL . '(7,2)');
		$this->addColumn(\common\models\ArticleTariffs::tableName(), 'footnote_text', Schema::TYPE_TEXT);

		$this->dropColumn(\common\models\Tariffs::tableName(), 'addition_id');

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Tariffs::tableName(), 'price_sms');
		$this->dropColumn(\common\models\ArticleTariffs::tableName(), 'fix_profit');
		$this->dropColumn(\common\models\ArticleTariffs::tableName(), 'footnote_text');

		$this->addColumn(\common\models\Tariffs::tableName(), 'addition_id', Schema::TYPE_INTEGER);

		return true;
	}
}
