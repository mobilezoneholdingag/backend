<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Manufacturer;

class m150713_143048_APP_599 extends Migration
{
	public function up()
	{
		$this->addColumn(Manufacturer::tableName(), 'smartphone_title', Schema::TYPE_STRING);
		$this->addColumn(Manufacturer::tableName(), 'smartphone_subtext', Schema::TYPE_STRING);
		$this->addColumn(Manufacturer::tableName(), 'tablet_title', Schema::TYPE_STRING);
		$this->addColumn(Manufacturer::tableName(), 'tablet_subtext', Schema::TYPE_STRING);
		
		return true;
	}

	public function down()
	{
		$this->dropColumn(Manufacturer::tableName(), 'smartphone_title');
		$this->dropColumn(Manufacturer::tableName(), 'smartphone_subtext');
		$this->dropColumn(Manufacturer::tableName(), 'tablet_title');
		$this->dropColumn(Manufacturer::tableName(), 'tablet_subtext');
		
		return true;
	}
}
