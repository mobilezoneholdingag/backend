<?php

use yii\db\Migration;

class m170602_092425_MZ_1041_add_price_discounted_column_to_article_table extends Migration
{
	const TABLE = 'articles';
	const COLUMN = 'price_discounted';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->decimal(6, 2));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}
}
