<?php

use yii\db\Migration;

class m170214_183548_MZ_734_make_tag_not_unique_for_marketing_images extends Migration
{
	const TABLE = 'marketing_images';

	public function up()
	{
		$this->dropIndex('tag', self::TABLE);
		$this->createIndex(
			'unique_tag_language',
			self::TABLE,
			['tag', 'language'],
			true
		);

		return true;
	}

	public function down()
	{
		$this->dropIndex('unique_tag_language', self::TABLE);
		$this->createIndex('tag', self::TABLE, 'tag', true);

		return true;
	}
}
