<?php

use yii\db\Migration;

class m161205_155146_MZ_354_add_field_additional_information_to_articles extends Migration
{
	const TABLE_ARTICLES = 'articles';
	const NEW_FIELD = 'additional_information';

	public function up()
	{
		$this->addColumn(self::TABLE_ARTICLES, self::NEW_FIELD, $this->text());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_ARTICLES, self::NEW_FIELD);

		return false;
	}
}
