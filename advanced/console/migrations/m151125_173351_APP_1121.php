<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_173351_APP_1121 extends Migration
{
	public function up()
	{
		$this->createTable('coupons_meinetanne', [
			'id' => Schema::TYPE_PK,
			'code' => Schema::TYPE_STRING,
			'sale_id' => Schema::TYPE_INTEGER,
		]);

		$this->createIndex('code', 'coupons_meinetanne', 'code', true);
		$this->createIndex('sale_id', 'coupons_meinetanne', 'sale_id', true);
	}

	public function down()
	{
		$this->dropTable('coupons_meinetanne');
	}
}
