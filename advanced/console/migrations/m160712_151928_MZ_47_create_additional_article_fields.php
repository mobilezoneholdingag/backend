<?php

use common\models\Article;
use common\models\ArticleGroups;
use yii\db\Migration;

class m160712_151928_MZ_47_create_additional_article_fields extends Migration
{
	public function up()
	{
		$this->addColumn(Article::tableName(), 'mat_no', $this->string()->after('product_name'));
		$this->addColumn(Article::tableName(), 'barcode', $this->string()->after('mat_no'));
		$this->addColumn(Article::tableName(), 'sunrisenetprice', $this->decimal(6, 2)->after('price_buy'));
		$this->addColumn(Article::tableName(), 'is_discountable', $this->boolean());
		$this->addColumn(Article::tableName(), 'vat_rate', $this->decimal(2, 1));
		$this->addColumn(Article::tableName(), 'discount_type', $this->string()->after('is_discountable'));
		$this->addColumn(Article::tableName(), 'is_nettozone_active', $this->boolean()->after('is_active'));
		$this->addColumn(Article::tableName(), 'manufacturer_id', $this->integer()->after('id'));
		$this->addColumn(Article::tableName(), 'product_category_id', $this->integer()->after('manufacturer_id'));
		$this->addColumn(Article::tableName(), 'api_updated_at', $this->dateTime()->after('updated_at'));

		$this->dropForeignKey('FK_articles_articleGroup', Article::tableName());
	}

	public function down()
	{
		$this->dropColumn(Article::tableName(), 'mat_no');
		$this->dropColumn(Article::tableName(), 'barcode');
		$this->dropColumn(Article::tableName(), 'sunrisenetprice');
		$this->dropColumn(Article::tableName(), 'is_discountable');
		$this->dropColumn(Article::tableName(), 'vat_rate');
		$this->dropColumn(Article::tableName(), 'discount_type');
		$this->dropColumn(Article::tableName(), 'is_nettozone_active');
		$this->dropColumn(Article::tableName(), 'manufacturer_id');
		$this->dropColumn(Article::tableName(), 'product_category_id');
		$this->dropColumn(Article::tableName(), 'api_updated_at');

		$this->addForeignKey(
			'FK_articles_articleGroup',
			Article::tableName(),
			'group_id',
			ArticleGroups::tableName(),
			'id',
			'CASCADE'
		);
	}
}
