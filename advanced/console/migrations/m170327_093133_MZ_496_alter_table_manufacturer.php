<?php

use yii\db\Migration;

class m170327_093133_MZ_496_alter_table_manufacturer extends Migration
{
	const TABLE_NAME = 'manufacturer';

    public function up()
    {
    	// cleanup
		$this->dropColumn(self::TABLE_NAME, 'logo_medium');
	    $this->dropColumn(self::TABLE_NAME, 'logo_big');
	    // add
	    $this->addColumn(self::TABLE_NAME, 'image', $this->string());
	    $this->addColumn(self::TABLE_NAME, 'insurance_image', $this->string());

	    return true;
    }

    public function down()
    {
    	// cleanup
	    $this->dropColumn(self::TABLE_NAME, 'image');
	    $this->dropColumn(self::TABLE_NAME, 'insurance_image');
	    // add
	    $this->addColumn(self::TABLE_NAME, 'logo_medium', $this->string());
	    $this->addColumn(self::TABLE_NAME, 'logo_big', $this->string());

	    return true;
    }
}
