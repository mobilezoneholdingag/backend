<?php

use yii\db\Schema;
use yii\db\Migration;

class m150324_154138_APP_181 extends Migration
{
	public function up()
	{
		$this->addColumn('sales', 'payment_link', 'string');

		return true;
	}

	public function down()
	{
		$this->dropColumn('sales', 'payment_link');

		return true;
	}
}
