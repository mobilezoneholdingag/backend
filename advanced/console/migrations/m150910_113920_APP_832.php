<?php

use yii\db\Schema;
use yii\db\Migration;

class m150910_113920_APP_832 extends Migration
{
	public function up()
	{
		$tableName = \common\models\Tariffs::tableName();
		$this->renameColumn($tableName, 'internet', 'internet_flat');
		$this->update($tableName, ['internet_flat' => 1], ['internet_flat' => 'inklusive']);
		$this->alterColumn($tableName, 'internet_flat', Schema::TYPE_BOOLEAN);
		$this->update($tableName, ['flatrate_mobile_networks' => 1], ['flatrate_mobile_networks' => 'inklusive']);
		$this->alterColumn($tableName, 'flatrate_mobile_networks', Schema::TYPE_BOOLEAN);
		$this->renameColumn($tableName, 'tarifdetail_3', 'anytime_minutes');
		$this->alterColumn($tableName, 'anytime_minutes', Schema::TYPE_INTEGER);
		$this->update($tableName, ['sms_flatrate_mobile_networks' => 1], ['sms_flatrate_mobile_networks' => 'inklusive']);
		$this->alterColumn($tableName, 'sms_flatrate_mobile_networks', Schema::TYPE_BOOLEAN);
		$this->renameColumn($tableName, 'tarifdetail_4', 'free_sms');
		$this->alterColumn($tableName, 'free_sms', Schema::TYPE_INTEGER);
		$this->update($tableName, ['lte' => 1], ['lte' => 'inklusive']);
		$this->alterColumn($tableName, 'lte', Schema::TYPE_BOOLEAN);

		return true;
	}

	public function down()
	{
		$tableName = \common\models\Tariffs::tableName();
		$this->renameColumn($tableName, 'internet_flat', 'internet');
		$this->alterColumn($tableName, 'internet', Schema::TYPE_STRING);
		$this->update($tableName, ['internet' => 'inklusive'], ['internet' => '1']);
		$this->alterColumn($tableName, 'flatrate_mobile_networks', Schema::TYPE_STRING);
		$this->update($tableName, ['flatrate_mobile_networks' => 'inklusive'], ['flatrate_mobile_networks' => '1']);
		$this->renameColumn($tableName, 'anytime_minutes', 'tarifdetail_3');
		$this->alterColumn($tableName, 'tarifdetail_3', Schema::TYPE_STRING);
		$this->alterColumn($tableName, 'sms_flatrate_mobile_networks', Schema::TYPE_STRING);
		$this->update($tableName, ['sms_flatrate_mobile_networks' => 'inklusive'], ['sms_flatrate_mobile_networks' => '1']);
		$this->renameColumn($tableName, 'free_sms', 'tarifdetail_4');
		$this->alterColumn($tableName, 'tarifdetail_4', Schema::TYPE_STRING);
		$this->alterColumn($tableName, 'lte', Schema::TYPE_STRING);
		$this->update($tableName, ['lte' => 'inklusive'], ['lte' => '1']);

		return true;
	}

}
