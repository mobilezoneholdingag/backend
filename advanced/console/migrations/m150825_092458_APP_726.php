<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_092458_APP_726 extends Migration
{
	public function up()
	{
		$this->createTable('sales_whitelist', [
			'id' => Schema::TYPE_PK,
			'email' => Schema::TYPE_STRING,
		]);

		$this->createTable('sales_blacklist', [
			'id' => Schema::TYPE_PK,
			'email' => Schema::TYPE_STRING,
		]);

		$this->createIndex('email', 'sales_whitelist', 'email', true);
		$this->createIndex('email', 'sales_blacklist', 'email', true);

		return true;
	}

	public function down()
	{
		$this->dropTable('sales_whitelist');
		$this->dropTable('sales_blacklist');

		return true;
	}
}
