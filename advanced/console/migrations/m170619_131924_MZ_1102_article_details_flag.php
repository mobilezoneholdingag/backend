<?php

use yii\db\Migration;

class m170619_131924_MZ_1102_article_details_flag extends Migration
{
	const ARTICLE_TABLE = 'articles';
	const COLUMN = 'show_technical_information';

	public function up()
	{
		$this->addColumn(self::ARTICLE_TABLE, self::COLUMN, $this->boolean()->defaultValue(true));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::ARTICLE_TABLE, self::COLUMN);

		return true;
	}
}
