<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_155858_APP_295 extends Migration
{
	public function up()
	{
		$this->alterColumn('sales', 'bank_ec_valid_until', Schema::TYPE_STRING);
		$this->alterColumn('sales_unapproved', 'bank_ec_valid_until', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		echo "m150331_155858_APP_295 cannot be reverted.\n";

		return false;
	}
}
