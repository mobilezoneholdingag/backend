<?php

use yii\db\Migration;

class m170106_105836_MZ_374_create_table_article_group_tariff extends Migration
{
	const ARTICLE_GROUPS_TARIFF_TABLE_NAME = 'article_groups_tariff';

	const ARTICLE_GROUPS_TABLE_NAME = 'article_groups';
	const ARTICLE_GROUPS_ID_COLUMN = 'article_group_id';

	const TARIFFS_TABLE_NAME = 'tariffs';
	const TARIFFS_ID_COLUMN = 'tariff_id';

	const PROVIDER_TABLE_NAME = 'provider';
	const PROVIDER_ID_COLUMN = 'provider_id';

	/**
	 * @return bool
	 */
	public function up()
	{
		$this->createTable(
			self::ARTICLE_GROUPS_TARIFF_TABLE_NAME,
			[
				'id' => $this->primaryKey(),
				self::ARTICLE_GROUPS_ID_COLUMN => $this->integer()->notNull(),
				self::TARIFFS_ID_COLUMN => $this->integer()->notNull(),
				self::PROVIDER_ID_COLUMN => $this->integer()->notNull(),
			]
		);

		$this->addForeignKey(
			'fk_article_groups_tariff_article_group_id',
			self::ARTICLE_GROUPS_TARIFF_TABLE_NAME,
			self::ARTICLE_GROUPS_ID_COLUMN,
			self::ARTICLE_GROUPS_TABLE_NAME,
			'id',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_article_groups_tariff_tariff_id',
			self::ARTICLE_GROUPS_TARIFF_TABLE_NAME,
			self::TARIFFS_ID_COLUMN,
			self::TARIFFS_TABLE_NAME,
			'id',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_article_groups_tariff_provider_id',
			self::ARTICLE_GROUPS_TARIFF_TABLE_NAME,
			self::PROVIDER_ID_COLUMN,
			self::PROVIDER_TABLE_NAME,
			'id',
			'CASCADE'
		);

		return true;
	}

	/**
	 * @return bool
	 */
	public function down()
	{
		$this->dropTable(self::ARTICLE_GROUPS_TARIFF_TABLE_NAME);

		return true;
	}
}
