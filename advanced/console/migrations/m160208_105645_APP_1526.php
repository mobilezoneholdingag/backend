<?php

use yii\db\Migration;
use common\models\ArticleDetailsValues;

class m160208_105645_APP_1526 extends Migration
{

	public function up()
	{
		$this->alterColumn(ArticleDetailsValues::tableName(), 'detail_87', "VARCHAR(80) NOT NULL DEFAULT ''");

		$articleDetailValues = ArticleDetailsValues::find()->all();

		foreach ($articleDetailValues as $value) {
			$value->detail_40 = str_replace('.', '', $value->detail_40);
			$value->save(false);
		}

		return true;
	}

	public function down()
	{
		$this->alterColumn(ArticleDetailsValues::tableName(), 'detail_87', 'DECIMAL(3,1) NOT NULL DEFAULT 0.00');

		return true;
	}


}
