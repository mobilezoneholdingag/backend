<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ArticleImage;
use common\models\Image;

class m150831_071946_APP_558_transformation_01 extends Migration
{
	public function safeUp()
	{
		#$this->dropTable(Image::tableName());
		$this->db->createCommand('DROP TABLE IF EXISTS '.Image::tableName())->execute();
		$this->createTable(Image::tableName(), [
			'id' => 'INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT',
			'filename' => 'VARCHAR(255) NOT NULL',
			'name' => 'VARCHAR(255) NOT NULL',
			'attributes' => 'INT(11) NOT NULL DEFAULT 0',
		]);

		$this->createIndex('UQ_filename', Image::tableName(), ['filename'], true);

		$this->renameTable('article_images', ArticleImage::tableName());

		$articleImageTable  = ArticleImage::tableName();
		$imageTable         = Image::tableName();

		$this->addColumn($articleImageTable, 'image_id',        'INT(11)');
		$this->addColumn($articleImageTable, 'size',            'INT(1)');
		$this->addColumn($articleImageTable, 'date_of_expiry',  'DATETIME DEFAULT NULL');

		$this->addForeignKey(
			'FK_image', $articleImageTable, ['image_id'],
			$imageTable, ['id'],
			'CASCADE', 'CASCADE'
		);

		$allArticleImages = ArticleImage::find()->all();

		/** @var ArticleImage $articleImage */
		foreach($allArticleImages as $articleImage)
		{
			$sizes = array_flip(ArticleImage::IMAGE_SIZE_DIC);

			foreach(['original', 'small', 'medium', 'big'] as $size)
			{
				if (!empty($articleImage->$size) &&
					!is_numeric($articleImage->$size)
				) {
					if (($foundImage = Image::find()->where(['name' => $articleImage->$size])->one()) == false)
					{
						$image = new Image();

						$image->name = $articleImage->$size;
						$image->filename = $articleImage->$size;
						$image->attributes = 0;

						$image->save(false);
					} else {
						$image = $foundImage;
					}

					$newArticleImage = new ArticleImage();
					$newArticleImage->original =
						$newArticleImage->medium =
							$newArticleImage->small =
								$newArticleImage->big = '';

					$newArticleImage->article_id = $articleImage->article_id;
					$newArticleImage->image_id = $image->id;
					$newArticleImage->size = $sizes[$size];
					$newArticleImage->sort = $articleImage->sort;
					$newArticleImage->alt_text = $articleImage->alt_text;
					$newArticleImage->tooltip = $articleImage->tooltip;

					$newArticleImage->detachBehaviors();
					$newArticleImage->save(false);

					$articleImage->detachBehaviors();
					$articleImage->delete();
				}


			}
		}

		$this->dropColumn(ArticleImage::tableName(), 'original');
		$this->dropColumn(ArticleImage::tableName(), 'small');
		$this->dropColumn(ArticleImage::tableName(), 'medium');
		$this->dropColumn(ArticleImage::tableName(), 'big');
	}

	public function down()
	{
		echo 'This migration cannot be reverted.';
		return false;
	}
}
