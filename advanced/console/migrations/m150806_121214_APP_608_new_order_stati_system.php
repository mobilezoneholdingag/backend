<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\OrderStatus;

class m150806_121214_APP_608_new_order_stati_system extends Migration
{
	public function up()
	{
		// needs to be dropped as duplicate error codes are now allowed
		$this->dropIndex('code', OrderStatus::tableName());

		// new column, mark all existing codes as belonging to EAM
		$this->addColumn(OrderStatus::tableName(), 'fulfillment_partner_id', Schema::TYPE_INTEGER);
		$this->update(OrderStatus::tableName(), ['fulfillment_partner_id' => 1]);

		// add new codes for Drillisch
		$this->insert(OrderStatus::tableName(), ['code' => 102, 'title' => 'In Bearbeitung', 'final' => 0, 'fulfillment_partner_id' => 2]);
		$this->insert(OrderStatus::tableName(), ['code' => 104, 'title' => 'Ablehnung', 'final' => 1, 'fulfillment_partner_id' => 2]);
		$this->insert(OrderStatus::tableName(), ['code' => 1017, 'title' => 'Aktiviert', 'final' => 1, 'fulfillment_partner_id' => 2]);
		$this->insert(OrderStatus::tableName(), ['code' => 1011, 'title' => 'Auftragsnummer nicht bekannt', 'final' => 1, 'fulfillment_partner_id' => 2]);
		$this->insert(OrderStatus::tableName(), ['code' => 1012, 'title' => 'Boniprüfung erfolgreich', 'final' => 0, 'fulfillment_partner_id' => 2]);
		$this->insert(OrderStatus::tableName(), ['code' => 1016, 'title' => 'Versand erfolgt, noch nicht aktiviert', 'final' => 0, 'fulfillment_partner_id' => 2]);

		return true;
	}

	public function down()
	{
		$this->delete(OrderStatus::tableName(), ['fulfillment_partner_id' => 2]);
		$this->dropColumn(OrderStatus::tableName(), 'fulfillment_partner_id');

		$this->createIndex('code', OrderStatus::tableName(), true);

		return true;
	}
}
