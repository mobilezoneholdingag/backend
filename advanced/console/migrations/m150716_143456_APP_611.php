<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Provider;

class m150716_143456_APP_611 extends Migration
{
	public function up()
	{
		$this->addColumn(Provider::tableName(), 'mnp_text', Schema::TYPE_TEXT);

		$textO2 = 'Nachdem Ihre neue O2 SIM-Karte aktiviert wurde können Sie die Rufnummernmitnahme online bei O2 beantragen, alles weitere übernimmt O2. Es gelten die Fristen zur Rufnummernmitnahme von O2.';
		$textBase = 'Mit Ihrer Bestellung erhalten Sie ein Antragsformular für die Rufnummernmitnahme, nachdem Sie dieses ausgefüllt und an Ihren aktuellen Anbieter geschickt haben erledigt BASE alles Weitere für Sie. Bitte beachten Sie die Fristen, die bzgl. der Rufnummernmitnahme bei BASE gelten.';

		// set "MNP is possible" for providers BASE, O2 (we dont have a text for Vodafone now)
		$this->update(Provider::tableName(), ['mnp_text' => $textBase], ['id' => 1]);
		$this->update(Provider::tableName(), ['mnp_text' => $textO2], ['id' => 6]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Provider::tableName(), 'mnp_text');

		return true;
	}
}
