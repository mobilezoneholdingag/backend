<?php

use yii\db\Migration;

class m170203_105640_MZ_483_add_valid_until_date_to_renewal_requests extends Migration
{
	const TABLENAME = 'renewal_requests';
	const TABLENAMEOFFERS = 'renewal_request_offers';
	const TABLENAMEARTICLES = 'articles';
	const TABLENAMEOFFERTYPE = 'offer_type';

	public function up()
	{
		$this->addColumn(self::TABLENAME, 'valid_until_date', $this->date());
		$this->addColumn(self::TABLENAMEOFFERS, 'hash', $this->string());

		$this->dropColumn(self::TABLENAME, 'hash');
		$this->dropForeignKey('fk_articles_article_id', self::TABLENAME);
		$this->dropForeignKey('fk_article_tariffs_offer_id', self::TABLENAME);
		$this->dropColumn(self::TABLENAME, 'article_id');
		$this->dropColumn(self::TABLENAME, 'offer_id');

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLENAME, 'valid_until_date');
		$this->dropColumn(self::TABLENAMEOFFERS, 'hash');

		$this->addColumn(self::TABLENAME, 'hash', $this->string());
		$this->addColumn(self::TABLENAME, 'article_id', $this->integer());
		$this->addColumn(self::TABLENAME, 'offer_id', $this->integer());
		$this->addForeignKey('fk_articles_article_id', self::TABLENAME, 'article_id', self::TABLENAMEARTICLES, 'id');
		$this->addForeignKey('fk_article_tariffs_offer_id', self::TABLENAME, 'offer_id', self::TABLENAMEOFFERTYPE, 'id');

		return true;
	}
}
