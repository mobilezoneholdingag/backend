<?php

use yii\db\Migration;

/**
 * Class m161122_153714_MZ_269_create_table_article_group_sim
 */
class m161122_153714_MZ_269_create_table_article_group_sim extends Migration
{
	const TABLENAME = 'article_groups_sim';

	/**
	 * @return bool
	 */
	public function up()
	{
		$this->createTable(
			self::TABLENAME,
			[
				'id' => $this->primaryKey(11),
				'article_group_id' => $this->integer(11)->notNull(),
				'simcard_id' => $this->integer(11)->notNull(),
				'provider_id' => $this->integer(11)->notNull(),
			]
		);

		return true;
	}

	/**
	 * @return bool
	 */
	public function down()
	{
		$this->dropTable(self::TABLENAME);

		return true;
	}
}
