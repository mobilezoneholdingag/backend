<?php

use yii\db\Schema;
use yii\db\Migration;

class m151012_082958_APP_925 extends Migration
{
	public function up()
	{
		$table = \common\models\Tariffs::tableName();

		$this->dropColumn($table, 'tarifdetail_2');
		$this->dropColumn($table, 'tarifdetail_5');
		$this->dropColumn($table, 'tarifdetail_6');

		foreach (range(8, 11) as $number) {
			$this->dropColumn($table, 'tarifdetail_' . $number);
		}

		foreach (range(13, 16) as $number) {
			$this->dropColumn($table, 'tarifdetail_' . $number);
		}

		foreach (range(18, 28) as $number) {
			$this->dropColumn($table, 'tarifdetail_' . $number);
		}

		foreach (range(30, 37) as $number) {
			$this->dropColumn($table, 'tarifdetail_' . $number);
		}

		foreach (range(39, 43) as $number) {
			$this->dropColumn($table, 'tarifdetail_' . $number);
		}

		$this->dropColumn($table, 'tarifdetail_45');

		foreach (range(48, 85) as $number) {
			$this->dropColumn($table, 'tarifdetail_' . $number);
		}

		foreach (range(87, 97) as $number) {
			$this->dropColumn($table, 'tarifdetail_' . $number);
		}

		return true;
	}

	public function down()
	{
		$table = \common\models\Tariffs::tableName();

		$this->addColumn($table, 'tarifdetail_2', Schema::TYPE_STRING);
		$this->addColumn($table, 'tarifdetail_5', Schema::TYPE_STRING);
		$this->addColumn($table, 'tarifdetail_6', Schema::TYPE_STRING);

		foreach (range(8, 11) as $number) {
			$this->addColumn($table, 'tarifdetail_' . $number, Schema::TYPE_STRING);
		}

		foreach (range(13, 16) as $number) {
			$this->addColumn($table, 'tarifdetail_' . $number, Schema::TYPE_STRING);
		}

		foreach (range(18, 28) as $number) {
			$this->addColumn($table, 'tarifdetail_' . $number, Schema::TYPE_STRING);
		}

		foreach (range(30, 37) as $number) {
			$this->addColumn($table, 'tarifdetail_' . $number, Schema::TYPE_STRING);
		}

		foreach (range(39, 43) as $number) {
			$this->addColumn($table, 'tarifdetail_' . $number, Schema::TYPE_STRING);
		}

		$this->addColumn($table, 'tarifdetail_45', Schema::TYPE_STRING);

		foreach (range(48, 85) as $number) {
			$this->addColumn($table, 'tarifdetail_' . $number, Schema::TYPE_STRING);
		}

		foreach (range(87, 97) as $number) {
			$this->addColumn($table, 'tarifdetail_' . $number, Schema::TYPE_STRING);
		}

		return true;
	}
}
