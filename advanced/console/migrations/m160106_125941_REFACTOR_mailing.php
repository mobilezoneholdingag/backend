<?php

use yii\db\Schema;
use yii\db\Migration;

class m160106_125941_REFACTOR_mailing extends Migration
{
	public function up()
	{
		$this->dropTable('contest_teufel');
		$this->dropTable('contest_tuv');
		$this->dropTable('coupons_meinetanne');
		$this->dropTable('mailing_teufel');
	}

	public function down()
	{
		echo "m160106_125941_REFACTOR_mailing cannot be reverted.\n";
		return false;
	}
}
