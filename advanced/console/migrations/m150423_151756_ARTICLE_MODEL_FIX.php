<?php

use yii\db\Schema;
use yii\db\Migration;

class m150423_151756_ARTICLE_MODEL_FIX extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Article::tableName(), 'price_buy', Schema::TYPE_MONEY);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Article::tableName(), 'price_buy');

		return true;
	}
}
