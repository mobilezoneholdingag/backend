<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Provider;


class m150408_143113_NO_TICKET_add_provider_tos_link extends Migration
{
	public function up()
	{
		$this->addColumn(Provider::tableName(), 'link_tos', Schema::TYPE_STRING);
	}

	public function down()
	{
		$this->dropColumn(Provider::tableName(), 'link_tos');
	}
}
