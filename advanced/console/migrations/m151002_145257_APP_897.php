<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_145257_APP_897 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\LandingPages::tableName(), 'category_id', Schema::TYPE_INTEGER);
		$this->update(\common\models\LandingPages::tableName(), ['category_id' => 1]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\LandingPages::tableName(), 'category_id');

		return true;
	}
}
