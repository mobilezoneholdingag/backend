<?php

use yii\db\Migration;

class m170113_095146_MZ_494_add_rate_payment_count_field_to_tariffs_table extends Migration
{
	const TABLE = 'tariffs';
	const COLUMN = 'rate_payment_count';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->integer()->after('contract_duration'));
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);
	}
}
