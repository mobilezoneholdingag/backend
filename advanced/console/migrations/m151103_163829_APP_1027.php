<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_163829_APP_1027 extends Migration
{
	public function up()
	{
		$this->insert(\backend\models\Tracker::tableName(), ['code' => 'DIRECT', 'prio' => 1, 'title' => \backend\models\Tracker::DIRECT_SALE_TRACKER_TITLE]);

		$directSaleTrackerId = \backend\models\Tracker::find()->where(['title' => \backend\models\Tracker::DIRECT_SALE_TRACKER_TITLE])->one();

		$this->update(\common\models\Sales::tableName(), ['tracker_id' => $directSaleTrackerId->id], ['tracker_id' => null, 'bid' => null]);

		return true;
	}

	public function down()
	{
		$directSaleTrackerId = \backend\models\Tracker::find()->where(['title' => \backend\models\Tracker::DIRECT_SALE_TRACKER_TITLE])->one();

		$this->update(\common\models\Sales::tableName(), ['tracker_id' => null, 'bid' => null], ['tracker_id' => $directSaleTrackerId->id]);

		$this->delete(\backend\models\Tracker::tableName(), ['id' => $directSaleTrackerId->id]);

		return true;
	}

}
