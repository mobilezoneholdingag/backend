<?php

use common\modules\order\models\Item;
use common\modules\order\models\Order;
use yii\db\Migration;

class m160923_155256_update_migration_for_order_items_number_format_the_second extends Migration
{
	public function up()
	{
		$this->alterColumn(Item::tableName(), 'vat_rate', $this->decimal(4, 2));

		$this->alterColumn(Order::tableName(), 'price_once', $this->decimal(8, 2));
		$this->alterColumn(Order::tableName(), 'price_monthly', $this->decimal(8, 2));
		$this->alterColumn(Order::tableName(), 'vat_rate', $this->decimal(4, 2));
	}

	public function down()
	{
		echo "m160923_155256_update_migration_for_order_items_number_format_the_second cannot be reverted.\n";

		return false;
	}
}
