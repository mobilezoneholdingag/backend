<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;

class m150428_150223_APP_376_fix1 extends Migration
{
	public function up()
	{
		$this->execute("UPDATE sales SET tariff_device_final_price = tariff_device_price WHERE tariff_device_final_price IS NULL");
		return true;
	}

	public function down()
	{
		echo "m150428_150223_APP_376_fix1 cannot be reverted.\n";
		return false;
	}
}
