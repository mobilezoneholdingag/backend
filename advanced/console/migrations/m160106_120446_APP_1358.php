<?php

use yii\db\Schema;
use yii\db\Migration;

class m160106_120446_APP_1358 extends Migration
{
	public function up()
	{
		// ugly hard memory limit / runtime increase, but it's a necessary quick-fix to get order exports
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', '300');
		ini_set('max_input_time', '300');
		ini_set('default_socket_timeout', '300');

		$mapping = [
			0 => 1,
			1 => 2,
			2 => 3,
			3 => 4,
			4 => 5,
			5 => 6,
			7 => 7,
			9 => 8,
			10 => 9,
			17 => 10,
			19 => 11,
			102 => 12,
			104 => 13,
			1017 => 14,
			1011 => 15,
			1012 => 16,
			1016 => 17,
		];

		$i = 0;

		foreach (\common\models\Sales::find()->batch(500) as $sales) {
			foreach ($sales as $sale) {
				if ($sale->order_status !== null AND isset($mapping[$sale->order_status])) {
					$sale->order_status = $mapping[$sale->order_status];
					$sale->save(false);
				}
			}
			$i = $i + 500;
			echo $i . " done" . PHP_EOL;
		}

		return true;
	}

	public function down()
	{

		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', '300');
		ini_set('max_input_time', '300');
		ini_set('default_socket_timeout', '300');

		$mapping = [
			0 => 1,
			1 => 2,
			2 => 3,
			3 => 4,
			4 => 5,
			5 => 6,
			7 => 7,
			9 => 8,
			10 => 9,
			17 => 10,
			19 => 11,
			102 => 12,
			104 => 13,
			1017 => 14,
			1011 => 15,
			1012 => 16,
			1016 => 17,
		];
		$i = 0;
		$mapping = array_flip($mapping);

		foreach (\common\models\Sales::find()->batch(500) as $sales) {
			foreach ($sales as $sale) {
				if ($sale->order_status !== null AND isset($mapping[$sale->order_status])) {
					$sale->order_status = $mapping[$sale->order_status];
					$sale->save(false);
				}
			}
			$i = $i + 500;
			echo $i . " done" . PHP_EOL;
		}

		return true;
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
