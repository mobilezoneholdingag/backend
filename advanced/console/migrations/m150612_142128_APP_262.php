<?php

use yii\db\Schema;
use yii\db\Migration;

class m150612_142128_APP_262 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Tariffs::tableName(), 'special_id', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Tariffs::tableName(), 'special_id');

		return true;
	}
}
