<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\OrderStatus;

class m150807_093115_APP_608_new_order_stati_usertexts extends Migration
{
	public function up()
	{
		$this->addColumn(OrderStatus::tableName(), 'user_text', Schema::TYPE_STRING);

		$this->update(OrderStatus::tableName(), ['user_text' => 'Abgelehnt'], ['fulfillment_partner_id' => 2, 'code' => 104]);
		$this->update(OrderStatus::tableName(), ['user_text' => 'Bestellung erfolreich abgeschlossen)'], ['fulfillment_partner_id' => 2, 'code' => 1017]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(OrderStatus::tableName(), 'user_text');

		return true;
	}
}
