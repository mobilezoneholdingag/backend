<?php

use common\models\DeliveryStatus;
use yii\db\Migration;

class m160719_141101_MZ_47_create_mobilezone_delivery_statuses extends Migration
{
	public function up()
	{
		$this->execute('SET FOREIGN_KEY_CHECKS = 0');
		$this->truncateTable(DeliveryStatus::tableName());

		$this->insert(DeliveryStatus::tableName(), [
			'label' => 'Auf Lager',
			'css_class' => 'available',
			'due' => 'PT0S',
		]);

		$this->insert(DeliveryStatus::tableName(), [
			'label' => 'Mehr ist unterwegs',
			'css_class' => 'available',
			'due' => 'P3D',
		]);

		$this->insert(DeliveryStatus::tableName(), [
			'label' => 'Kein Lagerbestand',
			'css_class' => 'pending',
			'due' => 'preorder',
		]);
	}

	public function down()
	{
		$this->execute('SET FOREIGN_KEY_CHECKS = 0');
		$this->truncateTable(DeliveryStatus::tableName());
	}
}
