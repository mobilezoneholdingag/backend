<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;
use common\helper\DateHelper;

class m150528_174126_APP_434 extends Migration
{
	public function up()
	{
		foreach (Sales::find()->each() as $sale) {
			$sale->timestamp_sale = DateHelper::convertDateTimeLondonToDateTimeBerlin($sale->timestamp_sale);
			$sale->save(false);
		}
	}

	public function down()
	{
		foreach (Sales::find()->each() as $sale) {
			$sale->timestamp_sale = DateHelper::convertDateTimeBerlinToDateTimeLondon($sale->timestamp_sale);
			$sale->save(false);
		}
	}
}
