<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;
use backend\controllers\SalesController;

class m150430_153351_APP_376_new_mass_calculation_2 extends Migration
{
	public function up()
	{
		$sales = Sales::find()->all();
		foreach ($sales as $sale) {
			$sale->marge = SalesController::calculateMarge($sale);
			$sale->save(false);
		}
	}

	public function down()
	{
		echo "m150430_153351_APP_376_new_mass_calculation_2 cannot be reverted.\n";

		return false;
	}
}
