<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_112646_APP_827 extends Migration
{
	public function up()
	{
		$this->dropTable('tarifgruppen');
		$this->dropTable('tarifoptionen');
		$this->dropTable('tarifoptionen_gruppen');
		$this->dropTable('tarifoptionen_regeln');
		$this->dropTable('tarifoptionen_stammdaten');
	}

	public function down()
	{
		echo "m150921_112646_APP_827 cannot be reverted.\n";

		return false;
	}

}
