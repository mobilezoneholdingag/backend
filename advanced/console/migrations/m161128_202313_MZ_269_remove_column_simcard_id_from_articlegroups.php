<?php

use yii\db\Migration;

/**
 * Class m161128_202313_MZ_269_remove_column_simcard_id_from_articlegroups
 */
class m161128_202313_MZ_269_remove_column_simcard_id_from_articlegroups extends Migration
{
	const TABLENAME = 'article_groups';
	const COLNAME = 'simcard_id';

	/**
	 * @return bool
	 */
	public function up()
	{
		$this->dropColumn(self::TABLENAME, self::COLNAME);

		return true;
	}

	/**
	 * @return bool
	 */
	public function down()
	{
		$this->addColumn(self::TABLENAME, self::COLNAME, $this->integer(11));

		return true;
	}
}
