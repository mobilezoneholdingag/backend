<?php

use yii\db\Migration;

class m170523_161458_MZ_575_clean_up_language_column extends Migration
{
	const LANGUAGE_FIXES = [
		['language' => null, 'result' => 'de-CH'],
		['language' => 'de', 'result' => 'de-CH'],
		['language' => 'ch', 'result' => 'de-CH'],
		['language' => 'fr', 'result' => 'fr-FR'],
		['language' => 'it', 'result' => 'it-IT'],
	];

	public function up()
	{
		foreach (static::LANGUAGE_FIXES as $languageFix) {
			$this->update('customer', ['language' => $languageFix['result']], ['language' => $languageFix['language']]);
		}

		return true;
	}

	public function down()
	{
		echo "m170523_161458_MZ_575_clean_up_language_column cannot be reverted.\n";

		return false;
	}
}
