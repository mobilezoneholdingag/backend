<?php

use common\models\ArticleDetailsValues;
use yii\db\Migration;

class m160210_081354_APP_1526 extends Migration
{
	public function up()
	{

		$articleDetailValues = ArticleDetailsValues::find()->all();

		foreach ($articleDetailValues as $value) {
			$value->detail_48 = str_replace('.', '', $value->detail_48);
			$value->save(false);
		}

		return true;
	}

	public function down()
	{
		return false;
	}
}
