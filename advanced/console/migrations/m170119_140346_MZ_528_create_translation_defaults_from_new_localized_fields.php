<?php

use common\models\Article;
use common\models\Shop;
use yii\db\Migration;

class m170119_140346_MZ_528_create_translation_defaults_from_new_localized_fields extends Migration
{
	private $modelsToTranslate = [
		'common\models\Article' => [
			'product_name',
			'color_label',
		],
		'common\models\Shop' => [
			'name',
			'manager_function',
			'images_title',
		],
	];

	public function up()
	{
		/* @var Article|Shop $className */
		foreach ($this->modelsToTranslate as $className => $properties) {
			/* @var Article[]|Shop[] $models */
			$models = $className::find()->all();

			foreach ($models as $model) {
				foreach ($properties as $propertyName) {
					foreach (Yii::$app->params['languages'] as $language) {
						$model->translationsArr[$propertyName][$language]['translation'] = $model->{$propertyName};
					}
				}

				$model->save(false);
			}

			unset($models);
		}

		return true;
	}

	public function down()
	{
		echo "m170119_140346_MZ_528_create_translation_defaults_from_new_localized_fields cannot be reverted.\n";

		return false;
	}
}
