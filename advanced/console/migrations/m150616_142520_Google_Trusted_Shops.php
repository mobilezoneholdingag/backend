<?php

use yii\db\Schema;
use yii\db\Migration;

class m150616_142520_Google_Trusted_Shops extends Migration
{
	public function up()
	{
		$table = \common\models\DeliveryStatus::tableName();

		$this->addColumn(
			$table,
			'due',
			'VARCHAR(80)'
		);

		$dues = [
			1 => 'PT0S',
			2 => 'P3D',
			3 => 'P2W',
			4 => 'preorder',
			5 => 'preorder'
		];

		foreach($dues as $id => $due) {
			$this->update($table, ['due' => $dues[$id]], ['id' => $id]);
		}
	}

	public function down()
	{
		$this->dropColumn(\common\models\DeliveryStatus::tableName(), 'due');
	}
}
