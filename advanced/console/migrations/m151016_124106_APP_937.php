<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_124106_APP_937 extends Migration
{
	public function up()
	{
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'old_provider_id', Schema::TYPE_INTEGER);
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'old_phone_number_prefix', Schema::TYPE_STRING);
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'old_phone_number_main', Schema::TYPE_STRING);
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'old_provider_type', Schema::TYPE_INTEGER);
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'old_provider_port_request_status', Schema::TYPE_INTEGER);
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'old_provider_port_request_expiration_date', Schema::TYPE_STRING);
		$this->addColumn(\backend\models\SalesUnapproved::tableName(), 'password', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'old_provider_id');
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'old_phone_number_prefix');
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'old_phone_number_main');
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'old_provider_type');
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'old_provider_port_request_status');
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'old_provider_port_request_expiration_date');
		$this->dropColumn(\backend\models\SalesUnapproved::tableName(), 'password');

		return true;
	}
}
