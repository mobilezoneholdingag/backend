<?php

use yii\db\Schema;
use yii\db\Migration;

class m150923_102249_APP_821 extends Migration
{
	public function up()
	{
		$this->createTable('no_ads_customer', [
			'id' => Schema::TYPE_PK,
			'email' => Schema::TYPE_STRING,
			'created_at' => Schema::TYPE_INTEGER,
		]);
	}

	public function down()
	{
		$this->dropTable('no_ads_customer');
	}
}
