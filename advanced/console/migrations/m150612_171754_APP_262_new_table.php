<?php

use yii\db\Schema;
use yii\db\Migration;

class m150612_171754_APP_262_new_table extends Migration
{
	public function up()
	{
		$this->createTable('tariff_special', [
			'id' => Schema::TYPE_PK,
			'type' => Schema::TYPE_STRING,
			'name' => Schema::TYPE_STRING,
			'price_change' => Schema::TYPE_FLOAT,
			'infotext' => Schema::TYPE_TEXT,
			'infotext_2' => Schema::TYPE_TEXT,
		]);
	}

	public function down()
	{
		$this->dropTable('tariff_special');
	}
}
