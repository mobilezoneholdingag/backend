<?php

use yii\db\Schema;
use yii\db\Migration;
use backend\models\MarketingPartner;
use common\models\Sales;
use backend\models\SalesUnapproved;

class m160112_120900_APP_976 extends Migration
{

	const VERIVOX = 'Verivox';
	const CHECK24 = 'CHECK24';

	public function up()
	{
		$verivox_id = MarketingPartner::find()->where(['title' => self::VERIVOX])->one()->id;
		$check24_id = MarketingPartner::find()->where(['title' => self::CHECK24])->one()->id;

		$this->addColumn(Sales::tableName(), 'marketing_partner_temp_id', Schema::TYPE_STRING);

		$this->update(Sales::tableName(), ['marketing_partner_temp_id' => $verivox_id], ['not', ['verivox_id' => null]]);
		$this->update(Sales::tableName(), ['marketing_partner_temp_id' => $check24_id], ['not', ['check24_id' => null]]);

		$check_sales = Sales::find()->where(['not', ['check24_id' => null]])->all();
		$verivox_sales = Sales::find()->where(['not', ['verivox_id' => null]])->all();

		foreach ($check_sales as $sale) {
			$this->update(Sales::tableName(), [
				'verivox_id' => $sale->check24_id,
			],
				['id' => $sale->id]);
		}

		foreach ($verivox_sales as $sale) {
			$this->update(Sales::tableName(), [
				'check24_id' => $sale->marketing_partner_id,
			],
				['id' => $sale->id]);
		}

		$this->renameColumn(Sales::tableName(), 'verivox_id', 'marketing_partner_order_id');
		$this->renameColumn(Sales::tableName(), 'verivox_transaction_id', 'marketing_partner_transaction_id');
		$this->renameColumn(Sales::tableName(), 'check24_id', 'marketing_partner_sub_id');

		$this->dropColumn(Sales::tableName(), 'marketing_partner_id');

		$this->renameColumn(Sales::tableName(), 'marketing_partner_temp_id', 'marketing_partner_id');

		$this->update(Sales::tableName(), ['marketing_partner_sub_id' => null], ['marketing_partner_id' => $check24_id]);

		// same for sales_unapproved
		$this->addColumn(SalesUnapproved::tableName(), 'marketing_partner_temp_id', Schema::TYPE_STRING);

		$this->update(SalesUnapproved::tableName(), ['marketing_partner_temp_id' => $verivox_id], ['not', ['verivox_id' => null]]);
		$this->update(SalesUnapproved::tableName(), ['marketing_partner_temp_id' => $check24_id], ['not', ['check24_id' => null]]);

		$check_sales = SalesUnapproved::find()->where(['not', ['check24_id' => null]])->all();
		$verivox_sales = SalesUnapproved::find()->where(['not', ['verivox_id' => null]])->all();

		foreach ($check_sales as $sale) {
			$this->update(SalesUnapproved::tableName(), [
				'verivox_id' => $sale->check24_id,
			],
				['id' => $sale->id]);
		}

		foreach ($verivox_sales as $sale) {
			$this->update(SalesUnapproved::tableName(), [
				'check24_id' => $sale->marketing_partner_id,
			],
				['id' => $sale->id]);
		}

		$this->renameColumn(SalesUnapproved::tableName(), 'verivox_id', 'marketing_partner_order_id');
		$this->renameColumn(SalesUnapproved::tableName(), 'verivox_transaction_id', 'marketing_partner_transaction_id');
		$this->renameColumn(SalesUnapproved::tableName(), 'check24_id', 'marketing_partner_sub_id');

		$this->dropColumn(SalesUnapproved::tableName(), 'marketing_partner_id');

		$this->renameColumn(SalesUnapproved::tableName(), 'marketing_partner_temp_id', 'marketing_partner_id');

		$this->update(SalesUnapproved::tableName(), ['marketing_partner_sub_id' => null], ['marketing_partner_id' => $check24_id]);

		return true;
	}

	public function down()
	{
		echo "m160112_120900_APP_976 cannot be reverted.\n";

		return false;
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
