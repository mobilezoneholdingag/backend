<?php

use yii\db\Schema;
use yii\db\Migration;

class m160119_151933_APP_1419 extends Migration
{
	public function up()
	{
		$glossaryEntries = \common\models\Glossary::find()->all();
		foreach ($glossaryEntries as $glossaryEntry) {
			$glossaryEntry->text = str_replace('http://www.deinhandy', 'https://www.deinhandy', $glossaryEntry->text);
			$glossaryEntry->text = str_replace('http://deinhandy', 'https://www.deinhandy', $glossaryEntry->text);
			$glossaryEntry->text = str_replace('https://deinhandy', 'https://www.deinhandy', $glossaryEntry->text);
			$glossaryEntry->text = str_replace('.html', '', $glossaryEntry->text);
			$glossaryEntry->text = str_replace('/">', '">', $glossaryEntry->text);
			$glossaryEntry->save();
		}

		$landingPages = \common\models\LandingPages::find()->all();
		foreach ($landingPages as $landingPage) {
			$landingPage->html_code = str_replace('http://www.deinhandy', 'https://www.deinhandy', $landingPage->html_code);
			$landingPage->html_code = str_replace('http://deinhandy', 'https://www.deinhandy', $landingPage->html_code);
			$landingPage->html_code = str_replace('https://deinhandy', 'https://www.deinhandy', $landingPage->html_code);
			$landingPage->html_code = str_replace('.html', '', $landingPage->html_code);
			$landingPage->html_code = str_replace('/">', '">', $landingPage->html_code);
			$landingPage->save();
		}

		$seoEntries = \common\models\Seo::find()->all();
		foreach ($seoEntries as $seoEntry) {
			$seoEntry->html_content = str_replace('http://www.deinhandy', 'https://www.deinhandy', $seoEntry->html_content);
			$seoEntry->html_content = str_replace('http://deinhandy', 'https://www.deinhandy', $seoEntry->html_content);
			$seoEntry->html_content = str_replace('https://deinhandy', 'https://www.deinhandy', $seoEntry->html_content);
			$seoEntry->html_content = str_replace('.html', '', $seoEntry->html_content);
			$seoEntry->html_content = str_replace('/">', '">', $seoEntry->html_content);
			$seoEntry->save();
		}

		return true;
	}

	public function down()
	{
		echo "m160119_151933_APP_1419 cannot be reverted.\n";

		return true;
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
