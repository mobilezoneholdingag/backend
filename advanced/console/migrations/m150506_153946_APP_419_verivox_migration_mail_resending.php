<?php

use yii\db\Migration;
use common\models\Sales;

class m150506_153946_APP_419_verivox_migration_mail_resending extends Migration
{
	public function up()
	{
		$sales = Sales::find()->where(['>=', 'id', 1182])->andWhere(['<=', 'id', 1293])->all();

		foreach ($sales as $sale) {
			/**
			 * @var $sale Sales
			 */
			\common\helper\ConformationMailHelper::sendConfirmationMail($sale);
		}
	}

	public function down()
	{
		echo "m150506_153946_APP_419_verivox_migration_mail_resending cannot be reverted.\n";

		return false;
	}
}
