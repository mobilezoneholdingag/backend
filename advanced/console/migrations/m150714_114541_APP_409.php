<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_114541_APP_409 extends Migration
{
	public function up()
	{
		$this->alterColumn(\common\models\Tariffs::tableName(), 'tarifdetail_1', Schema::TYPE_BOOLEAN);
		$this->renameColumn(\common\models\Tariffs::tableName(), 'tarifdetail_1', 'is_hint');

		$this->update(\common\models\Tariffs::tableName(), ['is_hint' => 1]);

		return true;
	}

	public function down()
	{

		$this->alterColumn(\common\models\Tariffs::tableName(), 'is_hint', Schema::TYPE_STRING);
		$this->renameColumn(\common\models\Tariffs::tableName(), 'is_hint', 'tarifdetail_1');

		$this->update(\common\models\Tariffs::tableName(), ['tarifdetail_1' => '']);

		return true;
	}
}
