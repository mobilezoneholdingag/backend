<?php

use yii\db\Schema;
use yii\db\Migration;

class m150423_081735_APP_312 extends Migration
{
	public function up()
	{
		$this->update(\common\models\ShopConfig::tableName(), ['top10smartphones_set_id' => 21], ['id' => 1]);

		return true;
	}

	public function down()
	{
		echo "m150423_081735_APP_312 cannot be reverted.\n";

		return false;
	}
}
