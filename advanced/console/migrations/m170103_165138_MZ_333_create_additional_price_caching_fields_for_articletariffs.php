<?php

use yii\db\Migration;

class m170103_165138_MZ_333_create_additional_price_caching_fields_for_articletariffs extends Migration
{
	const TABLE = 'article_tariffs';

	public function up()
	{
		$this->addColumn(self::TABLE, 'price_article_first_month', $this->decimal(7, 2)->after('price_tariff_monthly'));
		$this->addColumn(self::TABLE, 'price_article_total', $this->decimal(7, 2)->after('price_article_first_month'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, 'price_article_first_month');
		$this->dropColumn(self::TABLE, 'price_article_total');

		return true;
	}
}
