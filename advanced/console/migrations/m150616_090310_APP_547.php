<?php

use yii\db\Schema;
use yii\db\Migration;

class m150616_090310_APP_547 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Manufacturer::tableName(), 'sort', Schema::TYPE_INTEGER);

		$manufacturers = \common\models\Manufacturer::find()->all();

		foreach ($manufacturers as $manufacturer) {
			$this->update(\common\models\Manufacturer::tableName(), ['sort' => $manufacturer->id], ['id' => $manufacturer->id]);
		}

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Manufacturer::tableName(), 'sort');

		return true;
	}
}
