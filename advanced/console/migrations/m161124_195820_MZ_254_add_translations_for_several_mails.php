<?php

use yii\db\Migration;

class m161124_195820_MZ_254_add_translations_for_several_mails extends Migration
{
	const TABLE_NAME_LANGUAGE_SOURCE = 'language_source';
	const TABLE_NAME_LANGUAGE_TRANSLATE = 'language_translate';
	const LANGUAGE_CH = 'de-CH';
	const LANGUAGE_FR = 'fr-FR';
	const LANGUAGE_IT = 'it-IT';

	public function up()
	{
		$this->addTranslation('mail_password_reset', 'subject', self::LANGUAGE_CH, 'Passwort zurücksetzen');
		$this->addTranslation('mail_password_reset', 'Hallo', self::LANGUAGE_CH, 'Hallo');
		$this->addTranslation(
			'mail_password_reset',
			'follow_link_for_password_reset',
			self::LANGUAGE_CH,
			'Folge dem Link um dein Passwort zurückzusetzen:'
		);
		$this->addTranslation('mail_order_confirmation', 'subject', self::LANGUAGE_CH, 'Bestellbestätigung');

		return true;
	}

	public function down()
	{
		$this->delete(self::TABLE_NAME_LANGUAGE_SOURCE, ['category' => 'mail_password_reset', 'message' => 'Hallo']);
		$this->delete(
			self::TABLE_NAME_LANGUAGE_SOURCE,
			['category' => 'mail_password_reset', 'message' => 'follow_link_for_password_reset']
		);

		return true;
	}

	/**
	 * @param $category
	 * @param $key
	 * @param null $language
	 * @param null $value
	 */
	protected function addTranslation($category, $key, $language = null, $value = null)
	{
		$this->insert(self::TABLE_NAME_LANGUAGE_SOURCE, ['category' => $category, 'message' => $key]);
		if (is_null($language) === false && is_null($value) === false) {
			$lastId = $this->db->getLastInsertID();
			$this->insert(
				self::TABLE_NAME_LANGUAGE_TRANSLATE,
				['id' => $lastId, 'language' => $language, 'translation' => $value]
			);
		}
	}
}
