<?php

use yii\db\Migration;

class m161118_084206_MZ_254_create_table_homer extends Migration
{
	public function up()
	{
		$this->createTable(
			'homer',
			[
				'id' => $this->primaryKey(),
				'mail_class' => $this->string()->notNull(),
				'entity_class' => $this->string(),
				'entity_id' => $this->integer(),
				'send_planned_at' => $this->dateTime(),
				'sent_at' => $this->dateTime(),
				'sent' => $this->boolean(),
				'recipients' => $this->text()->notNull(),
				'additional_template_parameters' => $this->text(),
				'language_code' => $this->string(),
			]
		);
		$this->createIndex('specific_homer', 'homer', ['mail_class', 'entity_class', 'entity_id']);
		$this->createIndex('sent', 'homer', ['sent']);
		$this->createIndex('really_sent', 'homer', ['sent', 'sent_at']);
		$this->createIndex('sent_at', 'homer', ['sent_at']);
		$this->createIndex('send_planned_at', 'homer', ['send_planned_at']);

		return true;
	}

	public function down()
	{
		$this->dropTable('homer');

		return true;
	}
}
