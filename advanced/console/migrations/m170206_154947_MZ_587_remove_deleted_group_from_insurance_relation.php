<?php

use yii\db\Migration;

class m170206_154947_MZ_587_remove_deleted_group_from_insurance_relation extends Migration
{
	const TABLE = 'article_groups_insurance';
	const GROUPS_TABLE = 'article_groups';

	public function up()
	{
		$this->dropForeignKey('g_id_g_id', self::TABLE);
		$this->addForeignKey('g_id_g_id', self::TABLE, 'group_id', self::GROUPS_TABLE, 'id', 'CASCADE', 'CASCADE');

		return true;
	}

	public function down()
	{
		$this->dropForeignKey('g_id_g_id', self::TABLE);
		$this->addForeignKey('g_id_g_id', self::TABLE, 'group_id', self::GROUPS_TABLE, 'id');

		return true;
	}
}
