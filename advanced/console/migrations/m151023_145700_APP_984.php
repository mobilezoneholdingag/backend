<?php

use yii\db\Schema;
use yii\db\Migration;

class m151023_145700_APP_984 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\ArticleTariffs::tableName(), 'delivery_status_id', Schema::TYPE_INTEGER);
		$this->addColumn(\backend\models\ArticleTariffsKeepList::tableName(), 'delivery_status_id', Schema::TYPE_INTEGER);
		$this->addColumn(\backend\models\ArticleTariffsKeepList::tableName(), 'subsidy_payment', Schema::TYPE_DECIMAL.'(7,2)');

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\ArticleTariffs::tableName(), 'delivery_status_id');
		$this->dropColumn(\backend\models\ArticleTariffsKeepList::tableName(), 'delivery_status_id');
		$this->dropColumn(\backend\models\ArticleTariffsKeepList::tableName(), 'subsidy_payment');

		return true;
	}

}
