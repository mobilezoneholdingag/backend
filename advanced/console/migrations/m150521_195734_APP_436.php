<?php

use yii\db\Schema;
use yii\db\Migration;
use backend\models\SalesUnapproved;
use common\models\Sales;
use backend\models\Tracker;

class m150521_195734_APP_436 extends Migration
{
	public function up()
	{
		$this->addColumn(SalesUnapproved::tableName(), 'tracker_id', Schema::TYPE_INTEGER);
		$this->addColumn(Sales::tableName(), 'tracker_id', Schema::TYPE_INTEGER);

		$sales = Sales::find()->all();

		foreach ($sales as $sale) {
			$sale->tracker_id = Tracker::getTrackerIdBySale($sale);
			$sale->save();
		}
	}

	public function down()
	{
		$this->dropColumn(SalesUnapproved::tableName(), 'tracker_id');
		$this->dropColumn(Sales::tableName(), 'tracker_id');

		$sales = Sales::find()->all();

		// tracker_id is not set to null here to prevent existing entries from getting overwritten
	}
}
