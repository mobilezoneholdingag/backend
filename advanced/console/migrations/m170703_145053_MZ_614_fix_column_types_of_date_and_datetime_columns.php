<?php

use yii\db\Migration;

class m170703_145053_MZ_614_fix_column_types_of_date_and_datetime_columns extends Migration
{
	public function up()
	{
		$this->alterColumn('customer', 'birthday', $this->date());

		$this->unixToDateTimeUp('user', 'created_at');
		$this->unixToDateTimeUp('user', 'updated_at');
	}

	public function down()
	{
		$this->alterColumn('customer', 'birthday', $this->string(255));

		$this->unixToDateTimeDown('user', 'created_at');
		$this->unixToDateTimeDown('user', 'updated_at');
	}

	private function unixToDateTimeUp(string $table, string $column)
	{
		$this->addColumn($table, "{$column}_datetime", $this->dateTime()->after($column));
		$this->execute("UPDATE `{$table}` SET `{$column}_datetime` = FROM_UNIXTIME(`{$column}`)");
		$this->dropColumn($table, $column);
		$this->renameColumn($table, "{$column}_datetime", $column);
	}

	private function unixToDateTimeDown(string $table, string $column)
	{
		$this->addColumn($table, "{$column}_timestamp", $this->integer(11)->after($column));
		$this->execute("UPDATE `{$table}` SET `{$column}_timestamp` = UNIX_TIMESTAMP(`{$column}`)");
		$this->dropColumn($table, $column);
		$this->renameColumn($table, "{$column}_timestamp", $column);
	}
}
