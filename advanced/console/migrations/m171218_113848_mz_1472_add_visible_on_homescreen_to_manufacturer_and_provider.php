<?php

use yii\db\Migration;

class m171218_113848_mz_1472_add_visible_on_homescreen_to_manufacturer_and_provider extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\Provider::tableName(), 'visible_homepage', 'int');
        $this->addColumn(\common\models\Manufacturer::tableName(), 'visible_homepage', 'int');
    }

    public function down() {
        $this->dropColumn(\common\models\Provider::tableName(), 'visible_homepage');
        $this->dropColumn(\common\models\Manufacturer::tableName(), 'visible_homepage');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
