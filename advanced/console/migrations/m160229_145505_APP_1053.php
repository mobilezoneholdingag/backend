<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\TariffSpecial;

class m160229_145505_APP_1053 extends Migration
{
	public function up()
	{
		$this->addColumn(TariffSpecial::tableName(), 'filter_type', \yii\db\mysql\Schema::TYPE_SMALLINT.' DEFAULT 0');

		/** @var TariffSpecial $tariffSpecial */
		foreach(TariffSpecial::find()->each() as $tariffSpecial) {
			switch ($tariffSpecial->type) {
				case 'adac':
					$tariffSpecial->filter_type = 2;
					break;
				case 'telekom':
					$tariffSpecial->filter_type = 1;
					break;
				default:
					$tariffSpecial->filter_type = 0;
					break;
			}

			$tariffSpecial->save();
		}
	}

	public function down()
	{
		$this->dropColumn(TariffSpecial::tableName(), 'filter_type');
	}
}
