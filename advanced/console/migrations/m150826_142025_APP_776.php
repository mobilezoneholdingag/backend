<?php

use yii\db\Schema;
use yii\db\Migration;

class m150826_142025_APP_776 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\TariffSpecial::tableName(), 'short_name', 'VARCHAR(255)');
	}

	public function down()
	{
		return $this->dropColumn(\common\models\TariffSpecial::tableName(), 'short_name');
	}
}
