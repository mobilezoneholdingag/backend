<?php

use yii\db\Migration;

class m160413_093918_APP_1886_create_fulfillmentpartner_mobilezone_ch extends Migration
{
	public function up()
	{
		$this->insert('fulfillment_partners', [
			'id' => 4,
			'title' => 'mobilezone.ch',
			'api_exist' => 1,
			'api_is_active' => 1,
		]);
	}

	public function down()
	{
		$this->delete('fulfillment_partners', ['id' => 4]);
	}
}
