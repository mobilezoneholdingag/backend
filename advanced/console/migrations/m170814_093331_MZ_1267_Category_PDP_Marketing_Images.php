<?php

use yii\db\Migration;

class m170814_093331_MZ_1267_Category_PDP_Marketing_Images extends Migration
{
	const MANUFACTURER_TABLE = 'manufacturer';
	const ARTICLE_GROUPS_TABLE = 'article_groups';

	public function up()
	{
		$this->addColumn(self::MANUFACTURER_TABLE, 'marketing_image', $this->string());
		$this->addColumn(self::MANUFACTURER_TABLE, 'show_on_category', $this->smallInteger(1));
		$this->addColumn(self::MANUFACTURER_TABLE, 'show_on_pdp', $this->smallInteger(1));
		$this->addColumn(self::ARTICLE_GROUPS_TABLE, 'marketing_image', $this->string());
		$this->addColumn(self::ARTICLE_GROUPS_TABLE, 'show_marketing_image', $this->smallInteger(1));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::MANUFACTURER_TABLE, 'marketing_image');
		$this->dropColumn(self::MANUFACTURER_TABLE, 'show_on_category');
		$this->dropColumn(self::MANUFACTURER_TABLE, 'show_on_pdp');
		$this->dropColumn(self::ARTICLE_GROUPS_TABLE, 'marketing_image');
		$this->dropColumn(self::ARTICLE_GROUPS_TABLE, 'show_marketing_image');

		return true;
	}
}
