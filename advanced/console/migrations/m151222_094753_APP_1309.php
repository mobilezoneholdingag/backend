<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_094753_APP_1309 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\OrderStatus::tableName(), 'internal_title', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\OrderStatus::tableName(), 'internal_title');

		return true;
	}
}
