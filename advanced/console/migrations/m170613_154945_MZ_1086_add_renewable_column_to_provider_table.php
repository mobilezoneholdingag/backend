<?php

use yii\db\Migration;

class m170613_154945_MZ_1086_add_renewable_column_to_provider_table extends Migration
{
	const PROVIDER_TABLE = 'provider';
	const COLUMN = 'renewable';

	public function up()
	{
		$this->addColumn(self::PROVIDER_TABLE, self::COLUMN, $this->boolean()->defaultValue(false));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::PROVIDER_TABLE, self::COLUMN);

		return true;
	}
}
