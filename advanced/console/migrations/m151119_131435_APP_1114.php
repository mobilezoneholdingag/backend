<?php

use yii\db\Schema;
use yii\db\Migration;

class m151119_131435_APP_1114 extends Migration
{
	public function up()
	{
		$this->renameColumn(\common\models\LandingPages::tableName(), 'category_id', 'sitemap_category_id');
		$this->addColumn(\common\models\LandingPages::tableName(), 'url_parameter', Schema::TYPE_STRING);

		$this->update(\common\models\LandingPages::tableName(), ['url_parameter' => 'special']);

		return true;
	}

	public function down()
	{
		$this->renameColumn(\common\models\LandingPages::tableName(), 'sitemap_category_id', 'category_id');
		$this->dropColumn(\common\models\LandingPages::tableName(), 'url_parameter');

		return true;
	}

}
