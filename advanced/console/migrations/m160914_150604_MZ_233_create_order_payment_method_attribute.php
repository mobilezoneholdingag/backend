<?php

use common\modules\order\models\Order;
use yii\db\Migration;

class m160914_150604_MZ_233_create_order_payment_method_attribute extends Migration
{
	public function up()
	{
		$this->addColumn(Order::tableName(), 'payment_method', $this->string()->notNull()->after('vat_rate'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(Order::tableName(), 'payment_method');

		return true;
	}
}
