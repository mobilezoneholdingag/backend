<?php

use yii\db\Migration;

/**
 * Handles adding hash to table `renewal_request`.
 */
class m161010_095528_add_hash_to_renewal_request extends Migration
{
	const TABLE = 'renewal_requests';

	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->addColumn(self::TABLE, 'hash', $this->string());

		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropColumn(self::TABLE, 'hash');

		return true;
	}
}
