<?php

use yii\db\Migration;

class m171018_084242_mz_1433_advanced_coupon extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\Coupon::tableName(), 'valid_from', 'int(11)');
        $this->addColumn(\common\models\Coupon::tableName(), 'valid_until', 'int(11)');
        $this->addColumn(\common\models\Coupon::tableName(), 'valid_unique', 'int(11)');
        $this->addColumn(\common\models\Coupon::tableName(), 'is_valid', 'int(11)');
    }

    public function down()
    {
        $this->dropColumn(\common\models\Coupon::tableName(), 'valid_from');
        $this->dropColumn(\common\models\Coupon::tableName(), 'valid_until');
        $this->dropColumn(\common\models\Coupon::tableName(), 'valid_unique');
        $this->dropColumn(\common\models\Coupon::tableName(), 'is_valid');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
