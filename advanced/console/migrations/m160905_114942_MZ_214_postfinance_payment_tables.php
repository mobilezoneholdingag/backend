<?php

use yii\db\Migration;

class m160905_114942_MZ_214_postfinance_payment_tables extends Migration
{
	const TABLE_FORM = 'postfinance_ecom_form';
	const TABLE_CALLBACK = 'postfinance_ecom_callback';

	const TABLE_MAINTENANCE_REQUEST = 'postfinance_directlink_maintenance_request';
	const TABLE_MAINTENANCE_RESPONSE = 'postfinance_directlink_maintenance_response';

	public function up()
	{
		$this->createTable(
			self::TABLE_FORM,
			[
				'id' => $this->primaryKey()->unsigned(),
				'psp_id' => $this->string()->notNull(),
				'order_id' => $this->integer()->notNull(),
				'amount' => $this->integer()->notNull(),
				'currency' => $this->char(3)->notNull(),
				'language' => $this->char(5)->notNull(),
				'cn' => $this->string()->comment('CN = Customer Name'),
				'email' => $this->string(),
				'owner_address' => $this->string(),
				'owner_zip' => $this->string(),
				'owner_town' => $this->string(),
				'owner_cty' => $this->string()->comment('country of the customer'),
				'owner_tel_no' => $this->string()->comment('phone number of the customer'),
				'sha_sign' => $this->string(),
				'accept_url' => $this->string(),
				'decline_url' => $this->string(),
				'exception_url' => $this->string(),
				'cancel_url' => $this->string(),
				'created_at' => $this->dateTime()->notNull(),
			]
		);

		$this->createTable(
			self::TABLE_CALLBACK,
			[
				'id' => $this->primaryKey()->unsigned(),
				'order_id' => $this->string(),
				'amount' => $this->decimal(6, 2),
				'payment_method' => $this->string(),
				'acceptance' => $this->string(),
				'status' => $this->string(),
				'card_number' => $this->string(),
				'customer_name' => $this->string(),
				'pay_id' => $this->string(),
				'nc_error' => $this->string(),
				'brand' => $this->string(),
				'creditdebit' => $this->string(),
				'sha_sign' => $this->string(),
				'is_sha_sign_valid' => $this->boolean(),
				'is_successful' => $this->boolean()->comment('Whether the payment was successful or not'),
				'created_at' => $this->dateTime()->notNull(),
			]
		);

		$this->createTable(
			self::TABLE_MAINTENANCE_REQUEST,
			[
				'id' => $this->primaryKey()->unsigned(),
				'ecom_callback_id' => $this->integer()->unsigned(),
				'operation' => $this->string(),
				'pay_id' => $this->string(),
				'pspid' => $this->string(),
				'userid' => $this->string(),
				'pswd' => $this->string(),
				'sha_sign' => $this->string()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
			]
		);
		$this->addForeignKey(
			'fk_postfinance_directlink_maintenance_request_postfinance_eco',
			self::TABLE_MAINTENANCE_REQUEST,
			'ecom_callback_id',
			self::TABLE_CALLBACK,
			'id',
			'RESTRICT',
			'CASCADE'
		);

		$this->createTable(
			self::TABLE_MAINTENANCE_RESPONSE,
			[
				'id' => $this->primaryKey()->unsigned(),
				'maintenance_request_id' => $this->integer()->unsigned(),
				'order_id' => $this->string(),
				'pay_id' => $this->string(),
				'pay_id_sub' => $this->string(),
				'nc_status' => $this->string(),
				'nc_error' => $this->string(),
				'nc_error_plus' => $this->string(),
				'acceptance' => $this->string(),
				'status' => $this->string(),
				'amount' => $this->string(),
				'currency' => $this->string(),
				'created_at' => $this->dateTime()->notNull(),
			]
		);
		$this->addForeignKey(
			'fk_postfinance_directlink_maintenance_response_postfinance_di1',
			self::TABLE_MAINTENANCE_RESPONSE,
			'maintenance_request_id',
			self::TABLE_MAINTENANCE_REQUEST,
			'id',
			'RESTRICT',
			'CASCADE'
		);
	}

	public function down()
	{
		$this->dropTable(self::TABLE_MAINTENANCE_RESPONSE);
		$this->dropTable(self::TABLE_MAINTENANCE_REQUEST);

		$this->dropTable(self::TABLE_CALLBACK);
		$this->dropTable(self::TABLE_FORM);

		return true;
	}
}
