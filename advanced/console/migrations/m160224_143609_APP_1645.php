<?php

use yii\db\Migration;

class m160224_143609_APP_1645 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Tariffs::tableName(), 'is_sim_only', \yii\db\mysql\Schema::TYPE_BOOLEAN);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Tariffs::tableName(), 'is_sim_only');

		return true;
	}
}
