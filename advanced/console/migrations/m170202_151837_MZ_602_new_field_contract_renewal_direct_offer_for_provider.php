<?php

use yii\db\Migration;

class m170202_151837_MZ_602_new_field_contract_renewal_direct_offer_for_provider extends Migration
{
	const TABLE = 'provider';
	const NEW_FIELD = 'contract_renewal_direct_offer';

	public function up()
	{
		$this->addColumn(self::TABLE, self::NEW_FIELD, $this->boolean()->defaultValue(0)->notNull());
		$this->update(self::TABLE, [self::NEW_FIELD => 1], ['system_title' => 'swisscom']);

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::NEW_FIELD);

		return true;
	}
}
