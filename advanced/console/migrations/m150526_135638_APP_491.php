<?php

use yii\db\Migration;
use common\models\Sales;

class m150526_135638_APP_491 extends Migration
{
	public function up()
	{
		$sale = Sales::find()->where(['order_id_eam' => 142255])->one();
		if ($sale) {
			$sale->partner_id = '1586';
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142256])->one();
		if ($sale) {
			$sale->partner_id = '1';
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142257])->one();
		if ($sale) {
			$sale->partner_id = '1586';
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142258])->one();
		if ($sale) {
			$sale->partner_id = '76';
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142259])->one();
		if ($sale) {
			$sale->partner_id = '76';
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142260])->one();
		if ($sale) {
			$sale->partner_id = '1';
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142262])->one();
		if ($sale) {
			$sale->partner_id = '1';
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142263])->one();
		if ($sale) {
			$sale->partner_id = '1';
			$sale->save(false);
		}
	}

	public function down()
	{
		$sale = Sales::find()->where(['order_id_eam' => 142255])->one();
		if ($sale) {
			$sale->partner_id = null;
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142256])->one();
		if ($sale) {
			$sale->partner_id = null;
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142257])->one();
		if ($sale) {
			$sale->partner_id = null;
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142258])->one();
		if ($sale) {
			$sale->partner_id = null;
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142259])->one();
		if ($sale) {
			$sale->partner_id = null;
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142260])->one();
		if ($sale) {
			$sale->partner_id = null;
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142262])->one();
		if ($sale) {
			$sale->partner_id = null;
			$sale->save(false);
		}

		$sale = Sales::find()->where(['order_id_eam' => 142263])->one();
		if ($sale) {
			$sale->partner_id = null;
			$sale->save(false);
		}
	}
}
