<?php

use yii\db\Migration;

class m170327_085739_MZ_533_add_fields_shop_table extends Migration
{
	const SHOP_TABLE = 'shop';
	const CLOSURE_DATE_COLUMN = 'closure_date';
	const OPENING_DATE_COLUMN = 'opening_date';

	public function up()
    {
    	$this->addColumn(self::SHOP_TABLE, self::CLOSURE_DATE_COLUMN, $this->date());
    	$this->addColumn(self::SHOP_TABLE, self::OPENING_DATE_COLUMN, $this->date()->notNull()->defaultValue('2017-01-01'));
    }

    public function down()
    {
    	$this->dropColumn(self::SHOP_TABLE, self::CLOSURE_DATE_COLUMN);
    	$this->dropColumn(self::SHOP_TABLE, self::OPENING_DATE_COLUMN);
    }
}
