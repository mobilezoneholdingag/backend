<?php

use yii\db\Schema;
use yii\db\Migration;

class m151019_124621_APP_881 extends Migration
{
	public function up()
	{
		// this tariff special exists 4 times with exactly the same content. To fit EAM's API requirements we still have
		// to save this 4 times, each time with a different ID
		foreach ([6, 7, 8, 9] as $tariff_special_id) {
			$this->insert(\common\models\TariffSpecial::tableName(), [
				'id' => $tariff_special_id,
				'type' => 'telekom',
				'name' => 'MagentaEINS-Rabatt',
				'price_change' => -10,
				'infotext' => 'Ich möchte vom MagentaEINS Paketvorteil profitieren und monatlich <strong>10,00 EUR</strong> EUR Rabatt erhalten',
				'custom_infotext_tariff' => 'mtl. Rabatt mit MagentaEINS Paketvorteil',
				'custom_infotext_cart' => 'Als Telekom Neu- oder Bestandskunde kannst du ab sofort Mobilfunk und Festnetz kombinieren und vom MagentaEINS Paketvorteil profitieren. Bitte gib hierzu bei der Bestellung deine Telekom IP-Festnetz-Nummer und deine Festnetz-Kundennummer (du findest diese oben rechts auf deiner Festnetz-Rechnung) an. Du erhältst so monatlich 10,00 EUR Rabatt auf deine Mobilfunkrechnung.',
				'short_name' => 'telekom'
			]);
		}

		return true;
	}

	public function down()
	{
		foreach ([6, 7, 8, 9] as $tariff_special_id) {
			$this->delete(\common\models\TariffSpecial::tableName(), ['id' => $tariff_special_id,]);
		}

		return true;
	}
}
