<?php

use common\modules\order\controllers\console\CommandController;
use Symfony\Component\Console\Output\ConsoleOutput;
use yii\db\Migration;

class m170630_134437_MZ_1143_add_mode_column_to_order_table extends Migration
{
	const COLUMN = 'mode';
	const TABLE = 'order';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->string(30));

		try {
			CommandController::fixOrderModes();
		} catch (\Exception $e) {
			(new ConsoleOutput())->writeln(PHP_EOL . 'Please manually perform the command ' .
				'<error>order/command/fix-order-modes</error> after all migrations have been processed!' . PHP_EOL);
		}

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}
}
