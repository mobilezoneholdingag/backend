<?php

use common\models\Tariffs;
use yii\db\Migration;

class m160831_154959_MZ_202_new_tariff_erp_name_field_and_others extends Migration
{
	public function up()
	{
		$this->addColumn(Tariffs::tableName(), 'erp_title', $this->string()->after('title'));
		$this->alterColumn(Tariffs::tableName(), 'recommended_for', $this->integer());

		$this->addColumn(Tariffs::tableName(), 'text_call', $this->string()->after('description'));
		$this->addColumn(Tariffs::tableName(), 'text_sms', $this->string()->after('text_call'));
		$this->addColumn(Tariffs::tableName(), 'text_internet', $this->string()->after('text_sms'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(Tariffs::tableName(), 'erp_title');
		$this->alterColumn(Tariffs::tableName(), 'recommended_for', $this->string());

		$this->dropColumn(Tariffs::tableName(), 'text_call');
		$this->dropColumn(Tariffs::tableName(), 'text_sms');
		$this->dropColumn(Tariffs::tableName(), 'text_internet');

		return true;
	}
}
