<?php

use yii\db\Migration;

class m170825_131448_MZ_1334_shop_bug_fix extends Migration
{
    public function up()
    {
	    $this->update('shop', ['shop_image_2' => null], ['id' => 106]);
    }
}
