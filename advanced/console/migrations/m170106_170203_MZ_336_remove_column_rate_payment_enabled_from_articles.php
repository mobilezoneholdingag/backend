<?php

use yii\db\Migration;

class m170106_170203_MZ_336_remove_column_rate_payment_enabled_from_articles extends Migration
{
	public function up()
	{
		$this->dropColumn('articles', 'rate_payment_enabled');

		return true;
	}

	public function down()
	{
		$this->addColumn('articles', 'rate_payment_enabled', $this->boolean());

		return true;
	}
}
