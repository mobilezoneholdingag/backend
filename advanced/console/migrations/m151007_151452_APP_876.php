<?php

use yii\db\Schema;
use yii\db\Migration;

class m151007_151452_APP_876 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\LandingPages::tableName(), 'active', Schema::TYPE_BOOLEAN);
		$this->update(\common\models\LandingPages::tableName(), ['active' => true]);
		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\LandingPages::tableName(), 'active');
		return true;
	}

}
