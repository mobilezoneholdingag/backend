<?php

use yii\db\Migration;

class m170419_094816_MZ_871_create_customer_for_acceptance_tests extends Migration
{
	const CUSTOMER_TABLE = 'customer';
	const TESTER_EMAIL = 'tester@deinhandy.de';

	public function up()
	{
		$this->insert(
			self::CUSTOMER_TABLE, [
			'email' => self::TESTER_EMAIL,
			'password_hash' => '$2y$13$Igp4pOMr522v8yIHvGVgPeVQPlnPNVqNqr8XH91YiGKwu0H95vUKO', // = "Wl^Z%mbRCsuZBzIHSr&2em"
			'language' => 'de',
			'nationality' => 'ch',
			'passport_id' => '9230478243',
			'passport_type' => '1',
			'birthday' => '1997-08-29',
			'updated_at' => '2017-04-19 09:47:32',
			'created_at' => '2017-04-19 09:46:27',
			'gender' => '1',
			'first_name' => 'Test',
			'last_name' => 'Tester',
		]);

		return true;
	}

	public function down()
	{
		$this->delete(self::CUSTOMER_TABLE, ['email' => self::TESTER_EMAIL]);

		return true;
	}
}
