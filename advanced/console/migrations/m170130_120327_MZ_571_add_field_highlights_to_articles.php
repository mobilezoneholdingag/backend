<?php

use yii\db\Migration;

class m170130_120327_MZ_571_add_field_highlights_to_articles extends Migration
{
	const TABLE_ARTICLES = 'articles';
	const NEW_FIELD = 'highlights';

	public function up()
	{
		$this->addColumn(self::TABLE_ARTICLES, self::NEW_FIELD, $this->text());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_ARTICLES, self::NEW_FIELD);

		return false;
	}
}
