<?php

use yii\db\Schema;
use yii\db\Migration;
use \common\models\ArticleGroups;
use \common\models\Article;

class m150914_115956_APP_394 extends Migration
{
	public function up()
	{
		$articleGroupsTableName = ArticleGroups::tableName();
		$articleTableName = Article::tableName();

		$this->addColumn($articleGroupsTableName, 'description_1', Schema::TYPE_TEXT);
		$this->addColumn($articleGroupsTableName, 'description_2', Schema::TYPE_TEXT);
		$this->addColumn($articleGroupsTableName, 'description_3', Schema::TYPE_TEXT);
		$this->addColumn($articleGroupsTableName, 'seo_text_1', Schema::TYPE_TEXT);
		$this->addColumn($articleGroupsTableName, 'seo_text_2', Schema::TYPE_TEXT);
		$this->addColumn($articleGroupsTableName, 'seo_text_3', Schema::TYPE_TEXT);
		$this->addColumn($articleGroupsTableName, 'seo_text_4', Schema::TYPE_TEXT);
		$this->addColumn($articleGroupsTableName, 'product_category_id', Schema::TYPE_INTEGER);
		$this->addColumn($articleGroupsTableName, 'short_name', Schema::TYPE_STRING);
		$this->addColumn($articleGroupsTableName, 'os_id', Schema::TYPE_INTEGER);

		$groups = ArticleGroups::find()->all();

		/**
		 * @var ArticleGroups[] $groups
		 */
		foreach ($groups as $group) {
			$topVariation = $group->topVariation;
			if ($topVariation) {
				$this->update($articleGroupsTableName,
					[
						'description_1' => $topVariation->description1,
						'description_2' => $topVariation->description2,
						'description_3' => $topVariation->description3,
						'seo_text_1' => $topVariation->seoblock1,
						'seo_text_2' => $topVariation->seoblock2,
						'seo_text_3' => $topVariation->seoblock3,
						'seo_text_4' => $topVariation->seoblock4,
						'product_category_id' => $topVariation->product_category,
						'short_name' => $topVariation->short_name,
						'os_id' => $topVariation->os_id,
					],
					['id' => $topVariation->group_id]
				);
			}
		}

		$this->dropColumn($articleTableName, 'description1');
		$this->dropColumn($articleTableName, 'description2');
		$this->dropColumn($articleTableName, 'description3');
		$this->dropColumn($articleTableName, 'seoblock1');
		$this->dropColumn($articleTableName, 'seoblock2');
		$this->dropColumn($articleTableName, 'seoblock3');
		$this->dropColumn($articleTableName, 'seoblock4');
		$this->dropForeignKey('articles_ibfk_1', $articleTableName);
		$this->dropColumn($articleTableName, 'product_category');
		$this->dropColumn($articleTableName, 'short_name');
		$this->dropColumn($articleTableName, 'os_id');

		return true;
	}

	public function down()
	{
		$articleGroupsTableName = ArticleGroups::tableName();
		$articleTableName = Article::tableName();

		$this->addColumn($articleTableName, 'description1', Schema::TYPE_TEXT);
		$this->addColumn($articleTableName, 'description2', Schema::TYPE_TEXT);
		$this->addColumn($articleTableName, 'description3', Schema::TYPE_TEXT);
		$this->addColumn($articleTableName, 'seoblock1', Schema::TYPE_TEXT);
		$this->addColumn($articleTableName, 'seoblock2', Schema::TYPE_TEXT);
		$this->addColumn($articleTableName, 'seoblock3', Schema::TYPE_TEXT);
		$this->addColumn($articleTableName, 'seoblock4', Schema::TYPE_TEXT);
		$this->addColumn($articleTableName, 'product_category', Schema::TYPE_INTEGER);
		$this->addForeignKey('articles_ibfk_1', $articleTableName, 'product_category', \common\models\ProductCategory::tableName(), 'id');
		$this->addColumn($articleTableName, 'short_name', Schema::TYPE_STRING);
		$this->addColumn($articleTableName, 'os_id', Schema::TYPE_INTEGER);

		$articles = Article::find()->all();

		/**
		 * @var Article[] $articles
		 */
		foreach ($articles as $article) {
			$group = $article->articleGroup;
			$this->update($articleTableName,
				[
					'description1' => $group->description_1,
					'description2' => $group->description_2,
					'description3' => $group->description_3,
					'seoblock1' => $group->seo_text_1,
					'seoblock2' => $group->seo_text_2,
					'seoblock3' => $group->seo_text_3,
					'seoblock4' => $group->seo_text_4,
					'product_category' => $group->product_category_id,
					'short_name' => $group->short_name,
					'os_id' => $group->os_id,
				],
				['group_id' => $group->id]
			);
		}

		$this->dropColumn($articleGroupsTableName, 'description_1');
		$this->dropColumn($articleGroupsTableName, 'description_2');
		$this->dropColumn($articleGroupsTableName, 'description_3');
		$this->dropColumn($articleGroupsTableName, 'seo_text_1');
		$this->dropColumn($articleGroupsTableName, 'seo_text_2');
		$this->dropColumn($articleGroupsTableName, 'seo_text_3');
		$this->dropColumn($articleGroupsTableName, 'seo_text_4');
		$this->dropColumn($articleGroupsTableName, 'product_category_id');
		$this->dropColumn($articleGroupsTableName, 'short_name');
		$this->dropColumn($articleGroupsTableName, 'os_id');
	}
}
