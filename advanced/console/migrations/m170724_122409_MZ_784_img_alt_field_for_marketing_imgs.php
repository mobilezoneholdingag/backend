<?php

use yii\db\Migration;

class m170724_122409_MZ_784_img_alt_field_for_marketing_imgs extends Migration
{
	const COLUMN = 'alt_text';
	const TABLE = 'marketing_images';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->text()->after('target_url'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}
}
