<?php

use yii\db\Migration;

class m170623_123607_MZ_527_connection_fee_clean_up extends Migration
{
	const TABLE = 'tariffs';
	const COLUMN = 'connection_fee';

	public function up()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}

	public function down()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->string()->after('rate_payment_count'));

		return true;
	}
}
