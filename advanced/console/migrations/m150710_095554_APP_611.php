<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;
use common\models\Provider;

class m150710_095554_APP_611 extends Migration
{
	public function up()
	{
		$this->addColumn(Sales::tableName(), 'old_provider_id', Schema::TYPE_INTEGER);
		$this->addColumn(Sales::tableName(), 'old_phone_number_prefix', Schema::TYPE_STRING);
		$this->addColumn(Sales::tableName(), 'old_phone_number_main', Schema::TYPE_STRING);

		$this->addColumn(Provider::tableName(), 'eam_id', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Sales::tableName(), 'old_provider_id');
		$this->dropColumn(Sales::tableName(), 'old_phone_number_prefix');
		$this->dropColumn(Sales::tableName(), 'old_phone_number_main');

		$this->dropColumn(Provider::tableName(), 'eam_id');

		return true;
	}

}
