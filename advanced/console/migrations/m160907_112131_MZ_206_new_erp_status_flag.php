<?php

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Manufacturer;
use common\models\Tariffs;
use yii\db\Migration;

class m160907_112131_MZ_206_new_erp_status_flag extends Migration
{
	public function up()
	{
		$this->addColumn(Tariffs::tableName(), 'is_active_erp', $this->boolean()->after('is_active'));
		$this->addColumn(Manufacturer::tableName(), 'is_active_erp', $this->boolean()->after('is_active'));
		$this->addColumn(Article::tableName(), 'is_active_erp', $this->boolean()->after('is_active'));
		$this->addColumn(ArticleTariffs::tableName(), 'is_active_erp', $this->boolean()->after('is_active'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(Tariffs::tableName(), 'is_active_erp');
		$this->dropColumn(Manufacturer::tableName(), 'is_active_erp');
		$this->dropColumn(Article::tableName(), 'is_active_erp');
		$this->dropColumn(ArticleTariffs::tableName(), 'is_active_erp');

		return true;
	}
}
