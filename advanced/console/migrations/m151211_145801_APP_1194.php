<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_145801_APP_1194 extends Migration
{
	public function up()
	{
		$this->addColumn(\backend\models\ArticleTariffsKeepList::tableName(), 'note', Schema::TYPE_TEXT);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\backend\models\ArticleTariffsKeepList::tableName(), 'note');

		return true;
	}
}
