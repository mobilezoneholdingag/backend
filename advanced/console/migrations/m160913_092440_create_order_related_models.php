<?php

use common\models\Customer;
use yii\db\Migration;

/**
 * Handles the creation for table `order_related_models`.
 */
class m160913_092440_create_order_related_models extends Migration
{
	const ORDER_TABLE_NAME = 'order';
	const ORDER_ITEM_TABLE_NAME = 'order_item';
	const ORDER_ADDRESS_TABLE_NAME = 'order_address';

	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable(
			self::ORDER_TABLE_NAME,
			[
				'id' => $this->primaryKey()->unsigned(),
				'customer_id' => $this->integer()->notNull(),
				'delivery_address_id' => $this->integer()->notNull(),
				'billing_address_id' => $this->integer()->notNull(),
				'price_once' => $this->decimal(8, 2),
				'price_monthly' => $this->decimal(8, 2),
				'vat_rate' => $this->decimal(4, 2),
				'status' => $this->integer()->notNull(),
				'created_at' => $this->dateTime()->notNull(),
				'updated_at' => $this->dateTime()->notNull(),
			]
		);

		$this->createTable(
			self::ORDER_ITEM_TABLE_NAME,
			[
				'id' => $this->primaryKey()->unsigned(),
				'order_id' => $this->integer()->unsigned(),
				'item_id' => $this->integer()->notNull(),
				'item_product_type_id' => $this->string(50)->notNull(),
				'parent_item_id' => $this->integer()->unsigned(), // for insurances
				'title' => $this->string()->notNull(),
				'price_once' => $this->decimal(8, 2),
				'price_monthly' => $this->decimal(8, 2),
				'vat_rate' => $this->decimal(4, 2),
				'created_at' => $this->dateTime()->notNull(),
				'updated_at' => $this->dateTime()->notNull(),
			]
		);

		$this->createTable(
			self::ORDER_ADDRESS_TABLE_NAME,
			[
				'id' => $this->primaryKey()->unsigned(),
				'customer_id' => $this->integer()->notNull(),

				//@todo: rename --> only copied from customer
				'guid' => $this->integer(),
				'sex' => $this->integer(1),
				'firstname' => $this->string(),
				'lastname' => $this->string(),
				'company' => $this->string(),
				'street' => $this->string(),
				'house_number' => $this->string(),
				'zip' => $this->string(),
				'city' => $this->string(),
				'country' => $this->string(),
				'contact_phone_number' => $this->integer(),
				'additional_info' => $this->string(),
				'created_at' => $this->dateTime()->notNull(),
				'updated_at' => $this->dateTime()->notNull(),
			]
		);

		$this->addForeignKey(
			'fk_order_id',
			self::ORDER_ITEM_TABLE_NAME,
			'order_id',
			self::ORDER_TABLE_NAME,
			'id',
			null,
			'cascade'
		);
		$this->addForeignKey(
			'fk_parent_order_item_id',
			self::ORDER_ITEM_TABLE_NAME,
			'parent_item_id',
			self::ORDER_ITEM_TABLE_NAME,
			'id'
		);
		$this->addForeignKey(
			'fk_customer_order_address_customer_id',
			self::ORDER_ADDRESS_TABLE_NAME,
			'customer_id',
			Customer::tableName(),
			'id',
			null,
			'cascade'
		);

		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropTable(self::ORDER_ADDRESS_TABLE_NAME);
		$this->dropTable(self::ORDER_ITEM_TABLE_NAME);
		$this->dropTable(self::ORDER_TABLE_NAME);

		return true;
	}
}
