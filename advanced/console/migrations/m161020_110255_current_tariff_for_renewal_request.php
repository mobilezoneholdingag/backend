<?php

use yii\db\Migration;

class m161020_110255_current_tariff_for_renewal_request extends Migration
{
	const TABLE = 'renewal_requests';

	public function up()
	{
		$this->addColumn(self::TABLE, 'current_tariff_id', $this->integer());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, 'current_tariff_id');

		return true;
	}
}
