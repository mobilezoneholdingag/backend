<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Provider;

class m150415_074800_APP_335 extends Migration
{
	public function up()
	{
		$this->addColumn(Provider::tableName(),'set_id', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Provider::tableName(),'set_id');

		return true;
	}

}
