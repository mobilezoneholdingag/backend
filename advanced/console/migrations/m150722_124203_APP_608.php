<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;

class m150722_124203_APP_608 extends Migration
{
	public function up()
	{
		$this->addColumn(Sales::tableName(), 'passport_creation_date', Schema::TYPE_DATE);
		$this->addColumn(Sales::tableName(), 'bank_name', Schema::TYPE_STRING);
		$this->addColumn(Sales::tableName(), 'bank_bic', Schema::TYPE_STRING);
		$this->addColumn(Sales::tableName(), 'confirmation_robinson', Schema::TYPE_BOOLEAN);
		$this->addColumn(Sales::tableName(), 'article_tariff_external_id', Schema::TYPE_STRING);
		$this->addColumn(Sales::tableName(), 'password', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Sales::tableName(), 'passport_creation_date');
		$this->dropColumn(Sales::tableName(), 'bank_name');
		$this->dropColumn(Sales::tableName(), 'bank_bic');
		$this->dropColumn(Sales::tableName(), 'confirmation_robinson');
		$this->dropColumn(Sales::tableName(), 'article_tariff_external_id');
		$this->dropColumn(Sales::tableName(), 'password');

		return true;
	}
}
