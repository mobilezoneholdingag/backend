<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_143335_APP_1074 extends Migration
{
	public function up()
	{
		$this->dropTable('additions');
		$this->dropTable('banner');
		$this->dropTable('banner_sets');
		$this->dropTable('basket');
		$this->dropTable('bestseller');
		$this->dropTable('campaign_codes');
		$this->dropTable('footnotes');
		$this->dropTable('startpage_offers');
		$this->dropTable('tarif_elemente');
		$this->dropTable('tariff_promotion');
		$this->dropTable('texte');
		$this->dropTable('user_online');
	}

	public function down()
	{
		echo "m151116_143335_APP_1074 cannot be reverted.\n";

		return false;
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}


	public function safeDown()
	{
	}
	*/
}
