<?php

use yii\db\Schema;
use yii\db\Migration;

class m180700_092842_MZ_1589_general_news extends Migration
{
    public function up()
    {
        $tableName = Yii::$app->db->tablePrefix . 'general_news';
        if (Yii::$app->db->getTableSchema($tableName, true) === null) {
            $this->createTable(
                'general_news',
                [
                    'id' => Schema::TYPE_PK,
                    'date' => Schema::TYPE_INTEGER,
                    'name' => Schema::TYPE_STRING,
                    'link' => Schema::TYPE_STRING
                ]
            );
        }
    }

    public function down()
    {
        $this->dropTable('general_news');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
