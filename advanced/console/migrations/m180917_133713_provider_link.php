<?php

use yii\db\Migration;

class m180917_133713_provider_link extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\Provider::tableName(), 'link', 'text');
    }

    public function down()
    {
        $this->dropColumn(\common\models\Provider::tableName(), 'link', 'text');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
