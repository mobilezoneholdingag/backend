<?php

use yii\db\Schema;
use yii\db\Migration;

class m151130_163530_APP_1181 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Sales::tableName(), 'manually_submitted', SCHEMA::TYPE_BOOLEAN);
	}

	public function down()
	{
		$this->dropColumn(\common\models\Sales::tableName(), 'manually_submitted');
	}
}
