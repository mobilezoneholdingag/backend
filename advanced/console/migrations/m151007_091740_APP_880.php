<?php

use yii\db\Schema;
use yii\db\Migration;

class m151007_091740_APP_880 extends Migration
{
	public function up()
	{
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_type', Schema::TYPE_INTEGER);
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_id', Schema::TYPE_STRING);
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_city', Schema::TYPE_STRING);
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_pob', Schema::TYPE_STRING);
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_valid_until', Schema::TYPE_DATE);
	}

	public function down()
	{
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_type', 'INTEGER(11) NOT NULL');
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_id', 'VARCHAR(255) NOT NULL');
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_city', 'VARCHAR(255) NOT NULL');
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_pob', 'VARCHAR(255) NOT NULL');
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_valid_until', 'DATE NOT NULL');
	}
}
