<?php

use yii\db\Migration;

class m170110_214836_MZ_464_phone_numbers_to_string extends Migration
{
    public function up()
    {
	    $this->alterColumn('customer_phone_number', 'number', $this->string());
	    $this->alterColumn('order_address', 'contact_phone_number', $this->string());

	    return true;
    }

    public function down()
    {
	    $this->alterColumn('customer_phone_number', 'number', $this->integer(25));
	    $this->alterColumn('order_address', 'contact_phone_number', $this->integer(11));

	    return true;
    }
}
