<?php

use yii\db\Migration;

class m160525_134445_APP_2416_url_shortener extends Migration
{
	public function up()
	{
		$this->createTable('short_urls', [
			'id' => $this->primaryKey(),
			'code' => $this->string()->notNull()->unique(),
			'url' => $this->string(2048)->notNull(),
		]);

		return true;
	}

	public function down()
	{
		$this->dropTable('short_urls');

		return true;
	}

}
