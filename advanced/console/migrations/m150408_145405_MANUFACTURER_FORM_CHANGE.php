<?php

use yii\db\Schema;
use yii\db\Migration;

class m150408_145405_MANUFACTURER_FORM_CHANGE extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Manufacturer::tableName(), 'is_active', Schema::TYPE_BOOLEAN);
		$this->update(\common\models\Manufacturer::tableName(), ['is_active' => 1]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Manufacturer::tableName(), 'is_active');

		return true;
	}
}
