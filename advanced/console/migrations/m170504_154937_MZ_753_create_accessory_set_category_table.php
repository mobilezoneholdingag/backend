<?php

use yii\db\Migration;

class m170504_154937_MZ_753_create_accessory_set_category_table extends Migration
{
	const ACCESSORY_CATEGORY_SET_TABLE = 'accessory_category_set';
	const ACCESSORY_CATEGORY_TABLE = 'accessory_category';
	const SET_TABLE = 'sets';
	const ACCESSORY_CATEGORIES = [
		'protection' => [8, 9, 10, 11],
		'charge' => [12, 19, 70, 13, 14],
		'audio' => [15, 16, 17],
		'misc' => [18, 20, 69],
		'car' => [21, 13, 86],
	];

	public function up()
	{
		$this->createTable(self::ACCESSORY_CATEGORY_SET_TABLE, [
			'accessory_category_id' => $this->integer(),
			'set_id' => $this->integer(),
		]);

		$this->createTable(self::ACCESSORY_CATEGORY_TABLE, [
			'id' => $this->primaryKey(),
			'title' => $this->string(255),
		]);

		$this->addForeignKey(
			'fk_accessory_categories_accessory_category_sets',
			self::ACCESSORY_CATEGORY_SET_TABLE,
			'accessory_category_id',
			self::ACCESSORY_CATEGORY_TABLE,
			'id',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_sets_accessory_categories',
			self::ACCESSORY_CATEGORY_SET_TABLE,
			'set_id',
			self::SET_TABLE,
			'id',
			'CASCADE'
		);

		foreach (self::ACCESSORY_CATEGORIES as $categoryTitle => $setIds) {
			$this->insert(self::ACCESSORY_CATEGORY_TABLE, ['title' => $categoryTitle]);
			$lastInsertedId = Yii::$app->db->lastInsertID;

			foreach ($setIds as $setId) {
				$this->insert(self::ACCESSORY_CATEGORY_SET_TABLE, [
					'set_id' => $setId,
					'accessory_category_id' => $lastInsertedId,
				]);
			}
		}

		return true;
	}

	public function down()
	{
		$this->dropForeignKey('fk_accessory_categories_accessory_category_sets', self::ACCESSORY_CATEGORY_SET_TABLE);
		$this->dropForeignKey('fk_sets_accessory_categories', self::ACCESSORY_CATEGORY_SET_TABLE);
		$this->dropTable(self::ACCESSORY_CATEGORY_SET_TABLE);
		$this->dropTable(self::ACCESSORY_CATEGORY_TABLE);

		return true;
	}

}
