<?php

use yii\db\Schema;
use yii\db\Migration;

class m150611_124708_APP_541 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\ArticleGroups::tableName(),'footnote_text', Schema::TYPE_TEXT);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\ArticleGroups::tableName(),'footnote_text');

		return true;
	}
}
