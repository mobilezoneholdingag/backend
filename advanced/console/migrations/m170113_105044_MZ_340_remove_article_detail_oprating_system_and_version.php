<?php

use yii\db\Migration;

class m170113_105044_MZ_340_remove_article_detail_oprating_system_and_version extends Migration
{
	const TABLE_ARTICLE_DETAILS = 'article_details';

	public function up()
	{
		$this->update(
			self::TABLE_ARTICLE_DETAILS,
			['detail_name' => '', 'sort_id' => 0, 'group_id' => 0, 'highlight' => 0],
			['in', 'detail_id', [59, 60]]
		);

		return true;
	}

	public function down()
	{
		$this->update(
			self::TABLE_ARTICLE_DETAILS,
			['detail_name' => 'Betriebssystem', 'sort_id' => 91, 'group_id' => 10, 'highlight' => 1],
			['detail_id' => 59]
		);
		$this->update(
			self::TABLE_ARTICLE_DETAILS,
			['detail_name' => 'Version', 'sort_id' => 92, 'group_id' => 10, 'highlight' => 0],
			['detail_id' => 60]
		);

		return true;
	}
}
