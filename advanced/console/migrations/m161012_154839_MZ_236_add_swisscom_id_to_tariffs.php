<?php

use yii\db\Migration;

class m161012_154839_MZ_236_add_swisscom_id_to_tariffs extends Migration
{
	const TABLE = 'tariffs';

	public function up()
	{
		$this->addColumn(self::TABLE, 'swisscom_id', $this->string()->after('offer_type_id'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, 'swisscom_id');

		return true;
	}
}
