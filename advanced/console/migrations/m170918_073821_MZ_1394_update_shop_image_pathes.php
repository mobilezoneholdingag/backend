<?php

use yii\db\Migration;

class m170918_073821_MZ_1394_update_shop_image_pathes extends Migration
{

    public function up()
    {
    	$this->execute("UPDATE shop SET manager_image = REPLACE(manager_image, 'deinhandy-dev', 'mobilezone') WHERE manager_image LIKE '%deinhandy-dev%'");
    	$this->execute("UPDATE shop SET shop_image_1 = REPLACE(shop_image_1, 'deinhandy-dev', 'mobilezone') WHERE shop_image_1 LIKE '%deinhandy-dev%'");
    	$this->execute("UPDATE shop SET shop_image_2 = REPLACE(shop_image_2, 'deinhandy-dev', 'mobilezone') WHERE shop_image_2 LIKE '%deinhandy-dev%'");
    	$this->execute("UPDATE shop SET shop_image_3 = REPLACE(shop_image_3, 'deinhandy-dev', 'mobilezone') WHERE shop_image_3 LIKE '%deinhandy-dev%'");

    	return true;
    }

    public function down()
    {
	    $this->execute("UPDATE shop SET manager_image = REPLACE(manager_image, 'mobilezone', 'deinhandy-dev') WHERE manager_image LIKE '%mobilezone%'");
	    $this->execute("UPDATE shop SET shop_image_1 = REPLACE(shop_image_1, 'mobilezone', 'deinhandy-dev') WHERE shop_image_1 LIKE '%mobilezone%'");
	    $this->execute("UPDATE shop SET shop_image_2 = REPLACE(shop_image_2, 'mobilezone', 'deinhandy-dev') WHERE shop_image_2 LIKE '%mobilezone%'");
	    $this->execute("UPDATE shop SET shop_image_3 = REPLACE(shop_image_3, 'mobilezone', 'deinhandy-dev') WHERE shop_image_3 LIKE '%mobilezone%'");

        return true;
    }


}
