<?php

use yii\db\Migration;

class m170811_144038_MZ_1305_add_column_sim_only_tariff_to_tariff_table extends Migration
{
	const TARIFFS_TABLE = 'tariffs';
	const SIM_ONLY_TARIFF_ID_COLUMN = 'sim_only_tariff_id';

	public function up()
	{
		$this->addColumn(self::TARIFFS_TABLE, self::SIM_ONLY_TARIFF_ID_COLUMN, $this->integer()->unsigned());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TARIFFS_TABLE, self::SIM_ONLY_TARIFF_ID_COLUMN);

		return true;
	}
}
