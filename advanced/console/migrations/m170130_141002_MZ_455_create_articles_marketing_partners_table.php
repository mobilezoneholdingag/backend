<?php

use yii\db\Migration;

class m170130_141002_MZ_455_create_articles_marketing_partners_table extends Migration
{
	const TABLE_NAME = 'articles_marketing_partners';

	public function up()
	{
		$this->createTable(
			self::TABLE_NAME,
			[
				'article_id' => $this->integer(),
				'partner_id' => $this->integer(),
			]
		);

		$this->addPrimaryKey('pk_article_partner_id', self::TABLE_NAME, ['article_id', 'partner_id']);

		$this->addForeignKey(
			'fk_article_id',
			self::TABLE_NAME,
			'article_id',
			'articles',
			'id',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_partner_id',
			self::TABLE_NAME,
			'partner_id',
			'marketing_partners',
			'id',
			'CASCADE'
		);

		$this->execute(
			'
			INSERT INTO articles_marketing_partners
			SELECT a.id, mp.id
			FROM articles a
			JOIN marketing_partners mp
			WHERE a.is_active_erp = 1
			AND a.is_active = 1
		'
		);
	}

	public function down()
	{
		$this->dropTable(self::TABLE_NAME);
	}
}
