<?php

use yii\db\Schema;
use yii\db\Migration;

class m150903_103954_APP_798 extends Migration
{
	public function up()
	{
		$this->addColumn(\frontend\models\Newsletter::tableName(), 'bid', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\frontend\models\Newsletter::tableName(), 'bid');

		return true;
	}

}
