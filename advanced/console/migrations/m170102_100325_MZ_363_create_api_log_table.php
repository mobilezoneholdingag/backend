<?php

use yii\db\Migration;

class m170102_100325_MZ_363_create_api_log_table extends Migration
{
	const API_LOG_TABLE = 'api_log_swissbilling';

	public function up()
    {
		$this->createTable(
			self::API_LOG_TABLE,
			[
				'id' => $this->primaryKey()->unsigned(),
				'created_at' => $this->dateTime()->notNull(),
				'url' => $this->string(500),
				'method' => $this->string(255),
				'request' => $this->text(),
				'response_headers' => $this->text(),
				'response' => $this->text(),
				'execution_time' => $this->integer(),
			]
		);
    }

    public function down()
    {
        $this->dropTable(self::API_LOG_TABLE);
    }
}
