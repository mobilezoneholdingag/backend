<?php

use yii\db\Migration;

class m170523_153521_MZ_788_remove_mfgroup_tables extends Migration
{
	const TABLES = [
		'mfgroup_card_number_request',
		'mfgroup_card_number_response',
		'mfgroup_confirmation_request',
		'mfgroup_confirmation_response',
		'mfgroup_financial_request',
		'mfgroup_financial_response',
	];

	const FOREIGN_KEYS = [
		'mfgroup_card_number_response' => ['fk_mfgroup_card_number_response_mfgroup_card_number_request'],
		'mfgroup_confirmation_request' => [
			'fk_mfgroup_confirmation_request_mfgroup_financial_request1',
			'fk_mfgroup_confirmation_request_mfgroup_financial_response1'
		],
		'mfgroup_confirmation_response' => ['fk_mfgroup_confirmation_response_mfgroup_confirmation_request1'],
		'mfgroup_financial_request' => ['fk_mfgroup_financial_request_mfgroup_card_number_response1'],
		'mfgroup_financial_response' => ['fk_mfgroup_financial_response_mfgroup_financial_request1'],
	];

	public function up()
	{
		foreach (static::FOREIGN_KEYS as $tableName => $foreignKeys) {
			foreach ($foreignKeys as $foreignKey) {
				$this->dropForeignKey($foreignKey, $tableName);
			}
		}

		foreach (static::TABLES as $tableName) {
			$this->dropTable($tableName);
		}

		return true;
	}

	public function down()
	{
		echo "m170523_153521_MZ_788_remove_mfgroup_tables cannot be reverted.\n";

		return false;
	}
}
