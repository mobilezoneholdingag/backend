<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m160707_092610_MZ_12_translation_tables
 */
class m160707_092610_MZ_12_translation_tables extends Migration
{
    /**
     * @var array The language table contains the list of languages.
     */
    protected $languages = [
        ['de-DE', 'de', 'de', 'Deutsch', 'German', 0],
        ['de-CH', 'ch', 'ch', 'Deutsch/Schweiz', 'Swiss', 1],
        ['fr-FR', 'fr', 'fr', 'Français (France)', 'French (France)', 1],
        ['it-IT', 'it', 'it', 'Italiano', 'Italian', 1],
    ];

	/**
     * This method contains the logic to be executed when applying this migration.
     * This method differs from [[up()]] in that the DB logic implemented here will
     * be enclosed within a DB transaction.
     * Child classes may implement this method instead of [[up()]] if the DB logic
     * needs to be within a transaction.
     * @return boolean return a false value to indicate the migration fails
     * and should not proceed further. All other return values mean the migration succeeds.
     */
    public function safeUp() {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%language}}', [
            'language_id' => Schema::TYPE_STRING . '(5) NOT NULL',
            'language' => Schema::TYPE_STRING . '(3) NOT NULL',
            'country' => Schema::TYPE_STRING . '(3) NOT NULL',
            'name' => Schema::TYPE_STRING . '(32) NOT NULL',
            'name_ascii' => Schema::TYPE_STRING . '(32) NOT NULL',
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL',
            'PRIMARY KEY (language_id)'
        ], $tableOptions);

        $this->batchInsert('{{%language}}', [
            'language_id',
            'language',
            'country',
            'name',
            'name_ascii',
            'status'
        ], $this->languages);

        $this->createTable('{{%language_source}}', [
            'id' => Schema::TYPE_PK,
            'category' => Schema::TYPE_STRING . '(32) DEFAULT NULL',
            'message' => Schema::TYPE_TEXT
        ], $tableOptions);

        $this->createTable('{{%language_translate}}', [
            'id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(5) NOT NULL',
            'translation' => Schema::TYPE_TEXT,
            'PRIMARY KEY (id, language)'
        ], $tableOptions);

        $this->createIndex('language_translate_idx_language', '{{%language_translate}}', 'language');

        $this->addForeignKey('language_translate_ibfk_1', '{{%language_translate}}', ['language'], '{{%language}}', ['language_id'], 'CASCADE', 'CASCADE');
        $this->addForeignKey('language_translate_ibfk_2', '{{%language_translate}}', ['id'], '{{%language_source}}', ['id'], 'CASCADE', 'CASCADE');
    }

	/**
     * This method contains the logic to be executed when removing this migration.
     * This method differs from [[down()]] in that the DB logic implemented here will
     * be enclosed within a DB transaction.
     * Child classes may implement this method instead of [[up()]] if the DB logic
     * needs to be within a transaction.
     * @return boolean return a false value to indicate the migration fails
     * and should not proceed further. All other return values mean the migration succeeds.
     */
    public function safeDown() {
        $this->dropTable('{{%language_translate}}');
        $this->dropTable('{{%language_source}}');
        $this->dropTable('{{%language}}');
    }
}
