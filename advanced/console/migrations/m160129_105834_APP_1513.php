<?php

use yii\db\Schema;
use yii\db\Migration;

class m160129_105834_APP_1513 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Article::tableName(), 'image_overlay_text', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Article::tableName(), 'image_overlay_text');

		return true;
	}
}
