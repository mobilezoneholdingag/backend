<?php

use common\modules\mfgroup\models\FinancialRequest;
use yii\db\Migration;

class m160915_131826_MZ_233_add_order_id_to_mfgroup_financial_request extends Migration
{
	public function up()
	{
		$this->addColumn(
			FinancialRequest::tableName(),
			'order_id',
			$this->integer()->notNull()->after('card_number_response_id')
		);

		return true;
	}

	public function down()
	{
		$this->dropColumn(FinancialRequest::tableName(), 'order_id');

		return true;
	}
}
