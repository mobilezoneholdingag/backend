<?php

use common\models\Article;
use common\models\Stock;
use yii\db\Migration;

class m160714_141335_MZ_133_create_stock_table extends Migration
{
	public function up()
	{
		$this->createTable(Stock::tableName(), [
			'id' => $this->primaryKey(),
			'quantity' => $this->integer()->notNull()->defaultValue(0),
			'next_delivery_date' => $this->date(),
		]);

		// Intentionally not added a foreign key, as we most likely will
		// add inventories before inserting/updating articles
	}

	public function down()
	{
		$this->dropTable('stock');
	}
}
