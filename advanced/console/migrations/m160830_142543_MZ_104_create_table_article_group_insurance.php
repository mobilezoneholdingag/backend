<?php

use common\models\Article;
use common\models\ArticleGroups;
use yii\db\Migration;

/**
 * Class m160830_142543_MZ_104_create_table_insurance_relation
 */
class m160830_142543_MZ_104_create_table_article_group_insurance extends Migration
{
	const TABLE_NAME = 'article_groups_insurance';
	const TABLE_INDEX = 'idx_insurance_id';

	/**
	 * Create table for insurance - article - group dependencies.
	 *
	 * @return bool
	 */
	public function up()
	{
		$this->createTable(
			self::TABLE_NAME,
			[
				'id' => $this->primaryKey(9),
				'insurance_id' => $this->integer(9)->notNull(),
				'group_id' => $this->integer(11)->notNull(false),
			]
		);

		$this->createIndex(self::TABLE_INDEX, self::TABLE_NAME, 'insurance_id');
		$this->addForeignKey('i_id_a_id', self::TABLE_NAME, 'insurance_id', Article::tableName(), 'id');
		$this->addForeignKey('g_id_g_id', self::TABLE_NAME, 'group_id', ArticleGroups::tableName(), 'id');

		return true;
	}

	/**
	 * Remove insurance table & index.
	 *
	 * @return bool
	 */
	public function down()
	{
		$this->dropTable(self::TABLE_NAME);

		return true;
	}
}
