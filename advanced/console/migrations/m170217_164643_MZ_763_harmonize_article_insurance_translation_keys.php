<?php

use yii\db\Migration;

class m170217_164643_MZ_763_harmonize_article_insurance_translation_keys extends Migration
{
	const TABLE = 'language_source';

	public function up()
	{
		$search = "common\\\\\\\\modules\\\\\\\\insurances\\\\\\\\models\\\\\\\\ArticleInsurance";
		$needle = "common\\\\modules\\\\insurances\\\\models\\\\ArticleInsurance";
		$replace = "common\\\\models\\\\Article";

		$connection = Yii::$app->getDb();
		$command = $connection->createCommand(
			"UPDATE " . self::TABLE .
			" SET message = REPLACE(message, '$needle', '$replace') ".
			" WHERE message LIKE '%$search%'"
		);

		$command->execute();

		return true;
	}

	public function down()
	{
		return false;
	}
}
