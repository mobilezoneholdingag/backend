<?php

use common\modules\order\models\Order;
use yii\db\Migration;

class m160914_114039_MZ_233_create_order_status_table extends Migration
{
	const TABLE = 'order_status';

	public function up()
	{
		$this->createTable(
			self::TABLE,
			[
				'id' => $this->primaryKey(),
				'code' => $this->string(),
			]
		);

		$this->insert(self::TABLE, ['id' => 1, 'code' => 'new']);
		$this->insert(self::TABLE, ['id' => 2, 'code' => 'received']);
		$this->insert(self::TABLE, ['id' => 3, 'code' => 'on_hold']);
		$this->insert(self::TABLE, ['id' => 4, 'code' => 'pending_psp_capture']);
		$this->insert(self::TABLE, ['id' => 5, 'code' => 'payment_successful']);
		$this->insert(self::TABLE, ['id' => 6, 'code' => 'payment_incomplete']);
		$this->insert(self::TABLE, ['id' => 7, 'code' => 'declined']);
		$this->insert(self::TABLE, ['id' => 8, 'code' => 'payment_failed']);
		$this->insert(self::TABLE, ['id' => 9, 'code' => 'shipped']);

		$this->addForeignKey('fk_status_id_of_order', Order::tableName(), 'status', self::TABLE, 'id');

		return true;
	}

	public function down()
	{
		$this->dropForeignKey('fk_status_id_of_order', Order::tableName());
		$this->dropTable(self::TABLE);

		return true;
	}
}
