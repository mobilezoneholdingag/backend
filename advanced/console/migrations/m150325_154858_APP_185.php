<?php

use yii\db\Schema;
use yii\db\Migration;

class m150325_154858_APP_185 extends Migration
{
	public function up()
	{
		$this->createTable('sales_unapproved', [
			'id' => Schema::TYPE_PK,

			'verivox_id' => Schema::TYPE_INTEGER,
			'verivox_transaction_id' => Schema::TYPE_STRING,

			'article_id' => Schema::TYPE_INTEGER,
			'article_name' => Schema::TYPE_STRING,
			'tariff_id' => Schema::TYPE_INTEGER,
			'tariff_name' => Schema::TYPE_STRING,

			'insurance' => Schema::TYPE_BOOLEAN,
			'customer_gender' => Schema::TYPE_INTEGER,
			'customer_prename' => Schema::TYPE_STRING,
			'customer_surname' => Schema::TYPE_STRING,
			'customer_street' => Schema::TYPE_STRING,
			'customer_streetnumber' => Schema::TYPE_STRING,
			'customer_zip' => Schema::TYPE_STRING,
			'customer_city' => Schema::TYPE_STRING,
			'customer_telephone' => Schema::TYPE_STRING,
			'customer_email' => Schema::TYPE_STRING,
			'delivery_address' => Schema::TYPE_BOOLEAN,
			'delivery_gender' => Schema::TYPE_STRING,
			'delivery_prename' => Schema::TYPE_STRING,
			'delivery_surname' => Schema::TYPE_STRING,
			'delivery_company' => Schema::TYPE_STRING,
			'delivery_street' => Schema::TYPE_STRING,
			'delivery_streetnumber' => Schema::TYPE_STRING,
			'delivery_zipcode' => Schema::TYPE_STRING,
			'delivery_city' => Schema::TYPE_STRING,
			'coupon_id' => Schema::TYPE_INTEGER,
			'tariff_device_price' => Schema::TYPE_FLOAT,
			'passport_type' => Schema::TYPE_INTEGER,
			'passport_type_name' => Schema::TYPE_STRING,
			'passport_id' => Schema::TYPE_STRING,
			'passport_pob' => Schema::TYPE_STRING,
			'passport_city' => Schema::TYPE_STRING,
			'passport_country' => Schema::TYPE_STRING,
			'passport_dob' => Schema::TYPE_DATE,
			'passport_valid_until' => Schema::TYPE_DATE,
			'passport_nationality' => Schema::TYPE_STRING,
			'passport_nationality_name' => Schema::TYPE_STRING,
			'passport_legalstatus' => Schema::TYPE_INTEGER,
			'passport_legaluntil' => Schema::TYPE_STRING,
			'bank_type' => Schema::TYPE_INTEGER,
			'bank_account_number' => Schema::TYPE_STRING,
			'bank_bank_number' => Schema::TYPE_STRING,
			'bank_iban' => Schema::TYPE_STRING,
			'bank_ec_number' => Schema::TYPE_STRING,
			'bank_ec_valid_until' => Schema::TYPE_DATE,
			'payment_type' => Schema::TYPE_INTEGER,
			'confirmation_tos' => Schema::TYPE_BOOLEAN,
			'confirmation_disclaimer' => Schema::TYPE_BOOLEAN,
			'confirmation_solvency' => Schema::TYPE_BOOLEAN,
			'confirmation_insurance' => Schema::TYPE_BOOLEAN,
			'timestamp_sale' => Schema::TYPE_DATETIME,
			'mail_sent' => Schema::TYPE_INTEGER,
			'order_id_eam' => Schema::TYPE_INTEGER,
			'payment_link' => Schema::TYPE_STRING,
		]);

		$this->createIndex('verivox_id', 'sales_unapproved', 'verivox_id', true);

		return true;
	}

	public function down()
	{
		$this->dropTable('sales_unapproved');

		return true;
	}
}
