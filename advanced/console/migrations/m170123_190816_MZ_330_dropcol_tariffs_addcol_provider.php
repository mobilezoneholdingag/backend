<?php

use yii\db\Migration;

class m170123_190816_MZ_330_dropcol_tariffs_addcol_provider extends Migration
{
	const OLD_TABLE_NAME = 'tariffs';
	const NEW_TABLE_NAME = 'provider';
	const COLUMN_NAME = 'pricing_strategy';

	/**
	 * @return bool
	 */
	public function up()
	{
		$this->dropColumn(self::OLD_TABLE_NAME, self::COLUMN_NAME);

		$this->addColumn(self::NEW_TABLE_NAME, self::COLUMN_NAME, $this->string(50)->defaultValue('default'));
		$this->update(self::NEW_TABLE_NAME, [self::COLUMN_NAME => 'swisscom'], ['id' => 2]);
		$this->update(self::NEW_TABLE_NAME, [self::COLUMN_NAME => 'sunrise'], ['id' => 1]);
		$this->update(self::NEW_TABLE_NAME, [self::COLUMN_NAME => 'upc'], ['id' => 13]);
		$this->update(self::NEW_TABLE_NAME, [self::COLUMN_NAME => 'talktalk'], ['id' => 20]);

		return true;
	}

	/**
	 * @return bool
	 */
	public function down()
	{
		$this->dropColumn(self::NEW_TABLE_NAME, self::COLUMN_NAME);
		$this->addColumn(self::OLD_TABLE_NAME, self::COLUMN_NAME, $this->string(50));

		return true;
	}
}
