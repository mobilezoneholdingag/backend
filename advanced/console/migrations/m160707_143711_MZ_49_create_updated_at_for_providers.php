<?php

use common\models\Provider;
use yii\db\Migration;
use yii\db\Schema;

class m160707_143711_MZ_49_create_updated_at_for_providers extends Migration
{
	public function up()
	{
		$this->addColumn(Provider::tableName(), 'api_updated_at', Schema::TYPE_DATETIME);
	}

	public function down()
	{
		$this->dropColumn(Provider::tableName(), 'api_updated_at');
	}
}
