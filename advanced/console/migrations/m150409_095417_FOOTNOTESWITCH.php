<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Tariffs;

class m150409_095417_FOOTNOTESWITCH extends Migration
{
	public function up()
	{
		$this->addColumn(Tariffs::tableName(),'footnote_text', Schema::TYPE_TEXT);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Tariffs::tableName(),'footnote_text');

		return true;
	}
}
