<?php

use yii\db\Schema;
use yii\db\Migration;

class m150715_133625_APP_646 extends Migration
{
	public function up()
	{
		$this->createTable('contest_tuv', [
			'id' => Schema::TYPE_PK,
			'sale_id' => Schema::TYPE_INTEGER,
			'hash' => Schema::TYPE_STRING,
		]);

		$this->createIndex('sale_id', 'contest_tuv', 'sale_id');
	}

	public function down()
	{
		$this->dropTable('contest_tuv');
	}
}
