<?php

use yii\db\Migration;

class m170126_153715_MZ_337_alter_table_order_add_column_contract_file_name extends Migration
{

	const TABLE_NAME = "order";
	const NEW_COL_NAME = "contract_file_name";

	public function up(): bool
	{
		$this->addColumn(self::TABLE_NAME, self::NEW_COL_NAME, $this->string());

		return true;
	}

	public function down(): bool
	{
		$this->dropColumn(self::TABLE_NAME, self::NEW_COL_NAME);

		return true;
	}
}
