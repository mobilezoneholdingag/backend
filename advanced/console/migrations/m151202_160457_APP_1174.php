<?php

use yii\db\Schema;
use yii\db\Migration;

class m151202_160457_APP_1174 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\OrderStatus::tableName(), 'rejected_order', Schema::TYPE_BOOLEAN);
		$this->update(\common\models\OrderStatus::tableName(), ['rejected_order' => 0]);
		$this->update(\common\models\OrderStatus::tableName(), ['rejected_order' => 1], ['in', 'code', [4, 104]]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\OrderStatus::tableName(), 'rejected_order');

		return true;
	}
}
