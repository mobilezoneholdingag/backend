<?php

use yii\db\Migration;
use yii\db\Schema;

class m160215_122215_APP_1563 extends Migration
{
	public function up()
	{
		$this->createTable('mailing_caseable_gutschein', [
			'id' => Schema::TYPE_PK,
			'code' => Schema::TYPE_STRING,
			'sale_id' => Schema::TYPE_INTEGER,
		]);

		$this->createIndex('code', 'mailing_caseable_gutschein', 'code', true);
		$this->createIndex('sale_id', 'mailing_caseable_gutschein', 'sale_id', true);
	}

	public function down()
	{
		$this->dropTable('mailing_caseable_gutschein');
	}

}
