<?php

use yii\db\Schema;
use yii\db\Migration;

class m150608_134139_APP_342 extends Migration
{
	public function up()
	{
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'SMS-Flat'],['id'=>5]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>7]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>8]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>9]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>10]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>11]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>12]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>13]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>14]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>15]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>16]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>17]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>25]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>27]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>28]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>29]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>30]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>31]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>32]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Musik-Flat*'],['id'=>33]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Musik-Flat*'],['id'=>34]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Musik-Flat*'],['id'=>35]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>38]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>39]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>40]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>41]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>42]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>43]);
		$this->update(\common\models\TariffHighlightDetails::tableName(), ['title'=>'Internet-Flat'],['id'=>44]);
		
		$this->update(\common\models\Tariffs::tableName(), ['title'=>'Data Go M'],['id'=>1136]);
		$this->update(\common\models\Tariffs::tableName(), ['title'=>'Data Go S eco'],['id'=>1161]);
		$this->update(\common\models\Tariffs::tableName(), ['title'=>'Data Go M eco'],['id'=>1162]);
		$this->update(\common\models\Tariffs::tableName(), ['title'=>'Data Go L eco'],['id'=>1163]);
		$this->update(\common\models\Tariffs::tableName(), ['title'=>'Data Go S eco Young'],['id'=>1164]);
		$this->update(\common\models\Tariffs::tableName(), ['title'=>'Data Go M eco Young'],['id'=>1165]);
		$this->update(\common\models\Tariffs::tableName(), ['title'=>'Data Go L eco Young'],['id'=>1166]);
		 
		$this->update('tarifoptionen_stammdaten', ['name'=>'Festnetz-Flat'],['option_id'=>3]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Allnet-Flat 100'],['option_id'=>6]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Allnet-Flat 300'],['option_id'=>7]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Internet-Flat S'],['option_id'=>9]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Internet-Flat'],['option_id'=>10]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Internet-Flat L'],['option_id'=>11]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Internet-Flat XL'],['option_id'=>12]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Wunschnetz-Flatrate'],['option_id'=>38]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Allnet-Flat'],['option_id'=>39]);
		$this->update('tarifoptionen_stammdaten', ['name'=>'Auslands-Festnetz 400'],['option_id'=>43]);

		$this->update('tarifoptionen_gruppen', ['name'=>'Internet-Optionen'],['option_group_id'=>3]);
		$this->update('tarifoptionen_gruppen', ['name'=>'Minuten-Kontingente'],['option_group_id'=>2]);
	}

	public function down()
	{
		echo "m150608_134139_APP_342 cannot be reverted.\n";

		return false;
	}
	
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}
	
	public function safeDown()
	{
	}
	*/
}
