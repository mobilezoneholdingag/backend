<?php

use yii\db\Migration;

/**
 * Class m161209_160445_MZ_375_alter_table_provider
 */
class m161209_160445_MZ_375_alter_table_provider extends Migration
{
	const TABLE_NAME = 'provider';
	const NEW_COL_NAME = 'image';

	/**
	 * @return bool
	 */
	public function up()
    {
		$this->addColumn(self::TABLE_NAME, self::NEW_COL_NAME, $this->string());
	    $this->dropColumn(self::TABLE_NAME, 'logo_small');
	    $this->dropColumn(self::TABLE_NAME, 'logo_medium');

	    return true;
    }

	/**
	 * @return bool
	 */
	public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::NEW_COL_NAME);
	    $this->addColumn(self::TABLE_NAME, 'logo_small', $this->string());
	    $this->addColumn(self::TABLE_NAME, 'logo_medium', $this->string());


	    return true;
    }
}
