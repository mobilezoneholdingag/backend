<?php

use yii\db\Schema;
use yii\db\Migration;

class m151106_104953_APP_106 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\OrderStatus::tableName(), 'successful_sale', Schema::TYPE_BOOLEAN);
		$this->update(\common\models\OrderStatus::tableName(), ['successful_sale' => false]);


		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\OrderStatus::tableName(), 'successful_sale');

		return true;
	}
}
