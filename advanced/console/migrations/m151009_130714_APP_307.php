<?php

use yii\db\Schema;
use yii\db\Migration;

class m151009_130714_APP_307 extends Migration
{
	/**
	 * see provider table to more info about who's who, there's also additional info inside
	 * https://deinhandy.atlassian.net/browse/APP-307
	 */
	public function up()
	{
		$this->addColumn(\common\models\Provider::tableName(), 'minimum_legal_status_term', Schema::TYPE_INTEGER);

		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 12], ['id' => 1]);
		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 0], ['id' => 2]);
		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 6], ['id' => 3]);
		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 6], ['id' => 4]);
		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 6], ['id' => 5]);
		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 6], ['id' => 6]);
		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 0], ['id' => 8]);
		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 12], ['id' => 9]);
		$this->update(\common\models\Provider::tableName(), ['minimum_legal_status_term' => 15], ['id' => 12]);
	}

	public function down()
	{
		$this->dropColumn(\common\models\Provider::tableName(), 'minimum_legal_status_term');

		return false;
	}
}
