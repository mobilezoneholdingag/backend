<?php

use yii\db\Migration;

class m160926_130742_make_article_id_in_renewal_request_optional extends Migration
{
	const TABLE = 'renewal_requests';

	public function up()
    {
		$this->alterColumn(static::TABLE, 'article_id', $this->integer(9));

	    return true;
    }

    public function down()
    {
	    $this->alterColumn(static::TABLE, 'article_id', $this->integer(9)->notNull());

	    return true;
    }
}
