<?php

use yii\db\Migration;
use yii\db\Schema;

class m170714_122134_MZ_1104_add_fields_to_shop extends Migration
{
	const TABLE_SHOP = 'shop';
	const TABLE_SPECIAL = 'special';
	const TABLE_SPEAK_LANGUAGE = 'speak_language';
	const TABLE_CERTIFICATIONS = 'certification';

	const TABLE_SHOP_SPECIAL = 'shop_special';
	const TABLE_SHOP_LANGUAGE = 'shop_language';
	const TABLE_SHOP_CERTIFICATION = 'shop_certification';

	const COLUMN_ID = 'id';
	const COLUMN_SHOP_ID = 'shop_id';
	const COLUMN_SPECIAL_ID = 'special_id';
	const COLUMN_LANGUAGE_ID = 'language_id';
	const COLUMN_CERTIFICATION_ID = 'certification_id';

	const COLUMN_SPECIAL_TITLE = 'special_title';
	const COLUMN_SPECIAL_DESCRIPTION = 'special_description';
	const COLUMN_LANGUAGE_NAME = 'name';
	const COLUMN_CERTIFICATION_TYPE = 'type';

	const COLUMN_DESCRIPTION = 'description';

	public function up()
	{
		$this->addColumn(self::TABLE_SHOP,self::COLUMN_DESCRIPTION, Schema::TYPE_TEXT. ' AFTER extra_line');

		//Special
		$this->createTable(
			self::TABLE_SPECIAL,
			[
				self::COLUMN_ID => Schema::TYPE_PK,
				self::COLUMN_SPECIAL_TITLE => Schema::TYPE_STRING,
				self::COLUMN_SPECIAL_DESCRIPTION => Schema::TYPE_TEXT,
			]
		);

		$this->createTable(
			self::TABLE_SHOP_SPECIAL,
			[
				self::COLUMN_SHOP_ID => Schema::TYPE_INTEGER,
				self::COLUMN_SPECIAL_ID => Schema::TYPE_INTEGER,
			]
		);

		//Language
		$this->createTable(
			self::TABLE_SPEAK_LANGUAGE,
			[
				self::COLUMN_ID => Schema::TYPE_PK,
				self::COLUMN_LANGUAGE_NAME => Schema::TYPE_STRING,
			]
		);

		$this->createTable(
			self::TABLE_SHOP_LANGUAGE,
			[
				self::COLUMN_SHOP_ID => Schema::TYPE_INTEGER,
				self::COLUMN_LANGUAGE_ID => Schema::TYPE_INTEGER,
			]
		);

		//Certifications
		$this->createTable(
			self::TABLE_CERTIFICATIONS,
			[
				self::COLUMN_ID => Schema::TYPE_PK,
				self::COLUMN_CERTIFICATION_TYPE => Schema::TYPE_STRING,
			]
		);

		$this->createTable(
			self::TABLE_SHOP_CERTIFICATION,
			[
				self::COLUMN_SHOP_ID => Schema::TYPE_INTEGER,
				self::COLUMN_CERTIFICATION_ID => Schema::TYPE_INTEGER,
			]
		);

		$this->addShopSpecialKeys();
		$this->addShopLanguageKeys();
		$this->addShopCertificationKeys();

		return true;
	}

	private function addShopSpecialKeys()
	{
		$this->addPrimaryKey(
			'PK_shop_special',
			self::TABLE_SHOP_SPECIAL,
			[self::COLUMN_SHOP_ID, self::COLUMN_SPECIAL_ID]
		);

		$this->addForeignKey(
			'FK_shop_special_id',
			self::TABLE_SHOP_SPECIAL,
			self::COLUMN_SHOP_ID,
			self::TABLE_SHOP,
			self::COLUMN_ID,
			'CASCADE'
		);

		$this->addForeignKey(
			'FK_special_special_id',
			self::TABLE_SHOP_SPECIAL,
			self::COLUMN_SPECIAL_ID,
			self::TABLE_SPECIAL,
			self::COLUMN_ID,
			'CASCADE'
		);
	}

	private function addShopLanguageKeys()
	{
		$this->addPrimaryKey(
			'PK_shop_language',
			self::TABLE_SHOP_LANGUAGE,
			[self::COLUMN_SHOP_ID, self::COLUMN_LANGUAGE_ID]
		);

		$this->addForeignKey(
			'FK_shop_language_id',
			self::TABLE_SHOP_LANGUAGE,
			self::COLUMN_SHOP_ID,
			self::TABLE_SHOP,
			self::COLUMN_ID,
			'CASCADE'
		);

		$this->addForeignKey(
			'FK_language_language_id',
			self::TABLE_SHOP_LANGUAGE,
			self::COLUMN_LANGUAGE_ID,
			self::TABLE_SPEAK_LANGUAGE,
			self::COLUMN_ID,
			'CASCADE'
		);
	}

	private function addShopCertificationKeys()
	{
		$this->addPrimaryKey(
			'PK_shop_certification',
			self::TABLE_SHOP_CERTIFICATION,
			[self::COLUMN_SHOP_ID, self::COLUMN_CERTIFICATION_ID]
		);

		$this->addForeignKey(
			'FK_shop_certification_id',
			self::TABLE_SHOP_CERTIFICATION,
			self::COLUMN_SHOP_ID,
			self::TABLE_SHOP,
			self::COLUMN_ID,
			'CASCADE'
		);

		$this->addForeignKey(
			'FK_certification_certification_id',
			self::TABLE_SHOP_CERTIFICATION,
			self::COLUMN_CERTIFICATION_ID,
			self::TABLE_CERTIFICATIONS,
			self::COLUMN_ID,
			'CASCADE'
		);
	}

	public function down()
	{
		$this->dropTable(self::TABLE_SPECIAL);
		$this->dropTable(self::TABLE_SHOP_SPECIAL);

		$this->dropTable(self::TABLE_CERTIFICATIONS);
		$this->dropTable(self::TABLE_SHOP_CERTIFICATION);

		$this->dropTable(self::TABLE_SPEAK_LANGUAGE);
		$this->dropTable(self::TABLE_SHOP_LANGUAGE);

		return true;
	}
}
