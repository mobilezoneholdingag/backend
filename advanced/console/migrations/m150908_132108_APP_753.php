<?php

use yii\db\Schema;
use yii\db\Migration;

class m150908_132108_APP_753 extends Migration
{
	public function up()
	{
		$this->createTable('user_auth', [
			'id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'user_id INT(11) NOT NULL',
			'source VARCHAR(255) NOT NULL',
			'source_id VARCHAR(255) NOT NULL'
		]);

		$this->addForeignKey('FK_user_id', 'user_auth', 'user_id', \common\models\User::tableName(), 'id', 'CASCADE', 'CASCADE');

		$this->addColumn(\common\models\User::tableName(), 'name', 'VARCHAR(255)');

		$users = [
			'mmanthey'          => 'Maik Manthey',
			'dguhl'             => 'Dominic Guhl',
			'sandres'           => 'Sandra Andres',
			'cotterstein'       => 'Christian Otterstein',
			'puya'              => 'Puya Rahimkhan',
			'raphael'           => 'Raphael Leifkes',
			'robert'            => 'Robert Ermich',
			'johannes'          => 'Johannes Kuhnert',
			'tino.rahn'         => 'Tino Rahn',
			'benedikt.illner'   => 'Benedikt Illner',
			'just'              => 'Just',
			'octopodo'          => 'Tintenfisch',
			'nils.roedelbronn'  => 'Nils Rödelbronn',
			'lisa.heck'         => 'Lisa Heck',
			'ujonas'            => 'Uchenna Jonas',
			'cgottschalk'       => 'Christian Gottschalk',
			'chris.greven'      => 'Chris Greven'
		];

		$transaction = $this->db->beginTransaction();
		foreach ($users as $username => $name)
		{
			$this->update(\common\models\User::tableName(), ['name' => $name], ['username' => $username]);
		}
		$transaction->commit();
	}

	public function down()
	{
		if (in_array('user_auth', $this->getDb()->getSchema()->tableNames)) {
			echo "m150908_132108_APP_753 cannot be reverted. If you're sure, please delete table 'user_auth' first.\n";

			return false;
		}

		$this->dropColumn(\common\models\User::tableName(), 'name');

		return true;
	}
}
