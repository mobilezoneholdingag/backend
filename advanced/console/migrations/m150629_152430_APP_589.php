<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ArticleGroups;

class m150629_152430_APP_589 extends Migration
{
	public function up()
	{
		$tableName = ArticleGroups::tableName();
		$this->addColumn($tableName, 'is_bundle', 'INT(1) NOT NULL DEFAULT 0');
	}

	public function down()
	{
		// Empty b/c revert would set up data loss.
		return true;
	}
	
}
