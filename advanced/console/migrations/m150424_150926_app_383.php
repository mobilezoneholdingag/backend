<?php

use yii\db\Migration;
use common\models\Sales;
use common\models\Tariffs;

class m150424_150926_app_383 extends Migration
{
	public function up()
	{
		foreach (Sales::find()->all() as $sale) {
			$sale->tariff_monthly_price = Tariffs::findOne(['id' => $sale->tariff_id])->price_monthly;
			$sale->tariff_connection_fee = Tariffs::findOne(['id' => $sale->tariff_id])->connection_fee;
			$sale->tariff_contract_duration = Tariffs::findOne(['id' => $sale->tariff_id])->contract_duration;
			$sale->save();
		}

		return true;
	}

	public function down()
	{
		foreach (Sales::find()->all() as $sale) {
			$sale->tariff_monthly_price = null;
			$sale->tariff_connection_fee = null;
			$sale->tariff_contract_duration = null;
			$sale->save();
		}

		return true;
	}
}
