<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_130840_APP_1571 extends Migration
{
	public function up()
	{
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_dob', Schema::TYPE_DATE);
		$this->alterColumn(\common\models\Sales::tableName(), 'bank_type', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->alterColumn(\common\models\Sales::tableName(), 'passport_dob', Schema::TYPE_DATE.' NOT NULL');
		$this->alterColumn(\common\models\Sales::tableName(), 'bank_type', Schema::TYPE_INTEGER.' NOT NULL');

		return true;
	}
}
