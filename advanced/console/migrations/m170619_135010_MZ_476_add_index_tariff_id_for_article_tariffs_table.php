<?php

use yii\db\Migration;

class m170619_135010_MZ_476_add_index_tariff_id_for_article_tariffs_table extends Migration
{
	const TARIFF_ID_COLUMN = 'tariff_id';
	const ARTICLE_TARIFFS_TABLE = 'article_tariffs';
	const INDEX_NAME = 'idx_articletariffs_tariff_id';

	public function up()
	{
		$this->createIndex(self::INDEX_NAME, self::ARTICLE_TARIFFS_TABLE, self::TARIFF_ID_COLUMN);

		return true;
	}

	public function down()
	{
		$this->dropIndex(self::INDEX_NAME, self::ARTICLE_TARIFFS_TABLE);

		return true;
	}
}
