<?php

use yii\db\Migration;

class m161220_125505_MZ_417_set_all_status_to_never_subscribed extends Migration
{
	const TABLE_NAME = 'newsletter_subscriber';

	public function up()
	{
		$this->update(self::TABLE_NAME, ['status' => 1]);

		return true;
	}

	public function down()
	{
		$this->update(self::TABLE_NAME, ['status' => null]);

		return true;
	}
}
