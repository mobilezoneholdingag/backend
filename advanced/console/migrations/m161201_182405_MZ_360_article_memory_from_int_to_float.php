<?php

use yii\db\Migration;

class m161201_182405_MZ_360_article_memory_from_int_to_float extends Migration
{
	public function up()
	{
		$this->alterColumn('articles', 'memory', $this->float()->notNull());

		return true;
	}

	public function down()
	{
		$this->alterColumn('articles', 'memory', $this->integer(3)->notNull());

		return true;
	}
}
