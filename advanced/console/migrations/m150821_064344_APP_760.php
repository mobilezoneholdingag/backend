<?php

use yii\db\Schema;
use yii\db\Migration;

class m150821_064344_APP_760 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Pathfinder::tableName(), 'articleTariff_result_1', Schema::TYPE_STRING);
		$this->addColumn(\common\models\Pathfinder::tableName(), 'articleTariff_result_2', Schema::TYPE_STRING);
		$this->addColumn(\common\models\Pathfinder::tableName(), 'articleTariff_result_3', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Pathfinder::tableName(), 'articleTariff_result_1');
		$this->dropColumn(\common\models\Pathfinder::tableName(), 'articleTariff_result_2');
		$this->dropColumn(\common\models\Pathfinder::tableName(), 'articleTariff_result_3');

		return true;
	}
}
