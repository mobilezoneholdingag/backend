<?php

use yii\db\Migration;

class m180409_125640_MZ_1515_add_custom_stock extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\Article::tableName(), 'custom_stock', 'text');
    }

    public function down()
    {
        $this->dropColumn(\common\models\Article::tableName(), 'custom_stock', 'text');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
