<?php

use common\models\Tariffs;
use yii\db\Migration;

class m170622_081839_MZ_1114_specific_legal_text extends Migration
{
	const COLUMN = 'specific_legal_text';

	public function up()
	{
		$this->addColumn(Tariffs::tableName(), self::COLUMN, $this->text()->after('text_internet'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(Tariffs::tableName(), self::COLUMN);

		return false;
	}
}
