<?php

use yii\db\Migration;

class m170828_123750_MZ_356_alternative_provider_img extends Migration
{
	const PROVIDER_TABLE = 'provider';
	const ALT_IMG_COLUMN = 'alt_image';
	const TARIFF_TABLE = 'tariffs';
	const IMG_CHECKBOX_COLUMN = 'show_alt_provider_img';

	public function up()
	{
		$this->addColumn(self::PROVIDER_TABLE, self::ALT_IMG_COLUMN, $this->string()->after('image'));
		$this->addColumn(self::TARIFF_TABLE, self::IMG_CHECKBOX_COLUMN, $this->smallInteger(1));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::PROVIDER_TABLE, self::ALT_IMG_COLUMN);
		$this->dropColumn(self::TARIFF_TABLE, self::IMG_CHECKBOX_COLUMN);

		return true;
	}
}
