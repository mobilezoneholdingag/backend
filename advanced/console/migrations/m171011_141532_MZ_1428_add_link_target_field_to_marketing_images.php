<?php

use yii\db\Migration;

class m171011_141532_MZ_1428_add_link_target_field_to_marketing_images extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\MarketingImages::tableName(), 'link_target', 'varchar(20)');
    }

    public function down()
    {
        $this->dropColumn(\common\models\MarketingImages::tableName(), 'link_target');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
