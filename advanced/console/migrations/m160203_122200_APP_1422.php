<?php

use yii\db\Schema;
use yii\db\Migration;

class m160203_122200_APP_1422 extends Migration
{
	public function up()
	{
		$this->addColumn(\frontend\models\Newsletter::tableName(), 'submit_source', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\frontend\models\Newsletter::tableName(), 'submit_source');

		return true;
	}
}
