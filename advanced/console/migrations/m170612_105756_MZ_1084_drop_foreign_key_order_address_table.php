<?php

use yii\db\Migration;

class m170612_105756_MZ_1084_drop_foreign_key_order_address_table extends Migration
{
	const FOREIGN_KEY_NAME = 'fk_customer_order_address_customer_id';
	const TABLE_NAME = 'order_address';

	public function up()
	{
		$this->dropForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_NAME);

		return true;
	}

	public function down()
	{
		$this->addForeignKey(
			self::FOREIGN_KEY_NAME,
			self::TABLE_NAME,
			'customer_id',
			'customer',
			'id',
			null,
			'cascade'
		);

		return true;
	}
}
