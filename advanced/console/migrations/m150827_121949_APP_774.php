<?php

use yii\db\Schema;
use yii\db\Migration;

class m150827_121949_APP_774 extends Migration
{
	public function up()
	{
		$this->createTable('article_tariffs_keep_list', [
		   'id' => Schema::TYPE_PK,
			'article_id' => Schema::TYPE_INTEGER,
			'tariff_id' => Schema::TYPE_INTEGER,
			'price' => Schema::TYPE_DECIMAL.'(7,2)'
		]);

		return true;
	}

	public function down()
	{
		$this->dropTable('article_tariffs_keep_list');

		return true;
	}
}
