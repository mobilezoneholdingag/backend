<?php

use common\models\Article;
use yii\db\Migration;

class m170210_132009_MZ_576_default_swisscom_sim_article_for_renewal extends Migration
{
	const MULTI_SIM_CARD_ID = 999999;

	public function up()
	{
		$article = Article::findOne(15706);
		$newArticle = new Article();
		$newArticle->attributes = $article->attributes;
		$newArticle->id = self::MULTI_SIM_CARD_ID;
		$newArticle->price_buy = 0.0;
		$newArticle->price_sale = 0.0;
		$newArticle->manufacturer_id = $article->manufacturer_id;
		$newArticle->product_category_id = $article->product_category_id;
		$newArticle->save(false);
	}

	public function down()
	{
		Article::findOne(self::MULTI_SIM_CARD_ID)->delete();
	}
}
