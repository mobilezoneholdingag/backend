<?php

use yii\db\Migration;

class m170418_131110_MZ_949_add_next_delivery_amount_to_stock extends Migration
{
	const TABLE_NAME = 'stock';

	public function up()
	{
		$this->addColumn(self::TABLE_NAME, 'next_delivery_amount', $this->integer(11)->defaultValue(NULL));
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_NAME, 'next_delivery_amount');
	}
}
