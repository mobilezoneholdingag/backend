<?php

use common\models\Article;
use yii\db\Migration;

/**
 * Class m160825_141512_MZ_184_delete_column_insurance_price_from_articles
 */
class m160825_141512_MZ_184_delete_column_insurance_price_from_articles extends Migration
{
	/**
	 * Drop column for insurance price.
	 *
	 * @return bool
	 */
	public function up()
    {
        $this->dropColumn(Article::tableName(), 'insurance_price');

        return true;
    }

	/**
     * Re-insert column for insurance price.
	 *
	 * @return bool
     */
    public function down()
    {
        $this->addColumn(Article::tableName(), 'insurance_price', $this->decimal(6,2));

	    return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
