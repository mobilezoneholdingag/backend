<?php

use yii\db\Migration;

class m160321_084101_APP_1818 extends Migration
{
	public function up()
	{
		$order_status_id = \common\models\OrderStatus::findOne(['code' => 1012])->id;

		$this->update(\common\models\Sales::tableName(), ['order_status' => $order_status_id], ['order_status' => 1012]);

		return true;
	}

	public function down()
	{
		echo "m160321_084101_APP_1818 cannot be reverted.\n";

		return false;
	}
}
