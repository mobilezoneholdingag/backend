<?php

use yii\db\Schema;
use yii\db\Migration;
use backend\models\Tracker;

class m160107_101507_APP_1352 extends Migration
{
	public function up()
	{
		$tracker_id = Tracker::find()->where(['code' => 'C24'])->one()->id;
		$this->update(\common\models\Sales::tableName(), ['tracker_id' => $tracker_id], ['not', ['check24_id' => null]]);

		$tracker_id = Tracker::find()->where(['code' => 'VVX'])->one()->id;
		$this->update(\common\models\Sales::tableName(), ['tracker_id' => $tracker_id], ['not', ['verivox_id' => null]]);
	}

	public function down()
	{
		echo "m160107_101507_APP_1352 cannot be reverted.\n";

		return false;
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
