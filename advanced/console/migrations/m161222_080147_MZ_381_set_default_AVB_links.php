<?php

use common\modules\insurances\models\ArticleInsurance;
use yii\db\Migration;

/**
 * Class m161222_080147_MZ_381_set_default_AVB_links
 */
class m161222_080147_MZ_381_set_default_AVB_links extends Migration
{
	const APPLE_URLS = [
		'de-CH' => 'http://www.apple.com/legal/sales-support/applecare/applecareplus/docs/applecareplus_chde_tc.html?websiteLang=de',
		'fr-FR' => 'http://www.apple.com/legal/sales-support/applecare/applecareplus/docs/applecareplus_chfr_tc.html?websiteLang=fr',
		'it-IT' => 'http://www.apple.com/legal/sales-support/applecare/applecareplus/docs/applecareplus_chit_tc.html?websiteLang=it',
	];

	const AVB_URLS = [
		'de-CH' => 'https://55ac187de1ca161539c1-813fb3d75440f466a8b71726e0d62b63.ssl.cf3.rackcdn.com/downloads/flyer/AVB_Protect Clever_07_2016_D_neu.pdf',
		'fr-FR' => 'https://55ac187de1ca161539c1-813fb3d75440f466a8b71726e0d62b63.ssl.cf3.rackcdn.com/downloads/flyer/AVB_Protect Clever_07_2016_F_neu.pdf',
		'it-IT' => 'https://55ac187de1ca161539c1-813fb3d75440f466a8b71726e0d62b63.ssl.cf3.rackcdn.com/downloads/flyer/AVB_Protect Clever_07_2016_I_neu.pdf',
	];

	private $rowNameToAddTranslationsTo = 'term_link';

	/**
	 * @return bool
	 * @throws ErrorException
	 */
	public function up()
	{
		$models = ArticleInsurance::find()->andWhere(['manufacturer_id' => [9, 32]])->all();

		foreach ($models as $model) {
			/* @var ArticleInsurance $model */
			$manufacturerBasedSettings = null;
			switch ($model->manufacturer_id) {
				case 9:
					$manufacturerBasedSettings = self::AVB_URLS;
					break;
				case 32:
					$manufacturerBasedSettings = self::APPLE_URLS;
					break;
				default:
					throw new ErrorException('unexpected manufacturerid found: ' . $model->manufacturer_id);
			}

			foreach (Yii::$app->params['languages'] as $language) {
				$model->translationsArr[$this->rowNameToAddTranslationsTo][$language]['translation'] = $manufacturerBasedSettings[$language];
			}

			$model->save(false);
		}

		return true;
	}

	/**
	 * @return bool
	 */
	public function down()
	{
		echo "m161222_080147_MZ_381_set_default_AVB_links cannot be reverted, because we don't know if anyone has entered data before.\n";

		return false;
	}
}
