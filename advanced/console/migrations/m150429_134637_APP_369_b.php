<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Sales;
use common\models\Article;

class m150429_134637_APP_369_b extends Migration
{
	public function up()
	{/*
		$sales = Sales::find()->all();
		foreach ($sales as $sale) {
			if ($sale->insurance == 1) {

				$devicePrice = Article::findOne(['id' => $sale->article_id])->price_sale;

				$sale->insurance_price_yearly = self::getMonthlyInsurancePrice($devicePrice) * 12;
				$sale->save();
			}
		}*/
	}

	public function down()
	{
		$sales = Sales::find()->all();
		foreach ($sales as $sale) {
			if ($sale->insurance == 1) {
				$sale->insurance_price_yearly = null;
				$sale->save();
			}
		}
	}

	public function getMonthlyInsurancePrice($devicePrice)
	{
		if ($devicePrice <= 250) {
			$argument = 250;
		} else if ($devicePrice <= 500) {
			$argument = 500;
		} else if ($devicePrice <= 750) {
			$argument = 750;
		} else if ($devicePrice <= 1000) {
			$argument = 1000;
		} else if ($devicePrice <= 1500) {
			$argument = 1500;
		} else {
			return null;
		}

		return Yii::$app->params['insuranceLevel'][$argument];
	}
}
