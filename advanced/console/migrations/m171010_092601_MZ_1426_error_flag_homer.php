<?php

use yii\db\Migration;
use yii\db\Schema;

class m171010_092601_MZ_1426_error_flag_homer extends Migration
{
	const TABLE = 'homer';
	const COLUMN = 'error';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, Schema::TYPE_BOOLEAN);
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}
}
