<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Provider;

class m150717_121912_APP_651 extends Migration
{
	public function up()
	{
		$this->addColumn(Provider::tableName(), 'mnp_blocked_providers', Schema::TYPE_TEXT);

		// set blocked providers for each provider
		$this->update(Provider::tableName(), ['mnp_blocked_providers' => '11'], ['id' => 1]);
		$this->update(Provider::tableName(), ['mnp_blocked_providers' => '102'], ['id' => 6]);
		$this->update(Provider::tableName(), ['mnp_blocked_providers' => '164,165'], ['id' => 4]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Provider::tableName(), 'mnp_blocked_providers');

		return true;
	}
}
