<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_132308_APP_607 extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Provider::tableName(), 'hardware_related_tariff_price', Schema::TYPE_BOOLEAN);
		$this->addColumn(\common\models\ArticleTariffs::tableName(), 'price_monthly', Schema::TYPE_DECIMAL.'(7,2)');

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Provider::tableName(), 'hardware_related_tariff_price');
		$this->dropColumn(\common\models\ArticleTariffs::tableName(), 'price_monthly');

		return true;
	}
}
