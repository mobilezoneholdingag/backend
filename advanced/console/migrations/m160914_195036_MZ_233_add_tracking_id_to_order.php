<?php

use common\modules\order\models\Order;
use yii\db\Migration;

class m160914_195036_MZ_233_add_tracking_id_to_order extends Migration
{
	public function up()
	{
		$this->addColumn(Order::tableName(), 'shipment_tracking_id', $this->string()->after('status'));

		return true;
	}

	public function down()
	{
		$this->dropColumn(Order::tableName(), 'shipment_tracking_id');

		return true;
	}
}
