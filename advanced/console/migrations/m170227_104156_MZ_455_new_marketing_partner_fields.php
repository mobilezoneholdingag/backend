<?php

use yii\db\Migration;

class m170227_104156_MZ_455_new_marketing_partner_fields extends Migration
{
	const OLD_TABLE = 'marketing_partners';

	public function up()
	{
		$this->addColumn(self::OLD_TABLE, 'filename', $this->string());
		$this->addColumn(self::OLD_TABLE, 'last_file_generated_at', $this->dateTime());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::OLD_TABLE, 'filename');
		$this->dropColumn(self::OLD_TABLE, 'last_file_generated_at');

		return true;
	}
}
