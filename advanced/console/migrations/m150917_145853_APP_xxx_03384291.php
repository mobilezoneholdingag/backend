<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_145853_APP_xxx_03384291 extends Migration
{
	public function up()
	{
		$this->addForeignKey('FK_articles_articleGroup', \common\models\Article::tableName(), 'group_id', \common\models\ArticleGroups::tableName(), 'id', 'CASCADE');
		return true;
	}

	public function down()
	{
		$this->dropForeignKey('FK_articles_articleGroup', \common\models\Article::tableName());
		return false;
	}
}
