<?php

use yii\db\Migration;

class m170804_131135_MZ_1154_preorder_order_status extends Migration
{
	const COLUMN = 'preorder';
	const TABLE = 'article_groups';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->integer(1));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}
}
