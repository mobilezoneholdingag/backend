<?php

use yii\db\Migration;

class m161116_120349_MZ_309_add_frontend_title_for_product_category extends Migration
{
	const TABLE = 'product_category';

	public function up()
	{
		$this->addColumn(self::TABLE, 'frontend_title', $this->string()->after('type'));
		$this->getDb()->createCommand('UPDATE ' . self::TABLE . ' SET frontend_title = "Accessory"')->execute();

		$this->update(self::TABLE, ['frontend_title' => 'Mobilephone'], ['id' => 1]);
		$this->update(self::TABLE, ['frontend_title' => 'Tablet'], ['id' => 48]);
		$this->update(self::TABLE, ['frontend_title' => 'Smartwatch'], ['id' => 69]);

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, 'frontend_title');

		return true;
	}
}
