<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Seo;

class m150328_210934_APP_214 extends Migration
{
	public function up()
	{
		$this->addColumn(Seo::tableName(), 'visible', Schema::TYPE_BOOLEAN);
		$this->addColumn(Seo::tableName(), 'html_content', Schema::TYPE_STRING);
		$this->addColumn(Seo::tableName(), 'redirect_url', Schema::TYPE_STRING);
		$this->addColumn(Seo::tableName(), 'canonical_url', Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		echo "m150328_210934_APP_214 cannot be reverted.\n";

		return false;
	}
}
