<?php

use yii\db\Migration;

class m160426_114105_APP_2184 extends Migration
{
	public function up()
	{
		$this->addColumn(\backend\models\ArticleTariffsKeepList::tableName(), 'external_id', \yii\db\mysql\Schema::TYPE_STRING);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\backend\models\ArticleTariffsKeepList::tableName(), 'external_id');

		return true;
	}
}
