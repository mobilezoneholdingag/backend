<?php

use yii\db\Schema;
use yii\db\Migration;

class m150401_123445_TARIFF_API extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\Tariffs::tableName(), 'data_only', Schema::TYPE_BOOLEAN);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\Tariffs::tableName(), 'data_only');

		return true;
	}
}
