<?php

use yii\db\Schema;
use yii\db\Migration;

class m150624_134923_APP_582 extends Migration
{
	public function up()
	{
		$this->addColumn(\frontend\models\Newsletter::tableName(), 'confirmed', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\frontend\models\Newsletter::tableName(), 'confirmed');

		return true;
	}
}
