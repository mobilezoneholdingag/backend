<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Manufacturer;

class m150408_094910_manufacturer_sets extends Migration
{
	public function up()
	{
		$this->addColumn(Manufacturer::tableName(), 'smartphone_set_id', Schema::TYPE_INTEGER);
		$this->addColumn(Manufacturer::tableName(), 'tablet_set_id', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->dropColumn(Manufacturer::tableName(), 'smartphone_set_id');
		$this->dropColumn(Manufacturer::tableName(), 'tablet_set_id');

		return true;
	}
}
