<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_133031_APP_255_2 extends Migration
{
	public function up()
	{
		$this->dropColumn(\common\models\Article::tableName(), 'zugaben_marge');
		$this->alterColumn(\common\models\Article::tableName(), 'giveaway_id', Schema::TYPE_INTEGER);

		return true;
	}

	public function down()
	{
		$this->addColumn(\common\models\Article::tableName(), 'zugaben_marge', Schema::TYPE_DECIMAL);
		$this->alterColumn(\common\models\Article::tableName(), 'giveaway_id', 'int not null');

		return true;
	}
}
