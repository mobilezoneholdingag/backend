<?php

use yii\db\Migration;

class m171117_081712_MZ_1456_add_special_opening_fields extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\Shop::tableName(), 'special_opening', 'text');
    }

    public function down()
    {
        $this->dropColumn(\common\models\Shop::tableName(), 'special_opening', 'text');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
