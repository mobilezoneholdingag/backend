<?php

use yii\db\Migration;

class m160914_073304_MZ_233_delete_legacy_order_stuff extends Migration
{
	public function up()
	{
		$this->dropTable('sales_unapproved');
		$this->dropTable('sales_whitelist');
		$this->dropTable('sales_whitelist_iban');
		$this->dropTable('sales_blacklist');
		$this->dropTable('sales_rejects_contacted');
		$this->dropTable('sales');
		$this->dropTable('order_status');
	}

	public function down()
	{
		echo "m160914_073304_MZ_233_delete_legacy_order_stuff cannot be reverted.\n";

		return false;
	}
}
