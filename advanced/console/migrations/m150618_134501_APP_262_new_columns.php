<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_134501_APP_262_new_columns extends Migration
{
	public function up()
	{
		$this->addColumn(\common\models\TariffSpecial::tableName(), 'custom_infotext_tariff', Schema::TYPE_TEXT);
		$this->addColumn(\common\models\TariffSpecial::tableName(), 'custom_infotext_cart', Schema::TYPE_TEXT);

		return true;
	}

	public function down()
	{
		$this->dropColumn(\common\models\TariffSpecial::tableName(), 'custom_infotext_tariff');
		$this->dropColumn(\common\models\TariffSpecial::tableName(), 'custom_infotext_cart');

		return true;
	}
}
