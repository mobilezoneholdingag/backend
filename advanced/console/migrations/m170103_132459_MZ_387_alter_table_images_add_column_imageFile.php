<?php

use yii\db\Migration;

class m170103_132459_MZ_387_alter_table_images_add_column_imageFile extends Migration
{
	const TABLE_NAME = 'images';
	const COLUMN_NAME = 'imageFile';

	public function up()
	{
		$this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->string(255));

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);

		return true;
	}
}
