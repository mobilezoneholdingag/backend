<?php

use yii\db\Migration;

class m160913_121601_MZ_41_initial_store_tables extends Migration
{
	public function up()
	{
		$this->createTable(
			'shop',
			[
				'id' => $this->primaryKey(),
				'name' => $this->string(255),
				'shop_tree_id' => $this->integer(),
				'shop_tree_name' => $this->integer(),

				'street' => $this->string(255),
				'extra_line' => $this->string(255),
				'zip' => $this->string(255),
				'city' => $this->string(255),
				'canton' => $this->string(255),
				'latitude' => $this->float(),
				'longitude' => $this->float(),
				'phone' => $this->string(255),
				'fax' => $this->string(255),
				'email' => $this->string(255),
				'sort' => $this->string(255),
				'state' => $this->string(255),

				'manager_name' => $this->string(255),
				'manager_function' => $this->string(255),
				'manager_image_id' => $this->integer(),

				'images_title' => $this->string(255),

				'shop_image_id_1' => $this->integer(),
				'shop_image_id_2' => $this->integer(),
				'shop_image_id_3' => $this->integer(),
				'is_helpcenter' => $this->boolean(),
			]
		);

		$this->createTable(
			'shop_image',
			[
				'id' => $this->primaryKey(),
				'shop_id' => $this->integer()->notNull(),
				'image_id' => $this->integer(),
				'type' => $this->string(255),
				'path' => $this->string(255),
				'url' => $this->string(255),
			]

		);

		$this->createTable(
			'shop_hour',
			[
				'id' => $this->primaryKey(),
				'shop_id' => $this->integer()->notNull(),
				'day' => $this->integer(),
				'opened_at' => $this->time(),
				'closed_at' => $this->time(),
			]

		);

		$this->addForeignKey('fk_shop_image_shop_id', 'shop_image', 'shop_id', 'shop', 'id', 'cascade');
		$this->addForeignKey('fk_shop_hour_shop_id', 'shop_hour', 'shop_id', 'shop', 'id', 'cascade');
	}

	public function down()
	{
		$this->dropTable('shop_image');
		$this->dropTable('shop_hour');
		$this->dropTable('shop');

		return true;
	}
}
