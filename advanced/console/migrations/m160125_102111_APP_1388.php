<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_102111_APP_1388 extends Migration
{
	public function up()
	{
		$this->alterColumn(\common\models\ArticleGroups::tableName(), 'article_feed_description', Schema::TYPE_TEXT);
		$this->alterColumn(\common\models\Tariffs::tableName(), 'tariff_feed_description', Schema::TYPE_TEXT);

		return true;
	}

	public function down()
	{
		$this->alterColumn(\common\models\ArticleGroups::tableName(), 'article_feed_description', Schema::TYPE_STRING);
		$this->alterColumn(\common\models\Tariffs::tableName(), 'tariff_feed_description', Schema::TYPE_STRING);

		return true;
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
