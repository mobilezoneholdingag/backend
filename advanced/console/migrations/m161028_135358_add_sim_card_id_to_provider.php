<?php

use yii\db\Migration;

/**
 * Handles adding sim_card_id to table `provider`.
 */
class m161028_135358_add_sim_card_id_to_provider extends Migration
{
	const TABLE = 'provider';

    /**
     * @inheritdoc
     */
    public function up()
    {
    	$this->addColumn(self::TABLE, 'simcard_id', $this->integer());

	    return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    $this->dropColumn(self::TABLE, 'simcard_id');

	    return true;
    }
}
