<?php

use yii\db\Migration;

class m170214_160412_MZ_698_update_table_customer_phone_number_with_missing_fields extends Migration
{
	const TABLE = 'customer_phone_number';

	public function up()
    {
		$this->addColumn(self::TABLE, 'wait_until_contract_ends', $this->boolean()->defaultValue(1)->notNull());
		$this->addColumn(self::TABLE, 'type', $this->integer());
    }

    public function down()
    {
		$this->dropColumn(self::TABLE, 'wait_until_contract_ends');
		$this->dropColumn(self::TABLE, 'type');
    }
}
