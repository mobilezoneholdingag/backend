<?php

use yii\db\Migration;

class m170531_132751_MZ_1042_add_minimum_price_column_to_coupon_table extends Migration
{
	const TABLE = 'coupons_normal';
	const COLUMN = 'minimum_price';

	public function up()
	{
		$this->addColumn(self::TABLE, self::COLUMN, $this->float());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::TABLE, self::COLUMN);

		return true;
	}
}
