<?php

use yii\db\Migration;

class m170309_165303_MZ_522_add_images_column_to_article extends Migration
{
	const TABLE_ARTICLE = 'articles';
	const COLUMN_IMAGES = 'images';

	public function up()
	{
		$this->addColumn(self::TABLE_ARTICLE, self::COLUMN_IMAGES, $this->text()->after('product_name'));
	}

	public function down()
	{
		$this->dropColumn(self::TABLE_ARTICLE, self::COLUMN_IMAGES);
	}
}
