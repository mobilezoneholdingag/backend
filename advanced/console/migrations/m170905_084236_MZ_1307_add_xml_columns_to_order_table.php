<?php

use yii\db\Migration;

class m170905_084236_MZ_1307_add_xml_columns_to_order_table extends Migration
{
	const ORDER_TABLE = 'order';
	const XML_CREATED_AT_COLUMN = 'xml_created_at';
	const XML_CONTENT_COLUMN = 'xml_content';

	public function up()
	{
		$this->addColumn(self::ORDER_TABLE, self::XML_CREATED_AT_COLUMN, $this->dateTime());
		$this->addColumn(self::ORDER_TABLE, self::XML_CONTENT_COLUMN, $this->text());

		return true;
	}

	public function down()
	{
		$this->dropColumn(self::ORDER_TABLE, self::XML_CREATED_AT_COLUMN);
		$this->dropColumn(self::ORDER_TABLE, self::XML_CONTENT_COLUMN);

		return true;
	}
}
