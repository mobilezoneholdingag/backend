<?php

namespace console\models\mobilezone\erp\types;

class AccessorySetTypes
{
	const TYPE_SCREEN_PROTECTOR = 8;
	const TYPE_SOFT_COVER = 9;
	const TYPE_COVER = 10;
	const TYPE_ETUI = 11;
	const TYPE_ACCU = 12;
	const TYPE_CHARGER_12V = 13;
	const TYPE_CHARGER_220V = 14;
	const TYPE_HEADSET_WIRELESS = 15;
	const TYPE_HEADSET_CABLE = 16;
	const TYPE_SPEAKERS = 17;
	const TYPE_MEMORY_CARDS = 18;
	const TYPE_MOBILE_DATA = 19;
	const TYPE_STYLUS = 20;
	const TYPE_CAR_HOLDER = 21;
	const TYPE_SMARTWATCH = 35;
	const TYPE_CAR_KIT = 21;
	const TYPE_SMARTWATCH_BAND = 69;
	const TYPE_BATTERY = 70;

	const ERP_PRODUCT_GROUP_SET_TYPE_MAP = [
		51 => self::TYPE_SCREEN_PROTECTOR,
		49 => self::TYPE_SOFT_COVER,
		2 => self::TYPE_COVER,
		8 => self::TYPE_ETUI,
		3 => self::TYPE_ACCU,
		5 => self::TYPE_CHARGER_12V,
		4 => self::TYPE_CHARGER_220V,
		50 => self::TYPE_HEADSET_WIRELESS,
		6 => self::TYPE_HEADSET_CABLE,
		60 => self::TYPE_SPEAKERS,
		58 => self::TYPE_MEMORY_CARDS,
		28 => self::TYPE_MOBILE_DATA,
		62 => self::TYPE_STYLUS,
		52 => self::TYPE_CAR_HOLDER,
		69 => self::TYPE_SMARTWATCH,
		63 => self::TYPE_SMARTWATCH,
		7 => self::TYPE_CAR_KIT,
		74 => self::TYPE_SMARTWATCH_BAND,
		30 => self::TYPE_BATTERY,
	];
}
