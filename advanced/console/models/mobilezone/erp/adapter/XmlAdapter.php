<?php

namespace console\models\mobilezone\erp\adapter;

use common\modules\fileProcessingLog\models\OrderInfoLog;
use components\services\FtpsService;
use console\models\mobilezone\erp\ArticleInterface;
use console\models\mobilezone\erp\ArticleTariffInterface;
use console\models\mobilezone\erp\ManufacturerInterface;
use console\models\mobilezone\erp\OfferTypeInterface;
use console\models\mobilezone\erp\ProductCategoryInterface;
use console\models\mobilezone\erp\ProviderInterface;
use console\models\mobilezone\erp\StockInterface;
use console\models\mobilezone\erp\TariffInterface;
use console\models\mobilezone\erp\xml\Article;
use console\models\mobilezone\erp\xml\ArticleTariff;
use console\models\mobilezone\erp\xml\Manufacturer;
use console\models\mobilezone\erp\xml\OfferType;
use console\models\mobilezone\erp\xml\OrderInfo;
use console\models\mobilezone\erp\xml\ProductGroup;
use console\models\mobilezone\erp\xml\Provider;
use console\models\mobilezone\erp\xml\Stock;
use console\models\mobilezone\erp\xml\Tariff;
use DateInterval;
use DateTime;
use RuntimeException;
use SimpleXMLElement;
use Yii;

/**
 * Adapter for Mobilezone-ERP-XML files, implementing adapter for Mobilezone-ERP, which will have a different
 * API in the end (at least, that is the plan). So a second adapter will have to be implemented, implementing the same
 * interface.
 */
class XmlAdapter implements AdapterInterface
{
	const ARCHIVE_DATETIME_FORMAT = 'Y-m-d-H-i';
	const ARCHIVE_DIRECTORY = 'archive';

	/**
	 * @inheritdoc
	 * @return ProviderInterface[]
	 */
	public function getProviders(): array
	{
		$this->checkFileFreshnessOrLogError('providers.xml', new DateInterval('PT24H'));
		$providers = $this->readXmlFile('providers.xml');

		$result = [];
		foreach ($providers->provider as $providerData) {
			$result[] = new Provider($providerData->asXML());
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 * @return ManufacturerInterface[]
	 */
	public function getManufacturers(): array
	{
		$this->checkFileFreshnessOrLogError('manufacturers.xml', new DateInterval('PT24H'));
		$manufacturers = $this->readXmlFile('manufacturers.xml');

		$result = [];
		foreach ($manufacturers->manufacturer as $manufacturerData) {
			$result[] = new Manufacturer($manufacturerData->asXML());
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 * @return OfferTypeInterface[]
	 */
	public function getOfferTypes(): array
	{
		$this->checkFileFreshnessOrLogError('offertypes.xml', new DateInterval('PT24H'));
		$offerTypes = $this->readXmlFile('offertypes.xml');

		$result = [];
		foreach ($offerTypes->offertype as $offerTypeData) {
			$result[] = new OfferType($offerTypeData->asXML());
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 * @return ProductCategoryInterface[]
	 */
	public function getProductCategories(): array
	{
		$this->checkFileFreshnessOrLogError('productgroups.xml', new DateInterval('PT24H'));
		$categories = $this->readXmlFile('productgroups.xml');

		$result = [];
		foreach ($categories->productgroup as $category) {
			$result[] = new ProductGroup($category->asXML());
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 * @return ArticleInterface[]
	 */
	public function getArticles(): array
	{
		$this->checkFileFreshnessOrLogError('articles.xml', new DateInterval('PT1H'));
		$articles = $this->readXmlFile('articles.xml');

		$result = [];
		foreach ($articles->article as $article) {
			$result[] = new Article($article->asXML());
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 * @return StockInterface[]
	 */
	public function getStocks(): array
	{
		$this->checkFileFreshnessOrLogError('inventories.xml', new DateInterval('PT1H'));
		$stocks = $this->readXmlFile('inventories.xml');

		$result = [];
		foreach ($stocks->inventory as $stock) {
			// Channel 1 is stock pool for webshop AND in central logistics
			// (this is the total stock that is available to the webshop)
			if ((int)$stock->CHANNEL != 1) {
				continue;
			}
			$result[] = new Stock($stock->asXML());
		}

		return $result;
	}

	/**
	 * List of all tariffs.
	 *
	 * @return TariffInterface[]
	 */
	public function getTariffs(): array
	{
		$this->checkFileFreshnessOrLogError('offers.xml', new DateInterval('PT24H'));
		$tariffs = $this->readXmlFile('offers.xml');

		$result = [];
		foreach ($tariffs->offer as $tariff) {
			$result[] = new Tariff($tariff->asXML());
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 * @return ArticleTariffInterface[]
	 */
	public function getArticleTariffs(): array
	{
		$this->checkFileFreshnessOrLogError('prices.xml', new DateInterval('PT24H'));
		$articleTariffs = $this->readXmlFile('prices.xml');

		$result = [];
		foreach ($articleTariffs->price as $articleTariff) {
			$result[] = new ArticleTariff($articleTariff->asXML());
		}

		return $result;
	}

	public function deleteEmptyOrderInfoFiles()
	{
		$fileNames = $this->getOrderInfoFileNames();
		foreach ($fileNames as $fileName => $fileInfo) {
			if ($fileInfo['size'] == 0) {
				$this->deleteOrderInfoFile($fileName);
			}
		}
	}

	public function getOrderInfoArrayFromFile(string $path): array
	{
		/** @var FtpsService $ftpsService */
		$ftpsService = Yii::$app->get('ftps');
		$xmlData = $ftpsService->getFileDataFromFtps($path);

		if (empty($xmlData)) {
			return [];
		}

		$orderInfos = [];
		$xmlElement = new SimpleXMLElement($xmlData);

		foreach ($xmlElement->info_update as $orderInfo) {
			$orderInfoObject = new OrderInfo((array)$orderInfo);

			if (!$orderInfoObject->orderid) {
				continue;
			}

			if (!isset($orderInfos[$orderInfoObject->orderid])) {
				$orderInfos[$orderInfoObject->orderid] = [];
			}

			$orderInfos[$orderInfoObject->orderid][] = $orderInfoObject;
		}

		return $orderInfos;
	}

	/**
	 * @param string $filename
	 *
	 * @return string
	 */
	private function generateErpXmlFilename(string $filename): string
	{
		return $this->getErpXmlPath() . $filename;
	}

	private function getErpXmlPath(): string
	{
		$directoryPathOnServer = '/mobilezone-erp-xml/erp/';

		$ftp = $this->getPreconfiguredFtps();
		if ($ftp->parentDirectory == '/dev') {
			// there is no dev-version of these files; dev and staging have to import "live" ones!
            // revert! seems to be needed on staging
			$directoryPathOnServer = '/..' . $directoryPathOnServer;
            // COMMENTED OUT ON BRANCH MZ-1537
		}

		return $directoryPathOnServer;
	}

	private function getArchivePath(): string
	{
		return $this->getErpXmlPath() . self::ARCHIVE_DIRECTORY;
	}

	/**
	 * @param string $filename
	 *
	 * @return SimpleXMLElement
	 */
	private function readXmlFile($filename): SimpleXMLElement
	{
		$ftp = $this->getPreconfiguredFtps();
		$xmlData = $ftp->getFileDataFromFtps($this->generateErpXmlFilename($filename));

		return new SimpleXMLElement($xmlData);
	}

	/**
	 * Please note that we intentionally do not abort the process when this check fails, as there might still be an
	 * older but still valid file existing, which should then be processed amongst the logged error.
	 *
	 * @param string $filename
	 * @param DateInterval $maximumAge
	 */
	private function checkFileFreshnessOrLogError(string $filename, DateInterval $maximumAge)
	{
		$ftp = $this->getPreconfiguredFtps();

		try {
			$lastModified = $ftp->getLastModified($this->generateErpXmlFilename($filename));
		} catch (RuntimeException $e) {
			Yii::error('File ' . $filename . ' could not be found on the FTP.', 'mobilezone-erp-xml-adapter');

			return;
		}

		$latestAllowedTime = new DateTime();
		$latestAllowedTime->sub($maximumAge);

		if ($lastModified < $latestAllowedTime) {
			Yii::error(
				'File ' . $filename . ' is older than allowed maximum age of ' . $maximumAge->format('%h') .
				' hour(s) (Last modified: ' . $lastModified->format('Y-m-d H:i:s') .
				' ' . $lastModified->getTimezone()->getName() . ').',
				'mobilezone-erp-xml-adapter'
			);
		}
	}

	/**
	 * @return \components\services\FtpsService|object
	 */
	private function getPreconfiguredFtps()
	{
		return Yii::$app->get('ftps');
	}

	public function getOrderInfoFileNames(): array
	{
		$ftp = $this->getPreconfiguredFtps();

		return $ftp->getFileListFromFtps($this->getOrderInfoDirectoryPath());
	}

	private function getOrderInfoDirectoryPath(): string
	{
		return '/mobilezone-erp-xml/orders/posinfo/';
	}

	public function markOrderInfoFileAsSolved($filename): bool
	{
		if ($this->archiveOrderInfoFile($filename)) {
			return $this->deleteOrderInfoFile($filename);
		}

		return false;
	}

	protected function deleteOrderInfoFile($filename): bool
	{
		$ftp = $this->getPreconfiguredFtps();

		return $ftp->deleteFile($this->getOrderInfoDirectoryPath() . $filename);
	}

	protected function archiveOrderInfoFile($filename): bool
	{
		$ftp = $this->getPreconfiguredFtps();
		$fileContent = $ftp->getFileDataFromFtps($this->getOrderInfoDirectoryPath() . $filename);
		$log = new OrderInfoLog();

		return $log->log($filename, $fileContent);
	}

	public function archiveFile(string $filename, string $destination = null)
	{
		$path = $this->generateErpXmlFilename($filename);
		$ftp = $this->getPreconfiguredFtps();
		$destination = $destination ?? $this->getArchiveDestination($path);
		$ftp->copyFile($path, $destination);
	}

	private function getArchiveDestination(string $path)
	{
		$pathParts = explode('/', $path);
		$length = count($pathParts);
		$filename = $pathParts[$length - 1];
		$pathParts[$length] = $this->getFilenameWithTimestamp($filename);
		$pathParts[$length - 1] = self::ARCHIVE_DIRECTORY;

		return implode('/', $pathParts);
	}

	private function getFilenameWithTimestamp(string $filename, DateTime $dateTime = null): string
	{
		$filenameParts = explode('.', $filename);
		$dateTime = $dateTime ?? new DateTime();
		$today = $dateTime->format(self::ARCHIVE_DATETIME_FORMAT);
		$filenameWithDate = "{$filenameParts[0]}_{$today}.{$filenameParts[1]}";

		return $filenameWithDate;
	}

	public function removeArchivedFiles(DateTime $olderThan = null): int
	{
		$ftp = $this->getPreconfiguredFtps();
		$archivePath = $this->getArchivePath();
		$files = $ftp->getFileList($archivePath);
		$monthAgo = $olderThan ?? (new DateTime())->modify('-1 months');

		$removeCount = 0;
		foreach ($files as $file) {
			if ($this->isArchivedFileOlderThan($file, $monthAgo)) {
				$removeCount += (int)$ftp->deleteFile("{$archivePath}/{$file}");
			}
		}

		return $removeCount;
	}

	private function isArchivedFileOlderThan(string $filename, DateTime $dateTime): bool
	{
		// filename format: articles_2017-06-15-01-08.xml
		$fileTimestamp = explode('.', explode('_', $filename)[1])[0];
		$fileDateTime = date_create_from_format(self::ARCHIVE_DATETIME_FORMAT, $fileTimestamp);

		return $fileDateTime < $dateTime;
	}
}
