<?php

namespace console\models\mobilezone\erp\adapter;

use console\models\mobilezone\erp\ArticleInterface;
use console\models\mobilezone\erp\ArticleTariffInterface;
use console\models\mobilezone\erp\ManufacturerInterface;
use console\models\mobilezone\erp\OfferTypeInterface;
use console\models\mobilezone\erp\OrderInfoInterface;
use console\models\mobilezone\erp\ProductCategoryInterface;
use console\models\mobilezone\erp\ProviderInterface;
use console\models\mobilezone\erp\StockInterface;
use console\models\mobilezone\erp\TariffInterface;
use console\models\mobilezone\erp\xml\OrderInfo;

/**
 * Interface for adapters reading from the mobilezone ERP.
 */
interface AdapterInterface
{
	/**
	 * List of all providers.
	 *
	 * @return ProviderInterface[]
	 */
	public function getProviders() : array;

	/**
	 * List of all manufacturers
	 *
	 * @return ManufacturerInterface[]
	 */
	public function getManufacturers() : array;

	/**
	 * List of all offer types (new contract, number porting, ...)
	 *
	 * @return OfferTypeInterface[]
	 */
	public function getOfferTypes() : array;

	/**
	 * List of all product categories.
	 *
	 * @return ProductCategoryInterface[]
	 */
	public function getProductCategories() : array;

	/**
	 * List of all articles.
	 *
	 * @return ArticleInterface[]
	 */
	public function getArticles() : array;

	/**
	 * List of all stocks (availabilities).
	 *
	 * @return StockInterface[]
	 */
	public function getStocks() : array;

	/**
	 * List of all tariffs.
	 *
	 * @return TariffInterface[]
	 */
	public function getTariffs() : array;

	/**
	 * List of all article tariffs.
	 *
	 * @return ArticleTariffInterface[]
	 */
	public function getArticleTariffs() : array;

	/**
	 * @param string $filename
	 *
	 * @return OrderInfo[]
	 */
	public function getOrderInfoArrayFromFile(string $filename) : array;

	/**
	 * Deletes and archives the order info file.
	 *
	 * @param $filename
	 *
	 * @return bool
	 */
	public function markOrderInfoFileAsSolved($filename) : bool;

	public function getOrderInfoFilenames() : array;
}
