<?php

namespace console\models\mobilezone\erp;

/**
 * Interface for article tariffs from the mobilezone ERP.
 */
interface ArticleTariffInterface
{
	public function getId() : int;

	public function getArticleId() : int;

	public function getTariffId() : int;

	public function getOneTimePrice() : float;

	/**
	 * ISO-4217 3-letter currency code, e.g. "CHF" or "EUR".
	 *
	 * @return string
	 */
	public function getCurrency() : string;

	public function getValidFromDate() : string;

	/**
	 * @return string|null Intentionally not set as return type hint, as PHP does not allow mixing types.
	 */
	public function getValidUntilDate();

	public function isActive() : bool;

	public function getUpdatedAt() : string;
}