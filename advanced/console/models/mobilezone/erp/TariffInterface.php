<?php

namespace console\models\mobilezone\erp;

/**
 * Interface for tariffs from the mobilezone ERP.
 */
interface TariffInterface
{
	public function getId() : int;

	public function getName() : string;

	public function isActive() : bool;

	public function getProviderId() : int;

	public function getOfferTypeId() : int;

	public function getFulfillmentPartnerId() : int;

	/**
	 * @return int In months.
	 */
	public function getContractDuration() : int;

	public function getUpdatedAt() : string;
}