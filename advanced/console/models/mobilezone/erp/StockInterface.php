<?php

namespace console\models\mobilezone\erp;

/**
 * Interface for stock (availability) entries from the mobilezone ERP.
 */
interface StockInterface
{
	public function getId() : int;

	public function getQuantity() : int;

	/**
	 * Will only be filled if there is a next delivery date (if article is still in stock, no need for this, null then)
	 *
	 * @return string|null
	 */
	public function getNextDeliveryDate();

	public function getNextDeliveryAmount();
}
