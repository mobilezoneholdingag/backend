<?php

namespace console\models\mobilezone\erp;

/**
 * Interface for provider entries from the mobilezone ERP.
 */
interface ProviderInterface
{
	public function getId() : int;

	public function getName() : string;

	public function getUpdatedAt() : string;
}