<?php

namespace console\models\mobilezone\erp;

/**
 * Interface for product group entries from the mobilezone ERP.
 */
interface ProductCategoryInterface
{
	public function getId() : int;

	public function getName() : string;

	public function isActive() : bool;

	public function getUpdatedAt() : string;
}