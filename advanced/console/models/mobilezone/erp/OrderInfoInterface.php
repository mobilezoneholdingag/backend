<?php

namespace console\models\mobilezone\erp;

interface OrderInfoInterface
{
	public function getOrderId() : int;

	public function getType() : int;

	public function getValue() : string;

	public function isSerialNumberInfo() : bool;

	public function isTrackingIdInfo() : bool;
}
