<?php

namespace console\models\mobilezone\erp;

/**
 * Interface for articles from the mobilezone ERP.
 */
interface ArticleInterface
{
	public function getId() : int;

	/**
	 * This is the "material number" - a ID used at the the cash registers, that employees of mobilezone most likely
	 * would like to search for in our backend. Hence, I kept the name.
	 *
	 * @return string
	 */
	public function getMatNo() : string;

	public function getName() : string;

	/**
	 * @return string|null Intentionally not set as return type hint, as PHP does not allow string or null.
	 */
	public function getBarCode();

	public function getManufacturerId() : int;

	public function getProductCategoryId() : int;

	/**
	 * This is the hardware only price, i.e. what a customer has to pay if he only wants to buy this hardware
	 * separately.
	 *
	 * @return float
	 */
	public function getPrice() : float;

	/**
	 * This is the price used as total input for calculation of monthly payments in combination with sunrise tariffs.
	 *
	 * @return float|null Intentionally not set as return type hint, as PHP does not allow returnung null for float.
	 */
	public function getSunriseNetPrice();

	/**
	 * Whether vouchers and so on can be applied for reducing the price of this article or not.
	 *
	 * @return bool
	 */
	public function isDiscountable() : bool;

	/**
	 * ISO-4217 3-letter currency code, e.g. "CHF" or "EUR".
	 *
	 * @return string
	 */
	public function getCurrency() : string;

	/**
	 * Value added tax rate, as a percentage.
	 *
	 * @return float
	 */
	public function getVatRate() : float;

	public function isActive() : bool;

	public function getUpdatedAt() : string;

	/**
	 * 'old price', to be displayed striked through.
	 *
	 * @see getDiscountType()
	 * @return float
	 */
	public function getStrikePrice() : float;

	/**
	 * Reason for discount, if there is any.
	 *
	 * @see getStrikePrice()
	 * @return string
	 */
	public function getDiscountType() : string;

	/**
	 * Whether this article should be shown in nettozone shop or not.
	 *
	 * @return bool
	 */
	public function isNettozoneActive() : bool;
}