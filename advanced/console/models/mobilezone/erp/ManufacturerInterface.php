<?php

namespace console\models\mobilezone\erp;

/**
 * Interface for manufacturer entries from the mobilezone ERP.
 */
interface ManufacturerInterface
{
	public function getId() : int;

	public function getName() : string;

	public function isActive() : bool;

	public function getUpdatedAt() : string;
}