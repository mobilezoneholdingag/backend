<?php
namespace console\models\mobilezone\erp;

use console\models\mobilezone\erp\xml\OrderInfo;

interface OrderInfoCollectionInterface
{

	public function addOrderInfo(OrderInfoInterface $orderInfo);

	/**
	 * @return OrderInfo[]
	 */
	public function getOrderInfos() : array;
}
