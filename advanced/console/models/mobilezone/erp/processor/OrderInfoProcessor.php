<?php
namespace console\models\mobilezone\erp\processor;

use common\modules\order\models\Order;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use console\models\mobilezone\erp\exceptions\UnknownEntityException;
use console\models\mobilezone\erp\xml\OrderInfo;
use LogicException;
use Yii;
use yii\base\Component;

class OrderInfoProcessor extends Component
{
	const POSINFO_PATH = '/mobilezone-erp-xml/orders/posinfo/';

	/** @var XmlAdapter */
	private $erpAdapter;
	/** @var Order[] */
	private $orders = [];

	public function __construct(array $config = [])
	{
		$this->erpAdapter = new XmlAdapter();
		$this->erpAdapter->deleteEmptyOrderInfoFiles();
		parent::__construct($config);
	}

	public function process()
	{
		$fileNames = Yii::$app->get('ftps')->getFileListFromFtps(self::POSINFO_PATH);

		foreach ($fileNames as $fileName => $fileInfo) {
			try {
				$orderInfoArray = $this->erpAdapter->getOrderInfoArrayFromFile(self::POSINFO_PATH . $fileName);
				$this->processOrderInformation($orderInfoArray);
				$this->erpAdapter->markOrderInfoFileAsSolved($fileName);
			} catch (\Exception $e) {
				Yii::error($e, 'order');
			}
		}

		$this->saveOrders();
	}

	private function processOrderInformation(array $orderInfoArray)
	{
		foreach ($orderInfoArray as $orderId => $orderInformation) {
			try {
				$order = $this->getOrder($orderId);
			} catch (UnknownEntityException $e) {
				continue;
			}
			$this->setOrderAttributes($order, $orderInformation);
			$this->orders[$orderId] = $order;
		}
	}

	private function getOrder($orderId)
	{
		if (!$orderId) {
			throw new UnknownEntityException("No order ID specified!");
		}

		$order = Order::findOne($orderId);

		if (!$order) {
			throw new UnknownEntityException("Order with ID '{$orderId}' not found!");
		}

		return $order;
	}

	private function setOrderAttributes(Order &$order, array $orderInformation)
	{
		/** @var OrderInfo[] $orderInformation */
		foreach ($orderInformation as $orderInfoItem) {
			switch ($orderInfoItem->typ) {
				case OrderInfo::TYPE_SERIAL_NUMBER:
					$order->addSerialNumber($orderInfoItem->textvalue);
					break;
				case OrderInfo::TYPE_TRACKING_ID:
					$order->addShipmentTrackingId($orderInfoItem->textvalue);
					break;
				default:
					return;
			}
		}
	}

	private function saveOrders()
	{
		foreach ($this->orders as $order) {
			try {
				$order->save();
			} catch (LogicException $e) {
				Yii::error(
					"Order with id {$order->id} could not be saved because of the logical error: {$e->getMessage()}",
					'order'
				);
			}
		}
	}
}
