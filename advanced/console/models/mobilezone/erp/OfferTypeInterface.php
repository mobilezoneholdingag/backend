<?php

namespace console\models\mobilezone\erp;

/**
 * Interface for offer type entries from the mobilezone ERP.
 */
interface OfferTypeInterface
{
	public function getId() : int;

	public function getName() : string;

	public function isActive() : bool;
}