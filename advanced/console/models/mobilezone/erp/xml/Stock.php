<?php

namespace console\models\mobilezone\erp\xml;

use console\models\mobilezone\erp\StockInterface;
use SimpleXMLElement;

/**
 * Provider data model for mobilezone ERP data via XML.
 */
class Stock extends SimpleXMLElement implements StockInterface
{
	public function getId() : int
	{
		return (int)$this->MATID;
	}


	public function getQuantity() : int
	{
		return (int)$this->STOCK;
	}

	/**
	 * @inheritdoc
	 */
	public function getNextDeliveryDate()
	{
		$date = (string)$this->NEXT_DELIVERY_DATE;
		if (empty($date)) {
			return null;
		}
		return $date;
	}

	public function getNextDeliveryAmount()
	{
		$amount = (string)$this->NEXT_DELIVERY_AMOUNT;
		if (empty($amount)) {
			return null;
		}
		return $amount;
	}
}
