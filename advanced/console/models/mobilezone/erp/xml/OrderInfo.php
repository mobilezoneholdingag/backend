<?php
namespace console\models\mobilezone\erp\xml;

use yii\base\Object;

class OrderInfo extends Object
{
	const TYPE_SERIAL_NUMBER = 10;
	const TYPE_TRACKING_ID = 20;

	/** @var int */
	public $orderid;
	/** @var int */
	public $positionid;
	/** @var int */
	public $typ;
	/** @var string */
	public $textvalue;
	/** @var int */
	public $numbervalue;
	/** @var string */
	public $timeofchange;

	public function __construct(array $orderInfo)
	{
		parent::__construct($orderInfo);
	}
}
