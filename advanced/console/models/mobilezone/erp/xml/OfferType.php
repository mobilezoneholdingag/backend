<?php

namespace console\models\mobilezone\erp\xml;

use console\models\mobilezone\erp\OfferTypeInterface;
use SimpleXMLElement;

/**
 * Offer type data model for mobilezone ERP data via XML.
 */
class OfferType extends SimpleXMLElement implements OfferTypeInterface
{
	public function getId() : int
	{
		return (int)$this->GID;
	}

	public function getName() : string
	{
		return $this->CPT;
	}

	public function isActive() : bool
	{
		return ((string)$this->STATUS === 'A');
	}
}