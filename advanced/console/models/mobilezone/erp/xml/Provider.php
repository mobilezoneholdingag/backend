<?php

namespace console\models\mobilezone\erp\xml;

use console\models\mobilezone\erp\ProviderInterface;
use SimpleXMLElement;

/**
 * Provider data model for mobilezone ERP data via XML.
 */
class Provider extends SimpleXMLElement implements ProviderInterface
{
	public function getId() : int
	{
		return (int)$this->GID;
	}

	public function getName() : string
	{
		return $this->SUPNAM1;
	}

	public function getUpdatedAt() : string
	{
		return $this->UPDSTMP;
	}
}