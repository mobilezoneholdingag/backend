<?php

namespace console\models\mobilezone\erp\xml;

use console\models\mobilezone\erp\ProductCategoryInterface;
use SimpleXMLElement;

/**
 * Product group data model for mobilezone ERP data via XML.
 */
class ProductGroup extends SimpleXMLElement implements ProductCategoryInterface
{
	public function getId() : int
	{
		return (int)$this->GID;
	}

	public function getName() : string
	{
		return $this->CPT;
	}

	public function isActive() : bool
	{
		return ((string)$this->STATUS === 'A');
	}

	public function getUpdatedAt() : string
	{
		return $this->UPDSTMP;
	}
}