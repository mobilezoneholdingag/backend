<?php

namespace console\models\mobilezone\erp\xml;

use console\models\mobilezone\erp\ArticleInterface;
use SimpleXMLElement;

/**
 * Article data model for mobilezone ERP data via XML.
 */
class Article extends SimpleXMLElement implements ArticleInterface
{
	public function getId() : int
	{
		return (int)$this->GID;
	}

	public function getName() : string
	{
		return $this->MATNAM;
	}

	/**
	 * @inheritdoc
	 */
	public function getMatNo() : string
	{
		return $this->MATNO;
	}

	public function getBarCode() : string
	{
		return $this->BARCODE ?: '';
	}

	public function getManufacturerId() : int
	{
		return (int)$this->WGID;
	}

	public function getProductCategoryId() : int
	{
		return (int)$this->MGID;
	}

	/**
	 * @inheritdoc
	 */
	public function getPrice() : float
	{
		return (float)$this->PRICE;
	}

	/**
	 * @inheritdoc
	 */
	public function getSunriseNetPrice()
	{
		return (float)$this->SUNRISENETPRICE ?: null;
	}

	/**
	 * @inheritdoc
	 */
	public function isDiscountable() : bool
	{
		// Articles that have a lower MINPRICE than PRICE are discountable
		// Actually, all articles that have the same MINPRICE as PRICE are defined to be "non discountable", however
		// this approach does better edge case covering.
		if (empty($this->MINPRICE) === false) {
			return ((float)$this->MINPRICE < (float)$this->PRICE);
		}

		// All other articles are discountable
		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function getCurrency() : string
	{
		return $this->CURRCODE;
	}

	/**
	 * @inheritdoc
	 */
	public function getVatRate() : float
	{
		return (float)$this->TAX;
	}

	public function isActive() : bool
	{
		return (
			(string)$this->STATUS === 'A'
			&& (string)$this->DNYUSE === 'N'
			&& (string)$this->DNYSALE === 'N'
		);
	}

	public function getUpdatedAt() : string
	{
		return $this->UPDSTMP;
	}

	/**
	 * @inheritdoc
	 */
	public function getDiscountType() : string
	{
		if ($this->getStrikePrice() > 0) {
			return 'Sale';
		}

		return $this->RABATTART;
	}

	/**
	 * @inheritdoc
	 */
	public function getStrikePrice() : float
	{
		return (float)$this->STATTPREIS;
	}

	/**
	 * Whether this article should be shown in nettozone shop or not.
	 *
	 * @return bool
	 */
	public function isNettozoneActive() : bool
	{
		return ((string)$this->NETTOZONE === 'Y');
	}
}
