<?php

namespace console\models\mobilezone\erp\xml;

use console\models\mobilezone\erp\TariffInterface;
use SimpleXMLElement;

/**
 * Tariff data model for mobilezone ERP data via XML.
 */
class Tariff extends SimpleXMLElement implements TariffInterface
{
	public function getId() : int
	{
		return (int)$this->GID;
	}

	public function getName() : string
	{
		return $this->PLLNAM;
	}

	public function isActive() : bool
	{
		return ((string)$this->STATUS == 'A');
	}

	public function getProviderId() : int
	{
		return (int)$this->SUPPID;
	}

	public function getOfferTypeId() : int
	{
		return (int)$this->TYPEID;
	}

	public function getFulfillmentPartnerId() : int
	{
		return 4;
	}

	/**
	 * @return int In months.
	 */
	public function getContractDuration() : int
	{
		return (int)$this->ADDNO1;
	}

	public function getUpdatedAt() : string
	{
		return $this->UPDSTMP;
	}
}