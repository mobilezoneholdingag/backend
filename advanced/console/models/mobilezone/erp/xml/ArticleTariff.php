<?php

namespace console\models\mobilezone\erp\xml;

use console\models\mobilezone\erp\ArticleTariffInterface;
use SimpleXMLElement;

/**
 * Article data model for mobilezone ERP data via XML.
 */
class ArticleTariff extends SimpleXMLElement implements ArticleTariffInterface
{
	public function getId() : int
	{
		return (int)$this->GID;
	}

	public function getArticleId() : int
	{
		return (int)$this->MATID;
	}

	public function getTariffId() : int
	{
		return (int)$this->PLID;
	}

	public function getOneTimePrice() : float
	{
		return (float)$this->VALUE;
	}

	/**
	 * @inheritdoc
	 * @return string
	 */
	public function getCurrency() : string
	{
		return $this->CURRCODE;
	}

	public function getValidFromDate() : string
	{
		return $this->DATE1;
	}

	/**
	 * @inheritdoc
	 */
	public function getValidUntilDate()
	{
		return (string)$this->DATE2 ?? null;
	}

	public function isActive() : bool
	{
		return ((string)$this->STATUS === 'A');
	}

	public function getUpdatedAt() : string
	{
		return $this->UPDSTMP;
	}
}