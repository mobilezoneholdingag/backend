<?php

namespace console\controllers;

use common\models\Manufacturer;
use deinhandy\picasso\File;
use deinhandy\yii2picasso\models\Image as PicassoImage;
use DirectoryIterator;
use Yii;
use yii\console\Controller;

/**
 * Class PicassoController
 *
 * IMPORTANT NOTE: CAN BE REMOVED AFTER INITIAL IMPORT OF ALL IMAGES TO PICASSO!
 *
 * @package console\controllers
 */
class PicassoController extends Controller
{
	public function actionImportManufacturerImages()
	{
		$error = [];
		printf("start traversing image dir\n");

		// iterate upload dir
		foreach (new DirectoryIterator('./console/upload/manufacturer') as $file) {
			if ($file->isDot()) {
				continue;
			}

			$extension = strtolower($file->getExtension());
			$filename = $file->getBasename('.' . $extension);

			printf("working on $filename \n");

			// note: reduced complexity due to bad naming scheme in example data
			$pattern = '/icon_hers_(\w*)/';
			preg_match($pattern, $filename, $matches);

			// skip if no matno
			if (empty($matches)) {
				continue;
			}

			$manufacturerName = $matches[1];

			/* @var Manufacturer $manufacturer */
			$manufacturer = Manufacturer::find()->where(['title' => $manufacturerName])->one();

			// skip if no corresponding article can be found for matno
			if (empty($manufacturer)) {
				continue;
			}

			$imid = $this->uploadFileToPicasso($manufacturerName . '.' . $extension, $file->getRealPath(), 'manufacturer');
			$manufacturer->image = new PicassoImage(compact('imid'));

			$error[$manufacturer->title] = $manufacturer->save();
		}

		if (!empty($error)) {
			print_r($error);
		}
	}

	public function actionImportInsurancesImages()
	{
		$error = [];
		printf("start traversing image dir\n");

		// iterate upload dir
		foreach (new DirectoryIterator('./console/upload/manufacturer') as $file) {
			if ($file->isDot()) {
				continue;
			}

			$extension = strtolower($file->getExtension());
			$filename = $file->getBasename('.' . $extension);

			printf("working on $filename \n");

			// note: reduced complexity due to bad naming scheme in example data
			$pattern = '/insurances_(\w*)/';
			preg_match($pattern, $filename, $matches);

			// skip if no matno
			if (empty($matches)) {
				continue;
			}

			$manufacturerName = $matches[1];

			/* @var Manufacturer $manufacturer */
			$manufacturer = Manufacturer::find()->where(['title' => $manufacturerName])->one();

			// skip if no corresponding article can be found for matno
			if (empty($manufacturer)) {
				continue;
			}

			$imid = $this->uploadFileToPicasso($manufacturerName . '.' . $extension, $file->getRealPath(), 'manufacturer');
			$manufacturer->insurance_image = new PicassoImage(compact('imid'));

			$error[$manufacturer->title] = $manufacturer->save();
		}

		if (!empty($error)) {
			print_r($error);
		}
	}

	/**
	 * @param string $fileName the upladed files basename
	 * @param string $tempName the current file location
	 * @param string $collectionName the picasso collection´s name
	 *
	 * @return string
	 */
	private function uploadFileToPicasso($fileName, $tempName, $collectionName = 'images'): string
	{
		$accountNamespace = Yii::$app->params['picasso_namespace'];
		/** @var \deinhandy\picasso\Client $picassoClient */
		$picassoClient = Yii::$container->get(\deinhandy\picasso\Client::class);
		$file = new File($fileName, fopen($tempName, 'r'));

		return $picassoClient->upload($file, $accountNamespace, $collectionName);
	}
}
