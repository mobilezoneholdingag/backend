<?php

namespace console\controllers;

use common\models\Article;
use common\models\Tariffs;
use GuzzleHttp\Client;
use SimpleXMLElement;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * TODO: Delete this controller, once all imports have ran successfully
 */
class ContentServController extends Controller
{
	public function actionImportTariffs()
	{
		$this->info('Starting import ...');

		$client = new Client([
			'base_uri' => 'http://cs.mobilezone.ch/mobilezone/rest/',
			'auth' => ['nils.roedelbronn', 'mobilezone'],
		]);

		$importedTariffIds = [];

		for ($pageCounter = 0;;$pageCounter++) {

			$start = $pageCounter * 20;

			$this->info('Current options: page: ' . $pageCounter . ' start: ' . $start);

			$page = $client->get('offers.xml?mz_Offer.state=active&:limit=' . $start . ',20');

			$xml = (string)$page->getBody();

			$xml = new SimpleXMLElement($xml);

			if (empty($xml->offer)) {
				$this->info('There seem to be no more offers on this page - exiting!');
				break;
			}

			foreach ($xml->offer as $offer) {

				$externalId = (string)$offer->external_id;
				$importedTariffIds[] = $externalId;

				$tariff = Tariffs::findOne($externalId);
				if (empty($tariff)) {
					$this->error('No tariff found in our DB for offer with external_id "' . $externalId . '" - skipping.');
					continue;
				}

				$tariff->swisscom_id = empty((string)$offer->swisscom_id) ? $tariff->swisscom_id : (string)$offer->swisscom_id;

				$dataVolumeInMb = $this->getFormattedDataVolumeInMegabyte($offer->data_credit_de);

				if (empty($dataVolumeInMb) === false) {
					$tariff->data_volume = $dataVolumeInMb;
				}

				$tariff->save(false);
			}
		}

		// Find imports that are in our DB but have NOT been imported (especially important for "unlimited" data volume,
		// which stays empty if the import does not run for those tariffs)
		$nonImportedTariffIds = Tariffs::find()->select('id')->where(['not in', 'id', $importedTariffIds])->asArray()->all();
		$flatArray = [];
		foreach ($nonImportedTariffIds as $id) {
			$flatArray[] = $id['id'];
		}
		$this->info('Non-imported tariff IDs: ' . implode(', ', $flatArray));

		$nonImportedTariffIds = Tariffs::find()->select('id')->where(['not in', 'id', $importedTariffIds])->andWhere('is_active = 1')->asArray()->all();
		$flatArray = [];
		foreach ($nonImportedTariffIds as $id) {
			$flatArray[] = $id['id'];
		}
		$this->info('Non-imported AND ACTIVE tariff IDs: ' . implode(', ', $flatArray));

		$this->info('Import completed.');
	}

	private function getFormattedDataVolumeInMegabyte($dataVolume)
	{
		$result = '';
		$gbPosition = strpos($dataVolume, 'GB');
		if ($gbPosition !== false) {
			$resultInGb = trim(substr($dataVolume, 0, $gbPosition));
			$result = $resultInGb * 1000;
		}

		$mbPosition = strpos($dataVolume, 'MB');
		if ($mbPosition !== false) {
			$result = trim(substr($dataVolume, 0, $mbPosition));
		}

		return $result;
	}

	public function actionImportSpecificDataForSpecificArticles()
	{
		$articlesToImport = Article::find()->select('id')->where(['is_active' => 1])->column();

		$this->info('Starting import ...');

		$client = new Client([
			'base_uri' => 'http://cs.mobilezone.ch/mobilezone/rest/',
			'auth' => ['nils.roedelbronn', 'mobilezone'],
		]);

		foreach ($articlesToImport as $articleId) {

			$this->info('Importing article with ID: ' . $articleId . ' ...');

			$page = $client->get('articles.xml?mz_Article.external_id=' . $articleId . '&:limit=0,20');

			$xml = (string)$page->getBody();

			$xml = new SimpleXMLElement($xml);

			if (empty($xml->article)) {
				$this->error('Article not found in API response.');
				continue;
			}

			$articleFromApi = $xml->article;

			$article = Article::findOne($articleId);

			$manufacturerPartNumber = trim((string)$articleFromApi->mpartno);
			if (empty($manufacturerPartNumber) === false) {
				$article->mpn = $manufacturerPartNumber;
			}

			$article->save(false);
		}

		$this->info('Import completed.');
	}

	public function actionActivateSpecifiedArticles()
	{
		$articleIds = [
			16486,
			16489,
			16487,
			16488,
			16493,
			16496,
			16494,
			16495,
			16497,
			16499,
			16498,
			16490,
			16492,
			16491,
			16500,
			16501,
			16369,
			16368,
			16367,
			16365,
			16363,
			16366,
			16364,
			16324,
			16325,
			16323,
			16094,
			16095,
			15454,
			15453,
			16274,
			16280,
			16279,
			16277,
			16275,
			16278,
			16276,
			16286,
			16287,
			16358,
			16359,
			16355,
			16328,
			16360,
			15890,
			15891,
			15894,
			15897,
			16310,
			16311,
			16341,
			16342,
			16320,
			16321,
			16322,
			16377,
			16378,
			16379,
			13430,
			16211,
			16212,
			16213,
			16214,
			15652,
			15657,
			15658,
			15653,
			15633,
			16374,
			16376,
			16375,
			16265,
			15955,
			15953,
			15954,
			14628,
			14627,
			15676,
			15677,
			15675,
			15350,
			15352,
			15354,
			16242,
			16243,
			11219,
			15127,
			15099,
			15098,
			16239,
			15919,
			15916,
			15917,
			15918,
			15467,
			16249,
			15654,
			15655,
			15915,
			16240,
			15912,
			15913,
			15914,
			12299,
			16482,
			11265,
			15938,
			15939,
			14626,
			14621,
			11413,
			11412,
			15569,
			15570,
			12796,
			15689,
			15690,
			15692,
			15693,
			16104,
			16106,
			16110,
			16108,
			13427,
			12794,
			13042,
			16253,
			16466,
			16254,
			16468,
			16469,
			15738,
			15739,
			16096,
			15339,
			16271,
			16272,
			16273,
			16285,
			15774,
			15778,
			16188,
			16189,
			16190,
			16191,
			16197,
			16198,
			16356,
			16357,
			16354,
			16327,
			16326,
			16092,
			15848,
			15852,
			15850,
			15931,
			15932,
			15933,
			15934,
			15901,
			15902,
			15900,
			15898,
			15899,
			16305,
			16307,
			16306,
			16304,
			16337,
			16338,
			16317,
			16318,
			16319,
			16329,
			16330,
			16331,
			15701,
			16169,
			15703,
			16170,
			15704,
			16158,
			15700,
			15702,
			16201,
			15681,
			15682,
			15573,
			14112,
			14102,
			14079,
			15634,
			16174,
			15643,
			15644,
			16176,
			15650,
			15647,
			15648,
			15641,
			15635,
			15638,
			16346,
			16347,
			16350,
			16351,
			16371,
			16372,
			16373,
			15253,
			11658,
			16199,
			15828,
			11655,
			15829,
			15037,
			15036,
			13049,
			15287,
			16258,
			16263,
			16259,
			16264,
			16262,
			16260,
			16261,
			16257,
			16281,
			16282,
			16283,
			16049,
			15698,
			9051,
			11249,
			11247,
			16483,
			12678,
			16055,
			15924,
			15920,
			15921,
			16651,
			15922,
			15923,
			15926,
			15925,
			16090,
			16091,
			16238,
			9964,
			14482,
			3491,
			3492,
			3493,
			3074,
			8767,
			12813,
			9913,
			15071,
			15074,
			15073,
			15791,
			15790,
			15789,
			14636,
			15572,
			15694,
			15691,
			16107,
			16111,
			15833,
			16168,
			13823,
			16255,
			16256,
			15773,
			15927,
			15928,
			15929,
			15930,
			15893,
			15892,
			15895,
			15896,
			16099,
			16308,
			16309,
			16339,
			16345,
			16344,
			16343,
			16315,
			16314,
			16313,
			16312,
			15722,
			13429,
			15672,
			15671,
			15670,
			16352,
			16353,
			16348,
			16349,
			12520,
			13766,
			16481,
			15889,
			15887,
			15888,
			14911,
			16244,
			16245,
			16172,
			16173,
			16241,
			15289,
			15911,
			12047,
			14495,
			15809,
			15810,
			14713,
			14712,
			15808,
			14963,
			14966,
			14967,
			16292,
			16246,
			14582,
			16361,
			16362,
			15952,
			15951,
			15948,
			15949,
			15950,
			15945,
			15947,
			15946,
			15944,
			15941,
			15943,
			15942,
			15940,
			15909,
			15910,
			15908,
			15907,
			15906,
			15903,
			15904,
			15905,
			16335,
			16334,
			16336,
			16332,
			16333,
			16298,
			16299,
			16301,
			16300,
			16302,
			16303,
			15566,
			15568,
			15282,
			15283,
			16086,
			16087,
			15010,
			15015,
			14906,
			14630,
			16093,
			15697,
			15696,
			14512,
			14513,
			14515,
			15695,
			13638,
			13643,
			14514,
			15936,
			15937,
			13923,
			16397,
			15090,
			15082,
			15092,
			15091,
			15085,
			14965,
			14964,
			16195,
			16196,
			15124,
			15103,
			15107,
			15105,
			15101,
			15115,
			15100,
		];

		$db = Yii::$app->getDb();
		$this->info('Activating ' . count($articleIds) . ' articles ... ');

		$command = $db->createCommand('UPDATE articles SET is_active = 1 WHERE id IN (' . implode(',', $articleIds) . ')');
		$command->execute();

		$this->info('Done.');
	}

	private function suffixWithDotIfNotAlready($string)
	{
		$string = trim($string);
		if (substr($string, -1, 1) == '.') {
			return $string;
		}
		if (empty($string)) {
			return '';
		}
		return $string . '.';
	}

	private function error($msg)
	{
		$this->stderr($msg . PHP_EOL, Console::FG_RED, Console::BOLD);
	}

	private function info($msg)
	{
		$this->stdout($msg . PHP_EOL, Console::FG_GREEN);
	}
}
