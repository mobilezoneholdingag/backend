<?php
namespace console\controllers;

use common\models\Article;
use common\models\ArticleGroups;
use common\models\ArticleTariffs;
use Yii;
use yii\console\Controller;

/**
 * Class CommandController
 * @package console\controllers
 */
class CommandController extends Controller
{
	public $debug = false;

	/**
	 * Returns the names of valid options for the action (id)
	 * An option requires the existence of a public member variable whose
	 * name is the option name.
	 * Child classes may override this method to specify possible options.
	 *
	 * Note that the values setting via options are not available
	 * until [[beforeAction()]] is being called.
	 *
	 * @param string $actionID the action id of the current request
	 *
	 * @return array the names of the options valid for the action
	 */
	public function options($actionID)
	{
		switch ($actionID) {
			case 'has-new-dependencies':
				return ['debug'];
				break;
		}

		return parent::options($actionID); // TODO: Change the autogenerated stub
	}


	public function actionTestdb()
	{
		$connection = \Yii::$app->db;
		$output = [
			'Driver Name' => $connection->driverName,
			'DSN' => $connection->dsn,
			'user' => $connection->username,
			'pass' => str_split($connection->password, 4)[0] . '*******',
			'attributes' => print_r($connection->attributes)
		];

		foreach ($output as $a => $line) {
			echo $a . ': ' . "\t" . $line . "\n";
		}
	}

	public function actionTestEnv()
	{
		echo 'YII_ENV: ' . YII_ENV . PHP_EOL;
		echo 'YII_ENV_DEV?  ' . (YII_ENV_DEV ? 'true' : 'false') . PHP_EOL;
		echo 'YII_ENV_PROD? ' . (YII_ENV_PROD ? 'true' : 'false') . PHP_EOL;
	}

	/**
	 * Flush the cache.
	 */
	public function actionFlush()
	{
		Yii::$app->cache->flush();
	}

	public function actionTestQuery()
	{
		echo ArticleTariffs::find()
				->innerJoin(['a' => Article::tableName()], 'a.id = article_id')
				->innerJoin(['ag' => ArticleGroups::tableName()], 'a.group_id = ag.id')
				->where('tariff_id = ag.start_tariff_id')
				->count() . PHP_EOL . PHP_EOL;

		return 0;
	}

	protected function getReleaseTags()
	{
		$mostRecent = trim(shell_exec('git describe --tags --abbrev=0 --match "r[0-9]*"'));

		preg_match('/r(\d+)/', $mostRecent, $match);

		$releases = [];

		if (count($match) >= 2) {
			$newRelease = 'r' . $match[1];
			$oldRelease = 'r' . ($match[1] - 1);

			$releases = [$oldRelease, $newRelease];
		}

		return $releases;
	}

	public function actionHasNewDependencies()
	{
		$releases = $this->getReleaseTags();
		if ((bool)$this->debug)
			var_dump($releases);

		$cmd = 'cd ' . Yii::getAlias('@app') . ' && '
			. 'git diff --name-only ' . join(' ', [$releases[1], 'HEAD'])
			. ' | grep composer.json '
			. ' | wc -l';
		if ((bool)$this->debug)
			echo $cmd . PHP_EOL;

		$hasChangedDependencies = (int)shell_exec($cmd);

		if ($hasChangedDependencies > 0) {
			echo '1';
			return;
		}

		echo '0';
	}

	public function actionMigrateAll()
	{
		$cwd = getcwd();
		chdir(Yii::getAlias('@app/..'));
		$this->stdout('... Migrate for dev/de'.PHP_EOL);
		passthru('php yii migrate/up');
		$this->stdout('... Migrate for dev/ch');
		passthru('php yii migrate/up --db="db02"'.PHP_EOL);
		chdir($cwd);
	}
}
