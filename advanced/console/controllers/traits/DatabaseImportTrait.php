<?php

namespace console\controllers\traits;

use Yii;

trait DatabaseImportTrait
{
	protected $updateArray = [];
	protected $insertArray = [];

	protected function executeInserts(string $table = ''): int
	{
		if (empty($this->insertArray[$table])) {
			return 0;
		}

		$columns = array_keys(current($this->insertArray[$table]));
		$command = Yii::$app->db->createCommand()->batchInsert($table, $columns, $this->insertArray[$table]);
		$this->insertArray[$table] = [];

		return $command->execute();
	}

	protected function executeInsertionsAndUpdpates(string $table)
	{
		$insertedRows = $this->executeInserts($table);
		$updatedRows = $this->executeUpdates($table);

		return [
			'inserted' => $insertedRows,
			'updated' => $updatedRows,
		];
	}

	protected function executeUpdates(string $table = ''): int
	{
		if (empty($this->updateArray[$table])) {
			return 0;
		}

		$updatedCount = 0;
		foreach ($this->updateArray[$table] as $row) {
			$command = Yii::$app->db->createCommand();
			$updatedCount += $command->update($table, $row, ['id' => $row['id']])->execute();
		}
		$this->updateArray[$table] = [];
		return $updatedCount;
	}
}
