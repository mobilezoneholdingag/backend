<?php

namespace console\controllers\traits;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Yii;
use yii\helpers\Console;

trait OutputTrait
{
	/**
	 * Wrapper for Yii::error() and console output.
	 *
	 * @see Yii::error
	 *
	 * @param string $message
	 */
	protected function error($message)
	{
		$this->stderr($message . PHP_EOL, Console::FG_RED, Console::BOLD);
		Yii::error($message, 'mobilezone-erp-controller');
	}

	/**
	 * Wrapper for Yii::info() and console output.
	 *
	 * @see Yii::info
	 *
	 * @param string $message
	 */
	protected function info($message)
	{
		$this->stdout($message . PHP_EOL, Console::FG_GREEN);
		Yii::info($message, 'mobilezone-erp-controller');
	}
	
	public function stderr($string)
	{
		$args = func_get_args();
		array_shift($args);
		$string = Console::ansiFormat($string, $args);
		
		return fwrite(\STDERR, $string);
	}
	
	public function stdout($string)
	{
		$args = func_get_args();
		array_shift($args);
		$string = Console::ansiFormat($string, $args);
			
		return Console::stdout($string);
	}

	/**
	 * @param int $count Total count to count up to.
	 *
	 * @return ProgressBar
	 */
	protected function createProgressBar($count)
	{
		$progress = new ProgressBar(new ConsoleOutput(OutputInterface::VERBOSITY_DEBUG), $count);

		return $progress;
	}
}
