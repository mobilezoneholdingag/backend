<?php

namespace console\controllers;

use common\models\Tariffs;
use components\filesystem\FilesystemProviderInterface;
use components\Locale;
use console\controllers\traits\OutputTrait;
use Generator;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Worksheet;
use yii\console\Controller;
use yii\data\ActiveDataProvider;
use yii\db\Exception;

class DataMigrationController extends Controller
{
    const START_VALUE = 65; // ascii for A
    const FILE_NAME_XLSX = '-data-migration.xlsx';
    const EXCEL_VERSION = 'Excel2007';

    use OutputTrait;

    public function actionTariff()
    {

        $this->info("Starte Export");

        $excel = new PHPExcel();
        $worksheet = $excel->setActiveSheetIndex();

        $this->info("Setze Titel");

        $worksheet->setTitle('Tariff Export');

        /*
         *  ['field', LOCALE]
         *  ['field', ['with' => 'provider']
         *  ['field', ['with' => 'provider', 'locale' => LOCALE]]
         */

        $this->info("Starte Prepare");

        $this->prepare($worksheet, [
            'SMSCREDITBALANCE'                      => ['text_sms', Locale::SUISSE],
            'SMSCREDITBALANCE_MULTIL'               => ['text_sms', Locale::FRANCE],
            'SMSCREDITBALANCE_MULTIL1'              => ['text_sms', Locale::ITALY],
            'MMSPRICEDESCRIPTION'                   => 'price_mms',
            'DATATRAFFICPRICEDESCRIPTION'           => ['text_internet', Locale::SUISSE],
            'DATATRAFFICPRICEDESCRIPTION_MULTIL'    => ['text_internet', Locale::FRANCE],
            'DATATRAFFICPRICEDESCRIPTION_MULTIL1'   => ['text_internet', Locale::ITALY],
            'SHORTDATACREDITBALANCE'                => 'data_volume',
            'SHORTEXTENDEDBENEFITS'                 => false,
            'EXTENDEDBENEFITS_MULTIL'               => false,
            'EXTENDEDBENEFITS_MULTIL1'              => false,
            'SHORTCONDITIONS'                       => ['footnote_text', Locale::SUISSE],
            'CONDITIONS_MULTIL'                     => ['footnote_text', Locale::FRANCE],
            'CONDITIONS_MULTIL1'                    => ['footnote_text', Locale::ITALY],
            'SWISSCOMID_1'                          => 'swisscom_id',
            'SWISSCOMID_2'                          => 'swisscom_id_2',
            'ISACTIVEERP'                           => 'is_active_erp',
            'ISACTIVESHOP'                          => 'is_active',
            'AGELIMIT'                              => ['young_age_limit', ['with' => 'provider']],
            'ABOID'                                 => 'id',
            'DISPLAYNAME'                           => 'title',
            'SHORTDESCRIPTION'                      => false,
            'ACTIVATIONFEE'                         => false,
            'MONTHLYFEE'                            => 'price_monthly',
            'CALLCHARGE'                            => ['text_call', Locale::SUISSE],
            'CALLCHARGE_MULTIL'                     => ['text_call', Locale::FRANCE],
            'CALLCHARGE_MULTIL1'                    => ['text_call', Locale::ITALY],
        ], $this->getTariffs());


        try {

            $this->info("Speichere Datei: tariff" . self::FILE_NAME_XLSX);
            $localFile = $this->saveFile($excel, 'tariff');
        } catch(Exception $e) {
            $this->error("Fehler beim speichern");
        }

    }

    private function prepare(PHPExcel_Worksheet $worksheet, $data, $model) {

        /*
         * Set Header Column
         */

        $this->setCellValues($worksheet, array_keys($data));

        /*
         * Iterate $model
         */

        foreach($model as $k => $m) {

            $re = [];

            foreach($data as $d) {
                if(is_array($d)) {
                    if(!empty($d[1]) && !is_array($d[1])) {
                        $re[] = $m->getTranslatedAttribute($d[0], $d[1]);
                    } elseif(is_array($d[1])) {
                        if(!empty($d[1]['with'])) {
                            if(empty($d[1]['locale'])) {
                                $re[] = $m->{$d[1]['with']}->{$d[0]};
                            } else {
                                $re[] = $m->{$d[1]['with']}->getTranslatedAttribute($d[0], $d[1]['locale']);
                            }
                        }
                    }

                } elseif(empty($d)) {
                    $re[] = null;
                } else {
                    $re[] = $m->$d;
                }

            }

            // $this->setCellValues($worksheet, $re, $k + 2);

            foreach($re as $rek => $rev) {
                $worksheet->setCellValueByColumnAndRow($rek, $k + 2, $rev);
            }

        }

    }

    private function saveFile($excel, $filePrefix = '')
    {
        $objWriter = PHPExcel_IOFactory::createWriter($excel, self::EXCEL_VERSION);
        ob_start();
        $objWriter->save('php://output');
        $excelOutput = ob_get_clean();

        /** @var FilesystemProviderInterface $uploadService */
        $uploadService = di(FilesystemProviderInterface::class);
        $uploadService->fs('exports')->put($filePrefix . self::FILE_NAME_XLSX, $excelOutput);

        return $uploadService->fs('exports')->get($filePrefix . self::FILE_NAME_XLSX)->getPath();

    }


    private function setCellValues(PHPExcel_Worksheet $worksheet, array $values, int $row = 1)
    {
        foreach ($values as $index => $value) {
            $cell = $this->getColumnId($index) . $row;
            $worksheet->setCellValue($cell, $value);
        }
    }


    private function getColumnId(int $index): string
    {
        $ascii = self::START_VALUE + $index;

        if ($ascii > 90) {
            $ascii -= 26;
            return 'A' . chr($ascii);
        }

        return chr($ascii);
    }


    protected function getTariffs(): Generator
    {

        $this->info("Hole Tarife");

        $dataProvider = new ActiveDataProvider();
        $dataProvider->query = Tariffs::find();
        $dataProvider->prepare(true);
        for ($i = 1; $i <= $dataProvider->getPagination()->getPageCount(); $i++) {
            $tariffs = $dataProvider->getModels();
            if (isset($tariffs)) {
                foreach ($tariffs as $tariff) {
                    yield $tariff;
                }
            }
            $dataProvider->getPagination()->setPage($i);
            $dataProvider->prepare(true);
        }
    }

}
