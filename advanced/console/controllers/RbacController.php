<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\rbac\DbManager;

class RbacController extends Controller
{
	/**
	 * Initializes Role Based Access Control (RBAC) role and permission structure
	 */
	public function actionInit()
	{
		/** @var DbManager $auth */
		$auth = Yii::$app->authManager;

		$auth->removeAll(); // prevents DbManager from creating duplicates

		$starlord = $auth->createRole('starlord');
		$auth->add($starlord);

		$admin = $auth->createRole('admin');
		$auth->add($admin);

		$editor = $auth->createRole('editor');
		$auth->add($editor);

		$backoffice = $auth->createRole('backoffice');
		$auth->add($backoffice);

		$user = $auth->createRole('user');
		$auth->add($user);

		$auth->addChild($editor, $user);
		$auth->addChild($admin, $editor);

		$handleUserAccounts = $auth->createPermission('handleUserAccounts');
		$auth->add($handleUserAccounts);
		$auth->addChild($admin, $handleUserAccounts);

		/**
		 * Additional permissions for 'starlord' role. These are even more than editor, in order to have them available
		 * for us, but not even visible for mobilezone 'admins'.
		 */
		$auth->addChild($starlord, $admin);

		$accessModelLogger = $auth->createPermission('accessModelLogger');
		$auth->add($accessModelLogger);
		$auth->addChild($starlord, $accessModelLogger);

		$createManufacturers = $auth->createPermission('createManufacturers');
		$auth->add($createManufacturers);
		$auth->addChild($starlord, $createManufacturers);

		$createProviders = $auth->createPermission('createProviders');
		$auth->add($createProviders);
		$auth->addChild($starlord, $createProviders);

		$createArticles = $auth->createPermission('createArticles');
		$auth->add($createArticles);
		$auth->addChild($starlord, $createArticles);

		$createTariffs = $auth->createPermission('createTariffs');
		$auth->add($createTariffs);
		$auth->addChild($starlord, $createTariffs);

		$createArticleTariffs = $auth->createPermission('createArticleTariffs');
		$auth->add($createArticleTariffs);
		$auth->addChild($starlord, $createArticleTariffs);

		$manageProductCategory = $auth->createPermission('manageProductCategory');
		$auth->add($manageProductCategory);
		$auth->addChild($starlord, $manageProductCategory);

		$manageStarlordUsers = $auth->createPermission('manageStarlordUsers');
		$auth->add($manageStarlordUsers);
		$auth->addChild($starlord, $manageStarlordUsers);
	}
}
