<?php

namespace console\controllers;

use common\models\Tariffs;
use components\filesystem\FilesystemProviderInterface;
use components\Locale;
use Generator;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Worksheet;
use yii\console\Controller;
use yii\data\ActiveDataProvider;

class TariffExportController extends Controller
{
	const START_VALUE = 65; // ascii for A
	const FILE_NAME_XLSX = 'tariffs.xlsx';
	const EXCEL_VERSION = 'Excel2007';

	public function actionExport()
	{
		$excel = new PHPExcel();
		$worksheet = $excel->setActiveSheetIndex();

		$worksheet->setTitle('Tariff Export');
		$this->setHeadColumn($worksheet);

		$row = 2;
		foreach ($this->getTariffs() as $tariff) {
			$this->setTariffColumn($worksheet, $tariff, $row);
			$row++;
		}

		$this->saveFile($excel);
	}

	private function saveFile($excel)
	{
		$objWriter = PHPExcel_IOFactory::createWriter($excel, self::EXCEL_VERSION);
		ob_start();
		$objWriter->save('php://output');
		$excelOutput = ob_get_clean();

		/** @var FilesystemProviderInterface $uploadService */
		$uploadService = di(FilesystemProviderInterface::class);
		$uploadService->fs('exports')->put(self::FILE_NAME_XLSX, $excelOutput);
	}

	private function setCellValues(PHPExcel_Worksheet $worksheet, array $values, int $row = 1)
	{
		foreach ($values as $index => $value) {
			$cell = $this->getColumnId($index) . $row;
			$worksheet->setCellValue($cell, $value);
		}
	}

	private function getColumnId(int $index): string
	{
		$ascii = self::START_VALUE + $index;

		if ($ascii > 90) {
			$ascii -= 26;
			return 'A' . chr($ascii);
		}

		return chr($ascii);
	}

	protected function getTariffs(): Generator
	{
		$dataProvider = new ActiveDataProvider();
		$dataProvider->query = Tariffs::find();
		$dataProvider->prepare(true);
		for ($i = 1; $i <= $dataProvider->getPagination()->getPageCount(); $i++) {
			$tariffs = $dataProvider->getModels();
			if (isset($tariffs)) {
				foreach ($tariffs as $tariff) {
					yield $tariff;
				}
			}
			$dataProvider->getPagination()->setPage($i);
			$dataProvider->prepare(true);
		}
	}

	private function setHeadColumn(PHPExcel_Worksheet $worksheet)
	{
		$this->setCellValues(
			$worksheet,
			[
				'Text_sms - D',
				'Text_sms - F',
				'Text_sms - I',
				'price_mms',
				'text_internet - D',
				'text_internet - F',
				'text_internet - I',
				'data_volume',
				'Text_Highlight - D',
				'Text_Highlight - F',
				'Text_Highlight - I',
				'footnote_text - D',
				'footnote_text - F',
				'footnote_text - I',
				'swisscom_id',
				'is_active_erp',
				'is_active',
				'young_people',
				'Young Age Limit',
				'ID',
				'Erp Title',
				'Shoptitle',
				'price_monthly',
				'text_call - D',
				'text_call - F',
				'text_call - I',
			]
		);
	}

	private function setTariffColumn(PHPExcel_Worksheet $worksheet, Tariffs $tariff, int $row = 2)
	{
		$this->setCellValues(
			$worksheet,
			[
				$tariff->getTranslatedAttribute('text_sms', Locale::SUISSE),
				$tariff->getTranslatedAttribute('text_sms', Locale::FRANCE),
				$tariff->getTranslatedAttribute('text_sms', Locale::ITALY),
				$tariff->price_sms,
				$tariff->getTranslatedAttribute('text_internet', Locale::SUISSE),
				$tariff->getTranslatedAttribute('text_internet', Locale::FRANCE),
				$tariff->getTranslatedAttribute('text_internet', Locale::ITALY),
				$tariff->data_volume,
				null,
				null,
				null,
				$tariff->getTranslatedAttribute('footnote_text', Locale::SUISSE),
				$tariff->getTranslatedAttribute('footnote_text', Locale::FRANCE),
				$tariff->getTranslatedAttribute('footnote_text', Locale::ITALY),
				$tariff->swisscom_id,
				$tariff->is_active_erp,
				$tariff->is_active,
				$tariff->young_people,
				$tariff->provider->young_age_limit,
				$tariff->id,
				$tariff->erp_title,
				$tariff->title,
				$tariff->price_monthly,
				$tariff->getTranslatedAttribute('text_call', Locale::SUISSE),
				$tariff->getTranslatedAttribute('text_call', Locale::FRANCE),
				$tariff->getTranslatedAttribute('text_call', Locale::ITALY),
			],
			$row
		);
	}
}
