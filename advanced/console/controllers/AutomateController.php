<?php

namespace console\controllers;

use backend\models\MarketingPartner;
use common\models\FulfillmentPartners;
use common\models\OrderStatus;
use backend\models\Tracker;
use Yii;
use yii\console\Controller;
use common\models\Sales;

/**
 * Class AutomateController
 * @package console\controllers
 */
class AutomateController extends Controller
{
	/**
	 * Writes a .csv (containing status reports of all Verivox orders of the last 31 days) to Verivox's SFTP server.
	 * We use a stream here, so no files are written on OUR filesystem :)
	 */
	public function actionUploadVerivoxOrderStatus()
	{
		$verivoxSales = Sales::find()
			->where([
				'marketing_partner_id' => MarketingPartner::VERIVOX_ID,
				'fulfillment_partner_id' => FulfillmentPartners::EAM_ID
			])
			->andWhere(['>', 'timestamp_sale', date('Y-m-d', strtotime('-1 month'))])
			->all();

		$eam_order_ids = [];

		if (!$verivoxSales) {
			return false;
		}

		foreach ($verivoxSales as $verivoxSale) {
			$eam_order_ids[] = $verivoxSale->order_id;
		}

		// get array of status details of set of order ids
		$statusArray = OrderStatus::getOrderStatusByIdsForEAMSales($eam_order_ids);
		// get array of raw CSV data of these orders
		$csvData = OrderStatus::getVerivoxCsvArray($statusArray);

		// pass this array to method (which creates a CSV stream) and upload to FTP
		self::uploadToVerivox($csvData);
	}

	/**
	 * Actual upload and csv generation process
	 * @param $csvData
	 */
	private function uploadToVerivox($csvData)
	{
		// create filename (absolute path on remote server)
		$fileName = '/home/deinhandy/export_verivox_' . date('Ymd_His') . '.csv';

		// create SSH-SFTP-connection
		$connection = ssh2_connect(Yii::$app->params['ftp']['verivox']['host'], Yii::$app->params['ftp']['verivox']['port']);
		ssh2_auth_password($connection, Yii::$app->params['ftp']['verivox']['user'], Yii::$app->params['ftp']['verivox']['pass']);
		$sftp = ssh2_sftp($connection);

		// open stream of target file
		$stream = fopen("ssh2.sftp://$sftp$fileName", 'w');

		// walk through csv data, put data into stream
		foreach ($csvData as $row) {
			fputcsv($stream, $row, ';', '"');
		}

		// close stream (necessary, otherwise we might have a corrupt file)
		fclose($stream);
	}

	public function actionUpdateTrackerInSalesWithUnknownBid()
	{
		$sales = Sales::find()->where(['tracker_id' => null])->all();
		$mailInfos = [];

		foreach ($sales as $sale) {
			$sale->tracker_id = Tracker::getTrackerIdBySale($sale);
			if (isset($sale->tracker_id)) {
				$bid = ($sale->bid) ? $sale->bid : Tracker::DIRECT_SALE_TRACKER_CODE;
				if ($sale->save()) {
					echo "added tracker_id for BID '" . $bid . "' to sale " . $sale->id . PHP_EOL;
				} else {
					echo "could not add tracker_id for BID '" . $bid . "' to sale " . $sale->id . PHP_EOL;
				}
			} else {
				$unknown = "unknown BID '" . $sale->bid . "'" . PHP_EOL;
				echo $unknown;
				$mailInfos[$sale->bid] = $unknown;
			}
		}

		if (!empty($mailInfos))
			$this->sendConfirmationMail($mailInfos, 'unknown BIDs in Sales');

		echo "Done." . PHP_EOL;
	}

	public function sendConfirmationMail($data, $subject)
	{
		return Yii::$app->mailer->compose(
			['html' => 'apiChangesInformationMail.php', 'text' => 'apiChangesInformationMail.php'],
			['data' => $data, 'html' => $data]
		)
			->setFrom(Yii::$app->params['mailing.confirmation.senderEmail'])
			->setTo(Yii::$app->params['api.contentChange.mailAddress'])
			->setSubject($subject)
			->send();
	}
}