<?php

namespace console\controllers;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\ProductCategory;
use common\models\Tariffs;
use lajax\translatemanager\models\LanguageSource;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Yii;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\Console;

class ArticleTariffPricesController extends Controller
{
	/**
	 * updates all cached prices for article tariffs having no price_article_once set.
	 */
	public function actionUpdateWithPricetotalonceNotNull()
	{
		/** @var ArticleTariffs[] $articleTariffs */
		$articleTariffs = ArticleTariffs::find()->where(['price_article_once' => null])->all();

		echo count($articleTariffs) . ' article tariffs found.' . PHP_EOL;

		foreach ($articleTariffs as $articleTariff) {
			$articleTariff->updateCachedPrices()
				->save();
			$this->stdout(" - articletariff ID($articleTariff->id) has been updated!" . PHP_EOL, Console::FG_GREEN);
		}

		echo 'all prices updated!' . PHP_EOL;
	}

	/**
	 * updates tall cached prices for article tariffs
	 */
	public function actionUpdateAll($providerId = null, $limit = null)
	{
		$batchSize = 100;
		$paramsMessages = [];

		$query = ArticleTariffs::find();

		if ($providerId !== null) {
			$this->filterByProviderId((int)$providerId, $query);
			$paramsMessages[] = [
				"only Provider ID $providerId items will be processed by set providerId",
				Console::FG_YELLOW,
			];
		}

		if ($limit !== null) {
			$this->filterByLimit((int)$limit, $query);
			$paramsMessages[] = ["only $limit items will be processed by set limit", Console::FG_YELLOW];
		}

		$articleTariffsCount = $query->count();
		$articleTariffBatches = $query->batch($batchSize);

		$this->outputFormatted("$articleTariffsCount article tariffs found in total", Console::FG_BLUE, 1);
		if (!empty($paramsMessages)) {
			$this->outputFormatted($paramsMessages, 0, 2);
		}
		$this->outputFormatted("starting reading in batches (this may take a while!)", Console::FG_RED, 1);

		$progress = $this->createProgressBar($limit ?? $articleTariffsCount);

		foreach ($articleTariffBatches as $batchIterator => $batch) {
			/* @var ArticleTariffs $batchItem */
			foreach ($batch as $singleIterator => $batchItem) {
				$progress->advance();
				$batchItem->updateCachedPrices();
				$batchItem->save();
			}
		}

		$progress->finish();
		$this->outputFormatted("All prices updated!", Console::FG_GREEN);

		return Controller::EXIT_CODE_NORMAL;
	}

	private function filterByProviderId(int $providerId, Query $query)
	{
		$query->select(ArticleTariffs::tableName() . '.*')
			->leftJoin(Tariffs::tableName() . ' as t', 't.id = tariff_id')
			->andWhere(['t.provider_id' => $providerId]);
	}

	private function filterByLimit(int $limit, Query $query)
	{
		$query->limit($limit);
	}

	private function outputFormatted($message = '', int $colorCode = Console::FG_GREEN, $depth = 0, $isRecursive = false) {
		$heightIndicator = "|";
		$depthIndicator = "\\";
		$depthEmptyString = " ";

		if (is_array($message)) {
			foreach ($message as $messageElem) {
				$this->outputFormatted($messageElem[0], $messageElem[1], $depth + 2, true);
			}
		} else {
			$newDepth = $depth - 1;
			$newDepth = $newDepth < 0 ? 0 : $depth;

			$this->stdout(
				($isRecursive ? '' : str_repeat($depthEmptyString, $newDepth) . $heightIndicator . PHP_EOL) .
				($isRecursive ? '' : str_repeat($depthEmptyString, $newDepth + 1) . $depthIndicator . PHP_EOL) .
				str_repeat($depthEmptyString, $newDepth + 2) . ' - ' . $message . PHP_EOL,
				$colorCode
			);
		}
	}

	private function createProgressBar($count)
	{
		$progress = new ProgressBar(new ConsoleOutput(OutputInterface::VERBOSITY_DEBUG), $count);

		return $progress;
	}

	public function actionUpdateTariffPriceMonthly(int $tariffId, $price)
	{
		$t = Tariffs::findOne($tariffId);
		$t->price_monthly = $price;
		$t->save();
	}

	public function actionKolja()
	{
		/*
		 * REPLACES THIS:
		 *
		 * -- SELECT articles.id AS article_id, articles.product_name, image_count
SELECT *
FROM mobilezone.articles
LEFT JOIN article_image AS outer_ai ON (outer_ai.article_id = articles.id)
LEFT JOIN (
	SELECT COUNT(*) AS image_count, article_image.article_id AS bla_article_id
	FROM article_image
    LEFT JOIN images AS blubb ON (image_id = blubb.id)
    WHERE imageFile IS NOT NULL
    )
    AS bla ON (bla.bla_article_id = outer_ai.article_id)

WHERE
is_active = 1
AND article_id = 13621

GROUP BY article_id;


SELECT COUNT(*) AS image_count, article_image.article_id FROM mobilezone.article_image
LEFT JOIN images ON (article_image.image_id = images.id)
WHERE article_id = 13621 AND imageFile IS NOT NULL;
		 */

		$query = Article::find()->where(['is_active' => 1]);

		$batchSize = 10;
		$paramsMessages = [];

		$articleTariffsCount = $query->count();
		$articleTariffBatches = $query->batch($batchSize);

		$this->outputFormatted("$articleTariffsCount article tariffs found in total", Console::FG_BLUE, 1);
		if (!empty($paramsMessages)) {
			$this->outputFormatted($paramsMessages, 0, 2);
		}
		$this->outputFormatted("starting reading in batches (this may take a while!)", Console::FG_RED, 1);

		$progress = $this->createProgressBar($limit ?? $articleTariffsCount);

		$csv = [];

		foreach ($articleTariffBatches as $batchIterator => $batch) {
			/* @var Article $batchItem */
			foreach ($batch as $singleIterator => $batchItem) {
				$progress->advance();

				$line = [];
				$line['article_id'] = $batchItem->id;
				$line['product_name'] = $batchItem->product_name;
				$line['category_id'] = $batchItem->product_category_id;
				$line['category_id'] = $batchItem->productCategory->type;
				$line['image_count'] = $batchItem->images->count;

				$csv[] = $line;
			}
		}

		$progress->finish();
		$this->outputFormatted("All prices updated!", Console::FG_GREEN);

		foreach ($csv as $line) {
			echo implode(';', $line) . PHP_EOL;
		}

		return Controller::EXIT_CODE_NORMAL;
	}

	public function actionKolja2()
	{
		$query = Article::find()->where(['not in', 'product_category_id', [ProductCategory::SMARTPHONE, ProductCategory::TABLET]]);

		$batchSize = 10;

		$articleTariffsCount = $query->count();
		$articleTariffBatches = $query->batch($batchSize);

		$progress = $this->createProgressBar($articleTariffsCount);

		foreach ($articleTariffBatches as $batchIterator => $batch) {
			/* @var Article $batchItem */
			foreach ($batch as $singleIterator => $batchItem) {
				$progress->advance();

				/** @var LanguageSource $langSource */
				$langSource = LanguageSource::findOne(['message' => 'common\models\Article_|_' . $batchItem->id . '_|_delivery_includes']);
				if ($langSource) {
					$langSource->delete();
				}

				$batchItem->delivery_includes = null;
				$batchItem->save(false);
			}
		}

		$progress->finish();
		$this->outputFormatted("All done!", Console::FG_GREEN);

		return Controller::EXIT_CODE_NORMAL;
	}

	public function actionTransferLocalizationsOfSpecifiedArticles()
	{
		$articleIds = [
			17005,
			17004,
			17003,
			17002,
			17001,
			17000,
			16999,
			16998,
			16997,
			16996,
			16995,
			16994,
			16993,
			16992,
			16991,
			16990,
			16989,
			16988,
			16987,
			16986,
			16985,
			16984,
			16983,
			16982,
			16981,
			16980,
			16979,
			16978,
			16977,
			16976,
			16975,
			16974,
			16973,
			16972,
			16971,
			16970,
			16969,
			16968,
			16967,
			16966,
			16965,
			16964,
			16963,
			16962,
			16961,
			16960,
			16959,
			16958,
			16957,
			16956,
			16955,
			16954,
			16953,
			16952,
			16951,
			16950,
			16949,
			16948,
			16947,
			16946,
			16945,
			16944,
			16943,
			16942,
			16941,
			16940,
			16939,
			16938,
			16937,
			16936,
			16935,
			16934,
			16931,
			16930,
			16929,
			16928,
			16927,
			16926,
			16925,
		];

		$query = Article::find()->where(['in', 'ID', $articleIds]);

		$batchSize = 10;

		$articleTariffsCount = $query->count();
		$articleTariffBatches = $query->batch($batchSize);

		$progress = $this->createProgressBar($articleTariffsCount);

		foreach ($articleTariffBatches as $batchIterator => $batch) {
			/* @var Article $batchItem */
			foreach ($batch as $singleIterator => $batchItem) {
				$progress->advance();

				foreach (Yii::$app->params['languages'] as $lang) {
					if (empty($batchItem->product_name) == false) {
						$batchItem->translationsArr['product_name'][$lang]['language'] = $lang;
						$batchItem->translationsArr['product_name'][$lang]['translation'] = $batchItem->product_name;
					}
				}

				$batchItem->save(false);
			}
		}

		$progress->finish();
		$this->outputFormatted("All done!", Console::FG_GREEN);

		return Controller::EXIT_CODE_NORMAL;
	}
}
