<?php
namespace console\controllers;

use console\controllers\mobilezoneerp\ExportOrderListXmlToFilesystemAction;
use console\controllers\mobilezoneerp\ExportOrderXmlForOrderAction;
use console\controllers\mobilezoneerp\ExportRenewalRequestsXmlToFilesystemAction;
use console\controllers\mobilezoneerp\ImportArticlesAction;
use console\controllers\mobilezoneerp\ImportArticleTariffsAction;
use console\controllers\mobilezoneerp\ImportManufacturersAction;
use console\controllers\mobilezoneerp\ImportOfferTypesAction;
use console\controllers\mobilezoneerp\ImportOrderInfoAction;
use console\controllers\mobilezoneerp\ImportProductCategoriesAction;
use console\controllers\mobilezoneerp\ImportProvidersAction;
use console\controllers\mobilezoneerp\ImportStocksAction;
use console\controllers\mobilezoneerp\ImportTariffsAction;
use console\controllers\mobilezoneerp\SetValidUntilDateForLogicallyObsoleteArticleTariffsAction;
use yii\console\Controller;

/**
 * @package console\controllers
 */
class MobilezoneErpController extends Controller
{
	public function actions()
	{
		return [
			'export-order-list-xml-to-filesystem' => ExportOrderListXmlToFilesystemAction::class,
			'export-order-xml-for-order' => ExportOrderXmlForOrderAction::class,
			'export-renewal-requests-xml-to-filesystem' => ExportRenewalRequestsXmlToFilesystemAction::class,
			'import-articles' => ImportArticlesAction::class,
			'import-article-tariffs' => ImportArticleTariffsAction::class,
			'import-manufacturers' => ImportManufacturersAction::class,
			'import-offer-types' => ImportOfferTypesAction::class,
			'import-order-info' => ImportOrderInfoAction::class,
			'import-product-categories' => ImportProductCategoriesAction::class,
			'import-providers' => ImportProvidersAction::class,
			'import-stocks' => ImportStocksAction::class,
			'import-tariffs' => ImportTariffsAction::class,
			'set-valid-until-date-for-logically-obsolete-article-tariffs' =>
				SetValidUntilDateForLogicallyObsoleteArticleTariffsAction::class,
		];
	}
}
