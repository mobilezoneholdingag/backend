<?php

namespace console\controllers;

use common\models\NewsletterSubscriber;
use components\filesystem\ExportService;
use League\Csv\Writer;
use yii\console\Controller;

class NewsletterController extends Controller
{
	public function actionExportSubscribers()
	{
		$writer = Writer::createFromFileObject(new \SplTempFileObject());
		$writer->setDelimiter(',');
		$writer->setEnclosure('"');
		$writer->setNewline("\r\n");
		$writer->setEncodingFrom("utf-8");
		$writer->setOutputBOM(Writer::BOM_UTF8);

		foreach (NewsletterSubscriber::find()->batch(30) as $subscribers) {
			foreach ($subscribers as $subscriber) {
				$row = [
					$subscriber->id,
					$subscriber->customer_id,
					$subscriber->statusLabel,
					$subscriber->customerEmail,
					$subscriber->customerGenderLabel,
					$subscriber->customerFirstName,
					$subscriber->customerLastName,
					$subscriber->customerLanguage,
				];

				$writer->insertOne($row);
			}

			gc_collect_cycles();
		}

		di(ExportService::class)->save(NewsletterSubscriber::LIST_CSV_NAME, $writer);
	}
}
