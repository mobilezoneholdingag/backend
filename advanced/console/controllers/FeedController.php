<?php
/**
 * Our host is Rackspace. Praise the lord of the clouds!
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace console\controllers;

use common\models\Article;
use common\models\Tariffs;
use components\services\ExportService;
use components\services\ExportService\partners\BasePartner;
use OpenCloud\ObjectStore\Resource\Container;
use OpenCloud\Rackspace;
use yii\console\Exception;
use yii\console\Controller;
use common\models\Export;
use yii\helpers\Console;
use yii;

class FeedController extends Controller {

	/** @var string Identify the feed to obtain
	 */
	public $feedKey = '';

	/** @var string File Name to store the cloud file with.
	 */
	public $fileName = '';

	/** @var string Server to obtain the file from. (Optional.)
	 */
	public $server = '162.13.176.65';

	/** @var Rackspace The Rackspace client interface */
	protected $client;

	protected $objectStoreService;

	/** @var Container */
	protected $container;

	const CLOUD_EXPORT = 'CF_EXPORTS';

	public function __construct($id, $module, $config = [])
	{
		try {
			$this->client = new Rackspace(Rackspace::UK_IDENTITY_ENDPOINT, \Yii::$app->params['host']['rackspace']['credentials']);
		} catch(\Exception $e) {
			$this->client = false;
		}

		if ($this->client) {
			$this->objectStoreService = $this->client->objectStoreService(null, 'LON');
		}

		parent::__construct($id, $module, $config);
	}

	protected function prepareContainerForExport()
	{
		$this->container = $this->objectStoreService->getContainer($this->getFeedContainerName());
		return $this->container;
	}

	/**
	 * Return an array with the directory configuration of the feeds
	 *
	 * @return array
	 */
	protected function getFeedParams()
	{
		return yii::$app->params['host']['rackspace']['directories']['feeds'];
	}

	/**
	 * Returns the Rackspace container name for the feeds
	 *
	 * @return string
	 */
	protected function getFeedContainerName()
	{
		$params = $this->getFeedParams();
		return $params['container']['name'];
	}

	public function options($actionID)
	{
		if ($actionID == 'provide')
			return ['feedKey', 'fileName', 'server'];

		return parent::options($actionID);
	}

	public function actionListPartners()
	{
		/** @var ExportService $service */
		$service = \Yii::$app->get('export');

		if ($service)
		{
			print_r($service->getPartnersListRaw());
		}
	}

	/**
	 * Executes all exports, which are active. The files will be written to the configured location.
	 *
	 * @return int
	 * @throws Exception
	 * @throws \Exception
	 * @throws yii\base\InvalidConfigException
	 */
	public function actionProvide()
	{
		if (empty($this->feedKey)) {
			echo $this->ansiFormat('Insufficient parameters.', Console::FG_RED);
			return 1;
		}

		if (!$this->client) {
			echo $this->ansiFormat('Unconfigured client.', Console::FG_RED);
			return 1;
		}

		/** @var ExportService|false $exportService */
		$exportService = \Yii::$app->get('export');
		if (!$exportService)
			throw new Exception('ExportService not initialized');

		$this->prepareContainerForExport();

		$fileName = $this->fileName;
		/** @var Export $export */
		if (($export = Export::findOne(['key' => $this->feedKey])) == false)
			throw new \Exception('Feed key not found. (Export not configured.)');

		if (empty($fileName)) {
			$fileName = $export->getFilename();
		}

		$f = 'php://temp';

		if (($partners = $exportService->exportToStreams([$this->feedKey => $f])) !== false) {
			/** @var BasePartner $partner */
			foreach($partners as $partner) {
				$f = $partner->format->handle;
				fseek($f, 0);
				stream_set_blocking($f, 0);
				stream_set_timeout($f, 576);

				$object = $this->container->uploadObject($fileName, $f);

				echo PHP_EOL . $this->ansiFormat($object->getPublicUrl(), Console::UNDERLINE) . PHP_EOL;
				$export->link = $object->getPublicUrl();

				$export->save(true, ['link', 'last_run', 'runs_today']);
				fclose($f);
			}
		}

		return 0;
	}

	public function actionRefreshAll()
	{
		$exports = Export::find()->where(['status' => true])->all();
		$failure = false;

		echo PHP_EOL.$this->ansiFormat('Start publishing.', Console::FG_CYAN).PHP_EOL;
		/** @var Export $export */
		foreach($exports as $export)
		{
			echo $this->ansiFormat('Publishing '.$export->partner.' feed...', Console::FG_YELLOW);
			if ($this->runAction('provide', ['feedKey' => $export->key]) === 0) {
				echo $this->ansiFormat(' finished.', Console::FG_GREEN).PHP_EOL;
			} else {
				echo $this->ansiFormat(' failed.', Console::FG_RED).PHP_EOL;
				$failure = true;
			}
		}

		if ($failure) {
			echo $this->ansiFormat('Feeds refreshed with errors.', Console::FG_PURPLE).PHP_EOL;
		} else {
			echo $this->ansiFormat('All feeds refreshed.', Console::FG_GREEN).PHP_EOL;
		}
	}

	public function actionCreateIndex()
	{
		$path = \Yii::getAlias(\Yii::$app->params['searchIndex']['directory']);
		$devicesIndexFilename = \Yii::getAlias(join('/', [ $path, \Yii::$app->params['searchIndex']['files']['devices'] ]));
		$tariffsIndexFilename = \Yii::getAlias(join('/', [ $path, \Yii::$app->params['searchIndex']['files']['tariffs'] ]));

		if (is_file($path)) {
			throw new \Exception('Index directory is a file.');
		}

		if (is_dir($path) == false && mkdir($path, 0750, true) !== false) {
			touch($devicesIndexFilename);
			touch($tariffsIndexFilename);
		}

		// Index articles

		$articleQuery = join(' ', [
			'SELECT DISTINCT article_id,',
			"( SELECT DISTINCT 1 FROM charts
			LEFT JOIN sets
			ON sets.id = charts.set_id
			LEFT JOIN product_category
			ON product_category.id = sets.product_category_id
			WHERE a.id = charts.product_id
			AND product_category.related_class = 'Article'
			AND sets.system_title LIKE 'top_%' LIMIT 1) AS isTop",
			'FROM article_tariffs AS at',
			'LEFT JOIN '.Article::tableName().' AS a',
			'ON a.id = at.article_id',
			'WHERE a.is_active = '.true
		]);

		$drArticleIds = \Yii::$app->db->createCommand($articleQuery)->queryAll();
		$articleIds = [];
		$topArticleIds = [];

		foreach ($drArticleIds as $idListEntry) {
			$articleIds[] = $idListEntry['article_id'];
			if ($idListEntry['isTop'] == 1)
				$topArticleIds[] = $idListEntry['article_id'];
		}

		/** @var Article[] $articles */
		$articles = Article::find()->where(['id' => $articleIds])->all();

		usort($articles, function($e1, $e2) use ($topArticleIds) {
			/** @var Article $e1 */
			/** @var Article $e2 */
			$r = 0;

			if (in_array($e1->id, $topArticleIds)) {
				$r--;
			}

			if (in_array($e2->id, $topArticleIds)) {
				$r++;
			}

			return $r;
		});

		// TODO CSV Service
		$fhDevices = fopen($devicesIndexFilename, 'w');
		rewind($fhDevices);
		foreach($articles as $device) {
			$indexedDevices = [
				'id'            => $device->id,
				'manufacturer'  => $device->manufacturerTitle,
				'name'          => $device->shortName,
				'memory'        => $device->memory.'GB',
				'color'         => $device->color,
				'color_label'   => $device->color_label,
				'top'           => in_array($device->id, $topArticleIds) ? 'is_top' : 'no_top'
			];
			if(ftell($fhDevices) === 0) {
				fputcsv($fhDevices, array_keys($indexedDevices));
			}
			fputcsv($fhDevices, $indexedDevices);
		}
		fclose($fhDevices);

		// Index tariffs
		// - redundant code to above, but could be handled in a different way
		$tariffQuery = join(' ', [
			'SELECT DISTINCT tariff_id,',
			"( SELECT DISTINCT 1 FROM charts
			LEFT JOIN sets
			ON sets.id = charts.set_id
			LEFT JOIN product_category
			ON product_category.id = sets.product_category_id
			WHERE t.id = charts.product_id
			AND product_category.related_class = 'Tariff'
			AND sets.system_title LIKE 'top_%' LIMIT 1) AS isTop",
			'FROM article_tariffs AS at',
			'LEFT JOIN '.Tariffs::tableName().' AS t',
			'ON t.id = at.tariff_id',
			'WHERE t.is_active = '.true
		]);


		$drTariffIds = \Yii::$app->db->createCommand($tariffQuery)->queryAll();
		$tariffIds = [];
		$topTariffIds = [];

		foreach ($drTariffIds as $idListEntry) {
			$tariffIds[] = $idListEntry['tariff_id'];
			if ($idListEntry['isTop'] == 1)
				$topTariffIds[] = $idListEntry['tariff_id'];
		}

		/** @var Tariffs[] $tariffs */
		$tariffs  = Tariffs::find()->where(['id' => $tariffIds])->all();

		usort($tariffs, function($e1, $e2) use ($topTariffIds) {
			/** @var Article $e1 */
			/** @var Article $e2 */
			$r = 0;

			if (in_array($e1->id, $topTariffIds)) {
				$r--;
			}

			if (in_array($e2->id, $topTariffIds)) {
				$r++;
			}

			return $r;
		});

		$fhTariffs = fopen($tariffsIndexFilename, 'w');
		rewind($fhTariffs);
		foreach($tariffs as $tariff) {
			$indexedTariffs = [
				'id'            => $tariff->id,
				'provider'      => $tariff->provider->title,
				'altProvider'   => $tariff->provider->system_title,
				'name'          => $tariff->title,
				'top'           => in_array($tariff->id, $topTariffIds) ? 'is_top' : 'no_top'
			];
			if(ftell($fhTariffs) === 0) {
				fputcsv($fhTariffs, array_keys($indexedTariffs));
			}
			fputcsv($fhTariffs, $indexedTariffs);
		}
		fclose($fhTariffs);

		chown($devicesIndexFilename, 'www-data');
		chown($tariffsIndexFilename, 'www-data');
		chgrp($devicesIndexFilename, 'www-data');
		chgrp($tariffsIndexFilename, 'www-data');
		chown($path, 'www-data');
		chgrp($path, 'www-data');
	}


}