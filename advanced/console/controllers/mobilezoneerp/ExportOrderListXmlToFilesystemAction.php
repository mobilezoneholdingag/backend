<?php

namespace console\controllers\mobilezoneerp;

use common\modules\mobilezoneErp\lib\OrderListDecorator;
use common\modules\order\models\Order;
use components\filesystem\ExportService;
use console\controllers\traits\OutputTrait;
use yii\base\Action;

class ExportOrderListXmlToFilesystemAction extends Action
{
	use OutputTrait;

	public function run()
	{
		$orders = Order::find()->all();
		$xml = (new OrderListDecorator($orders))->decorate();

		di(ExportService::class)->save(Order::LIST_XML_NAME, $xml);
	}
}
