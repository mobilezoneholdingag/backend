<?php

namespace console\controllers\mobilezoneerp;

use common\models\OfferType;
use console\controllers\traits\OutputTrait;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use yii\base\Action;

class ImportOfferTypesAction extends Action
{
	use OutputTrait;

	public function run()
	{
		$this->info('Importing offer types ...');

		foreach ((new XmlAdapter())->getOfferTypes() as $apiOfferType) {
			$offerType = OfferType::findOne($apiOfferType->getId());

			if (empty($offerType)) {
				$offerType = new OfferType();
			}

			$offerType->id = $apiOfferType->getId();
			$offerType->title = $apiOfferType->getName();
			$offerType->is_active = $apiOfferType->isActive();
			$offerType->save(false);
		}

		$this->info('Importing offer types done.');
	}
}
