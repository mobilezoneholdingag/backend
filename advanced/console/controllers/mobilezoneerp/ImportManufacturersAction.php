<?php

namespace console\controllers\mobilezoneerp;

use common\models\Manufacturer;
use console\controllers\traits\OutputTrait;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use yii\base\Action;

class ImportManufacturersAction extends Action
{
	use OutputTrait;

	public function run()
	{
		$this->info('Importing manufacturers ...');

		foreach ((new XmlAdapter())->getManufacturers() as $apiManufacturer) {
			$manufacturer = Manufacturer::findOne($apiManufacturer->getId());

			if (empty($manufacturer)) {
				$manufacturer = new Manufacturer();
			}

			if ($manufacturer->api_updated_at >= $apiManufacturer->getUpdatedAt()) {
				continue;
			}

			$manufacturer->id = $apiManufacturer->getId();
			if (empty($manufacturer->title)) { // Title from ERP is only used for initial importing
				$manufacturer->title = $apiManufacturer->getName();
			}
			$manufacturer->erp_title = $apiManufacturer->getName();
			$manufacturer->is_active_erp = $apiManufacturer->isActive();

			$manufacturer->api_updated_at = $apiManufacturer->getUpdatedAt();
			$manufacturer->save(false);
		}

		$this->info('Importing manufacturers done.');
	}
}
