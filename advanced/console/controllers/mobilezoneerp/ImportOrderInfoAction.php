<?php

namespace console\controllers\mobilezoneerp;

use console\controllers\traits\OutputTrait;
use console\models\mobilezone\erp\processor\OrderInfoProcessor;
use yii\base\Action;

class ImportOrderInfoAction extends Action
{
	use OutputTrait;

	public function run()
	{
		/** @var OrderInfoProcessor $orderInfoProcessor */
		$orderInfoProcessor = di(OrderInfoProcessor::class);
		$orderInfoProcessor->process();
	}
}
