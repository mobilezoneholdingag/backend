<?php

namespace console\controllers\mobilezoneerp;

use common\modules\mobilezoneErp\lib\OrderDecorator;
use common\modules\order\models\Order;
use components\services\FtpsService;
use console\controllers\traits\OutputTrait;
use Yii;
use yii\base\Action;

class ExportOrderXmlForOrderAction extends Action
{
	use OutputTrait;

	public function run($orderId)
	{
		$order = Order::findOne($orderId);

		$orderXml = (new OrderDecorator($order))->decorate();

		/** @var FtpsService $ftps */
		$ftps = Yii::$app->get('ftps');
		$ftps->putDataToFtps($orderXml, 'mobilezone-ch-orders/Order_MZ_' . $order->id . '.xml');
	}
}
