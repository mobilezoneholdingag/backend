<?php

namespace console\controllers\mobilezoneerp;

use common\models\Provider;
use console\controllers\traits\OutputTrait;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use yii\base\Action;

class ImportProvidersAction extends Action
{
	use OutputTrait;

	public function run()
	{
		$this->info('Importing providers ...');

		foreach ((new XmlAdapter())->getProviders() as $apiProvider) {
			$provider = Provider::findOne($apiProvider->getId());

			if (empty($provider)) {
				$provider = new Provider();
			}

			if ($provider->api_updated_at >= $apiProvider->getUpdatedAt()) {
				continue;
			}

			$provider->id = $apiProvider->getId();
			$provider->title = $apiProvider->getName();
			$provider->api_updated_at = $apiProvider->getUpdatedAt();
			$provider->save(false);
		}

		$this->info('Importing providers done.');
	}
}
