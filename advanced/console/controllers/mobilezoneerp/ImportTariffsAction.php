<?php

namespace console\controllers\mobilezoneerp;

use common\models\OfferType;
use common\models\Provider;
use common\models\Tariffs;
use console\controllers\traits\DatabaseImportTrait;
use console\controllers\traits\OutputTrait;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use yii\base\Action;

class ImportTariffsAction extends Action
{
	use OutputTrait;
	use DatabaseImportTrait;

	const CHUNK_SIZE = 100;

	public function run()
	{
		$this->info('Importing tariffs ...');
		$this->insertArray['tariffs'] = [];
		$this->updateArray['tariffs'] = [];

		$tariffs = (new XmlAdapter())->getTariffs();

		$progress = $this->createProgressBar(count($tariffs));

		$count = 0;
		foreach ($tariffs as $apiTariff) {
			$apiTariffId = $apiTariff->getId();
			$tariff = Tariffs::findOne($apiTariffId);
			$isNewTariff = false;

			$progress->advance();

			if (empty($tariff)) {
				$tariff = new Tariffs();
				$isNewTariff = true;
			}

			if ($tariff->api_updated_at >= $apiTariff->getUpdatedAt()) {
				// Intentionally commented out as this check is obsolete as of now because evo (ERP) does not properly
				// update the timestamp according to the real changes -> We have to import ALL the data every time
				// therefore - at least for now.
				//continue;
			}

			$providerId = $apiTariff->getProviderId();
			$providerExists = Provider::find()->where(['id' => $providerId])->exists();
			if (!$providerExists) {
				$this->error(
					PHP_EOL . 'Provider #' . $providerId .
					' could not be found when trying to set it to tariff #' . $apiTariffId . '.'
				);
				continue;
			}

			$offerTypeId = $apiTariff->getOfferTypeId();
			$offerTypeExists = OfferType::find()->where(['id' => $offerTypeId])->exists();
			if (!$offerTypeExists) {
				$this->error(
					PHP_EOL . 'OfferType #' . $offerTypeId .
					' could not be found when trying to set it to tariff #' . $apiTariffId . '.'
				);
				continue;
			}

			$tariffAttributes = [
				'id' => $apiTariffId,
				'external_id' => $apiTariffId,
				'title' => $tariff->title ? $tariff->title : $apiTariff->getName(),
				'erp_title' => $apiTariff->getName(),
				'is_active_erp' => $apiTariff->isActive(),
				'is_active' => $apiTariff->isActive(),
				'provider_id' => $providerId,
				'offer_type_id' => $offerTypeId,
				'contract_duration' => $apiTariff->getContractDuration(),
				'api_updated_at' => $apiTariff->getUpdatedAt(),
				'fulfillment_partner_id' => $apiTariff->getFulfillmentPartnerId(),
				'description' => '',
				'flatrate_landline' => '',
				'bill_online' => '',
				'comments' => '',
			];

			$this->{$isNewTariff ? 'insertArray' : 'updateArray'}['tariffs'][] = $tariffAttributes;
			$count++;

			if ($count % self::CHUNK_SIZE == 0) {
				echo PHP_EOL;
				$this->executeInsertionsAndUpdpates('tariffs');
			}
		}

		$progress->finish();
		echo PHP_EOL;

		$this->executeInsertionsAndUpdpates('tariffs');

		$this->info('Importing tariffs done.');
	}
}
