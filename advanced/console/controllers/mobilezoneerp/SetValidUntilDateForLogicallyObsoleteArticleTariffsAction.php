<?php

namespace console\controllers\mobilezoneerp;

use console\controllers\traits\OutputTrait;
use Yii;
use yii\base\Action;

class SetValidUntilDateForLogicallyObsoleteArticleTariffsAction extends Action
{
	use OutputTrait;

	public function run()
	{
		$yesterday = date('Y-m-d', (time() - 86400));

		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("
			UPDATE mobilezone.article_tariffs SET valid_until = :yesterday WHERE id IN (SELECT * FROM (
			SELECT DISTINCT
			at1.id
			FROM
			  mobilezone.article_tariffs AS at1
			  INNER JOIN
			  mobilezone.article_tariffs AS at2
			ON at1.article_id = at2.article_id
			  AND at1.tariff_id = at2.tariff_id
			  AND at1.id != at2.id
			  AND (at1.id < at2.id)
			WHERE (at1.valid_until IS NULL AND at2.valid_until IS NULL)) AS blubb);", [':yesterday' => $yesterday]);

		$affected = $command->execute();

		$this->info("Set valid_until to $yesterday for $affected rows.");
	}
}
