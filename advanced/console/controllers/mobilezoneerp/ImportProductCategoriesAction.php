<?php

namespace console\controllers\mobilezoneerp;

use common\models\ProductCategory;
use console\controllers\traits\OutputTrait;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use yii\base\Action;

class ImportProductCategoriesAction extends Action
{
	use OutputTrait;

	public function run()
	{
		$this->info('Importing product categories ...');

		foreach ((new XmlAdapter())->getProductCategories() as $apiCategory) {
			$category = ProductCategory::findOne($apiCategory->getId());

			if (empty($category)) {
				$category = new ProductCategory();
				$category->related_class = 'Article';
			}

			if ($category->api_updated_at >= $apiCategory->getUpdatedAt()) {
				continue;
			}

			$category->id = $apiCategory->getId();
			$category->type = $apiCategory->getName();
			$category->is_active = $apiCategory->isActive();
			$category->api_updated_at = $apiCategory->getUpdatedAt();
			$category->save(false);
		}

		$this->info('Importing product categories done.');
	}
}
