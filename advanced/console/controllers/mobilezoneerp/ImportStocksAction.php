<?php

namespace console\controllers\mobilezoneerp;

use common\models\Stock;
use console\controllers\traits\OutputTrait;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use yii\base\Action;

class ImportStocksAction extends Action
{
	use OutputTrait;

	public function run()
	{
		$this->info('Importing stocks ...');

		$stocks = (new XmlAdapter())->getStocks();

		$progress = $this->createProgressBar(count($stocks));

		foreach ($stocks as $apiStock) {
			$stock = Stock::findOne($apiStock->getId());

			if (empty($stock)) {
				$stock = new Stock();
			}

			$stock->id = $apiStock->getId();
			$stock->quantity = $apiStock->getQuantity();
			$stock->next_delivery_date = $apiStock->getNextDeliveryDate();
			$stock->next_delivery_amount = $apiStock->getNextDeliveryAmount();
			$stock->save(false);

			$progress->advance();
		}

		$progress->finish();
		echo PHP_EOL;

		$this->info('Importing stocks done.');
	}
}
