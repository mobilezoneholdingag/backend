<?php

namespace console\controllers\mobilezoneerp;

use console\controllers\traits\DatabaseImportTrait;
use console\controllers\traits\OutputTrait;
use yii\base\Action;
use console\models\mobilezone\erp\ArticleInterface;
use console\models\mobilezone\erp\types\AccessorySetTypes;
use common\modules\set\models\Chart;
use common\models\DeliveryStatus;
use common\models\Article;
use common\models\Manufacturer;
use common\models\ProductCategory;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use Yii;

class ImportArticlesAction extends Action
{
	use OutputTrait;
	use DatabaseImportTrait;

	const CHUNK_SIZE = 100;

	public function run()
	{
		try {
			$this->archiveArticlesXml();
		} catch (\Exception $e) {
			$this->error('Archiving failed! ' . $e->getMessage());
		}

		$this->info('Importing articles ...');
		$this->insertArray['articles'] = [];
		$this->insertArray['charts'] = [];
		$this->updateArray['articles'] = [];

		$xmlAdapter = new XmlAdapter();
		$articles = $xmlAdapter->getArticles();

        $this->info('Articles: ' . count($articles));

		$progress = $this->createProgressBar(count($articles));

		$count = 0;
		foreach ($articles as $apiArticle) {
			$progress->advance();

			// the "false" is intentional. same reason for this skip as in tariff import.
			if (false && !$this->articleHasUpdate($apiArticle->getId(), $apiArticle->getUpdatedAt())) {
				continue;
			}

			$isNewArticle = false;
			$apiArticleId = $apiArticle->getId();
			$productCategoryId = $apiArticle->getProductCategoryId();
			$manufacturerId = $apiArticle->getManufacturerId();
			$article = Article::findOne($apiArticleId);

			if (empty($article)) {
				$article = new Article();
				$isNewArticle = true;
				$article->is_active = false;
			}

			if (!$this->isApiArticleValid($apiArticle)) {
				continue;
			}

			$articleAttributes = [];

			$articleAttributes['id'] = $apiArticleId;
			$articleAttributes['product_name'] = $apiArticle->getName();
			$articleAttributes['mat_no'] = $apiArticle->getMatNo();
			$articleAttributes['manufacturer_id'] = $manufacturerId;
			$articleAttributes['manufacturer_article_id'] = $apiArticle->getBarCode();
			$articleAttributes['product_category_id'] = $productCategoryId;
			$articleAttributes['price_sale'] = $apiArticle->getPrice();
			$articleAttributes['sunrisenetprice'] = $apiArticle->getSunriseNetPrice();
			$articleAttributes['is_discountable'] = $apiArticle->isDiscountable();
			$articleAttributes['vat_rate'] = $apiArticle->getVatRate();
			$articleAttributes['is_active_erp'] = $apiArticle->isActive();
			$articleAttributes['api_updated_at'] = $apiArticle->getUpdatedAt();
			$articleAttributes['price_strike'] = $apiArticle->getStrikePrice();
			$articleAttributes['discount_type'] = $apiArticle->getDiscountType();
			$articleAttributes['is_nettozone_active'] = $apiArticle->isNettozoneActive();

			if (!$isNewArticle && !empty($articleAttributes['product_name'])) {
				$this->applyProductName($articleAttributes, $article, $apiArticleId);
			}

			$this->applyDeliveryStatus($isNewArticle, $article, $articleAttributes);

			if ($isNewArticle) {
				$this->addArticleToChart($article);
			}


			$this->{$isNewArticle ? 'insertArray' : 'updateArray'}['articles'][] = $articleAttributes;
			$count++;

			if ($count % self::CHUNK_SIZE == 0) {
				echo PHP_EOL;
				$this->executeInsertionsAndUpdpates('articles');
				$this->executeInsertionsAndUpdpates('charts');
			}
		}

		$progress->finish();
		echo PHP_EOL;

		$this->executeInsertionsAndUpdpates('articles');
		$this->executeInsertionsAndUpdpates('charts');
		$this->info('Importing articles done.');
	}

	private function articleHasUpdate(int $id, string $updatedAt)
	{
		return (bool) Yii::$app->db->createCommand("
			SELECT CASE 
				WHEN (articles.api_updated_at < :updatedAt OR COUNT(articles.id) = 0) THEN 1 ELSE 0 
				END
			FROM articles WHERE `id` = :id
		", [
			'id' => $id,
			'updatedAt' => $updatedAt,
		])->queryScalar();
	}

	private function isApiArticleValid(ArticleInterface $apiArticle): bool
	{
		if ($apiArticle->getCurrency() != 'CHF') {
			$this->error(
				'Articles with non-CHF currency are not supported. Article #' .
				$apiArticle->getId() . ' has currency ' . $apiArticle->getCurrency() . '.'
			);

			return false;
		}

		$manufacturerExists = Manufacturer::find()->where(['id' => $apiArticle->getManufacturerId()])->exists();
		if (!$manufacturerExists) {
			$this->error(
				'Manufacturer #' . $apiArticle->getManufacturerId() .
				' could not be found when trying to set it to article #' . $apiArticle->getId() . '.'
			);

			return false;
		}

		$productCategoryExists = ProductCategory::find()->where(['id' => $apiArticle->getProductCategoryId()])->exists();
		if (!$productCategoryExists) {
			$this->error(
				'Product category #' . $apiArticle->getProductCategoryId() .
				' could not be found when trying to set it to article #' . $apiArticle->getId()
			);

			return false;
		}

		return true;
	}

	private function applyProductName(array &$articleAttributes, Article $article, int $apiArticleId)
	{
		$productNameChanged = $articleAttributes['product_name'] !== $article->product_name;

		if ($productNameChanged) {
			$translations = [];

			foreach (Yii::$app->params['languages'] as $lang) {
				$translations[$lang] = $articleAttributes['product_name'];
			}

			$this->addArticleTranslation($apiArticleId, 'product_name', $translations);
		}
	}

	/**
	 * Delivery status depending on the use case. Please note that this does not necessarily reflect the
	 * delivery status that is shown on the PDP, as this is caluclated in real time depending on count of
	 * sold items
	 *
	 * @param bool $isNewArticle
	 * @param Article $article
	 * @param array $articleAttributes
	 */
	private function applyDeliveryStatus(bool $isNewArticle, Article $article, array &$articleAttributes)
	{
		if (!$isNewArticle && ($article->stock->quantity ?? 0) > 0) {
			$articleAttributes['delivery_status_id'] = DeliveryStatus::ID_IN_STOCK;
		} else {
			if (empty($article->stock->next_delivery_date) === false) {
				$articleAttributes['delivery_status_id'] = DeliveryStatus::ID_REORDERED;
			} else {
				$articleAttributes['delivery_status_id'] = DeliveryStatus::ID_NOT_IN_STOCK;
			}
		}
	}

	private function addArticleTranslation(int $id, string $attribute, array $translations)
	{
		$connection = Yii::$app->db;
		$inserted = (bool) $connection->createCommand()->insert('language_source', [
			'category' => 'database',
			'message' => "common\\models\\Article_|_{$id}_|_{$attribute}"
		])->execute();

		if (!$inserted) {
			$this->error("Language Source for attribute '{$attribute}' of article #{$id} could not be saved!");
			return;
		}

		$translationId = $connection->getLastInsertID();

		if (!$translationId) {
			return;
		}

		$inserted = 0;
		foreach ($translations as $language => $translation) {
			$inserted += $connection->createCommand()->insert('language_translate', [
				'id' => $translationId,
				'language' => $language,
				'translation' => $translation
			])->execute();
		}

		if ($inserted !== 3) {
			$this->error("Only {$inserted} of 3 translations have been saved to the attribute {$attribute} of article #{$id}!");
		}
	}

	private function addArticleToChart(Article $article)
	{
		if (Chart::find()->where(['product_id' => $article->id])->exists()
			|| !array_key_exists($article->product_category_id, AccessorySetTypes::ERP_PRODUCT_GROUP_SET_TYPE_MAP)
		) {
			return;
		}

		$setId = AccessorySetTypes::ERP_PRODUCT_GROUP_SET_TYPE_MAP[$article->product_category_id];
		$maxPos = Chart::find()->where(['set_id' => $setId])->max('position');

		$chart = [
			'set_id' => $setId,
			'position' => $maxPos + 1,
			'product_id' => $article->id,
		];
		$this->insertArray['charts'][] = $chart;
	}

	private function archiveArticlesXml()
	{
		$xmlAdapter = new XmlAdapter();
		$this->info('Archiving articles.xml...');
		$xmlAdapter->archiveFile('articles.xml');
		$this->info('Archiving done!');

		$this->info('Removing old archived files...');
		$removedFileCount = $xmlAdapter->removeArchivedFiles();
		$this->info("Removed {$removedFileCount} Files!");
	}
}
