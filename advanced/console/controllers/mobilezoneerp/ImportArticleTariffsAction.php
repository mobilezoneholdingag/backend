<?php

namespace console\controllers\mobilezoneerp;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use console\controllers\traits\OutputTrait;
use console\models\mobilezone\erp\adapter\XmlAdapter;
use Yii;
use yii\base\Action;
use yii\db\IntegrityException;

class ImportArticleTariffsAction extends Action
{
	use OutputTrait;

	public function run()
	{
		/** @var ArticleTariffs[] $duplicatesInDb */
		$duplicatesInDb = [];

		$this->info('Importing article tariffs ...');

		$articleTariffs = (new XmlAdapter())->getArticleTariffs();

		$progress = $this->createProgressBar(count($articleTariffs));

		if(count($articleTariffs > 0)) {
            $this->info('Truncating Table');
            Yii::$app->db->createCommand()->truncateTable(ArticleTariffs::tableName())->execute();
        }

		foreach ($articleTariffs as $apiArticleTariff) {
			$progress->advance();

			$articleTariffId = $apiArticleTariff->getId();
			$articleTariffHasUpdate = $this->articleTariffHasUpdate($articleTariffId, $apiArticleTariff->getUpdatedAt());

			if (!$articleTariffHasUpdate) {
				continue;
			}

			$articleId = $apiArticleTariff->getArticleId();
			$articleExists = Article::find()->where(['id' => $articleId])->exists();
			if (!$articleExists) {
				$this->error(
					'Article #' . $articleId .
					' could not be found when trying to set it to article tariff #' . $articleTariffId . '.'
				);
				continue;
			}

			$tariffId = $apiArticleTariff->getTariffId();
			$tariffExists = Tariffs::find()->where(['id' => $tariffId])->exists();
			if (!$tariffExists) {
				$this->error(
					'Tariff #' . $tariffId .
					' could not be found when trying to set it to article tariff #' . $articleTariffId . '.'
				);
				continue;
			}

			if ($apiArticleTariff->getCurrency() != 'CHF') {
				$this->error(
					'Articles tariffs with non-CHF currency are not supported. Article tariff #' .
					$articleTariffId . ' has currency ' . $apiArticleTariff->getCurrency() . '.'
				);
				continue;
			}

			$articleTariff = ArticleTariffs::findOne($articleTariffId);
			if (empty($articleTariff)) {
				$articleTariff = new ArticleTariffs();
			}

			$articleTariff->id = $articleTariffId;
			$articleTariff->article_id = $articleId;
			$articleTariff->tariff_id = $tariffId;
			$articleTariff->price = $apiArticleTariff->getOneTimePrice();
			$articleTariff->valid_from = $apiArticleTariff->getValidFromDate();
			$articleTariff->valid_until = $apiArticleTariff->getValidUntilDate();
			$articleTariff->is_active_erp = $apiArticleTariff->isActive();
			$articleTariff->api_updated_at = $apiArticleTariff->getUpdatedAt();
			try {
				$articleTariff->save(false);
			} catch (IntegrityException $e) {
				// We have agreed with mobilezone that duplicates are not processed, e.g. they will not occur in the DB.
				// Since this check relies on the unique index on the database, we are actually saving them at first,
				// but then delete them again once they are identified as duplicates, and once the import is complete
				$this->logDuplicateArticleTariff(
					[
						$articleTariffId,
						$articleId,
						$tariffId,
						$apiArticleTariff->getValidFromDate(),
					]
				);

				$duplicateInDb = ArticleTariffs::findOne(
					[
						'article_id' => $articleId,
						'tariff_id' => $tariffId,
						'valid_from' => $apiArticleTariff->getValidFromDate(),
					]
				);
				$duplicatesInDb[] = $duplicateInDb;

				$this->logDuplicateArticleTariff(
					[
						$duplicateInDb->id,
						$duplicateInDb->article_id,
						$duplicateInDb->tariff_id,
						$duplicateInDb->valid_from,
					]
				);
			}
		}

		$progress->finish();
		echo PHP_EOL;

		foreach ($duplicatesInDb as $duplicate) {
			$duplicate->delete();
		}

		$this->info('Importing article tariffs done.');
	}

	private function articleTariffHasUpdate(int $id, string $updatedAt)
	{
		return (bool) Yii::$app->db->createCommand("
			SELECT CASE 
				WHEN (article_tariffs.api_updated_at != :updatedAt OR COUNT(article_tariffs.id) = 0) THEN 1 ELSE 0 
				END
			FROM article_tariffs WHERE `id` = :id
		", [
			'id' => $id,
			'updatedAt' => $updatedAt,
		])->queryScalar();
	}

	/**
	 * Logs duplicate article tariffs for human assessment.
	 * @param array $articleTariffData
	 */
	private function logDuplicateArticleTariff(array $articleTariffData)
	{
		$this->error('Duplicate ArticleTariff detected (GID;MATID;PLID;DATE1): ' . implode(';', $articleTariffData));
	}
}
