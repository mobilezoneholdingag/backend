<?php

namespace console\controllers\mobilezoneerp;

use common\modules\mobilezoneErp\lib\RenewalRequestsDecorator;
use common\modules\renewalRequest\models\RenewalRequest;
use components\filesystem\ExportService;
use console\controllers\traits\OutputTrait;
use yii\base\Action;

class ExportRenewalRequestsXmlToFilesystemAction extends Action
{
	use OutputTrait;

	public function run()
	{
		$renewalRequests = RenewalRequest::find()->all();
		$xml = (new RenewalRequestsDecorator($renewalRequests))->decorate();

		di(ExportService::class)->save(RenewalRequest::LIST_XML_NAME, $xml);
	}
}
