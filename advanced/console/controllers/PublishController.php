<?php
namespace console\controllers;


use Yii;
use yii\console\Controller;
use yii\db\Connection;
use yii\helpers\Console;

/**
 * Class PublishController
 * @package console\controllers
 */
class PublishController extends Controller
{
	public $ignoreMigration = false;
	public $defaultAction = 'publish';
	/**
	 * @var Connection
	 */
	protected $db1;
	/**
	 * @var Connection
	 */
	protected $db2;
	protected $exportCommandTemplate = 'mysqldump -u {username} -p{password} -h {host} {dbname} {tbl_names}';
	protected $importCommandTemplate = 'mysql -u {username} -p{password} -D {dbname} -h {host}';
	protected $dumpFile = '';
	protected $tables = [
		'accessory_category_set',
		'accessory_category',
		'article_details',
		'article_details_group',
		'article_details_values',
		'article_groups',
		'article_groups_insurance',
		'article_groups_sim',
		'article_groups_tariff',
		'articles',
		'articles_marketing_partners',
		'article_tariffs',
		'article_tariffs_keep_list',
		'charts',
		// 'coupons_normal',
		'export',
		'fulfillment_partners',
		'landingpages',
		'language',
		'language_source',
		'language_translate',
		'manufacturer',
		'marketing_partners',
		'marketing_images',
		'net',
		'offer_type',
		'operating_systems',
		'order_status',
		'product_category',
		'provider',
		'seo',
		'sets',
		'shop',
		'shop_config',
		'shop_hour',
		'shop_certification',
		'shop_special',
		'shop_language',
		'special',
		'speak_language',
		'certification',
		'short_urls',
		'tariff_highlight_details',
		'tariff_highlights',
		'tariffs',
		'tariff_special',
		'user',
		'auth_assignment',
		'auth_item',
		'auth_item_child',
		'auth_rule',
		'job',
		'job_category',
        'general_news'
	];

	/**
	 * PublishController constructor.
	 * @param string $id
	 * @param \yii\base\Module $module
	 * @param array $config
	 */
	public function __construct($id, $module, $config = [])
	{
		$dir = Yii::$app->runtimePath . '/published';
		if (is_dir($dir) == false)
			mkdir($dir);
		$this->dumpFile = $dir . '/' . 'p' . (new \DateTime())->format('ymd_his') . '_' . Yii::$app->params['domain'] . '.sql';

		parent::__construct($id, $module, $config);
	}

	/**
	 * @param string $actionID
	 * @return array
	 */
	public function options($actionID)
	{
		if ($actionID == 'publish') {
			return ['ignoreMigration'];
		}

		return [];
	}

	/**
	 * @return bool
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionPublish()
	{
		if (YII_ENV_PROD) {
			echo Console::ansiFormat('This command is prohibited in PROD environment.', [Console::FG_RED]);
			return 1;
		}

		$localConnection = Yii::$app->getDb();
		$remoteConnection = new Connection(Yii::$app->get('liveDb'));

		$sql = 'SELECT * FROM migration';
		$drLocal = $localConnection->createCommand($sql)->query();
		$drRemote = $remoteConnection->createCommand($sql)->query();

		if ($drLocal->rowCount > $drRemote->rowCount && $this->ignoreMigration == false) {
			echo Console::ansiFormat('Migration required before publishing.', [Console::FG_RED]) . PHP_EOL;
			return 1;
		}

		$output = [];
		$return = 0;

		$exportDbNames = [];
		$hosts = [];
		$this->testConnections();
		preg_match('/dbname=([^\s]+)/', $this->db1->dsn, $exportDbNames);
		preg_match('/host=([\w\d\._-]+)/', $this->db1->dsn, $hosts);
		$exportCommand = preg_replace('/{dbname}/', $exportDbNames[1], $this->exportCommandTemplate);
		$exportCommand = preg_replace('/{tbl_names}/', join(' ', $this->tables), $exportCommand);
		$exportCommand = preg_replace([
			'/{username}/',
			'/{password}/',
			'/{host}/',
		], [
			$this->db1->username,
			$this->db1->password,
			$hosts[1],
		], $exportCommand);

		$exportCommand .= ' > ' . $this->dumpFile;

		exec($exportCommand, $output, $return);

		if ($return != 0) {
			die('Export failed.' . PHP_EOL . join("\n", $output));
		}

		$importDbNames = [];
		$hosts = [];
		preg_match('/dbname=([^\s]+)/', $this->db2->dsn, $importDbNames);
		preg_match('/host=([\w\d\._-]+)/', $this->db2->dsn, $hosts);
		$importCommand = preg_replace('/{dbname}/', $importDbNames[1], $this->importCommandTemplate);
		$importCommand = preg_replace([
			'/{username}/',
			'/{password}/',
			'/{host}/',
		], [
			$this->db2->username,
			$this->db2->password,
			$hosts[1],
		], $importCommand);

		$importCommand .= ' < ' . $this->dumpFile;

		exec($importCommand, $output, $return);

		if ($return != 0) {
			die('Import failed.' . PHP_EOL . join("\n", $output));
		}

		$this->resetPublishToken($remoteConnection);

		echo "Published.\n";
		return false;
	}

	/**
	 * @return bool
	 * @throws \yii\base\InvalidConfigException
	 */
	protected function testConnections()
	{
		$this->db1 = Yii::$app->db;
		$this->db2 = Yii::$app->get('liveDb');

		try {
			$this->db1->open();
			$this->db2->open();
		} catch (\Exception $e) {
			echo $e->getCode() . ': ' . $e->getMessage() . PHP_EOL;
			echo $e->getTraceAsString();
			die(1);
		}

		return true;
	}

	/**
	 * Reset publish token in table.
	 *
	 * @param Connection $remoteConnection
	 *
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\Exception
	 */
	protected function resetPublishToken(Connection $remoteConnection)
	{
		$publishToken = md5(microtime());
		$remoteConnection->createCommand(
			"UPDATE shop_config SET publish_token=:publish_token WHERE id='1';",
			[':publish_token' => $publishToken]
		)->execute();

		echo "Publish token reset.\n";
	}

	/**
	 * Trigger reset of publish token.
	 *
	 * @return boolean
	 */
	public function actionResetPublishToken()
	{
		$this->resetPublishToken(new Connection(Yii::$app->get('liveDb')));

		return false;
	}
}
