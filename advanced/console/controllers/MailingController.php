<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class MailingController extends Controller
{
	//For emails from txt
	private static $fileMailAddresses = "MailAdress_Test.txt";
	private static $templateHtml = "template/TemplateHtml.php";
	private static $templateText = "template/TemplateText.php";

	//For emails from csv with data
	private static $fileNameCsv = "MailAdress_Test.csv";
	private static $templateCsv = "template/TemplateCsvHtml.php";

	/**
	 * This is a Template to send Emails
	 * to a List of Users from a Text File.
	 */
	public function actionSendMailToListOfUsers()
	{
		$listOfUsers = file(Yii::getAlias('@rawData') . '/' . self::$fileMailAddresses);
		$listOfUsers = array_map('trim', $listOfUsers);

		$i = 1;
		foreach ($listOfUsers as $email) {
			self::sendMailToUser($email);
			echo "User: " . $i . " E-Mail: " . $email . "\n";
			$i++;
		}
		echo "Mails send: " . $i;
	}


	private static function sendMailToUser($email)
	{
		Yii::$app->mailer->compose(
			['html' => self::$templateHtml, 'text' => self::$templateText]
		)
			->setFrom(Yii::$app->params['mailing.confirmation.senderEmailSupport'])
//            ->setFrom(Yii::$app->params['mailing.confirmation.senderEmailNoReplay'])
//            ->setFrom(Yii::$app->params['mailing.confirmation.senderEmailNewsletter'])
			->setTo($email)
			->setSubject('!! Change this Subject !!')
//            ->attach(Yii::getAlias('@mailAttachments') . '/fileName')
			->send();
	}

	/**
	 * This is a Template to send Emails
	 * to a List of Users from a CSV File.
	 */
	public function actionSendMailToListOfUsersFromCsv()
	{
		$row = 1;
		if (($handle = fopen(Yii::getAlias('@rawData') . "/" . self::$fileNameCsv, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

				$email = $data[3];
				$firstName = $data[2];
				$orderId = $data[1];
				$orderDate = $data[0];

				echo "User: " . $row . " E-Mail: " . $email . "\n";
				$row++;

				self::sendMailToUserCsv($email, $firstName, $orderId, $orderDate);
			}
			fclose($handle);
		}
	}

	private static function sendMailToUserCsv($email, $firstName, $orderId, $orderDate)
	{
		Yii::$app->mailer->compose(
			['html' => self::$templateCsv],
			['firstName' => $firstName, 'orderId' => $orderId, 'orderDate' => $orderDate]
		)
			->setFrom(Yii::$app->params['mailing.confirmation.senderEmailSupport'])
//            ->setFrom(Yii::$app->params['mailing.confirmation.senderEmailNoReplay'])
//            ->setFrom(Yii::$app->params['mailing.confirmation.senderEmailNewsletter'])
			->setTo(trim($email))
			->setSubject('!! Change this Subject !!')
//            ->attach(Yii::getAlias('@mailAttachments') . '/fileName')
			->send();
	}

}