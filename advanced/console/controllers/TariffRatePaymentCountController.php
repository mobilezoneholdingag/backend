<?php

namespace console\controllers;

use common\models\Tariffs;
use Exception;
use yii\console\Controller;
use yii\helpers\Console;

class TariffRatePaymentCountController extends Controller
{
	public function actionUpdate()
	{
		$query = Tariffs::find()->where(['rate_payment_count' => null]);

		$tariffCount = $query->count();
		echo "$tariffCount tariffs found." . PHP_EOL;

		/** @var Tariffs[] $tariffs */
		$tariffs = $query->all();

		$n = 0;
		Console::startProgress($n, $tariffCount);

		foreach ($tariffs as $tariff) {
			try {
				$tariff->updateRatePayment()->save();
			} catch (Exception $e) {
				echo $e->getMessage();
			}
			Console::updateProgress(++$n, $tariffCount);
		}

		Console::endProgress();

		echo 'rate payment count updated!' . PHP_EOL;
	}
}
