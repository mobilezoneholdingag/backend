<?php

namespace console\controllers;

use common\models\Shop;
use common\models\ShopHour;
use common\models\ShopImage;
use yii\base\Exception;
use yii\console\Controller;

class ShopController extends Controller
{
	private $xml;
	private $weekDays = [
		0 => 'Sonntag',
		1 => 'Montag',
		2 => 'Dienstag',
		3 => 'Mittwoch',
		4 => 'Donerstag',
		5 => 'Freitag',
		6 => 'Samstag',
	];

	public function actionShopImport()
	{

		$url = 'http://j-beyer:hjdGF76@cs.mobilezone.ch/mobilezone/rest/shops.xml';
		$this->xml = simplexml_load_file($url);

		foreach ($this->xml->shop as $shop) {
			$newShop = new Shop();

			$newShop->name = $shop->name;
			$newShop->shop_tree_id = $shop->shop_tree_id;
			$newShop->shop_tree_name = $shop->shop_tree_name;

			//			$newShop->business_hours_de = $shop->business_hours_de;
			//			$newShop->business_hours_fr = $shop->business_hours_fr;
			//			$newShop->business_hours_it = $shop->business_hours_it;
			//			$newShop->business_days_de = $shop->business_days_de;
			//			$newShop->business_days_fr = $shop->business_days_fr;
			//			$newShop->business_days_it = $shop->business_days_it;
			$newShop->street = $shop->street;
			$newShop->extra_line = $shop->extra_line;
			$newShop->zip = $shop->zip;
			$newShop->city = ucfirst($shop->city_de);
			//			$newShop->city_de = $shop->city_de;
			//			$newShop->city_fr = $shop->city_fr;
			//			$newShop->city_it = $shop->city_it;
			$newShop->canton = strtolower($shop->canton);
			$newShop->latitude = $shop->latitude;
			$newShop->longitude = $shop->longitude;
			$newShop->phone = $shop->phone;
			$newShop->fax = $shop->fax;
			$newShop->email = strtolower($shop->email);
			$newShop->sort = $shop->sort;
			$newShop->state = $shop->state;
			$newShop->manager_name = $shop->manager_name;
			$newShop->manager_function = $shop->manager_function_de;
			//			$newShop->manager_function_de = $shop->manager_function_de;
			//			$newShop->manager_function_fr = $shop->manager_function_fr;
			//			$newShop->manager_function_it = $shop->manager_function_it;
			$newShop->manager_image_id = $shop->manager_image_id;
			$newShop->images_title = $shop->images_title_de;
			//			$newShop->images_title_de = $shop->images_title_de;
			//			$newShop->images_title_fr = $shop->images_title_fr;
			//			$newShop->images_title_it = $shop->images_title_it;
			$newShop->shop_image_id_1 = $shop->shop_image_id_1;
			$newShop->shop_image_id_2 = $shop->shop_image_id_2;
			$newShop->shop_image_id_3 = $shop->shop_image_id_3;
			$newShop->is_helpcenter = $shop->is_helpcenter == 'true' ? true : false;
			$newShop->save(false);

			$shopId = $newShop->id;

			//Split buggy data into good data
			$hours = $shop->business_hours_it;
			$days = $shop->business_days_de;
			$this->saveOpeningDaysWithTime($days, $hours, $shopId);

			foreach ($shop->children() as $images) {
				if ($images->getName() == 'manager_image') {
					$newShopImage = new ShopImage(['shop_id' => $shopId]);
					$newShopImage->image_id = $images->id;
					$newShopImage->type = 'manager_image';
					$newShopImage->path = $images->path;
					$newShopImage->url = $images->url;
					$newShopImage->save(false);
				}
				if ($images->getName() == 'shop_image_1') {
					$newShopImage = new ShopImage(['shop_id' => $shopId]);
					$newShopImage->image_id = $images->id;
					$newShopImage->type = 'shop_image_1';
					$newShopImage->path = $images->path;
					$newShopImage->url = $images->url;
					$newShopImage->save(false);
				}
				if ($images->getName() == 'shop_image_2') {
					$newShopImage = new ShopImage(['shop_id' => $shopId]);
					$newShopImage->image_id = $images->id;
					$newShopImage->type = 'shop_image_2';
					$newShopImage->path = $images->path;
					$newShopImage->url = $images->url;
					$newShopImage->save(false);
				}
				if ($images->getName() == 'shop_image_3') {
					$newShopImage = new ShopImage(['shop_id' => $shopId]);
					$newShopImage->image_id = $images->id;
					$newShopImage->type = 'shop_image_3';
					$newShopImage->path = $images->path;
					$newShopImage->url = $images->url;
					$newShopImage->save(false);
				}
			}
		}

		return 1;
	}

	private function saveOpeningDaysWithTime($days, $times, $shopId)
	{

		$days = (string)$days;
		$days = explode("<br />\n", $days);

		$times = (string)$times;
		$times = explode("<br />\n", $times);

		foreach ($days as $key => $day) {
			if (!isset($times[$key])) {
				continue;
			}

			$dayNumber = $this->openDays($day);
			$timesMatch = $this->openTime($times[$key]);

			foreach ($dayNumber as $numbers) {
				foreach ($timesMatch as $timesSplited) {
					$shopHour = new ShopHour(['shop_id' => $shopId]);
					$shopHour->opened_at = $timesSplited[0];
					$shopHour->closed_at = $timesSplited[1];
					$shopHour->day = $numbers;
					$shopHour->save(false);
				}
			}
		}
	}

	private function openDays($days)
	{
		if (empty($days)) {
			return [];
		}

		if (preg_match("/([\w\s]+)-([\w\s]+)/mi", $days, $matches)) {

			$start = $this->searchDayNumber(trim($matches[1]));
			$end = $this->searchDayNumber(trim($matches[2]));

			return range($start, $end);
		} elseif (preg_match("/([\w]+)/mi", $days, $matches)) {
			return [$this->searchDayNumber($matches[1])];
		}

		throw new Exception();
	}

	private function searchDayNumber($day)
	{
		return array_search($day, $this->weekDays);
	}

	private function openTime($time)
	{
		if (empty($time)) {
			return [];
		}

		$times = explode(" ", $time);
		$result = [];

		foreach ($times as $key => $time) {
			if (preg_match_all("/([\d:]+)-([\d:]+)/i", $time, $matches)) {

				array_shift($matches);

				if (count($matches) != 2) {
					return [];
				}

				foreach ($matches as $match) {
					$result[$key][] = $match[0];
				}
			}
		}

		return $result;
	}
}
