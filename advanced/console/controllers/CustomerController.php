<?php

namespace console\controllers;

use common\models\Customer;
use common\models\CustomerAddress;
use common\models\CustomerPhoneNumber;
use common\models\NewsletterSubscriber;
use yii\console\Controller;
use yii\db\Exception;

class CustomerController extends Controller
{
	private $counter = 0;
	private $xml;

	public function actionCustomerImport()
	{

		$passport = [
			1 => 'passport_id',
			2 => 'identity_card',
			3 => 'driver_license',
			4 => 'residence_permit_B',
			5 => 'residence_permit_C',
			6 => 'residence_permit_G',
			7 => 'residence_permit_L',
			8 => 'residence_permit_F',
			9 => 'residence_permit_N',
			//10 => 'commercial_register', //The type "Handelsregistereintrag" was not found in Customer.XML
		];

		$url = 'http://j-beyer:hjdGF76@cs.mobilezone.ch/mobilezone/rest/customers.xml?mz_Customer.group_id=1&:limit=' . $this->counter . ',20';
		$this->xml = simplexml_load_file($url);

		$time_start = microtime(true);

		while ($this->xml->count() != 0) {

			$url = 'http://j-beyer:hjdGF76@cs.mobilezone.ch/mobilezone/rest/customers.xml?mz_Customer.group_id=1&:limit=' . $this->counter . ',20';

			$this->xml = simplexml_load_file($url);

			//Can be removed if we can import all customer's!!!
			if ($this->counter == 60) {
				die("\n\rENDE des CustomerImports DEV\n\r");
			}

			echo $this->counter . "\n\r";
			$this->counter += 20;

			foreach ($this->xml->customer as $customers) {
				$newCustomer = new Customer();
				$newCustomerAddress = new CustomerAddress();
				$newCustomerPhone = new CustomerPhoneNumber();
				$newCustomerNewsletter = new NewsletterSubscriber();

				//CustomerData
				$newCustomer->nationality = strtolower($customers->nationality);
				$newCustomer->language = strtolower($customers->correspondence_language);
				$newCustomer->passport_id = (string)$customers->identification_number;
				$newCustomer->passport_type = array_search($customers->identification_type, $passport);
				$newCustomer->birthday = $customers->born_at;

				foreach ($customers->children() as $customer) {
					if ($customer->getName() == 'contract_address') {

						//CustomerData
						$newCustomer->email = strtolower($customer->email);
						$newCustomer->gender = $customer->gender == "male" ? 1 : 2;
						$newCustomer->first_name = ucfirst($customer->first_name);
						$newCustomer->last_name = ucfirst($customer->last_name);

						//CustomerPhoneNumber
						$newCustomerPhone->number = $customer->mobile_phone;

						//CustomerAddress
						$newCustomerAddress->sex = $customer->gender == "male" ? 1 : 2;
						$newCustomerAddress->firstname = ucfirst($customer->first_name);
						$newCustomerAddress->lastname = ucfirst($customer->last_name);
						$newCustomerAddress->company = $customer->company_name;
						$newCustomerAddress->street = $customer->street;
						$newCustomerAddress->zip = $customer->zip;
						$newCustomerAddress->city = $customer->city;
						$newCustomerAddress->country = strtolower($customer->country);
					}
				}

				try {
					$newCustomer->save(false);
					$newCustomerAddress->customer_id = $newCustomer->id;
					$newCustomerAddress->save(false);

					$newCustomerNewsletter->customer_id = $newCustomer->id;
					$newCustomerNewsletter->status = $newCustomerNewsletter::STATUS_NEVER_SUBSCRIBED;
					$newCustomerNewsletter->save(false);

					$newCustomerPhone->customer_id = $newCustomer->id;
					$newCustomerPhone->save(false);
				} catch (Exception $e) {
					echo "Duplicate User:" . $newCustomer->email . "\r\n";
				}
			}
		}

		$time_end = microtime(true);
		//dividing with 60 will give the execution time in minutes other wise seconds
		$execution_time = ($time_end - $time_start) / 60;

		echo "\r\n";
		echo '<b>Total Execution Time in minutes:</b> ' . $execution_time;
		echo "\r\n";

		return 1;
	}
}
