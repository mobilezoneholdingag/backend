<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;

class ArticleTariffActiveStatusController extends Controller
{
	public function actionUpdate()
	{
		$connection = Yii::$app->db;
		$command = $connection->createCommand('
			UPDATE article_tariffs
			SET is_active = CASE
				WHEN 
					(valid_from IS NULL OR valid_from <= DATE(NOW())) AND 
					(valid_until IS NULL OR valid_until >= DATE(NOW())) 
				THEN 1
				ELSE 0
			END
		');

		$updated = $command->execute();
		$this->stdout(number_format($updated) . " article tariffs have been updated!" . PHP_EOL, Console::FG_GREEN);
	}
}
