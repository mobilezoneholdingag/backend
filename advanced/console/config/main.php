<?php
$params = array_replace_recursive(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-console',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log', 'gii'],
	'controllerNamespace' => 'console\controllers',
	'modules' => [
		'gii' => 'yii\gii\Module',
	],
	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=mysql;dbname=mobilezone',
			'username' => 'root',
			'password' => '123',
			'charset' => 'utf8',
			'attributes' => [
				PDO::ATTR_PERSISTENT => false,
			],
		],
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\DbTarget',
					'levels' => ['error', 'warning'],
					'logTable' => '{{%log_console}}',
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['info'],
					'except' => ['yii\db\*'],
					'logFile' => '@runtime/logs/info.log',
				],
				'db' => [
					'class' => 'yii\log\DbTarget',
					'categories' => ['feed', 'api', 'mobilezone-erp-controller', 'mails', 'mobilezone-erp-xml-adapter', 'feed-debug'],
					'levels' => ['error', 'warning'],
					'logTable' => '{{%log_console}}',
				],
				[
					'class' => 'yii\log\DbTarget',
					'categories' => ['swisscom', 'postfinance', 'order', 'swissbilling'],
					'levels' => ['error', 'warning',],
					'logTable' => '{{%log_backend}}',
				],
				[
					'class' => 'yii\log\DbTarget',
					'categories' => ['checkout'],
					'levels' => ['error', 'warning'],
					'logTable' => '{{%log_frontend}}',
				],
			],
		],
		'urlManager' => [
			'class' => 'yii\web\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'baseUrl' => 'http://www.stardust.dev/',
			'rules' => [
				'offer/<id:[\w\-]+>' => 'checkout/cart/accept-offer',
				[
					'pattern' => '<category:(tablets|smartwatches)>/<id:[\d]+>/<manufacturerTitle:[\w\-]+>/<groupUrlText:[\w\-]+>',
					'route' => 'product/product/<category>',
					'defaults' => [
						'id' => null,
						'manufacturerTitle' => '',
						'groupUrlText' => '',
					],
				],
				[
					'pattern' => 'mobiltelefone/<id:[\d]+>/<manufacturerTitle:[\w\-]+>/<groupUrlText:[\w\-]+>',
					'route' => 'product/product/smartphones',
					'defaults' => [
						'id' => null,
						'manufacturerTitle' => '',
						'groupUrlText' => '',
					],
				],
				[
					'pattern' => 'zubehoer/<id:[\d]+>/<manufacturerTitle:[\w\-]+>/<groupUrlText:[\w\-]+>',
					'route' => 'product/product/accessories',
					'defaults' => [
						'id' => null,
						'manufacturerTitle' => '',
						'groupUrlText' => '',
					],
				],
			],
		],
		'user' => null,
	],
	'params' => $params,
];
