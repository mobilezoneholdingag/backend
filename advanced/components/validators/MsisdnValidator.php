<?php

namespace components\validators;

use Yii;
use yii\validators\Validator;

class MsisdnValidator extends Validator
{
	public function validateAttribute($model, $attribute)
	{
		if ($model->{$attribute} && preg_match('/417\d{8}/', $model->{$attribute}) !== 1) {
			$this->addError(
				$model,
				$attribute,
				Yii::t('app', 'The given MSISDN has to match the format "417xxxxxxxx"!')
			);
			$model->{$attribute} = null;
		}
	}
}
