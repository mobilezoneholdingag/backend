<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components\validators;


use components\Locale;
use yii\validators\Validator;

class PassportValidator extends Validator
{
	const OLD_GERMAN_ID       = 1;
	const GERMAN_ID           = 2;
	const GERMAN_PASSPORT     = 3;
	const FOREIGN_PASSPORT    = 4;

	// Suisse
	const SUISSE_PASSPORT         = 5;
	const SUISSE_ID               = 6;
	const SUISSE_DRIVER_LICENSE   = 7;
	const SUISSE_FOREIGNER_PASS_B = 8;
	const SUISSE_FOREIGNER_PASS_C = 9;
	const SUISSE_FOREIGNER_PASS_G = 10;
	const SUISSE_FOREIGNER_PASS_L = 11;
	const SUISSE_FOREIGNER_PASS_F = 12;
	const SUISSE_FOREIGNER_PASS_N = 13;
	const SUISSE_TRADE_REGISTER   = 14;

	protected $letterDictionary   = [];

	public $idType = self::GERMAN_ID;

	/**
	 * @inheritDoc
	 */
	public function init()
	{
		parent::init();

		// Create dictionary for letter-number translation
		$alphabet = range('A', 'Z');
		foreach($alphabet as $i => $letter)
			$this->letterDictionary[$letter] = $i + 10;

	}

	/**
	 * @inheritDoc
	 */
	public function validateAttribute($model, $attribute)
	{
		if (!is_numeric($this->idType))
			$this->idType = (int) $model->{$this->idType};

		parent::validateAttribute($model, $attribute);
	}


	/**
	 * @param string $passportId
	 * @inheritdoc
	 */
	protected function validateValue($passportId)
	{
		// No further validation required for foreign passports
		if ($this->idType == self::FOREIGN_PASSPORT) {
			return null;
		}

		if (self::recognizeType($passportId) != $this->idType) {
			return [
				\Yii::t('app', 'Wrong ID number'),
				[]
			];
		} elseif (self::recognizeType($passportId) == self::FOREIGN_PASSPORT) {
			return null;
		} elseif (!self::recognizeType($passportId)) {
			return [
				\Yii::t('app', 'The type of the ID has not been recognized'),
				[]
			];
		}

		$passportIdArray = str_split($passportId);
		if (end($passportIdArray) == 'D') {
			array_pop($passportIdArray);
			reset($passportIdArray);
		}

		$checkNumber = (int) array_pop($passportIdArray);
		$idNumberSequenceArray = [];

		foreach($passportIdArray as $positionValue) {
			if (array_key_exists($positionValue, $this->letterDictionary)) {
				$positionValue = $this->letterDictionary[$positionValue];
			}

			$idNumberSequenceArray[] = (int) $positionValue;
		}

		$multipliedSequence = [];
		$multipliers = [ 7, 3, 1 ];
		foreach($idNumberSequenceArray as $i => $v) {
			$multiplier = $multipliers[$i % count($multipliers)];
			$multipliedSequence[] = $multiplier * (int) $v;
		}

		if (array_sum($multipliedSequence) % 10 == $checkNumber)
			return null;

		return [
			\Yii::t('app', 'Wrong ID number'),
			[]
		];
	}

	static public function recognizeType($passportId)
	{
		if (Locale::in(Locale::GERMANY)) {
			if (preg_match('/^[0-9]{10}D?$/', $passportId))
				return self::OLD_GERMAN_ID;

			if (preg_match('/^[LMNPRTVWXY][CFGHJKLMNPRTVWXYZ0-9]{8}[0-9]D?$/', $passportId))
				return self::GERMAN_ID;

			if (preg_match('/^[CFGHJK][CFGHJKLMNPRTVWXYZ0-9]{8}[0-9]D?$/', $passportId))
				return self::GERMAN_PASSPORT;

			if (strlen($passportId) >= 5)
				return self::FOREIGN_PASSPORT;
		} else {
			return true;
		}

		return false;
	}

	/**
	 * @return bool|string  Validation result. False and a string are errors.
	 */
	public static function validateRequest()
	{
		$idnumber = \Yii::$app->request->get('idnumber', false);
		$idtype = \Yii::$app->request->get('idtype', false);
		$error = null;

		if (!$idtype || !$idnumber)
			return false;

		if (Locale::in(Locale::GERMANY)) {
			if (in_array($idnumber, [
				'333333333',
				'333333333D',
				'T220001293',
				'C01X0006H1'
			]))
				return false;


			$validator = new self();
			$validator->idType = (int)$idtype;


			$validationResult = $validator->validate($idnumber, $error);
		} else if (false == ($validationResult = strlen((string) $idnumber) > 1)) {
			$error = \Yii::t('app', 'Number missing');
		}

		if (!$validationResult) {
			return $error;
		}

		return $validationResult;
	}
}