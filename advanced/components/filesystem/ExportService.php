<?php

namespace components\filesystem;

use League\Flysystem\File;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Process\ProcessBuilder;
use Yii;
use yii\base\Component;
use yii\caching\Cache;
use yii\web\Response;

class ExportService extends Component
{
	/** @var FilesystemInterface */
	private $filesystem;
	/** @var Cache */
	private $cache;

	public function __construct($configName = 'exports', array $config = [])
	{
		/** @var FilesystemProviderInterface $filesystemProvider */
		$filesystemProvider = di(FilesystemProvider::class);
		$this->filesystem = $filesystemProvider->fs($configName);
		$this->cache = Yii::$app->cache;

		parent::__construct($config);
	}

	public function getFileUpdatedAt(string $filename): string
	{
		try {
			$timestamp = $this->cache->get("{$filename}_timestamp") ?: $this->filesystem->getTimestamp($filename);
		} catch (FileNotFoundException $e) {
			return '';
		}
		$datetime = Yii::$app->formatter->asDatetime($timestamp);

		return $datetime;
	}

	public function start(string $command)
	{
		$builder = new ProcessBuilder(['./yii', $command]);
		$builder->setTimeout(1800);
		$builder->setWorkingDirectory(Yii::getAlias('@app/..'));
		$builder->getProcess()->start();

		Yii::$app->session->addFlash(
			'success',
			"Export erfolgreich angestoßen!<br>" .
			"Dieser Vorgang kann nun bis zu einigen Minuten in Anspruch nehmen."
		);
	}

	public function save(string $filename, $content)
	{
		$this->cache->set("{$filename}_timestamp", time());

		$this->filesystem->put($filename, $content);
	}

	public function download(string $filename): Response
	{
		/** @var File $file */
		$file = $this->filesystem->get($filename);

		return Yii::$app->response->sendContentAsFile($file->read(), $filename);
	}
}
