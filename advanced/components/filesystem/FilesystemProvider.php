<?php

namespace components\filesystem;

use League\Flysystem\AdapterInterface;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;
use Yii;

class FilesystemProvider implements FilesystemProviderInterface
{
	protected $config = [];

	public function __construct()
	{
		$this->config = Yii::$app->params['filesystems'];
	}

	/**
	 * @param string $name
	 *
	 * @return FilesystemInterface
	 */
	public function fs(string $name = self::DEFAULT_CONFIG)
	{
		if (!isset($this->config[$name])) {
			throw new \InvalidArgumentException("There is not CDN endpoint defined for `{$name}`.");
		}

		$config = $this->config[$name];

		// if config is showing on another config
		if (is_string($config)) {
			return $this->fs($config);
		}

		/** @var AdapterInterface $adapter */
		$adapter = Yii::$container->get($config['adapter'], $config);

		return new Filesystem($adapter);
	}

	/**
	 * returns url for given filesystem with the passed path
	 *
	 * @param string $name
	 * @param string $path
	 *
	 * @return string
	 */
	public function url(string $path, string $name = self::DEFAULT_CONFIG)
	{
		if (!isset($this->config[$name])) {
			throw new \InvalidArgumentException("There is not CDN endpoint defined for `{$name}`.");
		}

		$config = $this->config[$name];
		// if config is showing on another config
		if (is_string($config)) {
			return $this->url($path, $config);
		}

		$url = $config['url'];

		if (is_callable($url)) {
			return $url($path);
		}

		return $url . $config['rootDir'] . $path;
	}
}
