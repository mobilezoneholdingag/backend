<?php

namespace components\filesystem;

use League\Flysystem\FilesystemInterface;

interface FilesystemProviderInterface
{
	/**
	 * if no config name is passed, the provider will look for the default config
	 *
	 * @var string
	 */
	const DEFAULT_CONFIG = 'default';

	/**
	 * @param string $name
	 *
	 * @return FilesystemInterface
	 */
	public function fs(string $name = self::DEFAULT_CONFIG);

	/**
	 * returns url for given filesystem with the passed path
	 *
	 * @param string $name
	 * @param string $path
	 *
	 * @return string
	 */
	public function url(string $path, string $name = self::DEFAULT_CONFIG);
}
