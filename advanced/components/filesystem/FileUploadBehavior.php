<?php

namespace components\filesystem;

use Yii;
use yii\base\Behavior;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class FileUploadBehavior extends Behavior
{
	/**
	 * list of attributes which should be handled as upload files. in this case files will be uploaded to the `default`
	 * filesystem
	 *
	 * you can pass a {key} => {value} pair. in this case
	 * key: attribute name
	 * value: filesystem which this file will be pushed to
	 * [
	 *      'manager_image' => 'rackspace'
	 * ]
	 *
	 * @var string[]
	 */
	public $attributes = [];

	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_UPDATE => 'handleUploads',
			ActiveRecord::EVENT_BEFORE_INSERT => 'handleUploads',
		];
	}

	/**
	 * uploads file to the configured filesystem
	 *
	 * @param $event
	 */
	public function handleUploads($event)
	{
		/** @var Model $model */
		$model = $this->owner;
		/** @var FilesystemProviderInterface $fsProvider */
		$fsProvider = Yii::$container->get(FilesystemProviderInterface::class);

		foreach ($this->attributes as $key => $val) {
			if (is_string($key)) {
				$attribute = $key;
				$config = $val;
			} else {
				$attribute = $val;
				$config = FilesystemProviderInterface::DEFAULT_CONFIG;
			}

			$model->{$attribute} = UploadedFile::getInstances($model, $attribute);

			if (empty($model->{$attribute})) {
				$model->{$attribute} = null;
			} else {
				$model->{$attribute} = $model->{$attribute}[0];
			}

			if ($model->validate()) {
				if (!empty($model->{$attribute})) {
					$uploadedFile = $model->{$attribute};
					$fileName = $uploadedFile->baseName . '.' . $uploadedFile->extension;

					$fsProvider->fs($config)->put($fileName, file_get_contents($uploadedFile->tempName));
					$model->{$attribute} = $fsProvider->url($fileName, $config);
				}
			}
		}
	}
}
