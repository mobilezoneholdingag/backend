<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components\widgets\InternalAd;


use yii\web\AssetBundle;

class InternalAdWidgetAsset extends AssetBundle
{
	public $sourcePath  = '@components/widgets/InternalAd/web';

	public $js = [
		'js/be.internal-ads.js'
	];

	public $css = [
		'css/be.internal-ads.css'
	];
}