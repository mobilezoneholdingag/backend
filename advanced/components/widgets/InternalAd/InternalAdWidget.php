<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components\widgets\InternalAd;


use common\helper\SaleableHelper;
use common\models\LandingPages;
use yii\db\ActiveRecord;
use yii\web\View;

class InternalAdWidget extends \yii\base\Widget
{
	/**
	 * @var array   Saleable items.
	 */
	public $saleables   = [];

	/**
	 * @var array   All kinds of saleables.
	 */
	public $allKinds    = [];

	/** @var LandingPages */
	public $model       = null;

	/** @var View */
	public $view        = null;

	/** @var string */
	public $attribute   = null;

	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();
	}

	protected function getSaleablesAsKeyValuePairs()
	{
		$convertedSaleables = [];

		foreach ($this->saleables as $saleable) {
			$convertedSaleables[$saleable['id']] = $saleable['name'];
		}

		return $convertedSaleables;
	}

	/**
	 * Executes the widget.
	 * @return string the result of widget execution to be outputted.
	 */
	public function run()
	{
		InternalAdWidgetAsset::register($this->getView());

		return $this->render('backend-widget', [
			'model'     => $this->model,
			'saleables' => $this->getSaleablesAsKeyValuePairs(),
			'allKinds'  => $this->allKinds,
		]);
	}


}