/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de - a Mister Mobile GmbH brand
 */

;(function($, document, window) {
    "use strict";

    var name    = "be-internal-ads";

    function InternalAds(element, options) {
        this.$element = element;
        this.defaults = {
            'endpoints': {
                'list': '/json/list-advertising-capable-products'
            },
            'templates': {
                'saleable': '<label><input type="checkbox" name="items[]" value="$id"> $label</label>'
            }
        };

        this.options  = $.extend(this.defaults, options);

        var self = this;

        this.onChangeKind = function(event) {
            var kind = $(event.target).val();

            $.when(
                $.ajax({
                    'url': self.options.endpoints.list,
                    'data': {
                        kind: kind
                    },
                    'method': 'get'
                })
            ).then(
                function(data, status, jqXHR) {
                    var $saleablesList = self.$element.find('.saleablesList div');
                    $saleablesList.empty();
                    $.each(data, function(key, value) {
                        var checkbox = self.options.templates.saleable;
                        checkbox = checkbox.replace(/\$id/, value.id).replace(/\$label/, value.name);
                        $saleablesList.append(checkbox);
                    });
                }
            );
        };

        this.onChangeOffer = function(event) {
            var offers = {
                'items': $('.saleablesList input[type="checkbox"]:checked').map(function (){ return this.value; }).get(),
                'kind': $('select[name="kind"]').val(),
                'alignment': $('select[name="alignment"]').val(),
            };

            $(self.$element).find('div input[type="hidden"]').val(JSON.stringify(offers));
        };

        this.init = function()
        {
            $(this.$element).on('change', 'select[name="kind"]', self.onChangeKind);
            $(this.$element).on('change', self.onChangeOffer);
        };

        this.init();
    }

    $.fn.internalAds = function(options) {
        return new InternalAds(this, options);
    }

})(jQuery, document, window);

$('.saleListWidget').internalAds();