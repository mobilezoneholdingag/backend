<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var array  $saleables */
/** @var array  $allKinds */
/** @var \yii\web\View $view */
/** @var \common\models\LandingPages $model */
/** @var string attribute */

?>

<div class="saleListWidget">

	<?= Html::dropDownList('kind', $model->getOffersAsArray()['kind'], $allKinds) ?>

	<?= Html::dropDownList('alignment', $model->getOffersAsArray()['alignment'], [1 => 'horizontal', 2 => 'vertikal'], ['label' => 'Ausrichtung']); ?>

	<div class="saleablesList" style="height: 430px; width: 450px; overflow: scroll; padding: 7px;">
		<?= Html::checkboxList('items', $model->getOffersAsArray()['items'], $saleables) ?>
	</div>

	<?php $form = ActiveForm::begin() ?>
	<?= $form->field($model, 'offers')->hiddenInput(['label' => '']) ?>

	<?php // $form::end() ?>
</div>