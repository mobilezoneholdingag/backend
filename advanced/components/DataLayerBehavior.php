<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components;


use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\web\View;

/**
 * @package components
 */
abstract class DataLayerBehavior extends Behavior
{
	const DYNAMICYIELD = 'dataLayer_dy';
	const GOOGLE = 'dataLayer_google';

	/* constant needs to get overwritten with value != null in extending classes to work as expected */
	const ACTIVE_STATE_PARAM_NAME = null;

	protected $dataLayerProperties = [];
	protected $dataLayerInitPrefix = '';
	protected $dataLayerInitVar = '';
	protected $dataFormat = '';
	
	/**
	 * @return array
	 */
	public function events()
	{
		return [
			View::EVENT_BEFORE_RENDER => 'beforeRender',
			View::EVENT_BEGIN_BODY => 'beginBody'
		];
	}

	/**
	 * @param Event $event
	 * @return null
	 */
	public function beforeRender(Event $event)
	{
		/* if there is a activeState const set in params, only return if param is set true */
		if(!$this->isSetActiveAsGlobalParam()) {
			return null;
		}

		Yii::$app->view->registerJs($this->getDataLayer(), View::POS_BEGIN);
	}

	/**
	 * @param Event $event
	 * @return null
	 */
	public function beginBody(Event $event = null)
	{
		if(!$this->isSetActiveAsGlobalParam()) {
			return null;
		}
	}

	/**
	 * @return string
	 */
	public function getDataLayer()
	{
		return
			$this->dataLayerInitPrefix . $this->dataLayerInitVar . ' = ' .
			$this->setDataType(json_encode($this->dataLayerProperties)) . ';';
	}

	/**
	 * @param string $data
	 * @return string
	 */
	protected function setDataType($data)
	{
		switch ($this->dataFormat) {
			case 'array':
				return "[{$data}]";
				break;
			default:
				return $data;
		}
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	public function getFromDataLayer($name)
	{
		return $this->__get($name);
	}

	/**
	 * @param $name
	 * @param $value
	 * @return $this
	 */
	public function setOnDataLayer($name, $value)
	{
		$this->__set($name, $value);
		return $this;
	}

	/**
	 * @param array $values
	 * @return $this|bool
	 */
	public function mergeOnDataLayer(array $values = [])
	{
		if (array_keys($this->dataLayerProperties) === range(0, count($this->dataLayerProperties) - 1))
			return false;

		$this->dataLayerProperties = array_merge($this->dataLayerProperties, $values);

		return $this;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		$dataLayerObject = new \ArrayObject($this->dataLayerProperties);
		return $dataLayerObject->offsetGet($name);
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 */
	public function __set($name, $value)
	{
		$this->dataLayerProperties[$name] = $value;
		return $this;
	}

	/**
	 * @return bool
	 */
	protected function isSetActiveAsGlobalParam()
	{
		return empty(static::ACTIVE_STATE_PARAM_NAME) || Yii::$app->params[static::ACTIVE_STATE_PARAM_NAME];
	}
}