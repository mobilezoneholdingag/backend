<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components;


class AssetHelper
{
	static public function hashPath($path)
	{
		return substr(hash('sha1', $path), 0, 8);
	}
}