<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2016 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components;

trait SubserviceTrait
{
	/** @var array   */
	public  $services = [];

	/**
	 * Loads declared services
	 * @param array $additionalProperties
	 */
	protected function loadServices($additionalProperties = [])
	{
		if (!empty($this->services)) {
			foreach($this->services as $key => &$entry) {
				$service = \Yii::createObject(array_merge($entry, $additionalProperties));
				$entry = $service;
			}
		}
	}
}