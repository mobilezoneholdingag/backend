<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components\events;


use yii\base\Event;

abstract class BaseRfsFileEvent extends Event
{
	protected $filePath = null;

	public function __construct($filePath = '')
	{
		parent::__construct();

		$this->filePath = $filePath;
	}

	public function getFilePath()
	{
		return $this->filePath;
	}

	public function setFilePath($filePath)
	{
		$this->filePath = $filePath;
	}
}