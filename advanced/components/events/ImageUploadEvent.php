<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 **/

namespace components\events;


class ImageUploadEvent extends BaseRfsFileEvent
{
	const EVENT_UPLOADED = 'components.event.image_upload';
}