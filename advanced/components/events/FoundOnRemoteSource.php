<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2015 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components\events;


class FoundOnRemoteSource extends BaseRfsFileEvent
{
	const EVENT_FOUND_REMOTE = 'components.events.foundOnRemoteSource';
}