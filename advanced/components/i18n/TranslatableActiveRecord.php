<?php

namespace components\i18n;

use lajax\translatemanager\models\LanguageSource;
use lajax\translatemanager\models\LanguageTranslate;
use sammaye\audittrail\LoggableBehavior;
use Yii;
use yii\base\Event;
use yii\base\InvalidParamException;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

/**
 * Class TanslatableActiveRecord
 */
class TranslatableActiveRecord extends \yii\db\ActiveRecord
{
	const SEPARATOR = '_|_';
	public $translationsArr = [];
	protected $translateAttributes = [];

	/**
	 * Overwriting unsafe handling to update $translationsArr data only - considering it being safe.
	 * (Others than that will still get logged!)
	 *
	 * @param string $name the unsafe attribute name
	 * @param mixed $value the attribute value
	 */
	public function onUnsafeAttribute($name, $value)
	{
		switch ($name) {
			case 'translationsArr':
				$this->$name = ArrayHelper::merge($this->$name, $value);
				break;
			default:
				parent::onUnsafeAttribute($name, $value);
		}
	}

	/**
	 * Helper funtion used in child classes rules() function to remove "required" rules for values set in
	 * translateAttributes.
	 *
	 * @param $calleeRules
	 *
	 * @return mixed
	 */
	public function translatableRules($calleeRules)
	{
		foreach ($calleeRules as $key => $rule) {
			if(in_array('required', $rule)) {
				foreach ($rule[0] as $attriuteKey => $attributeName) {
					if(in_array($attributeName, $this->translateAttributes)) {
						unset($calleeRules[$key][0][$attriuteKey]);
					}
				}
			}
		}

		return $calleeRules;
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		$behaviors = [
			[
				'class' => TranslatableModelBehavior::className(),
				'translateAttributes' => $this->translateAttributes,
			],
		];

		return Yii::$app->id == 'app-backend' ? ArrayHelper::merge(
			$behaviors,
			[LoggableBehavior::className()]
		) : $behaviors;
	}

	/**
	 * Bulk retrieval of translations from \TranslateBehavior::translateAttributes[].
	 * Additionally set/translate model´s property value in current user-language (Yii::$app->language).
	 * leads to: $model->property = translate_Id --> $model->description_1 = translation
	 *
	 * @return array Array containing translations row info
	 * return array´s syntax:
	 *      [propertyName => [countryCode => [id, translation, countryCode]]]
	 *      exmaple:
	 *          ['description_1' => [
	 *                  'de-CH' => [
	 *                      id => 1,
	 *                      language => 'de-CH',
	 *                      translation => 'i18n ist supergeil',
	 *                  ],
	 *                  'en-EN' => [...],
	 *              ]
	 *          ]
	 */
	public function prepareTranslations()
	{
		$messageStringArray = [];

		foreach ($this->translateAttributes as $attributeName) {
			$messageStringArray[$attributeName] = $this->encodeMessageString($attributeName);
		}

		$dependency = new TagDependency(['tags' => $this->getCacheTags()]);
		$activeQuery = LanguageTranslate::getDb()->cache(function() use ($messageStringArray) {
			return LanguageTranslate::find()
				->select('language_source.message, language_translate.*')
				->leftJoin(LanguageSource::tableName(), 'language_translate.id = language_source.id')
				->where(['language_source.message' => array_values($messageStringArray)])
				->asArray(true)
				->all();
		}, 0, $dependency);

		foreach ($activeQuery as $resultRow) {
			foreach ($messageStringArray as $attribute => $encodedAttribute) {
				if ($resultRow['message'] === $encodedAttribute) {
					$this->translationsArr[$attribute][$resultRow['language']] = $resultRow;
					if ($resultRow['language'] === Yii::$app->language) {
						$this->{$attribute} = $this->translationsArr[$attribute][Yii::$app->language]['translation'] ?? $this->{$attribute};
					}
				}
			}
		}

		return $this->translationsArr;
	}

	/**
	 * @param $attribute
	 *
	 * @return string
	 */
	protected function encodeMessageString($attribute)
	{
		$id = $this->getModelIdOrPrimaryKey();

		return $this->className() . self::SEPARATOR . $id . self::SEPARATOR . $attribute;
	}

	/**
	 * Not all models do have an ID, so use PK as surrogate.
	 *
	 * @return mixed|string
	 */
	protected function getModelIdOrPrimaryKey()
	{
		return isset($this->id) ? $this->id : implode('-', $this->getPrimaryKey(true));
	}

	/**
	 * Save translations to tbl_language_translate.
	 *
	 * @param Event $event
	 */
	public function saveTranslations(Event $event)
	{
		if (self::EVENT_AFTER_INSERT === $event->name) {
			$this->off(self::EVENT_AFTER_INSERT, [$this, 'saveTranslations']);
		}

		foreach ($this->translateAttributes as $attribute) {
			$this->{$attribute} = $this->translationsArr[$attribute][Yii::$app->language]['translation'] ?? $this->{$attribute};
		}

		if ($this->getModelIdOrPrimaryKey()) {
			foreach ($this->translationsArr as $modelValueName => $countryInfo) {

				// check against definition of translateable properties - skip if not found
				if (!in_array($modelValueName, $this->translateAttributes)) {
					continue;
				}

				// handle new language_source entry
				foreach ($countryInfo as $countryCode => $transArr) {
					if (!isset($transArr['id'])) {
						$messageString = $this->encodeMessageString($modelValueName);
						$langModel = LanguageSource::find()->where(
							['message' => $messageString]
						)->one();

						// model not found --> create new entry
						if (!$langModel) {
							$langModel = new LanguageSource();
							$langModel->category = 'database';
							$langModel->message = $messageString;
							$langModel->save();
							$langModel->refresh();
						}

						// set new language_source id on array elems
						foreach ($this->translationsArr[$modelValueName] as $countryCode => $transHelper) {
							$this->translationsArr[$modelValueName][$countryCode]['id'] = $langModel->id;
						}
						break;
					}
				}

				reset($countryInfo);

				// update each translation entry
				foreach ($this->translationsArr[$modelValueName] as $countryCode => $transArr) {

//					if (!empty($transArr['translation'])) {
						$transModel = LanguageTranslate::find()->where(
							['id' => $transArr['id'], 'language' => $countryCode]
						)->one();

						// not found --> create new entry
						if (!$transModel) {
							$transModel = new LanguageTranslate();
							$transModel->id = $transArr['id'];
							$transModel->language = $countryCode;
						}

						$transModel->translation = $transArr['translation'];
						$transModel->save();
//					}
				}
			}
		} else {
			$this->on(self::EVENT_AFTER_INSERT, [$this, 'saveTranslations']);
		}

		TagDependency::invalidate(Yii::$app->cache, $this->getCacheTags());
	}

	private function getCacheTags()
	{
		$tags = $this->translateAttributes;

		foreach ($tags as &$tag) {
			$tag = "translation-$tag";
		}

		return $tags;
	}

	public function checkAndSetDefaults()
	{
		foreach ($this->translateAttributes as $attributeName) {
			if (empty($this->{$attributeName})
				&& isset($this->translationsArr[$attributeName][Yii::$app->language]['translation'])
			) {
				$this->{$attributeName} = $this->translationsArr[$attributeName][Yii::$app->language]['translation'];
			}
		}

		return $this->validateOwnerWithAllLanguagesSet();

	}
	/**
	 * @return array
	 */
	protected function validateOwnerWithAllLanguagesSet()
	{
		$isValid = true;

		$availableCountries = array_values(Yii::$app->params['languages']);
		$ownerProperties = array_keys($this->translationsArr);

		foreach ($availableCountries as $language) {
			if(!$isValid) {
				continue;
			}
			foreach ($ownerProperties as $ownerProperty) {
				$this->{$ownerProperty} = $this->translationsArr[$ownerProperty][$language]['translation'];
			}
			$isValid = $this->validateWithoutBeforeHandler();
		}

		return $isValid;
	}

	/**
	 * Do validation of owner without re-triggering before_validate event.
	 *
	 * @return bool
	 */
	private function validateWithoutBeforeHandler()
	{
		$scenarios = $this->scenarios();
		$scenario = $this->getScenario();
		if (!isset($scenarios[$scenario])) {
			throw new InvalidParamException("Unknown scenario: $scenario");
		}
		$attributeNames = $this->activeAttributes();

		foreach ($this->getActiveValidators() as $validator) {
			$validator->validateAttributes($this, $attributeNames);
		}

		return !$this->hasErrors();
	}

	/**
	 * Returns the translated version of the given attribute in the given language.
	 * If no translation exists it returns null.
	 *
	 * @param $attributeName
	 * @param $languageCode
	 *
	 * @return null
	 */
	public function getTranslatedAttribute($attributeName, $languageCode)
	{
		return $this->translationsArr[$attributeName][$languageCode]['translation'] ?? null;
	}
}
