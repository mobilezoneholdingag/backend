<?php

namespace components\i18n\widget;

use dosamigos\ckeditor\CKEditor;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\jui\Tabs;
use yii\widgets\ActiveForm;

/**
 * Class TabWidget
 */
class TabWidget extends Widget
{
	const TYPE_WIDGET = 'widget';
	const TYPE_TEXTFIELD = 'field';
	const TYPE_TEXTAREA = 'area';
	/* @var $model \components\i18n\TranslatableActiveRecord */
	public $model;
	/* @var $form ActiveForm */
	public $form;
	public $fieldsToRender = [];
	public $editorOptions = [];
	public $inputType = self::TYPE_WIDGET;
	public $label = '';
	public $overrideLabel = null;

	/**
	 * Executes the widget.
	 *
	 * @return string the result of widget execution to be outputted.
	 */
	public function run()
	{
		$widgetsString = '';

		// List all fields if no specifeic selection provided.
		if (empty($this->fieldsToRender)) {
			$this->fieldsToRender = $this->model->translateAttributes;
		}

		foreach ($this->fieldsToRender as $modelValueName) {
			$items = [];
			$labelString = $this->overrideLabel ?? $this->model->getAttributeLabel($modelValueName) ?? $modelValueName;
			$widgetsString .= "<BR /><label class=\"control-label\">" . $labelString . "</label>";


			foreach (array_values(Yii::$app->params['languages']) as $countryCode) {
				$content = null;

				// Decide between CKEditor and textInput.
				// (set label value ''to hide auto generated field label == attribute name)
				switch ($this->inputType) {
					case self::TYPE_TEXTFIELD:
						$content = $this->form->field(
							$this->model,
							"translationsArr[$modelValueName][$countryCode][translation]"
						)->textInput($this->editorOptions ?? [])->label($this->label);
						break;
					case self::TYPE_TEXTAREA:
						$content = $this->form->field(
							$this->model,
							"translationsArr[$modelValueName][$countryCode][translation]"
						)->textarea($this->editorOptions ?? [])->label($this->label);
						break;
					default:
						$content = $this->form->field(
							$this->model,
							"translationsArr[$modelValueName][$countryCode][translation]"
						)->widget(
							CKEditor::className(),
							[
								'options' => [
									'rows' => 12,
								],
								'preset' => 'full',
							]
						)->label($this->label);
						if (!empty($this->editorOptions)) {
							foreach ($this->editorOptions as $functionName => $args) {
								$content->{$functionName}($args);
							}
						}
				}

				$items[] = [
					'label' => $countryCode,
					'content' => $content,
				];
			}

			$widgetsString .= Html::error($this->model, "$modelValueName");
			$widgetsString .= $this->createWidgetForModelProperty($items);
		}

		return $widgetsString;
	}

	/**
	 * @param array $items
	 *
	 * @return string
	 * @throws \Exception
	 */
	protected function createWidgetForModelProperty(array $items)
	{
		return Tabs::widget(
			[
				'items' => $items,
				'options' => ['tag' => 'div'],
				'itemOptions' => ['tag' => 'div'],
				'headerOptions' => ['class' => 'my-class'],
				'clientOptions' => ['collapsible' => false],
			]
		);
	}
}
