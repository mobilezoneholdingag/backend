<?php
/**
 * Created by PhpStorm.
 * User: fietekorb
 * Date: 24.06.16
 * Time: 14:27
 */

namespace components\i18n;


use yii\i18n\MissingTranslationEvent;

/**
 * Class TranslationEventHandler
 * @package components\i18n
 */
class TranslationEventHandler
{
    /**
     * @param MissingTranslationEvent $event
     */
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        $event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR LANGUAGE {$event->language} @";
    }
}