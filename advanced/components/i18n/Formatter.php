<?php

namespace components\i18n;

use yii\base\Arrayable;
use yii\i18n\Formatter as YiiFormatter;

class Formatter extends YiiFormatter
{
	/**
	 * @param string|array|mixed $value array, JSON string or object
	 *
	 * @return string human readable string (print_r)
	 */
	public function asReadable($value)
	{
		if (is_null($value)) {
			return $this->nullDisplay;
		}

		if (is_string($value)) {
			$value = json_decode($value, true);
		}
		
		if ($value instanceof Arrayable) {
			$value = $value->toArray();
		}

		$decodedValue = htmlspecialchars(print_r($value, true), ENT_XML1, 'UTF-8');

		return "<pre style='max-width:400px;'>{$decodedValue}</pre>";
	}
}
