<?php

namespace components\i18n;


use lajax\translatemanager\behaviors\TranslateBehavior;
use Yii;

/**
 * Class TranslateModelBehavior
 * @package components
 */
class TranslatableModelBehavior extends TranslateBehavior
{
    /**
     * Hooking on event AFTER_FIND:
     * Translates a message to the specified language and sets up translations array with alternatiove languages.
     * 
     * @param \yii\base\Event $event
     */
    public function translateAttributes($event)
    {
        /* @var $owner TranslatableActiveRecord */
        $owner = $this->owner;
        $owner->prepareTranslations();
    }

    /**
     * Prevent from overwriting base translation message erntry that is supposed to be an unique id.
     * @param \yii\base\Event $event
     * @return null|void
     */
    public function saveAttributes($event)
    {
        /* @var $owner TranslatableActiveRecord */
        $owner = $this->owner;
        $owner->saveTranslations($event);
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::events(),
            [
                TranslatableActiveRecord::EVENT_BEFORE_VALIDATE => 'checkAndSetDefaults',
            ]
        );
    }

	/**
     * @param $event
     */
    public function checkAndSetDefaults($event) {
        /* @var $owner TranslatableActiveRecord */
        $owner = $this->owner;
        $owner->checkAndSetDefaults();
    }
}