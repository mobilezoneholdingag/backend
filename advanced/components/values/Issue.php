<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2016 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components\values;

use deinhandy\Jira\Issue as JiraIssue;

class Issue extends JiraIssue implements \JsonSerializable
{
	/**
	 * Raw data for JSON serialization.
	 *
	 * @return mixed
	 */
	public function jsonSerialize()
	{
		$rThis = new \ReflectionClass($this);
		$properties = [];

		/** @var \ReflectionProperty $property */
		foreach($rThis->getProperties() as $property) {
			$property->setAccessible(true);
			$name   = $property->getName();
			$value  = $property->getValue($this);

			$properties[$name] = $value;
		}

		return $properties;
	}

}