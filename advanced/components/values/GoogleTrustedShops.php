<?php

namespace components\values;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;

class GoogleTrustedShops
{
	/** @var int */
	protected $orderId;

	/** @var string */
	protected $customerEmail;

	/** @var array */
	protected $price = [
		'total' => 0.00,
		'shipping' => 0.00,
		'tax' => 0.00,
		'discounts' => 0.00
	];

	/** @var \DateTime */
	protected $shippingDate;

	/** @var \DateTime */
	protected $deliveryDate;

	/** @var bool */
	protected $hasPreorder = false;

	/** @var array */
	protected $items = [];

	protected $storeId;

	function __construct()
	{
		$this->storeId = \Yii::$app->params['googleStoreId'];
	}

	/**
	 * @return int
	 */
	public function getOrderId()
	{
		return $this->orderId;
	}

	/**
	 * @param int $orderId
	 * @return $this
	 */
	public function setOrderId($orderId)
	{
		$this->orderId = $orderId;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCustomerEmail()
	{
		return $this->customerEmail;
	}

	/**
	 * @param string $customerEmail
	 * @return $this
	 */
	public function setCustomerEmail($customerEmail)
	{
		$this->customerEmail = $customerEmail;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param array $price
	 * @return $this
	 */
	public function setPrice($price)
	{
		$this->price = $price;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getTotal()
	{
		return $this->price['total'];
	}

	/**
	 * @param float $price
	 * @return $this
	 */
	public function setTotal($price)
	{
		$this->price['total'] = (float)$price;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getShippingTotal()
	{
		return $this->price['shipping'];
	}

	/**
	 * @param float $price
	 * @return $this
	 */
	public function setShippingTotal($price)
	{
		$this->price['shipping'] = (float)$price;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getTax()
	{
		return $this->price['tax'];
	}

	/**
	 * @param float $price
	 * @return $this
	 */
	public function setTax($price)
	{
		$this->price['tax'] = (float)$price;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getDiscounts()
	{
		return $this->price['discounts'];
	}

	/**
	 * @param float $price
	 * @return $this
	 */
	public function setDiscounts($price)
	{
		$this->price['discounts'] = $price;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getShippingDate()
	{
		return $this->shippingDate;
	}

	/**
	 * @param \DateTime $shippingDate
	 * @return $this
	 */
	public function setShippingDate($shippingDate)
	{
		$this->shippingDate = $shippingDate;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getDeliveryDate()
	{
		return $this->deliveryDate;
	}

	/**
	 * @param \DateTime $deliveryDate
	 * @return $this
	 */
	public function setDeliveryDate($deliveryDate)
	{
		$this->deliveryDate = $deliveryDate;
		return $this;
	}

	/**
	 * @param  bool|null Empty , if it's a get call
	 * @return boolean|$this
	 */
	public function hasPreorder($hasPreorder = null)
	{
		if (empty($hasPreorder))
			return $this->hasPreorder;

		$this->hasPreorder = $hasPreorder;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @param array $items
	 * @return $this
	 */
	public function setItems($items)
	{
		$this->items = $items;
		return $this;
	}

	/**
	 * @param Article $article
	 * @param Tariffs $tariffs
	 * @param int $quantity
	 */
	public function addItem(Article $article, $tariffs = null, $quantity = 1)
	{
		$item = new \stdClass();

		$item->name = $article->product_name;
		$item->price = $article->price_sale;
		$item->quantity = $quantity;
		$item->gsid = (isset($article, $tariffs)) ? $article->id . '-' . $tariffs->id : $article->id;

		$this->items[] = $item;
	}

	/**
	 * @return mixed
	 */
	public function getStoreId()
	{
		return $this->storeId;
	}

	/**
	 * @param mixed $storeId
	 */
	public function setStoreId($storeId)
	{
		$this->storeId = $storeId;
	}


}