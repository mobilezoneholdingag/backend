<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services;

use OpenCloud\Common\Collection\PaginatedIterator;
use OpenCloud\ObjectStore\Resource\DataObject;
use OpenCloud\OpenStack;
use OpenCloud\Rackspace;
use yii\base\Component;

/**
 * The FileService manages pathing and upload of files destined for the Content Delivery Network.
 *
 * @package components
 */
class FileService extends Component
{
	public $remotePath = '';

	/** @var null|resource */
	protected $curlHandler = null;

	/** @var OpenStack */
	protected $client = null;

	const CONTAINER_LIVE    = 'CF-LIVE-01';
	const CONTAINER_STAGING = 'CF-STAGING-01';
	const CONTAINER_EXPORT  = 'CF_EXPORTS';

	public function __construct($config = [])
	{
		parent::__construct($config);

		$this->remotePath   = \Yii::$app->params['cdn'];

		$this->curlHandler  = curl_init($this->remotePath);
		$this->initCloudClient();
	}

	protected function initCloudClient()
	{
		try {
			$this->client = new Rackspace(Rackspace::UK_IDENTITY_ENDPOINT, \Yii::$app->params['host']['rackspace']);
		} catch(\Exception $e) {
			$this->client = false;
		}
	}

	protected function getObjectStoreService()
	{
		if ($this->client) {
			return $this->client->objectStoreService(null, 'LON');
		}

		return $this->client;
	}

	public function __destruct()
	{
		if (!empty($this->curlHandler))
			curl_close($this->curlHandler);
	}

	public function getUrlFromFilename($name)
	{
		$pos = strpos($name, '/');
		if ($pos > 0 || $pos === false)
			$name = '/'.$name;

		return $this->remotePath.$name;
	}

	public function getCurlHandler()
	{
		curl_reset($this->curlHandler);
		return $this->curlHandler;
	}


	public function doesExist($name)
	{
		$curlHandler = $this->getCurlHandler();

		curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curlHandler, CURLOPT_NOBODY, true);
		curl_setopt($curlHandler, CURLOPT_URL, $this->getUrlFromFilename($name));

		curl_exec($this->curlHandler);
		$status = curl_getinfo($this->curlHandler, CURLINFO_HTTP_CODE);

		return ($status >= 200 && $status < 400);
	}

	/**
	 * Lists files in a directory.
	 *
	 * @param string $directory The directory in which files are listed.
	 * @param string $container
	 *
	 * @return array Array of Arrays.
	 *          [
	 *              'fileName'  => The file name.
	 *              'url'       => The public url of the file.
	 *          ]
	 */
	public function listFiles($directory, $container = self::CONTAINER_LIVE)
	{
		$container = $this->getObjectStoreService()->getContainer($container);

		/** @var PaginatedIterator $collection */
		$collection = $container->objectList(['prefix' => $directory]);

		$fileList = [];

		while($collection->valid()) {
			/** @var DataObject $element */
			$element = $collection->current();
			$fileList[] = [
				'fileName' => basename($element->getName()),
				'url' => $element->getPublicUrl()->__toString()
			];

			$collection->next();
		}

		return $fileList;
	}
}