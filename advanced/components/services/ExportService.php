<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services;


use common\models\ArticleTariffs;
use components\services\ExportService\data\ArticleTariffsContainer;
use components\services\ExportService\events\FileExportCompleteEvent;
use components\services\ExportService\formats\CsvExportFormat;
use components\services\ExportService\formats\ExportFormatFactory;
use components\services\ExportService\partners\BasePartner;
use yii\base\Component;
use common\models\Export;
use yii\base\Exception;
use yii\db\Query;
use yii\web\Response;

/**
 * External partner's feed export is provided by this service, which manages the different partners and their feed
 * formats for the application.
 * @package components\services\ExportService
 */
class ExportService extends Component
{
	const EVENT_EXPORT_COMPLETE = 'dh.service.export.complete';

	/** @var BasePartner[] */
	public $partners = [];

	public $partnerModules = [];

	public $formats = [];

	public $exportDir;

	/** @var ExportFormatFactory */
	public $formatFactory = null;

	public function __construct($config = [])
	{
		parent::__construct($config);

		$this->formatFactory = new ExportFormatFactory($this->formats);

		if (($exportDir = $this->getExportDirectory()) != false && !is_dir($exportDir))
			@mkdir($exportDir);

		$partners = $this->partners;
		foreach($partners as $i => $partner)
		{
			/** @var BasePartner $newPartner */
			$newPartner = \Yii::createObject($partner);

			unset($this->partners[$i]);
			$this->partnerModules[$newPartner::TOKEN] = $newPartner;
		}

		$this->configurePartners($this->getExportConfigurations(['partner' => array_keys($this->partnerModules)]));
	}

	public function getAvailableFormats()
	{
		return $this->formatFactory->getFormatList();
	}

	public function hasModuleForPartner($token)
	{
		return isset($this->partners[$token]);
	}

	public function getPartnersList()
	{
		return array_keys($this->partners);
	}

	public function getPartnersListRaw()
	{
		return $this->partners;
	}

	protected function getExportDirectory()
	{
		return \Yii::getAlias($this->exportDir);
	}

	/**
	 * @param Export[]|array $exportConfigurations
	 */
	protected function configurePartners(array $exportConfigurations)
	{
		foreach($exportConfigurations as $cfg)
		{
			if ($cfg instanceof Export) {
				/** @var BasePartner $partner */
				$partner = clone $this->partnerModules[$cfg->partner];
				$partner->bid = $cfg->bid;
				$partner->key = $cfg->key;

				if (!isset($this->partners[$cfg->partner]))
					$this->partners[$cfg->partner] = [];

				$this->partners[$cfg->partner][] = $partner;
			}
		}
	}

	/**
	 * @param BasePartner $partner
	 * @param Export $exportConfiguration
	 *
	 * @return BasePartner
	 * @throws \Exception
	 */
	protected function assignFormatModule(BasePartner $partner, Export $exportConfiguration)
	{
		$partner->format = $this->formatFactory->getExportFormat($exportConfiguration->preferred_format);

		if ($partner->format instanceof CsvExportFormat) {
			$delimiter = false;
			switch($exportConfiguration->delimiter) {
				case CsvExportFormat::TAB:
					$delimiter = "\t";
					break;
				case CsvExportFormat::SPACE:
					$delimiter = " ";
					break;
				default:
					$delimiter = $exportConfiguration->delimiter;
					break;
			}

			if ($delimiter)
				$partner->format->delimiter = $delimiter;
		}

		return $partner;
	}

	/**
	 * @param array $criteria
	 * @return Export[]|array
	 */
	protected function getExportConfigurations(array $criteria)
	{
		return Export::find()->where($criteria)->andWhere(['status' => 1])->all();
	}

	/**
	 * Exports data to partner files.
	 *
	 * @param array $resources   Key-value pair(s) of export keys and writable resource URIs.
	 * @param Response $response The response to trigger when stream is outputted.
	 *
	 * @return array|BasePartner The partners to which data has been exported. Open streams to according files included.
	 *
	 * @throws Exception         Thrown if a partner does not have a corresponding module.
	 */
	public function exportToStreams(array $resources)
	{
		$exports = $this->getExportConfigurations(['key' => array_keys($resources)]);
		$partners = [];

		/** @var Export $export */
		foreach($exports as $export)
		{
			if (!isset($this->partners[$export->partner]))
			{
				echo 'Configured partner doesn\'t have a module.';
				continue;
			}

			/** @var BasePartner $partner */
			foreach($this->partners[$export->partner] as $partner) {
				$partner->bid           = $export->bid;
				$partner->extra_parameters   = $export->extra_parameters;
				$partner->minProfit     = $export->min_profit;
				$partner->key           = $export->key;
				$partner                = $this->assignFormatModule($partner, $export);
				$partners[$export->key] = $partner;
			}
		}

		$dataContainer = new ArticleTariffsContainer(ArticleTariffs::find()->all());

		/** @var BasePartner $partner */
		foreach($partners as &$partner)
		{
			$partner->format->open($resources[$partner->key]);
			$this->applyProductsToPartners($dataContainer, $partner);
		}

		return $partners;
	}

	/**
	 * Fills the feed file of a partner.
	 * @param ArticleTariffsContainer $articleTariffsContainer
	 * @param BasePartner $partner
	 */
	protected function applyProductsToPartners(ArticleTariffsContainer $articleTariffsContainer, BasePartner $partner)
	{
		if (($customQuery = $partner->getArticleTariffs()) != false)
			$articleTariffsContainer = new ArticleTariffsContainer($customQuery->all());

		foreach($articleTariffsContainer->getArticleTariffs() as $i => $articleTariff)
		{
			$article = $articleTariffsContainer->getArticle($articleTariff->article_id);
			$tariff  = $articleTariffsContainer->getTariff($articleTariff->tariff_id);

			/** @var BasePartner $partner */
			if (ArticleTariffs::calcProfit($article, $tariff, $articleTariff) >= $partner->minProfit) {
				$partner->addProduct($articleTariff, $article, $tariff);
			}
		}

		$partner->finish();
	}
}