<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de
 */
namespace components\services;


use yii\base\Component;

/**
 * The ColorService provides static methods for grouping device colors.
 * @package components
 * @static
 */
class ColorService extends Component
{
	const WHITE     = 'white';
	const GREY      = 'grey';
	const BLACK     = 'black';
	const RED       = 'red';
	const GOLD      = 'gold';
	const SILVER    = 'silver';
	const BLUE      = 'blue';
	const YELLOW    = 'yellow';

	static protected $colorMap = [
		self::WHITE => [
			'white',
			'htc-desire-white-red',
			'htc-desire-white-blue'
		],
		self::GREY => [
			'grey',
			'silver'
		],
		self::BLACK => [
			'black',
			'charcoal black',
			'leather-black'
		],
		self::RED => [
			'red',
			'htc-desire-white-red',
			'leather-red',
			'leather-brown'
		],
		self::GOLD => [
			'gold'
		],
		self::SILVER => [
			'silver'
		],
		self::BLUE => [
			'cyan',
			'htc-desire-white-blue',
			'i5-blue'
		],
		self::YELLOW => [
			'yellow'
		]
	];

	static public function getColorByProductColorName($productColorName)
	{
		foreach(self::$colorMap as $color => $names) {
			if (in_array($productColorName, $names))
				return $color;
		}

		return null;
	}

	static public function getProductColorNames($color)
	{
		if (isset(self::$colorMap[$color]))
			return self::$colorMap[$color];

		return null;
	}

	static public function getPalette()
	{
		return array_keys(self::$colorMap);
	}
}