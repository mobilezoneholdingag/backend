<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services;


use yii\base\Component;

/**
 * Provides the generation of primitive indexes for the search component.
 * @package components
 */
class IndexService extends Component {

	public $indexFiles = [
		'devices' => '',
		'tariffs' => ''
	];

	public $owner = [
		'user' => 'www-data',
		'group' => 'www-data'
	];

	public $entries = 6;

	public function openIndexFiles()
	{
		$devicesIndexFilename = \Yii::getAlias($this->indexFiles['devices']);
		$tariffsIndexFilename = \Yii::getAlias($this->indexFiles['tariffs']);

		$fhDeviceIndex = @fopen($devicesIndexFilename, 'r');
		$fhTariffIndex = @fopen($tariffsIndexFilename, 'r');

		return [$fhDeviceIndex, $fhTariffIndex];
	}

	public function search($searchQuery, $entries = null, array &$results = null, $type = null)
	{
		$entries = $entries ?: $this->entries;

		if (!$results) {
			$results = [];
			foreach($this->indexFiles as $k => $f) {
				$results[$k] = [];
			}
		}

		$rankedMatchesDevices = $rankedMatchesTariffs = [];

		$matchedLinesTariffs = $matchedLinesDevices = [];
		$anyDevices = $anyTariffs = [];

		$pq = preg_replace('/(\d+)\s?GB/i', '$1GB', $searchQuery);

		$patterns = [
			'/,"?'.$pq.'"?,/i',
			'/'.str_replace(' ','.+',$pq).'/i',
		];

		list ($fhDeviceIndex, $fhTariffIndex) = $this->openIndexFiles();

		$totalMatches = 0;
		$sources = [
			[
				'fileHandle'    => &$fhDeviceIndex,
				'result'        => &$rankedMatchesDevices,
				'matchedLines'  => &$matchedLinesDevices
			],
			[
				'fileHandle'    => &$fhTariffIndex,
				'result'        => &$rankedMatchesTariffs,
				'matchedLines'  => &$matchedLinesTariffs
			]
		];

		while(($totalMatches < $entries || $entries == -1) && current($sources))
		{
			$source = each($sources)[1];
			$totalMatches += $this->rankMatchings($source['fileHandle'], $patterns, $source['result'], $source['matchedLines']);//die();
		}

		if (($totalMatches < $entries || $entries == -1)) {
			$altSources = [
				[
					'fileHandle'    => &$fhDeviceIndex,
					'result'        => &$anyDevices,
					'matchedLines'  => &$matchedLinesDevices
				],
				[
					'fileHandle'    => &$fhTariffIndex,
					'result'        => &$anyTariffs,
					'matchedLines'  => &$matchedLinesTariffs
				]
			];

			$altPatterns = [];
			foreach(explode(' ', $pq) as $pqComponent) {
				if (!is_numeric($pqComponent)) // Don't mismatch model no's for memory seeking
					$altPatterns[] = '/'.$pqComponent.'/i';
			}

			while(($totalMatches < $entries || $entries == -1) && current($altSources))
			{
				$source = each($altSources)[1];
				rewind($source['fileHandle']);
				$totalMatches += $this->rankMatchings($source['fileHandle'], $altPatterns, $source['result'], $source['matchedLines'], true);
				$this->sortByMostMatchesPerLine($source['result']);
			}
		}

		$anys = ['devices' => $anyDevices, 'tariffs' => $anyTariffs];
		$regs = ['devices' => $rankedMatchesDevices, 'tariffs' => $rankedMatchesTariffs];

		foreach($regs as $resultKey => $reg)
		{
			if ($type && $type != $resultKey) {
				continue;
			}
			$ids = [];
			if (empty($reg) && !empty($anys[$resultKey])) {
				foreach($anys[$resultKey] as $rankedMatches) {
					foreach($rankedMatches['lines'] as $id) {
						$ids[] = $id;

						if ($entries > 0 && count($ids) >= $entries)
							break 2;
					}

				}
			} else {
				foreach($reg as $rankedMatches) {
					foreach($rankedMatches as $id => $matches) {
						$ids[] = $id;

						if ($entries > 0 && count($ids) >= $entries)
							break 2;
					}
				}
			}

			$results[$resultKey] = $ids;
		}

		return $results;
	}

	protected function rankMatchings($fh, $patterns, array &$rankedMatches, array &$matchedIds = [], $meltResults = false)
	{
		$linePosition = 1;
		$allMatches = 0;

		while(!feof($fh)) {
			$line = fgets($fh, 4096);
			if ($linePosition == 1) {
				$linePosition++;
				continue;
			}

			$matches = [];

			foreach ($patterns as $i => $pattern) {
				$rank = $meltResults ? 0 : $i;
				if (preg_match($pattern, $line, $matches) && (!in_array($linePosition, $matchedIds) || $meltResults)) {
					$matchCount = count($matches);
					$allMatches += $matchCount;
					$id = [];
					preg_match('/^(\d+)/', $line, $id);
					$id = current($id);

					if (!isset($rankedMatches[$rank]))
						$rankedMatches[$rank] = [];

					if (!isset($rankedMatches[$rank][$id]))
						$rankedMatches[$rank][$id] = 0;

					$rankedMatches[$rank][$id] += $matchCount;

					$matchedIds[] = $id;
				}
			}

			ksort($rankedMatches);

			$linePosition++;
		}

		if ($meltResults && $allMatches) {
			$rankedMatches = $rankedMatches[0];
		}

		return $allMatches;
	}
	protected function sortByMostMatchesPerLine(array &$rankedMatches)
	{
		$mostMatchesPerLine = [];
		$matchIndex = [];

		foreach($rankedMatches as $line => $matches)
		{
			if ($key = array_search($matches, $matchIndex)) {
				$matchRegister = $mostMatchesPerLine[$key];
			} else {
				$matchRegister = [
					'matches' => $matches,
					'lines' => []
				];
			}

			if (!in_array($line, $matchRegister['lines']))
				$matchRegister['lines'][] = $line;

			if ($key === false) {
				$mostMatchesPerLine[] = $matchRegister;
				$matchIndex[] = $matches;
			} else {
				$mostMatchesPerLine[$key] = $matchRegister;
			}
		}

		$rankedMatches = $mostMatchesPerLine;
	}

	static public function isTop($fh, $id)
	{
		fseek($fh, 0);
		while(!feof($fh))
		{
			$line = fgets($fh);
			if (preg_match('/^'.$id.'.*is_top$/', $line) == 1) {
				return true;
			}
		}

		return false;
	}
}