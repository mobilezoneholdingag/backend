<?php
/**
 * @author    Dominic Guhl <dominic.guhl@deinhandy.de>
 * @copyright © 2016 DEINHANDY.de, a Mister Mobile GmbH brand
 */

namespace components\services;

use components\values\Issue;
use deinhandy\Jira\Issues\Walker;
use yii\base\Component;
use deinhandy\Jira\Api as JiraApi;
use deinhandy\Jira\Api\Authentication\Basic as ApiAuthBasic;
use deinhandy\Jira\Api\Client\CurlClient as ApiClientCurl;

/**
 * Manages development operations on this project.
 * @package components\services
 */
class OperationsService extends Component
{
	/**
	 * @var null|JiraApi
	 */
	public $jiraApi = null;

	/**
	 * Prepends an issue, so it can be called up on the browser.
	 *
	 * @var string
	 */
	public $urlPrefix = '';

	/**
	 * Instances managed by the operations services.
	 *
	 * @var array
	 */
	public $instances = [];

	/**
	 * Initializes the OperationsService and the JIRA API service below.
	 *
	 * @param array $config
	 */
	public function __construct(array $config = [])
	{
		parent::__construct($config);

		$this->getJiraService();
	}

	/**
	 * Returns the JIRA API service instance.
	 *
	 * @return JiraApi|null
	 */
	protected function getJiraService()
	{
		// If this service is not configured by the DIC
		if (empty($this->jiraApi)) {
			$params = \Yii::$app->params['jira'];
			$this->jiraApi = new JiraApi(
				$params['url'],
				new ApiAuthBasic(
					$params['credentials']['username'],
					$params['credentials']['password']
				),
				new ApiClientCurl()
			);
		}

		return $this->jiraApi;
	}


	/**
	 * Changes working directory to app directory, ensuring you're not on
	 * a foreign repo, e.g. one of a dependency.
	 */
	static protected function toAppDir()
	{
		@chdir(\Yii::getAlias('@app'));
	}

	/**
	 * Returns the latest local commit hash.
	 *
	 * @return string
	 */
	static public function getHeadCommitHash()
	{
		self::toAppDir();
		return trim(shell_exec('git log -n 1 --format="%H"'));
	}

	/**
	 * Returns current branch name.
	 *
	 * @return string
	 */
	static public function getHeadBranch()
	{
		self::toAppDir();
		return trim(shell_exec('git rev-parse --abbrev-ref HEAD'));
	}

	/**
	 * @param string $origin
	 * @return bool
	 */
	static public function isOriginExisting($origin = null)
	{
		self::toAppDir();

		if (empty($origin))
			$origin = self::getHeadBranch();

		return (bool) trim(@shell_exec('git branch --list origin/'.$origin.' --remote | wc -l'));
	}

	/**
	 * Refreshes local repo info.
	 */
	static public function refreshInfo()
	{
		self::toAppDir();
		@shell_exec('git fetch --all --quiet');
	}

	/**
	 * An array of commit logs which are in $c1, but not in $c2
	 *
	 * @param string $c1                (Optional) The younger reference with newer commits. Defaults: 'dev'
	 * @param string $c2                (Optional) The older reference. Defaults: 'master'
	 * @param string $commandAppend     (Optional) The opportunity to append some options and arguments to the git log
	 *                                  command. Defaults: ''
	 *
	 * @return array                Commit logs.
	 */
	static public function getDifferingLog($c1 = 'dev', $c2 = 'master', $commandAppend = '')
	{
		self::toAppDir();
		self::refreshInfo();

		if (self::isOriginExisting($c1) == false || self::isOriginExisting($c2) == false)
			return [];

		$output         = [];
		$shellReturn    = -1;

		@exec('git log origin/' . $c2 . '..origin/' . $c1 . ' --oneline --no-merges --quiet '.$commandAppend, $output, $shellReturn);

		if ($shellReturn === 0) {
			return $output;
		}

		return [];
	}

	static protected function fetchUniqueTicketNames($prefix = 'APP-')
	{
		$log = self::getDifferingLog('dev', 'master', '--grep=^'.$prefix.' --all-match -i --format="%s"');
		$list = [];
		$matches = [];

		foreach ($log as $logEntry) {
			preg_match('/^('.preg_quote($prefix).'\d+)/', $logEntry, $matches);
			if (!in_array($matches[1], $list))
				$list[] = $matches[1];
		}

		return $list;
	}

	/**
	 * @param string        $name   Name of the issue.
	 *
	 * @return Issue|null           Null, if the issue does not exist or other errors appeared.
	 */
	public function getTicket($name)
	{
		$service = $this->getJiraService();

		$ticket = new Issue($service->getIssue($name)->getResult());
		$xi     = $ticket->getExpandedInformation();

		if (isset($xi['errors'])) {
			$errorMessage = 'Unknown error occured on calling JIRA issue "'.$name.'"';

			if (!empty($xi['errorMessages'])) {
				$errorMessage = 'JIRA errors on issue '.$name.': '.$xi['errorMessages'][0];
			}

			\Yii::error($errorMessage, 'application.operation-service');

			return null;
		}

		return $ticket;
	}

	/**
	 * Compares commits or branches or tags to find tickets to query.
	 *
	 * @param string $recent    A commit hash or a branch or a tag containing recent commits.
	 * @param string $stable    A commit hash or a branch or a tag containing stable commits.
	 *
	 * @return Walker
	 */
	public function getNewTickets($recent = 'dev', $stable = 'master')
	{
		$service = $this->getJiraService();

		$this->refreshInfo();
		$issueList = self::fetchUniqueTicketNames();

		$issues = new Walker($service);
		$issues->push('key IN ('.join(',', $issueList).')');

		$issues->valid();

		return $issues;
	}
}