<?php
namespace components\services;

use DateTime;
use RuntimeException;
use yii\base\Component;

/**
 * Service for simple handling of FTPS.
 */
class FtpsService extends Component
{
	public $credentials = [
		'host' => '',
		'port' => '',
		'user' => '',
		'password' => ''
	];

	public $parentDirectory = '/dev';
	private $ftpsHandle;
	public $persistentConnection = true;

	/**
	 * @param string $data Data string to be put into the file.
	 * @param string $file Filename on the remote host, including the full path.
	 */
	public function putDataToFtps($data, $file)
	{
		$ftp = $this->getFtpsHandle();

		$stream = fopen('data://text/plain;base64,' . base64_encode($data), 'r');

		if (ftp_fput($ftp, $this->getFullPath($file), $stream, FTP_BINARY) === false) {
			throw new RuntimeException('Could not upload data to FTP.');
		}

		$this->closeFtpsHandle();
		fclose($stream);
	}

	/**
	 * @param string $path
	 *
	 * @return array
	 */
	public function getFileListFromFtps($path = '')
	{
		$ftps = $this->getFtpsHandle();

		$list = ftp_rawlist($ftps, $this->getFullPath($path));

		$fileList = [];
		foreach ($list as $dirElement) {
			preg_match('#^-.+?([\d]+) ([a-zA-Z]{3,4} +[0-9]{1,2} [0-9]{2}:[0-9]{2}) (.+)#', $dirElement, $matches);
			if (isset($matches[3])) {
				$filename = $matches[3];
				$fileList[$filename] = [];
				if (isset($matches[1])) {
					$fileList[$filename]['size'] = $matches[1];
				}
				if (isset($matches[2])) {
					$fileDateTime = date_create_from_format('M d H:i', $matches[2]);
					$fileList[$filename]['date_month'] = $fileDateTime->format('m');
					$fileList[$filename]['date_day'] = $fileDateTime->format('d');
					$fileList[$filename]['time'] = $fileDateTime->format('H:i');
				}
			}
		}
		$this->closeFtpsHandle();
		return $fileList;
	}

	/**
	 * @param $filename
	 * @param string $subPath
	 *
	 * @return bool|string
	 */
	public function getFileDataFromFtps($filename, $subPath = '')
	{
		$ftps = $this->getFtpsHandle();

		ob_start();
		if (ftp_get($ftps, "php://output", $this->getFullPath($subPath) . '/' . $filename, FTP_BINARY) === false) {
			ob_end_clean();
			throw new RuntimeException('Could not download data from FTP.');
		}
		$data = ob_get_contents();
		ob_end_clean();
		$this->closeFtpsHandle();

		return $data;
	}

	/**
	 * @param string $file
	 *
	 * @return DateTime
	 */
	public function getLastModified(string $file) : DateTime
	{
		$file = $this->getFullPath($file);
		$ftp = $this->getFtpsHandle();

		$lastModified = ftp_mdtm($ftp, $file);
		if ($lastModified === -1) {
			throw new RuntimeException('Could not get last modification date from FTP.');
		}

		$dateTime = new DateTime();
		$dateTime->setTimestamp($lastModified);

		return $dateTime;
	}

	public function deleteFile(string $filePath) : bool
	{
		$ftp = $this->getFtpsHandle();
		return ftp_delete($ftp, $this->getFullPath($filePath));
	}

	public function copyFile(string $path, string $destination)
	{
		$data = $this->getFileDataFromFtps($path);
		$this->putDataToFtps($data, $destination);
	}

	public function getFileList(string $path)
	{
		return ftp_nlist($this->getFtpsHandle(), $path);
	}

	/**
	 * @param $subPath
	 *
	 * @return string
	 */
	private function getFullPath($subPath)
	{
		return rtrim($this->parentDirectory, '/') . '/' . trim($subPath, '/');
	}

	/**
	 * @return resource
	 */
	protected function getFtpsHandle()
	{
		$this->closeFtpsHandle();
		$ftp = $this->ftpsHandle;
		if ($ftp === null) {
			$ftp = ftp_ssl_connect($this->credentials['host'], $this->credentials['port']);
			ftp_login($ftp, $this->credentials['user'], $this->credentials['password']);
			ftp_pasv($ftp, true);
			$this->ftpsHandle = $ftp;
		}

		return $ftp;
	}

	protected function closeFtpsHandle()
	{
		if ($this->persistentConnection === false && $this->ftpsHandle !== null) {
			ftp_close($this->ftpsHandle);
			$this->ftpsHandle = null;
		}
	}
}
