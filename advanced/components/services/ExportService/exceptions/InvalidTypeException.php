<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\exceptions;


use yii\base\Exception;

class InvalidTypeException extends Exception
{
	public function getName()
	{
		return 'InvalidTypeException';
	}

	public function __construct($message = "", $code = 0, \Exception $previous = null)
	{
		if (empty($message))
			$message = "Invalid type exception.";

		\Exception::__construct($message, $code, $previous);
	}
}