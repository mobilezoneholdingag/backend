<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\events;


use yii\base\Event;

/**
 * Class FileExportCompleteEvent
 *
 * Thrown once a file export completes.
 * @package components\services\ExportService\events
 */
class FileExportCompleteEvent extends Event
{


	public $key;
}