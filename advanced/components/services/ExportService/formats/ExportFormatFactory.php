<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\formats;


use yii\base\Exception;

class ExportFormatFactory
{
	/** @var BaseExportFormat[] */
	static protected $formats = [];

	public function __construct(array $formats)
	{
		/**
		 * @var string|int $format
		 * @var BaseExportFormat $formatClass
		 */
		foreach($formats as $format => $formatClass)
		{
			if(!is_string($formatClass) && !is_array($formatClass))
				continue;

			if (!is_string($format)) // Enables aliasing of format classes
				$format = $formatClass::SUFFIX;

			$requirements = [];
			if (is_array($formatClass)) {
				if (isset($formatClass['requires']))
					$requirements = $formatClass['requires'];

				if (isset($formatClass['class'])) {
					$formatClass = $formatClass['class'];
				} else {
					throw new \Exception('Parameter "class" required for export format definition.');
				}
			}

			$formatDefinition = [
				'class' => $formatClass,
				'requirements' => $requirements
			];

			self::$formats[$format] = $formatDefinition;
		}
	}

	static public function getFormatList()
	{
		$formats = array_keys(self::$formats);
		$formatList = [];
		foreach($formats as $format)
		{
			$formatList[$format] = $format;
		}

		return $formatList;
	}

	/**
	 * @param string $suffix The format suffix.
	 * @param array $config (Optional.) Additional configuration to the format, if needed.
	 * @return bool|BaseExportFormat Returns false, if there is no handler registered for that suffix.
	 * @throws \Exception Thrown if a required parameter is missing
	 */
	static public function getExportFormat($suffix, array &$config = [])
	{
		$definition = self::$formats[$suffix] ?: false;

		if ($definition) {
			$format = (new $definition['class']()) ?: false;

			if (is_object($format)) {
				foreach ($definition['requirements'] as $requirement) {
					if (!isset($config[$requirement]))
						throw new \Exception(sprintf('Parameter %s required for format %s', $requirement, $suffix));

					$format->$requirement = $config[$requirement];
					unset($config[$requirement]);
				}
			}
		} else {
			$format = false;
		}

		return $format;
	}
}