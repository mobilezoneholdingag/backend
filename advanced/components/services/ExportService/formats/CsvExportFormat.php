<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\formats;
use components\services\ExportService\exceptions\InvalidTypeException;

/**
 * Class CsvExportFormat
 * @package components\services\ExportService\formats
 */
class CsvExportFormat extends BaseExportFormat
{
	const SUFFIX = 'csv';

	const TAB = 'tab';
	const SPACE = 'space';

	public $delimiter = ',';

	protected $firstLine;

	public function __construct($delimiter = null)
	{
		if ($delimiter)
			$this->delimiter = $delimiter;
	}

	/**
	 * @param array $data
	 * @throws InvalidTypeException
	 * @throws \Exception
	 */
	public function export($data)
	{
		if (($handle = $this->handle) == false)
			throw new \Exception('No handle set to export.');

		if (!is_array($data))
			throw new InvalidTypeException(sprintf('Expected type: %s. Type delivered: %s', 'array', get_class($data)));

		if (!$this->firstLine) {
			$this->firstLine = array_keys($data[0]);
			fputcsv($handle, $this->firstLine, $this->delimiter);
		}

		foreach($data as $line) {
			fputcsv($handle, $line, $this->delimiter);
			fflush($handle);
		}
	}
}