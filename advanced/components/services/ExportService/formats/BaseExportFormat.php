<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\formats;


use yii\base\Component;

abstract class BaseExportFormat extends Component
{
	const SUFFIX = '';

	const EVENT_STREAM_EXPORT_COMPLETE = 'dh.service.export.file_export_complete';

	public $fileName;

	/**
	 * @var resource;
	 */
	public $handle;

	public function export($data) {}

	/**
	 * Opens a stream for output. Replaces open handle.
	 *
	 * @param resource|string|null  $resourceToken  (Optional.) Strings will be opened as file names, the fileName property will be taken if resourceToken is null. (Default: null)
	 *
	 * @return resource
	 */
	public function open($resourceToken = null)
	{
		$resourceToken = $resourceToken ?: $this->fileName ?: null;

		if ($this->handle && is_resource($this->handle))
			fclose($this->handle);

		if ($resourceToken) {
			if (!is_resource($resourceToken) && is_string($resourceToken)) {
				$this->handle = fopen($resourceToken, 'w+');
			} else {
				$this->handle = $resourceToken;
			}
		}

		return $this->handle;
	}

	public function writeOut() {}
}