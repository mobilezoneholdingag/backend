<?php
/**
 * Created by PhpStorm.
 * User: dguhl
 * Date: 21.05.15
 * Time: 14:02
 */

namespace components\services\ExportService\formats;


use DOMElement;
use DOMText;

class XmlExportFormat extends BaseExportFormat
{
	const SUFFIX = 'xml';

	public $rootTag = 'response';

	protected $itemTag = 'item';

	/** @var \XMLWriter  */
	protected $xmlWriter;

	public function __construct($rootTag = null)
	{
		if ($rootTag)
			$this->rootTag = $rootTag;
	}

	public function open($resourceToken = null)
	{
		parent::open($resourceToken);

		$this->xmlWriter = new \XMLWriter();
		$this->xmlWriter->openMemory();
		$this->xmlWriter->startDocument('1.0', 'utf-8');
		$this->xmlWriter->startElement($this->rootTag);
		$this->xmlWriter->setIndent(true);
	}

	public function export($data)
	{
		$this->build($data);
	}

	/**
	 * @param mixed $data
	 */
	protected function build($data)
	{
		if (is_array($data))
		{
			foreach($data as $tag => $content)
			{
				$tagName = is_int($tag) ? $this->itemTag : $tag;
				if (is_array($content)) {
					$this->xmlWriter->startElement($tagName);
					$this->build($content);
					$this->xmlWriter->endElement();
				} else {
					if (strpos($content, 'CDATA:') !== 0) {
						$this->xmlWriter->writeElement($tagName, $content);
					} else {
						$content = substr($content, strlen('CDATA:'));
						$this->xmlWriter->startElement($tagName);
						$this->xmlWriter->writeCdata($content);
						$this->xmlWriter->endElement();
					}
				}
				fwrite($this->handle, $this->xmlWriter->flush());
			}
		}
	}

	public function writeOut()
	{
		while ($this->xmlWriter->endElement() !== false) { continue; };
		$this->xmlWriter->endDocument();
		fwrite($this->handle, $this->xmlWriter->flush());
	}
}