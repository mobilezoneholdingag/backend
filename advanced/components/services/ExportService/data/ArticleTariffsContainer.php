<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\data;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Provider;
use common\models\Tariffs;

class ArticleTariffsContainer
{
	/** @var ArticleTariffs[]  */
	protected $articleTariffs = [];

	/** @var Article[]  */
	protected $articles = [];

	/** @var Tariffs[] */
	protected $tariffs = [];

	/**
	 * @param ArticleTariffs[] $articleTariffs
	 * @param boolean $filterActive (Optional. Default: true) Filter only active articles and tariffs
	 */
	public function __construct(array $articleTariffs, $filterActive = true)
	{
		$articleIds = $tariffIds = [];

		/** @var ArticleTariffs $articleTariff */
		foreach($articleTariffs as $articleTariff) {
			$articleIds[]   = $articleTariff->article_id;
			$tariffIds[]    = $articleTariff->tariff_id;
		}

		$articleCriteria = ['id' => $articleIds];
		$tariffCriteria = [Tariffs::tableName().'.id' => $tariffIds];

		if ($filterActive) {
			$articleCriteria['is_active'] = true;
			$tariffCriteria = array_merge($tariffCriteria, [
				Tariffs::tableName().'.is_active' => true,
				Provider::tableName().'.status' => Provider::IS_ACTIV
			]);
		}

		$articles = Article::find()->where($articleCriteria)->all();
		$tariffs = Tariffs::find()->where($tariffCriteria)->joinWith(Provider::tableName())->all();

		/** @var Article $article */
		foreach($articles as $article) {
			$this->articles[$article->id] = $article;
		}

		/** @var Tariffs $tariff */
		foreach($tariffs as $tariff) {
			$this->tariffs[$tariff->id] = $tariff;
		}

		if ($filterActive) {
			foreach ($articleTariffs as $articleTariff) {
				if (isset($this->articles[ $articleTariff->article_id ]) && isset($this->tariffs[ $articleTariff->tariff_id ]))
					$this->articleTariffs[] = $articleTariff;
			}
		} else {
			$this->articleTariffs = $articleTariff;
		}
	}

	/**
	 * @param int $id
	 * @param boolean $default (Optional. Default: false)
	 * @return Article|$default
	 */
	public function getArticle($id, $default = false)
	{
		return $this->articles[$id] ?: $default;
	}

	/**
	 * @param int $id
	 * @param boolean $default (Optional. Default: false)
	 * @return Tariffs|$default
	 */
	public function getTariff($id, $default = false)
	{
		return $this->tariffs[$id] ?: $default;
	}

	/**
	 * @return ArticleTariffs|\common\models\ArticleTariffs[]
	 */
	public function getArticleTariffs()
	{
		return $this->articleTariffs;
	}
}