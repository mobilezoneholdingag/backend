<?php

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;

class CriteoPartner extends BasePartner
{
	const TOKEN = 'Criteo';

	protected $supportedFormats = ['csv'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		$this->format->export([[
			'id' => $article->id . '-' . $tariff->id,
			'title' => $this->shortProductName($article, $tariff),
			'description' => $this->description($article),
			'google_product_category' => 'Elektronik > Kommunikationsgeräte > Telefone > Mobiltelefone > Smartphones',
			'Produkttyp' => 'Handys ' . $article->manufacturerTitle . ' mit Vertrag',
			'link' => '',
			'image_link' => '',
			'condition' => 'neu',
			'availability' => 'Auf Lager',
			'brand' => $article->manufacturerTitle,
			'gtin' => $article->manufacturer_article_id,
			'price' => $tariff->price_monthly,
			'txt' => ((int)round($articleTariff->price)) . ' €',
			'recommendable' => $this->isRecommended($articleTariff, $article, $tariff),
		]]);
	}

	/**
	 * @param Article $article
	 * @param Tariffs $tariff
	 * @param ArticleTariffs $articleTariff
	 * @return mixed
	 */
	protected function isRecommended(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		return (($article->articleGroup->start_tariff_id == $tariff->id) && round($articleTariff->price) < 100) ? 'true' : 'false';
	}
}
