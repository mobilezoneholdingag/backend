<?php
/**
 * @author Sascha Mummert <sascha.mummert@deinhandy.de>
 */

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;

class DynamicYieldPartner extends BasePartner
{
	const TOKEN = 'Dynamic Yield';

	protected $supportedFormats = ['csv'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		$this->format->export([[
			'sku' => $article->id . '-' . $tariff->id,
			'name' => $article->product_name . ', ' . $article->memory . 'GB' . ', ' . $article->color_label . ', ' . $tariff->provider->title . ', ' . $tariff->title,
			'url' => '',
			'price' => $articleTariff->price,
			'in_stock' => 'true',
			'image_url' => '',
			'categories' => $article->productCategoryLabel,
		]]);
	}
}
