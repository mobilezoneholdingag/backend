<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use common\helper\UrlHelper;

class AffilinetPartner extends BasePartner
{
	const TOKEN = 'Affilinet';

	protected $supportedFormats = ['csv'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		$this->format->export([[
			'id' => $article->id . '-' . $tariff->id,
			'title' => $this->productName($article, $tariff),
			'description' => $this->description($article),
			'google_product_category' => 'Elektronik > Kommunikationsgeräte > Telefone > Mobiltelefone > Smartphones',
			'Produkttyp' => 'Handys ' . $article->manufacturerTitle . ' mit Vertrag',
			'link' => UrlHelper::urlToDevice($article, $this->bid),
			'image_link' => '',
			'condition' => 'neu',
			'availability' => 'Auf Lager',
			'price' => $articleTariff->price,
			'brand' => $article->manufacturerTitle,
			'gtin' => $article->id,
		]]);
	}
}
