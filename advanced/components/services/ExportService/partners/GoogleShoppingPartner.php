<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;

class GoogleShoppingPartner extends BasePartner
{
	const TOKEN = 'Google Shopping';

	protected $supportedFormats = ['csv'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		$this->format->export([[
			'id' => $article->id . '-' . $tariff->id,
			'title' => $article->manufacturerTitle . ' ' . $this->shortProductName($article, $tariff) . ' ' . $article->color_label,
			'description' => $article->articleGroup->article_feed_description . ' ' . $tariff->tariff_feed_description,
			'google_product_category' => 'Elektronik > Kommunikationsgeräte > Telefone > Mobiltelefone > Smartphones',
			'Produkttyp' => 'Handys ' . $article->manufacturerTitle . ' mit Vertrag',
			'link' => '',
			'image_link' => '',
			'brand' => $article->manufacturerTitle,
			'condition' => 'neu',
			'availability' => 'Auf Lager',
			'price' => $articleTariff->price,
			'gtin' => $article->manufacturer_article_id,
			'color' => $article->color_label,
		]]);
	}
}
