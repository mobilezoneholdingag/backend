<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\partners;

use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use components\services\ExportService\formats\BaseExportFormat;
use yii\base\Component;

/**
 * Class BasePartner
 * @package components\services\ExportService\partner
 */
abstract class BasePartner extends Component
{
	/** Partner token to identify */
	const TOKEN = 'base';

	protected $supportedFormats = [];

	/** @var mixed */
	protected $exportData;

	/** @var BaseExportFormat The format the partner takes the feed in */
	public $format = null;

	/** @var string The file name for the feed.  */
	public $fileName = '';

	/** @var string The partner's backlink identifier */
	public $bid = '';

	public $extra_parameters = '';

	/** @var string Export key */
	public $key = '';

	/** @var float The minimal profit a partner is targeted with */
	public $minProfit = 0.00;

	public function __construct($config = [])
	{
		parent::__construct($config);
	}

	/**
	 * Adds a product to this partner.
	 *
	 * @param ArticleTariffs $articleTariff
	 * @param Article $article
	 * @param Tariffs $tariff
	 * @return array
	 */
	abstract public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff);

	/**
	 * Writes out remaining data.
	 */
	public function finish()
	{
		$this->format->writeOut();
	}

	public function getDefaultFileName()
	{
		if ($this->format) {
			/** @var BaseExportFormat $className */
			$className = get_class($this->format);
			return urlencode(join('.', [strtolower(self::TOKEN), $className::SUFFIX]));
		}

		return urlencode(strtolower(self::TOKEN));
	}

	protected function productName(Article $article, Tariffs $tariff)
	{
		return $article->uniqueTitle . ' + ' . $tariff->provider->title . ' ' . $tariff->title;
	}

	protected function shortProductName(Article $article, Tariffs $tariff)
	{
		return $article->shortName . ' + ' . $tariff->provider->title . ' ' . $tariff->title;
	}

	protected function description(Article $article)
	{
		$clean = preg_replace('/[\x00-\x1F\x7F]/', '', $article->articleGroup->description_2);
		return html_entity_decode(strip_tags($clean));
	}

	public function getExportData()
	{
		return $this->exportData;
	}

	/**
	 * @return \yii\db\ActiveQuery|bool
	 */
	public function getArticleTariffs()
	{
		return false;
	}

	public function isFormatSupported($format)
	{
		return in_array($format, $this->supportedFormats);
	}
}
