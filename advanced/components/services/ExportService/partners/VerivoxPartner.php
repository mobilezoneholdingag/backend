<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use common\helper\UrlHelper;
use components\services\ExportService\formats\CsvExportFormat;

class VerivoxPartner extends BasePartner
{
	const TOKEN = 'Verivox';

	protected $supportedFormats = ['csv', 'xml'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		if (strpos($article->product_name, '+') !== false)
			return;

		if ($this->format instanceof CsvExportFormat) {
			$line = [
				'id' => $article->id . '-' . $tariff->id,
				'title' => $this->productName($article, $tariff),
				'description' => $this->description($article),
				'google_product_category' => 'Elektronik > Kommunikationsgeräte > Telefone > Mobiltelefone > Smartphones',
				'Produkttyp' => 'Handys ' . $article->manufacturerTitle . ' mit Vertrag',
				'link' => '',
				'image_link' => '',
				'condition' => 'neu',
				'availability' => 'Auf Lager',
				'price' => $articleTariff->price,
				'brand' => $article->manufacturerTitle,
				'gtin' => $article->id,
			];
		} else {
			// XML-Ausgabe
			$line = [
				'offer' => [
					'id' => $article->id . '-' . $tariff->id,
					'title' => 'CDATA:' . $this->productName($article, $tariff),
					'link' => 'CDATA:' . UrlHelper::urlToDevice($article),
					'contract_duration' => $tariff->contract_duration,
				],
				'tariff' => [
					'id' => $tariff->id,
					'title' => $tariff->title,
					'provider' => $tariff->provider->title,
				],
				'hardware' => [
					'id' => $article->id,
					'title' => $article->product_name,
					'manufacturer_title' => $article->manufacturerTitle,
					'variation' => 'CDATA:' . str_replace('_', ' ', $article->variationUrlPart),
				],
				'costs' => [
					'shipping' => '0.00',
					'one_time_costs_hardware' => $articleTariff->price,
					'cash_monthly_hardware' => '0.00',
					'one_time_costs_tariff' => '0.00',
					'cash_monthly_tariff' => $tariff->price_monthly,
				],
			];
		}

		$this->format->export([$line]);
	}
}
