<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleGroups;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use common\helper\UrlHelper;

class BilligerDePartner extends BasePartner
{
	const TOKEN = 'billiger.de';

	protected $supportedFormats = ['csv'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		$this->format->export([[
			'aid' => $article->id . '-' . $tariff->id,
			'brand' => $article->manufacturerTitle,
			'mpnr' => $article->mpn,
			'ean' => $article->id,
			'name' => $this->productName($article, $tariff),
			'desc' => $this->description($article),
			'shop_cat' => 'Handy & Telefon > Handy mit Vertrag', // ?
			'price' => $articleTariff->price,
			'ppu' => $articleTariff->price,
			'link' => UrlHelper::urlToDevice($article, $this->bid),
			'image' => '',
			'dlv_time' => $article->deliveryStatus->label,
			'dlv_cost' => '0.00',
			'pzn' => null
		]]);
	}

	public function getArticleTariffs()
	{
		return ArticleTariffs::find()
			->innerJoin(['a' => Article::tableName()], 'a.id = article_id')
			->innerJoin(['ag' => ArticleGroups::tableName()], 'a.group_id = ag.id')
			->where('tariff_id = ag.start_tariff_id');
	}


}
