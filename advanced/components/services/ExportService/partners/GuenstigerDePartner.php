<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleDetailsValues;
use common\models\ArticleGroups;
use common\models\ArticleTariffs;
use common\models\Tariffs;
use common\helper\UrlHelper;

class GuenstigerDePartner extends BasePartner
{
	const TOKEN = 'guenstiger.de';

	protected $supportedFormats = ['csv'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		if ($articleTariff->price == 0.00) {
			$price = 0.01;
		} else {
			$price = $articleTariff->price;
		}

		$this->format->export([[
			'Bestellnummer' => $article->id . '-' . $tariff->id,
			'HerstellerArtNr' => $article->mpn,
			'Hersteller' => $article->manufacturerTitle,
			'ProduktBezeichnung' => $this->productName($article, $tariff),
			'ProduktBeschreibung' => substr($this->description($article), 0, 180),
			'Preis' => $price,
			'Lieferzeit' => $article->deliveryStatus->label,
			'ProduktLink' => UrlHelper::urlToDevice($article, $this->bid),
			// 'FotoLink' => $this->urlToDeviceImage($article),
			'EANCode' => $article->id,
			'Kategorie' => 'Handy mit Vertrag',
			'Gewicht' => ArticleDetailsValues::getValueByNumber($article->id, 10), //Weight
			'VersandVorkasse' => '0.00',
			'VersandNachnahme' => '0.00',
			'Grundpreis komplett' => $articleTariff->price,
		]]);
	}

	public function getArticleTariffs()
	{
		return ArticleTariffs::find()
				->innerJoin(['a' => Article::tableName()], 'a.id = article_id')
				->innerJoin(['ag' => ArticleGroups::tableName()], 'a.group_id = ag.id')
				->where('tariff_id = ag.start_tariff_id');
	}
}