<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;

class LeadAlliancePartner extends BasePartner
{
	const TOKEN = 'Lead Alliance';

	protected $supportedFormats = ['csv'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		$this->format->export([[
			'id' => $article->id . '-' . $tariff->id,
			'Device' => $this->productName($article, $tariff),
			'Tariff' => $this->fullTariffName($tariff),
			'description' => $this->description($article),
			'google_product_category' => 'Elektronik > Kommunikationsgeräte > Telefone > Mobiltelefone > Smartphones',
			'Produkttyp' => 'Handys ' . $article->manufacturerTitle . ' mit Vertrag',
			'link' => '',
			'image_link' => '',
			'condition' => 'neu',
			'availability' => 'Auf Lager',
			'price' => $articleTariff->price,
			'brand' => $article->manufacturerTitle,
			'gtin' => $article->id,
		]]);
	}

	protected function productName(Article $article, Tariffs $tariff)
	{
		return $article->uniqueTitle;
	}

	protected function fullTariffName(Tariffs $tariff)
	{
		return $tariff->provider->title . ' ' . $tariff->title;
	}
}
