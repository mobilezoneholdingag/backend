<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\services\ExportService\partners;


use common\models\Article;
use common\models\ArticleTariffs;
use common\models\Tariffs;

class InternalPartner extends BasePartner
{
	const TOKEN = 'Internal';

	protected $supportedFormats = ['csv'];

	/** @var array */
	protected $exportData = [];

	public function addProduct(ArticleTariffs $articleTariff, Article $article, Tariffs $tariff)
	{
		$this->format->export([[
			'id' => $article->id . '-' . $tariff->id,
			'title' => $this->productName($article, $tariff),
			'link' => '',
			'price' => $articleTariff->price,
			'profit' => $articleTariff::calcProfit($article, $tariff, $articleTariff),
			'mtl' => $articleTariff->price_monthly
		]]);
	}
}
