<?php
namespace components\services;

use phpseclib\Net\SFTP;
use RuntimeException;
use yii\base\Component;

/**
 * Service for simple handling of FTPS.
 */
class SftpService extends Component
{
	public $credentials = [
		'host' => '',
		'port' => '',
		'user' => '',
		'password' => ''
	];

	public $parentDirectory = '/dev';
	private $sftpHandle;
	public $persistentConnection = true;

	/**
	 * @param $subPath
	 *
	 * @return string
	 */
	private function getFullPath($subPath)
	{
		return rtrim($this->parentDirectory, '/') . '/' . trim($subPath, '/');
	}

	/**
	 * @param string $data
	 * @param string $file
	 *
	 * @return bool
	 */
	public function uploadDataToFileWithSftp($data, $file)
	{
		$sftp = $this->getSftpHandle();

		return $sftp->put($file, $data);
	}

	/**
	 * @param string $subPath
	 *
	 * @return array|null
	 */
	public function getFileListFromSftp($subPath = '')
	{
		$sftp = $this->getSftpHandle();
		$fileList = $sftp->_list($this->getFullPath($subPath));

		// we need no directories
		$fileList = array_filter(
			$fileList,
			function($value) {
				return $value['type'] == 1;
			}
		);

		return $fileList;
	}

	/**
	 * @param string $fileName
	 * @param string $subPath
	 * @param bool $asLocalFileHandle
	 *
	 * @return null|resource|string
	 */
	public function getFileFromSftp($fileName, $subPath = '', $asLocalFileHandle = true)
	{
		$sftp = $this->getSftpHandle();
		$path = $this->getFullPath($subPath) . '/' . $fileName;
		if ($asLocalFileHandle) {
			$fileHandle = tmpfile();
			$sftp->get($path, $fileHandle);

			return $fileHandle;
		} else {
			return $sftp->get($path);
		}
	}

	/**
	 * @param string $fileName
	 * @param string $subPath
	 *
	 */
	public function deleteFileFromSftp($fileName, $subPath = '')
	{
		$sftp = $this->getSftpHandle();
		$sftp->delete($this->getFullPath($subPath) . '/' . $fileName);
	}

	public function getSftpHandle()
	{
		$sftp = $this->sftpHandle;
		if ($sftp === null) {
			$sftp = new SFTP($this->credentials['host'], $this->credentials['port']);
			$result = $sftp->login($this->credentials['user'], $this->credentials['password']);
			if (false === $result) {
				throw new RuntimeException('Login to sftp failed for unknown reason');
			}
		}

		return $sftp;
	}
}