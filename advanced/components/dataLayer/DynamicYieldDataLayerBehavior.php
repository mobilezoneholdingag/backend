<?php

namespace components\dataLayer;

use components\DataLayerBehavior;
use frontend\controllers\SiteController;
use yii\base\Event;
use yii\web\View;

/**
 * Class DynamicYieldDataLayerBehavior
 * @package components\dataLayer
 */
class DynamicYieldDataLayerBehavior extends DataLayerBehavior
{
	const ACTIVE_STATE_PARAM_NAME = 'DynamicYieldActive';

	protected $dataLayerProperties = [
		'type' => '',
		'data' => '',
	];

	protected $dataLayerInitPrefix = 'window.DY = window.DY || {};';
	protected $dataLayerInitVar = 'DY.recommendationContext';
	protected $dataFormat = '';
	protected $cartData = null;


	/**
	 * @override
	 * @return string
	 */
	public function getDataLayer()
	{
		if ('' === $this->dataLayerProperties['type']) {
			return '';
		}

		return parent::getDataLayer();
	}

	/**
	 * set DY events (JS) on beginBody event
	 * @param Event $event
	 * @override
	 * @return bool
	 */
	public function beginBody(Event $event = null)
	{
		parent::beginBody($event);

		/* only set events on cart actions */
		if ($event->sender->context instanceof SiteController ||
			!in_array($event->sender->context->id, ['regular-cart'])
		) {
			return true;
		}

		switch ($event->sender->context->action->id) {
			case 'index':
				\Yii::$app->view->registerJs($this->getAddToCartJS(), View::POS_END);
				break;
			case 'step-5':
				\Yii::$app->view->registerJs($this->getPurchaseJS(), View::POS_END);
				break;
			default:
				return true;
		}
	}

	/**
	 * Generate ad-to-cart event js.
	 * @return string
	 */
	private function getAddToCartJS()
	{
		return "DY.API('event', {
					name: 'Add to Cart',
					properties: {
						dyType: 'add-to-cart-v1',
						value: 25.00, 
						currency: '" . \Yii::$app->formatter->currencyCode . "',
						productId: '" . \Yii::$app->session->get('device_id') . "-" . \Yii::$app->session->get('tariff_id') . "',
						quantity: 1
					}
				});";
	}

	/**
	 * Generate purchase event js.
	 * @return string
	 */
	private function getPurchaseJS()
	{
		return "DY.API('event', {
					name: 'Purchase',
					properties: {
						dyType: 'purchase-v1',
						value: 25.00,
						currency: '" . \Yii::$app->formatter->currencyCode . "',
						cart: [
							{
								productId: '" . $this->cartData['article_id'] . "-" . $this->cartData['tariff_id'] . "',
								quantity: 1,
								itemPrice: 25.00                                
							}
						]
					}
				});";
	}

	/**
	 * Retrieve data from Cart Model (surrogate for session data)
	 * @param array $cartData
	 */
	public function setDataLayerCartData(array $cartData)
	{
		$this->cartData = $cartData;
	}
}