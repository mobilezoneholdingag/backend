<?php
/**
 * @author Dominic Guhl <dominic.guhl@deinhandy.de>
 */

namespace components\dataLayer;

use components\DataLayerBehavior;
use yii\base\Event;
use yii\web\View;

/**
 * components\dataLayer
 *
 * @property string $pageType
 * @property string $pageName
 * @property string $visitorLoginState
 * @property int version
 * @property string $country
 * @property int $userId
 * @property string $language
 * @property string $environment
 *
 * @property string $categoryName
 * @property string $productName
 * @property string $productId
 * @property float $productPrice
 * @property string $productCurrency
 * @property string $productBrand
 * @property string $productCategory
 * @property string $productVariant
 * @property string $productQuantity
 *
 * @property string $ad4matCountry
 * @property string $ad4matAdspaceId
 * @property string $ad4matCatId
 * @property string $ad4matProductId
 * @property string $ad4matSearchName
 * @property string $ad4matSprd
 */
class GoogleDataLayerBehavior extends DataLayerBehavior
{
	protected $dataLayerProperties = [
		'pageType' => '',
		'pageName' => '',
		'visitorLoginState' => 'no',
		'version' => '1',
		'country' => '',
		'userId' => '',
		'language' => 'de_DE',
		'environment' => '',
		// Ad4mat
		'ad4matCountry' => 'ad4mat.de',
		'ad4matAdspaceId' => '15597',
		'ad4matCatId' => '',
		'ad4MatProductId' => '',
		'ad4matSearchName' => '',
		'ad4matSprd' => 'false'
	];

	protected $dataLayerInitVar = 'var dataLayer';
	protected $dataFormat = 'array';


	/**
	 * DataLayerBehavior constructor.
	 * @param array $config
	 */
	public function __construct($config = [])
	{
		parent::__construct($config);

		$this->environment = YII_ENV_DEV ? 'testing' : (YII_ENV_TEST ? 'staging' : 'production');
		$this->pageName = \Yii::$app->view->title;
	}

	/**
	 * @param Event $event
	 * @return null|void
	 */
	public function beginBody(Event $event = null)
	{
		parent::beginBody($event);

		// The code snippet contains noscript elements, which cannot be parsed as a
		// part of JavaScript tags.
		echo $this->getTrackingContainer();
	}

	/**
	 * @return string
	 */
	public function getTrackingContainer()
	{
		$gtmcId = \Yii::$app->params['google.tmc.id'];

		$container = <<<GTMC
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id={$gtmcId}"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','{$gtmcId}');</script>
<!-- End Google Tag Manager -->
GTMC;

		return $container;
	}
}