#!/bin/bash
# Pulls all docker images that are locally existing and appear to have a remote repository (recognized by slash in name)
docker images -a | awk '{print $1}' | fgrep -v "<none>" | fgrep -v REPOSITORY | fgrep / | xargs -L1 docker pull