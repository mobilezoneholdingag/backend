#!/bin/bash

if [ $(whoami) != "root" ]
then
    echo -e "You could need super privileges to setup hosts..."; exit 1;
fi

formerhosts=(
    "192.168.99.100 frontend.stardust.dev backend.stardust.dev api.stardust.dev"
);

hosts=(
    "192.168.99.100 frontend.stardust.dev backend.stardust.dev stardust.api"
);

cp /etc/hosts /etc/hosts.new
for delhost in "${formerhosts[@]}"
do
    echo "Removing... " $delhost
    sed -i "" "/$delhost/d" /etc/hosts.new
done
mv /etc/hosts.new /etc/hosts
for host in "${hosts[@]}"
do
    if ! grep -Fxq "${host}" /etc/hosts
    then
        echo "Adding... " $host;
        echo $host >> /etc/hosts;
    fi
done

echo -e "Host setup completed.";
