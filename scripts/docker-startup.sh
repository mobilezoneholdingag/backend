#!/bin/bash
docker-machine create --virtualbox-cpu-count 4 --virtualbox-memory 1024 --virtualbox-disk-size 50000 -d virtualbox stardust
# Start docker-machine VM with name Stardust
docker-machine start stardust
# Setze path variables um Zugriff auf den den Docker-Daemon zu erhalten
docker-machine env stardust

eval $(docker-machine env stardust)

